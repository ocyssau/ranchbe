#!/usr/bin/php
<?php
/**
 * Search for each mockups double file in previous folders
 */
require_once './conf/cliboot.php';
require_once './class/mockup.php'; //Class to manage the bookshop

$Mockup = new mockup();
$mockups = $Mockup->GetAll();

/*
$size = 0;
foreach($mockups as $mockEntry){
	$folder = $mockEntry['default_file_path'];
	$folder = str_replace('/__imported', '', $folder);

	$i = 1;
	$prevFolder = $folder . '-' . $i;
	while( is_dir($prevFolder) ){
		echo 'Follow the directory ' . $prevFolder . PHP_EOL;
		if( is_dir($prevFolder . '/__imported/cgr') ){
			$prevFiles = glob( $prevFolder . '/__imported/cgr/*' );
		}
		else{
			$prevFiles = glob( $prevFolder . '/__imported/*' );
		}
		foreach($prevFiles as $prevFile){
			$lastFile = str_replace($prevFolder, $folder, $prevFile);
			//echo $lastFile . PHP_EOL;
			if( is_file($lastFile) && is_file($prevFile) ){
				echo 'Suppress obsolete file ' . $prevFile . PHP_EOL;
				$size = $size + filesize($prevFile);
				$ok = unlink($prevFile);
				if(!$ok){
					echo 'Unable to suppress file ' . $prevFile . PHP_EOL;
				}
				//echo $size . PHP_EOL;
			}
		}
		$i++;
		$prevFolder = $folder . '-' . $i;
	}
}

$size = $size/1024; //Ko
$size = $size/1024; //Mo
echo 'Save ' . $size . 'Mo on filesystem' . PHP_EOL;



die;
*/

//=================================================================================
//=================================================================================
//=================================================================================
//=================================================================================


$size = 0;
$count = 0;
foreach($mockups as $mockEntry){
	$folder = $mockEntry['default_file_path'];
	$folder = str_replace('/__imported', '', $folder);

	$i = 1;
	$folder0 = $folder;
	$folder1 = $folder0 . '-' . $i;

	while( is_dir($folder1) ){
		echo 'Follow the directory ' . $folder1 . PHP_EOL;
		if( is_dir($folder1 . '/__imported/cgr') ){
			$f1 = $folder1 . '/__imported/cgr';
			$f0 = $folder0 . '/__imported/cgr';
		}
		else{
			$f1 = $folder1 . '/__imported';
			$f0 = $folder0 . '/__imported';
		}

		$ret = regr_clean_doublons($folder, $i);
		$size = $saved + $ret['saved'];
		$count = $count + $ret['count'];

		$i++;
		$folder0 = $folder1;
		$folder1 = $folder . '-' . $i;
	}
}

$size = $size/1024; //Ko
$size = $size/1024; //Mo
echo 'Save ' . $size . 'Mo on filesystem' . PHP_EOL;


/**
 * Regresive function
 *
 */
function regr_clean_doublons( $folder, $i){
	$saved = 0;
	$count = 0;
	$folder1 = $folder . '-' . $i;
	$i = $i-1;
	$folder0 = $folder . '-' . $i;
	while( is_dir($folder0) && $i >= 0 ){
		//echo  "regr_clean_doublons $folder0 , $i" . PHP_EOL;
		if( is_dir($folder1 . '/__imported/cgr') ){
			$f1 = $folder1 . '/__imported/cgr';
			$f0 = $folder0 . '/__imported/cgr';
		}
		else{
			$f1 = $folder1 . '/__imported';
			$f0 = $folder0 . '/__imported';
		}
		$ret = it_clean_doublons($f0, $f1);
		$size = $saved + $ret['saved'];
		$count = $count + $ret['count'];
		$i = $i-1;
		if($i == 0){
			$folder0 = $folder;
		}
		else{
			$folder0 = $folder . '-' . $i;
		}
	}
	return array('saved'=>$saved, 'count'=>$count);
}


/**
 * Suppress doublons from $folder1 find in $folder0
 *
 * @param string $folder0
 * @param string $folder1
 * @return array('saved'=>byte saved, 'count'=>number of suppressed doublons)
 */
function it_clean_doublons($folder0, $folder1){
	echo "Try to suppress doublons from $folder1 find in $folder0" . PHP_EOL;
	$saved = 0;
	$count = 0;
	$prevFiles = glob( $folder1 . '/*' );
	foreach($prevFiles as $prevFile){
		$lastFile = str_replace($folder1, $folder0, $prevFile);
		$prevFileTs = filemtime($prevFile);
		$toSuppress = false;
		if( is_file($lastFile) && is_file($prevFile) ){
			$lastFileTs = filemtime($lastFile);
			if($lastFileTs >= $prevFileTs){
				$toSuppress = $prevFile;
			}
			else{
				//$toSuppress = $lastFile;
				$prevFileTs = date ("m d Y H:i:s.", $prevFileTs);
				$lastFileTs = date ("m d Y H:i:s.", $lastFileTs);
				echo "The previous file $prevFile is more recent ($prevFileTs) that the last file $lastFile ($lastFileTs)" . PHP_EOL;
			}
			if($toSuppress){
				echo 'Suppress obsolete file ' . $toSuppress . PHP_EOL;
				$size = filesize($toSuppress);
				$ok = unlink($toSuppress);
				if(!$ok){
					echo 'Unable to suppress file ' . $toSuppress . PHP_EOL;
				}
				else{
					$saved = $saved + $size;
					$count++;
				}
			}
		}
	}
	return array('saved'=>$saved, 'count'=>$count);
}


