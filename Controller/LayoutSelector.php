<?php
namespace Controller;

class LayoutSelector
{
	public $pageId;

	/**
	 *
	 * @param unknown_type $pageId
	 */
	public function __construct($controller=null)
	{
	}

	/**
	 *
	 */
	public function setLayout($controller)
	{
		$pageId = $controller->pageId;
		isset($_SESSION['layout'][$pageId]) ? $layout = $_SESSION['layout'][$pageId] : $layout=null;
		isset($_REQUEST['layout']) ? $layout = $_REQUEST['layout'] : null;

		if($layout=='popup'){
			$controller->layout='layouts/popup.tpl';
		}
		else if($layout=='fragment'){
			$controller->layout='layouts/fragment.tpl';
		}
		else if($layout=='dialog'){
			$controller->layout='layouts/dialog.tpl';
		}
		else{
			$controller->layout='layouts/ranchbe.tpl';
		}
		$controller->view->assign('layout', $layout);

		$_SESSION['layout'][$pageId] = $layout;
		$this->controller = $controller;
		return $this;
	}

	/**
	 *
	 */
	public function clear($controller=null)
	{
		if(is_null($controller)){
			$_SESSION['layout'] = null;
		}
		else{
			$pageId = $controller->pageId;
			$_SESSION['layout'][$pageId] = null;
		}
		return $this;
	}

}

