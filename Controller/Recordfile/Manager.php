<?php
namespace Controller\Recordfile;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/common/docfile.php';
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/common/attachment.php';

class Manager extends \Controller\Controller
{
	public $pageId = 'recordfile_manager'; //(string)
	public $defaultSuccessForward = 'recordfile/manager/display';
	public $defaultFailedForward = 'recordfile/manager/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$_SESSION['DisplayDocfile'] = false;
		$_SESSION['DisplayRecordfile'] = true;

		$containerId = null;
		$spaceName = null;
		$pageId = $_REQUEST['page_id'];

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		if($spaceName){
			$this->spaceName = $spaceName;
			$this->container = \container::_factory($spaceName , $containerId);
			$this->space = $this->container->space;
			$this->docfile = $this->container->initRecordfile();
			$this->areaId = $this->container->AREA_ID;
		}

		$this->checkFlood = \check_flood($pageId);
		$this->areaId = $this->container->AREA_ID;

		//Assign name to particular fields
		$this->view->assign('spaceName', $spaceName);
		$this->view->assign('CONTAINER_NUMBER' , $spaceName.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $spaceName.'_description');
		$this->view->assign('CONTAINER_STATE' , $spaceName.'_state');
		$this->view->assign('CONTAINER_ID' , $spaceName.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $spaceName);
		$this->view->assign('file_icons_dir', BASEURL.'/img/filetypes');
		$this->view->assign('sameurl_elements', array('space', 'containerid'));
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 */
	public function viewfileAction()
	{
		isset($_REQUEST['checked']) ? $fileId = $_REQUEST['checked'][0] : null;
		isset($_REQUEST['file_id']) ? $fileId = $_REQUEST['file_id'] : null;

		$recordfile = $this->container->initRecordfile($fileId);
		$fsdata = $recordfile->initFsdata();
		$viewer = new \viewer();
		$viewer->initFsdata($fsdata);
		$viewer->pushfile();
	}

	/**
	 */
	public function deleteAction()
	{
		if( empty($_REQUEST['checked'][0]) ){
			return false;
		}
		$fileIds = $_REQUEST['checked'];

		$this->checkRight('delete', $this->areaId, 0);
		$recordfile = $this->container->initRecordfile();
		foreach($fileIds as $fileId){
			$recordfile->init($fileId);
			$recordfile->RemoveFile(true);
		}
		$this->redirect($this->ifSuccessForward);
	}


	/**
	 */
	public function putinwsAction()
	{
		if( empty($_REQUEST['checked'][0]) ){
			return false;
		}
		$fileIds = $_REQUEST['checked'];

		$this->checkRight('read', $this->areaId, 0);
		$recordfile = $this->container->initRecordfile();
		foreach($fileIds as $fileId){
			$recordfile->init($fileId);
			$recordfile->PutFileInWildspace();
		}
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	function displayAction()
	{
		$spaceName = $this->spaceName;

		$filterForm = new \Form\Docfile\FilterForm( $this->docfile, $this->view, $this->pageId );
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'file_name');
		$filterForm->bind($filter)->save();

		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$count = $this->docfile->count($filter);
		$paginator->setMaxLimit($count);
		$paginator->bind($filter)->save();

		$leftTable = $this->docfile->getTableName('object');
		$rightTable = $spaceName.'_documents';

		$select = array(
				'r.document_number',
				'r.document_state',
				'r.document_version',
				'r.document_indice_id',
				$leftTable.'.*'
		);

		$filter->with(array('table'=>$rightTable, 'alias'=>'r','on'=>'document_id'));
		$filter->select($select);

		//get all documents
		if(!$list){
			$list = $this->docfile->GetAllFilesInFather($containerId , $params);
		}
		$this->view->assign('list', $list);

		$template = 'recordfile/manager/display.tpl';

		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}

} //End of class
