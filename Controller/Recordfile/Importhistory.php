<?php
namespace Controller\Recordfile;

use Rbplm\Dao\Filter\Op;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/common/container.php'; //Class to manage the container
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/common/import.php'; //Class to manage the importations

class Importhistory extends \Controller\Document\Importhistory
{
	public $pageId = 'recordfile_importhistory';
	public $defaultSuccessForward = 'recordfile/importhistory/display';
	public $defaultFailedForward = 'recordfile/importhistory/display';
}
