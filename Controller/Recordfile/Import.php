<?php
namespace Controller\Recordfile;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/common/import.php';
require_once 'class/common/docfile.php';
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/common/attachment.php';
require_once 'lib/Date/date.php';
require_once 'class/messu/messulib.php';

class Import extends \Controller\Controller
{
	public $pageId = 'recordfile_import'; //(string)
	public $defaultSuccessForward = 'recordfile/import/display';
	public $defaultFailedForward = 'recordfile/import/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode

		$containerId = null;
		$spaceName = null;
		$pageId = $_REQUEST['page_id'];

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : null;
		isset($_REQUEST['page_id']) ? $this->pageId = $_REQUEST['page_id'] : null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		if($spaceName){
			$this->spaceName = $spaceName;
			$this->container = \container::_factory($spaceName , $containerId);
			$this->space = $this->container->space;
			$this->docfile = $this->container->initRecordfile();
			$this->areaId = $this->container->AREA_ID;
			$this->import = new \import($spaceName, $this->container);
		}

		$this->checkFlood = \check_flood($pageId);
		$this->areaId = $this->container->AREA_ID;
		$areaId = $Manager->AREA_ID;

		//Assign name to particular fields
		$this->view->assign('spaceName', $spaceName);
		$this->view->assign('CONTAINER_TYPE', $spaceName);
		$this->view->assign('CONTAINER_NUMBER' , $spaceName.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $spaceName.'_description');
		$this->view->assign('CONTAINER_STATE' , $spaceName.'_state');
		$this->view->assign('CONTAINER_ID' , $spaceName.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $spaceName);
		$this->view->assign('file_icons_dir', BASEURL.'/img/filetypes');
		$this->view->assign('sameurl_elements', array('space', 'containerid'));
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 */
	public function selectcontainerforimportAction()
	{
		$conainerIds = $_REQUEST['checked'];
		if ( count($conainerIds) == 1 ){
			$this->view->assign('container_id' , $conainerIds[0]);
		}
		else{
			die('none container selected');
		}
	}

	/**
	 */
	public function suppresspackageAction()
	{
		if( empty($_REQUEST['checked']) ){
			return false;
		}
		$fileIds = $_REQUEST['checked'];
		$this->checkRight('edit', $this->areaId, 0);

		foreach($fileIds as $importOrder){
			$Manager->SuppressPackage($importOrder);
		}
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function uncompresspackageAction()
	{
		if(empty($_REQUEST['checked'][0])){
			return false;
		}
		$fileIds = $_REQUEST['checked'];
		$this->checkRight('edit', $this->areaId, 0);

		$recordfile = $this->container->initRecordfile();
		foreach($fileIds as $importOrder){
			$Manager->UncompressPackage($importOrder, null);
		}
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function unpackAction()
	{
		$background = $_REQUEST['background'];
		$fileIds = $_REQUEST['checked'];
		$containerId = $_REQUEST['container_id'];
		$target = $_REQUEST['target'];

		$start_time = time();

		//Tache execute en arriere plan
		//If we want lunch the process in background
		//See chapter 40 of php documentation
		if ($background){
			session_write_close(); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
			ignore_user_abort(true); //Continue execution after deconnection of user

			print formatDate($start_time).'
			- La tache s\'execute en arriere plan. Vous recevrez un message lorsqu\'elle sera terminée.<br />
			Vous pouvez fermer cette fenêtre.<br />
			<a href="javascript:window.close()">Close Window</a><br />';
			flush(); // On vide le buffer de sortie
		}

		//unpack each package
		foreach ($fileIds as $importOrder){
			if (!$Manager->ImportPackage($importOrder, $containerId, $target)){
				$body = '<b>Not Imported package : </b>'.$Manager->PackInfos['package_file_name'].'<br/>';
				$body.= '<b>An error is occured during import task</b><br />';
				$subject = 'Error in import task of '.$Manager->PackInfos['package_file_name'];
			}
			else{
				$body = '<b>Imported package : </b>'.$Manager->PackInfos['package_file_name'].'<br/>';
				$subject = 'End of import task of '.$Manager->PackInfos['package_file_name'];
			}

			$end_time = time();

			$duration = ($end_time - $start_time);

			$body.= '<b>target dir : </b>'.$Manager->target_dir.'<br/>';
			$body.= '<b>target container : </b>'.$Manager->target_container_num.'<br/>';
			$body.= '<b>By : </b>'.$Manager->importUserName.'<br/>';
			$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
			$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
			$body.= '<b>duration : </b>'.$duration.' sec<br/>';

			if ($background){ //Tache execute en arriere plan
				$to = $user;
				$from = $user;
				$cc   = '';
				$body = str_replace('<br/>',"\n",$body);
				$priority = 3;
				$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
			}
			else{
				echo $body;
			}
		}
	}

	/**
	 *
	 */
	function displayAction()
	{
		$list = $this->import->GetAllImportPackage($params); //get infos on all package in deposit directories
		$this->view->assign('list', $list);

		$template = 'recordfile/import/display.tpl';

		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}

} //End of class
