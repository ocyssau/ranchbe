<?php
/**
* Zend Framework (http://framework.zend.com/)
*
* @link http://github.com/zendframework/zf2 for the canonical source repository
* @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
* @license http://framework.zend.com/license/new-bsd New BSD License
*/
use Zend\Json\Server\Client as JsonClient;
use Zend\Http\Client as httpClient;

ini_set('display_errors' , 1);
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING ^ E_STRICT ^ E_WARNING);

include('conf/boot.php');
rbinit_autoloader();
rbinit_includepath();

$httpClient=new httpClient();
$client = new JsonClient('http://192.9.200.4/rbsier/rest', $httpClient);
$client->call($method='test',$params=array());
var_dump($httpClient);

class RestClient
{
}
