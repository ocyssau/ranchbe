<?php
namespace Controller\Metadata;

use Rbs\Sys\Session;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended\Property;
use Rbplm\Dao\Filter\Op;

class Manager extends \Controller\Controller
{
	public $pageId = 'metadata_manager';
	public $defaultSuccessForward = 'metadata/manager/display';
	public $defaultFailedForward = 'metadata/manager/display';

	/**
	 * @var string
	 */
	protected $extendedObjectClass;

	/**
	 * @var container|document
	 */
	protected $extendedObject;

	/**
	 * @var metadata
	 */
	protected $metadata;

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;
		isset($_REQUEST['class']) ? $extendedCid = $_REQUEST['class'] : $extendedCid=null;

		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->extendedCid = $extendedCid;
		$this->checkFlood = \check_flood($pageId);

		\View\Helper\MainTabBar::get();

		$this->view->assign('spacename', $spaceName);
		$this->view->assign('class', $extendedCid);
		$this->view->assign('layout' , 'popup');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $propertyIds = $_REQUEST['checked'] : $propertyIds=array();

		//$areaId = $this->extendedObject->AREA_ID;
		$this->checkRight('admin', $this->areaId, 0);

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Property::$classId);

		foreach ( $propertyIds as $propertyId ){
			try{
				$dao->deleteFromId($propertyId);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
			}
		}

		$this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
	}

	/**
	 *
	 */
	public function createAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		isset($_REQUEST['extendedcid']) ? $extendedCid = $_REQUEST['extendedcid'] : $extendedCid = null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Property::$classId);

		//Set default values
		$property = Property::init();
		$property->setAppName('');
		$property->setExtended($extendedCid);
		$property->setType('text');
		$property->description = '';
		$property->label = '';
		$property->multiple = 0;
		$property->return = 0;
		$property->hide = 0;
		$property->attributes = array();
		$property->min = -1;
		$property->max = -1;
		$property->step = 1;

		//Get the project infos
		$form = new \Form\Metadata\EditForm($this->view, $factory);
		$form->setAttribute('action', $this->getRoute());
		$form->bind($property);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$property->setAppName($property->getAppName());
					$extendTable = $factory->getTable($extendedCid);
					$dao->setExtendedTable($extendTable);
					$dao->insert($property);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Create Property');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function editAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		isset($_REQUEST['id']) ? $propertyId = $_REQUEST['id'] : $propertyId = null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Property::$classId);
		$property = new Property();
		$dao->loadFromId($property, $propertyId);

		//Get the project infos
		$form = new \Form\Metadata\EditForm($this->view, $factory);
		$form->setAttribute('action', $this->getRoute());
		$form->removeElement('type');
		$form->removeElement('extendedcid');
		$form->bind($property);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->update($property);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Edit Property '. $property->getName());
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function displayAction($list=null)
	{
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(\Rbs\Extended\Property::$classId);
		$extendedCid = $this->extendedCid;

		$filterForm = new \Form\Metadata\FilterForm($this->view, $factory, $this->pageId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm->bind($filter)->save();

		if(!$list){
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = "`$asSys` AS `$asApp`";
			}
			$filter->select($select);

			if($extendedCid){
				$filter->andfind(':extendedCid', $dao->toSys('extendedCid'), Op::EQUAL);
				$bind = array(':extendedCid'=>$extendedCid);
			}

			$list = $factory->getList(\Rbs\Extended\Property::$classId);
			$list->load($filter, $bind);
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('mid', 'metadata/display.tpl');
		$this->view->assign('extendedObjectClass', 'document');
		$this->view->display($this->layout);
	}
}
