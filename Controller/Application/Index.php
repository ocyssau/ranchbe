<?php
namespace Controller\Application;


class Index extends \Controller\Controller
{
	public $pageId = 'application_index';
	public $defaultSuccessForward = 'application/index/index';
	public $defaultFailedForward = 'application/index/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 *
	 */
	public function getfileAction()
	{
		isset($_REQUEST['object_class']) ? $objectClass = $_REQUEST['object_class'] : null;
		isset($_REQUEST['file']) ? $file = $_REQUEST['file'] : null;
		isset($_REQUEST['file_mime_type']) ? $mimeType = $_REQUEST['file_mime_type'] : null;

		if( $objectClass == 'thumbnail' ){
			if( !strpos($file, '/__attachments/_thumbs/') ){
				return false;
			}
		}
		else if( $objectClass == 'visu' ){
			if( !strpos($file, '/__attachments/_visu/') ){
				return false;
			}
		}
		else if( $objectClass == 'picture' ){
			if( !strpos($file, '/__attachments/_picture/') ){
				return false;
			}
		}
		else{
			return false;
		}

		if( $mimeType ){
			header( 'Content-type: '.$mimeType );
		}
		else{
			header( 'Content-type: application/data' );
		}

		echo file_get_contents($file);
	}

	/**
	 *
	 */
	public function viewfileAction()
	{
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['checked']) ? $fileId = $_REQUEST['checked'][0] : null;
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : null;
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : null;
		isset($_REQUEST['class']) ? $objectClass = $_REQUEST['class'] : null;

		if( $documentId ){
			$this->redirect('document/viewer/viewfile', array('documentid'=>$documentId, 'space'=>$spaceName));
		}
		else if( $fileId ){
			if( $objectClass == 'docfile' ){
				$this->redirect('docfile/manager/viewfile', array('fileid'=>$fileId, 'space'=>$spaceName));
			}
			else if( $objectClass == 'recordfile' ){
				$this->redirect('recordfile/manager/viewfile', array('fileid'=>$fileId, 'space'=>$spaceName));
			}
			else if( $objectClass == 'recordfileVersion' ){
				$this->redirect('docfile/version/viewfile', array('fileid'=>$fileId, 'space'=>$spaceName));
			}
			else if( $objectClass == 'docfileVersion' ){
				$this->redirect('docfile/version/viewfile', array('fileid'=>$fileId, 'space'=>$spaceName));
			}
			else{
				$this->errorStack->warning('can not display %datapath%', array('datapath'=>$datapath));
				die;
			}
		}
	}

	/**
	 *
	 */
	public function viewobjectAction()
	{
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id=null;
		isset($_REQUEST['class']) ? $objectClass = $_REQUEST['class'] : $objectClass=null;

		if( $objectClass==\Rbplm\Ged\Document\Version::$classId ){
			$this->redirect('rbdocument/detail/index', array('documentid'=>$id, 'spacename'=>$spaceName));
		}
		elseif( $objectClass==\Rbplm\Ged\Docfile\Version::$classId ){
			$this->redirect('docfile/manager/viewfile', array('fileid'=>$id, 'spacename'=>$spaceName));
		}
		elseif( $objectClass==\Rbplm\Org\Workitem::$classId ){
			$this->redirect('rbdocument/manager/index', array('containerid'=>$id, 'spacename'=>$spaceName));
		}
		elseif( $objectClass==\Rbplm\Pdm\Product\Version::$classId ){
			$this->redirect('pdm/productexplorer/index/'.$id, array());
		}
		elseif( $objectClass==\Rbplm\Org\Project::$classId ){
			$this->redirect('project/index/edit', array('projectid'=>$id));
		}
		else{
			throw new \Exception(sprintf('Object class %s is unknow',$objectClass));
		}
	}

}