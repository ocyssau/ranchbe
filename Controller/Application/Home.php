<?php
namespace Controller\Application;

use Rbs\Space\Factory as DaoFactory;

use Rbplm\Sys\Message;
use Rbplm\Org\Container\Favorite;
use Rbplm\People\CurrentUser;
use Rbplm\Dao\Filter\Op;

/**
 *
 *
 */
class Home extends \Controller\Controller
{
	public $pageId = 'application_home';
	public $defaultSuccessForward = 'application/home/index';
	public $defaultFailedForward = 'application/home/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->factory=DaoFactory::get();
	}

	/**
	 *
	 */
	public function indexAction()
	{
		return $this->homeAction();
	}

	/**
	 *
	 */
	public function homeAction()
	{
		$currentUser = CurrentUser::get();
		$userName = $currentUser->login;
		$filter = $this->factory->getFilter(Message::$classId);
		$filter->page(1,5);
		$filter->sort('date','DESC');
		$filter->andFind('n','isRead', Op::EQUAL); // Get unread messages

		$list = $this->factory->getList(Message::$classId);
		$bind = array();
		$list->load($filter, $bind);
		$this->view->assign('items', $list->toArray());

		//list of favorites
		$favorites = $this->factory->getDao(Favorite::$classId);
		$favoritesList = $favorites->getFromUserId($currentUser->getId());
		$this->view->assign('favorites', $favoritesList);

		// Display the template
		\View\Helper\MainTabBar::get()->getTab('home')->activate();
		$this->view->assign('mid', 'application/home.tpl');
		$this->view->display($this->layout);
	}
}