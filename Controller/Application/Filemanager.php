<?php
namespace Controller\Application;

class Filemanager extends \Controller\Controller
{

	public $pageId = 'application_filemanager';

	public $defaultSuccessForward = 'application/filemanager/index';

	public $defaultFailedForward = 'application/filemanager/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$fileManager = new fileManager(USER_QUERIES_REPOSIT_DIR);
		$fileManager->getFiles();
		$fileManager->render();

		if ( empty($directory) ) {
			print 'Fatal Error: var $directory is empty <br />';
			die();
		}

		if ( !filesystem::limitDir($directory) ) {
			print 'Fatal Error: access to directory ' . $directory . ' is not auhorized. See $AUTHORIZED_DIR variable configuration <br />';
		}

		if ( !is_dir($directory) ) {
			print 'Error : USER_QUERIES_REPOSIT_DIR  do not exits';
			die();
		}

		$this->directory = $directory;
		$this->check_flood = check_flood();

		$this->view = & Ranchbe::getView();
	}

	/**
	 */
	public function suppressAction()
	{
		if ( empty($_REQUEST['checked'][0]) ) return;

		foreach( $_REQUEST['checked'] as $file_name ) {
			$ofile = new file($this->directory . '/' . $file_name);
			$ofile->suppress();
		}
	}

	/**
	 */
	public function copyAction()
	{
		if ( empty($_REQUEST['checked']) ) return false;
		if ( empty($_REQUEST['newName']) ) return false;
		if ( $_REQUEST['newName'] == 'null' ) return false;

		$ofile = new file($this->directory . '/' . $_REQUEST['checked']);

		if ( $_REQUEST['action'] == 'renameQueryFile' ) {
			$ofile->rename($this->directory . '/' . $_REQUEST['newName']);
		}
		if ( $_REQUEST['action'] == 'copyQueryFile' ) {
			$ofile->copy($this->directory . '/' . $_REQUEST['newName'], 0666, false);
		}
	}

	/**
	 */
	public function uploadAction()
	{
		file::UploadFile($_FILES['uploadFile'], $_REQUEST['overwrite'], $this->directory);
	}

	/**
	 */
	public function downloadAction()
	{
		require_once ('./class/common/attachment.php');
		$viewer = new viewer();
		$viewer->initRawFile($this->directory . '/' . $_REQUEST['checked'][0]);
		$viewer->pushfile();
	}

	/**
	 */
	public function downloadzipAction()
	{
		require_once ('File/Archive.php');
		File_Archive::setOption('zipCompressionLevel', 9);
		if ( is_array($_REQUEST['checked']) ) {
			foreach( $_REQUEST['checked'] as $file ) {
				$multiread[] = File_Archive::read($this->directory . '/' . $file, $file);
			}
		}
		else {
			return false;
		}
		$reader = File_Archive::readMulti($multiread);
		$writer = File_Archive::toArchive('files.zip', File_Archive::toOutput());
		File_Archive::extract($reader, $writer);
	}

	/**
	 */
	public function renameAction()
	{
		if ( empty($_REQUEST['checked']) ) return false;
		if ( empty($_REQUEST['newName']) ) return false;
		if ( $_REQUEST['newName'] == 'null' ) return false;

		$ofile = new file($this->directory . '/' . $_REQUEST['checked']);
		$ofile->rename($this->directory . '/' . $_REQUEST['newName']);
	}

	/**
	 */
	function getFiles()
	{
		$list = rdirectory::GetDatas($this->directory, $params);
		$this->view->assign_by_ref('list', $list);
	}

	/**
	 */
	function displayAction()
	{
		$this->view->assign('mid', 'application/filemanager.tpl');
		$this->view->display($this->layout);
	}
}



