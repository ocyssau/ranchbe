<?php
namespace Controller\Session;

use Rbs\Sys\Session;
use Rbplm\Org\Workitem;
use Rbplm\People\CurrentUser;

class Manager extends \Controller\Controller
{
	public $pageId = 'session_manage'; //(string)
	public $defaultSuccessForward = 'session/manager/index';
	public $defaultFailedForward = 'session/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('User')->activate('context');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$context = Session::get();

		//Assign name to particular fields
		$this->view->assign('area_id', $context->areaId);
		$this->view->assign('project_number', $context->projectNumber);
		$this->view->assign('project_id', $context->projectId);
		$this->view->assign('container_number', $context->containerNumber);
		$this->view->assign('container_id', $context->containerId);
		$this->view->assign('container_type', $context->spaceName);

		//Get detail on each object
		if ( isset ($context->objectList )){
			foreach ( $context->objectList as $object ){
				$type = $object['object_class'];
				if ($type == 'process')
					$list[] = array ( 'number' => $object['name'] ,
						'type' => $type ,
						'id' => $object['pId'], );
				else
					$list[] = array ( 'number' => $object[$type.'_number'] ,
						'type' => $type ,
						'id' => $object[$type.'_id'], );
			}// End of foreach
		}

		$this->view->assign_by_ref('list', $list);

		$this->view->assign('HeaderCol1', 'Type');
		$this->view->assign('HeaderCol2', 'Number');
		$this->view->assign('HeaderCol3', 'Id');

		$this->view->assign('id', 'id');

		$this->view->assign('col1', 'type');
		$this->view->assign('col2', 'number');
		$this->view->assign('col3', 'id');

		$this->view->assign('mid','session/index.tpl');
		$this->view->display('layouts/popup.tpl');

		if(DEBUG){
			echo '<hr /><br/><b>DEBUG : Dump of the session :</b><br />';
			echo '<pre>';var_dump($_SESSION);echo '</pre>';
		}
	}

	/**
	 *
	 */
	public function activateAction()
	{
		$containerId = null;
		$space = null;

		$_SESSION['DisplayDocfile'] = false;
		$_SESSION['DisplayRecordfile'] = false;

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : $containerId=null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;

		$session = Session::get();
		$userId = CurrentUser::get()->getId();

		if($spaceName){
			$this->container = $session->activate($containerId, $spaceName);
			$this->spaceName = $spaceName;
			$this->areaId = $this->container->AREA_ID;
		}

		if($this->container->fileOnly == 1){
			$this->redirect('recordfile/manager/display', array('spacename'=>$spaceName, 'containerid'=>$containerId));
		}
		else{
			$this->redirect('rbdocument/manager/index', array('spacename'=>$spaceName, 'containerid'=>$containerId));
		}
	}

	/**
	 *
	 */
	function resetAction()
	{
		$context = Session::get();
		$usr = CurrentUser::get();
		$context->clean($usr);
		$_SESSION['tabs']=null;
		$this->redirect('session/manager/index');
	}

	/**
	 *
	 */
	function generatecatiasearchorderAction()
	{
		$context = Session::get();
		$wildspace = new wildspace;
		$paths = array($wildspace->GetPath());

		$path_mapping = Ranchbe::getPathMap();
		$usr = Ranchbe::getCurrentUser();

		$path_mapping = str_replace( '%user%' ,  $usr->getLogin() , $path_mapping);
		$path_mapping = array_flip($path_mapping);
		$path_mapping = str_replace( '%user%' ,  $usr->getLogin() , $path_mapping);
		$path_mapping = array_flip($path_mapping);

		$search = array_keys($path_mapping);
		$replace = array_values($path_mapping);

		foreach (array('workitem','mockup','cadlib') as $container_type){
			if(is_array($context->links[$container_type])){
				foreach($context->links[$container_type] as $container){
					$paths[] =  $container['default_file_path'];
				}
			}
		}
		$res = str_replace( $search ,  $replace , $paths);
		if(CLIENT_OS == 'WINDOWS'){
			$res = str_replace( '/' ,  '\\' , $res);
		}
		$searchOrder = implode(PHP_EOL, $res);

		$this->view->assign('searchOrder', $searchOrder);
		$this->view->assign('CLIENT_OS', CLIENT_OS);
	}

	/**
	 *
	 */
	function savesearchorderAction()
	{
		if(CLIENT_OS == 'WINDOWS'){
			$_REQUEST['searchOrder'] = str_replace( '\\\\' ,  '\\' , $_REQUEST['searchOrder']);
		}
		header("Content-disposition: attachment; filename=".$_SESSION['SelectedProjectNum'].'_SearchOrder.txt');
		header("Content-Type: " . 'text/plain');
		header("Content-Transfer-Encoding: searchOrder\n"); // Surtout ne pas enlever le \n
		header("Content-Length: ".strlen($_REQUEST['searchOrder']));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		print $_REQUEST['searchOrder'] . PHP_EOL;
		die;
	}

} //End of class
