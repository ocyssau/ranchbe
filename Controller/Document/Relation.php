<?php
namespace Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Document\Link as DocLink;

class Relation extends \Controller\Controller
{
	public $pageId = 'document_relation'; //(string)
	public $defaultSuccessForward = 'rbdocument/relation/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$spaceName = null;

		(isset($_REQUEST['page_id'])) ? $pageId = $_REQUEST['page_id'] : $pageId=$this->pageId;
		(isset($_REQUEST['basecamp'])) ? $basecamp = $_REQUEST['basecamp'] : $basecamp=null;
		(isset($_REQUEST['spacename'])) ? $spaceName = $_REQUEST['spacename'] : null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;

		$this->spaceName = $spaceName;
		$this->checkFlood = \check_flood($pageId);
		$this->documentId = $documentId;

		$this->view->assign('spacename' , $spaceName);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 * Remove a son from document
	 */
	function removelinkService()
	{
		$return = array('errors', 'feedbacks');

		$input = $_REQUEST;
		isset($input['checked']) ? $checked = $input['checked'] : $checked=null;

		$spaceName = $this->spaceName;
		$areaId = $this->areaId;

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		$linkDao = DaoFactory::get($spaceName)->getDao(DocLink::$classId);

		foreach ($checked as $linkId){
			$linkDao->deleteFromId($linkId);
		}

		return $this->serviceReturn($return);
	}

	/**
	 * Add as child
	 *
	 * params{
	 *		    'space'=''
	 *		    'documentid' = '';
	 *          'space' = ''
	 *		    'encode' = 'json';
	 *		    'children'={
	 *			'child1':{
	 *				'documentid':,
	 *				'space':,
	 *			}
	 *		}
	 *	}
	 *
	 *
	 */
	function addchildrenService()
	{
		$return = array('errors', 'feedbacks');

		$input = $_REQUEST;
		isset($input['spacename']) ? $spaceName = $input['spacename'] : $spaceName=null;
		isset($input['documentid']) ? $documentId = $input['documentid'] : $documentId=null;
		isset($input['children']) ? $children = $input['children'] : $children=null;

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(DocLink::$classId);
		$document = $factory->getDao(DocumentVersion::$classId)->loadFromId(new DocumentVersion(), $documentId);

		$aCode = $document->checkAccess();
		if($aCode >= 100){
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('accessCode'=>$aCode));
		}

		/*------------- parse the document from input -------------*/
		for($i=2,$index='child1'; isset($children[$index]); $index='child'.$i++)
		{
			$message='link creation is ok';
			$child1 = $children[$index];
			$childId = $child1['documentid'];
			$childSpace = $child1['space'];
			$docLink = new DocLink();
			$docLink->setParent($document);
			$docLink->hydrate(array(
				'childId'=>$childId,
				'childSpace'=>$childSpace,
			));
			try{
				$dao->save($docLink);
			}
			catch(\Exception $e){
				$message=$e->getMessage();
			}

			$return['feedbacks']=array(
				'index'=>$index,
				'message'=>$message,
				'id'=>$childId,
				'properties'=>array(),
			);
		}

		return $this->serviceReturn($return);
	}

} //End of class
