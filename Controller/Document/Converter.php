<?php
namespace Controller\Document;

/*
use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;

use Rbs\Ged\Document\History as DocumentHistory;
use Rbplm\Signal;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbplm\Sys\Error;
use Rbplm\Ged\AccessCodeException;

use Rbs\Extended;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Ged\AccessCode;
*/

class Converter extends \Controller\Controller
{
	public $pageId = 'document_converter'; //(string)
	public $defaultSuccessForward = 'document/converter/index';
	public $defaultFailedForward = 'document/converter/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName = null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=null;

		$this->spaceName = $spaceName;
		$this->checkFlood = \check_flood($pageId);

		//Assign name to particular fields
		$this->view->assign('spacename' , $spaceName);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}

	/**
	 *
	 */
	function convertAction()
	{
		$form = new \HTML_QuickForm('form', 'post');
		$form->setAttribute('action',$this->getRoute());
		$renderer = $form->defaultRenderer();
		$form->setDefaults(array(
				'convert_format' => 'pdf',
		));

		//Select output format
		$format = array('ps'=>'ps', 'pdf'=>'pdf');
		$select = $form->addElement('select', 'convert_format', tra('output format'), $format);
		$select->setSize(1);
		$select->setMultiple(false);

		//Submit
		$form->addElement('submit', 'action', 'convert');
		$form->addElement('submit', 'cancel', 'Cancel');
		// Tries to validate the form
		if ($form->validate()) {
			if($_REQUEST['convert_format'] === 'pdf'){
				$output_dir = "c:/tmp/docconverter/";
				$doc_file = "c:/tmp/docconverter/test.doc";
				$pdf_file = "test1.pdf";
				$output_file = $output_dir.$pdf_file;
				$doc_file = "file:///".$doc_file;
				$output_file = "file:///".$output_file;
				\Ranchbe::log( 'documentManage.php::convert docfile: '.$doc_file.' output file: '.$output_file );
				//word2pdf($doc_file,$output_file);
			}
			elseif($_REQUEST['convert_format'] === 'ps'){

			}
		}
		$form->display();
	}

}