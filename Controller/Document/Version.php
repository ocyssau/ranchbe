<?php

namespace Controller\Document;

use Rbplm\Dao\Factory as DaoFactory;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;

use Rbplm\Org\Workitem;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbplm\Sys;
use Rbplm\Sys\Exception as RbplmException;
use Rbplm\Sys\Error;
use Rbplm\Sys\Exception;

class Version extends \Controller\Controller
{
    const ERROR_UPLOADFILE_CHECKSUM=10000;
    const ERROR_DOCUMENT_LOAD=10001;
    const ERROR_DOCFILE_LOAD=10002;
    const NOT_CHECKOUTED=10004;
    const USE_BY_OTHER_USER=10005;
    const FILE_NOT_EXISTING=10006;

    const ERROR_CHECKOUT=10007;
    const CHECKOUTED=10008;
    const NOT_FREE=10009;
    const ERROR_SAVE_RELATION=10010;
    const ERROR_CREATE_DOCUMENT=10011;

    /**
     * Create a resource
     *
     * document1.name
     *          .number
     *          .version
     *          .iteration
     *          .description
     *          .category.name
     *          .category.id
     *          .container.name
     *          .container.id
     *          .process.id
     *          .process.name
     *          .relations.parent1.id
     *          .relations.parent1.class
     *          .relations.child1.id
     *          .relations.child1.class
     *          .docfile1.name
     *          .docfile1.id
     *          .docfile1.version
     *          .docfile1.iteration
     *          .docfile1.description
     *          .docfile1.rule
     *          .docfile1.data.name
     *          .docfile1.data.md5
     *          .docfile1.data.size
     *          .docfile1.data.data
     *
     * @param  mixed $input
     * @return ApiProblem|mixed
     */
    public function createAction()
    {
        rbinit_rbplmuser();

        $input = $_REQUEST;
        $documents = $input['documents'];
        $documents = json_decode(json_encode($documents), false);

        /*------------- parse the document from input -------------*/
        for($i=2,$index='document1'; isset($documents->$index); $index='document'.$i++)
        {
            $doc1 = $documents->$index;

            isset($doc1->id)? $id = $doc1->id : $id = null;
            isset($doc1->name)? $name = $doc1->name : $name = null;

            try{
                //store document
                $feed = $this->_createOneDocument($doc1);
                $return['feedback'][]=array(
                    'index'=>$index,
                    'documentid'=>$doc1->id,
                    'documentname'=>$doc1->name,
                    'checkoutfeed'=>$feed,
                );

                $id = $doc1->id;

                //Create the relation between documents
                if(isset($doc1->relations)){
                    for($j=1,$index2='parent1'; isset($doc1->relations->$index2); $index2='parent'.$j++)
                    {
                        $parent = $doc1->relations->$index2;
                        $parentId = $parent->id;
                        $return[] = $this->_createLink($parentId, $id);
                    }

                    for($j=1,$index2='child1'; isset($doc1->relations->$index2); $index2='child'.$j++)
                    {
                        $child = $doc1->relations->$index2;
                        $childId = $child->id;
                        $return[] = $this->_createLink($id, $childId);
                    }
                }
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'index'=>$index,
                    'function'=>__FUNCTION__,
                    'documentid'=>$id,
                    'documentname'=>$name,
                    'code'=>self::ERROR_CREATE_DOCUMENT,
                    'message'=>'Error during document storing',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                );
                continue;
            }
        }
        echo json_encode($return);
    }

    /**
     *
     * @param stdObject $doc1
     */
    private function _createOneDocument($doc1)
    {
    	$factory = DaoFactory::get();

        try {
            isset($doc1->name)? $name = $doc1->name : $name = null;
            isset($doc->number)? $number = $doc->number : $number = uniqid($name);
            isset($doc1->description)? $description = $doc1->description : $description = null;
            isset($doc1->version)? $version = $doc1->version : $version = null;
            isset($doc1->iteration)? $iteration = $doc1->iteration : $iteration = null;

            $categoryId=null;
            $fileExtension=null;
            $processName = null;
            $processId = null;
            $categoryId = null;
            $containerId = null;

            if(isset($doc1->container)){
                isset($doc1->container->id)? $containerId = $doc1->container->id : null;
            }
            if(isset($doc1->category)){
                isset($doc1->category->id)? $categoryId = $doc1->category->id : null;
            }
            if(isset($doc1->process)){
                isset($doc1->process->id)? $processId = $doc1->process->id : null;
                isset($doc1->process->name)? $processName = $doc1->process->name : null;
            }
            $workitem = new Workitem();
            $factory->getDao($workitem)->load($workitem, $containerId);
        }
        catch (\Exception $e){
            return array(
                'returnCode'=>$e->getCode(),
                'ErrorMessage'=>$e->getMessage(),
            );
        }

        try {
            $document = new DocumentVersion();
            $docDao = $factory->getDao($document);
            $docDao->loadFromName($document, $name);
            return array(
                'returnCode'=>1000,
                'ErrorMessage'=>'DOCUMENT_NAME_IS_EXISTING',
            );
        }
        catch (\Exception $e){
            $document = DocumentVersion::init($name);
            $document->version = $version;
            $document->iteration = $iteration;
        }

        $document->description = $description;
        $document->categoryId = $categoryId;
        $document->processId = $processId;
        $document->setParent($workitem);
        $document->setNumber($number);

        $reposit = Reposit::init($workitem->repositPath);

        //Docfiles
        for($i=2,$index='docfile1'; isset($doc1->$index); $index='docfile'.$i++)
        {
            $inDocfile = $doc1->$index;
            $docfile = DocfileVersion::init($inDocfile->name);
            $docfile->version = $inDocfile->version;
            $docfile->iteration = $inDocfile->iteration;
            $docfile->description = $inDocfile->description;
            $docfile->rule = $inDocfile->rule;
            $docfile->setParent($document);
            if($i == 1){
                $mainDocfile = $inDocfile;
                $fileExtension = \Rbplm\Sys\Datatype\File::sGetExtension($mainDocfile->data->name);
            }
            try{
                //Put data in Vault
                $data = base64_decode($inDocfile->data->data);
                $record = Record::init($docfile->name);
                $toPath = $reposit->getUrl() .'/'. $docfile->name;
                file_put_contents($toPath, $data);

                $toFsdata = new Fsdata($toPath);
                $record->setFsdata($toFsdata);
                $docfile->setData($record);
                unset($data);
            }
            catch (RbplmException $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
            catch (\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
            $docfiles[]=$docfile;
        }

        $doctype = new Doctype();
        $factory->getDao($doctype)->loadFromDocument($doctype, $document, $fileExtension);
        $document->setDoctype($doctype);

        //Save Document
        $docDao->save($document);
        $doc1->id=$document->id;
        $doc1->name=$document->name;

        //Save Docfiles
        $dfDao = $factory->getDao($docfile);
        foreach($docfiles as $docfile){
            $dfDao->save($docfile);
        }

        return array(
            'returnCode'=>0,
            'documentId'=>$document->id,
            'docfile_id'=>$docfile->id,
            'new_file_in'=>$toPath,
        );
    }

    /**
     * Create a resource
     *
     * document1.id
     *          .relations.parent1.id
     *          .relations.parent1.class
     *          .relations.child1.id
     *          .relations.child1.class
     *
     * @param  mixed $input
     * @return ApiProblem|mixed
     */
    public function createrelationAction($returnJson=true)
    {
        rbinit_rbplmuser();

        $input = $_REQUEST;
        $documents = $input['documents'];

        for($i=2,$index='document1'; isset($documents[$index]); $index='document'.$i++)
        {
            $doc1 = json_decode(json_encode($documents[$index]), false);

            $id = $doc1->id;

            if(isset($doc1->relations)){
                for($j=1,$index2='parent1'; isset($doc1->relations->$index2); $index2='parent'.$j++)
                {
                    $parent = $doc1->relations->$index2;
                    $parentId = $parent->id;
                    $return[] = $this->_createLink($parentId, $id);
                }

                for($j=1,$index2='child1'; isset($doc1->relations->$index2); $index2='child'.$j++)
                {
                    $child = $doc1->relations->$index2;
                    $childId = $child->id;
                    $return[] = $this->_createLink($id, $childId);
                }
            }
        }

        if($returnJson){
            echo json_encode($return);
        }
        else{
            return $return;
        }
    }

    private function _createLink($parentId, $childId)
    {
        try{
            $Link = Doclink::init($parentId, $childId);
            DaoFactory::get()->getDao($Link)->save($Link);
            $return=array(
                'linkid'=>$Link->id,
                'childid'=>$childId,
                'parentid'=>$parentId
            );
        }
        catch(\Exception $e){
            $return=array(
                'error'=>array(
                    'function'=>__FUNCTION__,
                    'childid'=>$childId,
                    'parentid'=>$parentId,
                    'code'=>self::ERROR_SAVE_RELATION,
                    'message'=>'Relation is probably existing',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                )
            );
        }
        return $return;
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            $document = new DocumentVersion();
            $docDao = DaoFactory::get()->getDao($document);
            $docDao->loadFromId($document, $id);
        }
        catch (\Exception $e){
        }

        return array(
            'test'=>'ok',
            'test2'=>'ok2'
        );
    }
}
