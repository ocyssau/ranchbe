<?php
namespace Controller\Document;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/freeze.php';
require_once 'class/doccomment.php';
require_once 'class/common/metadata.php';
require_once 'class/common/container.php'; //Class to manage the container
require_once 'class/common/doclink.php';
require_once 'class/common/attachment.php';
require_once 'class/Document/Archiver.php';
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/DocBasket.php';

require_once 'GUI/GUI.php';

use Rbs\Sys\Session;

use Rbplm\Ged\Document\Version as document;
use container;

class Workflow extends \Controller\Controller
{
	public $pageId = 'document_workflow'; //(string)
	public $defaultSuccessForward = 'document/workflow/index';
	public $defaultFailedForward = 'document/workflow/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		rbinit_rbservice();

		//init Worflow service as in Module Application service factory
		$this->workflow = new \Ranchbe\Service\Workflow();
		$this->workflow->connect();
		\Ranchbe\Service\Workflow\Code::$processPath = realpath('./data/Process');
		set_include_path(get_include_path() . ':' . \Ranchbe\Service\Workflow\Code::$processPath);

		$containerId = null;
		$space = null;
		$pageId = $_REQUEST['page_id'];

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $space = $_REQUEST['space'] : null;
		isset($_REQUEST['document_id']) ? $this->documentIds = $_REQUEST['document_id'] : $this->documentIds = array();
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : null;

		if ($cancel){
			return $this->redirect('document/manager/display');
		}

		$this->session = new Session('changeStateCaddie');
		$context = Session::get();

		if(!$containerId){
			$containerId = $context->containerId;
			$space = $context->spaceName;
		}
		if(!$space){
			$space = $context->spaceName;
		}

		if($space){
			$this->spaceName = $space;
		}

		//to retrieve previous selection -- nice.
		if (!$this->documentIds && $this->session->caddie) {
			foreach($this->session->caddie as $elemt){
				$this->documentIds[] = $elemt['doc'];
			}
			$this->spaceName = $this->session->spaceName; //get space from session, else melting document is possible
		}
		else {
			$this->session->step = 'step1';
			$this->session->spaceName = $this->spaceName;
		}

		if (! is_array ( $this->documentIds ) && $this->documentIds) {
			$this->documentIds = array (( int ) $this->documentIds );
		}

		$this->checkFlood = \check_flood($pageId);
		//form dont resend ticket, so conserve it in session for use it in next steps
		$this->session->ticket = $this->ticket;

		//Assign name to particular fields
		$this->view->assign( 'action' , $_REQUEST['action']);
		$this->view->assign('CONTAINER_TYPE', $space);
		$this->view->assign('CONTAINER_NUMBER' , $space.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $space.'_description');
		$this->view->assign('CONTAINER_STATE' , $space.'_state');
		$this->view->assign('CONTAINER_ID' , $space.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $space);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 *
	 */
	public function indexAction()
	{
		isset($_REQUEST['step']) ? $step = $_REQUEST['step'] : null;

		if (!$this->documentIds) {
			$msg = tra ( 'You must select at least one item' );
			$this->errorStack->push(ERROR, 'Fatal', array(), $msg);
			return $this->redirect('document/manager/display');
		}

		if (empty ( $step )) {
			$step = $this->session->step;
		}

		if ($step == 'step1') {
			return $this->selectactivityAction();
		}
		else if ($step === 'step2') {
			return $this->activitytemplateAction();
		}
		else if ($step === 'step3') {
			$this->runAction();
		}
		else if ($step === 'step4') {
			$this->commentAction();
			return $this->_successForward ();
		}
		else{
			$this->_step1_selectActivity();
		}

		$this->errorStack->checkErrors ();
		return;
	} //End of method


	/**
	 * Step 1
	 */
	public function selectactivityAction()
	{
		$this->session->unsetAll();
		$this->session->caddie = array();

		$workflow = $this->workflow;

		//Construct the form with QuickForm lib
		$formCollection = new \Form\Collection ( 'form', 'post', $this->actionUrl('activitytemplate') );

		//Set hidden field
		$formCollection->addElement ( 'hidden', 'step', 'step2', array ('id' => 'step' ) );
		$formCollection->addElement ( 'hidden', 'space', $this->spaceName );
		$formCollection->addElement ( 'hidden', 'container_id', $this->container_id );
		$formCollection->addElement ( 'hidden', 'ticket', $this->ticket );

		$spaceName = $this->spaceName;
		$documentDao = new \Rbs\Ged\Document\VersionDao(null, $spaceName);
		$containerDao = new \Rbs\Org\WorkitemDao(null, $spaceName);
		$instanceDao = new \Rbs\Wf\InstanceDao();
		$processDao = new \Rbs\Wf\ProcessDao();

		//Create element a recap line in the select manager for each document
		$documentCaddie = array();
		$validInstance = null;
		foreach ( $this->documentIds as $documentId )
		{
			$instance = null;
			$defaultProcess = null;

			$document = new \Rbplm\Ged\Document\Version();
			$documentDao->loadFromId($document, $documentId);

			$container = new \Rbplm\Org\Workitem();
			$containerDao->loadFromId($container, $document->parentId);

			$document->setParent($container);

			//get process associated to document
			$processDefinition = $processDao->getDefaultProcessFromDocumentId($documentId)->current();
			$defaultProcess = new \Workflow\Model\Wf\Process();
			$processDao->hydrate($defaultProcess, $processDefinition);

			//get instance associated to document
			$instanceDefinition = $instanceDao->getInstanceFromDocumentId($documentId)->current();
			if($instanceDefinition){
				$instance = new \Workflow\Model\Wf\Instance();
				$instanceDao->hydrate($instance, $instanceDefinition);
				$instanceId = $instance->getId();
			}
			else{
				$instance = null;
				$instanceId = null;
			}

			$formFactory = new \Form\Factory\DocumentWorkflow($workflow);
			$formFactory->setDocument($document);
			$formFactory->setProcess($defaultProcess);
			$formFactory->setInstance($instance);

			if ( $formCollection->add( $formFactory->buildForm($documentId) ) ){ //generate html code for one line
				if ( !$formFactory->getErrors() ) {
					$documentCaddie[] = array('doc'=>$documentId, 'proc'=>$defaultProcess->getId(), 'inst'=>$instanceId); //add this document in caddie
					$validProcess = $defaultProcess;
					$validInstance = $instance;
				}
			}
		}

		$this->session->caddie = $documentCaddie;

		if ( count($this->session->caddie) > 0 ) { //create a activity select only if there is at least one valid document
			$selectSet = array ();
			$documentId = $this->documentIds[0];
			$processId = $validProcess->getId();

			//A instance of process is runnings
			if($validInstance){
				$activities = $instanceDao->getNextActivitiesFromDocumentId($documentId);
			}
			//...else an instance must be started
			else{
				$activities = $processDao->getActivity($processId, 'start');
			}

			//Generate select activity
			if ( $activities ) {
				foreach ( $activities as $activity ) {
					$selectSet[$activity ['activityId']] = $activity ['name'];
				}
			}

			//Generate select standalone activity
			$standalones = $processDao->getActivity($processId, 'standalone');
			if ( $standalones ) {
				foreach ( $standalones as $activity ) {
					$selectSet[$activity ['activityId']] = $activity ['name'];
				}
			}

			$select = $formCollection->addElement('select', 'activityId', tra('Next activity'), $selectSet);
		}

		//Get paths for graph
		$graph = GALAXIA_PROCESSES . '/' . $validProcess->getNormalizedName() . '/graph/' . $validProcess->getNormalizedName() . '.png';
		$graphUrl = ROOT_URL . '/getFile.php?file=' . $graph;
		$this->view->assign ('graphUrl', $graphUrl);
		$this->session->step = 'step2';
		$formCollection->addElement ( 'submit', 'validate', tra ('Run'), array ('onClick'=>'getElementById(\'redisplay\').value=0;getElementById(\'step\').value=\'step2\';' ) );

		return $this->_display($formCollection);
	} //End of method


	/**
	 * Step2: display activity template if necessary
	 */
	public function activitytemplateAction()
	{
		isset($_REQUEST['activityId']) ? $activityId = $_REQUEST['activityId'] : $activityId = null;

		$caddie = $this->session->caddie;
		$workflow = $this->workflow;

		$activityDao = new \Rbs\Wf\ActivityDao();
		$activity = $activityDao->loadFromId(new \Workflow\Model\Wf\Activity(), $activityId);
		$activity->dao = $activityDao;

		if($activity->isInteractive() == false){
			return $this->runAction($activity);
		}

		//Interactive activity = Display the view
		//var_dump($activity->isInteractive(), $activity->getId());die;
		return $this->runAction($activity);

		$processDao = new \Rbs\Wf\ProcessDao();
		$process = $processDao->loadFromId(new \Workflow\Model\Wf\Process(), $activity->getProcess(true));

		\Workflow\Service\Code::$processPath = GALAXIA_PROCESSES;
		$code = new \Workflow\Service\Activity\Code($process, $activity);

		$template = $code->getTemplateFile();
		$this->view->assign('mid',$template);
		$this->view->display($this->layout);
		$this->session->step = 'step3';
		return;
	} //End of method


	/**
	 * Step3: run activity on each selected document
	 */
	public function runAction($activity=null)
	{
		isset($_REQUEST['activityId']) ? $activityId = $_REQUEST['activityId'] : $activityId = null;

		$caddie = $this->session->caddie;
		$workflow = $this->workflow;

		$spaceName = $this->spaceName;
		$documentDao = new \Rbs\Ged\Document\VersionDao(null, $spaceName);

		foreach($caddie as $elemt){
			$documentId = $elemt['doc'];
			$processId = $elemt['proc'];
			$intanceId = $elemt['inst'];

			if($intanceId == null){ //process must be started
				try{
					//start process
					$startActInst = $workflow->startProcess($processId)->startInstance;

					//translate to next activities
					$workflow->translateActivity($startActInst->getId());

					//Now process instance is set in workflow :
					$instanceId = $workflow->instance->getId();

					//create link between process instance and workunit object
					$documentDao->updateFromArray($documentId, array('instanceId'=>$instanceId));
				}
				catch(\Exception $e){
					$r = array ('element' => $document->getName() );
					$msg = 'change state failed for document %element%';
					$this->errorStack->push(ERROR, 'Error', $r, $msg);
				}
			}
			else{ /* run the activity */
				$activityInstDao = new \Rbs\Wf\Instance\ActivityDao();
				$actInstance = $activityInstDao->loadFromActivityAndInstance(new \Workflow\Model\Wf\Instance\Activity(), $activityId, $intanceId);

				$act = $workflow->runActivity($actInstance)->lastActivity;
			}
		}

		if ($activity->isComment()) {
			return $this->_inputComment ( $docflow, $activityId ); //display comment page
		}
		else {
			$this->session->step = 'step1';
			$this->redirect($this->ifSuccessForward);
		}
	}


	/**
	 * Step4: validate comment and update activity
	 * comments are same for all activity of the current selection
	 */
	protected function commentAction()
	{
		isset($_REQUEST['__title']) ? $title = trim($_REQUEST['__title']) : $title = null;
		isset($_REQUEST['__comment']) ? $comment = trim($_REQUEST['__comment']) : $comment = null;
		isset($_REQUEST['activityId']) ? $activityId = trim($_REQUEST['activityId']) : $activityId = null;
		$activityId = $this->session->activityId;

		if (empty ( $title ) && empty ( $comment )){
			return $this->redirect($this->ifSuccessForward);
		}
		foreach ( $this->documentIds as $documentId ) { //loop instances from the previous execution
			$workflow = new Rb_Workflow_Docflow ( Rb_Document::get ( $this->spaceName, $documentId ) );
			$workflow->initInstance (); //init the instance from the document properties
			$workflow->initActivity ( $activityId ); //init current activity
			$workflow->setComment ( $title, $comment );
		}
		return $this->_successForward ();
	}


	/**
	 * Process comments
	 * @param Rb_Workflow_Workflow $docflow
	 * @param unknown_type $activity_id
	 */
	protected function _inputComment(Rb_Workflow_Workflow &$docflow, $activity_id)
	{

		//$this->_helper->viewRenderer->setNoRender(true);
		if (empty ( $activity_id )) {
			$msg = 'activity_id is empty';
			$this->errorStack->push ( Rb_Error::INFO, array (), $msg );
			return false;
		}
		$this->session->step = 'step4';

		//-- Construct the form with QuickForm lib
		$form = new RbView_Pear_Html_QuickForm ( 'inputComment', 'post', $this->actionUrl ( 'changestate' ) );
		$this->_initFormForward ( $form ); //init the hidden param to set the forward parameter

		$form->addElement ( 'header', 'title1', tra ( 'Activity completed' ) );
		$form->addElement ( 'text', '__title', tra ( 'Subject' ) );
		$form->addElement ( 'textarea', '__comment', '', array ('rows' => 5, 'cols' => 60 ) );
		$form->addElement ( 'submit', 'validate', tra ( 'Validate' ) );
		$form->addElement ( 'submit', 'cancel', tra ( 'No comment' ) );
		$form->applyFilter ( '__ALL__', 'trim' );

		return $this->_display ( $form );
	}


	/**
	 *
	 * @param unknown_type $form
	 */
	protected function _display($form)
	{
		$this->view->assign('changestateForm', $form->toHtml());

		$template = 'document/workflow/index.tpl';
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}


}
