<?php
namespace Controller\Document;

use Controller\ControllerException;

use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbs\Ged\Document\Listener;
use Rbplm\Signal;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbplm\Sys\Error;
use Rbplm\Ged\AccessCodeException;

use Rbs\Extended;
use Rbplm\Ged\AccessCode;

class Manager extends \Controller\Controller
{
	protected $areaId = 10;
	public $pageId = 'document_manager'; //(string)

	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	const SIGNAL_POST_RESETDOCTYPE = 'resetdoctype.post';
	const SIGNAL_PRE_RESETDOCTYPE = 'resetdoctype.pre';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : $containerId=null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : $spaceName=null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;

		$this->checkFlood = \check_flood($pageId);

		//Assign name to particular fields
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('spacename' , $spaceName);
		$this->view->assign('sameurl_elements', array('space', 'containerid'));

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 */
	public function editmultiAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds = array();
		(isset($_REQUEST['documentid'])) ? $documentIds[] = $_REQUEST['documentid'] : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if($cancel || !$documentIds ){
			$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'containerid'=>$this->containerId
			));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$document = new DocumentVersion();

		//-- Construct the form with QuickForm lib
		$form = new \Form\Document\EditForm($factory, $this->view);
		$form->setView($this->view);
		$form->setAttribute('action',$this->getRoute());

		foreach($documentIds as $documentId){
			$document = new DocumentVersion();
			$dao->loadFromId($document, $documentId);
			$form->add($document);
			/*List to recap selection in view*/
			$recapList[] = $document->getArrayCopy();
		}

		//Reset all rules
		$form->_rules = array();

		// Try to validate the form
		if($validate){
			if ($form->validate()){
				$form->freeze(); //and freeze it
				foreach($form->documents as $document){
					/* Set listener */
					$listener = new Listener($document, $factory);
					$listener->init($document::SIGNAL_POST_EDIT);

					$form->bind($document);
					$document->dao->save($document);
					Signal::trigger($document::SIGNAL_POST_EDIT, $document);
				}
			}
		}
		else{
			$form->prepareRenderer('form');
			// Display the template
			$this->view->pageTitle = "Edit Multi-Documents";
			$this->view->documents = $recapList;
			$this->view->mid = 'document/editform.tpl';
			$this->view->display($this->layout);
		}
	}

	/**
	 */
	function editAction()
	{
		(isset($_REQUEST['id'])) ? $documentId = $_REQUEST['id'] : $documentId=null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if(!$documentId){
			$this->errorStack()->error('Empty selection');
		}
		if($cancel||!$documentId){
			$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'containerid'=>$this->containerId
			));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$containerId = $this->containerId;

		/* Load extended properties */
		$loader = new Extended\Loader($factory);
		$loader->loadFromContainerId($containerId, $dao);

		$document = new DocumentVersion();
		$dao->loadFromId($document, $documentId);

		/* Set listener */
		$listener = new Listener($document, $factory);
		$listener->init($document::SIGNAL_POST_EDIT);

		$aCode = $document->checkAccess();
		if( ($aCode > AccessCode::FREE) || ($aCode < AccessCode::FREE || $aCode === AccessCode::CHECKOUT) ){
			throw new ControllerException('This document is locked with code %acode%, edit is not permit', array('acode'=>$aCode), $this);
		}

		/* Construct the form */
		$form = new \Form\Document\EditForm($factory, $this->view, $containerId);
		$form->setMode('edit');
		$form->setView($this->view);
		$form->setAttribute('action',$this->getRoute());

		//SET EXTENDED TO FORM
		$form->setExtended($dao->extended);
		$form->bind($document);

		/* Try to validate the form */
		if($validate){
			$form->setData($_POST);
			if ($form->validate()){
				try{
					$form->bind($document);
					$dao->save($document);
					Signal::trigger($document::SIGNAL_POST_EDIT, $document);

					$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
				}
				catch(\Exception $e){
					$this->errorStack()->error($e->getMessage());
				}
			}
		}
		else{
			$form->prepareRenderer('form');
			$this->view->assign( 'pageTitle' , 'Edit Document '.$document->getName());

			/* Display the template */
			$this->view->mid = 'document/editform.tpl';
			$this->view->display($this->layout);
		}
	}

	/**
	 */
	function createAction()
	{
		(isset($_REQUEST['id'])) ? $documentId = $_REQUEST['id'] : $documentId=null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if($cancel){
			$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
		}

		$this->checkRight('create', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$containerId = $this->containerId;

		/* Load extended properties */
		$loader = new Extended\Loader($factory);
		$loader->loadFromContainerId($containerId, $dao);

		$document = DocumentVersion::init();

		/* Construct the form */
		$form = new \Form\Document\EditForm($factory, $this->view, $containerId);
		$form->setView($this->view);
		$form->setAttribute('action',$this->getRoute());

		//SET EXTENDED TO FORM
		$form->setExtended($dao->extended);
		$form->setData($document->getArrayCopy());

		/* Try to validate the form */
		if($validate){
			if ($form->validate()){
				try{
					$form->freeze();
					$form->bind($document);

					$dao->save($document);
					$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
				}
				catch(\Exception $e){
					$this->errorStack()->error($e->getMessage());
				}
			}
		}
		else{
			$form->prepareRenderer('form');
			$this->view->assign( 'pageTitle' , 'Create New Document');

			/* Display the template */
			$this->view->mid = 'document/editform.tpl';
			$this->view->display($this->layout);
		}
	}

	/**
	 */
	function deleteAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : null;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : null;

		if (!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('delete', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$service = new \Rbs\Ged\Document\Service($factory);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$conn = $factory->getConnexion();

		if($validate){
			foreach ($documentIds as $documentId){
				$conn->beginTransaction();
				try{
					$document = new DocumentVersion();

					/* Set listener */
					$listener = new Listener($document, $factory);
					$listener->init($document::SIGNAL_POST_DELETE);

					$dao->loadFromId($document, $documentId);
					$service->delete($document);
					$conn->commit();
				}
				catch(\Exception $e){
					$conn->rollBack();
					throw $e;
				}
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else{ //Display validation
			$confirmForm = new \Form\Document\ConfirmationForm($factory, $this->view);
			$confirmForm->setAttribute('action',$this->getRoute());

			foreach ($documentIds as $documentId){
				$document = new DocumentVersion();
				$dao->loadFromId($document,$documentId);
				$confirmForm->addDocument($document);
			}

			$this->view->pageTitle = "Suppression Confirmation";
			$this->view->warning = "Are you sure that you want suppress this documents?";
			$this->view->help = "After deletion, documents files are placed in trash. In case of error, you can see your admin system to get back trashed files";
			$confirmForm->prepareRenderer('form');

			$this->view->assign('mid', $confirmForm->template);
			$this->view->display('layouts/ranchbe.tpl');
		}
	}

	/**
	 */
	function fixAction()
	{
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : $documentId=null;
		Session::get()->fixedDocument = $documentId;
		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 */
	function unfixAction()
	{
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : $documentId=null;
		Session::get()->fixedDocument = null;
		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	function lockdocAction($code=AccessCode::LOCKED)
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;

		if(!$documentIds){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$dfDao = $factory->getDao(DocfileVersion::$classId);

		$conn = $factory->getConnexion();

		foreach ($documentIds as $documentId) {
			$conn->beginTransaction();
			try{
				$document = new DocumentVersion();
				$dao->loadFromId($document, $documentId);

				/* Set listener */
				$listener = new Listener($document, $factory);
				$listener->init($document::SIGNAL_POST_UNLOCK);

				$dfDao->loadDocfiles($document);

				$aCode = $document->checkAccess();
				$coBy = $document->lockById;

				if( $aCode > AccessCode::FREE ){
					$this->errorStack()->error(Error::ERROR, tra("You cant lock this document"));
					continue;
				}

				$document->lock($code);
				foreach($document->getDocfiles() as $docfile){
					$docfile->lock($code);
					$dfDao->save($docfile);
				}
				$dao->save($document);

				Signal::trigger($document::SIGNAL_POST_UNLOCK, $document, 'LockWithCode:'.AccessCode::getName($code));
			}
			catch(AccessCodeException $e){
				$conn->rollBack();
				$this->errorStack()->error(Error::ERROR, $e->getMessage());
				continue;
			}
			catch(\Exception $e){
				$conn->rollBack();
				throw($e);
			}
			$conn->commit();
		}
		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	function marktosuppressdocAction()
	{
		$code = AccessCode::SUPPRESS;
		return $this->lockdocAction($code);
	}

	/**
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function unmarktosuppressdocAction()
	{
		return $this->unlockdocAction();
	}

	/**
	 *
	 */
	function unlockdocAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : null;
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;

		if(!$documentIds){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$dfDao = $factory->getDao(DocfileVersion::$classId);

		$conn = $factory->getConnexion();

		foreach ($documentIds as $documentId) {
			$conn->beginTransaction();
			try{
				$document = new DocumentVersion();
				$dao->loadFromId($document, $documentId);
				$dfDao->loadDocfiles($document);

				/* Init listener*/
				$listener = new Listener($document, $factory);
				$listener->init($document::SIGNAL_POST_UNLOCK);

				$aCode = $document->checkAccess();
				$coBy = $document->lockById;

				if($aCode<AccessCode::INWORKFLOW || $aCode>=AccessCode::HISTORY){
					$this->errorStack()->error(tra("You cant unlock this document"));
					$conn->rollBack();
					continue;
				}

				$document->unlock();
				foreach($document->getDocfiles() as $docfile){
					$docfile->unlock();
					$dfDao->save($docfile);
				}

				$dao->save($document);
				Signal::trigger($document::SIGNAL_POST_UNLOCK, $document, 'UnLock');
			}
			catch(AccessCodeException $e){
				$this->errorStack()->error(Error::ERROR, $e->getMessage());
				$conn->rollBack();
				continue;
			}
			catch(\Exception $e){
				$conn->rollBack();
				throw($e);
			}
			$conn->commit();
		}
		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	function resetdoctypeAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;
		(isset($_REQUEST['tag'])) ? $validate = $_REQUEST['tag'] : $validate=false;

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$dfDao = $factory->getDao(DocfileVersion::$classId);

		foreach($documentIds as $documentId){
			try{
				$document = new DocumentVersion();
				$dao->loadFromId($document, $documentId);
				$dfDao->loadDocfiles($document);

				/* Init listener */
				$listener = new Listener($document, $factory);
				$listener->init(self::SIGNAL_POST_RESETDOCTYPE);

				if($docfiles = $document->getDocfiles()){
					$mainData = array_shift($docfiles)->getData();
					$fileExtension = $mainData->extension;
					$fileType = $mainData->type;
				}
				else{
					$fileExtension = NULL;
					$fileType = 'nofile';
				}

				$doctype = new \Rbplm\Ged\Doctype();
				$factory->getDao($doctype::$classId)->loadFromDocument($doctype, $document, $fileExtension, $fileType);
				$document->setDoctype($doctype);
				$dao->save($document);
				Signal::trigger(self::SIGNAL_POST_RESETDOCTYPE, $document);
			}
			catch(\Exception $e){
				$this->errorStack()->error($document->getUid().': '.$e->getMessage());
				continue;
			}
		}

		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 * Upgrade indice of document
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function newversionAction()
	{
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : $documentIds=array();
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : null;
		(isset($_REQUEST['tocontainerid'])) ? $toContainerId = $_REQUEST['tocontainerid'] : $toContainerId=null;
		(isset($_REQUEST['version'])) ? $version = $_REQUEST['version'] : $version=null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel = false;

		if($cancel || !$documentIds){
			return $this->redirect($this->ifSuccessForward, array('containerid'=>$this->containerId, 'spacename'=>$this->spaceName));
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dfDao = $factory->getDao(DocfileVersion::$classId);
		$docDao = $factory->getDao(DocumentVersion::$classId);
		$docService = new \Rbs\Ged\Document\Service($factory);
		$loader = new \Rbs\Dao\Loader($factory);

		$form = new \Form\Document\NewVersionForm($factory, $this->view);
		$form->setAttribute('action',$this->actionUrl('newversion'));
		$form->setData($_POST);

		if($validate){
			if($toContainerId){
				$toContainer = $loader->loadFromId($toContainerId, Workitem::$classId);
			}
			else{
				$toContainer = null;
			}

			foreach($documentIds as $documentId){
				$document = new DocumentVersion();
				$docDao->loadFromId($document, $documentId);
				$dfDao->loadDocfiles($document);

				$listener = new Listener($document, $factory);
				$listener->init($document::SIGNAL_POST_UNLOCK, 'NewVersion: '.$newVersion);

				$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
				$document->setParent($container);
				$newDocument = $docService->newVersion($document, $toContainer, $newVersion);
			}
			return $this->redirect($this->ifSuccessForward, array('containerid'=>$this->containerId, 'spacename'=>$this->spaceName));
		}
		else{
			foreach($documentIds as $documentId){
				$document = new DocumentVersion();
				$docDao->loadFromId($document, $documentId);

				/* Check status */
				$aCode = $document->checkAccess();
				if($aCode<>AccessCode::FREE && $aCode<AccessCode::LOCKED){
					$this->errorStack()->warning("Document $document->getUid() is not in right state");
				}
				else{
					$form->addDocument($document);
				}
			}
			$form->setData(array(
				'containerid'=>$this->containerId,
				'tocontainerid'=>$toContainerId,
				'documentids'=>$documentIds,
				'version'=>''
			));
			$form->prepareRenderer();
			$this->view->pageTitle = 'New Version of Documents';
			$this->view->assign('mid', $form->template);
			$this->view->display($this->layout);
		}
	}

	/**
	 * Download a listing of the container
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function downloadlistingAction()
	{
		isset($_REQUEST['format']) ? $format = $_REQUEST['format'] : $format = null;
		isset($_REQUEST['phpscript']) ? $xlsRenderClassName = $_REQUEST['phpscript'] : $xlsRender = null;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = null;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if($cancel){
			$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'containerid'=>$this->containerId));
		}

		$this->checkRight('read', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* Get list of scripts */
		$path = Ranchbe::get()->getConfig('path.scripts.document.xlsrenderer');
		$form = new \Form\Application\SelectScriptForm($this->view, $path, 'XLS Renderer', $factory);
		$form->setAttribute('action', '#');

		$form->setDefaults(array(
			'format' => 'xls',
			'phpscript' => 'default.php',
		));

		/* Try to validate the form */
		if ($form->validate() && $validate) {
			if($format === 'csv'){
				/* get all documents */
				$list = $this->getList($filter, $factory);
				$this->view->assign('list', $list->toArray());
				header("Content-type: application/csv ");
				header("Content-Disposition: attachment; filename=".$this->container->getName().'_content.csv');
				$this->view->display("./csv_templates/container_listing_download.tpl");
				die;
			}
			elseif($format === 'xls'){
				$dao = $factory->getDao(DocumentVersion::$classId);
				$filter->sort($dao->toSys('uid'), 'ASC');
				$list = $factory->getList(DocumentVersion::$classId);
				$filter->andfind( $this->containerId, $dao->toSys('parentId'), Op::OP_EQUAL );
				$list->setOption('asapp',true);
				$list->setOption('dao',$dao);

				$list->load($filter);

				foreach($list as $item){
					$asApp[] = $item;
				}

				$xlsRenderClassName = '\XlsRenderer\Document\\'.$xlsRenderClassName;
				$xlsRender = new $xlsRenderClassName();
				$xlsRender->render($asApp, $this->containerId);
			}
		}
		else{
			$form->prepareRenderer('form');
			$this->view->assign( 'pageTitle' , 'Select the XLS Renderer' );

			/* Display the template */
			$this->view->mid = $form->template;
			$this->view->display($this->layout);
		}
	}

} //End of class
