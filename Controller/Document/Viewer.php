<?php
namespace Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;

class Viewer extends \Controller\Controller
{
	public $pageId = 'documentdata_manage'; //(string)
	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId = null;
		$spaceName = null;

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=$this->pageId;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;

		$this->checkFlood = \check_flood($pageId);

		//Assign name to particular fields
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('spacename' , $spaceName);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}

	/**
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function assocvisuAction()
	{
		(isset($_REQUEST['file'])) ? $fileName = $_REQUEST['file'] : $fileName=null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : $documentId=null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		$this->checkRight('edit', $this->areaId, $this->containerId, $this->ifFailedForward);

		if($cancel || !$documentId){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($this->spaceName);
		$service = new \Rbs\Ged\Document\Service($factory);
		$wildspace = Wildspace::get(CurrentUser::get());
		$dao = $factory->getDao(DocumentVersion::$classId);

		$document = new DocumentVersion();
		$dao->loadFromId($document, $documentId);
		$uid = $document->getUid();
		$number = $document->getNumber();

		$containerId = $document->getParent(true);
		$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);
		$document->setParent($container);

		//Filter and paginations
		$filter = new \Rbplm\Sys\Filesystem\Filter();
		$this->bindSortOrder($filter, 'name');

		if(!$fileName)
		{
			//ASSOC_FILE_CHECK_NAME is set in ranchbe_setup.php
			//Limit list of files to files with the document name in the name
			if (ASSOC_FILE_CHECK_NAME == true){
				$filter->andFind($number, 'name', Op::CONTAINS);
			}

			$filter->andFind('(.*[.jpg|.jpeg|.png|.gif])$', 'name', Op::REGEX);
			$form = new \Form\Wildspace\SelectFileForm($wildspace, $filter, $this->view);
			$form->setDefaults(array('documentid'=>$documentId));

			// Display the template
			$form->prepareRenderer('form');
			$this->view->pageTitle = "Select the Visu to associate to document $uid";
			$this->view->help = "Visu file must be in Wildspace, have the document name in his name and must be of type .jpg, .jpeg, .gif or .png";
			$this->view->assign('documentUid', $uid);
			$this->view->mid = $form->template;
			$this->view->display($this->layout);
		}
		else{
			$file = $wildspace->getPath().'/'.$fileName;
			if($fileName && is_file($file)){
				$service->associateFile($document, $file, true, true);
			}
			return $this->redirect($this->ifSuccessForward);
		}
	}

	/**
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function viewfileAction()
	{
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : null;

		$this->checkRight('read', $this->areaId, $this->containerId, $this->ifFailedForward);

		$document = new DocumentVersion();
		$factory = DaoFactory::get($this->spaceName);
		$factory->getDao($document)->loadFromId($document, $documentId);

		$viewer = new \Rbs\Ged\Document\Viewer();
		$viewer->initFromDocument($document);
		return $viewer->push();
	}

}

