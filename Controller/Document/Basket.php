<?php
namespace Controller\Document;


use Rbs\Ged\Document\Basket as DocBasket;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\CurrentUser;
use Rbplm\Dao\NotExistingException;

class Basket extends \Controller\Controller
{
	public $pageId = 'document_basket'; //(string)
	public $defaultSuccessForward = 'document/basket/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : $containerId=null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;

		$this->spaceName = $spaceName;
		$this->checkFlood = \check_flood($_REQUEST['page_id']);
		$this->view->assign('spacename' , $this->spaceName);
		$this->checkRight('read', $this->areaId, 0);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function addAction()
	{
		$this->addService();
		return $this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName));
	}

	/**
	 *
	 */
	public function addService()
	{
		isset($_REQUEST['checked'] ) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		isset($_REQUEST['documentid'] ) ? $documentIds[] = $_REQUEST['documentid'] : null;

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$spaceId = $factory->getId();

		$docBasket = new DocBasket(CurrentUser::get(), $factory);
		foreach($documentIds as $documentId){
			try{
				$docBasket->add($documentId, $spaceId);
			}
			catch(\PDOException $e){
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		if($this->_isService){
			$this->serviceReturn($documentIds);
		}
	}

	/**
	 *
	 */
	public function removeAction()
	{
		$this->removeService();
		return $this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName));
	}

	/**
	 *
	 */
	public function removeService()
	{
		isset($_REQUEST['checked'] ) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		isset($_REQUEST['documentid'] ) ? $documentIds[] = $_REQUEST['documentid'] : null;

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$spaceId = $factory->getId();

		$docBasket = new DocBasket( CurrentUser::get(),$factory );
		foreach( $documentIds as $documentId ){
			$docBasket->remove($documentId, $spaceId);
		}
		if($this->_isService){
			$this->serviceReturn($documentIds);
		}
	}

	/**
	 *
	 */
	public function cleanAction()
	{
		$this->cleanService();
		return $this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName));
	}

	/**
	 *
	 */
	public function cleanService()
	{
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$spaceId = $factory->getId();

		$docBasket = new DocBasket( CurrentUser::get(),$factory );
		$docBasket->clean();
		if($this->_isService){
			$this->serviceReturn(array());
		}
	}

	/**
	 */
	public function indexAction()
	{
		$defaultFwd = $this->defaultSuccessForward.'?spacename='.$this->spaceName;
		$this->basecamp()->save($defaultFwd, $this->ifFailedForward);

		$factory = DaoFactory::get($this->spaceName);
		try{
			$docBasket = new DocBasket(CurrentUser::get(), $factory);
			$list = $docBasket->get();
			$template = 'document/basket/index.tpl';
			$this->view->assign('list', $list);
			$this->view->assign('pageTitle', tra('My Documents Basket'));
			$this->view->assign('mid', $template);
			$this->view->display($this->layout);
		}
		catch(NotExistingException $e){
			$this->errorStack()->error('Basket is empty');
			return $this->redirect($this->ifFailedForward, array('spacename'=>$this->spaceName));
		}
	}

} //End of class
