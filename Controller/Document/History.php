<?php
namespace Controller\Document;

use Rbplm\Dao\Filter\Op;
use Rbs\Ged\Document\History as HistoryModel;
use Rbs\Space\Factory as DaoFactory;

class History extends \Controller\Controller
{
	public $pageId = 'document_history';
	public $defaultSuccessForward = 'document/history/display';
	public $defaultFailedForward = 'document/history/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : $spaceName = null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : $documentId = null;

		$this->documentId = $documentId;
		$this->spaceName = $spaceName;

		$this->view->assign('documentid' , $documentId);
		$this->view->assign('spacename' , $spaceName);

		\View\Helper\MainTabBar::get($this->view)->getTab('home')->activate();
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
	}

	/**
	 *
	 */
	public function displayAction()
	{
		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(HistoryModel::$classId);

		$filterForm = new \Form\Document\History\FilterForm( $this->view, $factory, $this->pageId );
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, $dao->toSys('action_id'));
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		$bind = null;
		$documentId = $this->documentId;

		if(!$list){
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = "`$asSys` AS `$asApp`";
			}
			$filter->select($select);

			if($documentId){
				$filter->andfind(':documentId', $dao->toSys('data_id'), Op::EQUAL);
				$bind = array(':documentId'=>$documentId);
			}

			$list = $factory->getList(HistoryModel::$classId);
			$count = $list->countAll($filter, $bind);
			$list->load($filter, $bind);

			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('pageTitle', 'Document History');
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'document/history.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function deleteAction()
	{
		isset($_REQUEST['checked']) ? $actionIds=$_REQUEST['checked'] : $actionIds=array();
		isset($_REQUEST['action_id']) ? $actionIds=array($_REQUEST['action_id']) : null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;

		if($cancel){
			$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'documentid'=>$this->documentId));
		}

		$spaceName = $this->spaceName;
		$this->checkRight('edit', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(HistoryModel::$classId);

		foreach ($actionIds as $actionId){
			try{
				$dao->deleteFromId($actionId);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}

		$this->redirect($this->ifSuccessForward, array('spacename'=>$this->spaceName, 'documentid'=>$this->documentId));
	}

} //End of class

