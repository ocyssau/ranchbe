<?php
namespace Controller\Document;

use Rbplm\Dao\Filter\Op;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/common/container.php'; //Class to manage the container
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/common/import.php'; //Class to manage the importations

class Importhistory extends \Controller\Controller
{
	public $pageId = 'docfile_import'; //(string)
	public $defaultSuccessForward = array ('module' => 'document', 'controller' => 'import', 'action' => 'index' );
	public $defaultFailedForward = array ('module' => 'document', 'controller' => 'import', 'action' => 'index' );

	/**
	 */
	protected function _checkErrors()
	{
		if ( $this->errorStack->hasErrors() ){
			return $this->errorStack->getErrors(true);
		}
	} //End of function

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId=null;
		$space=null;

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : null;

		if($spaceName){
			$this->container = \container::_factory($spaceName , $containerId);
			$this->space = $this->container->space;
			$this->spaceName = $spaceName;
			$this->areaId = $this->container->AREA_ID;
			$this->import = new \import($spaceName, $this->container);
		}

		$this->checkFlood = \check_flood($_REQUEST['page_id']);

		//Assign name to particular fields
		$this->view->assign( 'action' , $_REQUEST['action']);
		$this->view->assign('CONTAINER_TYPE', $space);
		$this->view->assign('CONTAINER_NUMBER' , $space.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $space.'_description');
		$this->view->assign('CONTAINER_STATE' , $space.'_state');
		$this->view->assign('CONTAINER_ID' , $space.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $space);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');


		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		//rbinit_tab();
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function suppressAction()
	{
		if ( empty($_REQUEST["checked"]) ) {
			print 'none selected package';
			return;
		}

		$this->checkRight('edit', $this->areaId, 0);

		foreach ($_REQUEST['checked'] as $order ){
			$Import->SuppressHistory($order);
		}
	}

	/**
	 *
	 */
	public function cleanAction()
	{
		$this->checkRight('edit', $this->areaId, 0);
		$Import->CleanHistory();
	}

	/**
	 *
	 */
	public function viewlogfileAction()
	{
		$logFile = $Import->GetLogFile($_REQUEST['import_order']);
		$fsdata = new \fsdata($logFile);
		if($fsdata->DownloadFile()){
			die;
		}
		else{
			$this->errorStack->push(ERROR, 'Warning', array('element'=>$logFile), 'The logfile %element% don\'t exist');
		}
	}

	/**
	 *
	 */
	public function viewerrorfileAction()
	{
		$logFile = $Import->GetErrorFile($_REQUEST['import_order']);
		$fsdata = new \fsdata($logFile);
		if($fsdata->DownloadFile()){
			die;
		}
		else{
			$this->errorStack->push(ERROR, 'Warning', array('element'=>$logFile), 'The logfile %element% don\'t exist');
		}
	}


	/**
	 *
	 */
	public function displayAction()
	{
		//Process the code for pagination and display management
		$filterForm = new \Form\Filter\StandardSimple($this->view, $this->pageId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'import_order');
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		//Display the users list of system
		$params = array (
			'orders' => array($filter->getSort() => $filter->getOrder()),
			'limit' => $filter->getLimit(),
			'offset' => $filter->getOffset(),
			'select'=>'all',
		);

		$spaceName = $this->spaceName;

		//get infos on all package
		if(isset($_REQUEST['fcontainer'])){
			$params['exact_find'][$spaceName . '_id'] = $_REQUEST['fcontainer'];
			$smarty->assign('fcontainer', $_REQUEST['fcontainer']);
		}

		$list = $this->import->GetImportHistory($params);
		$this->view->assign_by_ref('list', $list);

		$containers = $this->container->GetAll(array(
				'sort_field' => $spaceName . '_number',
				'sort_order' => 'ASC',
				'select' => array($spaceName . '_number', $spaceName . '_id')
		));
		foreach ($containers as $value ){
			$t[$value[$spaceName . '_id']] = $value[$spaceName . '_number'];
		}

		$allField[0] = array (
				'package_file_name' => 'package_file_name',
				'description' => 'description',
				'import_order'=>'import_order'
		);
		$this->view->assign('all_field', $allField[0]);
		$this->view->assign('containers', $t);

		// Display the template
		($template == null) ?  $template = 'document/import/index.tpl' : null;

		$this->view->assign('randWindowName', 'file import');
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');


		$this->view->assign('mid', 'docfile/import/history.tpl');
		$this->view->display($this->layout);
	}

}
