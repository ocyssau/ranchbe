<?php
namespace Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Org\Workitem;
use Rbplm\Ged\AccessCode;
use Rbplm\Sys\Error;

class Archiver extends \Controller\Controller
{
	public $pageId = 'document_archiver'; //(string)
	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName = null;

		$this->spaceName = $spaceName;
		$this->checkFlood = \check_flood($pageId);
		$this->view->assign('spacename' , $spaceName);

		\View\Helper\MainTabBar::get($this->view)->getTab('docuement')->activate();

		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}

	/**
	 * Archive
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function archiveAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds = array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if(!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get($this->spaceName);
		$loader = $factory->getLoader();
		$dao = $factory->getDao(DocumentVersion::$classId);
		$dfDao = $factory->getDao(DocfileVersion::$classId);
		$archiver = new \Rbs\Ged\Archiver($factory);

		if($validate){
			foreach ($documentIds as $documentId){
				try{
					//LOAD DOCUMENT
					$document = new DocumentVersion();
					$dao->loadFromId($document, $documentId);

					if($document->checkAccess() < $archiver::$accessCode){
						//IF NOT ARCHIVE, LOAD PARENT AND DOCFILES
						$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
						$document->setParent($container);
						$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode').'<'.$archiver::$accessCode);

						//RUN ARCHIVER
						$archiver->archive($document, $cleaniterations=true);
					}
				}
				catch(\Exception $e){
					$this->errorStack()->error(Error::ERROR, $e->getMessage());
					throw $e;
				}
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else{ //Display validation
			$confirmForm = new \Form\Document\ConfirmationForm($factory, $this->view);
			$confirmForm->setAttribute('action',$this->getRoute());

			foreach ($documentIds as $documentId){
				$document = new DocumentVersion();
				$dao->loadFromId($document,$documentId);
				$confirmForm->addDocument($document);
			}

			$confirmForm->setData($_GET);

			$this->view->pageTitle = "Archive Confirmation";
			$this->view->warning = "Are you sure that you want archive this documents?";
			$this->view->help = "";
			$confirmForm->prepareRenderer('form');
			$this->view->assign('mid', $confirmForm->template);
			$this->view->display('layouts/ranchbe.tpl');
		}
	}
} //End of class
