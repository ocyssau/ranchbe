<?php
namespace Controller\Document;

use Rbs\Sys\Session;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;

use Rbs\Extended;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;

class Smartstore extends \Controller\Controller
{
	public $pageId = 'document_smartstore'; //(string)
	protected $areaId = 10;
	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId = null;
		$spaceName = null;

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['action']) ? $action = $_REQUEST['action'] : null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->user = CurrentUser::get();
		$this->wildspace = new Wildspace($this->user);
		$this->factory = DaoFactory::get($spaceName);
		$this->containerId = $containerId;
		$this->spaceName = $spaceName;

		$this->container = $this->factory->getModel(Workitem::$classId);
		$this->container->dao = $this->factory->getDao(Workitem::$classId);
		$this->container->dao->loadFromId($this->container, $containerId);

		$this->areaId = null; //@todo TBD

		//Assign name to particular fields
		$this->view->assign( 'action' , $action);
		$this->view->assign('spacename', $spaceName);
		$this->view->assign('rbgateServerUrl', \Ranchbe::get()->getConfig('rbgate.server.url'));

		$this->checkRight('edit', $this->areaId, 0);

		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 */
	public function storeAction()
	{
		$files = array();
		isset($_REQUEST['checked']) ? $files = $_REQUEST['checked'] : null;
		isset($_REQUEST['file']) ? $files = $_REQUEST['file'] : null;
		isset($_REQUEST['loop']) ? $loop = $_REQUEST['loop'] : null;
		isset($_REQUEST['fields']) ? $fields = $_REQUEST['fields'] : null;
		isset($_POST['actions']) ? $actions = $_POST['actions'] : null;
		isset($_POST['cancel']) ? $cancel = true : $cancel = false;
		isset($_POST['save']) ? $save = true : $save = false;
		isset($_POST['pdmdata']) ? $inputPdmData = new \Pdm\Input\PdmData($_POST['pdmdata'], 'json') : $inputPdmData = false;

		if($cancel){
			$this->redirect($this->ifSuccessForward);
		}
		$this->checkRight('create', $this->areaId, $this->containerId, $this->ifSuccessForward);

		if ( count($files) == 0 ){
			$this->errorStack()->error('You must select at least one file');
			$this->redirect($this->ifSuccessForward);
		}

		$maxBatchSize = \Ranchbe::get()->getConfig('document.smartstore.maxsize');
		if ( count($files) > $maxBatchSize ){
			$chunkSelect = array_chunk($files, $maxBatchSize, true);
			$files = $chunkSelect[0];
			unset($chunkSelect);
			$msg = 'Sorry but your selection is too large and has been trunk to %count% elements';
			$msg.= 'Change the document.smartstore.maxsize configuration directive to increase this value';
			$this->errorStack()->warning($msg, array('count'=>$maxBatchSize));
		}

		$spaceName = $this->spaceName;
		$factory = $this->factory;
		$wsPath = $this->wildspace->getPath();
		$containerId = $this->containerId;
		$formCollection = $this->_getFormCollection();
		$container = $this->container;
		$loader = new \Rbs\Dao\Loader($factory);

		if($save){
			$formCollection->removeElement('save');
			$formCollection->removeElement('keepco');
			$template = 'document/smartstore/validate.tpl';
		}
		else{
			$template = 'document/smartstore/store.tpl';
		}

		$tmplist = array(); /* init the var for foreach loop */
		$cleanlist = array(); /* init the var for foreach loop */
		$i = 0;
		$loop = count($files)-1;
		/* Loop action and get datas */
		for ($i = 0; $i <= $loop; $i++){
			$this->view->loop = $i;
			$fileName = $files[$i];
			if(!isset($fileName)) continue;

			$smartProcessor = new \Rbs\Ged\SmartStoreProcessor($loader, $container);
			$smartProcessor->fromFile($fileName, $wsPath);
			$view = clone($this->view);
			$smartStoreForm = new \Form\Document\SmartStoreForm($i, $view, $factory, new \Form\Renderer\FieldsetSmartyRenderer($view, 'document/smartstore/smartstorefieldset.tpl'));
			$smartStoreForm->defaultRenderer()->loop = $i;

			if($save){
				if($inputPdmData){
					if($inputPdmData->offsetExists($fileName)){
						$inputElmt = $inputPdmData->offsetGet($fileName);
						$inputElmt->setWorkingDir($wsPath.'/rbconverter/');
						$smartProcessor->setPdmData($inputElmt);
					}
				}

				$smartStoreForm->setData($_POST);
				$smartStoreForm->bind($smartProcessor); /* populate processor with form datas */
				$smartStoreForm->setProcessor($smartProcessor);

				try{
					if(!$smartStoreForm->isFrozen()){
						$smartProcessor->run($actions[$i]);
						$smartStoreForm->freeze();
					}
					else{
						$smartStoreForm->freeze();
					}
				}
				catch(\Exception $e){
					$smartStoreForm->freeze();
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
			else{
				$smartStoreForm->setProcessor($smartProcessor);
				/* Load extended properties */
				$smartStoreForm->setExtended($smartProcessor->document->dao->extended);
			}

			$smartStoreForm->feedbackToElement($smartProcessor->feedbacks);
			$smartStoreForm->errorsToElement($smartProcessor->errors);
			$formCollection->add( $smartStoreForm );
		}

		$formCollection->addElement('hidden', 'loop', $i);
		$this->view->assign('createForm', $formCollection->toHtml());
		$this->view->assign('wildspacePath', $wsPath);
		$this->view->assign('pageTitle', tra('Store files in').' '.$container->getName());

		$this->view->assign('mid', $template);
		$this->view->display('layouts/ranchbe.tpl');
	}

	/**
	 *
	 */
	public function validateAction()
	{
		return $this->storeAction();
	}

	/**
	 *
	 */
	protected function _getFormCollection()
	{
		//Construct the form with QuickForm lib
		$formCollection = new \Form\Collection('form', 'post', new \Form\Renderer\CollectionSmartyRenderer($this->view, 'document/smartstore/smartstoreform.tpl'));
		$formCollection->setAttribute('action',$this->getControllerRoute().'/validate');
		$formCollection->setAttribute('class','form-inline');

		//Set hidden field
		$formCollection->addElement('hidden', 'spacename', $this->spaceName);
		$formCollection->addElement('hidden', 'containerId', $this->containerId);
		//$formCollection->addElement('checkbox', 'keepco', '', 'Keep CO<br />');
		//$formCollection->addElement('checkbox', 'supfile', '', 'Suppress files from Wildspace<br/>');

		//Add submit button
		$formCollection->addElement('submit', 'save', 'Save', array('class'=>'btn btn-success'));
		$formCollection->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));

		//Create a subForm to set the headers
		//$formHeader->addElement('header', '', tra('Store files in').' '.$this->containerId );
		//$formHeader->addElement('header', '', tra('Selection review') );
		//$formCollection->add( $formHeader );

		return $formCollection;
	}

}
