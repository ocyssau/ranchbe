<?php
namespace Controller\Document;

/*
use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;

use Rbs\Ged\Document\History as DocumentHistory;
use Rbplm\Signal;

use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbplm\Sys\Error;
use Rbplm\Ged\AccessCodeException;

use Rbs\Extended;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Ged\AccessCode;
*/

class Converter extends \Controller\Controller
{
	public $pageId = 'document_freezer';
	public $defaultSuccessForward = 'document/freezer/index';
	public $defaultFailedForward = 'document/freezer/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName = null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=null;

		$this->spaceName = $spaceName;
		$this->checkFlood = \check_flood($pageId);
		$this->checkRight('admin', $this->areaId, 0);

		//Assign name to particular fields
		$this->view->assign('spacename' , $spaceName);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}


	/**
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function freezeAction()
	{
		if (empty($_REQUEST['checked'])){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
		}
		else{
			$odocument = new \document($this->space);

			$freeze = new \freeze($this->container);

			foreach($_REQUEST['checked'] as $documentId){
				$odocument->init($documentId);
				$freeze->freezeDocument($odocument);
			}
		}
		return $this->indexAction();
	} //function end

	/**
	 * @param unknown_type $this->container
	 * @param unknown_type $this->view
	 */
	function freezeallAction()
	{
		$odocument = new \document($this->space);
		$freeze = new \freeze($this->container);

		$list = $this->container->GetAllDocuments( array() , false);

		foreach($list as $docInfos){
			$odocument->init( $docInfos['document_id'] );
			$freeze->freezeDocument($odocument);
		}
		return $this->indexAction();
	} //function end

}