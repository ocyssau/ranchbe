<?php
namespace Controller\Document;

use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;

/**
 *
 *
 */
class Service extends \Controller\Controller
{
	public $pageId = 'document_service'; //(string)
	public $defaultSuccessForward = 'document/service/index';
	public $defaultFailedForward = 'document/service/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId = null;
		$spaceName = null;

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : null;

		$this->spaceName = $spaceName;

		//Assign name to particular fields
		$this->view->assign('spacename' , $spaceName);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}

	/**
	 *
	 */
	function getfromnumberService()
	{
		//get the q parameter from URL
		$number = $_GET['number'];

		$dbranchbe = \Ranchbe::getDb();
		$query = 'SELECT document_number AS number, document_version AS version, document_indice_id AS indice, document_id AS id
		FROM workitem_documents
		WHERE document_number LIKE \''. $number .'%\'
		ORDER BY document_number ASC';
		$rs = $dbranchbe->SelectLimit( $query , 20 , 0);

		if(!$rs){
			return false;
		}
		if($rs->RecordCount() === 0){
			return 0;
		}

		//convert to json
		$all = $rs->GetArray();
		$all = json_encode($all);

		//output the response
		echo $all;
		return $all;
	}

	/**
	 * AJAX Method to search in product version table
	 */
	public function searchService()
	{
		(isset($_REQUEST['what'])) ? $what = $_REQUEST['what'] : $what = null;
		(isset($_REQUEST['where'])) ? $where = $_REQUEST['where'] : $where = null;
		(isset($_REQUEST['select'])) ? $select = $_REQUEST['select'] : $select = null;
		(isset($_REQUEST['op'])) ? $op = $_REQUEST['op'] : $op = 'contains';

		$factory = DaoFactory::get($this->spaceName);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(DocumentVersion::$classId);
		$list = $factory->getList(DocumentVersion::$classId);
		$filter->andfind($what, $dao->toSys($where), $op);

		$reducedSelect = array();
		if(is_string($select)){
			$select = array($select);
		}
		if(is_array($select)){
			foreach($dao->metaModel as $asSys=>$asApp){
				if(in_array($asApp, $select)){
					$reducedSelect[] = $asSys.' as '. $asApp;
				}
			}
			$filter->select($reducedSelect);
		}

		$list->load($filter);
		//var_dump($list->stmt->queryString);
		echo json_encode($list->toArray());
	}

}
