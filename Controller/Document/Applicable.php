<?php

require_once 'conf/ranchbe_setup.php';//ranchBE configuration
require_once 'class/common/container.php'; //Class to manage the bookshop
require_once 'lib/search.inc.php';//Search engine
require_once 'HTML/QuickForm.php'; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once 'HTML/QuickForm/jscalendar.php';
require_once 'lib/Date/date.php';
$smarty = Ranchbe::getView();

$Manager = container::_factory('bookshop'); //Create new manager

//------------------------------------------------------------------------------

function prepaSearch($values){
  //var_dump($values);
  $values['container_type'] = array('bookshop'); /*space where to search
  *Il n'est pas possible de selectionner plusieurs espaces a la fois ici car sinon le document ne peux pas �tre
  *rattach� a un conteneur.
  */  
  $smarty = Ranchbe::getView();
  $smarty->assign('container_type', $values['container_type'][0]);
  $values['selectd']=array('document_id','document_number','designation','document_indice_id','document_state'); //Fields to display

  unset($values['submit']);
  //Interdit une selection vide de domaines
  if(empty($values['domaines'])) {
  	print 'un domaine est obligatoire';
  	return false;
  }

  return search($values); //Search engine
}

//------------------------------------------------------------------------------
if ($_REQUEST['action'] == 'view'){
    if ( empty($_REQUEST['document_id']) ){
      $Manager->errorStack->push(ERROR, 'Fatal', array(), 'Vous devez selectionner au moins un document');
      return false;
    }else{
      require_once('./class/common/document.php');
      $odocument = new document( $Manager->space, $_REQUEST['document_id'] );
      $odocument->ViewDocument();
    }
}

//Construct the form with QuickForm lib
$form = new HTML_QuickForm('form', 'post');

//Set default values
$form->setDefaults(array(
  'numrows'      => '100',
));

//Mots cles
/*
$motsCles = array('' ,'vis' , 'verin');
$motsCles = array_combine($motsCles , $motsCles);
$select =& $form->addElement ('select', 'motscles', tra('Mots cl�s'), $motsCles );
$select->setSize(1);
$select->setMultiple(false);
*/

//Construit les champs de saisie a partir des champs perso de l'espace bookshop
require_once('./GUI/GUI.php');

$optionalFields = $Manager->GetMetadata(); //recupere la liste et les parametres des champs optionnels
foreach($optionalFields as $field){
//  if($field['field_name']=='motscles')
//    construct_element($field , $form); //function declared in GUI.php
  if($field['field_name']=='domaines') // recupere les options de la liste
    $domaines = explode('#',$field['field_list']); //transforme la cha�ne de texte en tableau
}
//Applicabilite
$domaines = array_combine($domaines , $domaines); //cr�e un tableau en mettant les cles = les valeurs
$select =& $form->addElement ('select', 'domaines', tra('Domaine'), $domaines );
$select->setSize(1);
$select->setMultiple(false);

//numrows
//$form->addElement('text', 'numrows', tra('Display rows'), array('size' => 4, 'maxlength' => 9));

//Reset and submit
$form->addElement('reset', 'reset', 'reset');
$form->addElement('submit', 'submit', 'Search');

// Try to validate the form
if ($form->validate()) { // Form is validated, then process the request
  if ($_REQUEST['submit'] == 'Search'){
    $list = $form->process('prepaSearch', true);
    $smarty->assign_by_ref('list', $list);
  }
} //End of validate form

//echo '<pre>';
//var_dump($list);
//echo '</pre>';

//Page title
$smarty->assign('PageTitle' , 'Documents applicables');

//Set the renderer for display QuickForm form in a smarty template
new \Form\Rendererset($form, $this->view, 'form');

// Display the template
$smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
$smarty->display('custom/documentsApplicables.tpl');
