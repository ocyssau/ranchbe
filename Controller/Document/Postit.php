<?php
namespace Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbs\Postit\Postit as PostitModel;

use Rbplm\Sys\Error;
use Rbplm\People\CurrentUser;


class Postit extends \Controller\Controller
{
	public $pageId = 'document_manage'; //(string)
	public $defaultSuccessForward = 'document/postit/index';
	public $defaultFailedForward = 'document/postit/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function addService()
	{
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['body']) ? $body = trim($_REQUEST['body']) : null;

		if($body==''){
			return $this->serviceReturn(array());
		}

		$postit = new PostitModel();
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao($postit);

		$postit->newUid();
		$postit->parentId = $documentId;
		$postit->parentCid = DocumentVersion::$classId;
		$postit->spaceName = $spaceName;
		$postit->setOwner(CurrentUser::get());
		$postit->setBody($body);
		$dao->save($postit);

		return $this->serviceReturn($postit->getArrayCopy());
	}

	/**
	 *
	 */
	public function deleteService()
	{
		isset($_REQUEST['id']) ? $postitId = $_REQUEST['id'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;

		$postit = new PostitModel();
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao($postit);

		try{
			$dao->deleteFromId($postitId);
		}
		catch(\Excpetion $e){
			$this->errorStack()->add(Error::ERROR, $e->getMessage());
		}

		return $this->serviceReturn(array());
	}

}