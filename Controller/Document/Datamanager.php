<?php
namespace Controller\Document;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbs\Ged\Document\Listener;
use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbplm\Sys\Error;
use Rbplm\Ged\AccessCodeException;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Ged\AccessCode;

class Datamanager extends \Controller\Controller
{
	public $pageId = 'documentdata_manage'; //(string)
	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId = null;
		$spaceName = null;

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;

		$this->checkFlood = \check_flood($pageId);

		//Assign name to particular fields
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('spacename' , $spaceName);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->basecamp()->setForward($this);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward);
	}

	/**
	 *
	 */
	public function checkoutdocAction()
	{
		(isset($_REQUEST['ifExistmode'])) ? $ifExistmode = $_REQUEST['ifExistmode'] : $ifExistmode=null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		if($validate){
			(isset($_SESSION['checkoutToValidate'])) ? $documentIds = $_SESSION['checkoutToValidate'] : null;
		}
		else{
			(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds = array();
			(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;
			$_SESSION['checkoutToValidate'] = null;
		}

		if(!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		$wildspace = new Wildspace(CurrentUser::get());
		$form = new \Form\Document\CheckoutForm($this->view, $factory);

		$this->view->warning = 'Some files are existings in your Wildspace. What do you want to do?';

		/* Documents to validate */
		$toValidate = array();
		foreach ( $documentIds as $documentId ) {
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);
			$ignore = false;

			/* Set listener */
			$listener = new Listener($document, $factory);
			$listener->init($document::SIGNAL_POST_CHECKOUT);

			$aCode = $document->checkAccess();
			if( ($aCode > AccessCode::FREE) || ($aCode < AccessCode::FREE || $aCode === AccessCode::CHECKOUT) ){
				$this->errorStack()->feedback(tra('this document is locked with code %acode%, checkOut is not permit'), array('acode' => $aCode));
			}

			/* Checkout only MAIN, ANNEX, FRAGMENT and all other with role < 16 */
			$dfDao->loadDocfiles($document, $dfDao->toSys('mainrole').' < '.Docfile\Role::VISU);
			$confirmNeed = false;
			foreach($document->getDocfiles() as $docfile){
				if($docfile->checkAccess() !== AccessCode::FREE){
					continue;
				}

				$docfile->setParent($document);
				$docfileId = $docfile->getId();

				/* TRY TO CHECKOUT EACH DOCFILES */
				$fileName = $docfile->getName();
				$wsFile = $wildspace->getPath().'/'.$fileName;

				/* If File Exist in Wildspace */
				if( is_file($wsFile) || is_dir($wsFile) ){
					if($validate){
						try{
							if(isset($ifExistmode[$docfileId])){
								if($ifExistmode[$docfileId] == 'ignore'){
									$ignore = true;
									continue;
								}
								elseif($ifExistmode[$docfileId] == 'replace'){
									$dfService->checkout($docfile, $replace=true, $getFiles=true);
								}
								elseif($ifExistmode[$docfileId] == 'keep'){
									$dfService->checkout($docfile, $replace=false, $getFiles=false);
								}
							}
						}
						catch(\Exception $e){
							$this->errorStack()->error($e->getMessage());
							continue;
						}
					}
					else{
						$form->addDocfile($docfile);
						$toValidate[$documentId] = $documentId;
						$confirmNeed = true;
					}
				}

				/* Docfile is not existing in wildspace */
				else{
					try{
						$dfService->checkOut($docfile, $replace=false, $getFiles=true);
					}
					catch(\Exception $e){
						$this->errorStack()->error($e->getMessage());
						continue;
					}
				}
			} //foreach docfiles

			/* None docfiles is in Wildspace for this document, all docfiles are validated */
			if( (($confirmNeed==true && $validate) || ($confirmNeed==false)) && $ignore==false){
				try{
					$service->checkOut($document, $withFiles=false);
				}
				catch(\Exception $e){
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
		} //foreach document

		if($toValidate){
			$_SESSION['checkoutToValidate'] = $toValidate;
			$form->prepareRenderer('form');
			$this->view->assign('mid', $form->template);
			$this->view->display($this->layout);
			return;
		}
		else{
			$_SESSION['checkoutToValidate'] = null;
			$this->redirect($this->ifSuccessForward);
		}
	}

	/**
	 *
	 */
	function checkindocAction($ciAndKeep=false)
	{
		isset($_REQUEST['checked']) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		isset($_REQUEST['documentid']) ? $documentIds = array($_REQUEST['documentid']) : null;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate=false;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['comment']) ? $comment = trim($_REQUEST['comment']) : null;
		isset($_REQUEST['checkinandkeep']) ? $ciAndKeep = $_REQUEST['checkinandkeep'] : null;
		isset($_POST['pdmdata']) ? $inputPdmData = new \Pdm\Input\PdmData($_POST['pdmdata'], 'json') : $inputPdmData = false;

		if(!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$wildspace = Wildspace::get(CurrentUser::get());
		$factory = DaoFactory::get($this->spaceName);
		$loader = $factory->getLoader();

		/* Set some properties used by checkin service */
		$service = new \Rbs\Ged\Document\Service($factory);
		$service->inputPdmData = $inputPdmData;
		$service->wildspace = $wildspace;

		/**
		 * @var \Rbs\Ged\Document\VersionDao $dao
		 */
		$dao = $factory->getDao(Document\Version::$classId);

		/**
		 * @var \Rbs\Ged\Docfile\VersionDao $dfDao
		 */
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$form = new \Form\Document\CheckinForm($this->view, $factory, $ciAndKeep);

		if($validate){
			foreach ($documentIds as $documentId){
				try{
					$document = new Document\Version();
					$dao->loadFromId($document, $documentId);

					/* Set listener */
					$listener = new Listener($document, $factory);
					$listener->init($document::SIGNAL_POST_CHECKIN);
					$listener->getHistory()->getAction()->setComment($comment);

					$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode').'=:accessCode', array(':accessCode'=>AccessCode::CHECKOUT));
					$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
					$document->setParent($container);

					/* Update data and keep in ws */
					if($ciAndKeep){
						$service->checkin($document, $releasing=false, $updateData=true, $deleteFileInWs=false, $fromWildspace=true, $checkAccess=true);
					}
					/* Checkin data in vault */
					else{
						$service->checkin($document, $releasing=true, $updateData=true, $deleteFileInWs=true, $fromWildspace=true, $checkAccess=true);
					}
				}
				catch(AccessCodeException $e){
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else{
			foreach ($documentIds as $documentId){
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);

				/* Load docfiles and set to form */
				$dfDao->loadDocfiles($document, $dfDao->toSys('accessCode').'=:accessCode', array(':accessCode'=>AccessCode::CHECKOUT));
				$form->addDocument($document);
			}

			$this->view->assign('pageTitle' , 'Checkin this documents.');
			$this->view->assign('help' ,
				'Replace this documents in Vault.'
				.' Documents files will be updated from files found in wildspace.'
				.' Input a comment text to explain the reasons of this update.'
			);
			$this->view->assign('warning' , 'Files will be suppressed from your wildspace');
			$form->prepareRenderer('form');
			$this->view->rbgateServerUrl = \Ranchbe::get()->getConfig('rbgate.server.url');
			$this->view->assign('mid', $form->template);
			$this->view->display($this->layout);
		}
	}

	/**
	 *
	 */
	function updatedocAction()
	{
		return $this->checkindocAction(true);
	}

	/**
	 *
	 */
	function resetdocAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;

		if(!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($this->spaceName);
		$service = new \Rbs\Ged\Document\Service($factory);
		$loader = $factory->getLoader();
		$dao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		foreach ($documentIds as $documentId) {
			try{
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);
				$dfDao->loadDocfiles($document);

				/* Set listener */
				$listener = new Listener($document, $factory);
				$listener->init($document::SIGNAL_POST_CHECKIN);

				$container = $loader->loadFromId($document->getParent(true), Workitem::$classId);
				$document->setParent($container);
				$service->checkin($document, $releasing=true, $updateData=false, $deleteFileInWs=false, $fromWildspace=true, $checkAccess=true);
			}
			catch(AccessCodeException $e){
				$this->errorStack()->error($e->getMessage());
			}
			catch(\Exception $e){
				throw($e);
			}
		}

		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 * Put file in wildspace
	 */
	function putinwsAction()
	{
		$this->checkRight('read', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));


		$this->putinwsService();
		return $this->redirect($this->ifSuccessForward,
			array(
				'spaceName'=>$this->spaceName,
				'containerid'=>$this->containerId
			));
	}

	/**
	 *
	 */
	function putinwsService()
	{
		$return = array('errors', 'feedbacks');

		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;

		if(!$documentIds || $cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$service = new \Rbs\Ged\Document\Service($factory);

		$this->checkRight('edit', $this->areaId, 0);

		foreach ($documentIds as $documentId){
			try{
				$document = new Document\Version();
				$dao->loadFromId($document, $documentId);
				$dfDao->loadDocfiles($document);
				$service->putinws($document);
			}
			catch(\Exception $e){
				$return['errors'][$fileId] = $e->getMessage();
				continue;
			}
			$return['feedbacks'][$fileId] = array(
				'message'=>'success',
				'id'=>$documentId,
				'properties'=>$document->getArrayCopy(),
			);
		}
		return $this->serviceReturn($return);
	}

	/**
	 *
	 */
	function movedocumentAction()
	{
		(isset($_REQUEST['checked'])) ? $documentIds = $_REQUEST['checked'] : $documentIds=array();
		(isset($_REQUEST['documentid'])) ? $documentIds = array($_REQUEST['documentid']) : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate = false;
		(isset($_REQUEST['containerid'])) ? $toContainerId = $_REQUEST['containerid'] : $toContainerId = null;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel = false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$service = new \Rbs\Ged\Document\Service($factory);

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$form = new \Form\Document\MoveForm($factory, $this->view);
		$form->setAttribute('action',$this->actionUrl('movedocument'));

		foreach ($documentIds as $documentId){
			$document = new Document\Version();
			$dao->loadFromId($document, $documentId);
			$form->add($document);
			/*List to recap selection in view */
			$recapList[] = array('uid'=>$document->getUid(), 'id'=>$document->getId(), 'name'=>$document->getName());
		}

		if($validate){ //Validate the form
			if ($form->validate()){ //Validate the form
				$form->freeze(); //and freeze it

				/* Set listener */
				$listener = new Listener($document, $factory);
				$listener->init($document::SIGNAL_POST_MOVE);

				$toContainer = $factory->getModel(Workitem::$classId);
				$factory->getDao(Workitem::$classId)->loadFromId($toContainer, $toContainerId);

				foreach ($form->documents as $document){
					try{
						$dfDao->loadDocfiles($document);
						$service->moveDocument($document, $toContainer);
					}
					catch(\Exception $e){
						$this->errorStack()->error($e->getMessage());
						return $this->redirect($this->ifFailedForward);
					}
					//TODO: Check user right on target container
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}

		$form->prepareRenderer();
		$this->view->pageTitle = 'Move Documents';
		$this->view->documents = $recapList;
		$this->view->assign('mid', $form->template);
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function copydocumentAction()
	{
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;
		(isset($_REQUEST['containerid'])) ? $toContainerId = $_REQUEST['containerid'] : null;
		(isset($_REQUEST['newname'])) ? $toName = $_REQUEST['newname'] : null;
		(isset($_REQUEST['version'])) ? $toVersion = $_REQUEST['version'] : null;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel = false;

		if($cancel || !$documentId){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(Document\Version::$classId);
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$service = new \Rbs\Ged\Document\Service($factory);

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		if(!$documentId){
			$this->errorStack()->error('You must select only one item');
		}

		$document = new Document\Version();
		$dao->loadFromId($document, $documentId);
		$dfDao->loadDocfiles($document);

		/* Set listener */
		$listener = new Listener($document, $factory);
		$listener->init($document::SIGNAL_POST_COPY);

		$form = new \Form\Document\CopyForm($factory, $this->view);
		$form->setAttribute('action',$this->actionUrl('copydocument'));

		$form->setData(array(
			'containerid'=>$this->containerId,
			'newname'=>'copyof_'.$document->getNumber(),
			'version'=>1,
			'documentid'=>$documentId,
		));

		if ($form->validate()){ //Validate the form...
			$form->freeze(); //...and freeze it
			//TODO: Check user right on target container
			$toContainer = $factory->getModel(Workitem::$classId);
			$factory->getDao(Workitem::$classId)->loadFromId($toContainer, $toContainerId);
			try{
				$service->copyDocument($document, $toContainer ,$toName , $toVersion);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				return $this->redirect($this->ifFailedForward);
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else{
			$form->addElement('submit', null, 'GO');
			$form->applyFilter('__ALL__', 'trim');
			$form->prepareRenderer();
			$this->view->pageTitle = 'Copy Document '.$document->getName();
			$this->view->assign('mid', $form->template);
			$this->view->display($this->layout);
		}
	}


	/**
	 * params{
	 *		    'space'=''
	 *		    'documentid' = '';
	 *		    'encode' = 'json';
	 *		    'files'={
	 *			'file1':{
	 *				'name':filename,
	 *				'data':{
	 *					'name':'',
	 *					'md5':$.md5(data),
	 *					'size':0,
	 *					'data':'',
	 *					'mimestype':'',
	 *					'encode':'base64'
	 *				}
	 *			}
	 *		}
	 *	}
	 *
	 *
	 */
	function assocfileService()
	{
		$return=array();
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : $documentId=null;
		isset($_REQUEST['files']) ? $files = $_REQUEST['files'] : $files=null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
						'spacename'=>$this->spaceName,
						'containerid'=>$this->containerId
				));

		$factory = DaoFactory::get($spaceName);
		$service = new \Rbs\Ged\Document\Service($factory);
		$wildspace = Wildspace::get(CurrentUser::get());

		if($spaceName && $documentId){
			$document = new Document\Version();
			$factory->getDao($document)->loadFromId($document, $documentId);

			$containerId = $document->getParent(true);
			$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);

			$document->setParent($container);
		}
		else{
			unset($files);
			$return['errors'][]=array(
				'function'=>__FUNCTION__,
				'code'=>5010,
				'message'=>'spacename and documentid must be set',
				'input'=>$_REQUEST,
			);
			echo json_encode($return);
			die;
		}

		/*------------- parse the document from input -------------*/
		for($i=2,$index='file1'; isset($files[$index]); $index='file'.$i++)
		{
			$file1 = $files[$index];
			isset($file1['name'])? $fileName = basename($file1['name']) : $fileName = uniqid();

			try{
				//Put data in Vault
				$data = base64_decode($file1['data']['data']);
				$file = $wildspace->getPath().'/'.$fileName;
				file_put_contents($file, $data);

				if($fileName && is_file($file)){
					$service->associateFile($document, $file);
				}

				/* Get the new Docfile */
				$docfile = end($document->getDocfiles());

				$return['feedbacks'][]=array(
					'index'=>$index,
					'name'=>$name,
					'fullpath'=>$file,
					'properies'=>$docfile->getArrayCopy()
				);
				$outfiles[] = $name;
			}
			catch (\Exception $e){
				$return['errors'][]=array(
					'index'=>$index,
					'name'=>$name,
					'fullpath'=>$toPath,
					'function'=>__FUNCTION__,
					'code'=>5000,
					'message'=>'Error during file upload',
					'exception_code'=>$e->getCode(),
					'exception_messsage'=>$e->getMessage(),
				);
				continue;
			}
		}
		$return['files'] = $outfiles;
	}

	/**
	 *
	 */
	function assocfileAction()
	{
		(isset($_REQUEST['file'])) ? $fileName = $_REQUEST['file'] : null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;
		(isset($_REQUEST['validate'])) ? $validate = $_REQUEST['validate'] : $validate=false;
		(isset($_REQUEST['cancel'])) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		$this->checkRight('edit', $this->areaId, $this->containerId,
				$this->ifSuccessForward,
				array(
					'spacename'=>$this->spaceName,
					'containerid'=>$this->containerId
				));

		if($cancel || !$documentId){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($this->spaceName);
		$service = new \Rbs\Ged\Document\Service($factory);
		$wildspace = Wildspace::get(CurrentUser::get());
		$dao = $factory->getDao(Document\Version::$classId);

		$document = new Document\Version();
		$dao->loadFromId($document, $documentId);
		$uid = $document->getUid();
		$number = $document->getNumber();

		$containerId = $document->getParent(true);
		$container = $factory->getDao(Workitem::$classId)->loadFromId(new Workitem(), $containerId);
		$document->setParent($container);

		//Filter and paginations
		$filter = new \Rbplm\Sys\Filesystem\Filter();
		$this->bindSortOrder($filter, 'name');

		if(!$fileName)
		{
			$this->checkRight('edit', $this->areaId, 0);

			//ASSOC_FILE_CHECK_NAME is set in ranchbe_setup.php
			//Limit list of files to files with the document name in the name
			if (ASSOC_FILE_CHECK_NAME == true){
				$filter->andFind($number, 'name', Op::CONTAINS);
			}

			$form = new \Form\Document\AssociateFileForm($wildspace, $filter, $this->view);
			$form->setDefaults(array('documentid'=>$documentId, 'mainrole'=>\Rbplm\Ged\Docfile\Role::ANNEX));

			// Display the template
			$form->prepareRenderer('form');
			$this->view->assign('documentUid', $uid);
			$this->view->mid = $form->template;
			$this->view->display($this->layout);
		}
		else{
			$file = $wildspace->getPath().'/'.$fileName;
			if($fileName && is_file($file)){
				$service->associateFile($document, $file, true, true);
			}
			return $this->redirect($this->ifSuccessForward);
		}
	}
}