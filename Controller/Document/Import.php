<?php
namespace Controller\Document;

use Rbplm\Dao\Filter\Op;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/freeze.php';
require_once 'class/doccomment.php';
require_once 'class/common/metadata.php';
require_once 'class/common/container.php'; //Class to manage the container
require_once 'class/common/doclink.php';
require_once 'class/common/attachment.php';
require_once 'class/Document/Archiver.php';
require_once 'class/wildspace.php';
require_once 'class/common/fsdata.php';
require_once 'class/DocBasket.php';
require_once 'class/common/import.php'; //Class to manage the importations

class Import extends \Controller\Controller
{

	public $pageId = 'document_import'; //(string)
	public $defaultSuccessForward = 'document/import/index';
	public $defaultFailedForward = 'document/import/index';

	/**
	 */
	function _checkErrors()
	{
		if ( $this->errorStack->hasErrors() ){
			return $this->errorStack->getErrors(true);
		}
	} //End of function

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId=null;
		$space=null;

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $space = $_REQUEST['space'] : null;

		if($space){
			$this->container = \container::_factory($space , $containerId);
			$this->space = $this->container->space;
			$this->spaceName = $space;
			$this->areaId = $this->container->AREA_ID;
			$this->import = new \import($space, $this->container);
		}

		$this->checkFlood = \check_flood($_REQUEST['page_id']);

		$this->checkRight('admin', $this->areaId, 0);

		//Assign name to particular fields
		$this->view->assign('CONTAINER_TYPE', $space);
		$this->view->assign('CONTAINER_NUMBER' , $space.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $space.'_description');
		$this->view->assign('CONTAINER_STATE' , $space.'_state');
		$this->view->assign('CONTAINER_ID' , $space.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $space);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *  Step 1
	 */
	public function indexAction()
	{
		$this->view->assign( 'step' , '1');
		$this->view->assign( 'step_description' , 'Select the csv file');

		($template == null) ?  $template = 'document/import/index.tpl' : null;

		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}

	/**
	 *  Step Step 2 select the cvs file and display it for validation and check it
	 */
	public function visucsvAction()
	{
		$this->view->assign( 'step' , '2');
		$this->view->assign( 'step_description' , 'Validate the csv file');

		if(!is_file($_FILES['importDocumentCsvList']['tmp_name'])){
			$this->view->assign( 'step' , '1');
			$this->view->assign( 'step_description' , 'Select the csv file');
			return $this->indexAction();
		}

		if(!is_dir($this->container->WILDSPACE . '/.rbetmp/')){
			if(!mkdir($this->container->WILDSPACE . '/.rbetmp/'))
			{
				print 'cant create tmpdir';
				return false;
			}
		}

		$csvfile = $this->container->WILDSPACE . '/.rbetmp/' . basename($_FILES['importDocumentCsvList']['tmp_name']);
		move_uploaded_file($_FILES['importDocumentCsvList']['tmp_name'], $csvfile);

		$list = $this->import->ImportCsv($csvfile);
		$this->view->assign_by_ref( 'list' , $list);
		$this->view->assign( 'cvsfile' , $csvfile);
		foreach($list[0] as $key=>$val){ //Construct a array with the header of each cols
			$fields[] =  $key;
		}
		$this->view->assign( 'fields' , $fields);

		//check each file
		$tmp_list = array();
		foreach($list as $document){
			$fileInfos = array();
			$docinfo = array();

			//---------- The TDP specifie a file
			if(!empty($document['file'])){
				if(!is_file($this->container->WILDSPACE . '/' . $document['file'])){
					$document['errors'][] = $document['file'] . ' is not in your Wildspace.';
					$document['actions']['ignore'] = 'Ignore';
					$clean_list[] = $document;
					continue;
				}
				$odocument = $this->container->initDoc();
				$odocfile = $odocument->initDocfile();
				$odocument->SetDocProperty('document_number', $document['document_number']);
				if($file_id = $odocfile->GetFileId($document['file'])){ //- The file is recorded in database
					$odocfile->init($file_id);
					$fdoc_id = $odocfile->GetDocumentId(); //Get info about the father document
					$odocument->init($fdoc_id);
					$docinfo = $odocument->GetDocumentInfos($fdoc_id); //Get the document of this file
					$document['errors'][] = 'file '.$document['file'] . ' exist in this database. ID=' . $file_id;
					$document['errors'][] = 'file '.$document['file'] . ' is linked to document '. $docinfo['document_number'];
					$document['actions']['update_doc'] = 'Update the document '.$docinfo['document_number'].' and files';
					$document['actions']['update_file'] = 'Update the file '.$document['file'].' only';
					//$document['actions']['upgrade_doc'] = 'Create a new indice of '.$docinfo['document_number'];
					$document['document_id'] = $fdoc_id;
					$document['file_id'] = $file_id;
				}
				else{//-- The file is not recorded in database
					if(empty($document['document_number'])){
						//Generate the number from the file name
						$document['document_number'] = substr($document['file'], 0, strrpos($document['file'], '.'));
						$odocument->SetDocProperty('document_number', $document['document_number']);
					}
					//Check double documents
					if(in_array($document['document_number'] , $tmp_list)){
						$document['errors'][] = $document['document_number'] . ' is not unique in this package';
						$document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
					}
					else{
						$tmp_list[] = $document['document_number'];
						//Check if document exist in database
						if($document_id = $odocument->DocumentExist($document['document_number'])){
							$document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
							$document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
							//$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
							$document['document_id'] = $document_id;
						}
						else{ //Document dont existing in database
							$fsdata = $odocfile->initFsdata( $this->container->WILDSPACE.'/'.$document['file'] );
							$fileInfos = $fsdata->getInfos(false); //Get info about file
							$document['actions']['create_doc'] = 'Create a new document';
						}
					}
				}
			}
			else{ //End of if file condition
				//The TDP specifie a document number
				if(!empty($document['document_number'])){
					$fileInfos['file_type'] = 'nofile'; //To set the doctype
					$fileInfos['file_root_name'] = $document['document_number']; //To set the doctype
					//Check if document exist in database
					$odocument = $this->container->initDoc();
					if($document_id = $odocument->DocumentExist($document['document_number'])){
						$document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
						$document['actions']['update_doc'] = 'Update the document '.$document['document_number'];
						//$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
						$document['document_id'] = $document_id;
					}
					else{
						$document['actions']['create_doc'] = 'Create a new document';
					}
				}
			}

			//Check that category_id is a integer
			if(!empty($document['category_id']))
				if(!ctype_digit($document['category_id'])){
				$document['errors'][] = 'category_id of '. $document['document_number'] . ' is not a integer';
			}
			//Check that document_indice_id is a integer
			if(!empty($document['document_indice_id']))
				if(!ctype_digit($document['document_indice_id'])){
				$document['errors'][] = 'document_indice_id of '. $document['document_number'] . ' is not a integer';
			}
			//Check the doctype for the new document
			if(isset($document['actions']['create_doc'])){
				$document['doctype'] = $odocument->SetDocType($fileInfos['file_extension'], $fileInfos['file_type']);
				if(!$document['doctype']){
					$document['errors'][] = 'This document has a bad doctype';
					unset($document['actions']); //Reinit all actions
				}
			}

			//Add default actions
			$document['actions']['ignore'] = 'Ignore';

			$clean_list[] = $document;
			$currentdata[] = $docinfo; //original values of documents properties.
		}
		$this->view->assign('currentdata', $currentdata);

		$list = $clean_list;
	}


	/**
	 * Step 2 select the zip file and display it for validation and check it
	 */
	public function visuzipfile(){

		$smarty->assign( 'step' , '2.1');
		$smarty->assign( 'step_description' , 'Validate the zip file');

		// Display the template
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		if(!is_file($_FILES['importzipfile']['tmp_name'])){
			$smarty->assign( 'step' , '1');
			$smarty->assign( 'step_description' , 'Select the csv file');
			break;
		}

		if(!is_dir($this->container->WILDSPACE . '/.rbetmp/')){
			if(!mkdir($this->container->WILDSPACE . '/.rbetmp/')){
				print 'cant create tmpdir';
				return false;
			}
		}

		/*$targetDir = $this->container->WILDSPACE.'/.rbetmp/'.time();
		 if(!mkdir($targetDir)) {print 'cant create '.$targetDir;return false;}*/
		$targetDir = $this->container->WILDSPACE;

		$zipfile = $this->container->WILDSPACE . '/.rbetmp/' . basename($_FILES['importzipfile']['name']);
		move_uploaded_file($_FILES['importzipfile']['tmp_name'], $zipfile);

		$Import->UnpackImportPackage($zipfile , $targetDir);
		$list = $Import->unpack;

		$tmp_list = array(); //init the var for foreach loop
		$clean_list = array(); //init the var for foreach loop
		$smarty->assign( 'displayHeader' , 1); // true for display header
		$i = 0;

		foreach($list as $file){
			$i = $i + 1;
			if(!is_file($this->container->WILDSPACE.'/'.basename($file))){
				$document['errors'][] = $file.' is not in your Wildspace.';
				$document['actions']['ignore'] = 'Ignore';
				continue;
			}
			$odocument = $this->container->initDoc();
			$odocfile = $odocument->initDocfile();
			if($file_id = $odocfile->GetFileId(basename($file))){ //- The file is recorded in database
				$odocfile->init($file_id);
				$fdoc_id = $odocfile->GetDocumentId(); //Get info about the father document
				$odocument->init($fdoc_id);
				$document = $odocument->GetDocumentInfos(); //Get the document of this file
				$document['errors'][] = 'file '.$document['file'] . ' exist in this database. ID=' . $file_id;
				$document['errors'][] = 'file '.$document['file'] . ' is linked to document '. $document['document_number'];
				$document['actions']['update_doc'] = 'Update the document '.$document['document_number'].' and files';
				$document['actions']['update_file'] = 'Update the file '.$document['file'].' only';
				//$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
				$document['file_id'] = $file_id;
				$document['file'] = basename($file);
			}else{//-- The file is not recorded in database
				//echo 'Generate the number from the file name<br>';
				$document['file'] = basename($file);
				$document['document_number'] = basename(substr($document['file'], 0, strrpos($document['file'], '.')));
				//Check double documents
				if(in_array($document['document_number'] , $tmp_list)){
					$document['errors'][] = $document['document_number'] . ' is not unique in this package';
					$document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
				}else{
					$tmp_list[] = $document['document_number'];
					//Check if document exist in database
					if($document_id = $odocument->DocumentExist($document['document_number'])){
						$document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
						$document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
						//$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
						$document['document_id'] = $document_id;
					}else{
						$document['actions']['create_doc'] = 'Create a new document';
					}
				}
			}

			//Check the doctype for the new document
			if(isset($document['actions']['create_doc'])){
				$fsdata = $odocfile->initFsdata($file);
				$document['doctype']=$odocument->SetDocType($fsdata->getProperty('file_extension'), $fsdata->getProperty('file_type'));
				if(!$document['doctype']){
					$document['errors'][] = 'This document has a bad doctype';
					unset($document['actions']); //Reinit all actions
				}
			}

			//Add default actions
			$document['actions']['ignore'] = 'Ignore';
			SetCreateDocForm($document, $i);
			$smarty->assign( 'document' , $document);
			$smarty->assign( 'loop_id' , $i);
			$smarty->display('documentImportZipfile.tpl');
			//Display the header only
			$smarty->assign('displayHeader' , 0);

			$document = array();

		}//End of foreach
		$smarty->assign( 'displayFooter' , 1);
		$smarty->display('documentImportZipfile.tpl');
		die;
		break;
	}

	/**
	 * Step 3 select options
	 */
	public function validatecsv(){
		$smarty->assign( 'step' , '3');
		$smarty->assign( 'step_description' , 'Choose options');
		$smarty->assign( 'maxcsvrow' , $this->container->maxcsvrow);

		//Record data in session
		//session_register('import'); //Record variable in session
		$_SESSION['import'] = $_POST;
		/*
		 for ($i = 0; $i < $_POST['loop']; $i++){
		echo $i . '-----<br>';
		echo $_POST['docaction'][$i] . '<br />';
		foreach($_POST['fields'] as $field){
		echo $field . ' : ';
		echo $_POST[$field][$i] . '<br />';
		}
		}*/
	}

} //End of class
