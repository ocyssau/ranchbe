<?php

namespace Controller\Test;


use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Connexion;
use Rbplm\People\CurrentUser;

class Test extends \Controller\Controller
{
	protected function _init()
	{
		error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
		ini_set ( 'display_errors', 1 );
		ini_set ( 'display_startup_errors', 1 );
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 1000);
		ini_set('xdebug.var_display_max_data', 5000);
		ini_set('xdebug.var_display_max_depth', 4);
		//echo 'include path: ' . get_include_path ()  . PHP_EOL;
	}
	
	/**
	 * 
	 */
	public function autoloaderAction()
	{
		$this->_init();
		$Object = new Connexion();
	}
	
	/**
	 * 
	 */
	public function pingAction()
	{
	    echo "Hello!";
	}
	
	/**
	 * 
	 */
	public function pingService()
	{
		return $this->pingAction();
	}
	
	/**
	 * 
	 */
	public function ajaxAction()
	{
		$this->view->display('test/ajax.tpl');
	}
	
	/**
	 * 
	 */
	public function referenceAction()
	{
		$cacheUsualName =& \Ranchbe::$registry;
		$cacheUsualName['added'] = 'titi';
		var_dump( \Ranchbe::$registry );
	}
	
	/**
	 * 
	 */
	public function subformAction()
	{
		require 'GUI/form/subform.php';
		$form = new \subform();
		$form->addElement('text', 'name', 'label', array('value'=>'default value'));
		$form->addElement('text', 'name2', 'label', array('value'=>'default value'));
		$form->addElement('text', 'name3', 'label', array('value'=>'default value'));
		echo $form->toHtml();
	}
	
	/**
	 * 
	 */
	public function jsAction()
	{
		$this->view->assign('mid','test/js.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
}