<?php

namespace Controller\Test;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Connexion;
use Rbplm\People\CurrentUser;

class Jquery extends \Controller\Controller
{
	protected function _init()
	{
		error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
		ini_set ( 'display_errors', 1 );
		ini_set ( 'display_startup_errors', 1 );
		
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 1000);
		ini_set('xdebug.var_display_max_data', 5000);
		ini_set('xdebug.var_display_max_depth', 4);
		
		//echo 'include path: ' . get_include_path ()  . PHP_EOL;
	}
	
	public function jsAction()
	{
		$this->view->assign('mid','test/js.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
	public function widgetAction()
	{
		$this->view->assign('mid','test/jquery/widget.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
	public function objectAction()
	{
		$this->view->assign('mid','test/jquery/object.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
	public function uniqidAction()
	{
		$this->view->assign('mid','test/jquery/uniqid.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
	public function ajaxtabAction()
	{
		$this->view->assign('mid','test/jquery/ajaxtab.tpl');
		$this->view->display('layouts/popup.tpl');
	}
	
}