<?php
namespace Controller\Test;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Connexion;
use Rbplm\People\CurrentUser;

class Jsplumb extends \Controller\Controller
{
	protected function _init()
	{
		error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
		ini_set ( 'display_errors', 1 );
		ini_set ( 'display_startup_errors', 1 );
		
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 1000);
		ini_set('xdebug.var_display_max_data', 5000);
		ini_set('xdebug.var_display_max_depth', 4);
		
		//echo 'include path: ' . get_include_path ()  . PHP_EOL;
	}
	
	public function indexAction()
	{
		$this->view->assign('mid','test/jsplumb/index.tpl');
		$this->view->display('test/jsplumb/index.tpl');
	}
}