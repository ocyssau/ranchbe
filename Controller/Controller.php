<?php
namespace Controller;

use Ranchbe;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Acl\AccessRestrictionException;
use Rbplm\People\CurrentUser;
use Rbplm\Dao\NotExistingException;

abstract class Controller
{
	public $view;
	public $errorStack;
	public $layout;

	protected $_request;

	protected $_route;
	protected $_baseurl;

	protected $_isService;

	public $pageId;
	public $ifSuccessForward;
	public $ifFailedForward;

	public $defaultSuccessForward;
	public $defaultFailedForward;

	/**
	 *
	 */
	public function __construct()
	{
		$this->layout = 'layouts/ranchbe.tpl';
	}

	/**
	 * @param unknown_type $view
	 * @param unknown_type $errorStack
	 */
	public function checkAccess()
	{
		$authService = Ranchbe::getAuthservice();
		$storage = $authService->getStorage();
		$usr = People\CurrentUser::get();

		if ( !$authService->hasIdentity() ){
			if($_REQUEST['logout'] != 1){
				$this->view->assign( 'hidden_serialized_post', serialize_request_post() );
			}
			return $this->redirect('auth/login', serialize_request_post());
		}
		$userProperties = $storage->read();

		$user = new People\User();
		$user->hydrate($userProperties);
		People\CurrentUser::set($user);

		$user->setLastLogin(new \DateTime());
		if( $user->isLoaded() ){
			DaoFactory::get()->getDao($user)->updateLastLogin($user);
		}
	}

	/**
	 * @param unknown_type $view
	 * @param unknown_type $errorStack
	 */
	public function checkRight($rightName, $areaId=1, $resourceId=0)
	{
		$dao = DaoFactory::get()->getDao(\Rbs\Acl\Right::$classId);
		$userId = CurrentUser::get()->getId();

		if($userId==1){
			return true;
		}

		try{
			try{
				$result = $dao->check($userId, $rightName, $areaId, $resourceId);
			}
			catch(NotExistingException $e){
				throw new AccessRestrictionException('check rights failed for this credentials');
			}

			if($result['rule']<>'allow'){
				throw new AccessRestrictionException('check rights failed for this credentials');
			}
		}
		catch(AccessRestrictionException $e){
			$this->errorStack->warning($e->getMessage());
			$this->redirect('application/home/home');
		}
	}

	/**
	 *
	 * @param unknown_type $view
	 * @param unknown_type $errorStack
	 */
	public function init($view=null, $errorStack=null)
	{
		($view==null) ? $this->view = \Ranchbe::getView() : $this->view = $view;
		($errorStack==null) ? $this->errorStack = \Ranchbe::getErrorStack() : $this->errorStack = $errorStack;

		$this->_isService = false;
		$this->view->assign('isService', false);

		isset($_REQUEST['page_id']) ? $this->pageId = $_REQUEST['page_id'] : null;
		$this->view->assign('pageid', $this->pageId);

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$layoutSelector = new \Controller\LayoutSelector();
		$layoutSelector->setLayout($this);
		$this->layoutSelector = $layoutSelector;

		$this->checkAccess();
		rbinit_context();
	}

	/**
	 *
	 */
	public function initService()
	{
		$this->init();
		$this->_isService = true;
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 * To run after action
	 * Not called if redirect is call
	 */
	public function postDispatch()
	{
		/*
		if(!isset($this->basecamp)){
			$basecamp = new \Controller\Basecamp('');
			$basecamp->clear();
		}
		*/
	}

	/**
	 * To run after action
	 * Not called if redirect is call
	 */
	public function basecamp()
	{
		if(!isset($this->basecamp)){
			$this->basecamp = new \Controller\Basecamp();
		}
		return $this->basecamp;
	}

	/**
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @return \Rbs\Dao\Sier\Filter
	 */
	public function bindSortOrder($filter, $byDefault='number')
	{
		isset($_REQUEST['orderby']) ? $sortBy = $_REQUEST['orderby'] : $sortBy = $byDefault;
		isset($_REQUEST['order']) ? $sortOrder = $_REQUEST['order'] : $sortOrder = 'asc';

		$this->view->assign('orderby', $sortBy);
		$this->view->assign('order', $sortOrder);

		$filter->sort($sortBy, $sortOrder);
		return $filter;
	}

	/**
	 *
	 */
	public function getRequest()
	{
		if(!isset($this->_request)){
			$this->_request = new \View\Mvc\Request();
		}

		return $this->_request;
	}

	/**
	 * Alias for getRequest
	 * Implement ZF2 controller helper
	 */
	public function params()
	{
		return $this->getRequest();
	}

	/**
	 *
	 * @param Zend_Controller_Request_Simple $request
	 */
	public function setRequest(\Zend_Controller_Request_Simple $request)
	{
		$this->_request = $request;
		return $this;
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function setRoute($module,$controller,$action)
	{
		$this->_route[0] = $module;
		$this->_route[1] = $controller;
		$this->_route[2] = $action;
		if($this->view){
			$this->view->assign('module',$module);
			$this->view->assign('controller',$controller);
			$this->view->assign('action',$action);
		}
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function setBaseUrl($baseUrl)
	{
		$baseUrl = trim($baseUrl,'/');
		if($baseUrl){
			$this->_baseurl = $baseUrl;
		}
		if($this->view){
			$this->view->assign('baseurl', $baseUrl);
		}
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function getRoute()
	{
		return strtolower($this->_baseurl.'/'.$this->_route[0].'/'.$this->_route[1].'/'.$this->_route[2]);
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function actionUrl($action)
	{
		return strtolower($this->_baseurl.'/'.$this->_route[0].'/'.$this->_route[1].'/'.$action);
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function getControllerRoute()
	{
		return strtolower($this->_baseurl.'/'.$this->_route[0].'/'.$this->_route[1]);
	}

	/**
	 * @param string
	 * @param string
	 * @param string
	 */
	public function getModuleRoute()
	{
		return strtolower($this->_baseurl.'/'.$this->_route[0]);
	}

	/**
	 *
	 */
	public function errorForward()
	{
		$this->redirect($this->ifFailedForward);
	}

	/**
	 *
	 */
	public function successForward()
	{
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function errorStack()
	{
		return $this->errorStack;
	}

	/**
	 *
	 * @param string $fwd
	 * @param array $params
	 */
	public function redirect($fwd, $params=null)
	{
		$baseUrl = $this->_baseurl;
		$location = ( strtolower($baseUrl.'/'.$fwd) );
		if(is_array($params)){
			$assigns = array();
			foreach($params as $name=>$value){
				$assign[] = $name.'='.$value;
			}
			$t = strpos($fwd, '?');
			if($t===false){
				$location = $location.'?'.implode('&', $assign);
			}
			else{
				$location = $location.'&'.implode('&', $assign);
			}
		}
		header("Location: $location");
		die;
	}

	/**
	 * $return is json encoded and send to respons.
	 * If errors is found in errorStack, then return the found errors as Json.
	 *
	 * @param array $return
	 */
	protected function serviceReturn($return)
	{
		$flash = $this->errorStack->getFlash();
		if($flash->hasError()){
			foreach($flash->getErrors() as $message){
				$return['errors'][] = array(
					'message'=>$message,
				);
			}
			$flash->clear();
			http_response_code(500);
			echo json_encode($return);
		}
		else{
			echo json_encode($return);
		}
	}

	/**
	 *
	 * @param unknown_type $return
	 */
	public function exceptionToJson($e)
	{
		$return = array('error'=>array(
			'message'=>$e->getMessage(),
			'code'=>$e->getCode(),
			'file'=>$e->getFile(),
			'line'=>$e->getLine(),
		));
		echo json_encode($return);
	}


}
