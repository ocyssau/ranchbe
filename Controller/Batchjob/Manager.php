<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once ('./conf/ranchbe_setup.php');//ranchBE configuration
require_once('./class/Batchjob.php');
require_once('./class/datatypes/Package.php');
require_once('./class/common/container.php');
$smarty = Ranchbe::getView();

/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_suppress($smarty, $error_stack)
{
	if ( empty($_REQUEST['checked']) ) {
		print 'none selected package';
		return;
	}
	
	$Job = new Rbplm_Batchjob();
	foreach ($_REQUEST['checked'] as $id ){
		$Job->suppress($id);
	}
}

/**
 */
function batchjobsmanager_seelog($smarty, $error_stack)
{
	$logFile = '/var/log/rbmockup/runRbBatchJob.log';
	echo '<pre>';
	$content = file_get_contents($logFile);
	echo $content;
	echo '</pre>';
	die;
}


/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_reinit($smarty, $error_stack)
{
	if ( empty($_REQUEST['checked']) ) {
		print 'none selected package';
		return;
	}
	
	foreach ($_REQUEST['checked'] as $id ){
		$Job = new Rbplm_Batchjob();
		$Job->load($id);
		$Job->setState('init');
		$Job->update();
	}
}


/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_detail($smarty, $error_stack)
{
	if ( empty($_REQUEST['checked']) ) {
		print 'none selected package';
		return;
	}
	
	$id = $_REQUEST['checked'];
	$Job = new Rbplm_Batchjob();
	$Job->load($id);
	$properties = $Job->getProperties();
	$smarty->assign('properties', $properties);
	$smarty->assign('pageTitle', 'Detail of job ' . $Job->getProperty('name') );
	
	//var_dump($properties);
	//var_dump( $Job->getProperty('data') );
	//var_dump( $Job->getProperty('id'), $id );
	//var_dump( $Job->getProperties() );
	//die;
	
	$data = array();
	foreach( $Job->getProperty('data') as $pack ){
		$packageId = $pack[0];
		$containerId = $pack[1];
		$Package = new Rb_Datatype_Package('mockup');
		$Package->load( $packageId );
		$Container = container::_factory( 'mockup', $containerId );
		$datas[] = array( 	'packageName'=>$Package->getProperty('fileName'),
							'packageId'=>$packageId,
							'target'=>$Container->getProperty('number'),
							'targetId'=>$containerId,
		);
	}
	
	//var_dump($datas);
	$smarty->assign('data', $datas);
	$smarty->display('batchjobDetail.tpl');
}


/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_refresh($smarty, $error_stack)
{
}

/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_list($smarty, $error_stack){
	batchjobsmanager_display($smarty, $error_stack);
	die;
}

/**
 * 
 * @param import $Job
 * @param container $Manager
 * @param smarty $smarty
 */
function batchjobsmanager_display($smarty, $error_stack)
{
	//Process the code for pagination and display management
	$filterForm = new \Form\Filter\StandardSimple($this->view, $this->pageId);
	$filter = new \Rbs\Dao\Sier\Filter('', false);
	$this->bindSortOrder($filter, 'handle');
	$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
	$filterForm->bind($filter)->save();
	
	//Display the users list of system
	$params = array (
			'orders' => array($filter->getSort() => $filter->getOrder()),
			'limit' => $filter->getLimit(),
			'offset' => $filter->getOffset(),
			'select'=>'all',
	);
	
	$Job = new Rbplm_Batchjob();
	$Manager =& $Job;
	$list = $Job->getAll( $params ); //get infos on all package in deposit directories
	$smarty->assign('list', $list);
	
	$error_stack->checkErrors();
	
	// Display the template
	$smarty->assign('randWindowName', 'file import');
	$smarty->display('batchjobs.tpl');
}

$smarty->assign( 'action' , $_REQUEST['action']);

//Call the action
if( $_REQUEST['action'] ){
	$actionFunc = 'batchjobsmanager_' . strtolower($_REQUEST['action']);
	call_user_func_array($actionFunc, array($smarty, Ranchbe::getErrorStack()));
}

//display
batchjobsmanager_display($smarty, $error_stack);
