<?php
namespace Controller\Project;

use Rbs\Org\Project\Link\Process as ProcessLink;
use Workflow\Model\Wf\Process;

class Linkprocess extends \Controller\Project\Adminlinks
{
	public $pageId = 'project_linkprocess';
	public $defaultSuccessForward = 'project/linkprocess/display';
	public $defaultFailedForward = 'project/linkprocess/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		$this->addTemplate = 'project/link/addprocess.tpl';
		$this->displayTemplate = 'project/link/addprocess.tpl';

		$this->linkClassId = ProcessLink::$classId;
		$this->childClassId = Process::$classId;
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller\Project.Adminlinks::_getModel()
	 */
	protected function _getModel()
	{
		return new ProcessLink();
	}
}
