<?php
namespace Controller\Project;

use Rbplm\Org\Project;
use Rbs\Space\Factory as DaoFactory;
use Form\Project\EditForm;

class Index extends \Controller\Controller
{
	protected $areaId = 1;
	public $pageId = 'project_index';
	public $defaultSuccessForward = 'project/index/display';
	public $defaultFailedForward = 'project/index/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		\View\Helper\MainTabBar::get()->getTab('project')->activate();
	}

	/**
	 *
	 */
	public function displayAction($list=null)
	{
		$this->checkRight('read', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);

		$filterForm = new \Form\Project\FilterForm($this->view, $factory, $this->pageId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'name');
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		//get projects
		if(!$list){
			$list = $factory->getList(Project::$classId);
			$list->load($filter->__toString());

			$count = $list->countAll($filter->__toString());
			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'project/display.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$this->checkRight('delete', $this->areaId, 0);

		isset($_REQUEST['checked']) ? $projectIds=$_REQUEST['checked'] : $projectIds=array();
		isset($_REQUEST['projectid']) ? $projectIds[]=$_REQUEST['projectid'] : null;

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);

		foreach ($projectIds as $projectId){
			try{
				$dao->deleteFromId($projectId);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				$this->redirect($this->ifFailedForward);
			}
		}
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('create', $this->areaId, 0);

		$project = Project::init();
		$project->setName(uniqid());

		//Get the project infos
		$factory = DaoFactory::get();
		$form = new EditForm($this->view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($project);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$project->setNumber($project->getName());
					$factory->getDao($project)->save($project);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Create Project');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function editAction()
	{
		isset($_REQUEST['projectid']) ? $projectId=$_REQUEST['projectid'] : null;
		isset($_REQUEST['id']) ? $projectId=$_REQUEST['id'] : null;
		isset($_REQUEST['checked']) ? $projectId=$_REQUEST['checked'] : null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Project::$classId);
		$project = new Project();
		$dao->loadFromId($project, $projectId);

		//Get the project infos
		$form = new EditForm($this->view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($project);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$project->dao->save($project);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Edit Project '.$project->getName());
		$this->view->display($this->layout);
	}

}
