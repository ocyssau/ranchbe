<?php
namespace Controller\Project;

require_once 'class/project.php';

class Assignpermission extends \Controller\Controller
{
	public $pageId = 'project_assignpermission';
	public $defaultSuccessForward = 'project/assignpermission/display';
	public $defaultFailedForward = 'project/assignpermission/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['project_id']) ? $this->projectId = $_REQUEST['project_id'] : null;
		isset($_REQUEST['group']) ? $this->groupId = $_REQUEST['group'] : null;

		$this->project = \project::_factory($this->projectId);

		$projectInfos = $this->project->GetProjectInfos($projectId);
		$this->projectAreaId = $projectInfos['area_id'];

		$this->view->assign('area_id', $this->projectAreaId );
		$this->view->assign('project_id', $this->projectId );
		$this->view->assign('group', $this->groupId );

		$this->groupId = $_REQUEST['group'];
		$this->user = \Ranchbe::getCurrentUser();
		$this->userlib = \Ranchbe::getUserlib();
		$this->lua = \Ranchbe::getLiveUserAdmin();

		\View\Helper\MainTabBar::get()->getTab('project')->activate();
	}

	/**
	 *
	 */
	public function assignAction()
	{
		$permId = $_REQUEST['perm'];
		$groupId = $_REQUEST['group'];
		$this->userlib->assign_permission_to_group($permId, $groupId);
		$this->redirect($this->ifSuccessForward, array('group'=>$groupId));
	}

	/**
	 *
	 */
	public function updateAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$groupId = $this->groupId;
		$permNames = $_REQUEST['permName'];
		$perm = $_REQUEST['perm'];

		foreach (array_keys($permNames) as $per) {
			if (isset($perm[$per])) {
				$ret = $this->userlib->assign_permission_to_group($per, $groupId, $this->lua);
			}
			else {
				$this->userlib->remove_permission_from_group($per, $groupId, $this->lua);
			}
		}
		$this->view->assign('msg', 'Permissions updated successful');
		$this->redirect($this->ifSuccessForward, array('group'=>$groupId, 'project_id'=>$this->projectId));
	}

	/**
	 *
	 */
	public function displayAction()
	{
		$groupId = $this->groupId;
		$areaId = $this->projectAreaId;

		//Get the permissions of the group
		$groupPerms = $this->userlib->get_group_permissions($groupId, $this->lua);
		$this->view->assign('group_perms', $groupPerms);

		//Get all permissions
		(isset($_REQUEST['find'])) ? $find = $_REQUEST['find'] : $find = '';
		(isset($_REQUEST['type'])) ? $type = $_REQUEST['type'] : $type= '';

		$sortField = 'area_id';
		$sortOrder = 'asc';
		$perms = $this->userlib->get_permissions(0, -1, $sortField, $sortOrder, $find, $type, $this->lua , $areaId);
		$this->view->assign_by_ref('perms', $perms);
		$this->view->assign('find', $find);
		$this->view->assign('type', $type);

		//Get all groups of system
		$groups = $this->userlib->get_groups(0, -1, 'group_define_name', 'ASC', '', $this->lua);
		$this->view->assign('groups', $groups);

		// Get infos about current selected group
		$groupProperties = $this->userlib->get_group_info($groupId, $this->lua);
		$this->view->assign_by_ref('group_info', $groupProperties);

		// disallow robots to index page:
		$this->view->assign('metatag_robots', 'NOINDEX, NOFOLLOW');

		// Display the template
		$this->view->assign('mid', 'project/admin/index.tpl');
		$this->view->assign('subMid', 'project/admin/assignpermission.tpl');
		$this->view->assign('Groups', 'active');
		$this->view->display($this->layout);

	}
}