<?php
namespace Controller\Project;

require_once 'class/project.php';
require_once 'lib/Date/date.php';
require_once 'Form/Project/Edit.php';

class Admin extends \Controller\Controller
{
	public $pageId = 'project_admin';
	public $defaultSuccessForward = 'project/admin/index';
	public $defaultFailedForward = 'project/admin/index';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		isset($_REQUEST['project_id']) ? $this->projectId = $_REQUEST['project_id'] : null;
		$this->project = \project::_factory($this->projectId);
		$this->view->assign('project_id', $this->projectId);
	}
	
	/**
	 *
	 */
	public function displayAction()
	{
		// Display the template
		\View\Helper\MainTabBar::get()->getTab('project')->activate();
		$this->view->assign('mid', 'project/admin/index.tpl');
		$this->view->display($this->layout);
	}
}