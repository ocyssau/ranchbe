<?php
namespace Controller\Project;

require_once 'class/project.php';

class Admingroups extends \Controller\Controller
{
	public $pageId = 'project_admingroups';
	public $defaultSuccessForward = 'project/admingroups/display';
	public $defaultFailedForward = 'project/admingroups/display';
	
	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		
		isset($_REQUEST['project_id']) ? $this->projectId = $_REQUEST['project_id'] : null;
		
		$this->project = \project::_factory($this->projectId);
		
		$projectInfos = $this->project->GetProjectInfos($projectId);
		$projectAreaId = $projectInfos['area_id'];
		
		$this->view->assign('area_id', $projectAreaId );
		$this->view->assign('project_id', $this->projectId );
		
		$this->groupId = $_REQUEST['group'];
		$this->user = \Ranchbe::getCurrentUser();
		$this->userlib = \Ranchbe::getUserlib();
		$this->lua = \Ranchbe::getLiveUserAdmin();
		$areaId = $this->project->AREA_ID;
		
		\View\Helper\MainTabBar::get()->getTab('project')->activate();
	}
	
	/**
	 *
	 */
	public function displayAction()
	{
		//Get all groups of system
		$groups = $this->userlib->get_groups(0, -1, 'group_define_name', 'ASC', '', $this->lua);
		$this->view->assign('users', $groups);
		
		// Display the template
		$this->view->assign('group', $groupId);
		$this->view->assign('mid', 'project/admin/index.tpl');
		$this->view->assign('subMid', 'project/admingroups/display.tpl');
		$this->view->assign('Groups', 'active');
		$this->view->display($this->layout);
	}
}