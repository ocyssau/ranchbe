<?php
namespace Controller\Project;

use Rbplm\Dao\Filter\Op;

require_once 'class/common/space.php';

require_once 'HTML/QuickForm.php'; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once 'GUI/GUI.php';

use space;
use Rbplm\Org\Project;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Sys\Date as DateTime;
use Rbs\Org\Project\History as ProjectHistory;


class History extends \Controller\Container\History
{
	public $pageId = 'project_history';
	public $defaultSuccessForward = 'project/history/index';
	public $defaultFailedForward = 'project/history/index';

	/**
	 *
	 */
	public function init()
	{
		\Controller\Controller::init();
		$spaceName = 'project';

		$input=$_REQUEST;
		isset($input['project_id']) ? $projectId = $input['project_id'] : null;
		isset($input['projectid']) ? $projectId = $input['projectid'] : null;

		$this->factory = DaoFactory::get();
		$dao = $this->factory->getDao(Project::$classId);

		if( $projectId ){
			$this->object = new Project();
			$dao->loadFromId($this->object, $projectId);
		}
		else{
			$this->object = Project::init();
		}

		$this->space = new \space($spaceName);
		$this->checkFlood = check_flood($this->pageId);

		$this->view->assign('containerid' , $projectId);
		$this->view->assign('space' , $spaceName);

		$this->view->assign('number_map', $this->object->getName());
		$this->view->assign('id_map', $this->object->getId());
		$this->view->assign('description_map', $this->object->description);
		$this->view->assign('state_map', $this->object->state);
		$this->view->assign('indice_map', $this->object->indiceId);
		$this->view->assign('father_map_id', $this->object->parentId);
		$this->view->assign('CONTAINER_TYPE', $spaceName);
		$this->view->assign('CONTAINER_NUMBER' , $spaceName . '_number' );
		$this->view->assign('CONTAINER_DESCRIPTION' , $spaceName . '_description' );
		$this->view->assign('CONTAINER_STATE' , $spaceName . '_state' );
		$this->view->assign('CONTAINER_ID' , $spaceName . '_id' );
		$this->view->assign('page_title' , $spaceName.' history' );
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		//rbinit_tab();
		//rbinit_pathmap();
		parent::initService();
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$history = new ProjectHistory();
		$factory = $this->factory;
		$dao = $factory->getDao($history);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->sort('action_date', 'DESC');
		$filter->select(array('*'));

		$containerId = $this->object->getId();
		if($containerId){
			$filter->andfind( $containerId, $dao->toSys('id'), Op::OP_EQUAL );
		}

		$filterForm = new \Form\Container\History\FilterForm($dao, $this->view, $this->pageId);
		$filterForm->bind($filter)->save();

		$list = $factory->getList($history->cid);
		$list->load($filter);
		$count = $list->countAll($filter);

		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$paginator->setMaxLimit($count);
		$paginator->bind($filter)->save();

		$this->view->assign('list', $list->toArray());
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());

		$this->view->assign('mid', 'project/history.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function deleteAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		$history = new ProjectHistory();
		$factory = $this->factory;
		$dao = $factory->getDao(ProjectHistory::$classId);

		$input=$_REQUEST;
		isset($input['histo_order']) ? $actionIds = $input['histo_order'] : null;

		if(!empty($actionIds)){
			foreach ( $actionIds as $id){
				$dao->deleteFromId($id);
			}
		}
		$this->redirect($this->ifSuccessForward);
	}

} //End of class

