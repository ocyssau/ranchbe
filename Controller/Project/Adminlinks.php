<?php
namespace Controller\Project;

use Rbplm\Org\Project;
use Rbs\Sys\Session;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;

abstract class Adminlinks extends \Controller\Container\Adminlinks
{
	public $linkClassId;
	public $childClassId;
	public $addTemplate;
	public $displayTemplate;

	/**
	 */
	public function init()
	{
		\Controller\Controller::init();
		isset($_REQUEST['projectid']) ? $projectId = $_REQUEST['projectid'] : null;
		isset($_REQUEST['areaid']) ? $projectAreaId = $_REQUEST['areaid'] : null;
		isset($_REQUEST['pageid']) ? $pageId = $_REQUEST['pageid'] : $pageId=$this->pageId;

		$this->projectId = $projectId;
		$this->areaId = null;
		$this->checkFlood = \check_flood($pageId);
		$this->spaceName = 'default';

		$this->view->assign('projectid', $projectId);
		$this->view->assign('linkid', $linkId);
		$this->view->assign('projectareaid', $projectAreaId );

		\View\Helper\MainTabBar::get($this->view)->getTab('project')->activate();
	}

	/**
	 *
	 */
	public function linkdefaultprocessAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'][0] :  $checked = null;

		if ( $_REQUEST['flag'] == 'validate' ){
			$this->project->linkDefaultProcess($this->projectId , $checked);
			$msg = 'link process success';
			$level='Info';
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		$processManager = new \Workflow\Manager\Process( \Ranchbe::getDb() );
		$offset = 0;
		$maxRecords = -1;
		$sortMode = 'lastModif_desc';
		$find = '';
		$where = '';
		$items = $processManager->listProcesses($offset, $maxRecords, $sortMode, $find, $where);
		$this->view->assign('items',$items['data']);

		$this->view->assign('mid','project/link/addprocess.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linkdoctypeAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$doctype = new \doctype;
		$params = array();
		isset($_REQUEST['link_id']) ? $checked = $_REQUEST['link_id'] :  $checked = array();

		if( isset($_REQUEST['flag']) ){
			foreach ($checked as $doctypeId){
				if(!$this->project->LinkDoctype($doctypeId)){
					$erreur = true;
				}
			}
			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link doctype success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		}

		$list = $doctype->GetAll($params);
		$this->view->assign('list', $list);
		$this->view->assign('mid','project/link/adddoctype.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linkbibAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$LOManager = \container::_factory('bookshop');
		$params = array();
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();

		if ( $_REQUEST['flag'] == 'validate' ){
			foreach ($checked as $bookshopId) {
				if(!$this->project->LinkBookshop($this->projectId , $bookshopId )){
					$erreur = true;
				}
			}
			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link mockup success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		//Get list of mockup
		$list = $LOManager->GetAll($params);
		$this->view->assign('list', $list);
		$this->view->assign('HeaderCol1', 'Number');
		$this->view->assign('HeaderCol2', 'Description');
		$this->view->assign('id', 'bookshop_id');
		$this->view->assign('col1', 'bookshop_number');
		$this->view->assign('col2', 'bookshop_description');

		$this->view->assign('mid','project/link/addlink.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linklibAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$LOManager = \container::_factory('cadlib');
		$params = array();
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();

		if ( $_REQUEST['flag'] == 'validate' ){
			foreach ($checked as $cadlibId) {
				if(!$this->project->LinkCadlib($this->projectId , $cadlibId )){
					$erreur = true;
				}
			}
			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link mockup success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		//Get list of mockup
		$list = $LOManager->GetAll($params);
		$this->view->assign('list', $list);
		$this->view->assign('HeaderCol1', 'Number');
		$this->view->assign('HeaderCol2', 'Description');
		$this->view->assign('id', 'cadlib_id');
		$this->view->assign('col1', 'cadlib_number');
		$this->view->assign('col2', 'cadlib_description');

		$this->view->assign('mid','project/link/addlink.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linkmockupAction()
	{
		$this->checkRight('edit', $this->areaId, 0);
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();

		$mockupsManager = \container::_factory('mockup');
		$params = array();

		if ( $_REQUEST['flag'] == 'validate' ){
			foreach ($checked as $mockupId) {
				if(!$this->project->LinkMockup($this->projectId , $mockupId )){
					$erreur = true;
				}
			}
			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link mockup success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		//Get list of mockup
		$list = $mockupsManager->GetAll($params);
		$this->view->assign('list', $list);
		$this->view->assign('HeaderCol1', 'Number');
		$this->view->assign('HeaderCol2', 'Description');
		$this->view->assign('id', 'mockup_id');
		$this->view->assign('col1', 'mockup_number');
		$this->view->assign('col2', 'description');

		$this->view->assign('mid','project/link/addlink.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linkpartnerAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$partnersManager = new \partner;
		$params = array();
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();


		if ( $_REQUEST['flag'] == 'validate' ){
			foreach ($checked as $LO_id) {
				if(!$this->project->LinkPartner($this->projectId , $LO_id )){
					$erreur = true;
				}
			}

			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link partner success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		//Get list of partners
		$list = $partnersManager->GetAllPartners($params);
		$this->view->assign('list', $list);
		$this->view->assign('HeaderCol1', 'Number');
		$this->view->assign('HeaderCol2', 'Description');
		$this->view->assign('id', 'partner_id');
		$this->view->assign('col1', 'partner_number');
		$this->view->assign('col2', 'description');

		$this->view->assign('mid','project/link/addlink.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function linkworkitemAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$workitemsManager = \container::_factory('workitem');
		$params = array();
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();


		if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
			foreach ($checked as $workitemId){
				$workitemsManager->init($workitemId);
				if(!$workitemsManager->LinkProject($this->projectId)){
					$erreur = true;
				}
			} //End of loop

			if(isset($erreur)) {
				$msg = 'An error is occured';
				$level='Error';
			}
			else {
				$msg = 'link workitem success';
				$level='Info';
			}
			$this->errorStack->push(ERROR, $level, array(), $msg);
			$this->errorStack->checkErrors(array('close_button'=>true));
			$this->view->display($this->layout);
			die;
		} //End of validate the request

		//Get list of workitems
		$list = $workitemsManager->GetAll($params);
		$this->view->assign('list', $list);

		$this->view->assign('HeaderCol1', 'Number');
		$this->view->assign('HeaderCol2', 'Description');
		$this->view->assign('id', 'workitem_id');
		$this->view->assign('col1', 'workitem_number');
		$this->view->assign('col2', 'workitem_description');

		$this->view->assign('message', 'Be careful : this workitems are maybe linked to anothers projects<br />
			If you link it to this project, he will be unlink from the original project.<br />
			');

		$this->view->assign('mid','project/link/addlink.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$this->checkRight('edit', $this->areaId, 0);

		$checked = $_REQUEST['checked'];

		foreach ($checked as $type => $values){
			foreach ( $values as $linkObjectId ){
				$this->project->RemoveLink( $this->projectId, $linkObjectId , $type);
			}
		}
		$this->redirect($this->ifSuccessForward, array('project_id'=>$this->projectId));
	}

	/**
	 *
	 */
	public function __displayAction()
	{
		$this->checkRight('read', 1, 0);

		$objectlist = $this->project->GetLinkChildsDetail($this->projectId);
		$list = array();

		//Get detail on each object
		if(is_array($objectlist)){
			foreach($objectlist as $object){
				$type = $object['object_class'];
				switch ( $type ) {
					case 'workitem':
						$list[] = array ( 'number' => $object['workitem_number'] , 'type' => $type , 'id' => $object['workitem_id']);
						break;
					case 'project':
						$list[] = array ( 'number' => $object['project_number'] , 'type' => $type , 'id' => $object['project_id']);
						break;
					case 'partner':
						$list[] = array ( 'number' => $object['partner_number'] , 'type' => $type , 'id' => $object['partner_id']);
						break;
					case 'mockup':
						$list[] = array ( 'number' => $object['mockup_number'] , 'type' => $type , 'id' => $object['mockup_id']);
						break;
					case 'doctype':
						$list[] = array ( 'number' => $object['doctype_number'] , 'type' => $type , 'id' => $object['doctype_id']);
						break;
					case 'process':
						$list[] = array ( 'number' => $object['name'].$object['version'] , 'type' => $type , 'id' => $object['pId']);
						break;
					case 'cadlib':
						$list[] = array ( 'number' => $object['cadlib_number'] , 'type' => $type , 'id' => $object['cadlib_id']);
						break;
					case 'bookshop':
						$list[] = array ( 'number' => $object['bookshop_number'] , 'type' => $type , 'id' => $object['bookshop_id']);
						break;
					default:
						print 'none manager for '. $type;
				}
			}
		}

		$this->view->assign('HeaderCol1', 'Type');
		$this->view->assign('HeaderCol2', 'Number');
		$this->view->assign('HeaderCol3', 'Id');
		$this->view->assign('id', 'id');
		$this->view->assign('col1', 'type');
		$this->view->assign('col2', 'number');
		$this->view->assign('col3', 'id');
		$this->view->assign('list', $list);

		// Display the template
		$this->view->assign('group', $groupId);
		$this->view->assign('mid', 'project/adminlinks/display.tpl');
		$this->view->display($this->layout);
	}
}

