<?php
namespace Controller\Container;

class Mockup extends Container
{
	public $pageId = 'container_mockup';
	public $defaultSuccessForward = 'ged/mockup/index';
	public $defaultFailedForward = 'ged/mockup/index';

	public $spaceName = 'mockup';
	public $tabName = 'mockupsTab';

	protected function getEditForm($factory)
	{
		if(!$this->editForm){
			$this->editForm = new \Form\Container\MockupEditForm($this->view, $factory);
		}
		return $this->editForm;
	}

}