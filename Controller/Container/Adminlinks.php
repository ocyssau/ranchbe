<?php
namespace Controller\Container;

use Rbs\Space\Factory as DaoFactory;

abstract class Adminlinks extends \Controller\Controller
{

	public $linkClassId;
	public $childClassId;
	public $addTemplate;
	public $displayTemplate;

	abstract protected function _getModel();

	/**
	 */
	public function init()
	{
		parent::init();
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : $containerId=null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;
		$this->areaId = null;

		$pageId = $this->pageId;
		$this->checkFlood = \check_flood($pageId);

		$this->view->assign('spacename', $spaceName);
		$this->view->assign('containerid', $this->containerId);
		$this->view->assign('linkid', $linkId);

		\View\Helper\MainTabBar::get($this->view);
	}

	/**
	 *
	 */
	public function addlinkAction($filter=null, $bind=null)
	{
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] :  $checked = array();
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] :  $validate = false;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] :  $cancel = false;

		if($cancel){
			$this->redirect($this->ifSuccessForward, array('containerid'=>$this->containerId, 'spacename'=>$this->spaceName));
		}

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao($this->childClassId);
		$lnkDao = $factory->getDao($this->linkClassId);

		if ( $validate ){
			foreach ( $checked as $propertyId ){
				try{
					$link = $this->_getModel();
					$link->parentId = $this->containerId;
					$link->childId = $propertyId;
					$lnkDao->insert($link);
				}
				catch(\Exception $e){
					$this->errorStack()->error($e->getMessage());
					continue;
				}
			}
			$this->redirect($this->ifSuccessForward, array('containerid'=>$this->containerId, 'spacename'=>$this->spaceName));
		}
		else{
			if(!$filter){
				$filter = new \Rbs\Dao\Sier\Filter('', false);
			}
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = "`$asSys` AS `$asApp`";
			}
			$filter->select($select);

			$list = $factory->getList($this->childClassId);
			$list->load($filter, $bind);
			$this->view->assign('list', $list->toArray());
			$this->view->assign('mid',$this->addTemplate);
			$this->view->display($this->layout);
		}
	}

	/**
	 *
	 */
	public function unlinkAction()
	{
		isset($_REQUEST['checked']) ? $linkIds = $_REQUEST['checked'] : $linkIds=array();
		isset($_REQUEST['checked']) ? $linkIds = $_REQUEST['checked'] : $linkIds=array();

		$this->checkRight('admin', $this->areaId, $this->containerId);

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$lnkDao = $factory->getDao($this->linkClassId);


		foreach ( $linkIds as $linkId ){
			$lnkDao->deleteFromId($linkId);
		}

		$this->redirect($this->ifSuccessForward, array('containerid'=>$this->containerId, 'spacename'=>$this->spaceName));
	}

	/**
	 *
	 */
	public function displayAction()
	{
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao($this->childClassId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'id');

		if(!$list){
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = "`child`.`$asSys` AS `$asApp`";
			}

			$lnkDao = $factory->getDao($this->linkClassId);
			$list = $lnkDao->getChildren($this->containerId, $select, $filter->__toString())->fetchAll();
		}
		$this->view->assign('list', $list);

		// Display the template
		$template = $this->displayTemplate;
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}
}
