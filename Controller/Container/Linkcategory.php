<?php
namespace Controller\Container;

use Rbs\Org\Container\Link\Category as CategoryLink;

class Linkcategory extends \Controller\Container\Adminlinks
{

	public $pageId = 'container_linkcategory';
	public $defaultSuccessForward = 'container/linkcategory/display';
	public $defaultFailedForward = 'container/linkcategory/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		$this->addTemplate = 'container/link/addcategory.tpl';
		$this->displayTemplate = 'container/link/categories.tpl';

		$this->linkClassId = CategoryLink::$classId;
		$this->childClassId = \Rbplm\Ged\Category::$classId;
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller\Container.Adminlinks::_getModel()
	 */
	protected function _getModel()
	{
		return new CategoryLink();
	}

}
