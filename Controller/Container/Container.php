<?php
namespace Controller\Container;

use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Org\Container\Favorite;
use Rbplm\Org\Workitem;
use Rbplm\People\CurrentUser;

abstract class Container extends \Controller\Controller
{
	public $spaceName = 'container';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		$this->factory = DaoFactory::get($this->spaceName);
		$this->view->assign('spacename', $this->spaceName);
		$this->areaId = 1;

		//Prevent reused old POST data
		$this->checkFlood = check_flood($this->pageId);

		\View\Helper\MainTabBar::get()->getTab($this->spaceName)->activate();
	}

	/**
	 *
	 */
	abstract protected function getEditForm($factory);

	/**
	 *
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $containerIds = $_REQUEST['checked'] : $containerIds=array();
		isset($_REQUEST['containerid']) ? $containerIds = array($_REQUEST['containerid']) : null;

		if (empty($containerIds)){
			throw new \Exception('You must select at least one item', \Rbplm\Sys\Error::ERROR);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$factory = $this->factory;
		$spaceName = $this->spaceName;
		$areaId = $factory->getId();
		$this->checkRight('delete', $this->areaId, 0);

		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao(Workitem::$classId);

		foreach ($containerIds as $containerId){
			try{
				$dao->deleteFromId($containerId);
			}
			catch(\PDOException $e){
				$this->errorStack()->error("CONTAINER MUST BE EMPTY TO BE DELETE");
				$this->errorStack()->error($e->getMessage());
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
			}
		}

		$this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function editAction()
	{
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['id']) ? $containerId = $_REQUEST['id'] : null;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate=false;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('edit', $this->areaId, 0);

		$factory = $this->factory;
		$spaceName = $this->spaceName;
		$currentUser = CurrentUser::get();
		$areaId = $factory->getId();
		$this->checkRight('edit', $this->areaId, 0);

		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao(Workitem::$classId);
		$extendedLoader = new Extended\Loader($factory);
		$extendedLoader->load($container->cid, $dao);
		$dao->loadFromId($container,$containerId);

		$form = $this->getEditForm($factory);
		$form->getElement('number')->setAttribute('disabled', true);

		//SET EXTENDED TO FORM
		if($dao->extended){
			$form->setExtended($dao->extended);
		}
		$form->bind($container);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($container);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}

		$form->prepareRenderer();
		$this->view->pageTitle = 'Edit Container '.$container->getName();
		$this->view->assign('mid', $form->template);
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('create', $this->areaId, 0);

		$factory = $this->factory;
		$spaceName = $this->spaceName;
		$currentUser = CurrentUser::get();
		$areaId = $factory->getId();
		$this->checkRight('edit', $this->areaId, 0);

		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao(Workitem::$classId);
		$extendedLoader = new Extended\Loader($factory);
		$extendedLoader->load($container->cid, $dao);

		$forseenCloseDate = new \DateTime();
		$forseenCloseDate->add(new \DateInterval('P5Y')); //5 years

		$container->setForseenCloseDate($forseenCloseDate);
		$container->setUid(uniqId());
		$container->description='New workitem';

		$form = $this->getEditForm($factory);

		/* SET EXTENDED TO FORM */
		if($dao->extended){
			$form->setExtended($dao->extended);
		}

		$form->bind($container);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$basePath = \Ranchbe::get()->getConfig('path.reposit.'.$this->spaceName);
					$wiName = $container->getUid();
					$path = $basePath.'/'.$wiName;
					$container->path = $path;
					$container->setName($wiName);

					try {
						$reposit = \Rbplm\Vault\Reposit::init($path);
						$dao->save($container);
					}
					catch (\Exception $e) {
						$this->errorStack()->error("Creation of container FAILED :" . $e->getMessage());
					}

					$dao->save($container);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->display($this->layout);
	}

	/**
	 */
	public function renameAction()
	{
		isset($_REQUEST['checked']) ? $containerIds = $_REQUEST['checked'] : null;
		isset($_REQUEST['newName']) ? $targetName = $_REQUEST['newName'] : null;

		if (empty($containerIds)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
			return false;
		}

		$this->checkRight('admin', $this->areaId, 0);

		$areaId = $this->container->AREA_ID;
		$this->checkRight('admin', $this->areaId, 0);

		$containerId = $containerIds[0];
		$this->container->init($containerId);

		$form = new \Form\AbstractForm('frmTest', 'POST');
		$form->setView($this->view);
		$form->addElement('hidden', 'checked[]', $container_id);
		$form->addElement('hidden', 'action', 'rename');
		$form->addElement('header', null, tra('Rename container ').' '.$this->container->getProperty('container_number'));
		$form->addElement('text', 'newName', tra('New name'));
		$form->addRule('newName', tra('Required'), 'required', null, 'client');
		$mask = constant('DEFAULT_'. strtoupper($spaceName) . '_MASK');
		if($mask){
			$form->addRule('newName', 'This number is not valid', 'regex', "/$mask/" , 'server');
		}

		if ($form->validate()){ /*Validate the form...*/
			$form->freeze(); /*...and freeze it */
			$targetName = str_replace('/', '_', $targetName);
			$targetName = str_replace('\\', '_', $targetName);
			$targetName = str_replace('.', '_', $targetName);
			$toPath = $this->container->DEFAULT_DEPOSIT_DIR .'/'. $targetName;
			$ok = $this->container->move($toPath);
			if( $ok ){
				if( strstr($this->container->GetProperty('default_file_path'),  '__imported' ) ){
					$toPath = $toPath . '/__imported';
				}
				$data = array($this->container->GetFieldName('number')=>$targetName, 'default_file_path'=>$toPath);
				$this->container->BasicUpdate( $data, $this->container->GetId() );
			}
			else{
				$this->errorStack->push(ERROR, 'Fatal', array(), 'Error during move' );
			}
		}
		else{
			$form->addElement('submit', null, 'GO');
			$form->applyFilter('__ALL__', 'trim');
		}

		$form->display();
		$this->errorStack->checkErrors(array('close_button'=>true));
		die;
	}

	/**
	 *
	 */
	public function getstatsAction()
	{
		$form = new \Form\Container\Stat($this->view, $this->container);
		$form->prepareRenderer();

		$this->checkRight('read', $this->areaId, 0);


		$pathQueriesScripts = PATH_STAT_QUERY;

		if($form->validate()){
			$graphs = array();
			foreach($form->getElementValue('statistics_queries') as $script){
				include $pathQueriesScripts.'/'.$script;
				$result = call_user_func(basename($script, '.php'), $this->container);
				if(is_array($result)){
					$graphs = array_merge($result, $graphs);
				}
			}
		}

		$this->view->assign('PageTitle' , 'Stats of ' . $spaceName .' '. $this->container->GetName());
		//Assign name to particular fields
		$this->view->assign('graphs', $graphs);
		$this->view->assign('CONTAINER_TYPE', $spaceName);
		$this->view->assign('CONTAINER_NUMBER' , $spaceName . '_number' );
		$this->view->assign('CONTAINER_DESCRIPTION' , $spaceName . '_description' );
		$this->view->assign('CONTAINER_STATE' , $spaceName . '_state' );
		$this->view->assign('CONTAINER_ID' , $spaceName . '_id' );

		// Display the template
		$this->view->assign('randWindowName', 'container_'.uniqid());
		$this->view->assign('mid', 'container/stats.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function addpreferedAction()
	{
		$factory = $this->factory;

		isset($_REQUEST['checked']) ? $containerIds = $_REQUEST['checked'] : null;
		isset($_REQUEST['containerid']) ? $containerIds = array($_REQUEST['containerid']) : null;

		$dao = $factory->getDao(\Rbplm\Org\Workitem::$classId);
		$favoriteDao = $factory->getDao(Favorite::$classId);

		$spaceId = $factory->getId();
		$spaceName = $factory->getName();
		$userId = CurrentUser::get()->getId();

		$this->checkRight('read', $this->areaId, 0);

		$bind = array(
			':ownerId'=>$userId,
			':spaceId'=>$spaceId,
			':spaceName'=>$spaceName
		);

		if (empty($containerIds)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
		}

		if( !is_array($containerIds) ){
			$containerIds = array($containerIds);
		}

		foreach ($containerIds as $id){
			$qstmt = $dao->query(array($dao->toSys('uid')), $dao->toSys('id').'=:id', array(':id'=>$id));
			$bind[':containerId'] = $id;
			$bind[':containerNumber'] = $qstmt->fetch(\PDO::FETCH_NUM)[0];
			try{
				$favoriteDao->add($bind);
			}
			catch(\Exception $e){
				$errors[]=$e->getMessage();
			}
		}

		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function removepreferedAction()
	{
		$factory = $this->factory;

		isset($_REQUEST['checked']) ? $containerIds = $_REQUEST['checked'] : null;
		isset($_REQUEST['containerid']) ? $containerIds = array($_REQUEST['containerid']) : null;

		$favoriteDao = $factory->getDao(Favorite::$classId);
		$spaceId = $factory->getId();
		$userId = CurrentUser::get()->getId();

		$this->checkRight('read', $this->areaId, 0);

		if (empty($containerIds)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
		}

		if( !is_array($containerIds) ){
			$containerIds = array($containerIds);
		}

		foreach ($containerIds as $id){
			$favoriteDao->remove($userId, $id, $spaceId);
		}

		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 *
	 */
	public function archiveAction()
	{
		if(!$this->checkFlood) return false;

		isset($_REQUEST['checked']) ? $containerIds = $_REQUEST['checked'] : null;
		isset($_REQUEST['containerid']) ? $containerIds = array($_REQUEST['containerid']) : null;
		isset($_REQUEST['validate_archive']) ? $validate = $_REQUEST['validate_archive'] : null;

		if (empty($containerIds)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
			return false;
		}

		$this->checkRight('admin', $this->areaId, 0);

		if($validate){
			foreach($containerIds as $containerId){
				$this->container->init($containerId);
				$archiver = new \Container_Archiver( $this->container );
				$archiver->archive();
			}
			return true;
		}
		else{ //Display validation
			foreach ($containerIds as $containerId){
				$list[] = $this->container->GetInfos($containerId);
			}
			$this->view->assign_by_ref('list' , $list);
			$this->view->assign('documentManage' , 'active');
			$this->view->assign('confirmation_text' , 'Are you sure that you want archive this containers');
			$this->view->assign('confirmation_return' , 'validate_archive');
			$this->view->assign('confirmation_formaction' , $spaceName . 's.php');
			$this->view->assign('confirmation_action' , $_REQUEST['action']);
			$this->view->assign('mid', 'container/confirm.tpl');
			$this->view->display('layouts/ranchbe.tpl');
			die;
		}
	}

}