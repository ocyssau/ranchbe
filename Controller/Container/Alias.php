<?php
namespace Controller\Container;

use Ranchbe;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Org\Container\Alias as ContainerAlias;
use Rbplm\Org\Workitem;

class Alias extends \Controller\Controller
{

	public $pageId = 'container_workitemAlias';

	public $defaultSuccessForward = 'container/alias/';

	public $defaultFailedForward = 'container/alias/';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		// Prevent reused old POST data
		$this->checkFlood = check_flood($this->pageId);

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName = null;
		$this->spaceName = $spaceName;

		$this->ifSuccessForward = "ged/$spaceName/index";
		\View\Helper\MainTabBar::get()->getTab($spaceName)->activate();
	}

	/**
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $aliasIds = $_REQUEST['checked'] : $aliasIds = array();
		isset($_REQUEST['aliasid']) ? $aliasIds[] = $_REQUEST['aliasid'] : null;
		$spaceName = $this->spaceName;

		$factory = DaoFactory::get($spaceName);
		$areaId = $factory->getId();
		$this->checkRight('admin', $this->areaId, 0);

		$alias = new ContainerAlias();
		$aliasDao = $factory->getDao($alias->cid);

		if ( empty($aliasIds) ) {
			throw new \Exception('You must select at least one item');
		}

		foreach( $aliasIds as $aliasId ) {
			$aliasDao->deleteFromId($aliasId);
		}

		return $this->redirect($this->ifSuccessForward);
	}

	/**
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;

		$spaceName = $this->spaceName;

		if ( $cancel ) {
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('create', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$areaId = $factory->getId();

		$container = $factory->getModel(Workitem::$classId);
		$alias = new ContainerAlias();
		$aliasDao = $factory->getDao($alias->cid);

		$defaults = array();

		$form = new \Form\Container\Alias\EditForm($this->view, $factory);
		$form->bind($alias);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$alias->setUid($alias->getUid() . '_alias');
					$aliasDao->save($alias);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->pageTitle = 'Create Alias';
		$this->view->display($this->layout);
	}

	/**
	 */
	public function editAction()
	{
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['aliasid']) ? $aliasId = $_REQUEST['aliasid'] : null;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;

		$spaceName = $this->spaceName;

		if ( $cancel ) {
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get($spaceName);
		$areaId = $factory->getId();
		$this->checkRight('edit', $this->areaId, 0);

		$alias = new ContainerAlias();
		$dao = $factory->getDao($alias->cid);
		$dao->loadFromId($alias, $aliasId);

		$form = new \Form\Container\Alias\EditForm($this->view, $factory);
		$form->bind($alias);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($alias);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->pageTitle = 'Edit Alias';
		$this->view->assign('mid', $form->template);
		$this->view->display($this->layout);
	}
}