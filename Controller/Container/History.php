<?php
namespace Controller\Container;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Org\Container\History as ContainerHistory;
use Rbplm\Dao\Filter\Op;

class History extends \Controller\Controller
{
	public $pageId = 'container_history';
	public $defaultSuccessForward = 'container/history/index';
	public $defaultFailedForward = 'container/history/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : $containerId=null;

		$this->checkFlood = check_flood($this->pageId);
		$this->spaceName = $spaceName;
		$this->containerId = $containerId;

		$this->ifSuccessForward = "container/$spaceName/display";
		$this->view->assign('spacename', $spaceName);

		\View\Helper\MainTabBar::get()->getTab($spaceName)->activate();
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$spaceName = $this->spaceName;

		$factory = DaoFactory::get($spaceName);
		$areaId = $factory->getId();
		$this->checkRight('read', $this->areaId, 0);

		$history = new ContainerHistory();
		$dao = $factory->getDao($history->cid);
		$list = $factory->getList($history->cid);
		$table = $factory->getTable($history->cid);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->select(array('*'));

		if($containerId){
			$filter->andfind( $containerId, 'workitem_id', Op::OP_EQUAL );
		}

		$filterForm = new \Form\Container\History\FilterForm($this->view, $factory, $this->pageId);
		$filterForm->bind($filter)->save();

		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$paginator->orderBy = 'action_id';
		$count = $list->countAll('1=1');
		$paginator->setMaxLimit($count);
		$paginator->bind($filter)->save();

		//Select main table
		$select = array(
			$dao->toSys('id') . ' as action_id',
			$dao->toSys('action_name') . ' as action_name',
			$dao->toSys('action_ownerId') . ' as action_ownerId',
			$dao->toSys('action_created') . ' as action_created',
			'workitem_id as data_id',
			'workitem_number as data_name',
			'workitem_description as data_description',
			'workitem_state as data_status',
			'workitem_indice_id as data_versionId',
		);
		$filter->select($select);

		$list->load($filter);
		$paginator->bindToView($this->view);
		$this->view->pageTitle = 'History Of '. $factory->getName();
		$this->view->assign('list', $list->toArray());
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('mid', 'container/history.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function deleteAction()
	{
		if(!empty($_REQUEST['histo_order'])){
			$this->checkRight('admin', $this->areaId, 0);
			foreach ( $_REQUEST['histo_order'] as $id){
				$this->history->RemoveHistorySingle($id);
			}
		}
		$this->redirect($this->ifSuccessForward, array('space'=>$this->space->getName()));
	}

} //End of class

