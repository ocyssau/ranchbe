<?php
namespace Controller\Container;

class Workitem extends Container
{
	public $pageId = 'container_workitem';
	public $defaultSuccessForward = 'ged/workitem/index';
	public $defaultFailedForward = 'ged/workitem/index';

	public $spaceName = 'workitem';
	public $tabName = 'workitemsTab';

	protected function getEditForm($factory)
	{
		if(!$this->editForm){
			$this->editForm = new \Form\Container\WorkitemEditForm($this->view, $factory);
		}
		return $this->editForm;
	}
}
