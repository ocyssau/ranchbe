<?php
namespace Controller\Container;

use Rbs\Org\Container\Link\Doctype as DoctypeLink;

class Linkdoctype extends \Controller\Container\Adminlinks
{

	public $pageId = 'container_linkdoctype';
	public $defaultSuccessForward = 'container/linkdoctype/display';
	public $defaultFailedForward = 'container/linkdoctype/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		$this->addTemplate = 'container/link/adddoctype.tpl';
		$this->displayTemplate = 'container/link/doctypes.tpl';

		$this->linkClassId = DoctypeLink::$classId;
		$this->childClassId = \Rbplm\Ged\Doctype::$classId;
	}

	protected function _getModel()
	{
		return new DoctypeLink();
	}
}
