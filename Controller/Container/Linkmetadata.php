<?php
namespace Controller\Container;

use Rbs\Org\Container\Link\Property as PropertyLink;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;


class Linkmetadata extends \Controller\Container\Adminlinks
{

	public $pageId = 'container_linkmetadata';
	public $defaultSuccessForward = 'container/linkmetadata/display';
	public $defaultFailedForward = 'container/linkmetadata/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		$this->addTemplate = 'container/link/addmetadata.tpl';
		$this->displayTemplate = 'container/link/metadatas.tpl';

		$this->linkClassId = PropertyLink::$classId;
		$this->childClassId = \Rbs\Extended\Property::$classId;
	}

	protected function _getModel()
	{
		return new PropertyLink();
	}

	/**
	 *
	 */
	public function addlinkAction()
	{
		$extendedCid = \Rbplm\Ged\Document\Version::$classId;
		$dao = DaoFactory::get($this->spaceName)->getDao($extendedCid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->andfind(':extendedCid', $dao->toSys('extendedCid'), Op::EQUAL);
		$bind = array(':extendedCid'=>$extendedCid);

		return parent::addlinkAction($filter, $bind);
	}
}
