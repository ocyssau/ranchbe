<?php
namespace Controller\Container;

class Cadlib extends Container
{
	public $pageId = 'container_cadlib';
	public $defaultSuccessForward = 'ged/cadlib/index';
	public $defaultFailedForward = 'ged/cadlib/index';

	public $spaceName = 'cadlib';
	public $tabName = 'cadlibsTab';

	protected function getEditForm($factory)
	{
		if(!$this->editForm){
			$this->editForm = new \Form\Container\CadlibEditForm($this->view, $factory);
		}
		return $this->editForm;
	}

}