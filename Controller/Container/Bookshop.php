<?php
namespace Controller\Container;

class Bookshop extends Container
{
	public $pageId = 'container_bookshop';
	public $defaultSuccessForward = 'ged/bookshop/index';
	public $defaultFailedForward = 'ged/bookshop/index';

	public $spaceName = 'bookshop';
	public $tabName = 'bookshopsTab';

	protected function getEditForm($factory)
	{
		if(!$this->editForm){
			$this->editForm = new \Form\Container\BookshopEditForm($this->view, $factory);
		}
		return $this->editForm;
	}

}
