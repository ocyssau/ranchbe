<?php
namespace Controller\Message;

use Rbplm\Sys\Message;
use Rbplm\People;

class Archive extends \Controller\Message\Mailbox
{

	public $pageId = 'message_archive';

	public $defaultSuccessForward = 'message/archive/index';

	public $defaultFailedForward = 'message/archive/index';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Controller.Controller::init()
	 */
	public function init()
	{
		\Controller\Controller::init();
		$this->message = new Message\Archived(Message::TYPE_ARCHIVE);
		$this->indexTemplate = 'message/archive/index.tpl';
	}

	/**
	 * Delete old messages
	 */
	public function autodeleteAction()
	{
		$userId = People\CurrentUser::get()->getUid();
		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		$days = (int)$this->params()->fromQuery('days');

		if ( $days < 1 ) {
			return $this->successForward();
		}

		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$filter = "`owner`=:user AND `date`<=:age";
		$bind = array(
			':user' => $userId,
			':age' => $age
		);

		$dao->delete($filter, $bind);

		return $this->successForward();
	}
} //End of class

