<?php
namespace Controller\Message;

use Rbplm\Sys\Message;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;

class Compose extends \Controller\Controller
{

	public $pageId = 'message_compose';

	public $defaultSuccessForward = 'message/compose/index';

	public $defaultFailedForward = 'message/compose/index';

	protected $message;

	protected $userId;

	protected $msgIds;

	/**
	 * (non-PHPdoc)
	 *
	 * @see Controller.Controller::init()
	 */
	public function init()
	{
		parent::init();

		$this->msgIds = $_REQUEST['msg'];

		\View\Helper\MainTabBar::get()->getTab('home')->activate();
	}

	/**
	 */
	public function indexAction()
	{
		foreach( array(
			'to',
			'cc',
			'bcc'
		) as $dest ) {
			if ( is_array($this->view->$dest) ) {
				$this->view->$dest = implode(', ', array_filter($this->view->$dest, 'ctype_alnum'));
			}
		}
		$this->view->activatemail = \Ranchbe::get()->getConfig('message.mail.enable');
	}

	/**
	 * Strip Re:Re:Re: from subject
	 */
	public function replyAction()
	{
		$msgId = $this->getRequest()->getParam('msgId');
		$reply = $this->getRequest()->getParam('reply');
		$replyall = $this->getRequest()->getParam('replyall');

		$currentUser = People\CurrentUser::get();
		$message = new Message(Message::TYPE_MAILBOX);

		$msg = $message->getMessage($currentUser->getId(), $msgId);
		$subject = $msg['subject'];
		$body = $msg['body'];

		// Strip Re:Re:Re: from subject
		if ( $reply || $replyall ) {
			$subject = tra("Re:") . ereg_replace("^(" . tra("Re:") . ")+", "", $subject);
		}
	}

	/**
	 */
	public function sendAction()
	{
		isset($_POST['validate']) ? $validate = $_POST['validate'] : $validate = null;
		isset($_POST['bymessage']) ? $byMessage = $_POST['bymessage'] : $byMessage = true;
		isset($_POST['bymail']) ? $byMail = $_POST['bymail'] : $byMail = false;
		isset($_POST['toall']) ? $toAll = $_POST['toall'] : $toAll = null;

		$datas = $_POST;
		isset($_POST['subject']) ? $datas['subject'] = $_POST['subject'] : $subject = null;
		isset($_POST['body']) ? $datas['body'] = $_POST['body'] : $body = null;
		isset($_POST['priority']) ? $datas['priority'] = $_POST['priority'] : $priority = null;

		$factory = DaoFactory::get();
		$currentUser = People\CurrentUser::get();

		$message = new Message(Message::TYPE_SEND);
		$message->setOwner($currentUser);
		$message->setFrom($currentUser->getLogin(), $currentUser->getLogin());

		$message->bymail = $byMail;
		$message->bymessage = $byMessage;
		$message->toall = $toAll;

		$form = new \Form\Message\ComposeForm($this->view);
		$form->bind($message);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($datas);
			if ( $form->validate() ) {
				/* and freeze it */
				$form->freeze();

				if ( $toAll ) {
					$users = $factory->getList(\Rbplm\People\User::$classId);
					$users->load();
					foreach( $users as $user ) {
						$to = $user['login'];
						$message->addTo($to, $user['login']);
					}
				}

				try {
					if ( $byMail ) {
						// $transport = new Mail\Transport\Sendmail();
						// $transport->send($mail);
					}
					if ( $byMessage ) {
						$dao = $factory->getDao(Message::$classId);
						$dao->save($message);

						$dao = $factory->getDao(Message\Sent::$classId);
						$dao->save($message);
					}
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
			}
		}

		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Compose Message');
		$this->view->display($this->layout);
	}
} //End of class

