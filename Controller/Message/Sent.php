<?php
namespace Controller\Message;

use Rbplm\Sys\Message;

class Sent extends \Controller\Message\Mailbox
{

	public $pageId = 'message_sent';

	public $defaultSuccessForward = 'message/sent/index';

	public $defaultFailedForward = 'message/sent/index';

	/**
	 */
	public function init()
	{
		\Controller\Controller::init();
		$this->message = new Message\Sent(Message::TYPE_SEND);
		$this->indexTemplate = 'message/sent/index.tpl';
	}
} //End of class

