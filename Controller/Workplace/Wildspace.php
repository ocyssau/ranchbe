<?php
namespace Controller\Workplace;

use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Sys\Datatype\File;
use Rbplm\Sys\Fsdata;
use Rbplm\Sys\Filesystem;


class Wildspace extends \Controller\Controller
{
	public $pageId = 'workplace_wildspace';
	public $defaultSuccessForward = 'workplace/wildspace/index';
	public $defaultFailedForward = 'workplace/wildspace/index';

	/**
	 * @var \wildspace
	 */
	public $wildspace;

	/**
	 *
	 * @var boolean
	 */
	public $checkFlood;

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->user = CurrentUser::get();
		$this->wildspace = new User\Wildspace($this->user);
		$this->checkFlood = \check_flood($_REQUEST['page_id']);

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('wildspace');

		$this->view->basecamp = $this->defaultSuccessForward;
	}

	/**
	 *
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		parent::initService();
	}

	/**
	 * suppress a file in the wildspace
	 */
	public function deletefileAction()
	{
		(isset($_REQUEST['checked'])) ? $checked = $_REQUEST['checked'] : $checked=array();
		(isset($_REQUEST['file'])) ? $checked[] = $_REQUEST['file'] : null;

		if($checked){
			foreach($checked as $file){
				$this->wildspace->suppressData($file);
			}
		}

		$this->redirect($this->ifSuccessForward);
	}

	/**
	 * rename a file in the wildspace
	 */
	public function renamefileService()
	{
		(isset($_REQUEST['newname'])) ? $newName = $_REQUEST['newname'] : null;
		(isset($_REQUEST['file'])) ? $oldName = $_REQUEST['file'] : null;

		if(!$newName || !$oldName){
			$this->serviceReturn(array('Parameters are not setted'), $newName, $oldName);
			return false;
		}

		$path = $this->wildspace->getPath();
		$file = new \Rbplm\Sys\Datatype\File( $path.'/'.$oldName );
		$file->rename($path.'/'.$newName);

		$this->serviceReturn(array('newfile'=>$path.'/'.$newName));
	}

	/**
	 * copy a file from the wildspace to the wildspace
	 */
	public function copyfileService()
	{
		(isset($_REQUEST['newname'])) ? $newName = $_REQUEST['newname'] : null;
		(isset($_REQUEST['file'])) ? $oldName = $_REQUEST['file'] : null;

		if(!$newName || !$oldName){
			$this->serviceReturn(array('Parameters are not setted'), $newName, $oldName);
			return false;
		}

		$path = $this->wildspace->getPath();
		$file = new \Rbplm\Sys\Datatype\File( $path.'/'.$oldName );
		$file->copy($path.'/'.$newName, 0666, false);

		$this->serviceReturn(array('newfile'=>$path.'/'.$newName));
	}

	/**
	 * Add a file to the wildspace
	 * Input is a json array in _REQUEST[files] as
	 *          .file1.name
	 *          .file1.data.name
	 *          .file1.data.md5
	 *          .file1.data.size
	 *          .file1.data.data
	 *
	 * @param  mixed $input
	 * @return ApiProblem|mixed
	 */
	public function uploadService()
	{
		$input = $_REQUEST;
		$files = $input['files'];
		$files = json_decode(json_encode($files), false);

		/*------------- parse the document from input -------------*/
		for($i=2,$index='file1'; isset($files->$index); $index='file'.$i++)
		{
			$file1 = $files->$index;
			isset($file1->name)? $name = basename($file1->name) : $name = uniqid();
			$wsPath = $this->wildspace->getPath();

			try{
				//Put data in Vault
				$data = base64_decode($file1->data->data);
				$toPath = $wsPath .'/'. $name;
				file_put_contents($toPath, $data);
				$return['feedback'][]=array(
					'index'=>$index,
					'name'=>$name,
					'fullpath'=>$toPath
				);
			}
			catch (\Exception $e){
				$return['error'][]=array(
					'index'=>$index,
					'name'=>$name,
					'fullpath'=>$toPath,
					'function'=>__FUNCTION__,
					'code'=>5000,
					'message'=>'Error during file upload',
					'exception_code'=>$e->getCode(),
					'exception_messsage'=>$e->getMessage(),
				);
				continue;
			}
		}
		echo json_encode($return);
	}

	/**
	 * download a file from the wildspace
	 */
	public function downloadfileAction()
	{
		(isset($_REQUEST['file'])) ? $fileName = $_REQUEST['file'] : $fileName=null;

		$file = $this->wildspace->getPath().'/'.$fileName;
		$fsdata = new Fsdata($file);
		$fsdata->download();
	}

	/**
	 * put many files from the wildspace in a zip and download
	 */
	public function downloadzipAction()
	{
		$tmpFile = uniqid('tempranchbedowloadzip').'.zip';
		$files = $_REQUEST['checked'];
		$zipfile = $this->wildspace->getPath().'/'.$tmpFile;

		if( is_array($files) ){
			$zip = new \ZipArchive();
			$zip->open($zipfile, \ZipArchive::CREATE);
			foreach($files as $file){
				$zip->addFile($this->wildspace->getPath().'/'.$file, $file);
			}
			$zip->close();

			$fsdata = new Fsdata($zipfile);
			$fsdata->download();
		}
	}

	/**
	 * Uncompress files in wildspace
	 */
	public function uncompressAction()
	{
		(isset($_REQUEST['checked'])) ? $newName = $_REQUEST['newname'] : null;
		(isset($_REQUEST['file'])) ? $oldName = $_REQUEST['file'] : null;

		$files = $_REQUEST['checked'];

		if(empty($files)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
			$this->redirect('workplace/wildspace/index');
		}

		foreach ($files as $file){
			if(!is_file($this->wildspace->getPath().'/'.$file)){
				continue; //To prevent lost of data
			}

			$extension = substr($file, strrpos($file, '.'));

			if(  $extension == '.Z' ){
				if(!exec(UNZIPCMD . " $file")){
					throw new \Exception("cant uncompress $file");
				}
			}

			if( $extension == '.adraw' || $extension == '.zip' ){
				$zip = new \ZipArchive;
				if ($zip->open($this->wildspace->getPath().'/'.$file) === TRUE) {
					$zip->extractTo( $this->wildspace->getPath() );
					$zip->close();
				}
				else{
					throw new \Exception("cant uncompress $file");
				}
				break;
			}
		}
		$this->redirect($this->ifSuccessForward);
	}

	/**
	 */
	public function indexAction()
	{
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward);

		/* Filter and paginations */
		$filterForm = new \Form\Wildspace\FilterForm($this->wildspace, $this->view, $this->pageId);
		$filter = new Filesystem\Filter();
		$this->bindSortOrder($filter, 'name');
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();
		$paginator->bind($filter)->save();

		$params = array(
			'displayNew'=>$filterForm->getValue('displayNew'),
			'displayMd5'=>$filterForm->getValue('displayMd5'),
		);

		/* get infos on files */
		$getStoredInfos = true;
		$path = $this->wildspace->getPath();

		$list = User\Wildspace::getDatas($path, $filter);
		$this->view->assign('list', $list);

		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('displayMd5', $filterForm->getValue('displayMd5'));
		$this->view->assign('currentUserId', $this->user->getId());
		$this->view->assign('currentUserName', $this->user->getLogin());
		$this->view->assign('wildspacePath', $this->wildspace->getPath());
		$this->view->assign('mid', 'workplace/wildspace.tpl');
		$this->view->display('layouts/ranchbe.tpl');
	}

}//class end

