<?php
namespace Controller\Tools;

use Rbplm\Sys\Directory;
use Rbplm\Sys\Filesystem;

class Trash extends \Controller\Controller
{
	public $pageId = 'tools_trash';
	public $defaultSuccessForward = 'tools/trash/display';
	public $defaultFailedForward = 'tools/trash/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		\View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('tools');
	}

	/**
	 * Get files in Trash
	 */
	public function displayAction()
	{
		$dir = new Directory();
		$path = \Ranchbe::get()->getConfig('path.reposit.trash');
		$iterator = $dir->getIterator($path);
		$list = array();

		if($iterator){
			/* @var \SplFileInfo $file */
			foreach($iterator as $key=>$file){
				$fName = $file->getFilename();

				if($fName=='.' || $fName=='..' ){
					continue;
				}

				/* Get the original directory */
				$oriPath = Filesystem::decodePath($fName);

				/*
				$oriPath = str_replace( '%2F' , '/', $file['file_name']);
				$oriPath = str_replace( '%2E' , '.', "$oriPath");
				*/
				$oriPath = str_replace( '%&_' , '/', $oriPath);
				$oriPath = ereg_replace( '\([0-9]{1,2}\).', '.', $oriPath );
				$list[] = array(
					'original_file'=>$oriPath,
					'name' => $fName,
					'path' => $file->getPath(),
					'size' => $file->getSize(),
					'mtime' => $file->getMTime(),
				);
			}
		}

		$this->view->assign('list', $list);
		/* Display the template */
		$this->view->display('trash/display.tpl');
	}
	/**
	 * To empty the trash
	 */
	public function vacummAction()
	{
		$result = EmptyTrash();
		echo '<pre>';
		var_dump($result);
		echo '</pre>';

		echo '<pre>';
		foreach ( $result as $detail){
			echo implode ( ";" , $detail ) . '<br>';
		}
		echo '</pre>';

		die;
	}
}

