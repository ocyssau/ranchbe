<?php
namespace Controller\Tools;

class Index extends \Controller\Controller
{
	public $pageId = 'tool_index';
	public $defaultSuccessForward = 'tools/index/display';
	public $defaultFailedForward = 'tools/index/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('tools');
	}

	/**
	 *
	 */
	public function sqlrequestAction()
	{
		include('inc/sql_user_queries/sql_user_queries.php');
		$this->view->assign('parametersTab', 'active');
		$this->view->display('application/index/about.tpl');
	}

	/**
	 *
	 */
	public function phpinfoAction()
	{
		phpinfo();
		die;
	}

	/**
	 *
	 */
	public function getfilesrecordwithoutfileAction()
	{
		$container = \container::_factory('workitem');
		$filesManager = new \docfile($container->space);

		$result = $filesManager->getFilesRecordWithoutFile();
		echo '<pre>';
		var_dump($result);
		echo '</pre>';

		echo '<pre>';
		foreach ( $result as $detail){
			echo implode ( ";" , $detail ) . '<br>';
		}
		echo '</pre>';

		die;
	}

	/**
	 * Get files without documents
	 */
	public function getfileswithoutdocumentsAction()
	{
		$container = \container::_factory('workitem');
		$filesManager = new \docfile($container->space);

		$result = $filesManager->getFilesWithoutDocuments();
		echo '<pre>';
		var_dump($result);
		echo '</pre>';

		echo '<pre>';
		foreach ( $result as $detail){
			echo implode ( ";" , $detail ) . '<br>';
		}
		echo '</pre>';

		die;
	}

	/**
	 *
	 */
	public function getdocumentswithoutfileAction()
	{
		$container = \container::_factory('workitem');
		$filesManager = new \docfile($container->space);

		$result = $filesManager->GetDocumentsWithoutFile();

		echo '<pre>';
		var_dump($result);
		echo '</pre>';

		echo '<pre>';
		foreach ( $result as $detail){
			echo implode ( ";" , $detail ) . '<br>';
		}
		echo '</pre>';
		die;
	}

	/**
	 * Get documents without container
	 */
	public function getdocumentswithoutcontainerAction()
	{
		$container = \container::_factory('workitem');
		$filesManager = new \docfile($container->space);

		$result = $filesManager->GetDocumentsWithoutContainer();
		echo '<pre>';
		var_dump($result);
		echo '</pre>';

		echo '<pre>';
		foreach ( $result as $detail){
			echo implode ( ";" , $detail ) . '<br>';
		}
		echo '</pre>';

		die;
	}

	/**
	 * Update doctypes icons
	 */
	public function updatedoctypeiconAction()
	{
		$odoctype = new \doctype();
		$p['select'] = array('doctype_id','icon', 'doctype_number');
		$result = $odoctype->GetAll($p);
		if(is_array($result)){
			foreach($result as $doctype){
				$odoctype->init($doctype['doctype_id']);
				if( $odoctype->CompileIcon( $doctype['icon'] , $doctype['doctype_id']) ){
					echo 'compile icon '.$doctype['icon'].' for doctype '.$doctype['doctype_number'].'<br />';
				}
				else{
					echo 'COMPILE FAILED for icon '.$doctype['icon'].' for doctype '.$doctype['doctype_number'].'<br />';
				}
			}
		}
		echo '<b>End</b><br />';
		die;
	}

	/**
	 *
	 */
	public function backupdatabaseAction()
	{
		$dbranchbe = \Ranchbe::getDb();

		$host_ranchbe   = $dbranchbe->host;
		$user_ranchbe   = $dbranchbe->user;
		$pass_ranchbe   = $dbranchbe->password;
		$dbs_ranchbe    = $dbranchbe->database;
		$date = date("Y-m-d_H-i-s"); // On définit le variable $date ( ici son format )

		// Utilise les fonctions syst�me : MySQLdump
		if ($_REQUEST['BackupDatabaseType'] == 'XML' ){
			$backup_file = $dbs_ranchbe. '_backup_'.$date.'.xml';
			$command = DUMPCMD." --xml -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
		}
		else{
			$backup_file = $dbs_ranchbe. '_backup_'.$date.'.sql';
			$command = DUMPCMD." --opt -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
		}

		$backup = shell_exec($command);

		header("Content-disposition: attachment; filename=$backup_file");
		header("Content-Type: text/rtf");
		header("Content-Transfer-Encoding: $backup_file\n"); // Surtout ne pas enlever le \n
		header("Content-Length: " . strlen($backup) );
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		echo $backup;
		die;
	}


	/**
	 * To generate doctypes table
	 */
	public function gendoctypesAction()
	{
		echo 'INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES ' ."<br>";

		$file = "./Docs/doctypes.csv";

		$handle = fopen($file, "r");

		$linecount = count(file($file));

		$row = 1;

		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			$doctype_number = $data[0];
			$doctype_description = $data[1];
			$file_extension = $data[2];
			$file_type = $data[3];
			$recognition_regexp = $data[4];
			$icon = $data[5];
			$file_extension_list[] = $file_extension;
			echo  "($row, '$doctype_number', '$doctype_description','0','','','','','$recognition_regexp', '$file_extension', '$file_type', '$icon')";
			if ( $linecount !== $row ) echo ",<br>";
			else  echo ";<br>";
			$row++;
		}
		fclose($handle);

		$rows = $row - 1;
		print "
		DROP TABLE `doctypes_seq`; <br>
		CREATE TABLE `doctypes_seq` (
		`sequence` int(11) NOT NULL auto_increment,
		PRIMARY KEY  (`sequence`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=$row ;
		<br>
		INSERT INTO `doctypes_seq` (`sequence`) VALUES
		($rows);
		";

		echo '<br />';
		foreach($file_extension_list as $extension){
			echo '$valid_file_ext["' .$extension. '"] = "' .$extension. '";';
			echo '<br>';
		}

		die;
	}

	/**
	 * get list of scripts and display it in navigator window
	 */
	public function getcustomAction()
	{
		$list = glob(dirname(__FILE__) . '/custom/*.php');

		$html = '<h1>CUSTOM RANCHBE SCRIPTS</h1>';

		$html .= '<ul>';
		foreach($list as $script){
			$script = basename($script);
			$html .= "<li><a href=./custom.php?module=$script>$script</a></li>";
		}
		$html .= '</ul>';
		$this->view->assign('literalContent', $html);
		$this->view->display('layouts/withoutTabs.tpl');
		die;
	}

	public function displayAction()
	{
		// Display the template
		$this->view->assign('mid', 'application/tools/display.tpl');
		$this->view->display('layouts/ranchbe.tpl');
	}
}

