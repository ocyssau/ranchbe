<?php
namespace Controller\Partner;

use Rbs\People\Somebody;
use Rbs\Space\Factory as DaoFactory;

class Index extends \Controller\Controller
{
	public $pageId = 'partner_index';
	public $defaultSuccessForward = 'partner/index/display';
	public $defaultFailedForward = 'partner/index/display';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();
		\View\Helper\MainTabBar::get()->getTab('partner')->activate();
	}

	/**
	 *
	 */
	public function displayAction($list=null)
	{
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);

		$filterForm = new \Form\Partner\FilterForm( $this->view, $factory, $this->pageId );
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'partner_number');
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		if(!$list){
			$select = array();
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = $asSys.' as '. $asApp;
			}
			$filter->select($select);

			$list = $factory->getList(Somebody::$classId);
			$list->load($filter);

			$count = $list->countAll($filter);
			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('pageTitle', 'Partner Manager');
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'partner/display.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function suppressAction()
	{
		$this->checkRight('delete', $this->areaId, 0);
		$partnerIds = $_REQUEST['checked'];
		foreach ($partnerIds as $partnerId){
			$this->partner->PartnerSuppress($partnerId);
		}
		if($this->errorStack->hasErrors()){
		}
		else{
			return $this->successForward();
		}
	}

	/**
	 *
	 */
	public function editAction()
	{
		isset($_REQUEST['id']) ? $partnerId=$_REQUEST['id'] : $partnerId=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->successForward();
		}

		$this->checkRight('edit', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);
		$partner = new Somebody();
		$dao->loadFromId($partner, $partnerId);

		$form = new \Form\Partner\EditForm($this->view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($partner);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($partner);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Edit Partner '.$partner->getName());
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->successForward();
		}

		$this->checkRight('create', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Somebody::$classId);
		$partner = Somebody::init();

		$form = new \Form\Partner\EditForm($this->view, $factory);
		$form->setAttribute('action', '#');
		$form->bind($partner);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($partner);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->successForward();
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Create New Partner');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function exportAction()
	{
		$list = $this->partner->GetAllPartners();
		$this->view->assign('list', $list);

		header("Content-type: application/csv ");
		header("Content-Disposition: attachment; filename=export_partners.csv ");
		$this->view->display('partner/export.tpl');
		die;
	}

}
