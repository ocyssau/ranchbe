<?php
namespace Controller\Acl;

use Rbplm\People\User;
use Rbs\Acl\Right;
use Rbs\Acl\Rule;
use Rbs\Space\Factory as DaoFactory;

class Permission extends \Controller\Controller
{
	public $pageId = 'acl_permission';
	public $defaultSuccessForward = 'user/roleadmin/display';
	public $defaultFailedForward = 'acl/permission/assign';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['resourceid']) ? $resourceId=$_REQUEST['resourceid'] : $resourceId=null;
		isset($_REQUEST['areaid']) ? $areaId=$_REQUEST['areaid'] : $areaId=null;

		$this->resourceId = $resourceId;
		$this->areaId = $areaId;

		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('admin');
	}

	/**
	 *
	 */
	public function assignAction()
	{
		isset($_REQUEST['roleid']) ? $roleId=$_REQUEST['roleid'] : $roleId=null;
		isset($_REQUEST['id']) ? $roleId=$_REQUEST['id'] : null;
		isset($_REQUEST['rights']) ? $rights=$_REQUEST['rights'] : $rights=array();
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=null;

		if($cancel){
			$this->redirect($this->ifSuccessForward);
		}

		$resourceId = $this->resourceId;
		$areaId = $this->areaId;

		$factory = DaoFactory::get();
		$rightDao = $factory->getDao(Right::$classId);

		if($validate){
			$rule = new Rule();
			$rule->setRoleId($roleId);
			$rule->setResourceId($resourceId);
			foreach($rights as $rightId=>$ruleName){
				$rule->addRule($rightId,$ruleName);
			}
			$ruleDao = $factory->getDao(Rule::$classId);
			$ruleDao->save($rule);
			$this->redirect($this->ifSuccessForward);
		}

		/* Get the permissions of the role */
		$roleRightsStmt = $rightDao->getFromRoleAndResource($roleId, $resourceId, $areaId);
		$roleRights = $roleRightsStmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->view->assign('list', $roleRights);

		// Display the template
		$template='acl/permission/assign.tpl';
		$this->view->assign('id', $roleId);
		$this->view->assign('pageTitle', 'Assign Permission To Role ' . $roleId);
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}
}