<?php
namespace Controller\Acl;

use Rbplm\People\User;
use Rbplm\People\Group ;
use Rbs\Acl\Role as AclRole;
use Rbs\Space\Factory as DaoFactory;

class Role extends \Controller\Controller
{
	public $pageId = 'acl_role';
	public $defaultSuccessForward = 'acl/role/assign';
	public $defaultFailedForward = 'acl/role/assign';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->areaId = "";
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('admin');
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		isset($_REQUEST['userid']) ? $userId=$_REQUEST['userid'] : null;
		isset($_REQUEST['roleid']) ? $roleId=$_REQUEST['roleid'] : $roleId=null;

		/* Load User */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(AclRole::$classId);

		$bind = array('userId'=>$userId, 'roleId'=>$roleId);

		try{
			$parentKey = $dao->getTranslator()->toSys('parentId');
			$childKey = $dao->getTranslator()->toSys('childId');
			$dao->delete($parentKey.'=:userId AND '.$childKey.'=:roleId', $bind);
		}
		catch(\Exception $e){
			$this->errorStack()->warning($e->getMessage());
		}

		$this->redirect($this->ifSuccessForward, array('userid'=>$userId));
	}

	/**
	 *
	 */
	public function assignAction()
	{
		isset($_REQUEST['userid']) ? $userId=$_REQUEST['userid'] : $userId=null;
		isset($_REQUEST['id']) ? $userId=$_REQUEST['id'] : null;
		isset($_REQUEST['roleid']) ? $roleId=$_REQUEST['roleid'] : $roleId=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		/* Load User */
		$factory = DaoFactory::get();
		$dao = $factory->getDao(User::$classId);
		$roleDao = $factory->getDao(AclRole::$classId);
		$user = new User();
		$dao->loadFromId($user, $userId);

		if($validate){
			$role = new AclRole();
			$role->setRoleId($roleId);
			$role->setUserId($userId);
			try{
				$roleDao->insert($role);
			}
			catch(\Exception $e){
				$this->errorStack()->warning('The Role is probably yet existing');
			}
			$this->redirect($this->ifSuccessForward, array('userid'=>$userId));
		}

		/* Select group props as Application sementic */
		foreach(\Rbs\People\GroupDao::$sysToApp as $asSys=>$asApp){
			$select1[] = "`$asSys` AS `$asApp`";
			$select2[] = "`child`.`$asSys` AS `$asApp`";
		}

		/* Get User Groups */
		$roleStmt = $roleDao->getChildren($userId, $select2);
		$roles = $roleStmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->view->assign('usergrps', $roles);

		/* Get Availables Groups */
		$grpList = $factory->getList(Group::$classId);
		$grpList->select($select1);
		$grpList->load("1=1 ORDER BY name");
		$roles = $grpList->toArray();
		$this->view->assign('availableGroups', $roles);

		/* Display the template */
		$template='acl/role/assign.tpl';
		$this->view->assign('id', $userId);
		$this->view->assign('user', $user->getArrayCopy());
		$this->view->assign('pageTitle', 'Assign Role To User ' . $user->getLogin());
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}
}
