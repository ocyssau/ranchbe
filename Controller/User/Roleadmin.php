<?php
namespace Controller\User;

use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Sys\Session;

class Roleadmin extends \Controller\Controller
{
	public $pageId = 'group_admin';
	public $defaultSuccessForward = 'user/roleadmin/display';
	public $defaultFailedForward = 'user/roleadmin/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->areaId = 1;
		$checkFlood = \check_flood($this->pageId);
		$this->session =& $_SESSION[$this->pageId];
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('role');
	}

	/**
	 *
	 */
	function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);
		$role = People\Group::init('New Role');

		$form = new \Form\Role\EditForm($this->view, $factory);
		$form->setAttribute('action',$this->getRoute());
		$form->bind($role);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Create Role');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function editAction()
	{
		isset($_REQUEST['roleid']) ? $roleId=$_REQUEST['roleid'] : $roleId=null;
		isset($_REQUEST['id']) ? $roleId=$_REQUEST['id'] : null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);
		$role = new People\Group();
		$dao->loadFromId($role, $roleId);

		$form = new \Form\Role\EditForm($this->view, $factory);
		$form->setAttribute('action',$this->getRoute());
		$form->bind($role);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($role);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Edit Role '.$role->getName());
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function deleteAction()
	{
		isset($_REQUEST['checked']) ? $Ids=$_REQUEST['checked'] : $Ids=array();
		isset($_REQUEST['id']) ? $Ids=array($_REQUEST['id']) : null;

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);

		foreach ($Ids as $Id){
			try{
				$dao->deleteFromId($Id);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
	}

	/**
	 *
	 */
	function displayAction()
	{
		isset($_REQUEST['resourceid']) ? $resourceId=$_REQUEST['resourceid'] : $resourceId=null;
		isset($_REQUEST['areaid']) ? $areaId=$_REQUEST['areaid'] : $areaId=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		isset($resourceId) ? $this->session['resourceId'] = $resourceId : $resourceId = $this->session['resourceId'];
		isset($areaId) ? $this->session['areaid'] = $areaId : $areaId = $this->session['areaid'];

		$this->checkRight('admin', $areaId, $resourceId);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\Group::$classId);

		$filterForm = new \Form\Role\FilterForm($this->view, $factory, $this->pageId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, $dao->toSys('name'));
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		if(!$list){
			$list = $factory->getList(People\Group::$classId);
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = "`$asSys` AS `$asApp`";
			}
			$filter->select($select);
			$list->load($filter);

			$count = $list->countAll($filter);
			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('resourceid', $resourceId);
		$this->view->assign('areaid', $areaId);
		$this->view->assign('pageTitle', 'Roles Manager');
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'user/roleadmin/display.tpl');
		$this->view->display($this->layout);
	}
}