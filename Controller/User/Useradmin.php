<?php
namespace Controller\User;

use Rbplm\People;
use Rbs\Acl\Role as AclRole;
use Rbs\Space\Factory as DaoFactory;

class Useradmin extends \Controller\Controller
{

	public $pageId = 'user_admin';

	public $defaultSuccessForward = 'user/useradmin/display';

	public $defaultFailedForward = 'user/useradmin/display';

	/**
	 */
	public function init()
	{
		parent::init();
		$this->areaId = 1;
		$checkFlood = \check_flood($this->pageId);
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('Admin')->activate('user');
	}

	/**
	 *
	 * @param unknown_type $this->userlib
	 */
	protected function _batchImportUsers()
	{
		$this->checkRight('admin', $this->areaId, 0);

		$fname = $_FILES['csvlist']['tmp_name'];
		$fhandle = fopen($fname, "r");
		$fields = fgetcsv($fhandle, 1000);
		if ( !$fields[0] ) {
			$this->view->assign('msg', tra("The file is not a CSV file or has not a correct syntax"));
			$this->view->display('application/error/error.tpl');
			die();
		}
		while( !feof($fhandle) ) {
			$data = fgetcsv($fhandle, 1000);
			$temp_max = count($fields);
			for ($i = 0; $i < $temp_max; $i++) {
				if ( $fields[$i] == "login" && function_exists("mb_detect_encoding") && mb_detect_encoding($data[$i], "ASCII, UTF-8, ISO-8859-1") == "ISO-8859-1" ) {
					$data[$i] = utf8_encode($data[$i]);
				}
				@$ar[$fields[$i]] = $data[$i];
			}
			$userrecs[] = $ar;
		}
		fclose($fhandle);

		if ( !is_array($userrecs) ) {
			$this->view->assign('msg', tra("No records were found. Check the file please!"));
			$this->view->display('application/error/error.tpl');
			die();
		}
		$added = 0;
		$errors = array();

		foreach( $userrecs as $u ) {
			if ( empty($u['login']) ) {
				$discarded[] = discardUser($u, tra("User login is required"));
			}
			elseif ( empty($u['password']) ) {
				$discarded[] = discardUser($u, tra("Password is required"));
			}
			elseif ( empty($u['email']) ) {
				$discarded[] = discardUser($u, tra("Email is required"));
			}
			elseif ( $this->userlib->user_exists($u['login']) and (!isset($_REQUEST['overwrite'])) ) {
				$discarded[] = discardUser($u, tra("User is duplicated"));
			}
			else {
				if ( !$this->userlib->user_exists($u['login']) ) {
					$this->userlib->add_user($u['login'], $u['password'], $u['password'], $u['email']);
					// $logslib->add_log('users',sprintf(tra("Created account %s <%s>"),$u["login"], $u["email"]));
				}

				if ( @$u['groups'] ) {
					$grps = explode(",", $u['groups']);

					foreach( $grps as $grp ) {
						$grp = preg_replace("/^ *(.*) *$/u", "$1", $grp);
						if ( !$this->userlib->group_exists($grp) ) {
							$err = tra("Unknown") . ": $grp";
							if ( !in_array($err, $errors) ) {
								$errors[] = $err;
							}
						}
						else {
							$this->userlib->assign_user_to_group($u['login'], $grp);
						}
					}
				}
				$added++;
			}
		}
		$this->view->assign('added', $added);
		if ( @is_array($discarded) ) {
			$this->view->assign('discarded', count($discarded));
		}
		@$this->view->assign('discardlist', $discarded);
		if ( count($errors) ) {
			array_unique($errors);
			$this->view->assign_by_ref('errors', $errors);
		}
	}
 // End of function batch import

	/**
	 * Process the form to add a user here
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;

		if ( $cancel ) {
			return $this->redirect($this->ifSuccessForward);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);
		$user = People\User::init();

		$form = new \Form\User\EditForm($this->view, $factory);
		$form->setAttribute('action', $this->getRoute());
		$form->bind($user);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($user);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Create User');
		$this->view->display($this->layout);
	}

	/**
	 * Process the form to edit a user
	 */
	public function edituserAction()
	{
		isset($_REQUEST['userid']) ? $userId = $_REQUEST['userid'] : $userId = null;
		isset($_REQUEST['id']) ? $userId = $_REQUEST['id'] : null;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;

		if ( $cancel ) {
			return $this->redirect($this->ifSuccessForward);
		}

		if ( $userId == 'me' ) {
			$user = People\CurrentUser::get();
			$userId = $user->getId();
		}
		else {
			$this->checkRight('admin', $this->areaId, 0);
			$factory = DaoFactory::get();
			$dao = $factory->getDao(People\User::$classId);
			$user = new People\User();
			$dao->loadFromId($user, $userId);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$form = new \Form\User\EditForm($this->view, $factory);
		$form->setAttribute('action', $this->getRoute());
		$form->bind($user);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($_POST);
			if ( $form->validate() ) {
				try {
					$dao->save($user);
				}
				catch( \Exception $e ) {
					$this->errorStack()->error($e->getMessage());
				}
				return $this->redirect($this->ifSuccessForward);
			}
		}
		$form->prepareRenderer();
		$this->view->assign('mid', $form->template);
		$this->view->assign('pageTitle', 'Edit User ' . $user->getLogin());
		$this->view->display($this->layout);
	}

	/**
	 * Process the form to edit a user
	 */
	public function setpasswordAction()
	{
		isset($_REQUEST['userid']) ? $userId = $_REQUEST['userid'] : $userId = null;
		isset($_REQUEST['id']) ? $userId = $_REQUEST['id'] : null;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : $cancel = false;
		isset($_REQUEST['validate']) ? $validate = $_REQUEST['validate'] : $validate = false;

		if ( $cancel ) {
			return $this->redirect($this->ifSuccessForward);
		}

		if ( $userId == 'me' ) {
			$user = People\CurrentUser::get();
			$userId = $user->getId();
		}
		else {
			$this->checkRight('admin', $this->areaId, 0);
			$factory = DaoFactory::get();
			$dao = $factory->getDao(People\User::$classId);
			$user = new People\User();
			$dao->loadFromId($user, $userId);
		}

		$this->checkRight('admin', $this->areaId, 0);

		$form = new \Form\User\PasswordForm($this->view, $factory);
		$form->setAttribute('action', $this->getRoute());
		$form->setData(array(
			'password1' => 'azerty',
			'password2' => 'azerty',
			'id' => $userId
		));
		$form->setData($_POST);

		// Try to validate the form
		if ( $validate && $form->validate() ) {
			$form->freeze(); // and freeze it
			try {
				$password = $form->exportValue('password1');
				$password = md5($password);
				$user->setPassword($password);
				$dao->save($user);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else {
			$form->prepareRenderer();
			$this->view->assign('mid', $form->template);
			$this->view->assign('pageTitle', 'Edit User ' . $user->getLogin());
			$this->view->display($this->layout);
		}
	}

	/**
	 * Process the form to suppress user actions request
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $Ids = $_REQUEST['checked'] : $Ids = array();
		isset($_REQUEST['userid']) ? $Ids = array(
			$_REQUEST['userid']
		) : null;

		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);

		foreach( $Ids as $Id ) {
			try {
				$dao->deleteFromId($Id);
			}
			catch( \Exception $e ) {
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->redirect($this->ifSuccessForward, array(
			'spacename' => $spaceName
		));
	}

	/**
	 */
	public function displayAction()
	{
		$factory = DaoFactory::get();
		$dao = $factory->getDao(People\User::$classId);

		$this->checkRight('read', $this->areaId, 0);

		$filterForm = new \Form\User\FilterForm($this->view, $factory, $this->pageId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, $dao->toSys('login'));
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		if ( !$list ) {
			$list = $factory->getList(People\User::$classId);
			foreach( $dao->metaModel as $asSys => $asApp ) {
				$select[] = "`$asSys` AS `$asApp`";
			}
			$filter->select($select);
			$list->load($filter);

			$count = $list->countAll($filter);
			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}

		$grpDao = $factory->getDao(People\Group::$classId);
		$roleDao = $factory->getDao(AclRole::$classId);
		/* Select group props as Application sementic */
		foreach( $grpDao->metaModel as $asSys => $asApp ) {
			$select2[] = "`child`.`$asSys` AS `$asApp`";
		}

		/* Get User Groups */
		$users = array();
		foreach( $list as $user ) {
			$userId = $user['id'];
			$roles = $roleDao->getChildren($userId, $select2)->fetchAll(\PDO::FETCH_ASSOC);
			$user['roles'] = $roles;
			$users[] = $user;
		}
		$this->view->assign('list', $users);

		// Display the template
		$this->view->assign('pageTitle', 'Users Manager');
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'user/useradmin/display.tpl');
		$this->view->display($this->layout);
	}
}