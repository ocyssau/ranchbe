<?php
namespace Controller\User;

use Rbplm\People\User;
use Rbplm\People\User\Preference as UserPreference;
use Rbplm\People\CurrentUser;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\NotExistingException;

class Preference extends \Controller\Controller
{
	public $pageId = 'user_preference';
	public $defaultSuccessForward = 'user/preference/editme';
	public $defaultFailedForward = 'user/preference/editme';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$tabs = \View\Helper\MainTabBar::get($this->view)->getTab('User')->activate('preferences');
	}

	/**
	 *
	 */
	public function editmeAction()
	{
		isset($_REQUEST['userid']) ? $userId=$_REQUEST['userid'] : $userId=null;
		isset($_REQUEST['id']) ? $userId=$_REQUEST['id'] : null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->redirect($this->ifSuccessForward);
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(UserPreference::$classId);
		$user = CurrentUser::get();
		$userId = $user->getId();

		try{
			$preference = new UserPreference();
			$dao->loadFromOwnerId($preference, $userId);
		}
		catch(NotExistingException $e){
			$preference = UserPreference::init();
			$preference->setOwner($user);
		}

		$form = new \Form\User\PreferenceForm($this->view, $factory);
		$form->setAttribute('action',$this->getRoute());
		$form->setData($preference->getArrayCopy());
		$form->setData($_POST);

		// Try to validate the form
		if ($validate && $form->validate()) {
			$form->freeze(); //and freeze it
			try{
				$form->bind($user);
				$dao->save($user);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
			}
			return $this->redirect($this->ifSuccessForward);
		}
		else{
			$form->prepareRenderer();
			$this->view->assign('mid', $form->template);
			$this->view->assign('pageTitle', 'Preference Of User '.$user->getLogin());
			$this->view->display($this->layout);
		}
	}
}
