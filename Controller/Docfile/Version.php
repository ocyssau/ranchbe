<?php
namespace Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Docfile\Iteration as DocfileIteration;
use Rbplm\People\User\Wildspace;
use Rbplm\People\User\CurrentUser;

class Version extends \Controller\Controller
{
	public $pageId = 'docfile_version';
	public $defaultSuccessForward = 'docfile/version/display';
	public $defaultFailedForward = 'docfile/version/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;

		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->view->spacename = $spaceName;

		//Assign name to particular fields
		$this->view->assign('file_icons_dir', BASEURL.'/img/filetypes');
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] : $checked=array();
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : $fileId=null;

		if($fileId){
			$checked[] = $fileId;
		}

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocfileIteration::$classId);
		$service = new \Rbs\Ged\Docfile\Service($factory);

		foreach($checked as $fileId){
			$docfile = $dao->loadFromId(new DocfileIteration(), $fileId);
			$service->delete($docfile, $withfiles=true, $withiterations=false);
		}

		$this->redirect($this->ifSuccessForward, array('space'=>$this->spaceName, 'fileid'=>$fileId));
	}

	/**
	 *
	 */
	public function putinwsService()
	{
		isset($_REQUEST['checked']) ? $checked = $_REQUEST['checked'] : $checked=array();
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : $fileId=null;

		if($fileId){
			$checked[] = $fileId;
		}

		$dao = DaoFactory::get($this->spaceName)->getDao(DocfileIteration::$classId);
		$wildspace = new Wildspace(CurrentUser::get());
		$toPath = $wildspace->getPath();
		$prefix='consult_';

		foreach($checked as $fileId){
			$docfile = $dao->loadFromId(new DocfileIteration(), $fileId);
			$docfile->getData()->getFsData()->copy($toPath.'/'.$prefix.$docfile->getData()->getName(), 0777, false);
		}
	}

	/**
	 *
	 */
	public function viewfileAction()
	{
		isset($_REQUEST['checked']) ? $fileId = $_REQUEST['checked'][0] : null;
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : null;

		$factory = DaoFactory::get($this->spaceName);
		$docfile = $factory->getDao(DocfileIteration::$classId)->loadFromId(new DocfileIteration(), $fileId);

		$viewer = new \Rbs\Viewer\Viewer();
		$viewer->initFromDocfile($docfile);
		$viewer->push();
	}

	/**
	 *
	 */
	public function displayAction()
	{
		isset($_REQUEST['file_id']) ? $fileId = $_REQUEST['file_id'] : $fileId=null;
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : null;

		$spaceName = $this->spaceName;
		$return = array('error', 'feedback');

		$factory = DaoFactory::get($spaceName);
		$list = $factory->getList(DocfileIteration::$classId);
		$dao = $factory->getDao(DocfileIteration::$classId);


		try{
			$docfile = new DocfileVersion();
			$factory->getDao($docfile)->loadFromId($docfile, $fileId);

			/* get all file versions */
			$select = array();
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[]=$asSys .' as '. $asApp;
			}
			$list->select($select);
			$list->load($dao->toSys('ofDocfileId').'=:parentId', array(':parentId'=>$fileId));
			$this->view->assign('list', $list->toArray());
		}
		catch(\Exception $e){
			$this->errorStack()->error($e->getMessage());
			$this->view->assign('list', array());
		}

		$this->view->assign('fileid', $fileId);
		$this->view->assign('pageTitle', 'Iterations Of ' . $docfile->getUid() . ' (#'.$docfile->getId().')');

		// Display the template
		$this->view->assign('randWindowName', 'container_'.uniqid());
		$this->view->assign('mid', 'docfile/version/display.tpl');
		$this->view->display($this->layout);
	}
}
