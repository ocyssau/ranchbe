<?php
namespace Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbs\Sys\Session;
use Rbplm\Dao\Filter\Op;

class Manager extends \Controller\Controller
{
	public $pageId = 'docfile_manager';
	public $defaultSuccessForward = 'docfile/manager/display';
	public $defaultFailedForward = 'docfile/manager/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $spaceName = $_REQUEST['space'] : $spaceName=null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;
		$this->checkFlood = \check_flood($pageId);

		/* Assign name to particular fields */
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('spacename' , $spaceName);

		/* Record url for page and Active the tab */
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function viewfileAction()
	{
		isset($_REQUEST['checked']) ? $fileId = $_REQUEST['checked'][0] : null;
		isset($_REQUEST['fileid']) ? $fileId = $_REQUEST['fileid'] : null;

		$factory = DaoFactory::get($this->spaceName);
		$docfile = $factory->getDao(DocfileVersion::$classId)->loadFromId(new DocfileVersion(), $fileId);

		$viewer = new \Rbs\Viewer\Viewer();
		$viewer->initFromDocfile($docfile);
		$viewer->push();
	}

	/**
	 *
	 */
	public function hideAction()
	{
		$_SESSION['DisplayDocfile'] = false;
		return $this->redirect('rbdocument/manager/index');
	}

	/**
	 *
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @param \Rbs\Space\Factory $factory
	 * @param \Form\Docfile\FilterForm $filterForm
	 */
	public function getList($filter, $factory, $filterForm=null)
	{
		$dao = $factory->getDao(DocfileVersion::$classId);
		$this->bindSortOrder($filter, $dao->toSys('uid'));

		$list = $factory->getList(DocfileVersion::$classId);
		$select = array();
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[] = $asSys.' as '. $asApp;
		}

		$filter->with(array(
			'table'=>$factory->getTable(DocumentVersion::$classId),
			'on'=>'document_id',
			'alias'=>'document',
			'select'=>array('document_id','workitem_id','document_number'),
		));
		$filter->select($select);
		$filter->andfind( $this->containerId, $dao->toSys('parentId'), Op::OP_EQUAL );
		return $list;
	}

	/**
	 *
	 */
	function displayAction($list=null, $template=null)
	{
		$_SESSION['DisplayDocfile'] = true;

		$this->checkRight('read', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'containerid'=>$this->containerId
			));

		($template == null) ?  $template = 'docfile/manager/display.tpl' : null;
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);

		if(!$list){
			$filterForm = new \Form\Docfile\FilterForm($this->view, $factory, $this->pageId, $this->containerId);
			$filter = new \Rbs\Dao\Sier\Filter('', false);

			$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
			$filterForm->bind($filter)->save();

			/* Get list */
			$dao = $factory->getDao(DocfileVersion::$classId);
			$docDao = $factory->getDao(DocumentVersion::$classId);

			$this->bindSortOrder($filter, 'df.'.$dao->toSys('uid'));
			$filter->sort('df.'.$dao->toSys('uid'), 'ASC');

			$list = $factory->getList(DocfileVersion::$classId);

			$parentIdAsSys = $docDao->toSys('parentId');

			$select = array();
			foreach($dao->metaModel as $asSys=>$asApp){
				$select[] = 'df.'.$asSys.' as '. $asApp;
			}
			$select[] = "document.$parentIdAsSys as containerId";
			$select[] = 'document.document_number as parentNumber';
			$select[] = 'document.document_indice_id as parentVersion';
			$select[] = 'document.document_version as parentIteration';

			$filter->with(array(
				'table'=>$factory->getTable(DocumentVersion::$classId),
				'on'=>'document_id',
				'alias'=>'document',
			));
			$filter->select($select);
			$filter->andfind( $this->containerId, 'document.'.$parentIdAsSys, Op::OP_EQUAL );

			$list->countAll = $list->countAll('');
			$paginator->setMaxLimit($list->countAll);
			$paginator->bind($filter)->save();

			$sql = $filter->toSql($factory->getTable(DocfileVersion::$classId), 'df');
			$list->loadFromSql($sql);
		}

		$this->view->assign('list', $list->toArray());
		$this->view->assign('filter', $filterForm->render());
		$paginator->bindToView($this->view);
		$this->view->assign('pageTitle' , tra('Docfile Manager'));
		$this->view->assign('mid', $template);
		$this->view->display($this->layout);
	}

} //End of class
