<?php
namespace Controller\Docfile;

use Rbplm\Dao\Filter\Op;

require_once 'class/common/space.php';
require_once 'class/common/document.php';
require_once 'class/common/metadata.php';
require_once 'class/common/container.php';
require_once 'class/common/doclink.php';
require_once 'class/wildspace.php';
require_once 'class/common/file.php';
require_once 'class/common/fsdata.php';
require_once 'class/common/import.php';
require_once 'lib/Date/date.php';
require_once 'HTML/Progress2.php';
require_once 'class/messu/messulib.php';
require_once 'HTML/QuickForm.php';
require_once 'class/Batchjob.php';

class Import extends \Controller\Controller
{
	public $pageId = 'docfile_import'; //(string)
	public $defaultSuccessForward = array ('module' => 'document', 'controller' => 'import', 'action' => 'index' );
	public $defaultFailedForward = array ('module' => 'document', 'controller' => 'import', 'action' => 'index' );

	/**
	 */
	protected function _checkErrors()
	{
		if ( $this->errorStack->hasErrors() ){
			return $this->errorStack->getErrors(true);
		}
	} //End of function

	/**
	 *
	 */
	protected function _unpack($containerId, $packageIds, $type='InContainer')
	{
		//unpack each package
		foreach ($packageIds as $packageId){
			if ( !$this->import->ImportPackage($packageId, $containerId, $type) ){
				$body = '<b>Not Imported package : </b>'.$this->import->PackInfos['package_file_name'].'<br/>';
				$body.= '<b>An error is occured during import task</b><br />';
				$subject = 'Error in import task of '.$this->import->PackInfos['package_file_name'];
			}else{
				$body = '<b>Imported package : </b>'.$this->import->PackInfos['package_file_name'].'<br/>';
				$subject = 'End of import task of '.$this->import->PackInfos['package_file_name'];
			}
			$end_time = time();
			$duration = ($end_time - $start_time);

			include_once './lib/Date/date.php';
			$body.= '<b>target dir : </b>'.$this->import->target_dir.'<br/>';
			$body.= '<b>target container : </b>'.$this->import->target_container_num.'<br/>';
			$body.= '<b>By : </b>'.$this->import->GetUserName().'<br/>';
			$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
			$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
			$body.= '<b>duration : </b>'.$duration.' sec<br/>';

			if (!$_REQUEST['background']){ //Tache execute en arriere plan
				echo $body;
			}

			//Send message to user
			include_once './class/messu/messulib.php';
			$to = $user;
			$from = $user;
			$cc   = '';
			//$body = str_replace('<br/>',"\n",ob_get_contents());
			$body = str_replace('<br/>',"\n",$body);
			$priority = 3;
			$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
		} //End of loop
		die;
	}


	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$containerId=null;
		$space=null;

		isset($_REQUEST['SelectedContainer']) ? $containerId = $_REQUEST['SelectedContainer'] : null;
		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['space']) ? $space = $_REQUEST['space'] : null;

		if($space){
			$this->container = \container::_factory($space , $containerId);
			$this->space = $this->container->space;
			$this->spaceName = $space;
			$this->areaId = $this->container->AREA_ID;
			$this->import = new \import($space, $this->container);
		}


		$this->checkFlood = \check_flood($_REQUEST['page_id']);

		$this->checkRight('admin', $this->areaId, 0);

		//Assign name to particular fields
		$this->view->assign( 'action' , $_REQUEST['action']);
		$this->view->assign('CONTAINER_TYPE', $space);
		$this->view->assign('CONTAINER_NUMBER' , $space.'_number');
		$this->view->assign('CONTAINER_DESCRIPTION' , $space.'_description');
		$this->view->assign('CONTAINER_STATE' , $space.'_state');
		$this->view->assign('CONTAINER_ID' , $space.'_id');
		$this->view->assign('containerid' , $containerId);
		$this->view->assign('space' , $space);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		rbinit_web();
		rbinit_view();
		rbinit_context();
		//rbinit_tab();
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function displayAction()
	{
		ignore_user_abort(false);

		$list = $this->import->GetAllImportPackage($params); //get infos on all package in deposit directories
		$this->view->assign_by_ref('list', $list);

		// Display the template
		($template == null) ?  $template = 'document/import/index.tpl' : null;

		$this->view->assign('randWindowName', 'file import');
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->view->assign('mid', 'docfile/import/display.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 * @param import $this->import
	 * @param container $Manager
	 * @param smarty $smarty
	 */
	public function viewlogfileAction()
	{
		$logFile = $this->import->GetLogFile($_REQUEST['import_order']);
		$fsdata = new \fsdata($logFile);
		if($fsdata->DownloadFile()){
			die;
		}
		else{
			$this->errorStack->push(ERROR, 'Warning', array('element'=>$logFile), 'The logfile %element% don\'t exist');
		}
	}

	/**
	 *
	 */
	public function uploadAction()
	{
		$dstfile = $this->import->IMPORT_DIR .'/'. basename($file['name']);
		\file::UploadFile($_FILES['uploadFile'], $_REQUEST['overwrite'], $dstfile);
	}

	/**
	 *
	 */
	public function suppresspackageAction()
	{
		$this->checkRight('admin', $this->areaId, 0);
		if ( empty($_REQUEST['checked']) ) {
			print 'none selected package';
			return;
		}

		foreach ($_REQUEST['checked'] as $order ){
			$this->import->SuppressPackage($order);
		}
	}

	/**
	 *
	 */
	public function uncompresspackageAction()
	{
		$this->checkRight('admin', $this->areaId, 0);
		if ( empty($_REQUEST['checked']) ) {
			print 'none selected package';
			return;
		}

		foreach ($_REQUEST['checked'] as $order){
			$this->import->UncompressPackage($order , NULL);
		}
	}

	/**
	 *
	 */
	public function unpackAction()
	{
		$this->checkRight('admin', $this->areaId, 0);
		if(empty($_REQUEST['checked'])){
			print 'none selected package';
			return;
		}

		$start_time = time();

		//If we want lunch the process in background
		//See chapter 40 of php documentation
		if($_REQUEST['background'] ){ //Tache execute en arriere plan
			session_write_close(); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
			ignore_user_abort(true); //Continue execution after deconnection of user

			print formatDate($start_time).'
			- La tache s\'execute en arriere plan. Vous recevrez un message lorsqu\'elle sera terminée.<br />
			Vous pouvez fermer cette fenêtre.<br />
			<a href="javascript:window.close()">Close Window</a><br />';
			flush(); // On vide le buffer de sortie
		}
		else{
			ob_end_flush(); //Switch off flush tempo
		}

		$containerId = $this->container->GetProperty('container_id');

		//unpack each package
		foreach ($_REQUEST['checked'] as $order){
			if(!$_REQUEST['background'] && USE_HTML_PROGRESS){
				$pb = new \HTML_Progress2(null, HTML_PROGRESS2_BAR_HORIZONTAL, 0, 100, false);
				$pb->setAnimSpeed(200);
				$pb->setIncrement(10);
				$pb->setIndeterminate(true);
				$pb->addLabel(HTML_PROGRESS2_LABEL_TEXT, 'LABEL1');
				$pb->setLabelAttributes('LABEL1', 'valign=right');

				$add_header = '<style type="text/css"><!--'.$pb->getStyle().'--></style>';
				$add_header .= $pb->getScript(false);
				$this->view->assign_by_ref('additionnal_header' , $add_header);

				require_once 'GUI/progressObserver.php';
				$pbObserver = new \progressObserver($pb);

				$this->import->attach_all($pbObserver);

				$this->view->display('layouts/htmlheader.tpl');
				$pb->display();

				echo '<br /><br />';
			}

			if ( !$this->import->ImportPackage($order, $containerId, $_REQUEST['target']) ){
				//if ( !$this->import_result ){
				$body = '<b>Not Imported package : </b>'.$this->import->PackInfos['package_file_name'].'<br/>';
				$body.= '<b>An error is occured during import task</b><br />';
				$subject = 'Error in import task of '.$this->import->PackInfos['package_file_name'];
			}
			else{
				$body = '<b>Imported package : </b>'.$this->import->PackInfos['package_file_name'].'<br/>';
				$subject = 'End of import task of '.$this->import->PackInfos['package_file_name'];
			}
			$end_time = time();
			$duration = ($end_time - $start_time);

			$body.= '<b>target dir : </b>'.$this->import->target_dir.'<br/>';
			$body.= '<b>target container : </b>'.$this->import->target_container_num.'<br/>';
			$body.= '<b>By : </b>'.$this->import->GetUserName().'<br/>';
			$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
			$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
			$body.= '<b>duration : </b>'.$duration.' sec<br/>';

			if ( !$_REQUEST['background'] ){ //Tache execute en arriere plan
				echo $body;
			}

			//Send message to user
			$to = $user;
			$from = $user;
			$cc   = '';
			//$body = str_replace('<br/>',"\n",ob_get_contents());
			$body = str_replace('<br/>',"\n",$body);
			$priority = 3;
			$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
		} //End of loop
		die;
	}



	/**
	 *
	 */
	public function viewcontentsAction()
	{
		if(empty($_REQUEST['file'])){
			die('none package selected');
		}

		$list = $this->import->ListContentImportPackage($_REQUEST['file'], false);

		print('<h1><b>Content of '.$_REQUEST['file'].'</b></h1><ul>');
		print '<table border=1>';
		print('<tr><td><b>file_name</b></td><td><b>mtime</b></td><td><b>size</b></td><td><b>gid</b></td><td><b>uid</b></td></tr>');
		$count = 0;
		while($list->next()){
			$count++;
			$stat = $list->getStat();
			print '<tr>';
			print('<td><b>'.$list->getFilename().'</b></td>'); //Get the name of the unpack file
			print('<td>'.strftime(LONG_DATE_FORMAT, $stat['mtime']).'</b></td>');
			print('<td>'.sprintf ("%.2f k%s",($stat['size']/1024),"o").'</b></td>');
			print('<td>'.$stat['gid'].'</b></td>');
			print('<td>'.$stat['uid'].'</b></td>');
			print '</tr>';
		}
		print '</table>';
		print '<b>'.$count.' elements in this package<br /></b>';
		die;
	}

	/**
	 *
	 */
	public function viewimportedAction()
	{
		if(!empty($_REQUEST['import_order'])){
			$this->view->assign( 'import_order' , $_REQUEST['import_order']);
			$all_field = array ('file_name' => 'Name',
					'file_extension' => 'Extensions',
					'file_path' => 'Path',
					'file_open_by' => 'Created by',
					'file_open_date' => 'Created date',
					'file_update_by' => 'Last update by',
					'file_update_date' => 'Last update date',
					'file_access_code' => 'Access',
					'file_state' => 'State',
					'file_version' => 'Version',
					'file_type' => 'Type',
					'file_size' => 'Size',
					'file_md5' => 'Md5',
			);
			$this->view->assign('all_field', $all_field);

			//Assign name to particular fields
			$this->view->assign('CONTAINER_TYPE', $this->container->SPACE_NAME);

			//Process the code for pagination and display management
			$filterForm = new \Form\Filter\StandardSimple($this->view, $this->pageId);
			$filter = new \Rbs\Dao\Sier\Filter('', false);
			$this->bindSortOrder($filter, 'handle');
			$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
			$filterForm->bind($filter)->save();

			//Display the users list of system
			$params = array (
					'orders' => array($filter->getSort() => $filter->getOrder()),
					'limit' => $filter->getLimit(),
					'offset' => $filter->getOffset(),
					'select'=>'all',
			);

			//get all files
			$list = $this->import->ListImportedFiles($_REQUEST['import_order'] , $params);
			$this->view->assign_by_ref('list', $list);

			//Assign additional var to add to URL when redisplay the same url
			$sameurl_elements[]='action';
			$sameurl_elements[]='import_order';

			$this->view->assign('mid', 'docfile/import/viewimported.tpl');
			$this->view->display($this->layout);
		}
		else{
			die('none package selected');
		}
		die;
	}


	/**
	 *
	 */
	function adddescriptionAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		if ( empty($_REQUEST['import_order']) ){
			die('none package selected');
		}

		$form = new \Form\AbstractForm('form', 'post'); //Construct the form with QuickForm lib

		//Get the current description
		$desc = $this->import->GetPackageDescription( $_REQUEST['import_order'] );

		//Set defaults values of elements
		$form->setDefaults(array('description' => $desc));

		//Add hidden fields
		$form->addElement('hidden', 'import_order', $_REQUEST['import_order']);
		$form->addElement('hidden', 'action', 'addDescription');

		//Add fields for input informations
		$form->addElement('textarea', 'description', 'Description', array('rows' => 3, 'cols' => 20));

		$form->addElement('reset', 'reset', 'reset');
		$form->addElement('submit', 'submit', 'Go');

		// Try to validate the form
		if ($form->validate()) {
			$form->freeze(); //and freeze it
			// Process the modify request
			$this->import->AddImportPackageDescription($_REQUEST['description'], $_REQUEST['import_order']);
		} //End of validate form

		$form->prepareRenderer('form');

		// Display the template
		$this->view->assign('mid', 'docfile/import/adddescription.tpl');
		$this->view->display($this->layout);
		die;
	}

	/**
	 *
	 */
	public function createbatchjobAction()
	{
		//Create the job
		if( isset($_REQUEST['data-jsonencoded']) ){

			$data = json_decode($_REQUEST['data-jsonencoded']);
			$cleanData = array();
			$wastedData = array();
			foreach($data as $map){
				$packageId = $map[0];
				$targetId = $map[1];
				if( $packageId && $targetId ){
					$cleanData[] = $map;
				}
				else{
					$wastedData[] = $map;
				}
			}
			$data = $cleanData;
			if(!$data){
				echo 'Nothing to put in batch' . PHP_EOL;
				die;
			}

			$batchJob = new \Rbplm_Batchjob( array('data'=>$data, 'runner'=>\Rbplm_Batchjob::RUNNER_FILEIMPORT) );
			$batchJob->setPlanned( time() );
			$jId = $batchJob->create();
			if(!$jId){
				header('HTTP/1.1 500 Internal Server Error');
				echo 'Creation of job has failed' . PHP_EOL;
			}
			else{
				//attach job to package
				foreach($data as $map){
					$packageId = $map[0];
					$targetId = $map[1];
					$ok = $this->import->UpdateImportPackage(array('jobid'=>$jId) , $packageId);
					if(!$ok){
						echo "unable to link job $jId to package $packageId" . PHP_EOL;
					}
				}
				echo 'Id of new JOB is :' . $jId . PHP_EOL;
			}
		}
		//Display creation screen
		else{
			if(empty($_REQUEST['checked'])){
				print 'none selected package';
				return;
			}

			$list = array();
			foreach ($_REQUEST['checked'] as $order){
				$list[] = $this->import->GetPackageInfos( $order );
			}
			$this->view->assign_by_ref('list', $list);

			$mockups = $this->container->GetAll( array('select'=>array('mockup_id', 'mockup_number', 'mockup_description'), 'where'=>'file_only=1' ) );
			//var_dump($mockups);
			$this->view->assign('mockups', $mockups);

			// Display the template
			$this->view->assign('mid', 'docfile/import/createbatchjob.tpl');
			$this->view->display($this->layout);
		}
	}

} //End of class
