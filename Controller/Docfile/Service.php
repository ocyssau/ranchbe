<?php
namespace Controller\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\Document\Link as DocLink;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\People\User\Wildspace;
use Rbplm\People\CurrentUser;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Sys\Error;

class Service extends \Controller\Controller
{
	public $pageId = 'docfile_service';
	protected $areaId = 10;
	public $defaultSuccessForward = 'document/detail/display';
	public $defaultFailedForward = 'document/detail/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : $documentId=null;

		$this->spaceName = $spaceName;
		$this->documentId = $documentId;

		$this->view->spacename = $spaceName;
		$this->view->documentid = $documentId;
	}

	/**
	 * Suppress file
	 *
	 */
	function deleteService()
	{
		$return = array('errors'=>array(), 'feedbacks'=>array());
		isset($_REQUEST['checked']) ? $fileIds=$_REQUEST['checked'] : $fileIds=array();
		isset($_REQUEST['fileid']) ? $fileIds[]=$_REQUEST['fileid'] : null;

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocfileVersion::$classId);
		$service = new \Rbs\Ged\Docfile\Service($factory);

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		foreach ($fileIds as $fileId){
			$docfile = new DocfileVersion();
			$dao->loadFromId($docfile, $fileId);
			try{
				$service->delete($docfile, $withfiles=true, $withiterations=true);
			}
			catch(\Exception $e){
				$return['errors'][$fileId] = $e->getMessage();
				continue;
			}
			$return['feedbacks'][$fileId] = array(
				'message'=>'success',
				'id'=>$fileId,
				'properties'=>$docfile->getArrayCopy(),
			);
		}
		return $this->serviceReturn($return);
	}

	/**
	 *
	 */
	function resetfileService()
	{
		$input = $_REQUEST;
		isset($input['checked']) ? $fileIds=$input['checked'] : $fileIds=array();

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocfileVersion::$classId);
		$service = new \Rbs\Ged\Docfile\Service($factory);
		$return = array('errors'=>array(), 'feedbacks'=>array());

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		foreach ($fileIds as $fileId){
			$docfile = new DocfileVersion();
			$dao->loadFromId($docfile, $fileId);
			try{
				$service->checkin($docfile, $releasing=true, $updateData=false, $checkAccess=true);
			}
			catch(\Exception $e){
				$return['errors'][$fileId] = $e->getMessage();
				continue;
			}
			$return['feedbacks'][$fileId] = array(
				'message'=>'success',
				'id'=>$fileId,
				'properties'=>$docfile->getArrayCopy(),
			);
		}
		return $this->serviceReturn($return);
	}

	/**
	 *
	 */
	function setroleService()
	{
		$return = array('errors', 'feedbacks');

		isset($_REQUEST['checked']) ? $fileIds = $_REQUEST['checked'] : $fileIds=array();
		isset($_REQUEST['fileid']) ? $fileIds = array($_REQUEST['fileid']) : null;
		isset($_REQUEST['roleid']) ? $roleId = $_REQUEST['roleid'] : $roleId=null;

		$spaceName = $this->spaceName;
		$dao = DaoFactory::get($spaceName)->getDao(DocfileVersion::$classId);

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		foreach ($fileIds as $fileId){
			$docfile = new DocfileVersion();
			$dao->loadFromId($docfile, $fileId);

			$aCode = $docfile->checkAccess();
			if($aCode >= 100){
				throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('accessCode'=>$aCode));
			}

			try{
				$docfile->setMainrole($roleId);
				$dao->save($docfile);
			}
			catch(\Exception $e){
				$return['errors'][$fileId] = $e->getMessage();
				continue;
			}
			$return['feedbacks'][$fileId] = array(
				'message'=>'success',
				'id'=>$fileId,
				'roleid'=>$roleId,
				'spacename'=>$spaceName
			);
		}
		return $this->serviceReturn($return);
	}

	/**
	 * Put file in wildspace
	 *
	 */
	function putinwsService()
	{
		$return = array('errors', 'feedbacks');
		isset($_REQUEST['checked']) ? $fileIds = $_REQUEST['checked'] : $fileIds=array();

		$this->checkRight('edit', $this->areaId, $this->documentId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));

		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(DocfileVersion::$classId);
		$wildspace = new Wildspace(CurrentUser::get());
		$toPath = $wildspace->getPath();
		$prefix = 'consult_';

		foreach($fileIds as $fileId){
			try{
				$docfile = new DocfileVersion();
				$dao->loadFromId($docfile, $fileId);
				$docfile->getData()->getFsData()->copy($toPath.'/'.$prefix.$docfile->getData()->getName(), 0777, true);
			}
			catch(\Exception $e){
				$return['errors'][$fileId] = $e->getMessage();
				continue;
			}
			$return['feedbacks'][$fileId] = array(
				'message'=>'success',
				'id'=>$fileId,
				'properties'=>$docfile->getArrayCopy(),
			);
		}
		return $this->serviceReturn($return);
	}

	/**
	 * View files
	 *
	 * @param unknown_type $this->document
	 * @param unknown_type $this->view
	 * @param unknown_type $checkFlood
	 */
	function viewfileAction()
	{
		$this->checkRight('read', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));
	}

	/**
	 *
	 */
	function viewfatherdocAction()
	{
		$space = $this->spaceName;
		$areaId = $this->areaId;

		$this->checkRight('edit', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'documentid'=>$this->documentId
			));


		if (!empty($_REQUEST['document_id'])){
			$docLink = new \doclink($this->document);
			$father_documents = $docLink->GetFathers();
			$this->view->assign_by_ref('father_documents', $father_documents);
		}
		$this->view->assign('content1display', 'none');
		$this->view->assign('content3display', 'block');
		$this->display();
	}
} //End of class

