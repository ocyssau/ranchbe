<?php
namespace Controller\Category;

use Rbplm\Ged\Category;
use Form\Category\EditForm;
use Rbs\Space\Factory as DaoFactory;

class Manager extends \Controller\Controller
{
	public $pageId = 'category_manage'; //(string)
	public $defaultSuccessForward = 'category/manager/display';
	public $defaultFailedForward = 'category/manager/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['page_id']) ? $pageId = $_REQUEST['page_id'] : $pageId=$this->pageId;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;

		$this->spaceName = $spaceName;
		$this->areaId = 1;

		$this->checkFlood = \check_flood($pageId);
		$this->view->assign('spacename', $spaceName);

		//Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view);
	}

	/**
	 * (non-PHPdoc)
	 * @see Controller.Controller::initService()
	 */
	public function initService()
	{
		parent::initService();
		$this->layout = 'layouts/fragment.tpl';
	}

	/**
	 *
	 */
	public function displayAction()
	{
		$factory = DaoFactory::get($this->spaceName);
		$dao = $factory->getDao(Category::$classId);

		$filterForm = new \Form\Category\FilterForm( $this->view, $factory, $this->pageId );
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, $dao->toSys('name'));
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();

		if(!$list){
			$list = $factory->getList(Category::$classId);
			$list->load($filter);

			$count = $list->countAll($filter);
			$paginator->setMaxLimit($count);
			$paginator->bind($filter)->save();
		}
		$this->view->assign('list', $list->toArray());

		// Display the template
		$this->view->assign('pageTitle', 'Categories Manager');
		$this->view->assign('filter', $filterForm->render());
		$this->view->assign('paginator', $paginator->render());
		$this->view->assign('mid', 'category/display.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $categoryIds=$_REQUEST['checked'] : $categoryIds=array();
		$spaceName = $this->spaceName;

		$this->checkRight('delete', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Category::$classId);

		foreach ($categoryIds as $categoryId){
			try{
				$dao->deleteFromId($categoryId);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
	}

	/**
	 *
	 */
	public function editAction()
	{
		isset($_REQUEST['categoryid']) ? $categoryId=$_REQUEST['categoryid'] : $categoryId=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		$spaceName = $this->spaceName;

		if($cancel){
			return $this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
		}

		$this->checkRight('edit', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Category::$classId);
		$category = new Category();
		$dao->loadFromId($category, $categoryId);

		$form = new EditForm($this->view, $factory);
		$form->setAttribute('action',$this->getRoute());
		$form->setData($category->getArrayCopy());

		// Try to validate the form
		if ($validate && $form->validate()) {
			$form->freeze(); //and freeze it
			$form->bind($category);
			$dao->save($category);
			return $this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
		}
		else{
			$form->prepareRenderer();
			$this->view->assign('mid', $form->template);
			$this->view->assign('pageTitle', 'Edit Category '.$category->getName());
			$this->view->display($this->layout);
		}
	}

	/**
	 *
	 */
	public function createAction()
	{
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		$spaceName = $this->spaceName;

		if($cancel){
			return $this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
		}

		$this->checkRight('create', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Category::$classId);
		$category = Category::init();
		$category->setName('Category_'.uniqid());

		$form = new EditForm($this->view, $factory);
		$form->setAttribute('action',$this->getRoute());
		$form->setData($category->getArrayCopy());

		// Try to validate the form
		if ($validate && $form->validate()) {
			$form->freeze(); //and freeze it
			$form->bind($category);
			$dao->save($category);
			return $this->redirect($this->ifSuccessForward, array('spacename'=>$spaceName));
		}
		else{
			$form->prepareRenderer();
			$this->view->assign('mid', $form->template);
			$this->view->assign('pageTitle', 'Create Category');
			$this->view->display($this->layout);
		}
	}

} //End of class
