ALTER TABLE `workitem_documents` ADD `space` VARCHAR( 30 ) NOT NULL DEFAULT 'workitem' AFTER `document_indice_id` ,
ADD INDEX ( `space` ) ;

ALTER TABLE `bookshop_documents` ADD `space` VARCHAR( 30 ) NOT NULL DEFAULT 'bookshop' AFTER `document_indice_id` ,
ADD INDEX ( `space` ) ;

ALTER TABLE `cadlib_documents` ADD `space` VARCHAR( 30 ) NOT NULL DEFAULT 'cadlib' AFTER `document_indice_id` ,
ADD INDEX ( `space` ) ;

ALTER TABLE `mockup_documents` ADD `space` VARCHAR( 30 ) NOT NULL DEFAULT 'mockup' AFTER `document_indice_id` ,
ADD INDEX ( `space` ) ;

ALTER TABLE `galaxia_instance_activities` 
	DROP PRIMARY KEY,
	ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
	ADD INDEX ( `instanceId` , `activityId` ),
	CHANGE `started` `started` INT( 14 ) NULL DEFAULT NULL,
	CHANGE `ended` `ended` INT( 14 ) NULL DEFAULT NULL,
	CHANGE `activityId` `activityId` INT( 14 ) NOT NULL,
	CHANGE `instanceId` `instanceId` INT( 14 ) NOT NULL;

ALTER TABLE `galaxia_instances` 
	CHANGE `status` `status` ENUM('running', 'exception', 'aborted', 'completed') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'running';
	
ALTER TABLE `galaxia_transitions` 
	CHANGE `actFromId` `parentId` INT( 14 ) NOT NULL,
	CHANGE `actToId` `childId` INT( 14 ) NOT NULL,
	CHANGE `pId` `processId` INT( 14 ) NOT NULL;

ALTER TABLE `galaxia_transitions` 
	ADD `name` VARCHAR( 30 ) NULL,
	ADD `uid` VARCHAR( 32 ) NOT NULL FIRST,
	ADD UNIQUE (`uid`),
	ADD `parentUid` VARCHAR( 32 ) NOT NULL AFTER `parentId`,
	ADD INDEX ( `parentUid` ),
	ADD `childUid` VARCHAR( 32 ) NOT NULL AFTER `childId`,
	ADD INDEX ( `childUid` ),
	ADD `lindex` INT(7) DEFAULT 0,
	ADD `attributes` TEXT NULL;

ALTER TABLE `galaxia_activities` 
	CHANGE `expirationTime` `expirationTime` INT( 6 ) UNSIGNED NULL,
	ADD `cid` INT NOT NULL DEFAULT '421' AFTER `activityId`,
	ADD `uid` VARCHAR( 32 ) NULL AFTER `activityId`,
	ADD `ownerId` varchar(255) NULL,
	ADD `parentId` int NULL,
	ADD `parentUid` varchar(255) NULL,
	ADD `updateById` varchar(255) NULL,
	ADD `updated` datetime NULL,
	ADD `progression` float NULL,
	ADD `roles` text NULL,
	ADD `attributes` TEXT NULL,
	ADD UNIQUE (`uid`),
	ADD INDEX (roles(255)),
	ADD INDEX ( `ownerId` ),
	ADD INDEX ( `parentId` ),
	ADD INDEX ( `parentUid` );

	
ALTER TABLE `liveuser_groups` ADD `uid` VARCHAR( 30 ) NOT NULL AFTER `group_id`;
UPDATE liveuser_groups SET uid=MD5(group_define_name);
ALTER TABLE `liveuser_groups` ADD UNIQUE INDEX `uid_UNIQUE` (`uid` ASC);

ALTER TABLE `liveuser_users` ADD `uid` VARCHAR( 30 ) NOT NULL AFTER `auth_user_id`; 
UPDATE liveuser_users SET uid=MD5(handle);
ALTER TABLE `liveuser_users` ADD UNIQUE INDEX `uid_UNIQUE` (`uid` ASC);

ALTER TABLE `workitems` 
	ADD `name` VARCHAR( 128 ) NOT NULL AFTER `workitem_number`,
	ADD `uid` VARCHAR( 128 ) NULL AFTER `name`;
UPDATE workitems SET name=workitem_number;
UPDATE workitems SET uid=workitem_number;
ALTER TABLE `workitems` ADD INDEX `name_idx` (`name`);
ALTER TABLE `workitems` ADD UNIQUE KEY `uid_idx` (`uid`);

ALTER TABLE `cadlibs` 
	ADD `name` VARCHAR( 128 ) NOT NULL AFTER `cadlib_number`,
	ADD `uid` VARCHAR( 128 ) NULL AFTER `name`;
UPDATE cadlibs SET name=cadlib_number;
UPDATE cadlibs SET uid=cadlib_number;
ALTER TABLE `cadlibs` ADD INDEX `name_idx` (`name`);
ALTER TABLE `cadlibs` ADD UNIQUE KEY `uid_idx` (`uid`);

ALTER TABLE `mockups` 
	ADD `name` VARCHAR( 128 ) NOT NULL AFTER `mockup_number`,
	ADD `uid` VARCHAR( 128 ) NULL AFTER `name`;
UPDATE mockups SET name=mockup_number;
UPDATE mockups SET uid=mockup_number;
ALTER TABLE `mockups` ADD INDEX `name_idx` (`name`);
ALTER TABLE `mockups` ADD UNIQUE KEY `uid_idx` (`uid`);

ALTER TABLE `bookshops` 
	ADD `name` VARCHAR( 128 ) NOT NULL AFTER `bookshop_number`,
	ADD `uid` VARCHAR( 128 ) NULL AFTER `name`;
UPDATE bookshops SET name=bookshop_number;
UPDATE bookshops SET uid=bookshop_number;
ALTER TABLE `bookshops` ADD INDEX `name_idx` (`name`);
ALTER TABLE `bookshops` ADD UNIQUE KEY `uid_idx` (`uid`);

ALTER TABLE `projects` 
	ADD `name` VARCHAR(128) NOT NULL AFTER `project_number`,
	ADD `uid` VARCHAR( 32 ) NULL AFTER `project_id`;
UPDATE projects SET name=project_number;
UPDATE projects SET uid=LEFT(UUID(),8);
ALTER TABLE `projects` ADD INDEX `name_idx` (`name`);
ALTER TABLE `projects` ADD UNIQUE KEY `uid_idx` (`uid`);
ALTER TABLE `projects` CHANGE COLUMN `uid` `uid` VARCHAR( 32 ) NOT NULL;

ALTER TABLE liveuser_groups ADD PRIMARY KEY (`group_id`);
ALTER TABLE liveuser_rights ADD PRIMARY KEY (`right_id`);
ALTER TABLE liveuser_grouprights ADD PRIMARY KEY (`group_id`,`right_id`);
ALTER TABLE liveuser_users ADD KEY `handle_idx` (`handle`),
	ADD PRIMARY KEY (`auth_user_id`);
ALTER TABLE liveuser_groupusers ADD PRIMARY KEY (`perm_user_id`,`group_id`);
ALTER TABLE liveuser_areas ADD PRIMARY KEY (`area_id`);

ALTER TABLE liveuser_perm_users CHANGE COLUMN `perm_user_id` `perm_user_id` INT(11) NOT NULL,
	ADD PRIMARY KEY (`perm_user_id`),
	ADD KEY `auth_idx` (`auth_user_id`);

ALTER TABLE `liveuser_userrights`
	CHANGE COLUMN `perm_user_id` `perm_user_id` INT(11) NOT NULL ,
	CHANGE COLUMN `right_id` `right_id` INT(11) NOT NULL,
	ADD PRIMARY KEY (`perm_user_id`, `right_id`);

ALTER TABLE `workitem_alias` 
	ADD `name` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `workitem_number`,
	ADD `uid` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `name`,
	ADD INDEX ( `uid` ),
	ADD INDEX ( `name` ) ;

ALTER TABLE `bookshop_alias` 
	ADD `name` VARCHAR( 128 ) NOT NULL DEFAULT 'bookshop' AFTER `bookshop_number`,
	ADD `uid` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `name`,
	ADD INDEX ( `uid` ),
	ADD INDEX ( `name` ) ;

ALTER TABLE `cadlib_alias` 
	ADD `name` VARCHAR( 128 ) NOT NULL DEFAULT 'cadlib' AFTER `cadlib_number` ,
	ADD `uid` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `name`,
	ADD INDEX ( `uid` ),
	ADD INDEX ( `name` ) ;

ALTER TABLE `mockup_alias` 
	ADD `name` VARCHAR( 128 ) NOT NULL DEFAULT 'mockup' AFTER `mockup_number` ,
	ADD `uid` VARCHAR( 128 ) NOT NULL DEFAULT 'workitem' AFTER `name`,
	ADD INDEX ( `uid` ),
	ADD INDEX ( `name` ) ;

CREATE TABLE `workitem_metadata_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;

CREATE TABLE `cadlib_metadata_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;

CREATE TABLE `bookshop_metadata_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;

CREATE TABLE `mockup_metadata_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;

ALTER TABLE `workitem_metadata` 
	ADD `id` int(11) FIRST,
	DROP PRIMARY KEY,
	ADD `appname` VARCHAR(30) AFTER `adv_select`,
	ADD `getter` VARCHAR(30),
	ADD `setter` VARCHAR(30),
	ADD `label` VARCHAR(30),
	ADD `min` decimal,
	ADD `max` decimal,
	ADD `step` decimal,
	ADD `dbfilter` VARCHAR(255),
	ADD `dbquery` VARCHAR(255),
	ADD `attributes` text,
	ADD `extendedCid` VARCHAR(32) AFTER field_name;
SET @position := 0;
UPDATE workitem_metadata SET `id`=(@position := @position + 1);
UPDATE workitem_metadata_seq SET id=(SELECT MAX(workitem_metadata.id) FROM workitem_metadata) LIMIT 100;
ALTER TABLE `workitem_metadata` 
	ADD PRIMARY KEY (`id`),
	ADD UNIQUE INDEX `fname-cid` (`field_name` ASC, `extendedCid` ASC);

ALTER TABLE `cadlib_metadata` 
	ADD `id` int(11) FIRST,
	DROP PRIMARY KEY,
	ADD `appname` VARCHAR(30) AFTER `adv_select`,
	ADD `getter` VARCHAR(30),
	ADD `setter` VARCHAR(30),
	ADD `label` VARCHAR(30),
	ADD `min` decimal,
	ADD `max` decimal,
	ADD `step` decimal,
	ADD `dbfilter` VARCHAR(255),
	ADD `dbquery` VARCHAR(255),
	ADD `attributes` text,
	ADD `extendedCid` VARCHAR(32) AFTER field_name;
SET @position := 0;
UPDATE cadlib_metadata
SET `id`=(@position := @position + 1);
UPDATE cadlib_metadata_seq SET id=(SELECT MAX(cadlib_metadata.id) FROM cadlib_metadata) LIMIT 100;
ALTER TABLE `cadlib_metadata`
	ADD PRIMARY KEY (`id`),
	ADD UNIQUE INDEX `fname-cid` (`field_name` ASC, `extendedCid` ASC);


ALTER TABLE `bookshop_metadata` 
	ADD `id` int(11) FIRST,
	DROP PRIMARY KEY,
	ADD `appname` VARCHAR(30) AFTER `adv_select`,
	ADD `getter` VARCHAR(30),
	ADD `setter` VARCHAR(30),
	ADD `label` VARCHAR(30),
	ADD `min` decimal,
	ADD `max` decimal,
	ADD `step` decimal,
	ADD `dbfilter` VARCHAR(255),
	ADD `dbquery` VARCHAR(255),
	ADD `attributes` text,
	ADD `extendedCid` VARCHAR(32) AFTER field_name;
SET @position := 0;
UPDATE bookshop_metadata
SET `id`=(@position := @position + 1);
UPDATE bookshop_metadata_seq SET id=(SELECT MAX(bookshop_metadata.id) FROM bookshop_metadata) LIMIT 100;
ALTER TABLE `bookshop_metadata` 
	ADD PRIMARY KEY (`id`),
	ADD UNIQUE INDEX `fname-cid` (`field_name` ASC, `extendedCid` ASC);


ALTER TABLE `mockup_metadata` 
	ADD `id` int(11) FIRST,
	DROP PRIMARY KEY,
	ADD `appname` VARCHAR(30) AFTER `adv_select`,
	ADD `getter` VARCHAR(30),
	ADD `setter` VARCHAR(30),
	ADD `label` VARCHAR(30),
	ADD `min` decimal,
	ADD `max` decimal,
	ADD `step` decimal,
	ADD `dbfilter` VARCHAR(255),
	ADD `dbquery` VARCHAR(255),
	ADD `attributes` text,
	ADD `extendedCid` VARCHAR(32) AFTER field_name;
SET @position := 0;
UPDATE mockup_metadata SET `id`=(@position := @position + 1);
UPDATE mockup_metadata_seq SET id=(SELECT MAX(mockup_metadata.id) FROM mockup_metadata);
ALTER TABLE `mockup_metadata`
	ADD PRIMARY KEY (`id`),
	ADD UNIQUE INDEX `fname-cid` (`field_name` ASC, `extendedCid` ASC);

ALTER TABLE `workitem_doc_rel` 
	ADD COLUMN `hash` char(32) default NULL AFTER `dr_access_code`,
	ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
ALTER TABLE `cadlib_doc_rel` 
	ADD COLUMN `hash` char(32) default NULL AFTER `dr_access_code`,
	ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
ALTER TABLE `bookshop_doc_rel` 
	ADD COLUMN `hash` char(32) default NULL AFTER `dr_access_code`,
	ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
ALTER TABLE `mockup_doc_rel` 
	ADD COLUMN `hash` char(32) default NULL AFTER `dr_access_code`,
	ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
	
ALTER TABLE `project_history` 
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	ADD `data` text DEFAULT NULL;

ALTER TABLE `workitem_documents_history` 
	ADD `data` text DEFAULT NULL,
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	DROP `check_out_by`,
	DROP `check_out_date`,
	DROP `issued_from_document`,
	DROP `update_date`,
	DROP `update_by`,
	DROP `open_date`,
	DROP `open_by`,
	DROP `doctype_id`,
	DROP `instance_id`;
ALTER TABLE `cadlib_documents_history` 
	ADD `data` text DEFAULT NULL,
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	DROP `check_out_by`,
	DROP `check_out_date`,
	DROP `issued_from_document`,
	DROP `update_date`,
	DROP `update_by`,
	DROP `open_date`,
	DROP `open_by`,
	DROP `doctype_id`,
	DROP `instance_id`;
ALTER TABLE `bookshop_documents_history` 
	ADD `data` text DEFAULT NULL,
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	DROP `check_out_by`,
	DROP `check_out_date`,
	DROP `issued_from_document`,
	DROP `update_date`,
	DROP `update_by`,
	DROP `open_date`,
	DROP `open_by`,
	DROP `doctype_id`,
	DROP `instance_id`;
ALTER TABLE `mockup_documents_history` 
	ADD `data` text DEFAULT NULL,
	ADD `action_comment` text DEFAULT NULL AFTER `action_date`,
	DROP `check_out_by`,
	DROP `check_out_date`,
	DROP `issued_from_document`,
	DROP `update_date`,
	DROP `update_by`,
	DROP `open_date`,
	DROP `open_by`,
	DROP `doctype_id`,
	DROP `instance_id`;
	
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parentId` int(11) NOT NULL,
  `parentUid` varchar(32) DEFAULT NULL,
  `parentCid` varchar(32) NOT NULL,
  `ownerId` varchar(32) DEFAULT NULL,
  `spaceName` varchar(32) NOT NULL,
  `body` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parentId`),
  KEY `K_parentUid` (`parentUid`),
  KEY `K_parentCid` (`parentCid`),
  KEY `K_spaceName` (`spaceName`),
  KEY `K_ownerId` (`ownerId`)
) ENGINE=InnoDB;

CREATE TABLE `postit_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO postit_seq(id) VALUES(10);

ALTER TABLE `workitem_metadata_rel`
ADD `property_id` int(1) NOT NULL,
DROP PRIMARY KEY;
UPDATE workitem_metadata_rel SET property_id=(SELECT id FROM workitem_metadata WHERE field_name=workitem_metadata_rel.field_name);
ALTER TABLE `workitem_metadata_rel`
DROP `field_name`,
ADD KEY (`workitem_id` ASC, `property_id` ASC),
DROP INDEX `workitem_metadata_rel_uniq`,
ADD UNIQUE INDEX `workitem_metadata_rel_uniq` (`workitem_id` ASC, `property_id` ASC),
ADD PRIMARY KEY (`link_id`);

-- ADD UID TO DOCUMENTS TABLE
-- ADD document_name TO DOCUMENTS TABLE
ALTER TABLE workitem_documents
ADD document_name VARCHAR(140) DEFAULT NULL AFTER document_number,
ADD uid VARCHAR(140) DEFAULT NULL AFTER document_id;
UPDATE workitem_documents SET uid=(CONCAT(document_number,'.',document_indice_id));
ALTER TABLE workitem_documents CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE cadlib_documents
ADD document_name VARCHAR(140) DEFAULT NULL AFTER document_number,
ADD uid VARCHAR(140) DEFAULT NULL AFTER document_id;
UPDATE cadlib_documents SET uid=(CONCAT(document_number,'.',document_indice_id));
ALTER TABLE cadlib_documents CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE bookshop_documents
ADD document_name VARCHAR(140) DEFAULT NULL AFTER document_number,
ADD uid VARCHAR(140) DEFAULT NULL AFTER document_id;
UPDATE bookshop_documents SET uid=(CONCAT(document_number,'.',document_indice_id));
ALTER TABLE bookshop_documents CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE mockup_documents
ADD document_name VARCHAR(140) DEFAULT NULL AFTER document_number,
ADD uid VARCHAR(140) DEFAULT NULL AFTER document_id;
UPDATE mockup_documents SET uid=(CONCAT(document_number,'.',document_indice_id));
ALTER TABLE mockup_documents CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

UPDATE workitem_documents SET document_name=document_number;
UPDATE cadlib_documents SET document_name=document_number;
UPDATE bookshop_documents SET document_name=document_number;
UPDATE mockup_documents SET document_name=document_number;

-- ADD UID TO DOCFILE TABLE
ALTER TABLE workitem_doc_files 
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD `mainrole` int(2) NOT NULL DEFAULT 4,
	ADD `roles` VARCHAR(512) NULL;
UPDATE workitem_doc_files SET uid=(CONCAT(file_name,'.',file_version));
ALTER TABLE workitem_doc_files CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE bookshop_doc_files 
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD `mainrole` int(2) NOT NULL DEFAULT 4,
	ADD `roles` VARCHAR(512) NULL;
UPDATE bookshop_doc_files SET uid=(CONCAT(file_name,'.',file_version));
ALTER TABLE bookshop_doc_files CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE cadlib_doc_files 
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD `mainrole` int(2) NOT NULL DEFAULT 4,
	ADD `roles` VARCHAR(512) NULL;
UPDATE cadlib_doc_files SET uid=(CONCAT(file_name,'.',file_version));
ALTER TABLE cadlib_doc_files CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

ALTER TABLE mockup_doc_files 
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD `mainrole` int(2) NOT NULL DEFAULT 4,
	ADD `roles` VARCHAR(512) NULL;
UPDATE mockup_doc_files SET uid=(CONCAT(file_name,'.',file_version));
ALTER TABLE mockup_doc_files CHANGE uid uid VARCHAR(140) NOT NULL UNIQUE;

-- REDEFINE DOCFILE VERSIONS
ALTER TABLE `workitem_doc_files_versions` 
	CHANGE `father_id` `document_id` INT(11) NOT NULL,
	CHANGE `file_path` `file_path` varchar(256) NOT NULL,
	ADD `of_file_id` int(11) DEFAULT NULL AFTER `document_id`,
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD INDEX ( `of_file_id` ),
	ADD `mainrole` int(2) NOT NULL DEFAULT 4 AFTER `file_path`,
	ADD `roles` VARCHAR(512) NULL AFTER `mainrole`,
	ADD `file_access_code` int(11) NOT NULL DEFAULT '0' AFTER `file_version`,
	ADD `file_checkout_by` int(11) DEFAULT NULL AFTER `file_md5`,
	ADD `file_checkout_date` int(11) DEFAULT NULL AFTER `file_checkout_by`;

ALTER TABLE `cadlib_doc_files_versions` 
	CHANGE `father_id` `document_id` INT(11) NOT NULL,
	CHANGE `file_path` `file_path` varchar(256) NOT NULL,
	ADD `of_file_id` int(11) DEFAULT NULL AFTER `document_id`,
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD INDEX ( `of_file_id` ),
	ADD `mainrole` int(2) NOT NULL DEFAULT 4 AFTER `file_path`,
	ADD `roles` VARCHAR(512) NULL AFTER `mainrole`,
	ADD `file_access_code` int(11) NOT NULL DEFAULT '0' AFTER `file_version`,
	ADD `file_checkout_by` int(11) DEFAULT NULL AFTER `file_md5`,
	ADD `file_checkout_date` int(11) DEFAULT NULL AFTER `file_checkout_by`;

ALTER TABLE `bookshop_doc_files_versions` 
	CHANGE `father_id` `document_id` INT(11) NOT NULL,
	CHANGE `file_path` `file_path` varchar(256) NOT NULL,
	ADD `of_file_id` int(11) DEFAULT NULL AFTER `document_id`,
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD INDEX ( `of_file_id` ),
	ADD `mainrole` int(2) NOT NULL DEFAULT 4 AFTER `file_path`,
	ADD `roles` VARCHAR(512) NULL AFTER `mainrole`,
	ADD `file_access_code` int(11) NOT NULL DEFAULT '0' AFTER `file_version`,
	ADD `file_checkout_by` int(11) DEFAULT NULL AFTER `file_md5`,
	ADD `file_checkout_date` int(11) DEFAULT NULL AFTER `file_checkout_by`;

ALTER TABLE `mockup_doc_files_versions` 
	CHANGE `father_id` `document_id` INT(11) NOT NULL,
	CHANGE `file_path` `file_path` varchar(256) NOT NULL,
	ADD `of_file_id` int(11) DEFAULT NULL AFTER `document_id`,
	ADD uid VARCHAR(140) DEFAULT NULL AFTER file_id,
	ADD INDEX ( `of_file_id` ),
	ADD `mainrole` int(2) NOT NULL DEFAULT 4 AFTER `file_path`,
	ADD `roles` VARCHAR(512) NULL AFTER `mainrole`,
	ADD `file_access_code` int(11) NOT NULL DEFAULT '0' AFTER `file_version`,
	ADD `file_checkout_by` int(11) DEFAULT NULL AFTER `file_md5`,
	ADD `file_checkout_date` int(11) DEFAULT NULL AFTER `file_checkout_by`;

-- REDEFINE USER TABLE
CREATE TABLE `acl_users` (
`id` varchar(32) NOT NULL,
`uid` varchar(30) NOT NULL,
`login` varchar(32) NOT NULL,
`lastname` VARCHAR(64) DEFAULT NULL,
`firstname` VARCHAR(64) DEFAULT NULL,
`password` varchar(32) DEFAULT NULL,
`mail` varchar(128) DEFAULT NULL,
`owner_id` int(11) DEFAULT NULL,
`primary_role_id` int(11) DEFAULT NULL,
`lastlogin` DATETIME DEFAULT '1970-01-01 00:00:00',
`is_active` tinyint(1) DEFAULT '1',
PRIMARY KEY (`id`),
KEY `login_idx` (`login`),
UNIQUE KEY `uid_idx` (`uid`)
);
-- COPY USERS TO NEW TABLE
INSERT INTO `acl_users`
(`id`, `uid`, `login`, `password`, `mail`, `lastlogin`, `is_active`) 
SELECT 
	`liveuser_users`.`auth_user_id`,
    `liveuser_users`.`uid`,
    `liveuser_users`.`handle`,
    `liveuser_users`.`passwd`,
    `liveuser_users`.`email`,
    `liveuser_users`.`lastlogin`,
    `liveuser_users`.`is_active`
FROM `liveuser_users`;

CREATE TABLE `acl_users_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10;

-- REDEFINE RIGHTS
CREATE TABLE `acl_rule` (
`role_id` int(11) NOT NULL,
`right_id` int(11) NOT NULL,
`resource_id` int(11) NOT NULL,
`rule` varchar(16) DEFAULT 'allow',
PRIMARY KEY (`role_id`,`right_id`, `resource_id`),
KEY role_index(`role_id`),
KEY right_index(`right_id`),
KEY resource_index(`resource_id`)
) ENGINE=InnoDB;

CREATE TABLE `acl_rights` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_i_idx` (`name`,`area_id`),
  KEY `name_idx` (`name`),
  KEY `area_idx` (`area_id`)
) ENGINE=InnoDB;

CREATE TABLE `acl_rights_seq` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;

INSERT INTO acl_rights 
(id, area_id, name, description) 
VALUES
-- RANCHBE AREA
(1,1,'read','Read'),
(2,1,'create','Create'),
(3,1,'edit','Edit'),
(4,1,'delete','Delete'),
(5,1,'admin','Admin'),
-- PROJECT AREA
(10,5,'read','Read'),
(11,5,'create','Create'),
(12,5,'edit','Edit'),
(13,5,'delete','Delete'),
(14,5,'admin','Admin'),
-- CONTAINER AREA
(20,10,'read','Read'),
(21,10,'create','Create'),
(22,10,'edit','Edit'),
(23,10,'delete','Delete'),
(24,10,'admin','Admin');

CREATE TABLE `acl_role` (
	`user_id` int(11) NOT NULL,
	`role_id` int(11) NOT NULL,
	PRIMARY KEY `id_i_idx` (`user_id`,`role_id`),
	KEY `user_index` (`user_id`),
	KEY `role_index` (`role_id`)
) ENGINE=InnoDB;

-- USER PREFERENCES DROP AND RECREATE
DROP TABLE `user_prefs`;
CREATE TABLE `user_prefs` (
	`user_id` int(11) NOT NULL,
	`css_sheet` varchar(32)  NOT NULL default 'default',
	`lang` varchar(32)  NOT NULL default 'default',
	`long_date_format` varchar(32)  NOT NULL default 'default',
	`short_date_format` varchar(32)  NOT NULL default 'default',
	`hour_format` varchar(32)  NOT NULL default 'default',
	`time_zone` varchar(32)  NOT NULL default 'default',
	`wildspace_path` varchar(128)  NOT NULL default 'default',
	`max_record` varchar(128)  NOT NULL default 'default',
	PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB;

ALTER TABLE `project_mockup_rel` ADD `link_id` INT(11),
ADD INDEX ( `link_id` );

ALTER TABLE `project_cadlib_rel` ADD `link_id` INT(11),
ADD INDEX ( `link_id` );

ALTER TABLE `project_bookshop_rel` ADD `link_id` INT(11),
ADD INDEX ( `link_id` );

 CREATE TABLE discussion_comment(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
 `name` VARCHAR(255) NULL DEFAULT NULL,
 `discussionUid` VARCHAR(255) NOT NULL,
 `ownerId` VARCHAR(255) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updated` datetime NOT NULL,
 `body` TEXT NOT NULL,
 PRIMARY KEY (`id`)
 );

 ALTER TABLE discussion_comment ADD UNIQUE (uid);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UID` (`uid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussionUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_OWNERID` (`ownerId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTID` (`parentId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UPDATED` (`updated` ASC);

CREATE TABLE `pdm_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO pdm_seq(id) VALUES(10);

CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) NULL,
  `version_id` int(11) NOT NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` varchar(32) NULL,
  `document_id` int(11) NULL,
  `document_uid` varchar(32) NULL,
  `spacename` varchar(32) NULL,
  `type` varchar(64) NULL,
  `materials` varchar(512) NULL,
  `weight` float NULL,
  `volume` float NULL,
  `wetsurface` float NULL,
  `density` float NULL,
  `gravitycenter` varchar(512) NULL,
  `inertiacenter` varchar(512) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_uid`,`version_id`),
  UNIQUE KEY `pdm_product_version_uniq2` (`uid`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version_id`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_instance_10` (`type`)
);
 CREATE TABLE `pdm_product_instance` (
 `id` int(11) NOT NULL,
 `uid` varchar(32) NOT NULL,
 `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
 `name` varchar(128) NOT NULL,
 `number` varchar(128) NOT NULL,
 `nomenclature` varchar(128) NULL,
 `description` varchar(512) NULL,
 `position` varchar(512) NULL,
 `quantity` int(11) NULL,
 `parent_id` int(11) NULL,
 `path` varchar(256) NULL,
 `of_product_id` int(11) NULL,
 `of_product_uid` varchar(32) NULL,
 `context_id` varchar(32) NULL,
 `type` varchar(64) NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
 KEY `INDEX_pdm_product_instance_1` (`cid`),
 KEY `INDEX_pdm_product_instance_2` (`number`),
 KEY `INDEX_pdm_product_instance_3` (`description`),
 KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
 KEY `INDEX_pdm_product_instance_5` (`uid`),
 KEY `INDEX_pdm_product_instance_6` (`name`),
 KEY `INDEX_pdm_product_instance_7` (`type`),
 KEY `INDEX_pdm_product_version_8` (`of_product_uid`),
 KEY `INDEX_pdm_product_version_9` (`of_product_id`),
 KEY `INDEX_pdm_product_version_10` (`path`)
 );

ALTER TABLE doctypes CHANGE file_extension file_extensions VARCHAR(256) DEFAULT NULL;

CREATE TABLE `notifications` (
 `id` INT NOT NULL,
 `owner` VARCHAR (128) NOT NULL,
 `referenceUid` VARCHAR (128) NOT NULL,
 `spacename` VARCHAR (64) NOT NULL,
 `events` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY  (`id`),
 KEY `INDEX_workitem_cont_notifications_1` (`owner`),
 KEY `INDEX_workitem_cont_notifications_2` (`referenceUid`),
 KEY `INDEX_workitem_cont_notifications_3` (`events`),
 UNIQUE KEY `UNIQ_workitem_cont_notifications_1` (`owner`,`referenceUid`,`events`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `message_mailbox` (
`id` int(14) NOT NULL,
`owner` varchar(40) NOT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`date` int(14) DEFAULT NULL,
`isRead` char(1)  DEFAULT NULL,
`isReplied` char(1)  DEFAULT NULL,
`isFlagged` char(1)  DEFAULT NULL,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `owner` )
) ENGINE=InnoDB;

CREATE TABLE `message_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO message_seq(id) VALUES(10);

CREATE TABLE IF NOT EXISTS `message_archive` (
`id` int(14) NOT NULL,
`owner` varchar(40) NOT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`date` int(14) DEFAULT NULL,
`isRead` char(1)  DEFAULT NULL,
`isReplied` char(1)  DEFAULT NULL,
`isFlagged` char(1)  DEFAULT NULL,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `owner` )
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `message_sent` (
`id` int(14) NOT NULL,
`owner` varchar(40) NOT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`date` int(14) DEFAULT NULL,
`isRead` char(1)  DEFAULT NULL,
`isReplied` char(1)  DEFAULT NULL,
`isFlagged` char(1)  DEFAULT NULL,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `owner` )
) ENGINE=InnoDB;

 CREATE TABLE `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `ownerUid` VARCHAR (128) NOT NULL,
 `ownerId` VARCHAR (128) NOT NULL,
 `referenceUid` VARCHAR (128) NOT NULL,
 `spacename` VARCHAR (64) NOT NULL,
 `events` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 KEY `INDEX_workitem_cont_notifications_1` (`ownerId`),
 KEY `INDEX_workitem_cont_notifications_2` (`referenceUid`),
 KEY `INDEX_workitem_cont_notifications_3` (`events`),
 UNIQUE KEY `UNIQ_workitem_cont_notifications_1` (`ownerId`,`referenceUid`,`events`)
) ENGINE=InnoDB;
