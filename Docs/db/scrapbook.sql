DROP VIEW view_completed_activities;


CREATE VIEW view_completed_activities AS
SELECT ACT.`name` AS NAME, IA.`status` AS IA_STATUS, IA.`ended` AS IA_ENDED,
  IA.`started` AS IA_STARTED, IA.`user` AS IA_USER, INS.`status` AS INSTANCE_STATUS,
  INS.`name` AS INSTANCE_NAME, INS.`started` AS INSTANCE_STARTED,
  INS.`ended` AS INSTANCE_ENDED, INS.`owner` AS INSTANCE_OWNER,
  IA.`activityId` AS `aid`, IA.`instanceId` AS `iid`, HISTO.`document_id` AS `document_id`,
  HISTO.`workitem_id` AS `container_id`
  FROM `ranchbesier`.`galaxia_activities` AS ACT 
       JOIN `ranchbesier`.`galaxia_instance_activities` AS IA ON ACT.`activityId` = IA.`activityId` 
       JOIN `ranchbesier`.`galaxia_instances` AS INS ON IA.`instanceId` = INS.`instanceId` 
       JOIN `ranchbesier`.`workitem_documents_history` AS HISTO ON HISTO.`instance_id` = INS.`instanceId`
  WHERE INS.`status` NOT LIKE 'aborted'
