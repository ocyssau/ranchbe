
SELECT 
rt.document_id AS rt_id, 
rt.document_number AS rt_number, 
applydocs.document_number AS toapply_number, 
applydocs.document_indice_id AS toapply_indice,
linked_docs.document_number AS from_number,
linked_docs.document_indice_id AS from_version
FROM workitem_documents AS rt
LEFT OUTER JOIN workitem_documents AS linked_docs ON linked_docs.document_id = rt.rt_apply_to
INNER JOIN workitem_documents AS applydocs ON linked_docs.document_number = applydocs.document_number
WHERE rt.doctype_id=184
AND applydocs.document_indice_id >= linked_docs.document_indice_id



DROP VIEW view_completed_activities;


CREATE VIEW view_completed_activities AS
SELECT ACT.`name` AS NAME, IA.`status` AS IA_STATUS, IA.`ended` AS IA_ENDED,
  IA.`started` AS IA_STARTED, IA.`user` AS IA_USER, INS.`status` AS INSTANCE_STATUS,
  INS.`name` AS INSTANCE_NAME, INS.`started` AS INSTANCE_STARTED,
  INS.`ended` AS INSTANCE_ENDED, INS.`owner` AS INSTANCE_OWNER,
  IA.`activityId` AS `aid`, IA.`instanceId` AS `iid`, HISTO.`document_id` AS `document_id`,
  HISTO.`workitem_id` AS `container_id`
  FROM `ranchbesier`.`galaxia_activities` AS ACT 
       JOIN `ranchbesier`.`galaxia_instance_activities` AS IA ON ACT.`activityId` = IA.`activityId` 
       JOIN `ranchbesier`.`galaxia_instances` AS INS ON IA.`instanceId` = INS.`instanceId` 
       JOIN `ranchbesier`.`workitem_documents_history` AS HISTO ON HISTO.`instance_id` = INS.`instanceId`
  WHERE INS.`status` NOT LIKE 'aborted'
