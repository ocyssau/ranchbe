-- phpMyAdmin SQL Dump
-- version 2.10.0.2
-- http://www.phpmyadmin.net
-- 
-- Serveur: localhost
-- Généré le : Mar 11 Décembre 2007 à 11:17
-- Version du serveur: 4.1.20
-- Version de PHP: 5.0.4
-- 
-- ranchbe0.4.6beta
-- 

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de données: `ranchbe`
-- 

-- 
-- Structure de la table `bookshop_familles`
-- 
CREATE TABLE `bookshop_familles` (
  `id` varchar(12) collate latin1_general_ci NOT NULL default '0',
  `famille_name` varchar(64) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


-- 
-- Structure de la table `bookshop_key_word`
-- 

CREATE TABLE `bookshop_key_word` (
  `id` int(11) NOT NULL auto_increment,
  `key_word` varchar(64) collate latin1_german1_ci NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;


-- 
-- Structure de la table `checkout_index`
-- 

CREATE TABLE `checkout_index` (
  `file_name` varchar(128) collate latin1_general_ci NOT NULL default '',
  `designation` varchar(128) collate latin1_general_ci default NULL,
  `container_type` varchar(128) collate latin1_general_ci NOT NULL default '',
  `container_id` int(11) NOT NULL default '0',
  `container_number` varchar(128) collate latin1_general_ci NOT NULL default '',
  `check_out_by` int(11) NOT NULL default '0',
  `document_id` int(11) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_indice`
-- 

CREATE TABLE `container_indice` (
  `indice_id` int(11) NOT NULL default '0',
  `indice_value` varchar(8) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `doctypes`
-- 

CREATE TABLE `doctypes` (
  `doctype_id` int(11) NOT NULL default '0',
  `doctype_number` varchar(64) collate latin1_general_ci NOT NULL default '',
  `doctype_description` varchar(128) collate latin1_general_ci default NULL,
  `can_be_composite` tinyint(4) NOT NULL default '1',
  `file_extension` varchar(32) collate latin1_general_ci default NULL,
  `file_type` varchar(128) collate latin1_general_ci default NULL,
  `icon` varchar(32) collate latin1_general_ci default NULL,
  `script_post_store` varchar(64) collate latin1_general_ci default NULL,
  `script_pre_store` varchar(64) collate latin1_general_ci default NULL,
  `script_post_update` varchar(64) collate latin1_general_ci default NULL,
  `script_pre_update` varchar(64) collate latin1_general_ci default NULL,
  `recognition_regexp` text collate latin1_general_ci,
  `visu_file_extension` varchar(32) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`doctype_id`),
  UNIQUE KEY `UC_doctype_number` (`doctype_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `document_indice`
-- 

CREATE TABLE `document_indice` (
  `document_indice_id` int(11) NOT NULL default '0',
  `indice_value` varchar(8) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`document_indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_activities`
-- 

CREATE TABLE `galaxia_activities` (
  `activityId` int(14) NOT NULL auto_increment,
  `name` varchar(80) collate latin1_general_ci default NULL,
  `normalized_name` varchar(80) collate latin1_general_ci default NULL,
  `pId` int(14) NOT NULL default '0',
  `type` enum('start','end','split','switch','join','activity','standalone') collate latin1_general_ci default NULL,
  `isAutoRouted` char(1) collate latin1_general_ci default NULL,
  `flowNum` int(10) default NULL,
  `isInteractive` char(1) collate latin1_general_ci default NULL,
  `lastModif` int(14) default NULL,
  `description` text collate latin1_general_ci,
  `expirationTime` int(6) unsigned NOT NULL default '0',
  PRIMARY KEY  (`activityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_activity_roles`
-- 

CREATE TABLE `galaxia_activity_roles` (
  `activityId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`activityId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_instances`
-- 

CREATE TABLE `galaxia_instances` (
  `instanceId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `started` int(14) default NULL,
  `name` varchar(200) collate latin1_general_ci default 'No Name',
  `owner` varchar(200) collate latin1_general_ci default NULL,
  `nextActivity` int(14) default NULL,
  `nextUser` varchar(200) collate latin1_general_ci default NULL,
  `ended` int(14) default NULL,
  `status` enum('active','exception','aborted','completed') collate latin1_general_ci default NULL,
  `properties` longblob,
  PRIMARY KEY  (`instanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_instance_activities`
-- 

CREATE TABLE `galaxia_instance_activities` (
  `instanceId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `started` int(14) NOT NULL default '0',
  `ended` int(14) NOT NULL default '0',
  `user` varchar(40) collate latin1_general_ci default NULL,
  `status` enum('running','completed') collate latin1_general_ci default NULL,
  PRIMARY KEY  (`instanceId`,`activityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_instance_comments`
-- 

CREATE TABLE `galaxia_instance_comments` (
  `cId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `user` varchar(40) collate latin1_general_ci default NULL,
  `activityId` int(14) default NULL,
  `hash` varchar(32) collate latin1_general_ci default NULL,
  `title` varchar(250) collate latin1_general_ci default NULL,
  `comment` text collate latin1_general_ci,
  `activity` varchar(80) collate latin1_general_ci default NULL,
  `timestamp` int(14) default NULL,
  PRIMARY KEY  (`cId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_processes`
-- 

CREATE TABLE `galaxia_processes` (
  `pId` int(14) NOT NULL auto_increment,
  `name` varchar(80) collate latin1_general_ci default NULL,
  `isValid` char(1) collate latin1_general_ci default NULL,
  `isActive` char(1) collate latin1_general_ci default NULL,
  `version` varchar(12) collate latin1_general_ci default NULL,
  `description` text collate latin1_general_ci,
  `lastModif` int(14) default NULL,
  `normalized_name` varchar(80) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`pId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_roles`
-- 

CREATE TABLE `galaxia_roles` (
  `roleId` int(14) NOT NULL auto_increment,
  `pId` int(14) NOT NULL default '0',
  `lastModif` int(14) default NULL,
  `name` varchar(80) collate latin1_general_ci default NULL,
  `description` text collate latin1_general_ci,
  PRIMARY KEY  (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_transitions`
-- 

CREATE TABLE `galaxia_transitions` (
  `pId` int(14) NOT NULL default '0',
  `actFromId` int(14) NOT NULL default '0',
  `actToId` int(14) NOT NULL default '0',
  PRIMARY KEY  (`actFromId`,`actToId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_user_roles`
-- 

CREATE TABLE `galaxia_user_roles` (
  `pId` int(14) NOT NULL default '0',
  `roleId` int(14) NOT NULL auto_increment,
  `user` varchar(40) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`roleId`,`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `galaxia_container`
-- 

CREATE TABLE `galaxia_container` (
  `itemId` int(14) NOT NULL auto_increment,
  `instanceId` int(14) NOT NULL default '0',
  `orderId` int(14) NOT NULL default '0',
  `activityId` int(14) NOT NULL default '0',
  `properties` longblob,
  `started` int(14) default NULL,
  `ended` int(14) default NULL,
  `user` varchar(40) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_applications`
-- 

CREATE TABLE `liveuser_applications` (
  `application_id` int(11) default '0',
  `application_define_name` varchar(32) collate latin1_general_ci default NULL,
  UNIQUE KEY `application_id_idx` (`application_id`),
  UNIQUE KEY `define_name_i_idx` (`application_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_areas`
-- 

CREATE TABLE `liveuser_areas` (
  `area_id` int(11) default '0',
  `application_id` int(11) default '0',
  `area_define_name` varchar(32) collate latin1_general_ci default NULL,
  UNIQUE KEY `area_id_idx` (`area_id`),
  UNIQUE KEY `define_name_i_idx` (`application_id`,`area_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_area_admin_areas`
-- 

CREATE TABLE `liveuser_area_admin_areas` (
  `area_id` int(11) default '0',
  `perm_user_id` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`area_id`,`perm_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_grouprights`
-- 

CREATE TABLE `liveuser_grouprights` (
  `group_id` int(11) default '0',
  `right_id` int(11) default '0',
  `right_level` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_groups`
-- 

CREATE TABLE `liveuser_groups` (
  `group_id` int(11) default '0',
  `group_type` int(11) default '0',
  `group_define_name` varchar(32) collate latin1_general_ci default NULL,
  `group_description` varchar(64) collate latin1_general_ci default NULL,
  `is_active` tinyint(1) default '1',
  `owner_user_id` int(11) default '0',
  `owner_group_id` int(11) default '0',
  UNIQUE KEY `group_id_idx` (`group_id`),
  UNIQUE KEY `define_name_i_idx` (`group_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_groupusers`
-- 

CREATE TABLE `liveuser_groupusers` (
  `perm_user_id` int(11) default '0',
  `group_id` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_group_subgroups`
-- 

CREATE TABLE `liveuser_group_subgroups` (
  `group_id` int(11) default '0',
  `subgroup_id` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_perm_users`
-- 

CREATE TABLE `liveuser_perm_users` (
  `perm_user_id` int(11) default '0',
  `auth_user_id` varchar(32) collate latin1_general_ci default NULL,
  `auth_container_name` varchar(32) collate latin1_general_ci default NULL,
  `perm_type` int(11) default '0',
  UNIQUE KEY `perm_user_id_idx` (`perm_user_id`),
  UNIQUE KEY `auth_id_i_idx` (`auth_user_id`,`auth_container_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_rights`
-- 

CREATE TABLE `liveuser_rights` (
  `right_id` int(11) default '0',
  `area_id` int(11) default '0',
  `right_define_name` varchar(32) collate latin1_general_ci default NULL,
  `right_description` varchar(64) collate latin1_general_ci default NULL,
  `has_implied` tinyint(1) default '1',
  UNIQUE KEY `right_id_idx` (`right_id`),
  UNIQUE KEY `define_name_i_idx` (`area_id`,`right_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_right_implied`
-- 

CREATE TABLE `liveuser_right_implied` (
  `right_id` int(11) default '0',
  `implied_right_id` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`right_id`,`implied_right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_translations`
-- 

CREATE TABLE `liveuser_translations` (
  `translation_id` int(11) default '0',
  `section_id` int(11) default '0',
  `section_type` int(11) default '0',
  `language_id` varchar(32) collate latin1_general_ci default NULL,
  `name` varchar(32) collate latin1_general_ci default NULL,
  `description` varchar(32) collate latin1_general_ci default NULL,
  UNIQUE KEY `translation_id_idx` (`translation_id`),
  UNIQUE KEY `translation_i_idx` (`section_id`,`section_type`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_userrights`
-- 

CREATE TABLE `liveuser_userrights` (
  `perm_user_id` int(11) default '0',
  `right_id` int(11) default '0',
  `right_level` int(11) default '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `liveuser_users`
-- 

CREATE TABLE `liveuser_users` (
  `auth_user_id` varchar(32) collate latin1_general_ci default NULL,
  `handle` varchar(32) collate latin1_general_ci default NULL,
  `passwd` varchar(32) collate latin1_general_ci default NULL,
  `email` varchar(128) collate latin1_general_ci default NULL,
  `owner_user_id` int(11) default '0',
  `owner_group_id` int(11) default '0',
  `lastlogin` datetime default '1970-01-01 00:00:00',
  `is_active` tinyint(1) default '1',
  UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `messu_archive`
-- 

CREATE TABLE `messu_archive` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) collate latin1_general_ci NOT NULL default '',
  `user_from` varchar(40) collate latin1_general_ci NOT NULL default '',
  `user_to` text collate latin1_general_ci,
  `user_cc` text collate latin1_general_ci,
  `user_bcc` text collate latin1_general_ci,
  `subject` varchar(255) collate latin1_general_ci default NULL,
  `body` text collate latin1_general_ci,
  `hash` varchar(32) collate latin1_general_ci default NULL,
  `replyto_hash` varchar(32) collate latin1_general_ci default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) collate latin1_general_ci default NULL,
  `isReplied` char(1) collate latin1_general_ci default NULL,
  `isFlagged` char(1) collate latin1_general_ci default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `messu_messages`
-- 

CREATE TABLE `messu_messages` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) collate latin1_general_ci NOT NULL default '',
  `user_from` varchar(200) collate latin1_general_ci NOT NULL default '',
  `user_to` text collate latin1_general_ci,
  `user_cc` text collate latin1_general_ci,
  `user_bcc` text collate latin1_general_ci,
  `subject` varchar(255) collate latin1_general_ci default NULL,
  `body` text collate latin1_general_ci,
  `hash` varchar(32) collate latin1_general_ci default NULL,
  `replyto_hash` varchar(32) collate latin1_general_ci default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) collate latin1_general_ci default NULL,
  `isReplied` char(1) collate latin1_general_ci default NULL,
  `isFlagged` char(1) collate latin1_general_ci default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `messu_sent`
-- 

CREATE TABLE `messu_sent` (
  `msgId` int(14) NOT NULL auto_increment,
  `user` varchar(40) collate latin1_general_ci NOT NULL default '',
  `user_from` varchar(40) collate latin1_general_ci NOT NULL default '',
  `user_to` text collate latin1_general_ci,
  `user_cc` text collate latin1_general_ci,
  `user_bcc` text collate latin1_general_ci,
  `subject` varchar(255) collate latin1_general_ci default NULL,
  `body` text collate latin1_general_ci,
  `hash` varchar(32) collate latin1_general_ci default NULL,
  `replyto_hash` varchar(32) collate latin1_general_ci default NULL,
  `date` int(14) default NULL,
  `isRead` char(1) collate latin1_general_ci default NULL,
  `isReplied` char(1) collate latin1_general_ci default NULL,
  `isFlagged` char(1) collate latin1_general_ci default NULL,
  `priority` int(2) default NULL,
  PRIMARY KEY  (`msgId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `partners`
-- 

CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL default '0',
  `partner_number` varchar(32) collate latin1_general_ci NOT NULL default '',
  `partner_type` enum('customer','supplier','staff') collate latin1_general_ci default NULL,
  `first_name` varchar(64) collate latin1_general_ci default NULL,
  `last_name` varchar(64) collate latin1_general_ci default NULL,
  `adress` varchar(64) collate latin1_general_ci default NULL,
  `city` varchar(64) collate latin1_general_ci default NULL,
  `zip_code` int(11) default NULL,
  `phone` varchar(64) collate latin1_general_ci default NULL,
  `cell_phone` varchar(64) collate latin1_general_ci default NULL,
  `mail` varchar(64) collate latin1_general_ci default NULL,
  `web_site` varchar(64) collate latin1_general_ci default NULL,
  `activity` varchar(64) collate latin1_general_ci default NULL,
  `company` varchar(64) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `products`
-- 

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL default '0',
  `product_number` varchar(16) collate latin1_general_ci NOT NULL default '',
  `product_state` varchar(16) collate latin1_general_ci NOT NULL default 'init',
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `product_description` varchar(128) collate latin1_general_ci default NULL,
  `product_indice` varchar(8) collate latin1_general_ci default NULL,
  `open_date` int(11) default NULL,
  `project_id` int(11) default NULL,
  PRIMARY KEY  (`product_id`),
  UNIQUE KEY `UC_product_number` (`product_number`),
  KEY `FK_products_1` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `products_container_documents`
-- 

CREATE TABLE `products_container_documents` (
  `document_id` int(11) NOT NULL default '0',
  `product_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`document_id`,`product_id`),
  KEY `FK_products_container_documents_1` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `product_history`
-- 

CREATE TABLE `product_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_by` varchar(32) collate latin1_general_ci default NULL,
  `action_date` int(11) default NULL,
  `state_before` varchar(32) collate latin1_general_ci default NULL,
  `state_after` varchar(32) collate latin1_general_ci default NULL,
  `comments` text collate latin1_general_ci,
  `indice_before` varchar(8) collate latin1_general_ci default NULL,
  `indice_after` decimal(10,0) default NULL,
  `action_name` varchar(32) collate latin1_general_ci default NULL,
  `product_id` int(11) NOT NULL default '0',
  `product_indice_id` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `projects`
-- 

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL default '0',
  `project_number` varchar(16) collate latin1_general_ci NOT NULL default '',
  `project_description` varchar(128) collate latin1_general_ci default NULL,
  `project_state` varchar(16) collate latin1_general_ci NOT NULL default 'init',
  `default_process_id` int(11) default NULL,
  `project_indice_id` int(11) default NULL,
  `area_id` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  `link_id` int(11) default NULL,
  PRIMARY KEY  (`project_id`),
  UNIQUE KEY `UC_project_number` (`project_number`),
  KEY `FK_projects_1` (`project_indice_id`),
  KEY `FK_projects_2` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_bookshop_rel`
-- 

CREATE TABLE `project_bookshop_rel` (
  `project_id` int(11) default NULL,
  `bookshop_id` int(11) default NULL,
  UNIQUE KEY `UC_project_bookshop_rel` (`project_id`,`bookshop_id`),
  KEY `FK_project_bookshop_rel_1` (`bookshop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_cadlib_rel`
-- 

CREATE TABLE `project_cadlib_rel` (
  `project_id` int(11) default NULL,
  `cadlib_id` int(11) default NULL,
  UNIQUE KEY `UC_project_cadlib_rel` (`project_id`,`cadlib_id`),
  KEY `FK_project_cadlib_rel_1` (`cadlib_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_container_rel`
-- 

CREATE TABLE `project_container_rel` (
  `project_id` int(11) default NULL,
  `container_id` int(11) default NULL,
  `container_type` varchar(64) collate latin1_general_ci NOT NULL default '',
  UNIQUE KEY `IDX_project_container_rel_1` (`project_id`,`container_id`,`container_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_doctype_process`
-- 

CREATE TABLE `project_doctype_process` (
  `link_id` int(11) NOT NULL default '0',
  `process_id` int(11) default NULL,
  `project_id` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `Uniq_doctype_project` (`doctype_id`,`project_id`),
  KEY `FK_project_doctype_process_3` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_history`
-- 

CREATE TABLE `project_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) collate latin1_general_ci NOT NULL default '',
  `action_by` varchar(32) collate latin1_general_ci NOT NULL default '',
  `action_date` int(11) NOT NULL default '0',
  `project_id` int(11) NOT NULL default '0',
  `project_number` varchar(16) collate latin1_general_ci default NULL,
  `project_description` varchar(128) collate latin1_general_ci default NULL,
  `project_state` varchar(16) collate latin1_general_ci default NULL,
  `project_indice_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `open_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `close_date` int(11) default NULL,
  `area_id` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_mockup_rel`
-- 

CREATE TABLE `project_mockup_rel` (
  `project_id` int(11) default NULL,
  `mockup_id` int(11) default NULL,
  UNIQUE KEY `UC_project_mockup_rel` (`project_id`,`mockup_id`),
  KEY `FK_project_mockup_rel_1` (`mockup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `project_partner_rel`
-- 

CREATE TABLE `project_partner_rel` (
  `project_id` int(11) default NULL,
  `partner_id` int(11) default NULL,
  `role` varchar(64) collate latin1_general_ci default NULL,
  UNIQUE KEY `IDX_project_partner_rel_1` (`partner_id`,`project_id`),
  KEY `FK_project_partner_rel_2` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `tiki_user_tasks`
-- 

CREATE TABLE `tiki_user_tasks` (
  `taskId` int(14) NOT NULL auto_increment,
  `last_version` int(4) NOT NULL default '0',
  `user` varchar(40) collate latin1_general_ci NOT NULL default '',
  `creator` varchar(200) collate latin1_general_ci NOT NULL default '',
  `public_for_group` int(11) default NULL,
  `rights_by_creator` char(1) collate latin1_general_ci default NULL,
  `created` int(14) NOT NULL default '0',
  `status` char(1) collate latin1_general_ci default NULL,
  `priority` int(2) default NULL,
  `completed` int(14) default NULL,
  `percentage` int(4) default NULL,
  PRIMARY KEY  (`taskId`),
  UNIQUE KEY `creator` (`creator`,`created`),
  UNIQUE KEY `creator_2` (`creator`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `tiki_user_tasks_history`
-- 

CREATE TABLE `tiki_user_tasks_history` (
  `belongs_to` int(14) NOT NULL default '0',
  `task_version` int(4) NOT NULL default '0',
  `title` varchar(250) collate latin1_general_ci NOT NULL default '',
  `description` text collate latin1_general_ci,
  `start` int(14) default NULL,
  `end` int(14) default NULL,
  `lasteditor` varchar(200) collate latin1_general_ci NOT NULL default '',
  `lastchanges` int(14) NOT NULL default '0',
  `priority` int(2) NOT NULL default '3',
  `completed` int(14) default NULL,
  `deleted` int(14) default NULL,
  `status` char(1) collate latin1_general_ci default NULL,
  `percentage` int(4) default NULL,
  `accepted_creator` char(1) collate latin1_general_ci default NULL,
  `accepted_user` char(1) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`belongs_to`,`task_version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container`
-- 

CREATE TABLE `container` (
  `id` int(11) NOT NULL default '0',
  `number` varchar(16) collate latin1_general_ci NOT NULL default '',
  `state` varchar(16) collate latin1_general_ci NOT NULL default 'init',
  `description` varchar(128) collate latin1_general_ci default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `access_code` int(11) NULL default '0',
  `indice_id` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_file_path` text collate latin1_general_ci NOT NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `type` varchar(10) collate latin1_general_ci NOT NULL default 'container',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `UC_container_number` (`number`),
  KEY `FK_container_1` (`indice_id`),
  KEY `FK_container_2` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_categories`
-- 
CREATE TABLE `container_categories` (
  `category_id` int(11) NOT NULL default '0',
  `category_number` varchar(32) collate latin1_general_ci default NULL,
  `category_description` text collate latin1_general_ci,
  `category_icon` varchar(32) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `UC_category_number` (`category_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_category_rel`
-- 
CREATE TABLE `container_category_rel` (
  `container_id` int(11) NOT NULL default '0',
  `category_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`container_id`,`category_id`),
  KEY `FK_container_category_rel_1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_cont_metadata`
-- 

CREATE TABLE `container_cont_metadata` (
  `field_name` varchar(32) collate latin1_general_ci NOT NULL default '',
  `field_description` varchar(128) collate latin1_general_ci NOT NULL default '',
  `field_type` varchar(32) collate latin1_general_ci NOT NULL default '',
  `field_regex` varchar(255) collate latin1_general_ci default NULL,
  `field_required` int(1) NOT NULL default '0',
  `field_multiple` int(1) NOT NULL default '0',
  `field_size` int(11) default NULL,
  `return_name` int(1) NOT NULL default '0',
  `field_list` varchar(255) collate latin1_general_ci default NULL,
  `field_where` varchar(255) collate latin1_general_ci default NULL,
  `is_hide` int(1) NOT NULL default '0',
  PRIMARY KEY  (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_doctype_process`
-- 

CREATE TABLE `container_doctype_process` (
  `link_id` int(11) NOT NULL default '0',
  `doctype_id` int(11) default NULL,
  `process_id` int(11) default NULL,
  `container_id` int(11) default NULL,
  `category_id` int(11) default NULL,
  PRIMARY KEY  (`link_id`),
  KEY `FK_container_doctype_process_1` (`doctype_id`),
  KEY `FK_container_doctype_process_3` (`category_id`),
  KEY `FK_container_doctype_process_4` (`container_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_documents`
-- 

CREATE TABLE `container_documents` (
  `document_id` int(11) NOT NULL default '0',
  `document_number` varchar(128) collate latin1_general_ci NOT NULL default '',
  `document_state` varchar(32) collate latin1_general_ci NOT NULL default 'init',
  `document_access_code` int(11) NOT NULL default '0',
  `document_version` int(11) NOT NULL default '1',
  `container_id` int(11) NOT NULL default '0',
  `document_indice_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `doctype_id` int(11) NOT NULL default '0',
  `default_process_id` int(11) default NULL,
  `category_id` int(11) default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `designation` varchar(128) collate latin1_general_ci default NULL,
  `issued_from_document` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` decimal(10,0) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `ci` varchar(255) collate latin1_general_ci default NULL,
  `type_gc` varchar(255) collate latin1_general_ci default NULL,
  `work_unit` varchar(255) collate latin1_general_ci default NULL,
  `father_ds` varchar(255) collate latin1_general_ci default NULL,
  `need_calcul` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`document_id`),
  UNIQUE KEY `IDX_container_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_container_documents_2` (`doctype_id`),
  KEY `FK_container_documents_3` (`document_indice_id`),
  KEY `FK_container_documents_4` (`container_id`),
  KEY `FK_container_documents_5` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------
-- 
-- Structure de la table `container_documents_history`
-- 

CREATE TABLE `container_documents_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) collate latin1_general_ci NOT NULL default '',
  `action_by` varchar(64) collate latin1_general_ci NOT NULL default '',
  `action_date` int(11) NOT NULL default '0',
  `document_id` int(11) default NULL,
  `container_id` int(11) default NULL,
  `document_number` varchar(128) collate latin1_general_ci default NULL,
  `document_state` varchar(32) collate latin1_general_ci default NULL,
  `document_access_code` int(11) default NULL,
  `document_version` int(11) default NULL,
  `designation` varchar(128) collate latin1_general_ci default NULL,
  `check_out_by` int(11) default NULL,
  `check_out_date` int(11) default NULL,
  `issued_from_document` int(11) default NULL,
  `update_date` int(11) default NULL,
  `update_by` int(11) default NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `document_indice_id` int(11) default NULL,
  `doctype_id` int(11) default NULL,
  `instance_id` int(11) default NULL,
  `activity_id` int(11) default NULL,
  `issued_from_document` int(11) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------
-- 
-- Structure de la table `container_import_history`
-- 

CREATE TABLE `container_import_history` (
  `import_order` int(11) NOT NULL default '0',
  `container_id` int(11) NOT NULL default '0',
  `package_file_name` varchar(128) collate latin1_general_ci NOT NULL default '',
  `description` varchar(64) collate latin1_general_ci default NULL,
  `state` varchar(32) collate latin1_general_ci default NULL,
  `import_date` int(11) default NULL,
  `import_by` int(11) default NULL,
  `import_errors_report` text collate latin1_general_ci,
  `import_logfiles_file` text collate latin1_general_ci,
  `package_file_path` text collate latin1_general_ci,
  `package_file_extension` varchar(32) collate latin1_general_ci default NULL,
  `package_file_mtime` int(11) default NULL,
  `package_file_size` int(11) default NULL,
  `package_file_md5` varchar(128) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------
-- 
-- Structure de la table `container_doc_files`
-- 

CREATE TABLE `container_doc_files` (
  `file_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file_name` varchar(128) collate latin1_general_ci NOT NULL,
  `file_used_name` varchar(128) collate latin1_general_ci NOT NULL,
  `file_path` text collate latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL default '1',
  `file_access_code` int(11) NOT NULL default '0',
  `file_root_name` varchar(128) collate latin1_general_ci NOT NULL,
  `file_extension` varchar(16) collate latin1_general_ci default NULL,
  `file_state` varchar(16) collate latin1_general_ci NOT NULL default 'init',
  `file_type` varchar(128) collate latin1_general_ci NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128) collate latin1_general_ci default NULL,
  `file_checkout_by` int(11) default NULL,
  `file_checkout_date` int(11) default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `uniq_file_name` (`file_name`,`file_path`(512)),
  KEY `FK_container_doc_files_1` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------
-- 
-- Structure de la table `container_doc_rel`
-- 

CREATE TABLE `container_doc_rel` (
  `dr_link_id` int(11) NOT NULL default '0',
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) NOT NULL default '15',
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_history`
-- 

CREATE TABLE `container_history` (
  `histo_order` int(11) NOT NULL default '0',
  `action_name` varchar(32) collate latin1_general_ci default NULL,
  `action_by` varchar(32) collate latin1_general_ci default NULL,
  `action_date` int(11) default NULL,
  `container_id` int(11) NOT NULL default '0',
  `container_number` varchar(16) collate latin1_general_ci default NULL,
  `container_state` varchar(16) collate latin1_general_ci default NULL,
  `container_description` varchar(128) collate latin1_general_ci default NULL,
  `container_indice_id` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_file_path` text collate latin1_general_ci,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  PRIMARY KEY  (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_metadata`
-- 

CREATE TABLE `container_metadata` (
  `field_name` varchar(32) collate latin1_general_ci NOT NULL default '',
  `field_description` varchar(128) collate latin1_general_ci NOT NULL default '',
  `field_type` varchar(32) collate latin1_general_ci NOT NULL default '',
  `field_regex` varchar(255) collate latin1_general_ci default NULL,
  `field_required` int(1) NOT NULL default '0',
  `field_multiple` int(1) NOT NULL default '0',
  `field_size` int(11) default NULL,
  `return_name` int(1) NOT NULL default '0',
  `field_list` varchar(255) collate latin1_general_ci default NULL,
  `field_where` varchar(255) collate latin1_general_ci default NULL,
  `is_hide` int(1) NOT NULL default '0',
  `table_name` varchar(32) collate latin1_general_ci default NULL,
  `field_for_value` varchar(32) collate latin1_general_ci default NULL,
  `field_for_display` varchar(32) collate latin1_general_ci default NULL,
  `date_format` varchar(32) collate latin1_general_ci default NULL,
  `adv_select` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

-- 
-- Structure de la table `container_metadata_rel`
-- 

CREATE TABLE `container_metadata_rel` (
  `link_id` int(11) NOT NULL default '0',
  `container_id` int(11) NOT NULL default '0',
  `field_name` varchar(32) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`link_id`),
  UNIQUE KEY `container_metadata_rel_uniq` (`container_id`,`field_name`),
  KEY `container_id` (`container_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Contraintes pour les tables exportées
-- 

-- 
-- Contraintes pour la table `products`
-- 
ALTER TABLE `products`
  ADD CONSTRAINT `FK_products_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `products_container_documents`
-- 
ALTER TABLE `products_container_documents`
  ADD CONSTRAINT `FK_products_container_documents_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `FK_products_container_documents_2` FOREIGN KEY (`document_id`) REFERENCES `container_documents` (`document_id`);

-- 
-- Contraintes pour la table `projects`
-- 
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_indice_id`) REFERENCES `container_indice` (`indice_id`),
  ADD CONSTRAINT `FK_projects_2` FOREIGN KEY (`link_id`) REFERENCES `project_doctype_process` (`link_id`);

-- 
-- Contraintes pour la table `project_bookshop_rel`
-- 
ALTER TABLE `project_bookshop_rel`
  ADD CONSTRAINT `FK_project_bookshop_rel_1` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  ADD CONSTRAINT `FK_project_bookshop_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `project_cadlib_rel`
-- 
ALTER TABLE `project_cadlib_rel`
  ADD CONSTRAINT `FK_project_cadlib_rel_1` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  ADD CONSTRAINT `FK_project_cadlib_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `project_doctype_process`
-- 
ALTER TABLE `project_doctype_process`
  ADD CONSTRAINT `FK_project_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_project_doctype_process_3` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `project_mockup_rel`
-- 
ALTER TABLE `project_mockup_rel`
  ADD CONSTRAINT `FK_project_mockup_rel_1` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  ADD CONSTRAINT `FK_project_mockup_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `project_partner_rel`
-- 
ALTER TABLE `project_partner_rel`
  ADD CONSTRAINT `FK_project_partner_rel_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_project_partner_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `container`
-- 
ALTER TABLE `container`
  ADD CONSTRAINT `FK_container_1` FOREIGN KEY (`container_indice_id`) REFERENCES `container_indice` (`indice_id`),
  ADD CONSTRAINT `FK_container_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

-- 
-- Contraintes pour la table `container_category_rel`
-- 
ALTER TABLE `container_category_rel`
  ADD CONSTRAINT `FK_container_category_rel_1` FOREIGN KEY (`category_id`) REFERENCES `container_categories` (`category_id`),
  ADD CONSTRAINT `FK_container_category_rel_2` FOREIGN KEY (`container_id`) REFERENCES `container` (`container_id`);

-- 
-- Contraintes pour la table `container_doctype_process`
-- 
ALTER TABLE `container_doctype_process`
  ADD CONSTRAINT `FK_container_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_container_doctype_process_3` FOREIGN KEY (`category_id`) REFERENCES `container_categories` (`category_id`),
  ADD CONSTRAINT `FK_container_doctype_process_4` FOREIGN KEY (`container_id`) REFERENCES `container` (`container_id`);

-- 
-- Contraintes pour la table `container_documents`
-- 
ALTER TABLE `container_documents`
  ADD CONSTRAINT `FK_container_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_container_documents_3` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`),
  ADD CONSTRAINT `FK_container_documents_4` FOREIGN KEY (`container_id`) REFERENCES `container` (`container_id`),
  ADD CONSTRAINT `FK_container_documents_5` FOREIGN KEY (`category_id`) REFERENCES `container_categories` (`category_id`);

-- 
-- Contraintes pour la table `container_doc_files`
-- 
ALTER TABLE `container_doc_files`
  ADD CONSTRAINT `FK_container_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `container_documents` (`document_id`);

-- 
-- Contraintes pour la table `container_doc_rel`
-- 
ALTER TABLE `container_doc_rel`
  ADD CONSTRAINT `FK_container_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `container_documents` (`document_id`) ON DELETE CASCADE;
  

ALTER TABLE `container_cont_metadata` 
ADD `table_name` VARCHAR( 32 ) NULL ,
ADD `field_for_value` VARCHAR( 32 ) NULL ,
ADD `field_for_display` VARCHAR( 32 ) NULL ,
ADD `date_format` VARCHAR( 32 ) NULL ,
ADD `display_both` BOOL NOT NULL ,
ADD `adv_select` BOOL NOT NULL ;


-- 
-- ALTER TABLE `bookshop_doc_files` ADD `file_used_name` VARCHAR( 128 ) NOT NULL COMMENT 'nom d usage' AFTER `file_name`;
-- 
-- ALTER TABLE `cadlib_doc_files` ADD `file_used_name` VARCHAR( 128 ) NOT NULL COMMENT 'nom d usage' AFTER `file_name`;
-- 
-- ALTER TABLE `mockup_doc_files` ADD `file_used_name` VARCHAR( 128 ) NOT NULL COMMENT 'nom d usage' AFTER `file_name`;
-- 
-- ALTER TABLE `container_doc_files` ADD `file_used_name` VARCHAR( 128 ) NOT NULL COMMENT 'nom d usage' AFTER `file_name`;
-- 
-- ALTER TABLE `bookshops` ADD `access_code` INT NULL DEFAULT NULL AFTER `file_only` 
-- ALTER TABLE `cadlibs` ADD `access_code` INT NULL DEFAULT NULL AFTER `file_only` 
-- ALTER TABLE `mockups` ADD `access_code` INT NULL DEFAULT NULL AFTER `file_only` 
-- ALTER TABLE `container` ADD `access_code` INT NULL DEFAULT NULL AFTER `file_only` 
