SELECT doc.* 
	FROM cadlib_documents as doc 
		JOIN cadlibs as cont ON cont.cadlib_id = doc.cadlib_id 
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM bookshop_documents as doc 
		JOIN bookshops as cont ON cont.bookshop_id = doc.bookshop_id 
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM mockup_documents as doc 
		JOIN mockups as cont ON cont.mockup_id = doc.mockup_id 
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM workitem_documents as doc 
		JOIN workitems as cont ON cont.workitem_id = doc.workitem_id 
ORDER BY document_number ASC

SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.cadlib_id AS cont_id , cont.container_type AS container_type , cont.cadlib_number AS cont_number FROM cadlib_documents as doc JOIN cadlibs as cont ON cont.cadlib_id = doc.cadlib_id WHERE (doc.document_number LIKE '%V21521354%')

SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.bookshop_id AS cont_id , cont.container_type AS container_type , cont.bookshop_number AS cont_number FROM bookshop_documents as doc JOIN bookshops as cont ON cont.bookshop_id = doc.bookshop_id WHERE (doc.document_number LIKE '%V21521354%')

SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.mockup_id AS cont_id , cont.container_type AS container_type , cont.mockup_number AS cont_number FROM mockup_documents as doc JOIN mockups as cont ON cont.mockup_id = doc.mockup_id WHERE (doc.document_number LIKE '%V21521354%')

SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.workitem_id AS cont_id , cont.container_type AS container_type , cont.workitem_number AS cont_number FROM workitem_documents as doc JOIN workitems as cont ON cont.workitem_id = doc.workitem_id WHERE (doc.document_number LIKE '%V21521354%')





 