DROP view view_rt;
CREATE ALGORITHM=UNDEFINED 
VIEW `view_rt` AS 
SELECT 
rt.document_id AS rt_id, 
rt.document_number AS rt_number, 
rt.document_state AS rt_status, 
rt.document_access_code AS rt_access_code, 
rt.open_date AS rt_open_date, 
applydocs.document_id AS applyto_id,
applydocs.document_number AS applyto_number, 
applydocs.document_indice_id AS applyto_indice,
applydocs.document_version AS applyto_version,
applydocs.document_state AS applyto_status,
linked_docs.document_number AS from_number,
linked_docs.document_indice_id AS from_version
FROM workitem_documents AS rt
LEFT OUTER JOIN workitem_documents AS linked_docs ON linked_docs.document_id = rt.rt_apply_to
INNER JOIN workitem_documents AS applydocs ON linked_docs.document_number = applydocs.document_number


-- 
-- WHERE rt.doctype_id=188
-- AND applydocs.document_indice_id >= linked_docs.document_indice_id;
--
