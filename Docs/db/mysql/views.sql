CREATE OR REPLACE VIEW `view_rejected_doc` AS 
SELECT 
`histo`.`document_id` AS `docid`,
`histo`.`document_number` AS `document_number`,
`histo`.`document_state` AS `state`,
`histo`.`action_by` AS `action_by`,
`histo`.`action_name` AS action_name,
`container`.`workitem_id` AS container_id,
`container`.`workitem_number` AS container_number,
FROM_UNIXTIME(`instance`.`started`) AS `started`,
`instance`.`owner` AS `owner`,
`instance`.`status` AS `status`,
SUBSTRING(`instance`.`properties`, 1, 1000) AS `properties`
FROM 
`workitem_documents_history` AS `histo` 
JOIN 
`galaxia_instances` AS `instance` 
ON (`histo`.`instance_id` = `instance`.`instanceId`)
JOIN 
`workitems` AS `container` 
ON (histo.workitem_id = `container`.`workitem_id`);

-- DOCUMENTS --
SELECT doc.* FROM cadlib_documents as doc 
	JOIN cadlibs as cont ON cont.cadlib_id = doc.cadlib_id
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM bookshop_documents as doc 
		JOIN bookshops as cont ON cont.bookshop_id = doc.bookshop_id 
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM mockup_documents as doc 
		JOIN mockups as cont ON cont.mockup_id = doc.mockup_id 
UNION 
SELECT doc.document_number , doc.doctype_id , doc.document_id 
	FROM workitem_documents as doc 
		JOIN workitems as cont ON cont.workitem_id = doc.workitem_id 
ORDER BY document_number ASC
SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.cadlib_id AS cont_id , cont.container_type AS container_type , cont.cadlib_number AS cont_number FROM cadlib_documents as doc JOIN cadlibs as cont ON cont.cadlib_id = doc.cadlib_id WHERE (doc.document_number LIKE '%V21521354%')
SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.bookshop_id AS cont_id , cont.container_type AS container_type , cont.bookshop_number AS cont_number FROM bookshop_documents as doc JOIN bookshops as cont ON cont.bookshop_id = doc.bookshop_id WHERE (doc.document_number LIKE '%V21521354%')
SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.mockup_id AS cont_id , cont.container_type AS container_type , cont.mockup_number AS cont_number FROM mockup_documents as doc JOIN mockups as cont ON cont.mockup_id = doc.mockup_id WHERE (doc.document_number LIKE '%V21521354%')
SELECT doc.document_number , doc.doctype_id , doc.document_id, cont.workitem_id AS cont_id , cont.container_type AS container_type , cont.workitem_number AS cont_number FROM workitem_documents as doc JOIN workitems as cont ON cont.workitem_id = doc.workitem_id WHERE (doc.document_number LIKE '%V21521354%')



