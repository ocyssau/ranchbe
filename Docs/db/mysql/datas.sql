-- ------------------------------------------------------ 
-- Données de la table `liveuser_groups`
-- 

INSERT INTO `liveuser_groups` (`group_id`, `group_type`, `group_define_name`, `group_description`, `is_active`, `owner_user_id`, `owner_group_id`) VALUES 
(1, 0, 'concepteurs', 'Concepteurs CAO', 1, 0, 0),
(2, 0, 'groupLeader', 'chef de groupe de conception', 1, 0, 0),
(3, 0, 'Leader', 'Charg� d''affaire', 1, 0, 0),
(4, 0, 'checkers', 'verificateurs des projets CAO', 1, 0, 0),
(5, 0, 'LibManager', 'Gestionnaires des librairies CAO', 1, 0, 0),
(6, 0, 'BibManager', 'Gestionnaire des bibliotheques', 1, 0, 0);

INSERT INTO `liveuser_groupusers` (`perm_user_id`, `group_id`) VALUES
(2, 1);

CREATE TABLE IF NOT EXISTS `liveuser_groups_seq` (
  `sequence` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

INSERT INTO `liveuser_groups_seq` (`sequence`) VALUES 
(19);

-- ------------------------------------------------------ 
-- Données de la table `liveuser_users`
-- 

-- Password for admin = admin00
-- Password for user = user00

INSERT INTO `liveuser_users` (`auth_user_id`, `handle`, `passwd`, `email`, `owner_user_id`, `owner_group_id`, `lastlogin`, `is_active`) VALUES 
('1', 'admin', 'a5a30bc4c47888cd59c4e9df68d80242', NULL, 0, 0, '2000-01-01 00:00:00', 1),
('2', 'user', '0baf78e0dcadd5125fbb6ae50514b3e7', NULL, 0, 0, '2000-01-01 00:00:00', 1);

CREATE TABLE IF NOT EXISTS `liveuser_users_seq` (
  `sequence` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `liveuser_users_seq` (`sequence`) VALUES 
(9);

-- ------------------------------------------------------ 
-- Données de la table `liveuser_perm_users`
-- 

INSERT INTO `liveuser_perm_users` (`perm_user_id`, `auth_user_id`, `auth_container_name`, `perm_type`) VALUES 
(1, '1', 'DB', 4),
(2, '2', 'DB', 0);

CREATE TABLE IF NOT EXISTS `liveuser_perm_users_seq` (
  `sequence` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `liveuser_perm_users_seq` (`sequence`) VALUES 
(9);

-- ------------------------------------------------------ 
-- Données de la table `liveuser_rights`
-- 

INSERT INTO `liveuser_rights` (`right_id`, `area_id`, `right_define_name`, `right_description`, `has_implied`) VALUES
(1, 1, 'partner_create', 'create a partner', 0),
(2, 1, 'partner_suppress', 'suppress a partner', 0),
(3, 1, 'partner_modify', 'modify partner properties', 0),
(4, 1, 'doctype_create', 'create a doctype', 0),
(5, 1, 'doctype_suppress', 'suppress a doctype', 0),
(6, 1, 'doctype_modify', 'modify doctype properties', 0),
(7, 1, 'user_create', 'create a user', 0),
(8, 1, 'user_suppress', 'suppress a user', 0),
(9, 1, 'user_modify', 'modify user properties', 0),
(10, 1, 'user_assign_group', 'assign a user to a group', 0),
(11, 1, 'user_assign_perm', 'assign permissions to a user', 0),
(12, 1, 'group_create', 'create a group', 0),
(13, 1, 'group_suppress', 'suppress a group', 0),
(14, 1, 'group_modify', 'modify group properties', 0),
(15, 1, 'group_assign_perm', 'assign permissions to a group', 0),
(16, 1, 'group_include', 'assign group to a another group', 0),
(17, 1, 'messu-archive', 'archive a message', 0),
(18, 1, 'messu-broadcast', 'send message to all users', 0),
(19, 1, 'messu-compose', 'compose a message', 0),
(20, 1, 'messu-mailbox', 'manage mailbox', 0),
(21, 1, 'messu-sent', 'sent a message', 0),
(22, 1, 'task-create', 'create a task', 0),
(23, 1, 'task-changestate', 'change state of a task', 0),
(24, 1, 'task-trash', 'put a task in trash', 0),
(25, 1, 'task-undotrash', 'untrash a task', 0),
(26, 1, 'task-emptytrash', 'empty task trash', 0),
(27, 1, 'container_document_instance', 'manage link between workflow and documents', 0),
(28, 5, 'project_create', 'create a new project', 0),
(29, 5, 'project_suppress', 'suppress a project', 0),
(30, 5, 'project_get', 'get list of the project', 0),
(31, 5, 'container_archive', 'archive a project', 0),
(32, 5, 'container_history', 'manage history of the project', 0),
(33, 5, 'project_permissions', 'manage users and permissions of project', 0),
(34, 5, 'project_links', 'manage links of project', 0),
(35, 10, 'container_create', 'create a new workitem', 0),
(36, 10, 'container_suppress', 'suppress a workitem', 0),
(37, 10, 'container_get', 'get list of the workitem', 0),
(38, 10, 'container_archive', 'archive a workitem', 0),
(39, 10, 'container_history', 'manage history of the workitem', 0),
(40, 10, 'container_doctype', 'manage doctypes of the workitem', 0),
(41, 10, 'container_category', 'manage categories of the workitem', 0),
(42, 10, 'container_get_stat', 'display stats about the workitem', 0),
(43, 15, 'container_create', 'create a new mockup', 0),
(44, 15, 'container_suppress', 'suppress a mockup', 0),
(45, 15, 'container_get', 'get list of the mockup', 0),
(46, 15, 'container_archive', 'archive a mockup', 0),
(47, 15, 'container_history', 'manage history of the mockup', 0),
(48, 15, 'container_import', 'import file in mockup', 0),
(49, 15, 'container_import_history', 'manage import history', 0),
(50, 15, 'container_file_manage', 'manage files imported in mockup', 0),
(51, 15, 'container_document_get', 'list documents of the mockup', 0),
(52, 15, 'container_document_manage', 'create/modify documents in mockup', 0),
(53, 15, 'container_document_suppress', 'suppress documents of mockup', 0),
(54, 15, 'container_document_archive', 'manage documents archive of mockup', 0),
(55, 15, 'container_document_change_indice', 'change indice of a mockup documents', 0),
(56, 15, 'container_document_change_state', 'change state of a mockup documents', 0),
(57, 15, 'container_document_link_file', 'manage files linked to documents', 0),
(58, 15, 'container_document_history', 'manage documents history', 0),
(59, 15, 'container_document_version', 'manage documents versions', 0),
(60, 15, 'container_doctype', 'manage doctypes of the mockup', 0),
(61, 15, 'container_category', 'manage categories of the mockup', 0),
(62, 15, 'container_get_stat', 'display stats about the mockup', 0),
(63, 20, 'container_create', 'create a new bookshop', 0),
(64, 20, 'container_suppress', 'suppress a bookshop', 0),
(65, 20, 'container_get', 'get list of the bookshop', 0),
(66, 20, 'container_archive', 'archive a bookshop', 0),
(67, 20, 'container_history', 'manage history of the bookshop', 0),
(68, 20, 'container_import', 'import file in bookshop', 0),
(69, 20, 'container_import_history', 'manage import history', 0),
(70, 20, 'container_file_manage', 'manage files imported in bookshop', 0),
(71, 20, 'container_document_get', 'list documents of the bookshop', 0),
(72, 20, 'container_document_manage', 'create/modify documents in bookshop', 0),
(73, 20, 'container_document_suppress', 'suppress documents of bookshop', 0),
(74, 20, 'container_document_archive', 'manage documents archive of bookshop', 0),
(75, 20, 'container_document_change_indice', 'change indice of a bookshop documents', 0),
(76, 20, 'container_document_change_state', 'change state of a bookshop documents', 0),
(77, 20, 'container_document_link_file', 'manage files linked to documents', 0),
(78, 20, 'container_document_history', 'manage documents history', 0),
(79, 20, 'container_document_version', 'manage documents versions', 0),
(80, 20, 'container_doctype', 'manage doctypes of the bookshop', 0),
(81, 20, 'container_category', 'manage categories of the bookshop', 0),
(82, 20, 'container_get_stat', 'display stats about the bookshop', 0),
(83, 25, 'container_create', 'create a new cadlib', 0),
(84, 25, 'container_suppress', 'suppress a cadlib', 0),
(85, 25, 'container_get', 'get list of the cadlib', 0),
(86, 25, 'container_archive', 'archive a cadlib', 0),
(87, 25, 'container_history', 'manage history of the cadlib', 0),
(88, 25, 'container_import', 'import file in cadlib', 0),
(89, 25, 'container_import_history', 'manage import history', 0),
(90, 25, 'container_file_manage', 'manage files imported in cadlib', 0),
(91, 25, 'container_document_get', 'list documents of the cadlib', 0),
(92, 25, 'container_document_manage', 'create/modify documents in cadlib', 0),
(93, 25, 'container_document_suppress', 'suppress documents of cadlib', 0),
(94, 25, 'container_document_archive', 'manage documents archive of cadlib', 0),
(95, 25, 'container_document_change_indice', 'change indice of a cadlib documents', 0),
(96, 25, 'container_document_change_state', 'change state of a cadlib documents', 0),
(97, 25, 'container_document_link_file', 'manage files linked to documents', 0),
(98, 25, 'container_document_history', 'manage documents history', 0),
(99, 25, 'container_document_version', 'manage documents versions', 0),
(100, 25, 'container_doctype', 'manage doctypes of the cadlib', 0),
(101, 25, 'container_category', 'manage categories of the cadlib', 0),
(102, 25, 'container_get_stat', 'display stats about the cadlib', 0),
(103, 30, 'g-admin-activities', 'admin activities', 0),
(104, 30, 'g-admin-graph', 'admin graph', 0),
(105, 30, 'g-admin-instance', 'admin instance', 0),
(106, 30, 'g-admin-processes', 'admin process', 0),
(107, 30, 'g-admin-roles', 'admin roles', 0),
(108, 30, 'g-admin-shared-source', 'admin program of activities', 0),
(109, 30, 'g-monitor-instances', 'monitor instances', 0),
(110, 30, 'g-user-instances', 'admin users mapping', 0),
(111, 5, 'container_doctype', 'manage doctypes linked to project', 0),
(112, 15, 'container_document_unlock', 'unlock documents of mockup', 0),
(113, 20, 'container_document_unlock', 'unlock documents of bookshop', 0),
(114, 25, 'container_document_unlock', 'unlock documents of cadlib', 0),
(115, 15, 'container_document_assoc', 'associate manually a file to a document of mockup', 0),
(116, 20, 'container_document_assoc', 'associate manually a file to a document of bookshop', 0),
(117, 25, 'container_document_assoc', 'associate manually a file to a document of cadlib', 0),
(118, 15, 'container_document_change_number', 'change number of a mockup documents', 0),
(119, 20, 'container_document_change_number', 'change number of a bookshop documents', 0),
(120, 25, 'container_document_change_number', 'change number of a cadlib documents', 0),
(121, 10, 'container_metadata_suppress', 'suppress metadata of the workitem', 0),
(122, 10, 'container_metadata_create', 'create metadata of the workitem', 0),
(123, 10, 'container_metadata_modify', 'modify metadata of the workitem', 0),
(124, 10, 'document_metadata_suppress', 'suppress metadata of the document of workitem', 0),
(125, 10, 'document_metadata_create', 'create metadata of the document of workitem', 0),
(126, 10, 'document_metadata_modify', 'modify metadata of the document of workitem', 0),
(127, 15, 'container_metadata_suppress', 'suppress metadata of the mockup', 0),
(128, 15, 'container_metadata_create', 'create metadata of the mockup', 0),
(129, 15, 'container_metadata_modify', 'modify metadata of the mockup', 0),
(130, 15, 'document_metadata_suppress', 'suppress metadata of the document of mockup', 0),
(131, 15, 'document_metadata_create', 'create metadata of the document of mockup', 0),
(132, 15, 'document_metadata_modify', 'modify metadata of the document of mockup', 0),
(133, 20, 'container_metadata_suppress', 'suppress metadata of the bookshop', 0),
(134, 20, 'container_metadata_create', 'create metadata of the bookshop', 0),
(135, 20, 'container_metadata_modify', 'modify metadata of the bookshop', 0),
(136, 20, 'document_metadata_suppress', 'suppress metadata of the document of bookshop', 0),
(137, 20, 'document_metadata_create', 'create metadata of the document of bookshop', 0),
(138, 20, 'document_metadata_modify', 'modify metadata of the document of bookshop', 0),
(139, 25, 'container_metadata_suppress', 'suppress metadata of the cadlib', 0),
(140, 25, 'container_metadata_create', 'create metadata of the cadlib', 0),
(141, 25, 'container_metadata_modify', 'modify metadata of the cadlib', 0),
(142, 25, 'document_metadata_suppress', 'suppress metadata of the document of cadlib', 0),
(143, 25, 'document_metadata_create', 'create metadata of the document of cadlib', 0),
(144, 25, 'document_metadata_modify', 'modify metadata of the document of cadlib', 0),
(145, 5, 'container_document_move', 'move document between container', 0),
(146, 5, 'container_document_copy', 'copy document', 0),
(147, 15, 'container_document_move', 'move document between container', 0),
(148, 15, 'container_document_copy', 'copy document', 0),
(149, 20, 'container_document_move', 'move document between container', 0),
(150, 20, 'container_document_copy', 'copy document', 0),
(151, 25, 'container_document_move', 'move document between container', 0),
(152, 25, 'container_document_copy', 'copy document', 0),
(153, 15, 'container_document_doctype_reset', 'recalculate the doctype of the documents of mockup', 0),
(154, 20, 'container_document_doctype_reset', 'recalculate the doctype of the documents of bookshop', 0),
(155, 25, 'container_document_doctype_reset', 'recalculate the doctype of the documents of cadlib', 0);

CREATE TABLE IF NOT EXISTS `liveuser_rights_seq` (
  `sequence` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=500 ;

INSERT INTO `liveuser_rights_seq` (`sequence`) VALUES
(499);

-- ------------------------------------------------------ 
-- Données de la table `liveuser_areas`
-- 

INSERT INTO `liveuser_areas` (`area_id`, `application_id`, `area_define_name`) VALUES 
(1, 1, 'ranchbe'),
(5, 1, 'project'),
(10, 1, 'workitem'),
(15, 1, 'mockup'),
(20, 1, 'bookshop'),
(25, 1, 'cadlib'),
(30, 1, 'galaxia');

CREATE TABLE IF NOT EXISTS `liveuser_areas_seq` (
  `sequence` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

INSERT INTO `liveuser_rights_seq` (`sequence`) VALUES 
(99);


-- ------------------------------------------------------ 
-- Données de la table `liveuser_applications`
-- 

INSERT INTO `liveuser_applications` (`application_id`, `application_define_name`) VALUES 
(1, 'ranchbe');

-- ------------------------------------------------------ 
-- Données de la table `partners`
-- 

INSERT INTO `partners` (`partner_id`, `partner_number`, `first_name`, `last_name`, `adress`, `city`, `zip_code`, `phone`, `mail`, `web_site`, `activity`) VALUES 
(1, 'Olivier CYSSAU', 'Olivier', 'CYSSAU', '', 'TOULOUSE', 31300, '', 'ocyssau@free.fr', 'www.ranchbe.com', 'informatique');

CREATE TABLE IF NOT EXISTS `partners_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `partners_seq` (`id`) VALUES (5);


-- ------------------------------------------------------ 
-- Donn�es de la table `doctypes`
-- 

INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES
(1, 'cao__cadds4x', 'Part cadds4x','0','','','','','', '_pd', 'cadds4', 'cadds.gif'),
(2, 'cao__cadds5', 'Parts cadds5','0','','','','','', '_fd', 'cadds5', 'cadds.gif'),
(3, 'cao__cadds_adrawc4', 'adraw cadds4x','0','','','','','', '.adrawc4', 'adrawc4', 'cadds.gif'),
(4, 'cao__cadds_adrawc5', 'adraw cadds5','0','','','','','', '.adrawc5', 'adrawc5', 'cadds.gif'),
(5, 'cao__cadds_camu', 'product cadds','0','','','','','', '_db', 'camu', 'cadds.gif'),
(6, 'cao__catpart', 'Fichier part catia','0','','','','','', '.CATPart', 'file', 'CATPart.gif'),
(7, 'cao__catdrawing', 'Fichier drawing catia','0','','','','','', '.CATDrawing', 'file', 'CATDrawing.gif'),
(8, 'cao__catproduct', 'Fichier product catia','0','','','','','', '.CATProduct', 'file', 'CATProduct.gif'),
(9, 'cao__catmodel', 'Fichier model catia','0','','','','','', '.model', 'file', 'model.gif'),
(10, 'cao__catalog', 'Fichier catalog catia','0','','','','','', '.catalog', 'file', ''),
(11, 'cao__catanalysis', 'Fichier analyse catia','0','','','','','', '.CATAnalysis', 'file', 'CATAnalysis.gif'),
(12, 'cao__catcatalog', 'Fichier analyse catia','0','','','','','', '.CATCatalog', 'file', 'CATCatalog.gif'),
(13, 'cao__catmaterial', 'Fichier material catia','0','','','','','', '.CATMaterial', 'file', 'CATMaterial.gif'),
(14, 'cao__catprocess', 'Fichier process catia','0','','','','','', '.CATProcess', 'file', 'CATProcess.gif'),
(15, 'cao__catsetting', 'Fichier setting catia','0','','','','','', '.CATSetting', 'file', 'CATSetting.gif'),
(16, 'cao__catscript', 'Fichier script catia','0','','','','','', '.CATScript', 'file', 'CATScript.gif'),
(17, 'cao__pstree', 'product ps','0','','','','','', '_ps', 'pstree', ''),
(18, 'cao__acad', 'Fichiers AutoCADdwg','0','','','','','', '.dwg', 'file', 'dwg.gif'),
(19, 'cao__drafting', 'Fichiers MATRA Prelude drafting','0','','','','','', '.drw', 'file', ''),
(20, 'cao__dxf', 'Fichiers AutoCAD','0','','','','','', '.dxf', 'file', 'dwf.gif'),
(21, 'cao__i-deas', 'Fichiers SDRC I-deas','0','','','','','', '.unv', 'file', ''),
(22, 'cao__iges', 'Format d''echange CAO IGES','0','','','','','', '.igs', 'file', ''),
(23, 'cao__pro_eng', 'Fichiers ProEngineer','0','','','','','', '.prt', 'file', ''),
(24, 'cao__set', 'Fichiers CAO set','0','','','','','', '.set', 'file', ''),
(25, 'cao__sla', 'Fichiers st�r�olithographie','0','','','','','', '.stl', 'file', ''),
(27, 'cao__step', 'Fichiers de donn�es STEP','0','','','','','', '.step', 'file', 'stp.gif'),
(28, 'cao__vda', 'Fichiers de surface','0','','','','','', '.vda', 'file', ''),
(29, 'text__pdf', 'Fichiers Adobe Acrobat','0','','','','','', '.pdf', 'file', 'pdf.gif'),
(30, 'text__postscript', 'Fichiers PostScript','0','','','','','', '.ps', 'file', 'ps.gif'),
(31, 'text__epostscript', 'Fichiers PostScript','0','','','','','', '.eps', 'file', 'eps.gif'),
(32, 'text__rtf', 'Format de texte enrichi','0','','','','','', '.rtf', 'file', 'rtf.gif'),
(33, 'text__htm', 'Fichiers HTML','0','','','','','', '.htm', 'file', 'html.gif'),
(34, 'text__html', 'Fichiers HTML','0','','','','','', '.html', 'file', 'htm.gif'),
(35, 'text__plain', 'Fichiers texte sans mise en forme','0','','','','','', '.txt', 'file', 'txt.gif'),
(36, 'text__tsv', 'Fichiers texte avec s�paration des valeurs par tabulations','0','','','','','', '.tsv', 'file', 'tsv.gif'),
(37, 'text__csv', 'Fichiers texte avec s�paration des valeurs par virgules','0','','','','','', '.csv', 'file', 'csv.gif'),
(38, 'text__msword', 'Fichiers msword','0','','','','','', '.doc', 'file', 'doc.gif'),
(39, 'text__msxls', 'Fichiers msexcel','0','','','','','', '.xls', 'file', 'xls.gif'),
(40, 'text__msppt', 'Fichiers msppt','0','','','','','', '.ppt', 'file', 'ppt.gif'),
(41, 'text__oowriter', 'Fichiers oo writer','0','','','','','', '.odt', 'file', 'odt.gif'),
(42, 'text__oocalc', 'Fichiers oo calc','0','','','','','', '.ods', 'file', 'ods.gif'),
(43, 'text__ooimpress', 'Fichiers oo impress','0','','','','','', '.odp', 'file', 'odp.gif'),
(44, 'model__msword', 'Fichiers model msword','0','','','','','', '.dot', 'file', 'dot.gif'),
(45, 'model__msxls', 'Fichiers model msexcel','0','','','','','', '.xlt', 'file', 'xls.gif'),
(46, 'model__msppt', 'Fichiers model msppt','0','','','','','', '.pot', 'file', 'pot.gif'),
(47, 'model__oowriter', 'Fichiers model oo writer','0','','','','','', '.ott', 'file', 'ott.gif'),
(48, 'model__oocalc', 'Fichiers model oo calc','0','','','','','', '.ots', 'file', 'ots.gif'),
(49, 'model__ooimpress', 'Fichiers model oo impress','0','','','','','', '.otp', 'file', 'otp.gif'),
(50, 'archive__gtar', 'Tar GNU','0','','','','','', '.gtar', 'file', 'gtar.gif'),
(51, 'archive__tar', 'Fichiers tar','0','','','','','', '.tar', 'file', 'tar.gif'),
(52, 'archive__zip', 'Fichiers compress�s ZIP','0','','','','','', '.zip', 'file', 'zip.gif'),
(53, 'archive__gzip', 'Fichiers archive GNU zip','0','','','','','', '.gzip', 'file', 'gzip.gif'),
(54, 'archive__gz', 'Fichiers archive GNU zip','0','','','','','', '.gz', 'file', 'gz.gif'),
(55, 'archive__Z', 'Fichiers archive unix','0','','','','','', '.Z', 'file', 'Z.gif'),
(56, 'audio__basic', 'Fichiers audio basiques','0','','','','','', '.snd', 'file', 'snd.gif'),
(57, 'audio__x-aiff', 'Fichiers audio AIFF','0','','','','','', '.aif', 'file', 'aif.gif'),
(58, 'audio__x-wav', 'Fichiers audio Wave','0','','','','','', '.wav', 'file', 'wav.gif'),
(59, 'image__gif', 'Images gif','0','','','','','', '.gif', 'file', 'gif.gif'),
(60, 'image__jpeg', 'Images Jpeg','0','','','','','', '.jpg', 'file', 'jpg.gif'),
(61, 'image__tiff', 'Images Tiff','0','','','','','', '.tif', 'file', 'tif.gif'),
(62, 'image__png', 'Images png','0','','','','','', '.png', 'file', 'png.gif'),
(63, 'nofile', 'document without file','0','','','','','', '', 'file', '_default.gif'),
(64, 'cao__cadds_zipadrawc4', 'adraw cadds4x','0','','','','','', '_pd', 'adrawc4', 'cadds.gif'),
(65, 'cao__cadds_zipadrawc5', 'adraw cadds5','0','','','','','', '_fd', 'adrawc5', 'cadds.gif');


CREATE TABLE IF NOT EXISTS `doctypes_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT	INTO `doctypes_seq` (`id`) VALUES (100);

-- ------------------------------------------------------ 
-- Donn�es de la table `document_indice`
-- 

INSERT INTO `document_indice` (`document_indice_id`, `indice_value`) VALUES 
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'P'),
(16, 'Q'),
(17, 'R'),
(18, 'S'),
(19, 'T'),
(20, 'U');

-- ------------------------------------------------------ 
-- Données de la table `document_indice`
-- 

INSERT INTO `container_indice` (`indice_id`, `indice_value`) VALUES 
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'P'),
(16, 'Q'),
(17, 'R'),
(18, 'S'),
(19, 'T'),
(20, 'U');

-- Insert null values in activity role. If not, cant list the activity created until
-- role/activity link creation.

INSERT INTO `galaxia_activity_roles` (`activityId`, `roleId`) VALUES 
(0, 0);
