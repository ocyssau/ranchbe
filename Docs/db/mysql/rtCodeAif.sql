-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Mer 02 Mars 2011 � 18:06
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de donn�es: `ranchbe`a
--

-- --------------------------------------------------------

--
-- Structure de la table `rt_aif_code`
--

CREATE TABLE IF NOT EXISTS `rt_aif_code` (
  `status` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `code` char(8) COLLATE latin1_general_ci NOT NULL,
  `title` text COLLATE latin1_general_ci NOT NULL,
  `itp` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `stec` tinyint(4) NOT NULL,
  `stg` tinyint(4) NOT NULL,
  KEY `rt_aif_code_index01` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `rt_aif_code`
--

INSERT INTO `rt_aif_code` (`status`, `code`, `title`, `itp`, `stec`, `stg`) VALUES
('-', '0000-Y01', 'Dossier de D�finition OK', 'General', 1, 1),
('INFO', '0100-YA1', 'Juste pour information - erreurs d�tect�es hors p�rim�tre CTD', 'General', 1, 1),
('INFO', '0100-YA3', 'Juste pour information - erreur d�tect�e durant la phase de transition', 'General', 1, 1),
('INFO', '0100-YA4', '', 'General', 1, 1),
('BLOCKING', '1020-B04', 'Percage non cr�� avec l''outil Hole & Fastener (Sauf fabrication, locating, passage,...)', 'General', 1, 0),
('BLOCKING', '1040-C02', 'Absence de vues ou sections d�clar�es dans "NOTA" ou sur plan', 'General', 1, 1),
('BLOCKING', '1040-C05', 'Lettre interdite (I/O) / chiffre pour le rep�rage des vues, des principes de montage et des sections', 'General', 1, 0),
('BLOCKING', '1040-C06', 'Incoh�rence entre la section et son plan de coupe', 'General', 1, 1),
('BLOCKING', '1040-C07', 'D�saccord entre le nom des vues et leur rep�rage', 'General', 1, 1),
('BLOCKING', '1040-C08', 'Absence de projection 3D ou planche vide non justifi�e', 'General', 1, 1),
('BLOCKING', '1050-D01', 'Incoh�rence entre le calcul du r�treint et la hauteur du bord tomb�', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D02', 'Incoh�rence entre le sens du rabattement indiqu� et le modele', 'Sheet Metal', 1, 1),
('BLOCKING', '1050-D04', 'Incoh�rence de position des trous entre model et d�velopp�', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D06', 'Absence repr�sentation de la section rabattue (angle dif de 90�)', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D07', 'Trait d''�pure EXTERIEUR absent ou mal positionn�', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D10', 'Incoh�rence entre le d�velopp� et le mod�le (contours)', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D11', 'Absence / Incoh�rence de la FONT approximatif sur le d�velopp�  ', 'Sheet Metal', 1, 0),
('BLOCKING', '1050-D12', 'D�velopp� Non conforme (contr�le rayons mini de d�tourage des pi�ces )', 'Sheet Metal', 1, 0),
('BLOCKING', '1060-E01', 'Non respect des layers', 'General', 1, 0),
('BLOCKING', '2000-F02', 'Code de protection absent ou incoh�rent', 'General', 1, 1),
('BLOCKING', '2000-F03', 'Incoh�rence entre Code mati�re en nomenclature et Epaisseur modele', 'General', 1, 1),
('BLOCKING', '2000-F04', 'Diff�rence entre la modification d�crite dans l''ECN et la modification r�alis�e ', 'General', 1, 1),
('BLOCKING', '2000-F05', 'Incoh�rence entre mod�le et repr�sentation des vues sur la planche', 'General', 1, 1),
('BLOCKING', '2000-F06', 'Absence du cercle de d�capage', 'General', 1, 1),
('BLOCKING', '2000-F07', 'Absence de rep�re de r�f�rence pi�ce pour le tableau de d�finition g�om�trique', 'General', 1, 1),
('BLOCKING', '2000-F08', 'Absence entit�s draw', 'General', 1, 1),
('BLOCKING', '2000-F14', 'Incoh�rence entre le tableau de soudure et le type (classe) de soudure', 'General', 1, 1),
('BLOCKING', '2000-F15', 'Probl�me de sauvegarde (fichier corrompu, obsol�te, taille�)', 'General', 1, 1),
('BLOCKING', '2000-F20', 'Nomenclature "vide" non justifi�e', 'General', 1, 1),
('BLOCKING', '2000-F21', 'Inscriptions sp�cifiques ne devant pas se trouver sur le plan (ex: F00G-12345-200, IPDAxxx) ', 'General', 1, 1),
('BLOCKING', '2000-F22', 'description de l''ECN non explicite (modification non v�rifiable)', 'General', 1, 1),
('BLOCKING', '2000-F24', 'Le logo ou le nom de la soci�t� S/T ne doit pas appara�tre sur le plan', 'General', 1, 1),
('BLOCKING', '2000-F25', 'Incoh�rence entre repr�sentation 2D, 3D (�l�ments standards) et d�signation en nomenclature', 'General', 1, 1),
('BLOCKING', '2000-F32', 'Interdiction d''indiquer sur la planche la classe ou la mati�re', 'General', 1, 1),
('BLOCKING', '2000-F34', 'Incoh�rence entre le montage rep�r� X et le principe de montage X ', 'Assembly drawing', 1, 1),
('BLOCKING', '2000-F35', 'Pi�ce sym�trique non conforme', 'General', 1, 1),
('BLOCKING', '2000-F38', 'Nom du "Body" incorrect (ex : Unfolded�)', 'General', 1, 0),
('BLOCKING', '2000-F39', 'Le "ConditionOfSupply" (sur�paisseur d''usinage,�) doit �tre pr�sent (hachur�) sur le CATDrawing et en "NoShow" dans la CATPart', 'General', 1, 0),
('BLOCKING', '2000-F40', 'Entit�s de per�age ne doivent pas �tre en NO SHOW dans le mod�le', 'General', 1, 1),
('BLOCKING', '2000-F41', 'Angle de cintrage de tuyauterie doit �tre inf�rieur � 175� et sup�rieur � 5� ', 'Piping', 1, 1),
('BLOCKING', '2000-F43', 'Incoh�rence entre la r�f�rence mati�re en nomenclature et les propri�t�s dans le fichier CAO', 'General', 1, 1),
('BLOCKING', '2000-F44', 'Incoh�rence entre NOTA et planche pour r�serves de peinture sur tuyauteries (Piping).', 'Piping', 1, 1),
('BLOCKING', '2000-F48', 'Reference non officielle mentionn�e sur plan ou nomenclature', 'General', 1, 1),
('BLOCKING', '2000-F50', 'R�f�rences incoh�rentes entre les diff�rentes vues/planches', 'General', 1, 1),
('BLOCKING', '2000-F53', 'Code mati�re absent', 'General', 1, 1),
('BLOCKING', '2000-F54', 'Absence de la masse', '', 1, 1),
('BLOCKING', '2000-F55', 'Interdiction d''indiquer sur la planche la protection', 'General', 1, 1),
('BLOCKING', '2000-F59', 'Num�rotation de la part incorrecte', 'General', 1, 1),
('BLOCKING', '2000-F60', 'Erreur concernant l''interchangeabilit�', 'Manufacturing  ', 1, 1),
('BLOCKING', '2010-G02', 'Diff�rence entre code G�o. BOM/plan ', 'General', 1, 1),
('BLOCKING', '2030-H03', 'Inscriptions manuscrites interdites (sauf signature)', 'General', 1, 1),
('BLOCKING', '2050-I01', 'Absence du visa soudure, normalisation, qualit� ou visas partenaires', 'General', 1, 1),
('BLOCKING', '2050-I03', 'Quantit� de m�tallisations inf�rieures au nombre indiqu� dans tableau de m�tallisation', 'General', 1, 1),
('BLOCKING', '2050-I04', 'Absence du tableau pression d''�preuve sur les plans de tuyauteries', 'Piping', 1, 1),
('BLOCKING', '2050-I05', 'Tampon de marquage absent ou incoh�rent entre la planche et la nomenclature', 'General', 1, 1),
('BLOCKING', '2050-I06', 'Absence du tableau m�tallisation ', 'General', 1, 1),
('BLOCKING', '2060-J01', 'Diff�rence entre rep�res nomenclature et rep�res planche', 'General', 1, 1),
('BLOCKING', '2060-J02', 'Rep�re vide ou non rattach� sur planche', 'General', 1, 1),
('BLOCKING', '2060-J03', 'Nota incorrect ou manquant', 'General', 1, 1),
('BLOCKING', '2070-K01', 'Le cartouche est incoh�rent, incomplet ou contient une information incorrecte', 'General', 1, 1),
('BLOCKING', '2070-K03', 'cartouche incorrect pour le programme', 'General', 1, 1),
('BLOCKING', '3000-M01', 'Incoh�rence entre l�indice de la planche et l�indice d�clar� dans nomenclature.', 'General', 1, 1),
('BLOCKING', '3000-M03', 'Incoh�rence entre l''indice de la pi�ce modifi�e et celui du dossier de d�finition', 'General', 1, 1),
('BLOCKING', '3010-N01', 'Incoh�rence entre les planches re�ues et les planches d�clar�es dans la nomenclature', 'General', 1, 1),
('BLOCKING', '4000-P01', 'Incoh�rence entre le diam�tre de per�age et le texte associ�', 'General', 1, 1),
('BLOCKING', '4000-P02', 'Absence ou erreur sur les valeurs des coordonn�es des points dans le tableau de d�finition g�om�trique', 'General', 1, 1),
('BLOCKING', '4000-P04', 'Cotation fonctionnelle non conforme � l''ISO1101 (localisation ou r�f�rence(s) erron�es ou absentes)', 'General', 1, 1),
('BLOCKING', '4000-P05', 'Incoh�rence ou absence de repr�sentation du soyage ASN453.01 par rapport � la d�signation', 'Sheet Metal', 1, 1),
('BLOCKING', '4000-P06', 'Incoh�rence entre la d�signation du rivet en nomenclature/cotation', 'General', 1, 1),
('BLOCKING', '4000-P07', 'Incoh�rence entre cotation fond de maille et �paisseur de pi�ce', 'Manufacturing  ', 1, 1),
('BLOCKING', '4000-P10', 'Incoh�rence ou absence entre la cotation du bord tomb� et la hauteur mesur�e (mod�le)', 'Sheet Metal', 1, 1),
('BLOCKING', '4000-P11', 'Absence ou incoh�rence de cotation du rayon ou de l''angle de pliage', 'Sheet Metal', 1, 1),
('BLOCKING', '4000-P12', 'Absence de cotation pour tuyauterie �quip�e (cotation ruban, �tiquette, embouts)', 'Piping', 1, 1),
('BLOCKING', '4000-P13', 'Incoh�rence entre le mod�le et la cotation', 'General', 1, 1),
('BLOCKING', '4000-P14', 'Absence de la cotation des per�ages (avants trous et NSA2010)', 'General', 1, 1),
('BLOCKING', '4000-P16', 'Cotation non conforme (cotes entre parenth�ses �)', 'General', 1, 1),
('BLOCKING', '4010-Q01', 'Absence du tableau de drapage (pr�sence de rosette)', 'Composites', 1, 1),
('BLOCKING', '4010-Q02', 'Rosette manquante par rapport � la pr�sence du tableau de Drapage', 'Composites', 1, 1),
('BLOCKING', '4010-Q03', 'Rosette doit �tre identifi�e par au moins deux directions de mani�re � identifier angle 0� et sens de rotation', 'Composites', 1, 1),
('BLOCKING', '4010-Q04', 'Incoh�rence entre la d�finition des plis et le tableau de drapage', 'Composites', 1, 1),
('INFO', '5401-R01', 'Nota "La s�quence de drapage d''une zone est d�finie�" manquant par rapport au tableau de Drapage', 'Composites', 1, 0),
('INFO', '5401-R02', 'Loi d''�paisseur manquante par rapport au tableau de Drapage', 'Composites', 1, 0),
('INFO', '5401-R03', 'Rosette repr�sent�e non normalis�e ', 'Composites', 1, 1),
('INFO', '5401-R04', 'Ajout d''angle interdit sur la rosette ', 'Composites', 1, 1),
('INFO', '5401-R05', 'Absence de r�f�rentiel d''orientation Rosette ', 'Composites', 1, 0),
('INFO', '5401-R06', 'FTC incomplete  (toute case coch�e doit �tre compl�t�e)', 'Composites', 1, 1),
('INFO', '5401-R07', 'Case Contr�le inspection coch�e - Document IQDA-06.04 manquant', 'Composites', 1, 1),
('INFO', '5401-R08', 'Case Contr�le d''aspect - ABD0076 non cit�e', 'Composites', 1, 1),
('INFO', '5401-R09', 'Case "Recherche du module de compression" coch�e - AITMI.0008 manquant', 'Composites', 1, 1),
('INFO', '5401-R10', 'Case "Traction sur �prouvette" coch�e - AITMI.0007 manquant', 'Composites', 1, 1),
('BLOCKING', '4020-S01', 'Couleur non autoris�e (? de rouge, bleu ou jaune) suivant AM2255 module 0', 'Assembly drawing', 1, 1),
('BLOCKING', '4020-S04', 'Principe de montage non d�fini ou absent', 'Assembly drawing', 1, 1),
('BLOCKING', '4020-S05', 'Cheminement Harnais ou nom incorrect', 'Assembly drawing', 1, 1),
('BLOCKING', '4020-S06', 'Environnement structure manquant dans la vue, section ou detail.', 'Assembly drawing', 1, 1),
('INFO', '5010-R01', '', 'General', 1, 1),
('INFO', '5010-R02', '', 'General', 1, 1),
('INFO', '5010-R03', '', 'General', 1, 1),
('INFO', '5020-A01', 'Erreur non d�tect�e dans le contr�le pr�c�dent.A corriger au prochain lancement', 'General', 1, 1),
('INFO', '5020-A02', 'Erreur � corriger au prochain lancement', 'General', 1, 1),
('BLOCKING', '6000-A01', 'Anomalie non corrig�e malgr� 3 notifications informatives : Refus bloquant', 'General', 1, 1);


-- --------------------------------------------------------

--
-- Structure de la vue `rt_aif_code_view1`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rt_aif_code_view1` AS select concat(`rt_aif_code`.`code`,'  ',`rt_aif_code`.`title`) AS `code_title`,`rt_aif_code`.`code` AS `code`,`rt_aif_code`.`title` AS `title` from `rt_aif_code`;

--
-- VIEW  `rt_aif_code_view1`
-- Donn�es: aucune
--



