-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- G�n�r� le : Jeu 31 Juillet 2014 � 19:10
-- Version du serveur: 5.5.29
-- Version de PHP: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Structure de la table `adodb_logsql`
--

CREATE TABLE IF NOT EXISTS `adodb_logsql` (
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sql0` varchar(250) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `sql1` text COLLATE latin1_general_ci NOT NULL,
  `params` text COLLATE latin1_general_ci NOT NULL,
  `tracer` text COLLATE latin1_general_ci NOT NULL,
  `timer` decimal(16,6) NOT NULL DEFAULT '0.000000'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


-- --------------------------------------------------------

--
-- Structure de la table `bookshops`
--

CREATE TABLE IF NOT EXISTS `bookshops` (
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `bookshop_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `bookshop_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `bookshop_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `proprietaire` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `project_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bookshop_id`),
  UNIQUE KEY `UC_bookshop_number` (`bookshop_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshops_seq`
--

CREATE TABLE IF NOT EXISTS `bookshops_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_alias`
--

CREATE TABLE IF NOT EXISTS `bookshop_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `bookshop_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshopAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_bookshop_alias` (`alias_id`,`bookshop_id`),
  KEY `K_bookshop_alias_1` (`bookshop_id`),
  KEY `K_bookshop_alias_2` (`bookshop_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_alias_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_categories`
--

CREATE TABLE IF NOT EXISTS `bookshop_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_categories_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_category_rel`
--

CREATE TABLE IF NOT EXISTS `bookshop_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_bookshop_categories_rel_1` (`category_id`,`bookshop_id`),
  KEY `K_bookshop_categories_rel_1` (`category_id`),
  KEY `K_bookshop_categories_rel_2` (`bookshop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_category_rel_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_cont_metadata`
--

CREATE TABLE IF NOT EXISTS `bookshop_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_cont_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doccomments`
--

CREATE TABLE IF NOT EXISTS `bookshop_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doccomments_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doctype_process`
--

CREATE TABLE IF NOT EXISTS `bookshop_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `bookshop_doctype_process_uniq1` (`bookshop_id`,`doctype_id`),
  KEY `FK_bookshop_doctype_process_1` (`category_id`),
  KEY `FK_bookshop_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doctype_process_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=119 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents`
--

CREATE TABLE IF NOT EXISTS `bookshop_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `bookshop_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'bookshop',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `doc_source` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `commentaires` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `applicabilite` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `maj_int_ind` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `min_int_ind` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `mot_cles` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fournisseur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_fournisseur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_famille` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `docsier_date` int(11) DEFAULT NULL,
  `docsier_keyword` text COLLATE latin1_general_ci,
  `docsier_format` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `sub_ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `a_type` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `orig_filename` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_bookshop_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_bookshop_documents_1` (`cad_model_supplier_id`),
  KEY `FK_bookshop_documents_2` (`supplier_id`),
  KEY `FK_bookshop_documents_3` (`bookshop_id`),
  KEY `FK_bookshop_documents_4` (`category_id`),
  KEY `FK_bookshop_documents_5` (`doctype_id`),
  KEY `FK_bookshop_documents_6` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents_history`
--

CREATE TABLE IF NOT EXISTS `bookshop_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `bookshop_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents_history_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=13905 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_documents_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11692 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_files`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_bookshop_doc_files_1` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_files_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7776 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_files_versions`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`),
  KEY `IK_bookshop_doc_files_version_1` (`father_id`),
  KEY `IK_bookshop_doc_files_version_2` (`file_name`),
  KEY `IK_bookshop_doc_files_version_3` (`file_path`),
  KEY `IK_bookshop_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_files_versions_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=555 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_rel`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_bookshop_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_doc_rel_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_familles`
--

CREATE TABLE IF NOT EXISTS `bookshop_familles` (
  `id` varchar(12) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `famille_name` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_files`
--

CREATE TABLE IF NOT EXISTS `bookshop_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_bookshop_files_1` (`bookshop_id`),
  KEY `FK_bookshop_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_history`
--

CREATE TABLE IF NOT EXISTS `bookshop_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `bookshop_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_history_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_import_history`
--

CREATE TABLE IF NOT EXISTS `bookshop_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_key_word`
--

CREATE TABLE IF NOT EXISTS `bookshop_key_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_word` varchar(64) COLLATE latin1_german1_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `key_word` (`key_word`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci AUTO_INCREMENT=1516 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_metadata`
--

CREATE TABLE IF NOT EXISTS `bookshop_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_metadata_rel`
--

CREATE TABLE IF NOT EXISTS `bookshop_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `bookshop_id` (`bookshop_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_metadata_rel_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Structure de la table `bookshop_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `bookshop_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlibs`
--

CREATE TABLE IF NOT EXISTS `cadlibs` (
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `cadlib_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `cadlib_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `cadlib_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `project_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cadlib_id`),
  UNIQUE KEY `UC_cadlib_number` (`cadlib_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlibs_seq`
--

CREATE TABLE IF NOT EXISTS `cadlibs_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_alias`
--

CREATE TABLE IF NOT EXISTS `cadlib_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `cadlib_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlibAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_cadlib_alias` (`alias_id`,`cadlib_id`),
  KEY `K_cadlib_alias_1` (`cadlib_id`),
  KEY `K_cadlib_alias_2` (`cadlib_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_alias_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_categories`
--

CREATE TABLE IF NOT EXISTS `cadlib_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_categories_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_category_rel`
--

CREATE TABLE IF NOT EXISTS `cadlib_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_cadlib_categories_rel_1` (`category_id`,`cadlib_id`),
  KEY `K_cadlib_categories_rel_1` (`category_id`),
  KEY `K_cadlib_categories_rel_2` (`cadlib_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_category_rel_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_cont_metadata`
--

CREATE TABLE IF NOT EXISTS `cadlib_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_cont_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doccomments`
--

CREATE TABLE IF NOT EXISTS `cadlib_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doccomments_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doctype_process`
--

CREATE TABLE IF NOT EXISTS `cadlib_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `cadlib_doctype_process_uniq1` (`cadlib_id`,`doctype_id`),
  KEY `FK_cadlib_doctype_process_1` (`category_id`),
  KEY `FK_cadlib_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doctype_process_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents`
--

CREATE TABLE IF NOT EXISTS `cadlib_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `cadlib_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'cadlib',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `fabricant_a` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fabricant_b` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `fabricant_c` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_a` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_b` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ref_c` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `chapitre` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subchapitre` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_cadlib_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_cadlib_documents_1` (`cad_model_supplier_id`),
  KEY `FK_cadlib_documents_2` (`supplier_id`),
  KEY `FK_cadlib_documents_3` (`cadlib_id`),
  KEY `FK_cadlib_documents_4` (`category_id`),
  KEY `FK_cadlib_documents_5` (`doctype_id`),
  KEY `FK_cadlib_documents_6` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents_history`
--

CREATE TABLE IF NOT EXISTS `cadlib_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `cadlib_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents_history_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=99 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_documents_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_files`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_cadlib_doc_files_1` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_files_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_files_versions`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_cadlib_doc_files_version_1` (`father_id`),
  KEY `IK_cadlib_doc_files_version_2` (`file_name`),
  KEY `IK_cadlib_doc_files_version_3` (`file_path`(128)),
  KEY `IK_cadlib_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_files_versions_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_rel`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_cadlib_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_doc_rel_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_files`
--

CREATE TABLE IF NOT EXISTS `cadlib_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_cadlib_files_1` (`cadlib_id`),
  KEY `FK_cadlib_files_2` (`import_order`),
  KEY `index_file_name` (`file_name`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_history`
--

CREATE TABLE IF NOT EXISTS `cadlib_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `cadlib_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_history_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_import_history`
--

CREATE TABLE IF NOT EXISTS `cadlib_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_metadata`
--

CREATE TABLE IF NOT EXISTS `cadlib_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_metadata_rel`
--

CREATE TABLE IF NOT EXISTS `cadlib_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `cadlib_metadata_rel_uniq` (`cadlib_id`,`field_name`),
  KEY `cadlib_id` (`cadlib_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_metadata_rel_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `cadlib_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `cadlib_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `checkout_index`
--

CREATE TABLE IF NOT EXISTS `checkout_index` (
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `container_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `container_id` int(11) NOT NULL DEFAULT '0',
  `container_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `check_out_by` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `container_favorite`
--

CREATE TABLE IF NOT EXISTS `container_favorite` (
  `user_id` int(11) NOT NULL,
  `container_id` int(11) NOT NULL,
  `container_number` varchar(512) COLLATE latin1_general_ci NOT NULL,
  `space_id` int(11) NOT NULL,
  `space_name` varchar(128) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`user_id`,`container_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `container_indice`
--

CREATE TABLE IF NOT EXISTS `container_indice` (
  `indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`indice_id`),
  UNIQUE KEY `UC_indice_id` (`indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `docbasket`
--

CREATE TABLE IF NOT EXISTS `docbasket` (
  `user_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL,
  `space_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`doc_id`,`space_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `doctypes`
--

CREATE TABLE IF NOT EXISTS `doctypes` (
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `doctype_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `doctype_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
  `file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `script_post_store` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_pre_store` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_post_update` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `script_pre_update` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `recognition_regexp` text COLLATE latin1_general_ci,
  `visu_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`doctype_id`),
  UNIQUE KEY `UC_doctype_number` (`doctype_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `doctypes_seq`
--

CREATE TABLE IF NOT EXISTS `doctypes_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `document_indice`
--

CREATE TABLE IF NOT EXISTS `document_indice` (
  `document_indice_id` int(11) NOT NULL DEFAULT '0',
  `indice_value` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`document_indice_id`),
  UNIQUE KEY `UC_indice_value` (`indice_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activities`
--

CREATE TABLE IF NOT EXISTS `galaxia_activities` (
  `activityId` int(14) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `pId` int(14) NOT NULL DEFAULT '0',
  `type` enum('start','end','split','switch','join','activity','standalone') COLLATE latin1_general_ci DEFAULT NULL,
  `isAutoRouted` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `flowNum` int(10) DEFAULT NULL,
  `isInteractive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `lastModif` int(14) DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `expirationTime` int(6) unsigned NOT NULL DEFAULT '0',
  `isAutomatic` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isComment` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`activityId`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_activity_roles`
--

CREATE TABLE IF NOT EXISTS `galaxia_activity_roles` (
  `activityId` int(14) NOT NULL DEFAULT '0',
  `roleId` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activityId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instances`
--

CREATE TABLE IF NOT EXISTS `galaxia_instances` (
  `instanceId` int(14) NOT NULL AUTO_INCREMENT,
  `pId` int(14) NOT NULL DEFAULT '0',
  `started` int(14) DEFAULT NULL,
  `name` varchar(200) COLLATE latin1_general_ci DEFAULT 'No Name',
  `owner` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `nextActivity` int(14) DEFAULT NULL,
  `nextUser` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `ended` int(14) DEFAULT NULL,
  `status` enum('active','exception','aborted','completed') COLLATE latin1_general_ci DEFAULT NULL,
  `properties` longblob,
  PRIMARY KEY (`instanceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=103585 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_activities`
--

CREATE TABLE IF NOT EXISTS `galaxia_instance_activities` (
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `activityId` int(14) NOT NULL DEFAULT '0',
  `started` int(14) NOT NULL DEFAULT '0',
  `ended` int(14) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  `status` enum('running','completed') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`instanceId`,`activityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_comments`
--

CREATE TABLE IF NOT EXISTS `galaxia_instance_comments` (
  `cId` int(14) NOT NULL AUTO_INCREMENT,
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  `activityId` int(14) DEFAULT NULL,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `title` varchar(250) COLLATE latin1_general_ci DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  `activity` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `timestamp` int(14) DEFAULT NULL,
  PRIMARY KEY (`cId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=581 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_instance_properties`
--

CREATE TABLE IF NOT EXISTS `galaxia_instance_properties` (
  `iid` int(11) NOT NULL,
  `a_verifier_user` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `previousActivity` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `previousUser` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `checkresult` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  `ecodes` varchar(256) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `iid` (`iid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_processes`
--

CREATE TABLE IF NOT EXISTS `galaxia_processes` (
  `pId` int(14) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `isValid` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isActive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `version` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `lastModif` int(14) DEFAULT NULL,
  `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_roles`
--

CREATE TABLE IF NOT EXISTS `galaxia_roles` (
  `roleId` int(14) NOT NULL AUTO_INCREMENT,
  `pId` int(14) NOT NULL DEFAULT '0',
  `lastModif` int(14) DEFAULT NULL,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_transitions`
--

CREATE TABLE IF NOT EXISTS `galaxia_transitions` (
  `pId` int(14) NOT NULL DEFAULT '0',
  `actFromId` int(14) NOT NULL DEFAULT '0',
  `actToId` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`actFromId`,`actToId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_user_roles`
--

CREATE TABLE IF NOT EXISTS `galaxia_user_roles` (
  `pId` int(14) NOT NULL DEFAULT '0',
  `roleId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`roleId`,`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Structure de la table `galaxia_workitems`
--

CREATE TABLE IF NOT EXISTS `galaxia_workitems` (
  `itemId` int(14) NOT NULL AUTO_INCREMENT,
  `instanceId` int(14) NOT NULL DEFAULT '0',
  `orderId` int(14) NOT NULL DEFAULT '0',
  `activityId` int(14) NOT NULL DEFAULT '0',
  `properties` longblob,
  `started` int(14) DEFAULT NULL,
  `ended` int(14) DEFAULT NULL,
  `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=446807 ;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_applications`
--

CREATE TABLE IF NOT EXISTS `liveuser_applications` (
  `application_id` int(11) DEFAULT '0',
  `application_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `application_id_idx` (`application_id`),
  UNIQUE KEY `define_name_i_idx` (`application_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_areas`
--

CREATE TABLE IF NOT EXISTS `liveuser_areas` (
  `area_id` int(11) DEFAULT '0',
  `application_id` int(11) DEFAULT '0',
  `area_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `area_id_idx` (`area_id`),
  UNIQUE KEY `define_name_i_idx` (`application_id`,`area_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_areas_seq`
--

CREATE TABLE IF NOT EXISTS `liveuser_areas_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_area_admin_areas`
--

CREATE TABLE IF NOT EXISTS `liveuser_area_admin_areas` (
  `area_id` int(11) DEFAULT '0',
  `perm_user_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`area_id`,`perm_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_grouprights`
--

CREATE TABLE IF NOT EXISTS `liveuser_grouprights` (
  `group_id` int(11) DEFAULT '0',
  `right_id` int(11) DEFAULT '0',
  `right_level` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_groups`
--

CREATE TABLE IF NOT EXISTS `liveuser_groups` (
  `group_id` int(11) DEFAULT '0',
  `group_type` int(11) DEFAULT '0',
  `group_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `group_description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `owner_user_id` int(11) DEFAULT '0',
  `owner_group_id` int(11) DEFAULT '0',
  UNIQUE KEY `group_id_idx` (`group_id`),
  UNIQUE KEY `define_name_i_idx` (`group_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_groups_seq`
--

CREATE TABLE IF NOT EXISTS `liveuser_groups_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_groupusers`
--

CREATE TABLE IF NOT EXISTS `liveuser_groupusers` (
  `perm_user_id` int(11) DEFAULT '0',
  `group_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_group_subgroups`
--

CREATE TABLE IF NOT EXISTS `liveuser_group_subgroups` (
  `group_id` int(11) DEFAULT '0',
  `subgroup_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_perm_users`
--

CREATE TABLE IF NOT EXISTS `liveuser_perm_users` (
  `perm_user_id` int(11) DEFAULT '0',
  `auth_user_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `auth_container_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `perm_type` int(11) DEFAULT '0',
  UNIQUE KEY `perm_user_id_idx` (`perm_user_id`),
  UNIQUE KEY `auth_id_i_idx` (`auth_user_id`,`auth_container_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_perm_users_seq`
--

CREATE TABLE IF NOT EXISTS `liveuser_perm_users_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123 ;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_rights`
--

CREATE TABLE IF NOT EXISTS `liveuser_rights` (
  `right_id` int(11) DEFAULT '0',
  `area_id` int(11) DEFAULT '0',
  `right_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `right_description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `has_implied` tinyint(1) DEFAULT '1',
  UNIQUE KEY `right_id_idx` (`right_id`),
  UNIQUE KEY `define_name_i_idx` (`area_id`,`right_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_rights_seq`
--

CREATE TABLE IF NOT EXISTS `liveuser_rights_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=806 ;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_right_implied`
--

CREATE TABLE IF NOT EXISTS `liveuser_right_implied` (
  `right_id` int(11) DEFAULT '0',
  `implied_right_id` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`right_id`,`implied_right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_translations`
--

CREATE TABLE IF NOT EXISTS `liveuser_translations` (
  `translation_id` int(11) DEFAULT '0',
  `section_id` int(11) DEFAULT '0',
  `section_type` int(11) DEFAULT '0',
  `language_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `translation_id_idx` (`translation_id`),
  UNIQUE KEY `translation_i_idx` (`section_id`,`section_type`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_userrights`
--

CREATE TABLE IF NOT EXISTS `liveuser_userrights` (
  `perm_user_id` int(11) DEFAULT '0',
  `right_id` int(11) DEFAULT '0',
  `right_level` int(11) DEFAULT '0',
  UNIQUE KEY `id_i_idx` (`perm_user_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_users`
--

CREATE TABLE IF NOT EXISTS `liveuser_users` (
  `auth_user_id` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `handle` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `passwd` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT '0',
  `owner_group_id` int(11) DEFAULT '0',
  `lastlogin` datetime DEFAULT '1970-01-01 00:00:00',
  `is_active` tinyint(1) DEFAULT '1',
  UNIQUE KEY `auth_user_id_idx` (`auth_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liveuser_users_seq`
--

CREATE TABLE IF NOT EXISTS `liveuser_users_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_archive`
--

CREATE TABLE IF NOT EXISTS `messu_archive` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=117783 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_messages`
--

CREATE TABLE IF NOT EXISTS `messu_messages` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=149617 ;

-- --------------------------------------------------------

--
-- Structure de la table `messu_sent`
--

CREATE TABLE IF NOT EXISTS `messu_sent` (
  `msgId` int(14) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_from` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_to` text COLLATE latin1_general_ci,
  `user_cc` text COLLATE latin1_general_ci,
  `user_bcc` text COLLATE latin1_general_ci,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `body` text COLLATE latin1_general_ci,
  `hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `replyto_hash` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date` int(14) DEFAULT NULL,
  `isRead` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isReplied` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isFlagged` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  PRIMARY KEY (`msgId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockups`
--

CREATE TABLE IF NOT EXISTS `mockups` (
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mockup_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `mockup_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `mockup_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `package_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`mockup_id`),
  UNIQUE KEY `UC_mockup_number` (`mockup_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockups_seq`
--

CREATE TABLE IF NOT EXISTS `mockups_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_alias`
--

CREATE TABLE IF NOT EXISTS `mockup_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mockup_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockupAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_mockup_alias` (`alias_id`,`mockup_id`),
  KEY `K_mockup_alias_1` (`mockup_id`),
  KEY `K_mockup_alias_2` (`mockup_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_alias_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_categories`
--

CREATE TABLE IF NOT EXISTS `mockup_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_categories_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_category_rel`
--

CREATE TABLE IF NOT EXISTS `mockup_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_mockup_categories_rel_1` (`category_id`,`mockup_id`),
  KEY `K_mockup_categories_rel_1` (`category_id`),
  KEY `K_mockup_categories_rel_2` (`mockup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_category_rel_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_cont_metadata`
--

CREATE TABLE IF NOT EXISTS `mockup_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_cont_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doccomments`
--

CREATE TABLE IF NOT EXISTS `mockup_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doccomments_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doctype_process`
--

CREATE TABLE IF NOT EXISTS `mockup_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_doctype_process_uniq1` (`mockup_id`,`doctype_id`),
  KEY `FK_mockup_doctype_process_1` (`category_id`),
  KEY `FK_mockup_doctype_process_3` (`doctype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doctype_process_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents`
--

CREATE TABLE IF NOT EXISTS `mockup_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `mockup_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(6) COLLATE latin1_general_ci NOT NULL DEFAULT 'mockup',
  `doctype_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_mockup_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
  KEY `FK_mockup_documents_2` (`supplier_id`),
  KEY `FK_mockup_documents_3` (`mockup_id`),
  KEY `FK_mockup_documents_4` (`category_id`),
  KEY `FK_mockup_documents_5` (`doctype_id`),
  KEY `FK_mockup_documents_6` (`document_indice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents_history`
--

CREATE TABLE IF NOT EXISTS `mockup_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `mockup_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `cad_model_supplier_id` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents_history_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_documents_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_files`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(512)),
  KEY `FK_mockup_doc_files_1` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_files_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_files_versions`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_mockup_doc_files_version_1` (`father_id`),
  KEY `IK_mockup_doc_files_version_2` (`file_name`),
  KEY `IK_mockup_doc_files_version_3` (`file_path`(128)),
  KEY `IK_mockup_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_files_versions_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_rel`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  KEY `FK_mockup_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_doc_rel_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_files`
--

CREATE TABLE IF NOT EXISTS `mockup_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `import_order` int(11) DEFAULT NULL,
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) DEFAULT NULL,
  `file_open_by` int(11) DEFAULT NULL,
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `FK_mockup_files_1` (`mockup_id`),
  KEY `FK_mockup_files_2` (`import_order`),
  KEY `index_file_code_name` (`file_access_code`,`file_name`),
  KEY `index_file_name_version` (`file_name`,`file_version`),
  KEY `index_file_name` (`file_name`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_files_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_files_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_history`
--

CREATE TABLE IF NOT EXISTS `mockup_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `mockup_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `mockup_indice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_history_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_import_history`
--

CREATE TABLE IF NOT EXISTS `mockup_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` text COLLATE latin1_general_ci,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_import_history_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_import_history_seq` (
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_metadata`
--

CREATE TABLE IF NOT EXISTS `mockup_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_metadata_rel`
--

CREATE TABLE IF NOT EXISTS `mockup_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `mockup_metadata_rel_uniq` (`mockup_id`,`field_name`),
  KEY `mockup_id` (`mockup_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_metadata_rel_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `mockup_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `mockup_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `partner_id` int(11) NOT NULL DEFAULT '0',
  `partner_number` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `partner_type` enum('customer','supplier','staff') COLLATE latin1_general_ci DEFAULT NULL,
  `first_name` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `adress` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `city` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `cell_phone` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `mail` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `web_site` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `activity` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `company` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `partners_seq`
--

CREATE TABLE IF NOT EXISTS `partners_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=937 ;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `product_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `product_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `product_indice` varchar(8) COLLATE latin1_general_ci DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `UC_product_number` (`product_number`),
  KEY `FK_products_1` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `products_workitem_documents`
--

CREATE TABLE IF NOT EXISTS `products_workitem_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`document_id`,`product_id`),
  KEY `FK_products_workitem_documents_1` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product_history`
--

CREATE TABLE IF NOT EXISTS `product_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_date` int(11) DEFAULT NULL,
  `state_before` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `state_after` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `comments` text COLLATE latin1_general_ci,
  `indice_before` varchar(8) COLLATE latin1_general_ci DEFAULT NULL,
  `indice_after` decimal(10,0) DEFAULT NULL,
  `action_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_indice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `project_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `default_process_id` int(11) DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `UC_project_number` (`project_number`),
  KEY `FK_projects_1` (`project_indice_id`),
  KEY `FK_projects_2` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `projects_seq`
--

CREATE TABLE IF NOT EXISTS `projects_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Structure de la table `project_bookshop_rel`
--

CREATE TABLE IF NOT EXISTS `project_bookshop_rel` (
  `project_id` int(11) DEFAULT NULL,
  `bookshop_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_bookshop_rel` (`project_id`,`bookshop_id`),
  KEY `FK_project_bookshop_rel_1` (`bookshop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_cadlib_rel`
--

CREATE TABLE IF NOT EXISTS `project_cadlib_rel` (
  `project_id` int(11) DEFAULT NULL,
  `cadlib_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_cadlib_rel` (`project_id`,`cadlib_id`),
  KEY `FK_project_cadlib_rel_1` (`cadlib_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_container_rel`
--

CREATE TABLE IF NOT EXISTS `project_container_rel` (
  `project_id` int(11) DEFAULT NULL,
  `container_id` int(11) DEFAULT NULL,
  `container_type` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  UNIQUE KEY `IDX_project_container_rel_1` (`project_id`,`container_id`,`container_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_doctype_process`
--

CREATE TABLE IF NOT EXISTS `project_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `process_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `Uniq_doctype_project` (`doctype_id`,`project_id`),
  KEY `FK_project_doctype_process_3` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_doctype_process_seq`
--

CREATE TABLE IF NOT EXISTS `project_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=141 ;

-- --------------------------------------------------------

--
-- Structure de la table `project_history`
--

CREATE TABLE IF NOT EXISTS `project_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_history_seq`
--

CREATE TABLE IF NOT EXISTS `project_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Structure de la table `project_mockup_rel`
--

CREATE TABLE IF NOT EXISTS `project_mockup_rel` (
  `project_id` int(11) DEFAULT NULL,
  `mockup_id` int(11) DEFAULT NULL,
  UNIQUE KEY `UC_project_mockup_rel` (`project_id`,`mockup_id`),
  KEY `FK_project_mockup_rel_1` (`mockup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_partner_rel`
--

CREATE TABLE IF NOT EXISTS `project_partner_rel` (
  `project_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `role` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `IDX_project_partner_rel_1` (`partner_id`,`project_id`),
  KEY `FK_project_partner_rel_2` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rt_aif_code_a350`
--

CREATE TABLE IF NOT EXISTS `rt_aif_code_a350` (
  `code` char(15) COLLATE latin1_general_ci NOT NULL,
  `itp_cluster` char(4) COLLATE latin1_general_ci DEFAULT NULL,
  `check` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `mistake` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `type` enum('B','I','OS') COLLATE latin1_general_ci NOT NULL,
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Codes erreurs pour le programme A350';

-- --------------------------------------------------------

--
-- Structure de la table `rt_aif_code_a350_seq`
--

CREATE TABLE IF NOT EXISTS `rt_aif_code_a350_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `rt_aif_code_a380`
--

CREATE TABLE IF NOT EXISTS `rt_aif_code_a380` (
  `status` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `code` char(8) COLLATE latin1_general_ci NOT NULL,
  `title` text COLLATE latin1_general_ci NOT NULL,
  `itp` varchar(24) COLLATE latin1_general_ci NOT NULL,
  `stec` tinyint(4) NOT NULL,
  `stg` tinyint(4) NOT NULL,
  KEY `rt_aif_code_index01` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `rt_aif_code_view1`
--
CREATE TABLE IF NOT EXISTS `rt_aif_code_view1` (
`code_title` varchar(272)
,`code` char(15)
,`title` varchar(255)
);
-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks`
--

CREATE TABLE IF NOT EXISTS `tiki_user_tasks` (
  `taskId` int(14) NOT NULL AUTO_INCREMENT,
  `last_version` int(4) NOT NULL DEFAULT '0',
  `user` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `creator` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `public_for_group` int(11) DEFAULT NULL,
  `rights_by_creator` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `created` int(14) NOT NULL DEFAULT '0',
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  `completed` int(14) DEFAULT NULL,
  `percentage` int(4) DEFAULT NULL,
  PRIMARY KEY (`taskId`),
  UNIQUE KEY `creator` (`creator`,`created`),
  UNIQUE KEY `creator_2` (`creator`,`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `tiki_user_tasks_history`
--

CREATE TABLE IF NOT EXISTS `tiki_user_tasks_history` (
  `belongs_to` int(14) NOT NULL DEFAULT '0',
  `task_version` int(4) NOT NULL DEFAULT '0',
  `title` varchar(250) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` text COLLATE latin1_general_ci,
  `start` int(14) DEFAULT NULL,
  `end` int(14) DEFAULT NULL,
  `lasteditor` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `lastchanges` int(14) NOT NULL DEFAULT '0',
  `priority` int(2) NOT NULL DEFAULT '3',
  `completed` int(14) DEFAULT NULL,
  `deleted` int(14) DEFAULT NULL,
  `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `percentage` int(4) DEFAULT NULL,
  `accepted_creator` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `accepted_user` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`belongs_to`,`task_version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_prefs`
--

CREATE TABLE IF NOT EXISTS `user_prefs` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `css_sheet` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `lang` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `long_date_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `short_date_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `hour_format` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `time_zone` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `wildspace_path` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  `max_record` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_sessions`
--

CREATE TABLE IF NOT EXISTS `user_sessions` (
  `user_id` int(11) NOT NULL,
  `datas` longtext COLLATE latin1_general_ci,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_containerfavorite_users`
--
CREATE TABLE IF NOT EXISTS `view_containerfavorite_users` (
`user_id` int(11)
,`container_id` int(11)
,`container_number` varchar(512)
,`space_id` int(11)
,`space_name` varchar(128)
,`username` varchar(32)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_countapprove_byuser`
--
CREATE TABLE IF NOT EXISTS `view_countapprove_byuser` (
`count` bigint(21)
,`owner` varchar(200)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_countreject_byuser`
--
CREATE TABLE IF NOT EXISTS `view_countreject_byuser` (
`count` bigint(21)
,`owner` varchar(200)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_docfile`
--
CREATE TABLE IF NOT EXISTS `view_docfile` (
`document_id` int(11)
,`document_number` varchar(128)
,`document_name` varchar(255)
,`document_state` varchar(32)
,`document_access_code` int(11)
,`document_version` int(11)
,`workitem_id` int(11)
,`document_indice_id` int(11)
,`doc_space` varchar(8)
,`instance_id` int(11)
,`doctype_id` int(11)
,`default_process_id` int(11)
,`category_id` int(11)
,`check_out_by` int(11)
,`check_out_date` int(11)
,`designation` varchar(128)
,`issued_from_document` int(11)
,`update_date` int(11)
,`update_by` decimal(10,0)
,`open_date` int(11)
,`open_by` int(11)
,`ci` varchar(255)
,`type_gc` varchar(255)
,`work_unit` varchar(255)
,`father_ds` varchar(255)
,`need_calcul` varchar(255)
,`ata` varchar(255)
,`receptionneur` varchar(32)
,`indice_client` varchar(255)
,`recept_date` int(11)
,`observation` varchar(255)
,`date_de_remise` int(11)
,`sub_ata` varchar(255)
,`work_package` varchar(255)
,`subject` varchar(255)
,`weight` double
,`cost` double
,`fab_process` varchar(255)
,`material` varchar(255)
,`materiau_mak` varchar(255)
,`masse_mak` double
,`rt_name` varchar(255)
,`rt_type` varchar(255)
,`rt_reason` varchar(255)
,`rt_emitted` int(11)
,`rt_apply_to` int(11)
,`modification` varchar(255)
,`file_version` int(11)
,`file_name` varchar(128)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_document_hierarchy`
--
CREATE TABLE IF NOT EXISTS `view_document_hierarchy` (
`id` int(11)
,`parent` int(11)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_filedoc`
--
CREATE TABLE IF NOT EXISTS `view_filedoc` (
`file_id` int(11)
,`document_id` int(11)
,`file_name` varchar(128)
,`file_used_name` varchar(128)
,`file_path` text
,`file_version` int(11)
,`file_access_code` int(11)
,`file_root_name` varchar(128)
,`file_extension` varchar(16)
,`file_state` varchar(16)
,`file_type` varchar(128)
,`file_size` int(11)
,`file_mtime` int(11)
,`file_md5` varchar(128)
,`file_checkout_by` int(11)
,`file_checkout_date` int(11)
,`file_open_date` int(11)
,`file_open_by` int(11)
,`file_update_date` int(11)
,`file_update_by` int(11)
,`document_version` int(11)
,`document_indice_id` int(11)
,`document_number` varchar(128)
,`designation` varchar(128)
,`document_state` varchar(32)
,`document_access_code` int(11)
,`workitem_id` int(11)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_filedoc_rel`
--
CREATE TABLE IF NOT EXISTS `view_filedoc_rel` (
`docId` int(11)
,`filename` varchar(128)
,`iteration` int(11)
,`indice` int(11)
,`designation` varchar(128)
,`state` varchar(32)
,`accesscode` int(11)
,`workitemId` int(11)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_instances_activities`
--
CREATE TABLE IF NOT EXISTS `view_instances_activities` (
`inst_name` varchar(200)
,`inst_id` int(14)
,`inst_start` int(14)
,`inst_end` int(14)
,`act_user` varchar(40)
,`act_name` varchar(80)
,`document_id` int(11)
,`container_id` int(11)
,`user_id` varchar(32)
,`user_email` varchar(128)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_rejected_doc`
--
CREATE TABLE IF NOT EXISTS `view_rejected_doc` (
`docid` int(11)
,`document_number` varchar(128)
,`state` varchar(32)
,`action_by` varchar(64)
,`action_name` varchar(32)
,`container_id` int(11)
,`container_number` varchar(16)
,`started` datetime
,`owner` varchar(200)
,`status` enum('active','exception','aborted','completed')
,`properties` blob
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_rejected_docc`
--
CREATE TABLE IF NOT EXISTS `view_rejected_docc` (
`instanceId` int(14)
,`docid` int(11)
,`document_number` varchar(128)
,`state` varchar(32)
,`action_by` varchar(64)
,`action_name` varchar(32)
,`doctype` varchar(64)
,`container_id` int(11)
,`container_number` varchar(16)
,`started` datetime
,`ended` datetime
,`owner` varchar(200)
,`status` enum('active','exception','aborted','completed')
,`previousActivity` varchar(256)
,`previousUser` varchar(256)
,`checkresult` varchar(256)
,`ecodes` varchar(256)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_rt`
--
CREATE TABLE IF NOT EXISTS `view_rt` (
`rt_id` int(11)
,`rt_number` varchar(128)
,`rt_status` varchar(32)
,`rt_access_code` int(11)
,`rt_open_date` int(11)
,`rt_reason` varchar(255)
,`rt_type` varchar(255)
,`applyto_id` int(11)
,`applyto_number` varchar(128)
,`applyto_indice` int(11)
,`applyto_version` int(11)
,`applyto_status` varchar(32)
,`from_number` varchar(128)
,`from_version` int(11)
,`container_id` int(11)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_rt_b`
--
CREATE TABLE IF NOT EXISTS `view_rt_b` (
`rt_id` int(11)
,`rt_number` varchar(128)
,`rt_status` varchar(32)
,`rt_access_code` int(11)
,`rt_open_date` int(11)
,`rt_reason` varchar(255)
,`rt_type` varchar(255)
,`applyto_id` int(11)
,`applyto_number` varchar(128)
,`applyto_indice` int(11)
,`applyto_version` int(11)
,`applyto_status` varchar(32)
,`from_number` varchar(128)
,`from_version` int(11)
,`container_id` int(11)
);
-- --------------------------------------------------------

--
-- Structure de la table `workitems`
--

CREATE TABLE IF NOT EXISTS `workitems` (
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `workitem_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `workitem_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_only` tinyint(1) NOT NULL DEFAULT '0',
  `access_code` int(11) DEFAULT NULL,
  `workitem_indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci NOT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `container_type` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `affaires_liees` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `interloc_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `interloc_sier` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `numero_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `object_class` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `chef_group` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`workitem_id`),
  UNIQUE KEY `UC_workitem_number` (`workitem_number`),
  KEY `FK_workitems_1` (`workitem_indice_id`),
  KEY `FK_workitems_2` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitems_seq`
--

CREATE TABLE IF NOT EXISTS `workitems_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=199 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_alias`
--

CREATE TABLE IF NOT EXISTS `workitem_alias` (
  `alias_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `workitem_description` text COLLATE latin1_general_ci,
  `object_class` varchar(13) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitemAlias',
  PRIMARY KEY (`alias_id`),
  UNIQUE KEY `UC_workitem_alias` (`alias_id`,`workitem_id`),
  KEY `K_workitem_alias_1` (`workitem_id`),
  KEY `K_workitem_alias_2` (`workitem_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_alias_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_alias_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_categories`
--

CREATE TABLE IF NOT EXISTS `workitem_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `UC_category_number` (`category_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_categories_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_category_rel`
--

CREATE TABLE IF NOT EXISTS `workitem_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_workitem_categories_rel_1` (`category_id`,`workitem_id`),
  KEY `K_workitem_categories_rel_1` (`category_id`),
  KEY `K_workitem_categories_rel_2` (`workitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_category_rel_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=119 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_cont_metadata`
--

CREATE TABLE IF NOT EXISTS `workitem_cont_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `display_both` tinyint(1) NOT NULL DEFAULT '0',
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_cont_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_cont_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doccomments`
--

CREATE TABLE IF NOT EXISTS `workitem_doccomments` (
  `comment_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE latin1_general_ci NOT NULL,
  `open_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doccomments_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_doccomments_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2225 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doctype_process`
--

CREATE TABLE IF NOT EXISTS `workitem_doctype_process` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `doctype_id` int(11) DEFAULT NULL,
  `process_id` int(11) DEFAULT NULL,
  `workitem_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `FK_workitem_doctype_process_1` (`doctype_id`),
  KEY `FK_workitem_doctype_process_3` (`category_id`),
  KEY `FK_workitem_doctype_process_4` (`workitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doctype_process_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_doctype_process_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=167 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents`
--

CREATE TABLE IF NOT EXISTS `workitem_documents` (
  `document_id` int(11) NOT NULL DEFAULT '0',
  `document_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `document_name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `document_access_code` int(11) NOT NULL DEFAULT '0',
  `document_version` int(11) NOT NULL DEFAULT '1',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `document_indice_id` int(11) DEFAULT NULL,
  `doc_space` varchar(8) COLLATE latin1_general_ci NOT NULL DEFAULT 'workitem',
  `instance_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) NOT NULL DEFAULT '0',
  `default_process_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` decimal(10,0) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `ci` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `type_gc` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `work_unit` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `father_ds` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `need_calcul` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `receptionneur` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `indice_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `recept_date` int(11) DEFAULT NULL,
  `observation` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `date_de_remise` int(11) DEFAULT NULL,
  `sub_ata` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `work_package` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `fab_process` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `material` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `materiau_mak` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `masse_mak` double DEFAULT NULL,
  `rt_name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_type` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_reason` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `rt_emitted` int(11) DEFAULT NULL,
  `rt_apply_to` int(11) DEFAULT NULL,
  `modification` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `IDX_workitem_documents_1` (`document_number`,`document_indice_id`),
  KEY `FK_workitem_documents_2` (`doctype_id`),
  KEY `FK_workitem_documents_3` (`document_indice_id`),
  KEY `FK_workitem_documents_4` (`workitem_id`),
  KEY `FK_workitem_documents_5` (`category_id`),
  KEY `document_name` (`document_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents_history`
--

CREATE TABLE IF NOT EXISTS `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(64) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) DEFAULT NULL,
  `workitem_id` int(11) DEFAULT NULL,
  `document_number` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `document_state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `document_access_code` int(11) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `check_out_by` int(11) DEFAULT NULL,
  `check_out_date` int(11) DEFAULT NULL,
  `issued_from_document` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `doctype_id` int(11) DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  PRIMARY KEY (`histo_order`),
  KEY `workitem_documents_history_i001` (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents_history_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_documents_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=933818 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_documents_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_documents_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=116473 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_files`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_files` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_used_name` varchar(128) COLLATE latin1_general_ci NOT NULL COMMENT 'nom d usage',
  `file_path` text COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_access_code` int(11) NOT NULL DEFAULT '0',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_checkout_by` int(11) DEFAULT NULL,
  `file_checkout_date` int(11) DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_file_name` (`file_name`,`file_path`(512)),
  KEY `FK_workitem_doc_files_1` (`document_id`),
  KEY `file_md5` (`file_md5`),
  KEY `file_name` (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_files_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=206893 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_files_versions`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_files_versions` (
  `file_id` int(11) NOT NULL DEFAULT '0',
  `father_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_path` tinytext COLLATE latin1_general_ci NOT NULL,
  `file_version` int(11) NOT NULL DEFAULT '1',
  `file_root_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_extension` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `file_state` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
  `file_type` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `file_mtime` int(11) DEFAULT NULL,
  `file_md5` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `file_open_date` int(11) NOT NULL DEFAULT '0',
  `file_open_by` int(11) NOT NULL DEFAULT '0',
  `file_update_date` int(11) DEFAULT NULL,
  `file_update_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_workitem_doc_files_version_1` (`father_id`),
  KEY `IK_workitem_doc_files_version_2` (`file_name`),
  KEY `IK_workitem_doc_files_version_3` (`file_path`(128)),
  KEY `IK_workitem_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_files_versions_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_files_versions_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=212791 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_rel`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_rel` (
  `dr_link_id` int(11) NOT NULL DEFAULT '0',
  `dr_document_id` int(11) DEFAULT NULL,
  `dr_l_document_id` int(11) DEFAULT NULL,
  `dr_access_code` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`dr_link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_doc_rel_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_doc_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14533 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_history`
--

CREATE TABLE IF NOT EXISTS `workitem_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_by` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `action_date` int(11) DEFAULT NULL,
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `workitem_indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_file_path` text COLLATE latin1_general_ci,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_history_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_history_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=391 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_import_history`
--

CREATE TABLE IF NOT EXISTS `workitem_import_history` (
  `import_order` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `package_file_name` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `description` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `state` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` int(11) DEFAULT NULL,
  `import_by` int(11) DEFAULT NULL,
  `import_errors_report` text COLLATE latin1_general_ci,
  `import_logfiles_file` text COLLATE latin1_general_ci,
  `package_file_path` text COLLATE latin1_general_ci,
  `package_file_extension` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `package_file_mtime` int(11) DEFAULT NULL,
  `package_file_size` int(11) DEFAULT NULL,
  `package_file_md5` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`import_order`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_metadata`
--

CREATE TABLE IF NOT EXISTS `workitem_metadata` (
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_metadata_rel`
--

CREATE TABLE IF NOT EXISTS `workitem_metadata_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `workitem_metadata_rel_uniq` (`workitem_id`,`field_name`),
  KEY `workitem_id` (`workitem_id`),
  KEY `field_name` (`field_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_metadata_rel_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_metadata_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=98 ;

-- --------------------------------------------------------

--
-- Structure de la table `workitem_metadata_seq`
--

CREATE TABLE IF NOT EXISTS `workitem_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Structure de la vue `rt_aif_code_view1`
--
DROP TABLE IF EXISTS `rt_aif_code_view1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rt_aif_code_view1` AS select concat(`rt_aif_code_a350`.`code`,'  ',`rt_aif_code_a350`.`description`) AS `code_title`,`rt_aif_code_a350`.`code` AS `code`,`rt_aif_code_a350`.`description` AS `title` from `rt_aif_code_a350`;

-- --------------------------------------------------------

--
-- Structure de la vue `view_containerfavorite_users`
--
DROP TABLE IF EXISTS `view_containerfavorite_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_containerfavorite_users` AS select `cf`.`user_id` AS `user_id`,`cf`.`container_id` AS `container_id`,`cf`.`container_number` AS `container_number`,`cf`.`space_id` AS `space_id`,`cf`.`space_name` AS `space_name`,`lu`.`handle` AS `username` from (`container_favorite` `cf` join `liveuser_users` `lu` on((`cf`.`user_id` = `lu`.`auth_user_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_countapprove_byuser`
--
DROP TABLE IF EXISTS `view_countapprove_byuser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_countapprove_byuser` AS select count(`view_rejected_docc`.`docid`) AS `count`,`view_rejected_docc`.`owner` AS `owner` from `view_rejected_docc` where (`view_rejected_docc`.`checkresult` = 'approve') group by `view_rejected_docc`.`owner`;

-- --------------------------------------------------------

--
-- Structure de la vue `view_countreject_byuser`
--
DROP TABLE IF EXISTS `view_countreject_byuser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_countreject_byuser` AS select count(`view_rejected_docc`.`docid`) AS `count`,`view_rejected_docc`.`owner` AS `owner` from `view_rejected_docc` where (`view_rejected_docc`.`checkresult` like 'rejete%') group by `view_rejected_docc`.`owner`;

-- --------------------------------------------------------

--
-- Structure de la vue `view_docfile`
--
DROP TABLE IF EXISTS `view_docfile`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_docfile` AS select `doc`.`document_id` AS `document_id`,`doc`.`document_number` AS `document_number`,`doc`.`document_name` AS `document_name`,`doc`.`document_state` AS `document_state`,`doc`.`document_access_code` AS `document_access_code`,`doc`.`document_version` AS `document_version`,`doc`.`workitem_id` AS `workitem_id`,`doc`.`document_indice_id` AS `document_indice_id`,`doc`.`doc_space` AS `doc_space`,`doc`.`instance_id` AS `instance_id`,`doc`.`doctype_id` AS `doctype_id`,`doc`.`default_process_id` AS `default_process_id`,`doc`.`category_id` AS `category_id`,`doc`.`check_out_by` AS `check_out_by`,`doc`.`check_out_date` AS `check_out_date`,`doc`.`designation` AS `designation`,`doc`.`issued_from_document` AS `issued_from_document`,`doc`.`update_date` AS `update_date`,`doc`.`update_by` AS `update_by`,`doc`.`open_date` AS `open_date`,`doc`.`open_by` AS `open_by`,`doc`.`ci` AS `ci`,`doc`.`type_gc` AS `type_gc`,`doc`.`work_unit` AS `work_unit`,`doc`.`father_ds` AS `father_ds`,`doc`.`need_calcul` AS `need_calcul`,`doc`.`ata` AS `ata`,`doc`.`receptionneur` AS `receptionneur`,`doc`.`indice_client` AS `indice_client`,`doc`.`recept_date` AS `recept_date`,`doc`.`observation` AS `observation`,`doc`.`date_de_remise` AS `date_de_remise`,`doc`.`sub_ata` AS `sub_ata`,`doc`.`work_package` AS `work_package`,`doc`.`subject` AS `subject`,`doc`.`weight` AS `weight`,`doc`.`cost` AS `cost`,`doc`.`fab_process` AS `fab_process`,`doc`.`material` AS `material`,`doc`.`materiau_mak` AS `materiau_mak`,`doc`.`masse_mak` AS `masse_mak`,`doc`.`rt_name` AS `rt_name`,`doc`.`rt_type` AS `rt_type`,`doc`.`rt_reason` AS `rt_reason`,`doc`.`rt_emitted` AS `rt_emitted`,`doc`.`rt_apply_to` AS `rt_apply_to`,`doc`.`modification` AS `modification`,`df`.`file_version` AS `file_version`,`df`.`file_name` AS `file_name` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_document_hierarchy`
--
DROP TABLE IF EXISTS `view_document_hierarchy`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_document_hierarchy` AS select `docrel`.`dr_l_document_id` AS `id`,`docrel`.`dr_document_id` AS `parent` from `workitem_doc_rel` `docrel`;

-- --------------------------------------------------------

--
-- Structure de la vue `view_filedoc`
--
DROP TABLE IF EXISTS `view_filedoc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_filedoc` AS select `df`.`file_id` AS `file_id`,`df`.`document_id` AS `document_id`,`df`.`file_name` AS `file_name`,`df`.`file_used_name` AS `file_used_name`,`df`.`file_path` AS `file_path`,`df`.`file_version` AS `file_version`,`df`.`file_access_code` AS `file_access_code`,`df`.`file_root_name` AS `file_root_name`,`df`.`file_extension` AS `file_extension`,`df`.`file_state` AS `file_state`,`df`.`file_type` AS `file_type`,`df`.`file_size` AS `file_size`,`df`.`file_mtime` AS `file_mtime`,`df`.`file_md5` AS `file_md5`,`df`.`file_checkout_by` AS `file_checkout_by`,`df`.`file_checkout_date` AS `file_checkout_date`,`df`.`file_open_date` AS `file_open_date`,`df`.`file_open_by` AS `file_open_by`,`df`.`file_update_date` AS `file_update_date`,`df`.`file_update_by` AS `file_update_by`,`doc`.`document_version` AS `document_version`,`doc`.`document_indice_id` AS `document_indice_id`,`doc`.`document_number` AS `document_number`,`doc`.`designation` AS `designation`,`doc`.`document_state` AS `document_state`,`doc`.`document_access_code` AS `document_access_code`,`doc`.`workitem_id` AS `workitem_id` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_filedoc_rel`
--
DROP TABLE IF EXISTS `view_filedoc_rel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_filedoc_rel` AS select `doc`.`document_id` AS `docId`,`df`.`file_name` AS `filename`,`df`.`file_version` AS `iteration`,`doc`.`document_indice_id` AS `indice`,`doc`.`designation` AS `designation`,`doc`.`document_state` AS `state`,`doc`.`document_access_code` AS `accesscode`,`doc`.`workitem_id` AS `workitemId` from (`workitem_doc_files` `df` join `workitem_documents` `doc` on((`df`.`document_id` = `doc`.`document_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_instances_activities`
--
DROP TABLE IF EXISTS `view_instances_activities`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_instances_activities` AS select `inst`.`name` AS `inst_name`,`inst`.`instanceId` AS `inst_id`,`inst`.`started` AS `inst_start`,`inst`.`ended` AS `inst_end`,`inst_act`.`user` AS `act_user`,`act`.`name` AS `act_name`,`histo`.`document_id` AS `document_id`,`histo`.`workitem_id` AS `container_id`,`users`.`auth_user_id` AS `user_id`,`users`.`email` AS `user_email` from ((((`galaxia_instances` `inst` join `galaxia_instance_activities` `inst_act` on((`inst`.`instanceId` = `inst_act`.`instanceId`))) join `galaxia_activities` `act` on((`act`.`activityId` = `inst_act`.`activityId`))) join `workitem_documents_history` `histo` on((`histo`.`instance_id` = `inst`.`instanceId`))) join `liveuser_users` `users` on((`users`.`handle` = `inst_act`.`user`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_rejected_doc`
--
DROP TABLE IF EXISTS `view_rejected_doc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rejected_doc` AS select `histo`.`document_id` AS `docid`,`histo`.`document_number` AS `document_number`,`histo`.`document_state` AS `state`,`histo`.`action_by` AS `action_by`,`histo`.`action_name` AS `action_name`,`container`.`workitem_id` AS `container_id`,`container`.`workitem_number` AS `container_number`,from_unixtime(`instance`.`started`) AS `started`,`instance`.`owner` AS `owner`,`instance`.`status` AS `status`,substr(`instance`.`properties`,1,1000) AS `properties` from ((`workitem_documents_history` `histo` join `galaxia_instances` `instance` on((`histo`.`instance_id` = `instance`.`instanceId`))) join `workitems` `container` on((`histo`.`workitem_id` = `container`.`workitem_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_rejected_docc`
--
DROP TABLE IF EXISTS `view_rejected_docc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_rejected_docc` AS select `instance`.`instanceId` AS `instanceId`,`histo`.`document_id` AS `docid`,`histo`.`document_number` AS `document_number`,`histo`.`document_state` AS `state`,`histo`.`action_by` AS `action_by`,`histo`.`action_name` AS `action_name`,`dt`.`doctype_number` AS `doctype`,`container`.`workitem_id` AS `container_id`,`container`.`workitem_number` AS `container_number`,from_unixtime(`instance`.`started`) AS `started`,from_unixtime(`instance`.`ended`) AS `ended`,`instance`.`owner` AS `owner`,`instance`.`status` AS `status`,`ip`.`previousActivity` AS `previousActivity`,`ip`.`previousUser` AS `previousUser`,`ip`.`checkresult` AS `checkresult`,`ip`.`ecodes` AS `ecodes` from ((((`workitem_documents_history` `histo` join `galaxia_instances` `instance` on((`histo`.`instance_id` = `instance`.`instanceId`))) join `workitems` `container` on((`histo`.`workitem_id` = `container`.`workitem_id`))) join `galaxia_instance_properties` `ip` on((`instance`.`instanceId` = `ip`.`iid`))) join `doctypes` `dt` on((`histo`.`doctype_id` = `dt`.`doctype_id`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_rt`
--
DROP TABLE IF EXISTS `view_rt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rt` AS select `rt`.`document_id` AS `rt_id`,`rt`.`document_number` AS `rt_number`,`rt`.`document_state` AS `rt_status`,`rt`.`document_access_code` AS `rt_access_code`,`rt`.`open_date` AS `rt_open_date`,`rt`.`rt_reason` AS `rt_reason`,`rt`.`rt_type` AS `rt_type`,`applydocs`.`document_id` AS `applyto_id`,`applydocs`.`document_number` AS `applyto_number`,`applydocs`.`document_indice_id` AS `applyto_indice`,`applydocs`.`document_version` AS `applyto_version`,`applydocs`.`document_state` AS `applyto_status`,`linked_docs`.`document_number` AS `from_number`,`linked_docs`.`document_indice_id` AS `from_version`,`linked_docs`.`workitem_id` AS `container_id` from ((`workitem_documents` `rt` left join `workitem_documents` `linked_docs` on((`linked_docs`.`document_id` = `rt`.`rt_apply_to`))) join `workitem_documents` `applydocs` on((`linked_docs`.`document_number` = `applydocs`.`document_number`)));

-- --------------------------------------------------------

--
-- Structure de la vue `view_rt_b`
--
DROP TABLE IF EXISTS `view_rt_b`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ranchbe`@`localhost` SQL SECURITY DEFINER VIEW `view_rt_b` AS select `rt`.`document_id` AS `rt_id`,`rt`.`document_number` AS `rt_number`,`rt`.`document_state` AS `rt_status`,`rt`.`document_access_code` AS `rt_access_code`,`rt`.`open_date` AS `rt_open_date`,`rt`.`rt_reason` AS `rt_reason`,`rt`.`rt_type` AS `rt_type`,`linked_docs`.`document_id` AS `applyto_id`,`linked_docs`.`document_number` AS `applyto_number`,`linked_docs`.`document_indice_id` AS `applyto_indice`,`linked_docs`.`document_version` AS `applyto_version`,`linked_docs`.`document_state` AS `applyto_status`,`linked_docs`.`document_number` AS `from_number`,`linked_docs`.`document_indice_id` AS `from_version`,`linked_docs`.`workitem_id` AS `container_id` from (`workitem_documents` `rt` left join `workitem_documents` `linked_docs` on((`linked_docs`.`document_id` = `rt`.`rt_apply_to`)));

--
-- Contraintes pour les tables export�es
--

--
-- Contraintes pour la table `bookshop_category_rel`
--
ALTER TABLE `bookshop_category_rel`
  ADD CONSTRAINT `bookshop_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bookshop_category_rel_ibfk_2` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `bookshop_doccomments`
--
ALTER TABLE `bookshop_doccomments`
  ADD CONSTRAINT `bookshop_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `bookshop_doctype_process`
--
ALTER TABLE `bookshop_doctype_process`
  ADD CONSTRAINT `FK_bookshop_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`),
  ADD CONSTRAINT `FK_bookshop_doctype_process_2` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  ADD CONSTRAINT `FK_bookshop_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`);

--
-- Contraintes pour la table `bookshop_documents`
--
ALTER TABLE `bookshop_documents`
  ADD CONSTRAINT `FK_bookshop_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_bookshop_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_bookshop_documents_3` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  ADD CONSTRAINT `FK_bookshop_documents_4` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`),
  ADD CONSTRAINT `FK_bookshop_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_bookshop_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`);

--
-- Contraintes pour la table `bookshop_doc_files`
--
ALTER TABLE `bookshop_doc_files`
  ADD CONSTRAINT `FK_bookshop_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `bookshop_documents` (`document_id`);

--
-- Contraintes pour la table `bookshop_doc_rel`
--
ALTER TABLE `bookshop_doc_rel`
  ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `bookshop_files`
--
ALTER TABLE `bookshop_files`
  ADD CONSTRAINT `FK_bookshop_files_1` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  ADD CONSTRAINT `FK_bookshop_files_2` FOREIGN KEY (`import_order`) REFERENCES `bookshop_import_history` (`import_order`);

--
-- Contraintes pour la table `cadlib_category_rel`
--
ALTER TABLE `cadlib_category_rel`
  ADD CONSTRAINT `cadlib_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cadlib_category_rel_ibfk_2` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cadlib_doccomments`
--
ALTER TABLE `cadlib_doccomments`
  ADD CONSTRAINT `cadlib_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cadlib_doctype_process`
--
ALTER TABLE `cadlib_doctype_process`
  ADD CONSTRAINT `FK_cadlib_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`),
  ADD CONSTRAINT `FK_cadlib_doctype_process_2` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  ADD CONSTRAINT `FK_cadlib_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`);

--
-- Contraintes pour la table `cadlib_documents`
--
ALTER TABLE `cadlib_documents`
  ADD CONSTRAINT `FK_cadlib_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_cadlib_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_cadlib_documents_3` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  ADD CONSTRAINT `FK_cadlib_documents_4` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`),
  ADD CONSTRAINT `FK_cadlib_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_cadlib_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`);

--
-- Contraintes pour la table `cadlib_doc_files`
--
ALTER TABLE `cadlib_doc_files`
  ADD CONSTRAINT `FK_cadlib_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `cadlib_documents` (`document_id`);

--
-- Contraintes pour la table `cadlib_doc_rel`
--
ALTER TABLE `cadlib_doc_rel`
  ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `cadlib_files`
--
ALTER TABLE `cadlib_files`
  ADD CONSTRAINT `FK_cadlib_files_1` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  ADD CONSTRAINT `FK_cadlib_files_2` FOREIGN KEY (`import_order`) REFERENCES `cadlib_import_history` (`import_order`);

--
-- Contraintes pour la table `mockup_category_rel`
--
ALTER TABLE `mockup_category_rel`
  ADD CONSTRAINT `mockup_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mockup_category_rel_ibfk_2` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `mockup_doccomments`
--
ALTER TABLE `mockup_doccomments`
  ADD CONSTRAINT `mockup_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `mockup_doctype_process`
--
ALTER TABLE `mockup_doctype_process`
  ADD CONSTRAINT `FK_mockup_doctype_process_1` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`),
  ADD CONSTRAINT `FK_mockup_doctype_process_2` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  ADD CONSTRAINT `FK_mockup_doctype_process_3` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`);

--
-- Contraintes pour la table `mockup_documents`
--
ALTER TABLE `mockup_documents`
  ADD CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_mockup_documents_3` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  ADD CONSTRAINT `FK_mockup_documents_4` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`),
  ADD CONSTRAINT `FK_mockup_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_mockup_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`);

--
-- Contraintes pour la table `mockup_doc_files`
--
ALTER TABLE `mockup_doc_files`
  ADD CONSTRAINT `FK_mockup_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `mockup_documents` (`document_id`);

--
-- Contraintes pour la table `mockup_doc_rel`
--
ALTER TABLE `mockup_doc_rel`
  ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `mockup_files`
--
ALTER TABLE `mockup_files`
  ADD CONSTRAINT `FK_mockup_files_2` FOREIGN KEY (`import_order`) REFERENCES `mockup_import_history` (`import_order`);

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_products_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `products_workitem_documents`
--
ALTER TABLE `products_workitem_documents`
  ADD CONSTRAINT `FK_products_workitem_documents_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `FK_products_workitem_documents_2` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`);

--
-- Contraintes pour la table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_projects_1` FOREIGN KEY (`project_indice_id`) REFERENCES `container_indice` (`indice_id`),
  ADD CONSTRAINT `FK_projects_2` FOREIGN KEY (`link_id`) REFERENCES `project_doctype_process` (`link_id`);

--
-- Contraintes pour la table `project_bookshop_rel`
--
ALTER TABLE `project_bookshop_rel`
  ADD CONSTRAINT `FK_project_bookshop_rel_1` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`),
  ADD CONSTRAINT `FK_project_bookshop_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `project_cadlib_rel`
--
ALTER TABLE `project_cadlib_rel`
  ADD CONSTRAINT `FK_project_cadlib_rel_1` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`),
  ADD CONSTRAINT `FK_project_cadlib_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `project_doctype_process`
--
ALTER TABLE `project_doctype_process`
  ADD CONSTRAINT `FK_project_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_project_doctype_process_3` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `project_mockup_rel`
--
ALTER TABLE `project_mockup_rel`
  ADD CONSTRAINT `FK_project_mockup_rel_1` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
  ADD CONSTRAINT `FK_project_mockup_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `project_partner_rel`
--
ALTER TABLE `project_partner_rel`
  ADD CONSTRAINT `FK_project_partner_rel_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`partner_id`),
  ADD CONSTRAINT `FK_project_partner_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `user_prefs`
--
ALTER TABLE `user_prefs`
  ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) REFERENCES `liveuser_perm_users` (`perm_user_id`);

--
-- Contraintes pour la table `workitems`
--
ALTER TABLE `workitems`
  ADD CONSTRAINT `FK_workitems_1` FOREIGN KEY (`workitem_indice_id`) REFERENCES `container_indice` (`indice_id`),
  ADD CONSTRAINT `FK_workitems_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Contraintes pour la table `workitem_category_rel`
--
ALTER TABLE `workitem_category_rel`
  ADD CONSTRAINT `workitem_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `workitem_category_rel_ibfk_2` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `workitem_doccomments`
--
ALTER TABLE `workitem_doccomments`
  ADD CONSTRAINT `workitem_doccomments_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `workitem_doctype_process`
--
ALTER TABLE `workitem_doctype_process`
  ADD CONSTRAINT `FK_workitem_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_workitem_doctype_process_3` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`),
  ADD CONSTRAINT `FK_workitem_doctype_process_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`);

--
-- Contraintes pour la table `workitem_documents`
--
ALTER TABLE `workitem_documents`
  ADD CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
  ADD CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`),
  ADD CONSTRAINT `FK_workitem_documents_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`),
  ADD CONSTRAINT `FK_workitem_documents_5` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`);

--
-- Contraintes pour la table `workitem_doc_files`
--
ALTER TABLE `workitem_doc_files`
  ADD CONSTRAINT `FK_workitem_doc_files_1` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`);

--
-- Contraintes pour la table `workitem_doc_rel`
--
ALTER TABLE `workitem_doc_rel`
  ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;
