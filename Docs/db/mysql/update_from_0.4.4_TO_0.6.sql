
ALTER TABLE `bookshop_doc_files` CHANGE `file_id` `file_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `document_id` `document_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `file_type` `file_type` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_doc_files` CHANGE `file_open_date` `file_open_date` int(11) NOT NULL;
ALTER TABLE `bookshop_doc_files` CHANGE `file_open_by` `file_open_by` int(11) NOT NULL;

ALTER TABLE `bookshop_doc_rel` CHANGE `dr_access_code` `dr_access_code` INT( 11 ) NOT NULL DEFAULT '15';

ALTER TABLE `bookshop_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `bookshop_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `bookshop_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';


ALTER TABLE `cadlib_doc_files` CHANGE `file_id` `file_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `document_id` `document_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `file_type` `file_type` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_doc_files` CHANGE `file_open_date` `file_open_date` int(11) NOT NULL;
ALTER TABLE `cadlib_doc_files` CHANGE `file_open_by` `file_open_by` int(11) NOT NULL;

ALTER TABLE `cadlib_doc_rel` CHANGE `dr_access_code` `dr_access_code` INT( 11 ) NOT NULL DEFAULT '15';

ALTER TABLE `cadlib_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `cadlib_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `cadlib_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';

ALTER TABLE `cadlib_metadata_rel` ADD UNIQUE `cadlib_metadata_rel_uniq` ( `cadlib_id` , `field_name` );

ALTER TABLE `container_indice` ADD `indice_value` varchar(8) NOT NULL;
INSERT INTO `container_indice` (`indice_id`, `indice_value`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'P'),
(16, 'Q'),
(17, 'R'),
(18, 'S'),
(19, 'T'),
(20, 'U'),
(21, 'V'),
(22, 'W'),
(23, 'X'),
(24, 'Y'),
(25, 'Z'),
(26, 'AA'),
(27, 'AB'),
(28, 'AC'),
(29, 'AD'),
(30, 'AE'),
(31, 'AF'),
(32, 'AG'),
(33, 'AH'),
(34, 'AJ'),
(35, 'AK'),
(36, 'AL'),
(37, 'AM'),
(38, 'AN'),
(39, 'AP'),
(40, 'AQ'),
(41, 'AR'),
(42, 'AS'),
(43, 'AT'),
(44, 'AU'),
(45, 'AV'),
(46, 'AW'),
(47, 'AX'),
(48, 'AY'),
(49, 'AZ');



ALTER TABLE `doctypes` CHANGE `file_extension` `file_extension` varchar(32) DEFAULT NULL;
ALTER TABLE `doctypes` ADD `visu_file_extension` varchar(32) DEFAULT NULL;


ALTER TABLE `mockup_doc_files` CHANGE `file_id` `file_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `document_id` `document_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `file_type` `file_type` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `mockup_doc_files` CHANGE `file_open_date` `file_open_date` int(11) NOT NULL;
ALTER TABLE `mockup_doc_files` CHANGE `file_open_by` `file_open_by` int(11) NOT NULL;

ALTER TABLE `mockup_doc_rel` CHANGE `dr_access_code` `dr_access_code` INT( 11 ) NOT NULL DEFAULT '15';

ALTER TABLE `mockup_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `mockup_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `mockup_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';

ALTER TABLE `mockup_metadata_rel` ADD UNIQUE `mockup_metadata_rel_uniq` ( `mockup_id` , `field_name` );






ALTER TABLE `workitem_documents_history` ADD `comment` text DEFAULT NULL;
ALTER TABLE `bookshop_documents_history` ADD `comment` text DEFAULT NULL;
ALTER TABLE `cadlib_documents_history` ADD `comment` text DEFAULT NULL;
ALTER TABLE `mockup_documents_history` ADD `comment` text DEFAULT NULL;

ALTER TABLE `bookshop_documents` ADD `doc_space` CHAR( 8 ) NOT NULL DEFAULT 'bookshop' AFTER `document_indice_id`;
ALTER TABLE `cadlib_documents` ADD `doc_space` CHAR( 6 ) NOT NULL DEFAULT 'cadlib' AFTER `document_indice_id`;
ALTER TABLE `mockup_documents` ADD `doc_space` CHAR( 6 ) NOT NULL DEFAULT 'mockup' AFTER `document_indice_id`;
ALTER TABLE `workitem_documents` ADD `doc_space` CHAR( 8 ) NOT NULL DEFAULT 'workitem' AFTER `document_indice_id`;



ALTER TABLE `workitem_doc_files` CHANGE `file_id` `file_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `document_id` `document_id` int(11) NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `file_type` `file_type` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `workitem_doc_files` CHANGE `file_open_date` `file_open_date` int(11) NOT NULL;
ALTER TABLE `workitem_doc_files` CHANGE `file_open_by` `file_open_by` int(11) NOT NULL;

ALTER TABLE `workitem_doc_rel` CHANGE `dr_access_code` `dr_access_code` INT( 11 ) NOT NULL DEFAULT '15';

ALTER TABLE `workitem_files` CHANGE `file_name` `file_name` varchar(128) NOT NULL DEFAULT '';
ALTER TABLE `workitem_files` CHANGE `file_path` `file_path` text NOT NULL DEFAULT '';
ALTER TABLE `workitem_files` CHANGE `file_root_name` `file_root_name` varchar(128) NOT NULL DEFAULT '';

ALTER TABLE `workitem_metadata_rel` ADD UNIQUE `workitem_metadata_rel_uniq` ( `workitem_id` , `field_name` );

CREATE TABLE `workitem_import_history` (
  `import_order` int(11) NOT NULL default '0',
  `workitem_id` int(11) NOT NULL default '0',
  `package_file_name` varchar(128) NOT NULL default '',
  `description` varchar(64) default NULL,
  `state` varchar(32) default NULL,
  `import_date` int(11) default NULL,
  `import_by` int(11) default NULL,
  `import_errors_report` text ,
  `import_logfiles_file` text ,
  `package_file_path` text ,
  `package_file_extension` varchar(32) default NULL,
  `package_file_mtime` int(11) default NULL,
  `package_file_size` int(11) default NULL,
  `package_file_md5` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`import_order`)
);

