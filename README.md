# README #

This README would normally document whatever steps are necessary to get your application up and running.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### What is this repository for? ###

Ranchbe, le ranch du bureau d'étude, est le premier PDM/PLM/GED.
Il incorpore les pratiques éditées par OMG.

Ranchbe is a PLM/PDM/GED for little ingineering organisation wich use 
CAO system.
The purpose of this software is to manage all documents of the engineering 
department. A clear distinction is operate between documents used for 
works and documents to delivers to customers or to manufacturers.

### Fonctionalities ###
+ Ged
+ Pdm
	(@todo: list of OMG implementation)
+ Vault
+ Workflow manager and designer
+ User access and and ACL
+ Modularity
+ 3D viewer

### Technologie ###
+ Language = PHP5.6
+ Framework php = Zend framework 2
+ Framework Javascripts = Jquery, threeJs, babyloneJs
+ Framework Css = Bootstrap

### Version identification ###

Ranchbe 1.0 is a beta version. Its not for production.

Productions versions are identified by a even minor version.
Odd minor version is a dev version, for tests only.

Example :

+ 1.1 = dev version
+ 1.2 = production version
+ 1.3 = dev version

### Summary of set up for Ubuntu 14.04 ###

#### Setup your proxy connection if necessary

	export http_proxy=http://$user:$passwd@$proxyhost:$proxyport
	export https_proxy=http://$user:$passwd@$proxyhost:$proxyport
	export HTTP_PROXY=http://$user:$passwd@$proxyhost:$proxyport
	export HTTPS_PROXY=http://$user:$passwd@$proxyhost:$proxyport

#### Clone and install some dependencies :

	git clone https://ocyssau@bitbucket.org/ocyssau/ranchbe.git
	cd rbsier
	sudo apt-get install curl apache2 php5 mysql libssh2-1-dev libssh2-php
	sudo a2enmod rewrite
	php installer
	php composer.phar self-update
	php composer.phar install

#### Apache Configuration :

	sudo cp config/dist/apache.conf /etc/apache2/sites-available/rbsier.conf
	sudo nano /etc/apache2/sites-available/rbsier.conf
	sudo service apache2 restart

#### Init permissions :

	chmod -R 755 *

#### Init the owner of ranchbe file :

Ensure that the run user of apache is matching with the owner of the writable files. Apache user is defined in /etc/apache2/envvars, find follow lines :

	export APACHE_RUN_USER=www-data
	export APACHE_RUN_GROUP=www-data

And set the owner of the writables ranchbe files :

	chown -R www-data data/
	
For a production site you must set your own user/group rights stategy.

#### Install jquery-md5 :

	cd public/js
	git clone https://github.com/gabrieleromanato/jQuery-MD5
	cd ../..

#### Install threejs :
	
	cd public/js
	git clone https://github.com/mrdoob/three.js.git
	cd ../..

#### Database configuration :

	mysqladmin create rbsier -u root -p
	mysql -u root -p rbsier < Docs/db/exportStructureRbsier.sql
	mysql -u root -p rbsier < Docs/db/mysql/datas.sql
	mysql -u root -p rbsier < Docs/db/inituser.sql
	mysql -u root -p rbsier < Docs/db/mysql/sequences.sql
	mysql -u root -p rbsier < Docs/db/from0.6.1TO0.6.2.sql

#### Init configuration :
	run install script
	
	sudo php install.php

#### Restart the web server :
		
	sudo service apache2 restart
	
#### Configuration :

Edit public/.htaccess and config/autoload/local.php and setup your parameters. Other parameters may be copied from config/autoload/global.php. Adapt only the local.php file, global.php will be replaced at each update of ranchbe.

	nano public/.htaccess
	nano config/autoload/local.php

#### How to run tests :

@todo

#### Deployment instructions :

@todo

### Contribution guidelines ###
* Writing tests
@todo
* Code review
@todo
* Other guidelines
@todo

### Who do I talk to? ###
* Repo owner or admin
Olivier CYSSAU : ocyssau@free.fr

* Other community or team contact
None