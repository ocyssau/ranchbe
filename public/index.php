<?php
use Zend\Stdlib\ArrayUtils;


/*
 * Controller for ranchbe 0.6
* respond to url like /<module>/<controller>/<action>
*
* <module> is a sub-directory of Controller directory
*
* <controller> is class name define in file <controller>.php.
* This class must be set in namespace <controller>
*
* <action> is a method of <controller>
*/
chdir(__DIR__.'/../.');
//var_dump(getcwd());die;

ini_set('display_errors' , 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);

if (!defined('APPLICATION_PATH')) {
	define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

//Controller
$redirectUrl = $_SERVER['REDIRECT_URL'];
$redirectBase = $_SERVER['REDIRECT_BASE'];
$redirectUrl = trim(str_replace($redirectBase, '', $redirectUrl), '/');

$r = explode('/', $redirectUrl);
$action = strtolower($r[2]);
$controllerName = ucfirst($r[1]);
$module = ucfirst($r[0]);

($action == '') ? $action='index' : null;
($controllerName == '') ? $controllerName='Home' : null;
($module == '') ? $module='Application' : null;

require_once 'module/Ranchbe/Ranchbe.php';
require_once 'config/boot.php';

$appConfig = include 'config/application.config.php';
rbinit_autoloader($appConfig);

$ranchbe = new Ranchbe();
$local = include 'config/autoload/local.php';
$global = include 'config/autoload/global.php';
$config = ArrayUtils::merge($global,$local);
$ranchbe->setConfig($config['rbp']);

rbinit_rbservice($config['rbp']);
rbinit_web($config['rbp']);
rbinit_enableDebug(true);
rbinit_userprefs($config['rbp']);
rbinit_session($config['rbp']);
rbinit_authservice($config['rbp']);
rbinit_currentUser();
rbinit_view($config['rbp']);
if(DEBUG === true){
	rbinit_checkInstall($config['rbp']);
	rbinit_enableDebug(true);
}
else{
	rbinit_enableDebug(false);
}
rbinit_errorStack($config['rbp']);
rbinit_unactive(DESACTIVATE);
rbinit_filesystem($config['rbp']);
rbinit_logger($config['rbp']);

/* init ZF2 Mvc */
if(in_array($module, array('Docflow', 'Workflow', 'Auth', 'Ranchbe', 'Pdm', 'Discussion', 'Rbdocument', 'Rbapplication', 'Custom', 'Ged', 'Message', 'Search'))){
	Zend\Mvc\Application::init($appConfig)->run();
	die;
}

/* Detect AJAX request */
if( ( (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ) )
{
	$isService = true;
}
else{
	$isService = false;
}

$includeFile = 'Controller/' .$module. '/' .$controllerName. '.php';
if(is_file($includeFile))
{
	$controllerClass='\\Controller\\'.$module.'\\'.$controllerName;
	$controller = new $controllerClass();
	$controller->setRoute($module, $controllerName, $action);
	$controller->setBaseUrl(BASEURL);

	if($isService){
		$controller->initService();
		$methodName = $action . 'Service';
		try{
			if( method_exists($controller, $methodName) ){
				call_user_func(array($controller, $methodName));
			}
			else{
				$methodName = $action . 'Action';
				call_user_func(array($controller, $methodName));
			}
		}
		catch(\Exception $e){
			http_response_code(500);
			$controller->exceptionToJson($e);
		}
	}
	else{
		try{
			$controller->init();
			call_user_func(array($controller, $action . 'Action'));
			$controller->postDispatch();
		}
		catch(\Controller\ControllerException $e){
			$controller->errorStack()->error($e->getMessage());
			$controller->errorForward();
		}
	}
}
else{
	header("HTTP/1.0 404 Not Found");
	var_dump($module, $controller, $action, $includeFile);
}
