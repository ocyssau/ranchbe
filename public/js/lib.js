//Container for ranchbe globals vars
var ranchbe = function(){
	this.init = function(){
	};
}

//Fonction pour afficher une fenetre pop-up
function popup(pageUrl, windowName)
{
	return popupP(pageUrl, windowName, 300, 900);
}

//Fonction pour afficher une fenetre pop-up large
function popup2(pageUrl, windowName)
{
	return popupP(pageUrl, windowName, 300, 800);
}

//Fonction pour afficher une fenetre pop-up
function popup8x6(pageUrl, windowName)
{
	return popupP(pageUrl, windowName, 600, 800);
}

//fonction pour selections multiples d'options
function SversD() 
{
	indexS=document.trans.source.options.selectedIndex;
	if (indexS < 0) return;
	valeur=document.trans.source.options[indexS].text;
	document.trans.source.options[indexS]=null;
	a = new Option(valeur);
	indexD=document.trans.MetaAffProjectContentList.options.length;
	document.trans.MetaAffProjectContentList.options[indexD]=a;
}

function DversS() 
{
	indexD=document.trans.MetaAffProjectContentList.options.selectedIndex;
	if (indexD < 0) return;
	valeur=document.trans.MetaAffProjectContentList.options[indexD].text;
	document.trans.MetaAffProjectContentList.options[indexD]=null;
	a = new Option(valeur);
	indexS=document.trans.source.options.length;
	document.trans.source.options[indexS]=a;
}

//fonction pour ouvrir un formulaire dans un popup
//http://www.asp-php.net/ressources/codes/JavaScript-Ouvrir+un+popup+avec+un+envoi+POST.aspx
//Merci CrazyCat
//document.checkform.action='http://ranchbe.sierbla.int/smartStore.php'; pop_it(checkform , 600 , 1024); return false;
function pop_it(the_form , height , width){
	if(typeof height == 'undefined') height=300;
	if(typeof width == 'undefined') width=900;
	my_form = eval(the_form);
	popupP("/wait.php", "popup" , height , width);
	my_form.target = "popup";
	my_form.submit();
}


function popform(myformName, url, height, width)
{
	if(typeof height == 'undefined') height=300;
	if(typeof width == 'undefined') width=900;
	popupP("/wait.php", "popup" , height , width);
	var myform=$('form[name='+myformName+']');
	$('<input type="hidden" name="layout" value="popup">').appendTo(myform);
	myform.attr("action", url);
	myform.attr("target", "popup");
	myform.submit();
}

function nopopform(myformName, url){
	var myform=$('form[name='+myformName+']');
	myform.attr("action", url);
	myform.attr("target", "");
	myform.submit();
}

function pop_no(my_form) {
	my_form.target = '';
	my_form.submit();
}

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;


//display a dialog box for show details abour document id of spacename
function rbDocumentGetDetail(documentid,spacename,baseurl){
	var url = baseurl+"/document/detail/index?document_id="+documentid+"&space="+spacename;
	$.ajax({
		type : 'GET',
		url : url,
		dataType: 'html',
		success : function(data, textStatus, jqXHR){
			var domfrag=$(data); //convert html to JQuery element
			var dialogdiv=$('<div></div>').uniqueId();
			dialogdiv.append(domfrag);
			$('html').append(dialogdiv);
			var title = $(dialogdiv).find("#tabs-documentdetail").find("h3").text();
			dialogdiv.dialog({
				'height':700,
				'width':1200,
				title: title,
				close: function( event, ui ) {
					this.parentElement.remove();
					console.log("destroy dialog");
				},
				focus: function( event, ui ) {
				},
				dragStart: function(event,ui){
					$(this).parent(".ui-dialog").fadeTo(0,0.4);
				},
				dragStop: function(event,ui){
					$(this).parent(".ui-dialog").fadeTo(0,1);
				}
			});
		},
		error : function(data, textStatus, jqXHR){
			alert("None details " + data);
		}
	});
	$(window).scroll(function () {
		$(".ui-dialog").css("position","fixed");
	});
}


