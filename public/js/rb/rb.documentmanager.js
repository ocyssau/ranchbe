$(function() {
	$(".dropchildrenzone").droppable({
		drop:function(e, ui){
			var childId = $(ui.draggable).attr("id");
			var childSpace = $(ui.draggable).data("spacename");
			var childNumber = $(ui.draggable).attr("name");
			console.log(childId, childSpace);
			
			//create ajax request for create relation
			var spacename = $(this).data("spacename");
			var documentId = $(this).attr("id");
			var baseurl = document.baseurl;
			var url= baseurl+"/document/relation/addchildren";
			
			var params={};
			params.spacename = spacename;
			params.documentid = documentId;
			params.encode = 'json';
			params.children={
				'child1':{
					'documentid':childId,
					'space':childSpace
				}
			};
			$.ajax({
				type : 'POST',
				url : url,
				data : params,
				dataType: 'json',
				success : function(data, textStatus, jqXHR){
					//alert('Child relation is created.');
					$( ".dropchildrenzone" ).effect( 'highlight', {}, 500 );
					$( "p.dropFeedback" ).html(childNumber + ' is added!');
				},
				error : function(data, textStatus, jqXHR){
					return ajaxErrorToMsg(data);
				}
			});
			$(this).removeClass("dragOver");
			$(this).addClass("draggable");
			
		},
		over:function(e,ui){
			$(this).removeClass("draggable");
			$(this).addClass("dragOver");
			return false;
		},
		out:function(e,ui){
			$(this).removeClass("dragOver");
			$(this).addClass("draggable");
			return false;
		}
	});

	/*BUTTONS DEFINITIONS*/
	$(".delete-btn").click(function(e){
		if(confirm('Do you want really suppress this documents?')){
			var url = document.baseurl+'/document/manager/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".create-btn").click(function(e){
		var url = document.baseurl+'/document/manager/create';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".editmulti-btn").click(function(e){
		var url = document.baseurl+'/document/manager/editmulti';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});
	
	$(".edit-btn").click(function(e){
		var url = document.baseurl+'/document/manager/edit';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".marktosuppress-btn").click(function(e){
		var url = document.baseurl+'/document/manager/marktosuppressdoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".unmarktosuppress-btn").click(function(e){
		var url = document.baseurl+'/document/manager/unmarktosuppressdoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".checkout-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/checkoutdoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".chekin-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/checkindoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".checkinandkeep-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/updatedoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".checkinreset-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/resetdoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".putinws-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/putinws';
		return buttonOrAhrefAction(e, $(this), url, false);
	});

	$(".move-btn").click(function(e){
		var url = document.baseurl+'/document/datamanager/movedocument';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".newversion-btn").click(function(e){
		var url = document.baseurl+'/document/manager/newversion';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".runworkflow-btn").click(function(e){
		var url = document.baseurl+'/docflow/index/index';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".lock-btn").click(function(e){
		if(confirm('The lock is definitif. Are you sure?')){
			var url = document.baseurl+'/document/manager/lockdoc';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".unlock-btn").click(function(e){
		var url = document.baseurl+'/document/manager/unlockdoc';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".resetdoctype-btn").click(function(e){
		var url = document.baseurl+'/document/manager/resetdoctype';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".archive-btn").click(function(e){
		if(confirm('Do you want really archive this documents?')){
			var url = document.baseurl+'/document/archiver/archive';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".downloadlist-btn").click(function(e){
		var url = document.baseurl+'/document/manager/downloadlisting';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".basketadd-btn").click( function(e){
		var url = document.baseurl+'/document/basket/add';
		return buttonOrAhrefAction(e, $(this), url);
	});

	$(".basketremove-btn").click(function(e){
		var url = document.baseurl+'/document/basket/remove';
		return buttonOrAhrefAction(e, $(this), url);
	});

	$(".basketclean-btn").click(function(e){
		var url = document.baseurl+'/document/basket/clean';
		return buttonOrAhrefAction(e, $(this), url);
	});

	$(".refresh-btn").click(function(e){
		document.location.reload(true);
		return false;
	});

	/*POSTITS*/
	$(".postit-content").hide();
	$(".postit-button").click(function (e) {
		var content = $($(this).parent(".postit")[0].children[1]);
		content.show();
		return false;
	});

	$(".addpostit-btn").click(function(e){
		e.preventDefault();
		
		var newComment = prompt("Input your message", "");
		if (newComment == null) {
			return false;
		}

		var url = $(this).attr('href');
		var data = $(this).parents("#document-context-menu").first().data('menuItemData');
		var queryData = {};
		queryData.body = newComment;
		queryData.documentid = data.id;
		queryData.spacename = data.spacename;

		$.ajax({
			type : 'GET',
			url : url,
			data : queryData,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				document.location.reload();
				return false;
			},
			error : function(data, textStatus, jqXHR){
				alert("Postit failed" + data);
				return false;
			}
		});
		
		return false;
	});

	$(".deletepostit-btn").click(function(e){
		e.preventDefault();
		
		if(!confirm("Are you Sure?")){
			return false;
		}

		var url=document.baseurl+"/document/postit/delete";
		var id = $(this).parents(".rb-postit-item").data('id');
		var spaceName = $(this).parents(".rb-postit-item").data('spacename');

		$.ajax({
			type : 'GET',
			data : {id:id, spacename:spaceName},
			url : url,
			dataType: 'json',
			success : function(data, textStatus, jqXHR){
				document.location.reload();
			},
			error : function(data, textStatus, jqXHR){
				alert("Postit failed" + data);
			}
		});
		return false;
	});

	/*DRAG AND DROP*/
	$(".draggable").draggable({
		revert:true,
		helper:"clone",
		scroll:false,
		iframeFix: true
	});

	$(".droppable").droppable({
		drop: function(event, ui){
			var documentId = $(this).attr("id");
			var spaceName = $(this).attr("spacename");
			$( this ).addClass( "ui-state-highlight" );

			var url = document.baseurl+"/document/detail/addchildren?documentid="+documentId+"&spacename="+spaceName;
			location.replace(url);
		}
	});

	/*FIX DOCUMENTS*/
	$(".document-fix").click(function (e) {
		var documentId = $(this).attr("id");
		var spaceName = $(this).attr("spacename");
		var url = document.baseurl+"/document/manager/fix?documentid="+documentId+"&spacename="+spaceName;
		location.replace(url);
		
		return false;
	});
	
	$(".document-unfix").click(function (e) {
		var documentId = $(this).attr("id");
		var spaceName = $(this).attr("spacename");

		var url = document.baseurl+"/document/manager/unfix?documentid="+documentId+"&spacename="+spaceName;
		location.replace(url);
		return false;
	});
	
	

	/*INIT CONTEXTUAL MENU*/
	initContextMenu(".contextmenu-showbtn", "#document-context-menu");
	
	$("#document-context-menu .context-action-regular").click(function(e){
		e.preventDefault();

		//var menudefinition = $("#document-context-menu");
		var data = $(this).parents("#document-context-menu").first().data('menuItemData');
		console.log(data);
		var id = data.id;
		var spacename = data.spacename;
		var basecamp = data.basecamp;
		var acode = data.acode;
		var currentUserId = data.currentUserId;
		
		var url = $(this).attr('href');
		var urlDecomposed = url.split('?');
		var baseUrl = urlDecomposed[0];
		var searchUrl = urlDecomposed[1];
		console.log(urlDecomposed);

		url = baseUrl+"?documentid="+id+"&spacename="+spacename;
		if(searchUrl != ""){
			url = url+"&"+searchUrl;
		}
		console.log(url);

		document.location.assign(url);
		return false;
	});
	
	/**
	 * 
	 */
	function buttonOrAhrefAction(e,elemt,url,reload){
		if(elemt.is("button")){
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		else if(elemt.is("a")){
			var data = elemt.parents("#document-context-menu").first().data('menuItemData');
			var queryData = {};
			queryData.documentid = data.id;
			queryData.spacename = data.spacename;
			$.ajax({
				type : 'GET',
				url : url,
				data : queryData,
				dataType: 'json',
				success : function(data, textStatus, jqXHR){
					if(reload==true){
						document.location.reload();
					}
					return false;
				},
				error : function(data, textStatus, jqXHR){
					console.log(data.responseJSON);
					return false;
				}
			});
		}
		return false;
	}
	
	/**
	 * 
	 */
	function buttonOrAhrefGetpage(e,elemt,url){
		if(elemt.is("button")){
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		else if(elemt.is("a")){
			var data = elemt.parents("#document-context-menu").first().data('menuItemData');
			var queryData = {};
			queryData.documentid = data.id;
			queryData.spacename = data.spacename;
		}
		return false;
	}
	
	$("#iframe1").dialog();
});

