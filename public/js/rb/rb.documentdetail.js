$(function() {
	$( ".tabs" ).tabs();
	
	/* Master div must have data-id, data-spacename defined */
	var masterDiv = $("#rb-documentdetail");

	/*BUTTONS DEFINITIONS*/
	$(".af-role-select").change(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.buttonTrigger($(this));
		return false;
	});

	$("#assocfile-action-menu > .af-delete-btn").click(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.buttonTrigger($(this));
		document.location.hash = "#tab-files";
		document.location.reload();
		return false;
	});

	$("#assocfile-action-menu > .af-reset-btn").click(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.buttonTrigger($(this));
		document.location.hash = "#tab-files";
		document.location.reload();
		return false;
	});

	$("#assocfile-action-menu > .af-putinws-btn").click(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.buttonTrigger($(this));
		return false;
	});

	$("#assocfile-action-menu > .af-refresh-btn").click(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.refresh("files");
		return false;
	});

	$("#children-action-menu > .rel-delete-btn").click(function(e){
		var rbDocumentDetail = new rbdocumentdetail(masterDiv);
		rbDocumentDetail.buttonTrigger($(this));
		document.location.hash = "#tab-children";
		document.location.reload(true);
		return false;
	});
	
	$(".rel-getdetail-btn").click(function(e){
		var spacename = $(this).data("spacename");
		var documentId = $(this).data("documentid");
		var url = document.baseurl+"/rbdocument/detail/index?documentid="+documentId+"&spacename="+spacename;
		popupP(url, "Detail Of " + documentId, 800, 1200);
		return false;
	});
	
 });

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function rbdocumentdetail(div)
{
	this.masterDiv = div;

	this.options = {
		baseurl: document.baseurl,
		documentId: div.data('id'),
		spacename: div.data('spacename'),
		uploadUrl:''
	};

	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};

	this.ping = function()
	{
		console.log("hello!");
	};

	/**
	 * 
	 */
	this.buttonTrigger = function(button)
	{
		var actionName = button.attr('value');
		return eval("this." + actionName + "Action(button)");
	};

	/**
	 * 
	 */
	this.action = function(button, url, successMsg, confirmMsg, successcallback)
	{
		if(confirmMsg != ""){
			if(!confirm(confirmMsg)){
				return false;
			}
		}

		/* retrieve the parent form */
		var datastring = this.preventForm($(button).parents("form")).serialize();
		console.log( datastring );

		$.ajax({
			type: "POST",
			async: false, // Mode synchrone
			url: url,
			data: datastring,
			dataType: "json",
			success: function(data){
				console.log(data);
			},
			error: function(data){
				return ajaxErrorToMsg(data);
			}
		});

		return false;
	};

	/**
	 * 
	 */
	this.reload = function(tab)
	{
		/* inject div in context */
		var myDiv = this.masterDiv;
		
		$.ajax({
			type : 'GET',
			url : this.options.baseurl+"/rbdocument/detail/getcontent?documentid="+this.options.documentId+"&spacename="+this.options.spacename+"&tab="+tab,
			data : {},
			dataType: 'html',
			success : function(data, textStatus, jqXHR){
				var s = "#tab-"+tab;
				$(myDiv).find(s).empty().append(data); //retrieve the right tab div from context div
			},
			error : function(data, textStatus, jqXHR){
				alert(data);
			}
		});
	};
	
	/**
	 * 
	 */
	this.refresh = function(tab)
	{
		document.location.hash = "#tab-"+tab;
		document.location.reload();
	};

	/**
	 * To prevent the default submit action on the form
	 */
	this.preventForm = function(form)
	{
		console.log( "refine prevent default submit on "+form.id );
		form.unbind("submit");
		form.bind("submit", function(e){
			e.defaultPrevented();
			e.stopPropagation();
			console.log( "submit "+form.id );
			return false;
		});
		return form;
	};

	/**
	 * 
	 */
	this.deletefileAction = function(button)
	{
		var url = this.options.baseurl+"/docfile/service/delete";
		var successMsg="";
		var confirmMsg = "Do you want really suppress this file links";
		this.action(button, url, successMsg, confirmMsg);
		return false;
	};

	/**
	 * 
	 */
	this.resetfileAction = function (button)
	{
		var url = this.options.baseurl+"/docfile/service/resetfile";
		var successMsg = "Files are reset";
		var confirmMsg = "Do you want really reset this file links";
		this.action(button, url, successMsg, confirmMsg);
	};

	/**
	 * 
	 */
	this.setroleAction = function (select)
	{
		var url = this.options.baseurl+"/docfile/service/setrole";
		var successMsg="";
		var confirmMsg="";
		var roleId = select.val();
		var fileId = select.data('id');
		var spaceName = select.data('spacename');

		var ajaxdata = {fileid:fileId, roleid:roleId, spacename:spaceName};
		console.log( ajaxdata );

		$.ajax({
			type: "POST",
			url: url,
			data: ajaxdata,
			dataType: "json",
			success: function(data){
				console.log(data);
				select.parent().parent().effect( "highlight", {}, 1000 );
			},
			error: function(data){
				 return ajaxErrorToMsg(data);
			}
		});
		return false;
	};

	/**
	 * 
	 */
	this.putinwsAction = function(button)
	{
		var url=this.options.baseurl+"/docfile/service/putinws";
		var successMsg="Files are in your wildspace with prefix 'consult_'";
		
		var datastring = $(button).parents("form").serialize();
		console.log( datastring );

		$.ajax({
			type: "POST",
			url: url,
			data: datastring,
			dataType: "json",
			success: function(data){
				alert(successMsg);
			},
			error: function(data){
				ajaxErrorToMsg(data);
			}
		});
	};

	/**
	 * 
	 */
	this.addchildAction = function (button)
	{
		var url=this.options.baseurl+"/rbdocument/relation/addchild";
		var successMsg="";
		var confirmMsg="";
		this.action(button, url,successMsg,confirmMsg);
	};

	/**
	 * 
	 */
	this.childrendeleteAction = function (button)
	{
		var url=this.options.baseurl+"/rbdocument/relation/removelink";
		var successMsg="";
		var confirmMsg="";
		this.action(button, url,successMsg,confirmMsg);
	};

	/**
	 * 
	 */
	this.viewfatherdocAction = function (button)
	{
		var url=this.options.baseurl+"/rbdocument/detail/viewfatherdoc";
		var successMsg="";
		var confirmMsg="";
		this.action(button, url,successMsg,confirmMsg);
	};
	
	/**
	 * Call by fileReader, in the context of the fileReader
	 */
	this.uploadAndAssociateFile = function(event)
	{
		var splitresult = event.target.result.split(',');
		var mimestype = splitresult[0].split(';')[0];
		var encode = splitresult[0].split(';')[1];
		var data = splitresult[1];
		var filename = this.file.name;
		var spacename = this.rbContext.options.spacename;
		var documentId = this.rbContext.options.documentId;
		var params={};
		/* put rbContext in context */
		var rbContext = this.rbContext;
		
		params.spacename = spacename;
		params.documentid = documentId;
		params.encode = 'json';
		params.files = {
			'file1':{
				'name':filename,
				'data':{
					'name':filename,
					'md5':$.md5(data),
					'size':data.length,
					'data':data,
					'mimestype':mimestype,
					'encode':encode
				}
			}
		};
		$.ajax({
			type : 'POST',
			url : this.url,
			data : params,
			dataType: 'html',
			success : function(data, textStatus, jqXHR){
				rbContext.refresh('files');
			},
			error : function(data, textStatus, jqXHR){
				return ajaxErrorToMsg(data);
			}
		});
	};
	
};

