/**
 * 
 */
function rbconverter(form)
{
	/**
	 * 
	 */
	this.form = form;

	/**
	 * 
	 */
	this.options = {
		baseurl: document.baseurl,
		rbconverterUrl:document.baseurl,
	};

	/**
	 * 
	 */
	this.setOption = function(name,value)
	{
		eval("this.options."+name+"='"+value+"'");
		return this;
	};

	/**
	 * 
	 */
	this.setOptions = function(options)
	{
		this.options = options;
		return this;
	};
	
	/**
	 * 
	 */
	this.ping = function()
	{
		console.log("hello!");
	};
	
	/**
	 * 
	 */
	this.convertAndSubmit = function(files)
	{
		var form = this.form;
		var rbConverterUrl = this.options.rbconverterUrl;
		var ajaxData = {};
		ajaxData.files = files;
		console.log( ajaxData );
		
		$.ajax({
			type : 'get',
			url : rbConverterUrl,
			data : ajaxData,
			dataType: 'json',
			async: false,
			timeout: 3000,
		})
		.done(function(data, textStatus, jqXHR){
			form.append('<input type="hidden" value="save" name="save">');
			var pdmDataElemt = $('<input type="hidden" name="pdmdata">');
			pdmDataElemt.val(JSON.stringify(data));
			form.append(pdmDataElemt);
			console.log(data);
			console.log(pdmDataElemt);
			form.submit();
		})
		.fail(function(jqXHR, textStatus){
			if(jqXHR.responseJSON){
				if(jqXHR.responseJSON){
					if(jqXHR.responseJSON.errors){
						if(jqXHR.responseJSON.errors[0]){
							errorMessage = jqXHR.responseJSON.errors[0];
						}
					}
				}
				alert("Conversion Failed : " + errorMessage);
				console.log(jqXHR.responseJSON);
			}
			else{
				console.log(jqXHR);
				form.append('<input type="hidden" value="save" name="save">')
				form.submit();
			}
		});
		return false;
	};
	
	/**
	 * 
	 */
	this.convertAndSee = function(files)
	{
		var rbConverterUrl = this.options.rbconverterUrl;
		var ajaxData = {};
		ajaxData.files = files;
		console.log( ajaxData );
		
		$.ajax({
			type : 'get',
			url : rbConverterUrl,
			data : ajaxData,
			dataType: 'json',
			async: false,
			timeout: 3000,
		})
		.done(function(data, textStatus, jqXHR){
			console.log(data);
		})
		.fail(function(jqXHR, textStatus){
			var errorMessage = "";
			if(jqXHR.responseJSON){
				if(jqXHR.responseJSON){
					if(jqXHR.responseJSON.errors){
						if(jqXHR.responseJSON.errors[0]){
							errorMessage = jqXHR.responseJSON.errors[0];
						}
					}
				}
				alert("Conversion Failed : " + errorMessage);
				console.log(jqXHR.responseJSON);
			}
			else{
				console.log(jqXHR);
			}
		});
		return false;
	}
}

