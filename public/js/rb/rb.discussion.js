function discussion(){
	$('.rbp-discussion-newbtn').click(function($e){
		var rootElemt = $(this).parents('.rbp-discussions-panel').first();
		var discussionElemt = rootElemt.find('.rbp-discussions').first();
		var discussionUid = discussionElemt.attr('id');
		var newDiscussionElemt = rootElemt.find('.rbp-discussions-new').first();
		var inputElemt = newDiscussionElemt.find('.rbp-comment-respons-input').first();
		var commentText = inputElemt.val().trim();

		if(commentText != ""){
			data={
				discussionUid: discussionUid,
				parentId: null,
				body: commentText
			};

			$.ajax({
				type: 'get',
				url: document.baseurl+"/discussion/index/add",
				data: data,
				dataType:'json',
				error: function(jqXHR, textStatus, errorThrown){
					alert('Error durind creation: ' + textStatus);
				},
				success: function(data, textStatus, jqXHR){
					var newComment = commentFactory(data);
					discussionElemt.append(newComment);
				}
			});
		}

		return false;
	});

	$('.comment-delete-btn').click(function(){
		var commentElemt = $(this).parents('.rbp-comment').first();
		var id = commentElemt.attr('id');

		var ok = confirm('Are you sure that you want to delete this comment and all his children ?');
		if(ok != true) return;

		data={
				commentid: id,
		};

		$.ajax({
			type: 'get',
			url: document.baseurl+"/discussion/index/delete",
			data: data,
			dataType:'json',
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind deletion: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				commentElemt.remove();
			}
		});
	});

	$('.rbp-comment-responsbtn').click(function($e){
		var commentElemt = $(this).parents('.rbp-comment').first();
		var responsForm = commentElemt.find('.rbp-comment-respons-form').first();
		responsForm.toggle(200);
		return false;
	});

	$('.rbp-comment-savebtn').click(function($e){
		//get discussion
		var rootElemt = $(this).parents('.rbp-discussions-panel').first();
		var discussionElemt = rootElemt.find('.rbp-discussions').first();
		var discussionUid = discussionElemt.attr('id');

		//get parent comment
		var commentElemt = $(this).parents('.rbp-comment').first();
		var parentId = commentElemt.attr('id');

		//get respons datas
		var inputElemt = commentElemt.find('.rbp-comment-respons-input').first();
		var commentText = jQuery.trim( inputElemt.val() );

		if(commentText != ""){
			data={
					discussionUid: discussionUid,
					parentId: parentId,
					body: commentText
			};

			$.ajax({
				type: 'get',
				url: document.baseurl+"/discussion/index/add",
				data: data,
				dataType:'json',
				error: function(jqXHR, textStatus, errorThrown){
					alert('Error durind creation: ' + textStatus);
				},
				success: function(data, textStatus, jqXHR){
					var newComment = commentFactory(data);
					discussionElemt.append(newComment);

					var responsForm = commentElemt.find('.rbp-comment-respons-form').first();
					responsForm.toggle(200);

					var reponsDiv = commentElemt.find(".rbp-comment-respons").first();
					reponsDiv.append(newComment);
				}
			});
		}
		return false;
	});
}

function commentFactory(data){
	var head = 'By ' + data.ownerId + ' on ' + data.updated;
	var id = data.id;
	var body = data.body;

	var newComment = $('.rbp-comment-template').clone(true);
	newComment.removeClass('rbp-comment-template');
	newComment.addClass('rbp-comment');
	newComment.attr('id', id);
	newComment.find('.rbp-comment-heading').html(head);
	newComment.find('.rbp-comment-body').html(body);
	newComment.show();
	return newComment;
}

/**
 * Load discussion from ajax
 */
function loadDiscussionFromAjax(discussionUid){
	data={
			discussionUid: discussionUid
	};

	var discussionElemt = $('.rbp-discussions').filter('#' + discussionUid);

	$.ajax({
		type: 'get',
		url: document.baseurl + "/discussion/index",
		data: data,
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown){
			alert('unable to get the discussion: ' + textStatus);
		},
		success: function(list, textStatus, jqXHR){
			$.each(list, function(index, data){
				var commentElemt = commentFactory(data);

				if(data.parentId && data.parentId != 0){
					var parentElemt = $('#' + data.parentId);
					var reponsDiv = parentElemt.find(".rbp-comment-respons").first();
					reponsDiv.append(commentElemt);
				}
				else{
					discussionElemt.append(commentElemt);
				}
			});
		}
	});
}

/**
 * Load discussion from json
 */
function loadDiscussionFromJson(list, discussionUid){
	var discussionElemt = $('.rbp-discussions').filter('#' + discussionUid);

	$.each(list, function(index, data){
		var commentElemt = commentFactory(data);

		if(data.parentId && data.parentId != 0){
			var parentElemt = $('#' + data.parentId);
			var reponsDiv = parentElemt.find(".rbp-comment-respons").first();
			reponsDiv.prepend(commentElemt);
		}
		else{
			discussionElemt.prepend(commentElemt);
		}
	});
}

