function wumanager(){
	
	this.baseUrl = "";
	
	/**
	 * 
	 */
	this.addDeliverable = function(){
        var currentCount = $('.rbp-deliverable-list').children().length;
        //var template = $('.rbp-deliverable-item-template').first().html();
        var template = $('form').find("[data-template]").data("template");
        template = template.replace(/__index__/g, currentCount);
        $('.rbp-deliverable-list').last().append(template);
        
    	var fieldset = $(".rbp-deliverable-list").find("fieldset").last();
    	this.formatDeliverableFieldset(fieldset);
        
        return false;
	};
	
	/**
	 * 
	 */
	this.deleteDeliverable = function(button){
		var ok = confirm('Are you sur that you want delete this entry?');
		if(ok==true){
			var item = $(this).parents(".rbp-deliverable-item").first();
			var uid = item.find("#uid").attr("value");
			
			var data = {uid:uid};
			$.ajax({
				type: 'get',
				url:wumanager.baseUrl+"deliverable/delete",
				data: data,
				error: function(jqXHR, textStatus, errorThrown){
					alert('Error durind deletion: ' + textStatus);
				},
				success: function(data, textStatus, jqXHR){
					var form = item.parents("form").first();
					item.parents("form").append('<input type="hidden" name="deleted[]" value="'+uid+'">');
					item.remove(); //remove line in list editor
				}
			});
		}
		return false;
	};
	
	
	
	/**
	 */
	this.delFile = function(componentId, fileId, button)
	{
		var myGetterUrl = this.componentGetterBaseUrl+"/delfile";
		$.ajax({
			type: 'get',
			url:myGetterUrl,
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error durind deletion of file: ' + textStatus);
			},
			success: function(data, textStatus, jqXHR){
				//reload current component
				var comptype = 'filelist';
				var url = rbportail.componentGetterBaseUrl+"/get"+comptype+"/"+componentId;
				$.get( url, function( data ) {
					$(button).parents('div.rbp-component').first().parent().html( data );
				});
			}
		});
	};
	
	/**
	 * 
	 * @param fieldset
	 */
	this.formatDeliverableFieldset = function(fieldset, mode){
		panel = $('<div class="panel panel-default rbp-deliverable-item"><div class="panel-heading"></div><div class="panel-body"></div></div>');
		var col1 = $('<div class="col-md-4"></div>');
		var col2 = $('<div class="col-md-4"></div>');
		var col3 = $('<div class="col-md-4"></div>');
		var row = $('<div class="row"></div>');
		row.append(col1);
		row.append(col2);
		row.append(col3);
		panel.find('.panel-body').append(fieldset);
		panel.find('.panel-body').append(row);
		$(".rbp-deliverable-list").append(panel);
		
		var pcol = col1;
		var c = fieldset.find('label').length;
		var d = c/3;
		var i = 0;
		$.each(fieldset.find('label'), function(index, formControl){
			var fgroup = $('<div class="form-group"></div>');
			var nextElemt = $(formControl).next();
			if(nextElemt.is('ul') ){
				//move error messages ul
				fgroup.append(nextElemt);
			}
			fgroup.prepend(formControl);
			pcol.append(fgroup);
			i++;
			if(i>d){
				pcol = pcol.next();
				i=0;
			}
		});
		
		if(mode=='edit'){
			var minusButton = $('<button></button>');
			minusButton.attr('title', 'Remove');
			minusButton.attr('data-toggle', 'tooltip');
			minusButton.addClass("btn btn-danger btn-xs remove-btn rbp-delete-row pull-right");
			minusButton.css("height", "15px");
			minusButton.css("width", "15px");
			minusButton.append('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
			minusButton.click(wumanager.deleteDeliverable);
			panel.prepend(minusButton);
		}
	};
	
	/**
	 * 
	 */
	this.saveStatus = function(id, status){
		data={};
		data.id = id;
		data.status = status;
		$.ajax({
			type: 'get',
			url:wumanager.baseUrl+"workunit/status",
			data: data,
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus +': '+ errorThrown);
			},
			success: function(data, textStatus, jqXHR){
			}
		});
	};
	
	/**
	 * Get activity from ajax request
	 */
	this.selectActivity = function(url){
		$.ajax({
			type: 'get',
			url:url,
			data:{},
			dataType:'json',
			error: function(jqXHR, textStatus, errorThrown){
				alert(textStatus + ': ' + errorThrown);
			},
			success: function(activities, textStatus, jqXHR){
				var parent = $('#selectActivityModal').find(".activities-list").first();
				parent.empty();
				
				var wuname = $('#selectActivityModal').find(".wu-name").first();
				wuname.html('');
				
				var instanceId = "";
				var processName = "";
				
				var row = $();
				var col1 = $();
				var col2 = $();
				var col3 = $();

				$.each(activities.activities, function(index,activity){
					if(activity.instanceId != instanceId){
						instanceId = activity.instanceId;
						processName = activity.procName;
						row = $('<div class="row"></div>');
						col1 = $('<div class="col-md-4"></div>');
						col2 = $('<div class="col-md-4"></div>');
						col3 = $('<div class="col-md-4"></div>');
						
						parent.append(row);
						row.append(col1);
						row.append(col2);
						row.append(col3);
						
						col1.append(processName+'-'+instanceId);
					}
					
					var btn = $('<button type="button" class="btn btn-primary btn-run-activity"></button>');
					col2.append(btn);
					btn.html(activity.label);
					btn.data("iaid", activity.id); //instance activity id
					btn.data("aid", activity.activityId); //activity id
					btn.data("iid", activity.instanceId); //process instance id
					btn.data("type", activity.type); //type of activity
					
					var comments = JSON.parse(activity.attributes).comments;
					col3.append(comments);
				});
				
				$.each(activities.standalone, function(index,activity){
					var btn = $('<button type="button" class="btn btn-primary btn-run-activity"></button>');
					col2.append(btn);
					btn.html(activity.label);
					btn.data("iaid", activity.activityId); //instance activity id
					btn.data("aid", activity.activityId); //activity id
					btn.data("iid", activity.procId); //process instance id
					btn.data("type", activity.type); //type of activity

					var comments = JSON.parse(activity.attributes).comments;
					col3.append(comments);
				});
				
				$(".btn-run-activity").click(function(e){
					var activityId = $(this).data("iaid");
					var procInstId = $(this).data("iid");
					var type = $(this).data("type");
					runActivity(activityId, procInstId, type);
				});
			}
		});
		$('#selectActivityModal').modal({});
	};
}

