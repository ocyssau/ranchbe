#!/usr/bin/php
<?php

namespace rbservicecli;

/**
 * Necessite curl php5 extension.
 * sudo apt-get install php5-curl
 */

/**
 *
 */
function displayhelp()
{
	$display = 'Utilisation :

	php tests.php
	[-h]
	[-t <fonction>]

	Avec:
	-h : Affiche cette aide.
	-t : Execute la function test <fonction> parmis les fonctions suivantes :';
	echo $display;
	displaylisttest();
}


function displaylisttest()
{
	echo $display='
	checkoutPOST
	checkinPOST
	checkinFromDocfilePOST
	uploadPOST
	downloadGET
	downloadPOST
	ZIP
	';
}

/**
 *
 */
function run()
{
	error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
	ini_set ( 'display_errors', 1 );
	ini_set ( 'display_startup_errors', 1 );

	ini_set('xdebug.collect_assignments', 1);
	ini_set('xdebug.collect_params', 4);
	ini_set('xdebug.collect_return', 1);
	ini_set('xdebug.collect_vars', 1);
	ini_set('xdebug.var_display_max_children', 1000);
	ini_set('xdebug.var_display_max_data', 5000);
	ini_set('xdebug.var_display_max_depth', 4);

	echo 'include path: ' . get_include_path ()  . PHP_EOL;

	$shortopts = 'h';
	$shortopts .= "t:"; //test function testcli_test_[suffix]

	$longopts  = array(
	);

	$options = getopt($shortopts, $longopts);

	if( isset($options['h']) ){
		displayhelp();
	}

	if ( isset($options['t']) ) {
		$suffix = $options['t'];
		call_user_func(__NAMESPACE__.'\test_'.$suffix, array());
	}
}

/**
 * install Zip extension
 sudo apt-get install make php5-dev libpcre3-dev
 mkdir php5-zip-install
 cd php5-zip-install
 wget http://pecl.php.net/get/
 uncompress zip-1.12.4.tgz
 tar xvf zip-1.12.4.tar
 cd zip-1.12.4
 phpize
 ./configure
 make
 sudo make install
 echo "extension=zip.so" > zip.ini
 sudo cp zip.ini /etc/php5/mods-available/zip.ini
 cd /usr/lib/php5/20121212+lfs/
 ls|grep zip
 php5enmod -s ALL zip
 sudo service apache2 restart
 *
 */
function test_ZIP()
{
	$file1='./data/testfile1.txt';
	$file2='./data/testfile2.txt';
	touch($file1);
	touch($file2);
	file_put_contents($file1, 'version001');
	file_put_contents($file2, 'version001');

	$zipFileName='./data/zip.zip';

	if(is_file($zipFileName)){
		unlink($zipFileName);
	}

	//create a ZipArchive
	$Zip=new \ZipArchive();
	$ok=$Zip->open($zipFileName, \ZipArchive::CREATE);
	var_dump($ok,$Zip->getStatusString());
	$ok=$Zip->addFile($file1, 'file1.txt');
	var_dump($ok,$Zip->getStatusString());
	$ok=$Zip->addFile($file2,  'file2.txt');
	var_dump($ok,$Zip->getStatusString());
	$ok=$Zip->close();

	assert(is_file($zipFileName));

	unlink($file1);
	unlink($file2);
}


/**
 *
 */
function test_checkoutPOST()
{
	//CHECKOUT
	$serviceUrl = 'http://localhost/rbservice/checkout';
	$curl = curl_init($serviceUrl);
	$serviceParams = array(
		'document1'=>json_encode(array('id'=>50)),
		'document2'=>json_encode(array('id'=>51)),
		'getfiles'=>0,
		'putinws'=>1,
		'replace'=>1,
	);

	$serviceParamsFromDocfile = array(
		'docfile1'=>json_encode(array('name'=>'DOCUMENT_TEST_1_df1.txt')),
		'docfile2'=>json_encode(array('name'=>'DOCUMENT_TEST_1_df2.txt')),
		'getfiles'=>0,
		'putinws'=>1,
		'replace'=>1,
	);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParamsFromDocfile);
	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	var_dump($curlResponse);
	curl_close($curl);
}

/**
 *
 */
function test_checkinPOST()
{
	$content='version'.uniqid();

	$myDocument1=array(
		'id'=>50,
		'docfile1'=>array(
			'name'=>'DOCUMENT_TEST_1_df1.txt',
			'data'=>array(
				'name'=>'DOCUMENT_TEST_1_df1.txt',
				'md5'=>\md5($content),
				'size'=>sizeof($content),
				'type'=>'file',
				'extension'=>'.txt',
			),
		),
		'docfile1'=>array(
			'name'=>'DOCUMENT_TEST_1_df2.txt',
			'data'=>array(
				'name'=>'DOCUMENT_TEST_1_df2.txt',
				'md5'=>\md5($content),
				'size'=>sizeof($content),
				'type'=>'file',
				'extension'=>'.txt',
			),
		),
	);

	/*
	 $myDocument1=array(
	 	'id'=>50,
	 );
	*/

	$myDocument2=array(
		'id'=>51,
	);


	$myDocfile1=array(
		'name'=>'DOCUMENT_TEST_1_df1.txt',
		'data'=>array(
			'name'=>'DOCUMENT_TEST_1_df1.txt',
			'md5'=>\md5($content),
			'size'=>sizeof($content),
			'type'=>'file',
			'extension'=>'.txt',
		),
	);

	$myDocfile2=array(
		'name'=>'DOCUMENT_TEST_1_df2.txt',
		'data'=>array(
			'name'=>'DOCUMENT_TEST_1_df2.txt',
			'md5'=>\md5($content),
			'size'=>sizeof($content),
			'type'=>'file',
			'extension'=>'.txt',
		),
	);

	//var_dump(md5_file('./data/wildspace/admin/DOCUMENT_TEST_1_df1.txt'), md5($content));die;
	//var_dump(filesize('./data/wildspace/admin/DOCUMENT_TEST_1_df1.txt'), sizeof($content));die;

	//CHECKIN
	$serviceUrl = 'http://localhost/rbservice/checkin';
	$curl = curl_init($serviceUrl);
	$serviceParamsDocument = array(
		'document1'=>json_encode($myDocument1),
		'document2'=>json_encode($myDocument2),
		'updatefile'=>'fromws', //fromzipdata, fromrequest or fromws or none : depuis les donn�es base64, ou la transmission ou depuis le wildspace ou pas de mise � jour
		'releasing'=>1,
	);

	$serviceParamsDocfile = array(
		'docfile1'=>json_encode($myDocfile1),
		'docfile2'=>json_encode($myDocfile2),
		'updatefile'=>'fromws', //fromzipdata, fromrequest or fromws or none : depuis les donn�es base64, ou la transmission ou depuis le wildspace ou pas de mise � jour
		'releasing'=>1,
	);

	$serviceParams=$serviceParamsDocfile;

	if($serviceParams['updatefile']=='fromws'){
		$wsPath=realpath('./data/wildspace/admin');
		$wsFile1=$wsPath.'/DOCUMENT_TEST_1_df1.txt';
		$wsFile2=$wsPath.'/DOCUMENT_TEST_1_df2.txt';
		file_put_contents($wsFile1, $content);
		file_put_contents($wsFile2, $content);
	}
	if($serviceParams['updatefile']=='fromzipdata'){
		$zipfile = './data/'.uniqid().'.zip';
		$zip = new \ZipArchive();
		$zip->open($zipfile, \ZipArchive::CREATE);
		$zip->addFromString('DOCUMENT_TEST_1_df1.txt', $content);
		$zip->addFromString('DOCUMENT_TEST_1_df2.txt', $content);
		$zip->close();
		$zipfile = realpath($zipfile);
		$serviceParams['zipdata'] = base64_encode(file_get_contents($zipfile));
	}

	/*
	 $serviceParams = array(
	 	'files'=>"@$zipFile",
	 );
	*/

	//curl_setopt($curl, CURLOPT_URL,$serviceUrl);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParams);

	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	var_dump($curlResponse);
	curl_close($curl);

	if(is_file($zipfile)){
		unlink($zipfile);
	}
}


function test_checkinFromDocfilePOST()
{
	$content='version'.uniqid();

	$myDocfile1=array(
		'name'=>'DOCUMENT_TEST_1_df1.txt',
		'data'=>array(
			'name'=>'DOCUMENT_TEST_1_df1.txt',
			'md5'=>\md5($content),
			'size'=>sizeof($content),
			'type'=>'file',
			'extension'=>'.txt',
		),
	);
	$myDocfile2=array(
		'name'=>'DOCUMENT_TEST_1_df2.txt',
		'data'=>array(
			'name'=>'DOCUMENT_TEST_1_df2.txt',
			'md5'=>\md5($content),
			'size'=>sizeof($content),
			'type'=>'file',
			'extension'=>'.txt',
		),
	);

	//CHECKIN
	$serviceUrl = 'http://localhost/rbservice/checkin';
	$curl = curl_init($serviceUrl);
	$serviceParams = array(
		'docfile1'=>json_encode($myDocfile1),
		'docfile2'=>json_encode($myDocfile2),
		'updatefile'=>'fromws', //fromzipdata, fromrequest or fromws or none : depuis les donn�es base64, ou la transmission ou depuis le wildspace ou pas de mise � jour
		'releasing'=>1,
	);

	if($serviceParams['updatefile']=='fromws'){
		$wsPath=realpath('./data/wildspace/admin');
		$wsFile1=$wsPath.'/DOCUMENT_TEST_1_df1.txt';
		$wsFile2=$wsPath.'/DOCUMENT_TEST_1_df2.txt';
		file_put_contents($wsFile1, $content);
		file_put_contents($wsFile2, $content);
	}
	if($serviceParams['updatefile']=='fromzipdata'){
		$zipfile = './data/'.uniqid().'.zip';
		$zip = new \ZipArchive();
		$zip->open($zipfile, \ZipArchive::CREATE);
		$zip->addFromString('DOCUMENT_TEST_1_df1.txt', $content);
		$zip->addFromString('DOCUMENT_TEST_1_df2.txt', $content);
		$zip->close();
		$zipfile = realpath($zipfile);
		$serviceParams['zipdata'] = base64_encode(file_get_contents($zipfile));
	}

	/*
	 $serviceParams = array(
	 	'files'=>"@$zipFile",
	 );
	*/

	//curl_setopt($curl, CURLOPT_URL,$serviceUrl);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParams);

	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	var_dump($curlResponse);
	curl_close($curl);

	if(is_file($zipfile)){
		unlink($zipfile);
	}
}


function _getProtoDocument($docid)
{
	$fname = $docid.'df1.txt';
	$content = $fname . ' v1.1';

	return array(
		'id'=>$docid,
		'name'=>'',
		'version'=>1,
		'iteration'=>1,
		'description'=>'a new document',
		'category'=>array(
			'name'=>'ma categorie'
		),
		'container'=>array(
			'id'=>10
		),
		'docfile1'=>array(
			'name'=>$fname,
			'version'=>1,
			'iteration'=>1,
			'description'=>'fichier1',
			'rule'=>0,
			'data'=>array(
				'name'=>$fname,
				'md5'=>md5($content),
				'size'=>0,
				'data'=>base64_encode($content),
				'type'=>'file',
				'extension'=>'.txt',
			),
		),
	);
}

/**
 *
 */
function test_uploadPOST()
{
	//init test data
	$zipfile='./data/tmpTestFile.zip';
	if(!is_file($zipfile)){
		$content=uniqid();
		$zip = new \ZipArchive();
		$zip->open($zipfile, \ZipArchive::CREATE);
		$zip->addFromString('DOCUMENT_TEST_1_df1.txt', $content);
		$zip->addFromString('DOCUMENT_TEST_1_df2.txt', $content);
		$zip->close();
	}

	//init curl
	$serviceUrl = 'http://localhost/rbservice/upload';
	$curl = curl_init($serviceUrl);

	//upload file
	$postParams=array(
		'zipfile' => '@' . realpath($zipfile),
		'param2'=>'test'
	);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postParams);

	// output the response
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	var_dump($curlResponse);
	curl_close($curl);
}

/**
 *
 */
function test_downloadGET()
{
	//init test data
	$zipfile='./data/download/tmpTestFile.zip';
	if(!is_file($zipfile)){
		$content=uniqid();
		$zip = new \ZipArchive();
		$zip->open($zipfile, \ZipArchive::CREATE);
		$zip->addFromString('DOCUMENT_TEST_1_df1.txt', $content);
		$zip->addFromString('DOCUMENT_TEST_1_df2.txt', $content);
		$zip->close();
	}
	$md5 = md5_file($zipfile);

	//init curl
	$serviceUrl = 'http://localhost/rbservice/download/tmpTestFile.zip';
	$curl = curl_init($serviceUrl);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($curl, CURLOPT_SSLVERSION,3);

	//GET download file
	curl_setopt($curl, CURLOPT_HTTPGET, true);
	$curlResponse = curl_exec($curl);
	$curlError = curl_error($curl);
	curl_close ($curl);

	$localZipFile='./data/GETreponse-'.basename($zipfile);
	file_put_contents($localZipFile, $curlResponse);

	$localmd5 = md5_file($localZipFile);
	assert( $md5 == $localmd5 );
	unlink($localZipFile);
}

/**
 *
 */
function test_downloadPOST()
{
	//init test data
	$zipfile='./data/download/tmpTestFile.zip';
	if(!is_file($zipfile)){
		$content=uniqid();
		$zip = new \ZipArchive();
		$zip->open($zipfile, \ZipArchive::CREATE);
		$zip->addFromString('DOCUMENT_TEST_1_df1.txt', $content);
		$zip->addFromString('DOCUMENT_TEST_1_df2.txt', $content);
		$zip->close();
	}
	$md5 = md5_file($zipfile);

	//init curl
	$serviceUrl = 'http://localhost/rbservice/download';
	$curl = curl_init($serviceUrl);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POST, true);
	$postParams=array(
		'file' => 'toto/tmpTestFile.zip',
	);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postParams);

	//POST download file
	$curlResponse = curl_exec($curl);
	$curlError = curl_error($curl);
	curl_close ($curl);

	$localZipFile='./data/POSTreponse-'.basename($zipfile);
	file_put_contents($localZipFile, $curlResponse);

	$localmd5 = md5_file($localZipFile);
	assert( $md5 == $localmd5 );
	unlink($localZipFile);
}

/**
 * php vendor/zfcampus/zf-oauth2/bin/bcrypt.php test
 * =>
 * $2y$10$V0fHo0lZNssPyaDePIScmuBvz4jm2YyAnJzizcJlShh9SCcx2Fx/.
 * =>
 * $2y$10$HbUlJ9IjO8suDMGqDeA/q.s95sPJ80yfNtiVhNJ9JPvQZwwAHRpzm
 *
 * http://localhost/rbservice/oauth/authorize?response_type=code&client_id=testclient&redirect_uri=/oauth/receivecode&state=xyz
 *
 INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
 ('MONAPP', '', '/oauth/receivecode', 'password refresh_token', NULL, NULL); *
 *
 INSERT INTO `oauth_users` (`username`, `password`, `first_name`, `last_name`) VALUES
 ('mon@mail.fr', '$2y$10$ivIGZ4msI0tvor2TqwIVhuQftk1HfD3S7s0nINeg3cYBVzWfFxGTS', 'PRENOM', 'NOM'); //testpass
 *
 *
 */
function test_oauth()
{
	$serviceUrl="http://localhost/rbservice/oauth";
	$curl = curl_init($serviceUrl);

	$serviceParams = array(
		'username'=>'mon@mail.fr',
		'password'=>'testpass',
		'grant_type'=>'password',
		'client_id'=>'MONAPP',
	);

	//curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($curl, CURLOPT_SSLVERSION,3);

	//GET download file
	//curl_setopt($curl, CURLOPT_HTTPGET, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParams);

	$curlResponse = curl_exec($curl);
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	$header = array();
	$header[] = 'Content-length: 0';
	$header[] = 'Content-type: application/json';
	$header[] = 'Authorization: Bearer '.$curlResponse['access_token'];
	//curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER,$header);

	//'Authorization : Bearer 448b94bf167ce3e5cd17b63794da510d7409bd74';

	$curlResponse = curl_exec($curl);
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	//appel un service non public



	curl_close ($curl);


}

/**
 *
 CREATE TABLE oauth_clients (
 client_id VARCHAR(80) NOT NULL,
 client_secret VARCHAR(80) NOT NULL,
 redirect_uri VARCHAR(2000) NOT NULL,
 grant_types VARCHAR(80),
 scope VARCHAR(2000),
 user_id VARCHAR(255),
 CONSTRAINT clients_client_id_pk PRIMARY KEY (client_id)
 );
 CREATE TABLE oauth_access_tokens (
 access_token VARCHAR(40) NOT NULL,
 client_id VARCHAR(80) NOT NULL,
 user_id VARCHAR(255),
 expires TIMESTAMP NOT NULL,
 scope VARCHAR(2000),
 CONSTRAINT access_token_pk PRIMARY KEY (access_token)
 );
 CREATE TABLE oauth_authorization_codes (
 authorization_code VARCHAR(40) NOT NULL,
 client_id VARCHAR(80) NOT NULL,
 user_id VARCHAR(255),
 redirect_uri VARCHAR(2000),
 expires TIMESTAMP NOT NULL,
 scope VARCHAR(2000),
 id_token VARCHAR(2000),
 CONSTRAINT auth_code_pk PRIMARY KEY (authorization_code)
 );
 CREATE TABLE oauth_refresh_tokens (
 refresh_token VARCHAR(40) NOT NULL,
 client_id VARCHAR(80) NOT NULL,
 user_id VARCHAR(255),
 expires TIMESTAMP NOT NULL,
 scope VARCHAR(2000),
 CONSTRAINT refresh_token_pk PRIMARY KEY (refresh_token)
 );
 CREATE TABLE oauth_users (
 username VARCHAR(255) NOT NULL,
 password VARCHAR(2000),
 first_name VARCHAR(255),
 last_name VARCHAR(255),
 CONSTRAINT username_pk PRIMARY KEY (username)
 );
 CREATE TABLE oauth_scopes (
 type VARCHAR(255) NOT NULL DEFAULT "supported",
 scope VARCHAR(2000),
 client_id VARCHAR (80),
 is_default SMALLINT DEFAULT NULL
 );
 CREATE TABLE oauth_jwt (
 client_id VARCHAR(80) NOT NULL,
 subject VARCHAR(80),
 public_key VARCHAR(2000),
 CONSTRAINT jwt_client_id_pk PRIMARY KEY (client_id)
 );
 *
 *
 INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
 ('MONAPP', '', '/oauth/receivecode', 'password refresh_token', NULL, NULL);

 php /vendor/zfcampus/zf-oauth2/bin/bcrypt.php MONMOTDEPASSE

 INSERT INTO `oauth_users` (`username`, `password`, `first_name`, `last_name`) VALUES
 ('mon@mail.fr', '$2y$10$4ixjHUo4zed.luLcbQQJhe8El0y7K7RqGO3vWI.rNCyomMg6Cg9WS', 'PRENOM', 'NOM');
 *
 */
function test_oauth()
{
	$serviceUrl = 'http://localhost/rbservice/oauth';
	$curl = curl_init($serviceUrl);

	$serviceParams = array(
		'username'=>'mon@mail.fr',
		'password'=>'testpass',
		'grant_type'=>'password',
		'client_id'=>'MONAPP',
	);

	$header=array(
		'Accept'=>'application/json',
		'Content-Type'=>'application/json',
	);

	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParams);

	//recupere le token
	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	$curlError = curl_error($curl);

	var_dump($curlResponse, $curlError);

	//recupere le token
	$header[]='Authorization: Bearer'. $curlResponse->access_token;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	$curlResponse = curl_exec($curl);

	var_dump($curlResponse, $curlError);

	//requete un service non public
	//$curl = curl_init('http://localhost/rbservice/container');
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => 'http://localhost/rbservice/container/'.$id,
		CURLOPT_HTTPGET=>true,
	));
	$curlResponse = json_decode(curl_exec($curl));
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	curl_close($curl);
}

function test_oauthdata()
{
	$loader = include __DIR__.'/vendor/autoload.php';
	$loader->set('Rbplm', realpath(__DIR__.'/vendor/'));
	$loader->set('Rba', realpath(__DIR__.'/vendor/'));

	$Bcrypt = new \Zend\Crypt\Password\Bcrypt;
	$password = 'testpass';
	$cryptedPass=$Bcrypt->create($password);
	var_dump($cryptedPass);

	//Bootstrap
	$config=include(__DIR__.'/config/autoload/local.php');
	\Rbplm\Dao\Connexion::setConfig($config['rbplm']['db']);
	$Connexion=\Rbplm\Dao\Connexion::get();
	try{
		$sql="INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES ('MONAPP', '', '/oauth/receivecode', 'password refresh_token', NULL, NULL);";
		$r=$Connexion->exec($sql);
	}
	catch(\Exception $e){
		var_dump($r,$sql);
	}
	try{
		$sql="INSERT INTO `oauth_users` (`username`, `password`, `first_name`, `last_name`) VALUES('mon@mail.fr', '$crypedPass', 'PRENOM', 'NOM');";
		$r=$Connexion->exec($sql);
	}
	catch(\Exception $e){
		var_dump($r, $sql);
	}
}


function test_container()
{
	$serviceUrl = 'http://localhost/rbservice/container';
	$curl = curl_init($serviceUrl);

	echo "POST \n";
	$serviceParams = array(
		'name'=>uniqid('container1'),
		'description'=>'container1',
		'reposit'=>'/tmp/',
		'forseenclosedate'=>'10-10-2015',
		'project'=>json_encode(array('id'=>10)),
		//'process'=>array('id'=>10),
		'extendproperties'=>json_encode(array(
			'property1'=>500,
			'property2'=>500,
		)),
	);

	//var_dump($serviceParams);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $serviceParams);

	$curlResponse = curl_exec($curl);
	$curlResponse=json_decode($curlResponse);
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	$id=$curlResponse->id;

	echo "GET \n";
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $serviceUrl.'/'.$id,
		CURLOPT_HTTPGET=>true,
	));
	$curlResponse = json_decode(curl_exec($curl));
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	echo "GET COLLECTION fetchAll \n";
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $serviceUrl,
		CURLOPT_HTTPGET=>true,
	));
	$curlResponse = json_decode(curl_exec($curl));
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	echo "PUT \n";
	$headers = array(
		"Content-Type" =>       "application/json",
		"Accept" =>             "application/json"
	);
	$updateParams['description']='modified';
	$query=http_build_query($serviceParams,'','&');
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER=>1,
		CURLOPT_CUSTOMREQUEST=>"PUT",
		CURLOPT_URL=>$serviceUrl.'/'.$id,
		CURLOPT_HEADER=>1,
		CURLOPT_HTTPHEADER=>$headers,
		//CURLOPT_POSTFIELDS=>$query,
	));
	$curlResponse = json_decode(curl_exec($curl));
	$curlError = curl_error($curl);
	var_dump($curlResponse);//,$curlError,$query);
	return;

	echo "DELETE \n";
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_CUSTOMREQUEST=>"DELETE",
		CURLOPT_URL => $serviceUrl.'/'.$id,
		CURLOPT_HTTPGET=>true,
	));
	$curlResponse = json_decode(curl_exec($curl));
	$curlError = curl_error($curl);
	var_dump($curlResponse,$curlError);

	curl_close($curl);
}



run();
