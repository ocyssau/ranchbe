<?php
//%LICENCE_HEADER%

namespace RbsTest;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Connexion;
use Rbplm\Vault\Reposit;
use Rbplm\Sys;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class InitData extends \Rbplm\Test\Test
{
    /**
     * @var    \Rbplm\Org\Unit
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        Sys\Filesystem::isSecure(false);
        $this->tid='5496995cc0aa4';
        
        $dataPath=realpath('data');
        var_dump($dataPath);
        $reposit = Reposit::init($dataPath.'/repositTest/'.$this->tid);
        
        echo '--PREPARE FILES ON DISK--'.CRLF;
        $file1 = $reposit->getUrl().'/DOCUMENT_TEST_1_df1.txt';
        $file2 = $reposit->getUrl().'/DOCUMENT_TEST_1_df2.txt';
        var_dump($file1, $file2);
        touch($file1);
        touch($file2);
        \file_put_contents($file1, 'version001' . "\n");
        \file_put_contents($file2, 'version001' . "\n");
        
        echo '--RUN SQL SCRIPTS--'.CRLF;
        $sql=file_get_contents($dataPath.'/dataSetForTest/forServiceTest.sql');
        echo $sql;
        $stmt = Connexion::get()->prepare($sql);
        $ok=$stmt->execute();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }

    /**
     */
    function Test_Dao()
    {
    }
}
