<?php
//%LICENCE_HEADER%

namespace RbsTest;

use Rbplm\Rbplm;
use Rbplm\Any;
use Rbplm\AnyObject;
use Rbplm\AnyPermanent;
use Rbplm\Uuid;
use Rbplm\Link;
use Rbplm\Signal;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Loader;

use Rbplm\Org\Project;
use Rbs\Org\Project\History;

use Rbplm\People\CurrentUser;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Org/UnitTest.php
 *
 */
class AnyTest extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		var_dump(Rbplm::getApiFullVersion());
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_Date()
	{
		$workitem = Workitem::init('TESTWI');
		$dao = DaoFactory::get()->getDao($workitem);

		//var_dump($dao->metaModel);
		//var_dump($dao->metaModelFilters);
		$dao->loadFromName($workitem, 'TESTWI');
		var_dump($workitem->forseenCloseDate);
		$workitem->forseenCloseDate->setDate(2020,06,01);
		var_dump($workitem->forseenCloseDate->getTimestamp());
		$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); //+5 years
		$workitem->forseenCloseDate->add(new \DateInterval('P5D')); //+5 days
		$workitem->forseenCloseDate->add(new \DateInterval('P5M')); //+5 month
		$dao->save($workitem);
	}


}


