<?php
//%LICENCE_HEADER%

namespace RbsTest\Acl;

use Rbs\Space\Factory as DaoFactory;

use Rbplm\Dao\NotExistingException;
use Rbplm\Dao\ExistingException;

use Rbplm\Acl\Acl;
use Rbplm\People\User;
use Rbplm\People\Group;
use Rbplm\People\CurrentUser;
use Rbplm\Org\Project;
use Rbplm\Org\Workitem;


use Rbs\Lu\Area;
use Rbs\Lu\Right;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Acl/Test.php
 */
class Test extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get();

		/*Reset current user*/
		$user = new User();
		$factory->getDao($user)->load($user,'handle=:handle', array(':handle'=>'admin'));
		$currentUser = CurrentUser::set($user);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_Lu()
	{
		$factory = DaoFactory::get();
		$user = CurrentUser::get();

		//get project
		try{
			$project = new Project();
			$factory->getDao($project)->loadFromUid($project, 'ACLTEST');
		}
		catch(NotExistingException $e){
			$project = Project::init('ACLTEST');
			$project->setUid('ACLTEST');
			$area = new Area($project->name);
			$right = new Right($area);
			$project->area = $area;

			$factory->getDao($area)->save($area);
			$factory->getDao($right)->save($right);
			$factory->getDao($project)->save($project);
		}

		//get group
		try{
			$group = new Group();
			$factory->getDao($group)->loadFromUid($group, 'GROUPTESTER');
		}
		catch(NotExistingException $e){
			$group = Group::init( 'GROUPTESTER' );
			$group->setDescription('description Of gtester');
			$group->setUid('GROUPTESTER');
			$factory->getDao($group)->save($group);
		}
		
		//get group
		try{
			$group2 = new Group();
			$factory->getDao($group2)->loadFromUid($group2, 'GROUPTESTER2');
		}
		catch(NotExistingException $e){
			$group2 = Group::init( 'GROUPTESTER2' );
			$group2->setDescription('description Of gtester');
			$group2->setUid('GROUPTESTER2');
			$factory->getDao($group2)->save($group2);
		}

		$areaId = $project->areaId;
		
		var_dump($user->getId(), $group->getId(), $group2->getId());
		
		//assign perm and role to user
		try{
			$factory->getDao(Group::$classId)->deletePerm($group->getId(), 'container_document_get', $areaId);
		}
		catch(ExistingException $e){
		}
		
		try{
			$factory->getDao(Group::$classId)->assignPerm($group2->getId(), 'container_document_get', $areaId);
		}
		catch(ExistingException $e){
		}
		
		//assign perm and role to user
		try{
			$factory->getDao(User::$classId)->deleteRole($user->getId(), $group->getId());
		}
		catch(ExistingException $e){
		}
		
		try{
			$factory->getDao(User::$classId)->deleteRole($user->getId(), $group2->getId());
		}
		catch(ExistingException $e){
		}
		
		//check permissions
		$luDao = new \Rbs\Lu\RightDao();
		$areaId = $project->areaId;
		$check = $luDao->check(CurrentUser::get()->getId(), 'container_document_get', $areaId);

		var_dump($areaId, $check);
	}

} //End of class

