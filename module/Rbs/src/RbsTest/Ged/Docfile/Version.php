<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged\Docfile;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Connexion;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Historize;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

/**
 * @brief Test class for \Rbs\Ged\Docfile\VersionDao
 */
class Version extends \Rbplm\Test\Test
{

	/**
	 * @var    \Rbplm\Ged\Docfile\Version
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->tid = uniqid();
		\Rbplm\Sys\Filesystem::isSecure(false);

		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		echo '--LOAD WORKITEM--'.CRLF;
		$workitem = Workitem::init('COTESTWI3');
		try{
			$factory->getDao($workitem)->loadFromName($workitem, 'COTESTWI3');
		}
		catch(NotExistingException $e){
			$reposit = Reposit::init($dataPath.'/repositTest/'.$workitem->getName());
			$workitem->path = $reposit->getPath();
			$workitem->description = 'Wi de test';

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); //5 years

			$workitem->setParent($project);
			$factory->getDao($workitem)->save($workitem);
		}
		$this->workitem = $workitem;

		echo '--LOAD DOCUMENT--'.CRLF;
		try{
			$document = new DocumentVersion();
			$factory->getDao($document)->loadFromUid($document, 'CODOCUMENTTEST_1');
		}
		catch(NotExistingException $e){
			$document = DocumentVersion::init('CODOCUMENTTEST_1');
			$document->setUid('CODOCUMENTTEST_1');
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setParent($this->workitem);

			$doctype = new Doctype();
			$factory->getDao($doctype)->loadFromName($doctype, 'text__plain');
			$document->setDoctype($doctype);

			$factory->getDao($document)->save($document);
		}
		$this->document = $document;

		echo '--LOAD DOCFILE--'.CRLF;
		try{
			$docfile = new DocfileVersion();
			$dfDao = $factory->getDao($docfile);
			$factory->getDao($docfile)->load($docfile, $dfDao->toSys('parentId').'='.$document->getId().' ORDER BY '.$dfDao->toSys('iteration').' DESC LIMIT 1');
		}
		catch(NotExistingException $e){
			$docfile = DocfileVersion::init($this->tid);
			$docfile->setNumber($this->tid);
			$docfile->setData($record);
			$docfile->setParent($document);
			$docfile->setCreateBy($currentUser);
			$factory->getDao($docfile)->save($docfile);
			$id = $docfile->id;
		}
		var_dump($docfile);
		$this->document->addDocfile($docfile);
		$this->docfile = $docfile;
	}

	/**
	 * From class: \Rbplm\Ged\Docfile\Version
	 */
	function Test_DaoSaveLoad()
	{
		$factory = $this->factory;
		$currentUser = CurrentUser::get();
		$currentUser->setUid('tester');
		$currentUser->setId(9999);

		$dataPath = realpath('data/cache');
		var_dump($dataPath);

		//Vault file
		echo '--CREATE RECORD IN VAULT--'.CRLF;
		$file1 = $dataPath.'/testfile1.txt';
		touch($file1);
		file_put_contents($file1, 'version001' . "\n");

		$reposit = Reposit::init($this->workitem->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$record = Record::init();
		$vaultfilename = 'data'.$this->tid.'.txt';
		$fsdata = new Fsdata($file1);
		$record = $vault->record( Record::init(), $fsdata, 'file', $vaultfilename );
		//var_dump($record);

		$record = $this->docfile->getData();
		var_dump($record->fullpath);
		var_dump($record->getFsdata()->getFullpath());

		assert($record->getFsdata() instanceof Fsdata);
		assert($record->getFsdata()->getFullpath() == $record->fullpath);
		assert( is_file($record->fullpath) );
		assert($this->docfile->getName()==$record->getName());
	}

	/**
	 * From class: \Rbplm\Ged\Docfile\Version
	 */
	function Test_Role()
	{
		$factory = $this->factory;
		$currentUser = CurrentUser::get();

		$document = DocumentVersion::init();
		$docfile = DocfileVersion::init();
		$docfileRole =
	}

}
