<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged;


use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Loader;
use Rbplm\Dao\Connexion;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;
use Rbplm\Ged\AccessCode;

use Rbs\Ged\DocumentDao;
use Rbs\Ged\DocumentVersionDao;
use Rbs\Ged\DocfileDao;
use RbS\Ged\DocfileVersionDao;
use Rbs\Ged\DoctypeDao;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbs\Lu\Area;
use Rbs\Lu\Right;

use Rbplm\Sys;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Date as DateTime;

use Rbplm\Ged\Historize;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class CheckOutIn extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->tid = uniqid();
		Sys\Filesystem::isSecure(false);

		$this->dataPath = realpath('data/cache');
		$dataPath = $this->dataPath;
		var_dump($this->dataPath);

		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		echo '--PREPARE FILES ON DISK--'.CRLF;
		$this->file1 = $this->dataPath.'/tmpTestfile1.txt';
		$this->file2 = $this->dataPath.'/tmpTestfile2.txt';
		touch($this->file1);
		touch($this->file2);
		\file_put_contents($this->file1, 'version001' . "\n");
		\file_put_contents($this->file2, 'version001' . "\n");

		echo '--LOAD PROJECT--'.CRLF;
		$project = Project::init('TESTPROJECT');
		try{
			$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');
		}
		catch(NotExistingException $e){
			$project->description='Projet de test';
			$area = new Area($project->getName());
			$right = new Right($area);
			$project->area = $area;
			$factory->getDao($area)->save($area);
			$factory->getDao($right)->save($right);
			$factory->getDao($project)->save($project);
		}
		$this->project = $project;

		echo '--LOAD WORKITEM--'.CRLF;
		$workitem = Workitem::init('WIFORTEST1');
		try{
			$factory->getDao($workitem)->loadFromName($workitem, 'WIFORTEST1');
		}
		catch(NotExistingException $e){
			$reposit = Reposit::init($dataPath.'/repositTest/'.$workitem->getName());
			$workitem->path = $reposit->getPath();
			$workitem->description = 'Wi de test';
			$workitem->setName('WIFORTEST1');
			$workitem->setUid('WIFORTEST1');

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); //5 years

			$workitem->setParent($project);
			$factory->getDao($workitem)->save($workitem);
		}
		$this->workitem = $workitem;
	}

	/**
	 *
	 */
	public function Test_Store()
	{
		$factory = $this->factory;
		$dataPath = $this->dataPath;
		$file1 = $this->file1;
		$file2 = $this->file2;

		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$reposit = Reposit::init($this->workitem->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		echo '--SAVE DOCUMENT v1--'.CRLF;
		try{
			$document = new DocumentVersion();
			$factory->getDao($document)->loadFromName($document, 'DOCUMENTTEST_1');
		}
		catch(NotExistingException $e){
			$document = DocumentVersion::init('DOCUMENTTEST_1');
			$document->setUid('DOCUMENTTEST_1');
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setBase(Document::init('BaseDocument')->setNumber($this->tid));
			$document->setParent($this->workitem);

			$doctype = new Doctype();
			$factory->getDao($doctype)->loadFromName($doctype, 'text__plain');
			$document->setDoctype($doctype);

			$category = Category::init(uniqid('CATEGORY1'));
			$category->description='Category de test';
			$document->setCategory($category);

			$factory->getDao($document)->save($document);
		}

		echo '--CREATE DOCFILES--'.CRLF;
		$fsdata1 = new Fsdata($file1);
		$fsdata2 = new Fsdata($file2);

		try{
			$docfile1 = new DocfileVersion();
			$factory->getDao($docfile1)->loadFromName($docfile1, 'DOCFILETEST_1.txt');

			$docfile2 = new DocfileVersion();
			$factory->getDao($docfile2)->loadFromName($docfile2, 'DOCFILETEST_2.txt');
		}
		catch(NotExistingException $e){
			$docfile1 = DocfileVersion::init();
			$docfile1->setBase(Docfile::init(uniqid('baseDocfile1')));
			$docfile1->setParent($document);

			$docfile2 = DocfileVersion::init();
			$docfile2->setBase(Docfile::init(uniqid('baseDocfile2')));
			$docfile2->setParent($document);
		}

		echo '--SAVE RECORDS IN VAULT--'.CRLF;
		try{
			$record1 = $vault->record( Record::init(), $fsdata1, 'DOCFILETEST_1.txt' );
			$record2 = $vault->record( Record::init(), $fsdata2, 'DOCFILETEST_2.txt' );
		}
		catch(ExistingException $e){
		}

		$docfile1->setData( $record1 );
		$docfile2->setData( $record2 );

		echo '--SAVE DOCFILE v1--'.CRLF;
		$factory->getDao($docfile1)->save($docfile1);
		$factory->getDao($docfile2)->save($docfile2);
	}


	/**
	 *
	 */
	public function Test_CoCi()
	{
		$factory = DaoFactory::get('Workitem');

		echo '--LOAD DOCUMENT v1--'.CRLF;
		$document1 = new DocumentVersion();
		$factory->getDao($document1)->loadFromName($document1, 'CODOCUMENTTEST_1');

		$document1->lock(AccessCode::FREE, null);
		$this->_checkOutDocument($document1);

		$document1 = new DocumentVersion();
		$factory->getDao($document1)->loadFromName($document1, 'CODOCUMENTTEST_1');
		$this->_checkInDocument($document1);
	}

	/**
	 *
	 * @param unknown_type $document
	 * @throws Exception
	 */
	private function _checkOutDocument($document)
	{
		$factory = DaoFactory::get('Workitem');
		$dfDao = $factory->getDao(DocfileVersion::$classId);

		echo '--CHECKOUT DOCUMENT--'.CRLF;
		$aCode = $document->checkAccess();

		var_dump($document->checkAccess());

		if( ($aCode > 1) || ($aCode < 0 || $aCode === 1) ){
			throw new Exception('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
		}
		else{
			$document->lock(AccessCode::CHECKOUT, CurrentUser::get());
		}

		echo '--CHECKOUT DOCFILE--'.CRLF;
		$dfDao->loadDocfiles($document);
		foreach($document->getDocfiles() as $docfile){
			var_dump($docfile->getUid());
			var_dump($item);

			if( $docfile->checkAccess() !== 0 ){
				continue;
			}
			else{
				$docfile->lock(AccessCode::CHECKOUT, CurrentUser::get());
			}
			$factory->getDao($docfile)->save($docfile);
		}

		$factory->getDao($document)->save($document);
	}


	/**
	 * Replace a file checkout in the vault and unlock it.
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 *   If the file has been changed(check by md5 code comparaison), create a new iteration
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param Bool $releasing		if true, release the docfile after replace
	 * @param Bool $checkAccess		if true, check if access code is right
	 * @throws Exception
	 * @return DocfileVersion
	 */
	private function _checkInDocument(\Rbplm\Ged\Document\Version $document, $releasing=true, $checkAccess=true)
	{
		$factory = DaoFactory::get('Workitem');

		echo '--PREPARE FILES ON DISK--'.CRLF;
		$dataPath = realpath('data');
		$reposit = Reposit::init($dataPath.'/repositTest/'.$this->tid);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));
		$file1 = $dataPath.'/tmpTestfile1.txt';
		$file2 = $dataPath.'/tmpTestfile2.txt';
		file_put_contents($file1, 'version002' . "\n");
		file_put_contents($file2, 'version002' . "\n");

		echo '--CHECKIN DOCUMENT--'.CRLF;
		$aCode = $document->checkAccess();
		$coBy = $document->lockById;

		var_dump($aCode, $coBy, CurrentUser::get()->getId());

		if( ($aCode!=1 OR $coBy!=CurrentUser::get()->getId()) AND $checkAccess ){
			throw new Exception('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
		}
		else{
			$document->unLock();
		}

		echo '--CHECKIN DOCFILES--'.CRLF;
		$docfileList = $factory->getList(DocfileVersion::$classId)->load('document_id='.$document->getId());
		foreach($docfileList as $item){
			$docfile = new DocfileVersion();
			$factory->getDao($docfile)->hydrate($docfile, $item);

			var_dump($docfile->getUid());

			$aCode = $docfile->checkAccess();
			$coBy = $docfile->lockById;

			var_dump($aCode, $coBy, $checkAccess);

			if( ($aCode!=1 OR $coBy!=CurrentUser::get()->getId()) AND $checkAccess)
			{
				throw new Exception(Error::LIMIT_ACCESS_VIOLATION,Error::LIMIT_ACCESS_VIOLATION,array());
				throw new Exception('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
			}
			else{
				$oldMd5=$docfile->getData()->md5;
				$newMd5=md5_file($file1);
				if($oldMd5==$newMd5){
					if($releasing){
						$docfile->unLock();
						$factory->getDao($docfile)->save($docfile);
					}
				}
				else{
					Historize::depriveRecordToIteration($reposit, $docfile->getData(), $docfile->iteration);
					$fsdata = new \Rbplm\Sys\Fsdata($file1);
					$vault->record($docfile->getData(), $fsdata, $docfile->getName() . '.file1.txt');
					$docfile->iteration = $docfile->iteration+1;
					//set the iteration id of document to the max iteration of docfiles
					if($nextDocumentIteration < $docfile->iteration){
						$nextDocumentIteration=$docfile->iteration;
					}
					if($releasing){
						$docfile->unLock();
					}
					$factory->getDao($docfile)->save($docfile);
				}
			}
		}

		if($document->iteration+1 > $nextDocumentIteration){
			$nextDocumentIteration=$document->iteration+1;
		}
		$document->iteration=$nextDocumentIteration;
		$factory->getDao($document)->save($document);
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		//DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
	}
}
