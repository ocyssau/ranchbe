<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged\Document;

/*
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Loader;
use Rbplm\Dao\Connexion;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\User\Wildspace;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;
use Rbplm\Ged\AccessCode;

use Rbs\Ged\DocumentDao;
use Rbs\Ged\DocumentVersionDao;
use Rbs\Ged\DocfileDao;
use RbS\Ged\DocfileVersionDao;
use Rbs\Ged\DoctypeDao;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbs\Lu\Area;
use Rbs\Lu\Right;

use Rbplm\Sys;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Date as DateTime;

use Rbplm\Ged\Historize;
*/

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Archive extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 *
	 * @throws NotExistingException
	 */
	public function getDocument()
	{
		echo '--LOAD DOCUMENT v1--'.CRLF;
		try{
			$document = new DocumentVersion();
			$this->factory->getDao($document)->loadFromName($document, 'DOCUMENTTEST_1');
			$document->setParent($this->workitem);
			echo '--LOAD DOCUMENT DOCFILES--'.CRLF;
			$this->factory->getDao(DocfileVersion::$classId)->loadDocfiles($document);
		}
		catch(NotExistingException $e){
			throw $e;
		}

		return $document;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		//DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
	}
}
