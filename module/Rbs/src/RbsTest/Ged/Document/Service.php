<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged\Document;


use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Loader;
use Rbplm\Dao\Connexion;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\User\Wildspace;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;
use Rbplm\Ged\AccessCode;

use Rbs\Ged\DocumentDao;
use Rbs\Ged\DocumentVersionDao;
use Rbs\Ged\DocfileDao;
use RbS\Ged\DocfileVersionDao;
use Rbs\Ged\DoctypeDao;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbs\Lu\Area;
use Rbs\Lu\Right;

use Rbplm\Sys;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Date as DateTime;

use Rbplm\Ged\Historize;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Service extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->tid = uniqid();
		Sys\Filesystem::isSecure(false);

		$this->dataPath = realpath('data/cache');
		$dataPath = $this->dataPath;
		var_dump($this->dataPath);

		$factory = DaoFactory::get('Workitem');
		$this->factory = $factory;

		echo '--LOAD PROJECT--'.CRLF;
		$project = Project::init('TESTPROJECT');
		try{
			$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');
		}
		catch(NotExistingException $e){
			$project->description='Projet de test';
			$area = new Area($project->getName());
			$right = new Right($area);
			$project->area = $area;
			$factory->getDao($area)->save($area);
			$factory->getDao($right)->save($right);
			$factory->getDao($project)->save($project);
		}
		$this->project = $project;

		echo '--LOAD WORKITEM--'.CRLF;
		$workitem = Workitem::init('WIFORTEST1');
		try{
			$factory->getDao($workitem)->loadFromName($workitem, 'WIFORTEST1');
		}
		catch(NotExistingException $e){
			$reposit = Reposit::init($dataPath.'/repositTest/'.$workitem->getName());
			$workitem->path = $reposit->getPath();
			$workitem->description = 'Wi de test';
			$workitem->setName('WIFORTEST1');
			$workitem->setUid('WIFORTEST1');

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new \DateInterval('P5Y')); //5 years

			$workitem->setParent($project);
			$factory->getDao($workitem)->save($workitem);
		}
		$this->workitem = $workitem;
	}

	/**
	 *
	 * @throws NotExistingException
	 */
	public function getDocument()
	{
		echo '--LOAD DOCUMENT v1--'.CRLF;
		try{
			$document = new DocumentVersion();
			$this->factory->getDao($document)->loadFromName($document, 'DOCUMENTTEST_1');
			$document->setParent($this->workitem);
			echo '--LOAD DOCUMENT DOCFILES--'.CRLF;
			$this->factory->getDao(DocfileVersion::$classId)->loadDocfiles($document);
		}
		catch(NotExistingException $e){
			throw $e;
		}

		return $document;
	}

	/**
	 *
	 */
	public function Test_Delete()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$reposit = Reposit::init($this->workitem->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$document = $this->getDocument();

		$service = new \Rbs\Ged\Document\Service($factory);
		$service->delete($document);
	}

	/**
	 *
	 */
	public function Test_Create()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		$document = $this->getDocument();

		echo '--PREPARE FILES ON DISK--'.CRLF;
		$file1 = '/tmp/Testfile1.txt';
		$file2 = '/tmp/Testfile2.txt';
		touch($file1);
		touch($file2);
		\file_put_contents($file1, 'version001' . "\n");
		\file_put_contents($file2, 'version001' . "\n");

		echo '--CREATE DOCFILES--'.CRLF;
		$fsdata1 = new Fsdata($file1);
		$fsdata2 = new Fsdata($file2);

		$docfile1 = DocfileVersion::init()->setParent($document);
		$docfile2 = DocfileVersion::init()->setParent($document);
		$docfile1->dao = $factory->getDao($docfile1->cid);
		$docfile2->dao = $factory->getDao($docfile2->cid);

		//Set fsdata property to indicate file must be vaulted
		$docfile1->fsdata = $fsdata1;
		$docfile2->fsdata = $fsdata2;
		$document->addDocfile($docfile1)->addDocfile($docfile2);

		$service->createdocument($document);
		var_dump($service->errors);
	}

	/**
	 *
	 */
	public function Test_Checkout()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		$document = $this->getDocument();

		$service->checkout($document, $withFiles=true, $replace=true, $getFiles=true);
	}

	/**
	 *
	 */
	public function Test_Checkin()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		$loader = $factory->getLoader();
		$wildspace = Wildspace::get(CurrentUser::get());
		$dao = $factory->getDao(DocumentVersion::$classId);

		$document = $this->getDocument();

		$service->checkin($document, $releasing=true, $updateData=true, $deleteFileInWs=true, $checkAccess=true);
	}

	/**
	 *
	 */
	public function Test_Reset()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		$dao = $factory->getDao(DocumentVersion::$classId);

		$document = $this->getDocument();

		$service->checkin($document, $releasing=true, $updateData=false, $deleteFileInWs=false, $checkAccess=true);
	}

	/**
	 *
	 */
	public function Test_Update()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		$dao = $factory->getDao(DocumentVersion::$classId);

		$document = $this->getDocument();

		$service->checkin($document, $releasing=false, $updateData=true, $deleteFileInWs=false, $checkAccess=true);
	}

	/**
	 *
	 */
	public function Test_Copy()
	{
		$factory = $this->factory;
		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$service = new \Rbs\Ged\Document\Service($factory);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		$dao = $factory->getDao(DocumentVersion::$classId);

		$document = $this->getDocument();

		$toDocument = $service->copyDocument($document, $this->workitem, 'COPY1', 1);

		foreach($toDocument->getDocfiles() as $docfile){
			$fsdata = $docfile->getData()->getFsdata();
			var_dump($fsdata->getFullpath());
		}
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
		//DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
	}
}
