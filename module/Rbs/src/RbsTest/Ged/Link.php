<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Loader;

use Rbplm\Org\Workitem;
use Rbplm\Org\Mockup;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;

use Rbs\Ged\DocumentDao;
use Rbs\Ged\DocumentVersionDao;
use Rbs\Ged\DocfileDao;
use RbS\Ged\DocfileVersionDao;
use Rbs\Ged\DoctypeDao;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbs\Lu\Area;
use Rbs\Lu\Right;

use Rbplm\Dao\NotExistingException;

use Rbplm\Sys;
use Rbplm\Sys\Date as DateTime;
use DateInterval;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Link extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get('Workitem');

		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$testFile='/tmp/test.txt';
		\file_put_contents($testFile, 'version 1.0');
		Sys\Filesystem::isSecure(false);

		$project = Project::init('TESTPROJECT');
		try{
			$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');
		}
		catch(NotExistingException $e){
			$project->description='Projet de test';
			$area = new Area($project->getName());
			$right=new Right($area);
			$project->area=$area;
			$factory->getDao($area)->save($area);
			$factory->getDao($right)->save($right);
			$factory->getDao($project)->save($project);
		}

		$workitem = Workitem::init('TESTWI');
		try{
			$factory->getDao($workitem)->loadFromName($workitem, 'TESTWI');
		}
		catch(NotExistingException $e){
			$workitem->path='/tmp/reposit1';
			$workitem->description = 'Wi de test';

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new DateInterval('P5Y')); //5 years

			$workitem->setParent($project);
			$factory->getDao($workitem)->save($workitem);
		}

		$doctype = new Doctype();
		$factory->getDao($doctype)->loadFromName($doctype, 'text__plain');

		try{
			$document=new DocumentVersion();
			$factory->getDao($document->cid)->loadFromName($document, 'DOCUMENTTEST_1');
		}
		catch(NotExistingException $e){
			$document=DocumentVersion::init('DOCUMENTTEST_1');
			//$document->cid = 'workitem_document_version';

			$document->number='DOCUMENTTEST_1';
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setParent($workitem);
			$document->setDoctype($doctype);

			$category=Category::init(uniqid('CATEGORY1'));
			$category->description='Category de test';
			$document->setCategory($category);

			$factory->getDao($document)->save($document);
		}

		try{
			$document2=new DocumentVersion();
			//$document2->cid = 'workitem_document_version';

			$factory->getDao($document2)->loadFromName($document2, 'DOCUMENTTEST_2');
		}
		catch(NotExistingException $e){
			$document2=DocumentVersion::init('DOCUMENTTEST_2');
			//$document2->cid = 'workitem_document_version';

			$document2->number='DOCUMENTTEST_2';
			$document2->description = "Document version de test";
			$document2->version = 1;
			$document2->iteration = 1;
			$document2->setParent($workitem);
			$document2->setDoctype($doctype);
			$factory->getDao($document2)->save($document2);
		}

		$this->project=$project;
		$this->workitem=$workitem;
		$this->document=$document;
		$this->document2=$document2;
	}

	/**
	 *
	 */
	function Test_DaoDocLink()
	{
		$document1 = $this->getDocument();
		$document2 = clone($document1);

		$document->setParent($this->workitem);
		$document2->setParent($this->workitem);

		$document->setName(uniqid());
		$document2->setName(uniqid());

		$factory = DaoFactory::get('Workitem');
		$dao = $factory->getDao($document);

		$dao->save($document);
		$dao->save($document2);

		$link = Doclink::init($document, $document2);
		$linkDao = $factory->getDao($link);
		$linkDao->save($link);

		try{
			$link = new Doclink();
			$hash = Doclink::hash($document, $document2);
			$factory->getDao($link)->load($link, "hash=:hash", array(':hash'=>$hash));
		}
		catch(\Rbplm\Dao\NotExistingException $e){
			throw new \Exception('Load of link must be debugged');
			echo $e->getMessage().PHP_EOL;
		}
		catch(\Exception $e){
			echo $e->getMessage().PHP_EOL;
			echo $e->getTraceAsString();
		}
	}
}
