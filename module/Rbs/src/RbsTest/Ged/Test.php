<?php
//%LICENCE_HEADER%

namespace RbsTest\Ged;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Loader;

use Rbplm\Org\Workitem;
use Rbplm\Org\Mockup;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\Document\Link as Doclink;

use Rbs\Ged\DocumentDao;
use Rbs\Ged\DocumentVersionDao;
use Rbs\Ged\DocfileDao;
use RbS\Ged\DocfileVersionDao;
use Rbs\Ged\DoctypeDao;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

use Rbs\Lu\Area;
use Rbs\Lu\Right;

use Rbplm\Dao\NotExistingException;

use Rbplm\Sys;
use Rbplm\Sys\Date as DateTime;
use DateInterval;

/**
 * @brief Test class for \Rbs\Org\Unit.
 *
 * @include Rbs/Org/UnitTest.php
 *
 */
class Test extends \Rbplm\Test\Test
{
	/**
	 * @var    \Rbplm\Org\Unit
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = DaoFactory::get('Workitem');

		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$testFile='/tmp/test.txt';
		\file_put_contents($testFile, 'version 1.0');
		Sys\Filesystem::isSecure(false);

		$project = Project::init('TESTPROJECT');
		try{
			$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');
		}
		catch(NotExistingException $e){
			$project->description='Projet de test';
			$area = new Area($project->getName());
			$right=new Right($area);
			$project->area=$area;
			$factory->getDao($area)->save($area);
			$factory->getDao($right)->save($right);
			$factory->getDao($project)->save($project);
		}

		$workitem = Workitem::init('TESTWI');
		try{
			$factory->getDao($workitem)->loadFromName($workitem, 'TESTWI');
		}
		catch(NotExistingException $e){
			$workitem->path='/tmp/reposit1';
			$workitem->description = 'Wi de test';

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new DateInterval('P5Y')); //5 years

			$workitem->setParent($project);
			$factory->getDao($workitem)->save($workitem);
		}

		$doctype = new Doctype();
		$factory->getDao($doctype)->loadFromName($doctype, 'text__plain');

		try{
			$document=new DocumentVersion();
			$factory->getDao($document->cid)->loadFromName($document, 'DOCUMENTTEST_1');
		}
		catch(NotExistingException $e){
			$document=DocumentVersion::init('DOCUMENTTEST_1');
			//$document->cid = 'workitem_document_version';

			$document->number='DOCUMENTTEST_1';
			$document->description = "Document version de test";
			$document->version = 1;
			$document->iteration = 1;
			$document->setParent($workitem);
			$document->setDoctype($doctype);

			$category=Category::init(uniqid('CATEGORY1'));
			$category->description='Category de test';
			$document->setCategory($category);

			$factory->getDao($document)->save($document);
		}

		try{
			$document2=new DocumentVersion();
			//$document2->cid = 'workitem_document_version';

			$factory->getDao($document2)->loadFromName($document2, 'DOCUMENTTEST_2');
		}
		catch(NotExistingException $e){
			$document2=DocumentVersion::init('DOCUMENTTEST_2');
			//$document2->cid = 'workitem_document_version';

			$document2->number='DOCUMENTTEST_2';
			$document2->description = "Document version de test";
			$document2->version = 1;
			$document2->iteration = 1;
			$document2->setParent($workitem);
			$document2->setDoctype($doctype);
			$factory->getDao($document2)->save($document2);
		}

		$this->project=$project;
		$this->workitem=$workitem;
		$this->document=$document;
		$this->document2=$document2;
	}

	/**
	 */
	function Test_Uuid()
	{
		$factory = DaoFactory::get('Workitem');

		$workitem = new Workitem();
		$factory->getDao($workitem)->loadFromName($workitem, 'TESTWI');
		var_dump($workitem->getUid());
		//assert($workitem->getUid() <> null);

		$document=new DocumentVersion();
		$factory->getDao($document)->loadFromName($document, 'DOCUMENTTEST_1');
		var_dump($document->getUid());
		//assert($document->getUid() <> null);

		$project=new Project();
		$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');
		var_dump($project->getUid());
		//assert($document->getUid() <> null);
	}

	/**
	 */
	function Test_Workitem()
	{
		$factory = DaoFactory::get('Workitem');
		$dao = $factory->getDao(Workitem::$classId);

		try{
			$workitem = new Workitem();
			$dao->loadFromUid($workitem, 'TESTWI3');
		}
		catch(NotExistingException $e)
		{
			$workitem = Workitem::init('TESTWI3');
			$workitem->setUid('TESTWI3');

			$workitem->repositPath='/tmp/reposit1';
			$workitem->description = 'Wi de test';

			$workitem->forseenCloseDate = new DateTime();
			$workitem->forseenCloseDate->add(new DateInterval('P5Y')); //5 years

			$dao->save($workitem);
		}

		$workitem->forseenCloseDate = new DateTime();
		$workitem->forseenCloseDate->add(new DateInterval('P5Y')); //5 years

		$dao->save($workitem);
	}

	/**
	 */
	function Test_Reposit()
	{
		$basePath = \Ranchbe::get()->getConfig('path.reposit.workitem');
		$wiName = 'test';
		$factory = DaoFactory::get('Workitem');
		$path = $basePath.'/'.$wiName;
		var_dump($path);
		$reposit = \Rbplm\Vault\Reposit::init($path);
	}

	/**
	 *
	 */
	function Test_Mockup()
	{
		$factory = DaoFactory::get('Mockup');

		$mockup = Workitem::init('TESTMOCKUP');
		$dao = $factory->getDao(Workitem::$classId);

		$project = new Project();
		$factory->getDao($project)->loadFromName($project, 'TESTPROJECT');

		try{
			$dao->loadFromName($mockup, 'TESTMOCKUP');
		}
		catch(NotExistingException $e){
			$mockup->repositPath='/tmp/reposit1';
			$mockup->description = 'Mockup for test';

			$mockup->forseenCloseDate = new DateTime();
			$mockup->forseenCloseDate->add(new DateInterval('P5Y')); //5 years
			$mockup->setParent($project);

			$dao->save($mockup);
		}

		$document = DocumentVersion::init(uniqid());
		$document->setParent($mockup);
		$dao = $factory->getDao($document);
		$dao->save($document);

		$docfile = DocfileVersion::init(uniqid());
	}

	/**
	 *
	 */
	function Test_Dao()
	{
		$factory = DaoFactory::get('Workitem');

		$user = new User();
		$factory->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);

		$testFile = '/tmp/test.txt';
		\file_put_contents($testFile, 'version 1.0');
		Sys\Filesystem::isSecure(false);

		$project = Project::init(uniqid('PROJ001'));
		$project->description='Projet de test';
		$area=new Area($project->getName());
		$right=new Right($area);
		$project->area=$area;

		$category = Category::init(uniqid('CATEGORY1'));
		//$category->cid = 'workitem_category';
		$category->description='Category de test';

		$workitem = Workitem::init(uniqid('WI001'));
		$workitem->repositPath='/tmp/reposit1';
		$workitem->description = 'Wi de test';
		$workitem->forseenCloseDate = time()+1000;
		$workitem->setParent($project);

		$doctype = new Doctype();
		$factory->getDao($doctype)->loadFromName($doctype, 'text__plain');

		$document = DocumentVersion::init(uniqid('DOCV001'));
		$document->number = uniqid('DOCV001');
		$document->description = "Document version de test";
		$document->version = 1;
		$document->iteration = 1;
		$document->setParent($workitem);
		$document->setDoctype($doctype);

		$document2 = DocumentVersion::init(uniqid('DOC2'));
		$document2->number = uniqid('DOC2');
		$document2->description = "Document version de test";
		$document2->version = 1;
		$document2->iteration = 1;
		$document2->setParent($workitem);
		$document2->setDoctype($doctype);

		$docfile=DocfileVersion::init(uniqid('DF001'));
		$docfile->number=uniqid('DF001');
		$docfile->description = "Document version de test";
		$docfile->version = 1;
		$docfile->iteration = 1;
		$docfile->setParent($document);

		$reposit = Reposit::init($workitem->repositPath);
		$record = Record::init('new record');

		$fromFsdata = new Fsdata($testFile);
		$toPath = $reposit->getUrl() .'/'. $docfile->number;

		$fromFsdata->copy($toPath, 0755, false);
		$toFsdata = new Fsdata($toPath);
		$record->setFsdata($toFsdata);
		$docfile->setData($record);

		//SAVE
		echo "Save Project area\n";
		$factory->getDao($area)->save($area);

		echo "Save Project right\n";
		$factory->getDao($right)->save($right);

		echo "Save Project area\n";
		$factory->getDao($project)->save($project);

		echo "Save Wi \n";
		$factory->getDao($workitem)->save($workitem);

		echo "Save Document \n";
		$factory->getDao($category)->save($category);
		$factory->getDao($document)->save($document);

		echo "Save Docfile \n";
		$factory->getDao($docfile)->save($docfile);

		//LOAD
		echo "Load Workitem \n";
		$id = $workitem->id;
		$object = new Workitem();
		$factory->getDao($object)->loadFromId($object, $id);

		//RELATION LOADER
		//process, project, user
		//var_dump($object->ownerId);

		//UPDATE SAVE
		echo "Update Workitem \n";
		$object->setName(uniqid('modified'));
		$factory->getDao($object)->save($object);

		$document->setCategory($category);
		$factory->getDao($document)->save($document);

		//DELETE
		echo "Delete Docfile, Document, Workitem, Project, Area, Process \n";
		$factory->getDao($docfile)->deleteFromId($docfile->getId());
		$factory->getDao($document)->deleteFromId($document->getId())->deleteFromId($document2->getId());
		$factory->getDao($workitem)->deleteFromId($workitem->getId());
		$factory->getDao($project)->deleteFromId($project->getId());
		$factory->getDao($area)->deleteFromId($area->getId());
		$factory->getDao($category)->deleteFromId($category->getId());
		return;
	}
}
