<?php
//%LICENCE_HEADER%

namespace RbsTest\Viewer;

use Rbplm\Rbplm;
use Rbplm\Any;
use Rbplm\AnyObject;
use Rbplm\AnyPermanent;
use Rbplm\Uuid;
use Rbplm\Link;
use Rbplm\Signal;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\People\CurrentUser;

/**
 * @brief Test class for \Rbplm\Org\Unit.
 *
 * @include Rbplm/Org/UnitTest.php
 *
 */
class Test extends \Rbplm\Test\Test
{
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		var_dump(Rbplm::getApiFullVersion());
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}




}
