<?php
//%LICENCE_HEADER%

namespace RbsTest\Extended;

use Rbplm\Any;
use Rbplm\AnyObject;
use Rbplm\Org\Workitem;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\CurrentUser;
use Rbs\Extended;

/**
 *
 */
class PropertyTest extends \Rbplm\Test\Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_LinkWithWi()
	{
		$link = new Link();
		$link->setParent();
		$link->setChild();
	}


	/**
	 *
	 */
	public function Test_Dao()
	{
		$factory = DaoFactory::get();

		$name = uniqId('fortest');
		$ecid = Workitem::$classId;

		$extend = new Extended\Property();
		$extend->setAppName($name);
		$extend->setType('text');
		$extend->setExtended($ecid);
		$extend->description = 'For test';

		$extend->multiple = 0;
		$extend->return = 0;
		$extend->hide = 0;
		$extend->attributes = array('class=input');

		$dao = $factory->getDao($extend);
		assert($dao instanceof Extended\PropertyDao);

		$extendTable = $factory->getTable(Workitem::$classId);
		$dao->setExtendedTable($extendTable);

		$dao->insert($extend);

		$extend2 = new Extended\Property();
		$dao->load($extend2, "appname=:name AND extendedCid=:cid", array(':name'=>$name, ':cid'=>$ecid));

		assert($extend2->description == $extend2->description);
		assert($extend2->getType() == $extend2->getType());
		assert($extend2->attributes == $extend2->attributes);

		$dao->deleteFromName($extend->getName(),Workitem::$classId);

		var_dump($dao);
	}

	/**
	 *
	 */
	public function Test_Loader()
	{
		$loader = new Extended\Loader();
		$optionalFields = $loader->load($container->cid, $dao);
		if(is_array($dao->extendedModel)){
			foreach($dao->extendedModel as $asSys=>$asApp){
				$select[] = $asSys . ' as '. $asApp;
			}
		}
	}

	/**
	 *
	 */
	public function Test_ExtendWi()
	{
		$factory = DaoFactory::get();
		$workitem = new Workitem();

		try{
			$extend = new Extended\Property();
			$extend->setAppName('fortest');
			$extend->setType('text');
			$extend->setExtended($workitem->cid);
			$extend->description = 'For test';
			$dao = $factory->getDao($extend);
			$extendTable = $factory->getTable($workitem->cid);
			$dao->setExtendedTable($extendTable);
			$dao->insert($extend);
		}
		catch(\Exception $e){
			echo 'extended is yet existing'.PHP_EOL;
		}

		$dao = $factory->getDao($workitem);

		/*
		$dao->extendedModel = array($extend->getName()=>'fortest');
		$dao->metaModel = array_merge($dao->metaModel,$dao->extendedModel);
		*/

		//Load extendedModel from loader
		$loader = new Extended\Loader();
		$loader->load($workitem->cid, $dao);

		var_dump($dao->extendedModel,$dao->metaModel);

		try{
			$dao->loadFromName($workitem, 'TESTWI');
			$workitem->fortest = 'Captain Jack Barrow';
			$workitem->description = uniqid();
			$dao->save($workitem);
		}
		catch(\Exception $e){
		}

		$workitem2 = new Workitem();
		try{
			$dao->loadFromName($workitem2, 'TESTWI');
			var_dump($workitem2->fortest);
		}
		catch(\Exception $e){
		}
	}
}

