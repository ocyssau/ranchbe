<?php

namespace Rbs\Viewer;

use Rbplm\Sys\Fsdata;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
/**
 *  Class to view file or attachment file
 */
class Viewer
{
	protected $fsdata; //fsdata Object
	protected $filePath; //Path to file
	protected $extension;
	protected $mimetype;
	protected $driverName; //The name of the driver to use
	protected $driver; //The driver to use
	protected $noread;

	/**
	 *
	 * @param Any $any
	 */
	function __construct()
	{
	}//End of method

	/**
	 *
	 * @param unknown_type $name
	 */
	public function __get($name)
	{
		return $this->$name;
	}//End of method

	/**
	 * Load the code for a specific Viewer driver. protected function.
	 */
	protected function _initDriver()
	{
		if(empty($this->driverName)){
			$this->driverName = 'Raw';
		}
		$driverClass = __NAMESPACE__.'\\Driver\\'.$this->driverName;
		$this->driver = new $driverClass($this);
		return true;
	} //End of method

	/**
	 * Get the properties from fsdata object
	 */
	public function initFromFsdata($fsdata)
	{
		$this->filePath = $fsdata->getFullpath();
		$this->extension = $fsdata->getExtension();
		$this->mimetype = $fsdata->getMimetype();
		$this->noread = false;
		$this->driverName = null;
		$this->fsdata = $fsdata;
		return $this;
	} //End of method

	/**
	 * Get the properties from docfile object
	 *
	 * @param DocfileVersion|DocfileIteration
	 */
	public function initFromDocfile($docfile)
	{
		$fsdata = $docfile->getdata()->getFsdata();
		$this->initFromFsdata($fsdata);
		return $this;
	} //End of method

	/**
	 * Init the properties of the current object from the file
	 */
	public function initFromFile($file)
	{
		if(is_file($file)||is_dir($file)){
			$fsdata = new Fsdata($file);
		}
		else{
			return false;
		}
		$this->initFromFsdata($fsdata);
		return $this;
	} //End of method

	/**
	 * Generate a embeded Viewer for the file
	 *  Return html code of the embeded Viewer
	 *
	 * @param
	 */
	protected function _embeded()
	{
		if(isset($this->driver) && isset($this->filePath))
			return $this->driver->embededViewer($this->filePath);
		else{
			return false;
		}
	}//End of method

	/**
	 * Push the file to the client
	 *
	 * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * Return true or false.
	 * @param $file(string) path of the file to display on the client computer
	 */
	protected function _pushfile()
	{
		if($this->noread == 'no_read'){
			throw new \Exception("you cant view this file $file by this way");
		}
		$this->fsdata->download();
	}//End of method

	/**
	 * Push main file to download it
	 */
	public function push()
	{
		return $this->_pushfile();
	}//End of method

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 *
	 * @param
	 */
	public function display()
	{
		$this->_initDriver(); //init the driver for the file
		return $this->_embeded(); //Return the embeded Viewer
	}//End of method

}//End of class
