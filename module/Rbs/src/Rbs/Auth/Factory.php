<?php
namespace Rbs\Auth;

use Rbplm\Dao\Connexion;

class Factory
{
	/**
	 * @return \Zend\Authentication\Adapter\AdapterInterface
	 */
	public static function getAdapter($conf)
	{
		if($conf['adapter']=='ldap'){
			$options = $conf['options'];
			$username = "";
			$password = "";
			$authAdapter = new Ldap($options,$username,$password);
		}
		elseif($conf['adapter']=='passall'){
			$authAdapter = new PassallAdapter();
		}
		elseif($conf['adapter']=='db'){
			$authAdapter = new DbAdapter();
			$authAdapter->setConnexion(Connexion::get());
		}
		return $authAdapter;
	}
}
