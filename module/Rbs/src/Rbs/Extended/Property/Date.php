<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended\Property;

class Date extends \Application\Model\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 206;
	
	/**
	 * @var string
	 */
	public $type = 'date';
	
	/**
	 * @var \date
	 */
	public $min=null;
	
	/**
	 * @var \date
	 */
	public $max=null;
	
	/**
	 * @var \integer
	 */
	public $step=null;
}
