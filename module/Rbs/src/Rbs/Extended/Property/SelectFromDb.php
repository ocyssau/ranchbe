<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended\Property;

class SelectFromDb extends \Application\Model\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 205;
	
	/**
	 * @var string
	 */
	public $type = 'selectfromdb';
	
	/**
	 * @var boolean
	 */
	public $multiple = false;
	
	/**
	 * @var string [value,name]
	 */
	public $return = 'value';
	
	/**
	 * @var string
	 */
	public $dbtable='';
	
	/**
	 * @var string
	 */
	public $dbFieldForName='';
	
	/**
	 * @var string
	 */
	public $dbFieldForValue='';
	
	/**
	 * @var string
	 */
	public $dbfilter='';
	
	/**
	 * @var string
	 */
	public $dbquery='';
}
