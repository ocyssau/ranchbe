<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended\Property;

class Select extends \Application\Model\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 204;
	
	/**
	 * @var string
	 */
	public $type = 'select';
	
	/**
	 * Item list where each item is separated by #
	 * Example : item1#item2#item3
	 * @var string
	 */
	public $items = '';
	
	/**
	 * @var boolean
	 */
	public $multiple = false;
	
	/**
	 * @var string [value,name]
	 */
	public $return = 'value';
}
