<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended\Property;

class Text extends \Application\Model\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 201;
	
	/**
	 * @var string
	 */
	public $type = 'text';
}
