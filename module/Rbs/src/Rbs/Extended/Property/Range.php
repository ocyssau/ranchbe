<?php
//%LICENCE_HEADER%

namespace Application\Model\Extended\Property;

class Range extends \Application\Model\Extended\Property
{
	/**
	 * @var integer
	 */
	static $classId = 203;
	
	/**
	 * @var \string
	 */
	public $type = 'range';
	
	/**
	 * @var \decimal
	 */
	public $min=null;
	
	/**
	 * @var \decimal
	 */
	public $max=null;
	
	/**
	 * @var \decimal
	 */
	public $step=null;
}
