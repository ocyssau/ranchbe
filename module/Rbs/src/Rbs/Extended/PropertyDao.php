<?php
//%LICENCE_HEADER%

namespace Rbs\Extended;

use Rbplm\Dao\MappedInterface;

use Rbplm\Dao\Connexion;
use Rbplm\Sys\Exception As Exception;
use Rbplm\Sys\Error As Error;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE extendedmodel;
 <<*/

/**
 * @brief Entity of a property definition.
 */
abstract class PropertyDao extends \Rbplm\Dao\Mysql
{

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = '';
	public static $sequenceName='';
	public static $sequenceKey='id';

	/**
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 * @var string
	 */
	protected $extendedTable;

	/**
	 *
	 * @var \PDOStatement
	 */
	protected $insertStmt;
	protected $updateStmt;
	protected $seqStmt;
	protected $deleteStmt;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id' =>'id',
		'field_name' =>'name',
		'appname' =>'appName',
		'extendedCid'=>'extendedCid',
		'field_description'=>'description',
		'field_type' =>'type',
		'field_regex' =>'regex',
		'field_required' =>'required',
		'field_multiple' =>'multiple',
		'field_size' =>'size',
		'table_name' =>'dbtable',
		'field_for_display' =>'forDisplay',
		'field_for_value' =>'forValue',
		'return_name' =>'return',
		'field_list' =>'list',
		'is_hide'=>'hide',
		'date_format'=>'dateFormat',
		'getter' =>'getter',
		'setter' =>'setter',
		'label' =>'label',
		'min' =>'min',
		'max' =>'max',
		'step' =>'step',
		'dbfilter' =>'dbfilter',
		'dbquery' =>'dbquery',
		'attributes'=>'attributes'
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn=null)
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->_table = static::$table;

		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;
	} //End of function

	/**
	 *
	 * @param string $table
	 */
	public function setExtendedTable($table)
	{
		$this->extendedTable = $table;
	}

	/**
	 *
	 */
	public function bind($mapped)
	{
		$properties = $mapped->getArrayCopy();
		$bind = array();
		foreach($this->metaModel as $key=>$name){
			$val = $properties[$name];
			if(is_array($val)){
				$val = json_encode($val, true);
			}
			$bind[':'.$name] = $val;
		}
		return $bind;
	}

	/**
	 *
	 */
	public function bindType($appType)
	{
		switch ( $appType )
		{
			case 'text':
				$this->sysType = 'VARCHAR(255)';
				$this->size = 255;
				$this->validDatas = array('size','regex','required', 'label');
				break;
			case 'longtext':
				$this->sysType = 'text';
				$this->size = 255;
				$this->validDatas = array('size','regex','required', 'label');
				break;
			case 'htmlarea':
				$this->sysType = 'text';
				$this->field_size = 255;
				$this->validDatas = array('size','required', 'label');
				break;
			case 'partner':
				$this->sysType = 'VARCHAR(128)';
				$this->field_size = 128;
				$this->validDatas = array('size','required','multiple','return','dbfilter','label');
				break;
			case 'doctype':
				$this->sysType = 'VARCHAR(128)';
				$this->field_size = 128;
				$this->validDatas = array('size','required','multiple','return','dbfilter', 'label');
				break;
			case 'version':
				$this->sysType = 'VARCHAR(16)';
				$this->size = 16;
				$this->validDatas = array('size','required','multiple','return','dbfilter', 'label');
				break;
			case 'user':
				$this->sysType = 'VARCHAR(128)';
				$this->size = 128;
				$this->validDatas = array('size','required','multiple','return','dbfilter', 'label');
				break;
			case 'process':
				$this->sysType = 'VARCHAR(128)';
				$this->size = 128;
				$this->validDatas = array('size','required','multiple','return','dbfilter', 'label');
				break;
			case 'category':
				$this->sysType = 'VARCHAR(128)';
				$this->size = 128;
				$this->validDatas = array('size','required','multiple','return','dbfilter', 'label');
				break;
			case 'date':
				$this->sysType = 'DATETIME';
				$this->validDatas = array('required', 'label');
				break;
			case 'integer':
				$this->sysType = 'int';
				$this->validDatas = array('size','required', 'label', 'min','max','step');
				break;
			case 'number':
				$this->type = 'decimal';
				$this->validDatas = array('size','required', 'label', 'min','max','step');
				break;
			case 'select':
				$this->sysType = 'text';
				$this->size = 255;
				$this->validDatas = array('size','required','multiple','return','list', 'label');
				break;
			case 'selectFromDB':
				$this->sysType = 'text';
				$this->size = 255;
				$this->validDatas = array('size','required','multiple','return','dbtable','forValue','forDisplay', 'label');
				break;
			case 'liveSearch':
				$this->sysType = 'text';
				$this->size = 255;
				$this->validDatas = array('size','required','multiple','return','dbtable','forValue','forDisplay', 'label');
				break;
			default:
				$this->sysType = 'text';
				$this->size = 255;
				$this->validDatas = array('size','regex','required', 'label');
				break;
		} //End of switch
		return $this->sysType;
	}

	/**
	 * @todo Escape sql injection on select
	 * $mapped \Application\Model\Extended\Property
	 */
	public function insert($mapped)
	{
		$table = $this->_table;
		$this->connexion->beginTransaction();

		if($this->_sequenceName){
			$sql = 'UPDATE ' . $this->_sequenceName .' SET '.$this->_sequenceKey.'=LAST_INSERT_ID('.$this->_sequenceKey.' + 1) LIMIT 1;';
			$this->seqStmt = $this->connexion->prepare($sql);
		}

		if(!$this->insertStmt){
			foreach($this->metaModel as $key=>$name){
				$set[] = $key;
				$values[] = ':'.$name;
			}

			$sql = 'INSERT INTO '.$table.' ('.implode(',', $set).')';
			$sql .= ' VALUES('.implode(',', $values).')';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		//insert property request
		$alterClassId = $mapped->getExtended(true);
		$alterTable = $this->extendedTable;

		if($alterTable==null){
			throw new Exception('$this->extendedTable is not set. Set it with setExtendedTable method');
		}

		$column = $mapped->getName();
		$type = $mapped->getType();
		if($type==null){
			throw new Exception('$type is not set');
		}
		$sysType = $this->bindType($type);
		$alterSql = "ALTER TABLE $alterTable ADD COLUMN `$column` $sysType DEFAULT NULL";

		try{
			if($this->_sequenceName){
				$this->seqStmt->execute();
				$id = $this->connexion->lastInsertId($this->_sequenceName);
				$mapped->setId($id);
			}

			$this->insertStmt->execute($this->bind($mapped));
			$this->connexion->query($alterSql);
			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}

		return $this;
	} //End of function

	/**
	 */
	public function update($mapped, $select = null )
	{
		return $this->_update($mapped, $select);
	} //End of function

	/**
	 *
	 */
	public function delete($uid, $withChildren = false, $withTrans=null)
	{
		throw new \Exception('Not implemented');
	}

	/**
	 *
	 */
	protected function _insert($mapped, $select=null)
	{
		throw new \Exception('Not implemented');
	}

	/**
	 * @param Rbh\AnyObject   $mapped
	 * @param array				  $bind		Bind definition to add to generic bind construct from $sysToApp
	 * @param array				  $select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 *
	 */
	protected function _update($mapped, $select=null)
	{
		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;
		$bind = array();
		$table = $this->_table;

		if(!$this->updateStmt){
			if(is_array($select)){
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach($sysToApp as $asSys=>$asApp){
				$set[] = $asSys . '=:' . $asApp;
			}
			$sql = "UPDATE $table SET " . implode(',', $set) . ' WHERE '.$this->toSys('id').'=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}

		$bind = $this->bind($mapped);

		if(is_array($select)){
			array_walk($select, function(&$val,$key){
				$val=':'.$val;
			});
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->getId();
		}

		try{
			$this->connexion->beginTransaction();
			$this->updateStmt->execute($bind);
			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
	}

	/**
	 *
	 */
	public function deleteFromFilter($filter, $bind)
	{
		$table = $this->_table;

		try{
			$this->connexion->beginTransaction();

			//get extended table
			$sql = "SELECT extendedCid,field_name FROM $table AS obj WHERE $filter";
			$stmt = $this->connexion->prepare($sql);
			$stmt->execute($bind);
			$row = $stmt->fetch(\PDO::FETCH_ASSOC);
			$alterClassId = $row['extendedCid'];
			$column = $row['field_name'];
			$alterTable = $this->factory->getTable($alterClassId);

			//delete property
			$sql = "DELETE FROM $table WHERE $filter";
			$suppressStmt = $this->connexion->prepare($sql);
			$suppressStmt->execute($bind);

			//delete column
			$alterSql = "ALTER TABLE $alterTable DROP `$column`";
			$this->connexion->query($alterSql);

			$this->connexion->commit();
		}
		catch(\Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}

		return $this;
	} //End of function

	/**
	 * @return Dao
	 */
	public function deleteFromId($id)
	{
		$filter = "id=:id";
		$bind = array( ':id'=>$id );
		$this->deleteFromFilter($filter, $bind);
		return $this;
	} //End of method
}
