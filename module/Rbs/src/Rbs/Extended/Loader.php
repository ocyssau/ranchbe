<?php

namespace Rbs\Extended;

/**
 * @brief Load the extended properties and configure the Dao.
 *
 * Complete Dao metamodel
 * Set the plugin to Dao
 *
 * @author olivier
 *
 */
class Loader
{
	public $loadStmt=null;
	public $table = 'workitem_metadata';
	public $connexion;

	/**
	 *
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
		$this->connexion = $factory->getConnexion();
		$spaceName = strtolower($factory->getName());
		$this->table = $spaceName.'_metadata';
		$this->spaceName = $spaceName;
	}

	/**
	 * @throws Exception
	 */
	public function load($cid, $dao)
	{
		if(!$this->loadStmt){
			$connexion = $this->connexion;

			$table = $this->table;

			$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
			foreach($allSelect as $asSys=>$asApp){
				$select[]="`$asSys` AS `$asApp`";
			}
			$select = implode(',', $select);
			$sql = "SELECT $select FROM $table as prop WHERE extendedCid=:cid";
			$stmt = $connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadStmt = $stmt;
		}

		$this->loadStmt->execute(array(':cid'=>$cid));
		$fetch = $this->loadStmt->fetchAll();

		if($fetch){
			$this->extendDao($fetch, $dao);
		}

		return $fetch;
	}

	/**
	 * @param integer $containerId
	 * @param array $select 	Array of asApp property name to put in select
	 * @return array 	The extended property entity in App semantic
	 */
	public function loadFromContainerId($containerId, $dao, $select=null)
	{
		$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
		if($select){
			$allSelect = array_intersect($allSelect, $select);
		}

		$select = array();
		foreach($allSelect as $asSys=>$asApp){
			$select[]="`$asSys` AS `$asApp`";
		}

		$lnkDao = $this->factory->getDao(\Rbs\Org\Container\Link\Property::$classId);
		$extended = $lnkDao->getChildren($containerId, $select)->fetchAll();
		$this->extendDao($extended, $dao);
	}

	/**
	 * @throws Exception
	 */
	public function loadFromDocumentId($documentId, $dao, $select=null)
	{
		$allSelect = \Rbs\Extended\PropertyDao::$sysToApp;
		if($select){
			$allSelect = array_intersect($allSelect, $select);
		}

		$select = array();
		foreach($allSelect as $asSys=>$asApp){
			$select[]="`$asSys` AS `$asApp`";
		}
		$select = implode(',', $select);

		if(!$this->loadFromDocumentIdStmt){
			$connexion = $dao->getConnexion();

			$spaceName = $this->spaceName;
			$table = $this->table;
			$relTable = $table.'_rel';
			$docTable = $spaceName.'_documents';
			$pkey = $spaceName.'_id';

			$sql = "
				SELECT $select
				FROM $docTable as parent
				JOIN $relTable as link ON link.$pkey=parent.$pkey
				JOIN $table as child ON link.property_id=child.id
				WHERE document_id=:documentId";
			$stmt = $connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadFromDocumentIdStmt = $stmt;
		}

		$this->loadFromDocumentIdStmt->execute(array(':documentId'=>$documentId));
		$fetch = $this->loadFromDocumentIdStmt->fetchAll();

		if($fetch){
			$this->extendDao($fetch, $dao);
		}

		return $fetch;
	}

	/**
	 * @param array $extended 	In App sementic
	 * @param \Rbs\Dao\Sier $dao
	 * @throws Exception
	 */
	public function extendDao(array $extended, $dao)
	{
	    $metaModel = array();
		foreach($extended as $i){
			$extendedModel[$i['name']] = $i['appName'];
			$metaModel[$i['name']] = $i['appName'];
		}
		$dao->metaModel = array_merge($dao::$sysToApp, $metaModel);
		$dao->extendedModel = $extendedModel;
		$dao->extended = $extended;
	}
}
