<?php
//%LICENCE_HEADER%

namespace Rbs\Dao;

use Rbplm\Dao\FactoryAbstract;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory extends FactoryAbstract
{
	const GENERIC = 200;
}
