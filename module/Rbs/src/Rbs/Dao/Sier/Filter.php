<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use Rbplm\Dao\FilterAbstract;

/**
 * Create params to search in database.
 *
 */
class Filter extends FilterAbstract
{
	/**
	 * @return array
	 */
	public function getSelect()
	{
		return $this->select;
	}

}//End of class
