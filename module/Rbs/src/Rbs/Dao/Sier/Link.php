<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use \Rbplm\Dao\Connexion;
use \Rbplm\Sys\Exception As Exception;
use \Rbplm\Sys\Error As Error;
use Rbs\Dao\Sier\Filter;
use Rbs\Dao\Sier\MetamodelTranslator;

class Link
{
	/**
	 * @var \PDO
	 */
	protected $connexion;

	public $_table;
	public $_vtable;

	/**
	 *
	 * @var MetamodelTranslator
	 */
	protected $translator;

	/**
	 * @var array
	 */
	protected $metaModel;

	/**
	 * @var array
	 */
	protected $metaModelFilters;

	/**
	 * @var unknown_type
	 */
	protected $insertStmt;
	protected $updateStmt;
	protected $deleteStmt;

	/**
	 * @var array
	 */
	protected $options = array('withtrans'=>true);

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = '';
	public static $childTable = null;
	public static $childForeignKey = '';
	public static $parentTable = null;
	public static $parentForeignKey = '';
	public static $sequenceName = '';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var unknown_type
	 */
	static $sysToApp = array(
		'uid'=>'uid',
		'parentId'=>'parentId',
		'childId'=>'childId',
		'parentUid'=>'parentUid',
		'childUid'=>'childUid',
		'name'=>'name',
		'index'=>'index',
		'attributes'=>'attributes',
	);

	/**
	 *
	 * @var unknown_type
	 */
	static $sysToAppFilter = array(
		'attributes'=>'json',
	);

	/**
	 *
	 * @param \PDO $conn
	 */
	public function __construct( $conn = null )
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;
		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;
	}

	/**
	 * @return MetamodelTranslator
	 */
	public function getTranslator()
	{
		if(!isset($this->translator)){
			$this->translator = new MetamodelTranslator($this->metaModel, $this->metaModelFilters);
		}
		return $this->translator;
	}

	/**
	 * @param unknown_type $mapped
	 * @return multitype:NULL unknown
	 */
	public function bind($mapped)
	{
		$bind = array();
		$properties = $mapped->getArrayCopy();

		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;

		/* @var Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();

		foreach($sysToApp as $sysName=>$appName)
		{
			if(isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties)){
				$value = $properties[$appName];
				$filterMethod = $sysToAppFilter[$sysName].'ToSys';
				$bind[':'.$appName] = $translator->$filterMethod($value);
			}
			else{
				$bind[':'.$appName] = $properties[$appName];
			}
		}
		return $bind;
	}

	/**
	 * @return \PDO
	 */
	public function getConnexion()
	{
		if( !$this->connexion ){
			throw new Exception('CONNEXION_IS_NOT_SET', Error::ERROR);
		}
		return $this->connexion;
	}

	/**
	 * @todo Escape sql injection on select
	 * $mapped \Rbh\Link
	 */
	public function insert($mapped, array $select = null)
	{
		$table = static::$table;
		$sysToApp = $this->metaModel;

		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		/* Init statement */
		if(!$this->insertStmt){
			foreach($sysToApp as $asSys=>$asApp)
			{
				$sysSet[] = '`'.$asSys.'`';
				$appSet[] = ':'.$asApp;
			}

			if($this->_sequenceName){
				$sql = 'UPDATE ' . $this->_sequenceName .' SET '.$this->_sequenceKey.'=LAST_INSERT_ID('.$this->_sequenceKey.' + 1) LIMIT 1;';
				$this->seqStmt = $this->connexion->prepare($sql);
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}
		try{
			if($withTrans) $this->connexion->beginTransaction();
			if($this->_sequenceName){
				$this->seqStmt->execute();
				$id = $this->connexion->lastInsertId($this->_sequenceName);
				$mapped->setId($id);
				$bind = $this->bind($mapped);
				$this->insertStmt->execute($bind);
			}
			else{
				$bind = $this->bind($mapped);

				$this->insertStmt->execute($bind);
				$id = $this->connexion->lastInsertId($table);
				$mapped->setId($id);
			}
			if($withTrans) $this->connexion->commit();
		}
		catch(Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * @todo Escape sql injection on select
	 *
	 * @param Any $mapped
	 * @param array $select
	 * @throws Exception
	 * @return Link
	 */
	public function update($mapped, $select=null)
	{
		$table = static::$table;
		$sysToApp = $this->metaModel;

		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		/* Init statement */
		if(!$this->updateStmt AND $this->updateSelect=$select){
			if(is_array($select)){
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach($sysToApp as $asSys=>$asApp){
				$set[] = $asSys . '=:' . $asApp;
			}
			$set = implode(',', $set);
			$idKey = array_search('id', $sysToApp);
			$sql = "UPDATE $table SET $set WHERE $idKey=:id;";
			$this->updateStmt = $this->connexion->prepare($sql);
			$this->updateSelect = $select;
		}

		$bind = $this->_bind($mapped);

		if(is_array($select)){
			foreach($select as $key=>&$val){
				$val=':'.$val;
			}
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->getId();
		}

		try{
			if($withTrans) $this->connexion->beginTransaction();
			$this->updateStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * @todo Escape sql injection
	 */
	public function delete($filter, $bind=null)
	{
		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		if(!$this->deleteStmt || $filter<>$this->deleteFilter){
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE $filter";
			$this->deleteStmt = $this->connexion->prepare($sql);
			$this->deleteFilter = $filter;
		}

		try{
			if($withTrans) $this->connexion->beginTransaction();
			$this->deleteStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * @todo Escape sql injection
	 */
	public function deleteFromId($id)
	{
		$toSysId = array_search('id', $this->metaModel);
		$filter = "$toSysId=:id";
		$bind = array( ':id'=>$id );
		$this->delete($filter, $bind);
		return $this;
	}

	/**
	 * Suppress all links without child or parent
	 */
	public function cleanOrpheanLinks()
	{
		throw new Exception('Not implemented method');

		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		$table = static::$table;
		$sql = "DELETE FROM $table as l WHERE l.childId NOT IN(SELECT id FROM anyobject) OR l.parentId NOT IN (SELECT id FROM anyobject)";
		$stmt = $this->connexion->prepare($sql);

		try{
			if($withTrans) $this->connexion->beginTransaction();
			$stmt->execute();
			if($withTrans) $this->connexion->commit();
		}
		catch(Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the children of the parentId.
	 * If select is set return only the result of the select.
	 * Note that at the first excution, the statment is build, so at second excution the select is ignored.
	 * To recreate the statment you must explicitly reset the public property $loadparentStmt as :
	 * @code $link->loadparentStmt = null;
	 *
	 * Tables are Aliased with child. and link. prefix, so you can refer to this alias in your select and filters
	 *
	 * @param integer $parentId
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getChildren($parentId, $select=null, $filter=null)
	{
		if(!$this->getChildrenStmt){
			if(!$select){
				$select = array('child.*');
			}

			$table = static::$table;

			$leftTable = static::$parentTable;
			$leftKey = static::$parentForeignKey;

			$rightTable = static::$childTable;
			$rightKey = static::$childForeignKey;

			$sysToApp = $this->metaModel;
			$parentIdKey = array_search('parentId', $sysToApp);
			$childIdKey = array_search('childId', $sysToApp);

			/* ADD LINK PROPERTIES TO SELECT */
			foreach($sysToApp as $asSys=>$asApp)
			{
				$select[] = "link.$asSys AS link_$asApp";
			}
			$select = implode($select, ', ');

			$sql = "SELECT $select FROM $table AS link";

			if($rightTable && $rightKey){
				$sql.=" JOIN $rightTable AS child ON link.$childIdKey=child.$rightKey";
			}
			$sql.=" WHERE link.$parentIdKey=:parentId";
			if($filter){
				$sql.= ' AND ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getChildrenStmt = $stmt;
		}

		$this->getChildrenStmt->execute(array(':parentId'=>$parentId));
		return $this->getChildrenStmt;
	}

	/**
	 * Return the PDOStatement populate with the full defintion of the parent of the childId.
	 * If select is set return only the result of the select.
	 * Note that at the first excution, the statment is build, so at second excution the select is ignored.
	 * To recreate the statment you must explicitly reset the public property $loadparentStmt as :
	 * @code $link->loadparentStmt = null;
	 *
	 * @param integer $childId
	 * @param array $select
	 * @param string $filter
	 * @return \PDOStatement
	 */
	public function getParent($childId, $select=null, $filter=null)
	{
		if(!$this->getParentStmt){
			if(!$select){
				$select = array('parent.*');
			}

			$table = static::$table;
			$leftKey = 'parentId';
			$joinTable = static::$parentTable;
			$rightKey = static::$parentForeignKey;
			$sysToApp = $this->metaModel;
			$childIdKey = array_search('childId', $sysToApp);

			$select = array();
			foreach($sysToApp as $asSys=>$asApp)
			{
				$select[] = "link.$asSys AS link_$asApp";
			}
			$select = implode($select, ', ');

			$sql="SELECT $select FROM $table AS link";

			if($joinTable && $rightKey){
				$sql.=" JOIN $joinTable AS parent ON link.$leftKey=parent.$rightKey";
			}
			$sql.=" WHERE link.$childIdKey=:childId";
			if($filter){
				$sql.= ' AND ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->getParentStmt = $stmt;
		}

		$this->getParentStmt->execute(array(':childId'=>$childId));
		return $this->getParentStmt;
	}

} //End of class
