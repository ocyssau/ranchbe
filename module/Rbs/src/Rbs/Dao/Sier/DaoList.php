<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use Rbplm\Dao\DaoListAbstract;

use Rbplm\Dao\Registry;

use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;


/** SQL_VIEW>>
 <<*/

/**
 * A list of records in db.
 * This class implements a iterator.
 *
 */
class DaoList extends DaoListAbstract
{
	/**
	 *
	 * @var Rbplm\Dao\DaoInterface
	 */
	public $dao;

	/**
	 * @see Rbplm\Dao.ListInterface::toApp()
	 *
	 * Translate current row from system notation to application notation.
	 *
	 * Class variable $this->dao may be set the dao object.
	 * If $dao is a object, assume that is a type with a method toApp().
	 * If $dao is null, try to use the cid property from record to get the dao
	 * In this last case, require an attribut cid in current statement.
	 *
	 * @param $dao	[OPTIONAL]	Set a Dao class or any other class with a method toApp(), to use for convert attributs.
	 * @return array
	 */
	public function toApp( $daoClassOrObjectOrCid=null )
	{
		$current = $this->_current;
		
		if( is_string($daoClassOrObjectOrCid) || is_object($daoClassOrObjectOrCid) ){
			$dao = $daoClassOrObjectOrCid;
		}
		elseif( is_integer($daoClassOrObjectOrCid) ){
			$classId = $daoClassOrObjectOrCid;
			$dao = $this->factory->getDaoClass($classId);
		}
		elseif( isset($this->options['dao']) ){
			$dao = $this->options['dao'];
		}
		elseif( isset($current['cid']) ){
			$classId = $current['cid'];
			$dao = $this->factory->getDaoClass($classId);
		}
		else{
			throw new Exception('cid_IS_NOT_SET', Error::WARNING, $current);
		}

		return call_user_func(array($dao, 'toApp'), $current);
	}


	/**
	 * Translate current row to Rbplm object.
	 * If $useRegistry = true, get object from registry if exist and add it if not.
	 *
	 * @param bool $useRegistry
	 * @return AnyObject
	 *
	 */
	public function toObject($useRegistry = true)
	{
		$current = $this->current();

		if($useRegistry){
			$mapped = Registry::singleton()->get($current['uid']);
		}

		if( !$mapped ){
			if( isset($current['cid']) ){
				$cid = $current['cid'];
				$dao = $this->factory->getDao($cid);
				$model = $this->factory->getModel($cid);
				$dao->loadFromArray($model, $current, false);
				if($useRegistry){
					Registry::singleton()->add($model);
				}
			}
			else{
				throw new Exception('cid_IS_NOT_REACHABLE', Error::ERROR, $current);
			}
		}

		return $model;
	}

} //End of class
