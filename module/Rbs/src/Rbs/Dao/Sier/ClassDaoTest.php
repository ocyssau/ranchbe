<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use Rbs\Dao\Sier\ClassDao;
use Rbplm\Dao\Connexion;
use Rbplm\Dao\Registry;
use Rbplm\Dao\Factory as DaoFactory;


/**
 * @brief Test class for \Rbplm\Dao\Pg\Class
 * 
 */
class ClassDaoTest extends \Rbplm\Test\Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }
	
	/**
	 * 
	 */
	function Test_Dao()
	{
		$Conn = Connexion::get( Connexion::getDefault() );
		$Class = ClassDao::singleton()->setConnexion($Conn);
		
		var_dump($Class->toName(10));
		var_dump($Class->toId('\Rbplm\AnyObject'));
		var_dump($Class->getTable('\Rbplm\AnyObject'));
		var_dump($Class->getTable(10));
		
		assert($Class->toName(10) == '\Rbplm\AnyObject');
		assert($Class->toId('\Rbplm\AnyObject') == 10);
		assert($Class->getTable('\Rbplm\AnyObject') == 'anyobject');
		assert($Class->getTable(10) == 'anyobject');
	}
}


