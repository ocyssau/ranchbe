<?php
//%LICENCE_HEADER%

namespace Rbs\Dao\Sier;

use Rbplm\Dao\Registry;
use Rbplm\Dao\Factory as DaoFactory;

    /**
     * Assume that tests datas are loaded in db with this structure:
     * 
     *		INSERT INTO test VALUES ('Top');
     *		INSERT INTO test VALUES ('Top.Science');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy.Astrophysics');
     *		INSERT INTO test VALUES ('Top.Science.Astronomy.Cosmology');
     *		INSERT INTO test VALUES ('Top.Hobbies');
     *		INSERT INTO test VALUES ('Top.Hobbies.Amateurs_Astronomy');
     *		INSERT INTO test VALUES ('Top.Collections');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Stars');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Galaxies');
     *		INSERT INTO test VALUES ('Top.Collections.Pictures.Astronomy.Astronauts');
     * 
     */


/**
 * @brief Test class for \Rbplm\Dao\Pg\Filter
 * 
 */
class FilterTest extends \Rbplm\Test\Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	Registry::singleton()->reset();
    }
	
	/**
	 * 
	 */
	function Test_Literal()
	{
		$Filter = DaoFactory::getFilter('\Rbplm\Org\Unit');
		
		//many ancestors condition
		$Filter->children('RanchbePlm/Top/Science', array('level'=>1, 'depth'=>0, 'boolOp'=>'OR'));
		$Filter->children('RanchbePlm/Top/Collections', array('level'=>1, 'depth'=>0, 'boolOp'=>'OR'));
		echo $Filter . CRLF;
		
		//with sub filter
		$SubFilter = DaoFactory::getFilter('\Rbplm\Org\Unit');
		$SubFilter->andfind('qqueschose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$SubFilter->orfind('autrechose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$SubFilter->orfind('encoreautrechose', 'name', \Rbplm\Dao\Filter\Op::OP_CONTAINS);
		$Filter->subor($SubFilter);
		echo $Filter . CRLF;;
	}
}


