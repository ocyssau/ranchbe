<?php
//%LICENCE_HEADER%

namespace Rbs\Dao;

use Rbplm\Dao\Mysql;
use Rbplm\Dao\Connexion;
use Rbplm\Sys\Exception As Exception;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Error As Error;
use Rbplm\Signal As Signal;
use Rbs\Dao\Sier\Filter;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Sier extends Mysql
{

	/**
	 *
	 * @var array
	 */
	public $extendedModel = array();

	/**
	 *
	 * @var string
	 */
	protected $_lvtable;

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->metaModelFilters = static::$sysToAppFilter;

		$this->_sequenceName = static::$sequenceName;
		$this->_sequenceKey = static::$sequenceKey;

		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
		$this->_ltable = static::$ltable;
		$this->_lvtable = static::$lvtable;
	}

	/**
	 * @param Rbh\AnyObject   $mapped
	 * @param array				  $bind		Bind definition to add to generic bind construct from $sysToApp
	 * @param array				  $select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 *
	 */
	protected function _update($mapped, $select=null)
	{
		$sysToApp = $this->metaModel;
		$bind = array();
		$table = $this->_table;

		if(!$this->updateStmt || $this->updateSelect<>$select){
			if(is_array($select)){
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach($sysToApp as $asSys=>$asApp){
				$set[] = '`' . $asSys . '`' . '=:' . $asApp;
			}
			$sql = "UPDATE $table SET " . implode(',', $set) . ' WHERE '.$this->toSys('id').'=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);
			$this->updateSelect = $select;
		}

		$bind = $this->_bind($mapped);

		if(is_array($select)){
			array_walk($select, function(&$val,$key){
				$val=':'.$val;
			});
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->getId();
		}
		$this->updateStmt->execute($bind);
	}

	/**
	 * @param \Rbh\AnyObject   	$mapped
	 * @param array				$bind		Bind definition to add to generic bind construct from $sysToApp
	 * @param array				$select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 *
	 */
	protected function _insert($mapped, $select=null)
	{
		$sysToApp = $this->metaModel;
		$table = $this->_table;

		//Init statement
		if(!$this->insertStmt){
			foreach($sysToApp as $asSys=>$asApp)
			{
				$sysSet[] = '`'.$asSys.'`';
				$appSet[] = ':'.$asApp;
			}

			if($this->_sequenceName){
				$sql = 'UPDATE ' . $this->_sequenceName .' SET '.$this->_sequenceKey.'=LAST_INSERT_ID('.$this->_sequenceKey.' + 1) LIMIT 1;';
				$this->seqStmt = $this->connexion->prepare($sql);
			}

			$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		if($this->_sequenceName){
			$this->seqStmt->execute();
			$id = $this->connexion->lastInsertId($this->_sequenceName);
			$mapped->setId($id);
			$bind = $this->_bind($mapped);
			$this->insertStmt->execute($bind);
		}
		else{
			$bind = $this->_bind($mapped);
			$this->insertStmt->execute($bind);
			$id = $this->connexion->lastInsertId($table);
			$mapped->setId($id);
		}
	}

	/**
	 *
	 * @return array
	 */
	protected function _bind($mapped)
	{
		$bind = array();
		$properties = $mapped->getArrayCopy();

		$sysToApp = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;

		/* @var Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();

		/*
		foreach($sysToApp as $sysName=>$appName)
		{
			if(isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties)){
				$filterMethod = $sysToAppFilter[$sysName].'ToSys';
				$bind[':'.$appName] = static::$filterMethod($properties[$appName]);
			}
			else{
				$bind[':'.$appName] = $properties[$appName];
			}
		}
		*/

		foreach($sysToApp as $sysName=>$appName)
		{
			if(isset($sysToAppFilter[$sysName]) && array_key_exists($appName, $properties)){
				$value = $properties[$appName];
				$filterMethod = $sysToAppFilter[$sysName].'ToSys';
				$bind[':'.$appName] = $translator->$filterMethod($value);
			}
			else{
				$bind[':'.$appName] = $properties[$appName];
			}
		}

		return $bind;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.DaoInterface::hydrate()
	 *
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param AnyObject $mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean $fromApp		If true, assume that keys $row are name of properties as set in  model, else are set as in persitence system.
	 * @return AnyObject
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		$properties = array();

		/* @var Rbs\Dao\Sier\MetamodelTranslator */
		$translator = $this->getTranslator();

		if($fromApp){
			foreach($this->metaModel as $asSys=>$asApp){
				$properties[$asApp] = $row[$asApp];
			}
		}
		else{
			$sysToAppFilter = $this->metaModelFilters;
			foreach($this->metaModel as $asSys=>$asApp){
				if(isset($sysToAppFilter[$asSys])){
					$value = $row[$asSys];
					$filterMethod = $sysToAppFilter[$asSys].'ToApp';
					$properties[$asApp] = $translator->$filterMethod($value);
				}
				else{
					$properties[$asApp] = $row[$asSys];
				}
			}
		}
		$mapped->hydrate($properties);

		/* Load the extended model */
		if($this->extendedModel){
			if($fromApp){
				foreach($this->extendedModel as $asSys=>$asApp){
					$mapped->$asApp = $row[$asApp];
				}
			}
			else{
				foreach($this->extendedModel as $asSys=>$asApp){
					$mapped->$asApp = $row[$asSys];
				}
			}
		}

		return $mapped;
	}

	/**
	 *
	 * @param string $filter
	 * @param array $bind
	 * @param boolean $withChildren
	 * @param boolean $withTrans
	 * @throws Exception
	 * @return \Application\Dao\Dao
	 */
	protected function _deleteFromFilter($filter, $bind, $withChildren=true, $withTrans=null)
	{
		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->deleteStmt || $this->deleteFilter <> $filter){
			$table = $this->_table;

			$sql = 'DELETE FROM '.$table.' WHERE ' . $filter;
			$this->deleteStmt = $this->connexion->prepare($sql);

			$this->deleteFilter == $filter;
		}

		try{
			Signal::trigger(self::SIGNAL_PRE_DELETE, $this);
			$this->deleteStmt->execute($bind);
			Signal::trigger(self::SIGNAL_POST_DELETE, $this);

			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}


	/**
	 * Alias for hydrate.
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		return $this->hydrate($mapped,$row,$fromApp);
	}

	/**
	 * @return stmt
	 */
	public function query( $select, $filter=null, $bind=null, $fromApp = false )
	{
		$table = $this->_table;

		$select = implode($select, ',');
		$sql = "SELECT $select FROM $table AS obj";

		if(is_string($filter)){
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		if(!$stmt){
			throw new NotExistingException(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql));
		}
		return $stmt;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.DaoInterface::delete()
	 *
	 * Suppress current record in database
	 *
	 * @param string	$uid
	 * @param boolean $withChildren	If true, suppress all childs and childs of childs
	 * @param boolean $withTrans	If true, open a new transaction
	 * @throws Exception
	 * @return DaoInterface
	 */
	public function delete($uid, $withChildren = false, $withTrans=null)
	{
		$toSysId = $this->toSys('uid');
		$filter = "$toSysId=:uid";
		$bind = array( ':uid'=>$uid );
		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 * @return Dao
	 */
	public function deleteFromId($id, $withChildren=true, $withTrans=null)
	{
		$toSysId = $this->toSys('id');
		$filter = "$toSysId=:id";
		$bind = array(':id'=>$id);

		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}

	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function dateToApp($timestamp)
	{
		if($timestamp == 0){
			return new \Rbplm\Sys\Date();
		}
		else{
			return new \Rbplm\Sys\Date((int)$timestamp);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function dateToSys($in)
	{
		if($in instanceof \DateTime){
			return $in->getTimestamp();
		}
		elseif($in == 0){
			$date = new \DateTime();
			return $date->getTimestamp();
		}
		elseif(is_string($in)){
			$date = new \DateTime($in);
			return $date->getTimestamp();
		}
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToApp($json)
	{
		if($json[0]=='{' || $json[0]=='['){
			return json_decode($json, true);
		}
		else{
			return array($json);
		}
	}

	/**
	 * @param string $in
	 * @return array
	 */
	public static function jsonToSys($array)
	{
		return json_encode($array, true);
	}

	/**
	 *
	 * @param unknown_type $yesOrNo
	 */
	protected static function yesOrNoToApp($yesOrNo)
	{
		return ($yesOrNo == 'y') ? true : false;
	}

	/**
	 * @param unknown_type $yesOrNo
	 */
	protected static function yesOrNoToSys($yesOrNo)
	{
		return ($yesOrNo == true) ? 'y' : 'n';
	}
} //End of class
