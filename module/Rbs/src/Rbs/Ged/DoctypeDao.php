<?php
//%LICENCE_HEADER%

namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Dao\Exception;
use Rbplm\Sys\Error;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `doctypes` (
 `doctype_id` int(11) NOT NULL DEFAULT '0',
 `doctype_number` varchar(64) NOT NULL DEFAULT '',
 `doctype_descriptions` varchar(256) DEFAULT NULL,
 `can_be_composite` tinyint(4) NOT NULL DEFAULT '1',
 `file_extension` varchar(256) DEFAULT NULL,
 `file_type` varchar(128) DEFAULT NULL,
 `icon` varchar(32) DEFAULT NULL,
 `script_post_store` varchar(64) DEFAULT NULL,
 `script_pre_store` varchar(64) DEFAULT NULL,
 `script_post_update` varchar(64) DEFAULT NULL,
 `script_pre_update` varchar(64) DEFAULT NULL,
 `recognition_regexp` text COLLATE latin1_general_ci,
 `visu_file_extension` varchar(32) DEFAULT NULL,
 PRIMARY KEY (`doctype_id`),
 UNIQUE KEY `UC_doctype_number` (`doctype_number`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class DoctypeDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='doctypes';

	/**
	 *
	 * @var string
	 */
	public static $vtable='doctypes';

	/**
	 * @var string
	 */
	public static $sequenceName='doctypes_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'doctype_id'=>'id',
		'doctype_number'=>'name',
		'doctype_description'=>'description',
		'can_be_composite'=>'mayBeComposite',
		'file_extensions'=>'fileExtensions',
		'file_type'=>'fileType',
		'icon'=>'icon',
		'script_post_store'=>'scriptPostStore',
		'script_pre_store'=>'scriptPreStore',
		'script_post_update'=>'scriptPostUpdate',
		'script_pre_update'=>'scriptPreUpdate',
		'recognition_regexp'=>'recognitionRegexp',
		'visu_file_extension'=>'visuFileExtension',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'file_extensions' => 'json'
	);

	/**
	 * Getter for categories. Return a list.
	 *
	 * @param \Rbplm\Dao\MappedInterface
	 * @param boolean
	 * @return \Rbplm\Dao\Pg\DaoList
	 */
	public function getCategories($mapped, $loadInCollection = false)
	{
		$list = new \Rbplm\Dao\Pg\DaoList( array('table'=>'view_ged_category_links') );
		$list->setConnexion( $this->getConnexion() );
		$uid = $mapped->getUid();
		if($loadInCollection){
			$list->loadInCollection($mapped->getCategories(), "lparent='$uid'");
		}
		else{
			$list->load("lparent='$uid'");
		}
		return $list;
	}

	/**
	 * @param 	\Rbplm\Ged\Doctype	$mapped
	 * @param 	string 				$documentNumber
	 * @param 	string 				$fileExtension
	 * @param 	string 				$fileType
	 */
	public function loadFromDocument( $mapped, $documentNumber, $fileExtension=null, $fileType=null )
	{
		$bind = array();
		$filters = array();

		if(!is_null($fileExtension)){
			$fileExtension = '%'.$fileExtension.'%';
			$filters[] = 'file_extensions LIKE :fileExtension';
			$bind[':fileExtension'] = $fileExtension;
		}

		if(!is_null($fileType)){
			$filters[] = 'file_type LIKE :fileType';
			$bind[':fileType'] = $fileType;
		}

		$filter = implode(' AND ', $filters);
		$table = static::$table;
		$sql = "SELECT * FROM $table WHERE $filter";
		$stmt = $this->getConnexion()->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		if( $stmt->rowCount() == 0){
			throw new Exception('NONE_VALID_DOCTYPE', Error::WARNING, $documentNumber );
		}

		/* For each doctype test if current Document corresponding to regex */
		while( $row = $stmt->fetch() ){
			if( !empty($row['recognition_regexp']) ){
				if(preg_match('/'.$row['recognition_regexp'].'/', $documentNumber ) ){
					/* assign doctype and exit loop */
					$doctypeProps = $row;
					break;
				}
			}
			else{
				/* if recognition_regexp field is empty, its a generic doctype */
				$genericDoctype = $row;
			}
		}

		/* If no match doctypes, then assign the generic doctype, else return false */
		if(!$doctypeProps && $genericDoctype){
			$doctypeProps = $genericDoctype;
		}

		/**/
		if( !$doctypeProps ){
			throw new Exception('NONE_VALID_DOCTYPE', Error::WARNING, $documentNumber );
		}

		$this->loadFromArray($mapped, $doctypeProps);
	}

}
