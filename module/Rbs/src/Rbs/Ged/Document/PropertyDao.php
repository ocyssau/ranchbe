<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Document;

/** SQL_SCRIPT>>
 CREATE TABLE `workitem_doc_metadata` (
 `field_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
 `field_description` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
 `field_type` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
 `field_regex` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 `field_required` int(1) NOT NULL DEFAULT '0',
 `field_multiple` int(1) NOT NULL DEFAULT '0',
 `field_size` int(11) DEFAULT NULL,
 `return_name` int(1) NOT NULL DEFAULT '0',
 `field_list` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 `field_where` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 `is_hide` int(1) NOT NULL DEFAULT '0',
 `table_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
 `field_for_value` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
 `field_for_display` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
 `date_format` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
 `adv_select` tinyint(1) NOT NULL DEFAULT '0',
 PRIMARY KEY (`field_name`)
 );
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `workitem_metadata` ADD `appname` VARCHAR(30) AFTER `adv_select`;
 ALTER TABLE `workitem_metadata` ADD `getter` VARCHAR(30);
 ALTER TABLE `workitem_metadata` ADD `setter` VARCHAR(30);
 ALTER TABLE `workitem_metadata` ADD `label` VARCHAR(30);
 ALTER TABLE `workitem_metadata` ADD `min` decimal;
 ALTER TABLE `workitem_metadata` ADD `max` decimal;
 ALTER TABLE `workitem_metadata` ADD `step` decimal;
 ALTER TABLE `workitem_metadata` ADD `dbfilter` VARCHAR(255);
 ALTER TABLE `workitem_metadata` ADD `dbquery` VARCHAR(255);
 ALTER TABLE `workitem_metadata` ADD `attributes` text;
 ALTER TABLE `workitem_metadata` ADD `extendedCid` VARCHAR(32) AFTER field_name;

 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE extendedmodel;
 <<*/

/**
 * @brief Entity of a property definition.
 */
class PropertyDao
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'workitem_doc_metadata';
}
