<?php

namespace Rbs\Ged\Document;

/**
 * Thumbnail of the document to rapid pre-visualisation of the document.
 *
 */
class Thumbnail
{

	protected $document = false;
	protected $fileExtension = false; //output file extension .jpg|.gif|.png
	protected $errorStack = false;
	protected $thumbnailDir = false; //path to thumbnails directory
	protected $outputFile = false; //output file path

	public $xsize = 120; //width of the Thumbnail in pixel
	public $ysize = 120; //height of the Thumbnail in pixel

	/**
	 *
	 * @param Version $document
	 * @return boolean
	 */
	function __construct(Version $document)
	{
		if($document->getId() < 1){
			throw new Exception('document_id is not set', E_USER_WARNING);
		}
		$this->document = $document;
		$this->fileExtension = '.' . Ranchbe::get()->getConfig('thumbnails.extension');
		$this->thumbnailDir = Ranchbe::getConfig('thumbnails.path');
	}//End of method

	/**
	 * Get the file name and path of the attachment file
	 *
	 */
	protected function _getOutputfile()
	{
		if(!is_dir($this->thumbnailDir.'/'.$this->document->getSpaceName())){
			throw new Exception($this->thumbnailDir.' is not reachable', E_USER_WARNING);
		}
		return $this->outputFile = $this->thumbnailDir.'/'
		.$this->document->getSpaceName().'/'
		.$this->document->getId()
		.$this->fileExtension;
	}//End of method


	/**
	 * generate a Thumbnail of the document from the main picture docfile
	 *
	 */
	function generate()
	{
		$docfile = $this->document->getDocfile('mainpicture');
		if($docfile){
			$file = $docfile->getProperty('file');
		}
		else{
			return false;
		}

		if( !$outputFile=$this->_getOutputfile() ) return false;

		// Def of width and height
		$xsize = $this->xsize;
		$ysize = $this->ysize;

		$fileExtension = substr($file, strrpos($file, '.'));
		switch($fileExtension){
			case('.jpg'):
			case('.gif'):
			case('.png'):
			case('.bmp'):
				break;
			default:
				return false;
				break;
		}

		// Cacul des nouvelles dimensions
		list($xsizeOrig, $ysizeOrig) = getimagesize($file);

		$ratioOrig = $xsizeOrig/$ysizeOrig;
		if ($xsize/$ysize > $ratioOrig) {
			$xsize = $ysize*$ratioOrig;
		} else {
			$ysize = $xsize/$ratioOrig;
		}

		// Redimensionnement
		switch($fileExtension){
			case('.jpg'):
				$image = imagecreatefromjpeg($file);
				break;
			case('.gif'):
				$image = imagecreatefromgif($file);
				break;
			case('.png'):
				$image = imagecreatefrompng($file);
				break;
			case('.bmp'):
				$image = imagecreatefromwbmp($file);
				break;
			default:
				return false;
				break;
		}
		$imagep = imagecreatetruecolor($xsize, $ysize);
		imagecopyresampled($imagep, $image, 0, 0, 0, 0, $xsize, $ysize, $xsizeOrig, $ysizeOrig);
		imagetruecolortopalette($imagep , true , 16); //convert in 16 colors
		// Create file
		switch($this->fileExtension){
			case('.png'):
				imagepng($imagep, $outputFile);
				break;
			case('.gif'):
				imagegif($imagep, $outputFile);
				break;
			case('.jpg'):
				imagejpeg($imagep, $outputFile);
				break;
			default:
				return false;
				break;
		}

		return $outputFile;
	}//End of method

} //End of class

