<?php

namespace Rbs\Ged\Document;

use Rbplm\Ged\Document\Version as DocumentVersion;

/** Class to view file or attachment file
 */
class Viewer extends \Rbs\Viewer\Viewer
{
	private $document;
	private $visuFile; //Filename and path to the visu file
	private $thumbFile; //Filename and path to the thumbnail file
	private $pictureFile; //Filename and path to the picture file

	/**
	 * @param Version $document
	 */
	function __construct()
	{
	}//End of method

	/**
	 *
	 * @param string $property
	 */
	public function __get($name)
	{
		switch($name){
			case 'document':
				return $this->document;
			case 'thumbFile':
				return $this->thumbFile;
			case 'visuFile':
				return $this->_getVisuFile();
			case 'pictureFile':
				return $this->_getPictureFile();
			default:
				return parent::__get($name);
		}
	}//End of method

	/**
	 * Get the properties from fsdata object
	 */
	public function initFromDocument(DocumentVersion $document)
	{
		if($document->getId() < 1){
			throw new Exception('document is not set', E_USER_WARNING);
		}

		$this->document = $document;
		return $this;
	} //End of method

	/**
	 * Show the picture associate to current document in a html page (embeded)
	 *
	 */
	public function displayPicture()
	{
		if(!$this->_getPictureFile()){
			return false;
		}

		$encHeader = 'data:'.$this->_mimetype.';base64,';
		$imgdata = $encHeader.base64_encode(file_get_contents($this->pictureFile));
		return '<img border=0 align="center" src="'.$imgdata.'" height="300" />';
	}//End of method

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 *
	 */
	public function displayVisu()
	{
		if(!$this->_getVisuFile()){
			return false;
		}

		$this->_initDriver();
		return $this->_embeded();
	}//End of method

	/**
	 * Show the visualisation file of the current document in a html page (embeded)
	 */
	public function display()
	{
		if(!$this->_getMainFile()){
			return false;
		}

		$this->_initDriver();
		return $this->_embeded();
	}//End of method

	/**
	 * Push picture file to download it
	 *
	 */
	public function pushPicture()
	{
		if(!$this->_getPictureFile()){
			return false;
		}

		return $this->_pushfile();
	}//End of method

	/**
	 * Push visu file to download it
	 */
	public function pushVisu()
	{
		if(!$this->_getVisuFile()){
			return false;
		}

		return $this->_pushfile();
	}//End of method

	/**
	 * Get the visualisation file name and path
	 */
	public function push()
	{
		if(!$this->_getMainFile()){
			return false;
		}

		return $this->_pushfile();
	}//End of method

	/**
	 * Get the picture file name and path
	 */
	private function _getPictureFile()
	{
		if(!$this->pictureFile){
			if(!isset($this->document)) return false;
			$docfile = $this->document->getDocfile('mainpicture');
			if($docfile){
				$this->_initFsdata($docfile->getFsdata());
				$this->pictureFile = $this->_file_path;
			}
			else{
				return false;
			}
		}
		return $this->pictureFile;
	}//End of method

	/**
	 * Get the visualisation file name and path
	 */
	private function _getVisuFile()
	{
		if(!$this->visuFile){
			if(!isset($this->document)){
				return false;
			}
			$docfile = false;
			$search_order = Ranchbe::get()->getConfig('visu.roles.searchorder');
			if(!$search_order){
				$search_order = array('main');
			}
			foreach($search_order as $role){
				$docfile = $this->document->getDocfile($role);
				if($docfile) break;
			}
			if($docfile){
				$this->_initFsdata($docfile->getFsdata());
				$this->visuFile = $this->_file_path;
			}
			else{
				return false;
			}
		}
		return $this->visuFile;
	}//End of method

	/**
	 * Get the visualisation file name and path
	 *
	 */
	private function _getMainFile()
	{
		if(!isset($this->document)){
			return false;
		}

		$factory = $this->document->dao->factory;
		$dao = $factory->getDao(\Rbplm\Ged\Docfile\Version::$classId);
		//$role = \Rbplm\Ged\Docfile\Role::MAIN;
		$dao->loadDocfiles($this->document);

		$docfile = array_shift($this->document->getDocfiles());
		if($docfile){
			$this->initFromFsdata($docfile->getData()->getFsdata());
		}
		else{
			return false;
		}
		return $docfile;
	}//End of method

}//End of class
