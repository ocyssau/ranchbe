<?php
namespace Rbs\Ged\Document;

use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Ged\AccessCode;
use Rbplm\Ged\Doctype;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Dao\NotExistingException;
use Rbplm\Ged\AccessCodeException;
use Rbplm\Org\Workitem;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Vault\Vault;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Signal;
use Rbplm\Sys\Date as DateTime;

class Service
{

	/**
	 *
	 * @param Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * Put files of a Document from the vault reposit directory to user wildspace and lock the Document.
	 *
	 * CheckOut is use for modify a Document. The files are put in a directory where user has write access
	 * and the Document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 *
	 * If option $checkoutFile is false, options $ifExistMode, $getFiles and $withFiles are ignored
	 * If option $getFiles is false, options $ifExistMode is ignored
	 *
	 * @param Document\Version $document
	 * @param boolean $withFiles
	 *        	if true, checkin all associated docfiles loaded in document
	 * @param enum $replace
	 *        	if true replace file in ws if existing
	 * @param boolean $getFiles
	 *        	if true copy file in wildspace
	 *        	@throw Exception
	 * @return Service
	 *
	 */
	public function checkout($document, $withFiles = true, $replace = true, $getFiles = true)
	{
		$factory = $this->factory;
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* check if access is free, valide codes : 0-1 */
		$aCode = $document->checkAccess();
		if ( $aCode > AccessCode::FREE ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'accessCode' => $aCode
			));
		}

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_PRE_CHECKOUT, $document);

		$document->lock(AccessCode::CHECKOUT, CurrentUser::get());

		/* get list of associated files and checkout */
		if ( $withFiles ) {
			$docfiles = $document->getDocfiles();
			if ( $docfiles ) {
				foreach( $docfiles as $docfile ) {
					try {
						$dfService->checkout($docfile, $replace, $getFiles);
					}
					catch( AccessCodeException $e ) {
						continue;
					}
				}
			}
		}
		$document->dao->save($document);

		Signal::trigger($document::SIGNAL_POST_CHECKOUT, $document);

		return $this;
	}

	/**
	 * Replace a file checkout in the vault and unlock it.
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed (check by md5 code comparaison), create a new iteration
	 *
	 * The document must be populate with Docfiles and Parent container
	 *
	 * @param Document\Version $document
	 * @param bool $releasing
	 *        	if true, release the docfile after replace
	 * @param bool $checkAccess
	 *        	if true, check if access code is right
	 * @throws Exception
	 * @return Service
	 */
	public function checkin(Document\Version $document, $releasing = true, $updateData = true, $deleteFileInWs = true, $fromWildspace = true, $checkAccess = true)
	{
		$factory = $this->factory;
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* Action Name definition */
		$actionName = null;
		if ( $releasing == true && $updateData == true ) {
			$actionName = 'checkinAndRelease';
		}
		elseif ( $releasing == true && $updateData == false ) {
			$actionName = 'checkinAndReset';
		}
		elseif ( $releasing == false && $updateData == true ) {
			if ( $postIteration != $preIteration ) {
				$actionName = 'checkinAndKeep';
			}
		}












		$container = $document->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if ( ($aCode != AccessCode::CHECKOUT or $coBy != CurrentUser::get()->getId()) and $checkAccess ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'accessCode' => $aCode
			));
		}

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_PRE_CHECKIN, $document);

		/** @var Docfile\Version $mainDf Main Docfile */
		$mainDf = $document->getMainDocfile();

		if ( $actionName != 'checkinAndReset' ) {
			/* Set pdmDatas from mainDocfile name as key */
			if ( $this->inputPdmData ) {
				/** @var \Pdm\Input\PdmData $inputPdmData */
				$inputPdmData = $this->inputPdmData;

				if ( !isset($this->wildspace) ) {
					$wildspace = Wildspace::get(CurrentUser::get());
				}
				else {
					$wildspace = $this->wildspace;
				}

				$rbconverterWorkingDir = $wildspace->getPath() . '/rbconverter/';

				if ( $mainDf ) {
					$pdmdataKey = $mainDf->getName();
					if ( $inputPdmData->offsetExists($pdmdataKey) ) {
						/** @var \Pdm\Input\PdmData\Element $inputElmt */
						$inputElmt = $inputPdmData->offsetGet($pdmdataKey);
						$inputElmt->setWorkingDir($rbconverterWorkingDir);
						$setter = new \Pdm\Input\PdmData\Setter($factory);
						$setter->setDocument($document, $inputElmt);
					}
				}
			}

			/* Remove previous VISU only except for reset action */
			$tempdoc = new Document\Version();
			$tempdoc->setId($document->getId());
			$dfdao = $factory->getDao(Docfile\Version::$classId);
			$dfdao->loadDocfiles($tempdoc, $dfdao->toSys('mainrole') . ' >= ' . Docfile\Role::VISU);
			foreach( $tempdoc->getDocfiles() as $df ) {
				$dfService->delete($df, $withfiles = true, $withiterations = false);
			}
			unset($tempdoc);
		}

		/** @var array $docfiles list of checkouted  associated files */
		$docfiles = $document->getDocfiles();

		/* Checkin associated files */
		$preIteration = $document->iteration;
		if ( $docfiles ) {
			foreach( $docfiles as $docfile ) {
				try {
					/* If docfile is existing in db */
					if ( $docfile->getId() > 0 ) {
						if ( $updateData && $fromWildspace ) {
							$dfService->checkinFromWs($docfile, $releasing, $updateData, $deleteFileInWs, $checkAccess);
						}
						else {
							$dfService->checkin($docfile, $releasing, $updateData, $checkAccess);
						}
					}
					/* If not yet saved */
					else {
						$dfService->create($docfile, $vault);
					}
				}
				catch( AccessCodeException $e ) {
					continue;
				}
				catch( \PDOException $e ) {
					throw $e;
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
		}
		$postIteration = $document->iteration;

		/* Remove checkout access restriction */
		if ( $releasing == true ) {
			$document->unLock();
		}

		/* Checkin and save product */
		if ( $document->product ) {
			$product = $document->product;
			$product->setDocument($document);
			$pdmservice = new \Rbs\Pdm\Product\Service($factory);
			$pdmservice->checkin($product, $lock);
			unset($pdmservice);
		}

		/* Save Document */
		$document->dao->save($document);

		/* Notiy observers on event */
		if ( $actionName ) {
			Signal::trigger($document::SIGNAL_POST_CHECKIN, $document, $actionName);
		}
	}

	/**
	 * This method can be used to remove a document from a container
	 * Return true or false.
	 *
	 * This method suppress the record of the document and associated files
	 * and suppress files and version files from the container directory.
	 * If a history indice exist, suppress the current indice and restore the previous indice.
	 *
	 * @param Document\Version $document
	 * @return Service
	 * @throws Exception
	 */
	function delete(Document\Version $document)
	{
		$factory = $this->factory;
		$dao = $factory->getDao(Document\Version::$classId);
		$dfdao = $factory->getDao(Docfile\Version::$classId);

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_PRE_DELETE, $document);

		/* get the container */
		$container = $document->getParent();
		if ( !$container ) {
			$containerId = $document->getParent(true);
			$container = $factory->getModel(Workitem::$classId);
			$container->dao = $factory->getDao(Workitem::$classId);
			$container->dao->loadFromId($container, $containerId);
			$document->setParent($container);
		}

		/* check if access is free */
		$access = $document->checkAccess();
		if ( !($access == AccessCode::FREE || $access == AccessCode::SUPPRESS || $access >= 100) ) {
			throw new AccessCodeException('Document %element% is locked with code %element1%, suppress is not permit', ERROR, array(
				'element' => $document->getUid(),
				'element1' => $access
			));
		}

		/* get docfiles */
		$dfdao->loadDocfiles($document);
		$dfService = new \Rbs\Ged\Docfile\Service($factory);

		/* Delete files of current version and old iteration */
		foreach( $document->getDocfiles() as $docfile ) {
			try {
				$dfService->delete($docfile, $withfile = true, $withiterations = true);
			}
			catch( \Exception $e ) {
				throw $e;
			}
		}

		/* Get the previous version of document */
		try {
			$previous = new Document\Version();
			$dao->loadPreviousVersion($previous, $document->getId());
		}
		catch( NotExistingException $e ) {
			$previous = null;
		}
		catch( \Exception $e ) {
			throw $e;
		}

		/* get the container */
		if ( $previous ) {
			$container = $previous->getParent();
			if ( !$container ) {
				$containerId = $previous->getParent(true);
				$container = $factory->getModel(Workitem::$classId);
				$container->dao = $factory->getDao(Workitem::$classId);
				$container->dao->loadFromId($container, $containerId);
				$previous->setParent($container);
			}

			/* Restore file of previous version */
			$previous->lock(AccessCode::FREE);
			$previous->setLifeStage('GETBACK');
			$dfdao->loadDocfiles($previous);
			foreach( $previous->getDocfiles() as $docfile ) {
				try {
					$docfile->lock(AccessCode::FREE);
					$docfile->setLifeStage('GETBACK');
					$docfile->setParent($previous);
					$dfService->restoreVersion($docfile);
					$dfdao->save($docfile);
				}
				catch( \Exception $e ) {
					throw $e;
				}
			}
		}

		/* Delete record of document */
		$dao->deleteFromId($document->getId());

		/* @todo: Delete record of all process instance */
		/* @todo: Delete or archive history */
		/* @todo remove visu file */
		/* @todo remove thumbnail file */
		// $attach = new thumbnail($this);
		// if(is_file($attach->getProperty('file'))){
		// $attach->putInTrash();
		// }

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_POST_DELETE, $document);

		return $this;
	}

	/**
	 *
	 * @param boolean $lock
	 *        	The document must be populate with parent container before
	 *        	The document must be populate with docfiles
	 *        	Each docfile must have a property fsdata set with the fsdata to store in vault
	 *
	 */
	public function createdocument($document, $lock = false)
	{
		$factory = $this->factory;

		$container = $document->getParent();
		$docfiles = $document->getDocfiles();

		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_PRE_CREATE, $document);

		/* OPEN TRANSACTION */
		$connexion = $factory->getConnexion();
		$connexion->beginTransaction();

		/* SET DOCTYPE, ASSUME THAT FIRST DOCFILE IS THE MAIN DOCFILE */
		$doctype = new Doctype();
		$mainDocfile = reset($docfiles); /* First element */

		$factory->getDao($doctype->cid)->loadFromDocument($doctype, $document->getNumber(), $mainDocfile->fsdata->getExtension(), $mainDocfile->fsdata->getType());
		$document->setDoctype($doctype);
		$document->setUid($document->getNumber() . '' . $document->version);

		/* SAVE DOCUMENT */
		if ( !$document->getId() ) {
			$this->feedbacks[] = 'create_doc : ' . $document->getNumber();
			try {
				$document->dao->save($document);
				$this->feedbacks[] = 'The doc ' . $document->getNumber() . ' has been created';
			}
			catch( \Exception $e ) {
				$this->errors[] = 'Can not create the doc ' . $document->getNumber();
				$this->errors[] = $e->getMessage();
				$connexion->rollBack();
				return false;
			}
		}

		/* PUT FILE IN VAULT */
		$dfService = new \Rbs\Ged\Docfile\Service($factory);
		foreach( $docfiles as $docfile ) {
			try {
				$this->feedbacks[] = 'add_file : ' . $docfile->getName() . ' to ' . $document->getUid();
				$dfService->create($docfile, $vault);
				$this->feedbacks[] = 'the file ' . $docfile->getName() . ' has been associated to document ' . $document->getUid();
			}
			catch( \Exception $e ) {
				$this->errors[] = 'The file ' . $docfile->getName() . ' can not be associated to the document';
				$this->errors[] = $e->getMessage();
			}
		}

		$connexion->commit();

		if ( $this->keepCo ) {}
		if ( $lock ) {}

		/* Notiy observers on event */
		Signal::trigger($document::SIGNAL_POST_CREATE, $document);

		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function newVersion($document, $newContainer = null, $newVersionId = null)
	{
		$factory = $this->factory;
		$dfDao = $factory->getDao(Docfile\Version::$classId);
		$docDao = $factory->getDao(Document\Version::$classId);
		$dfDao->loadDocfiles($document);

		$container = $document->getParent();
		$docfiles = $document->getDocfiles();

		if ( $newContainer ) {
			$tocontainer = $newContainer;
		}
		else {
			$tocontainer = $container;
		}

		$fromvault = new Vault(Reposit::init($container->path), $factory->getDao(Record::$classId));
		if ( $newContainer ) {
			$tovault = new Vault(Reposit::init($newContainer->path), $factory->getDao(Record::$classId));
		}
		else {
			$tovault = $fromvault;
		}

		if ( check_ticket('container_document_change_indice', $this->areaId, false) === false ) {
			$this->feedbacks[] = 'You have not permission to upgrade the document ' . $document->getUid();
			return;
		}

		/* Check access */
		if ( $document->checkAccess() == AccessCode::FREE ) {
			/* SEND SIGNAL */
			Signal::trigger($document::SIGNAL_PRE_NEWVERSION, $document);

			/* Lock the current indice */
			$document->lock(AccessCode::HISTORY);

			/* Lock access to files too */
			foreach( $docfiles as $docfile ) {
				$docfile->lock(AccessCode::HISTORY);
			}
			$this->feedbacks[] = 'the document ' . $document->getUid() . ' has been locked.';
		}
		elseif ( $document->checkAccess() < AccessCode::LOCKED ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'access_code' => $aCode
			));
		}

		/* Set the version */
		$lastVersionId = (int)$document->dao->getLastVersion($document->getUid());
		if ( $newVersionId && $newVersionId > $lastVersionId ) {
			$newVersionId = (int)$newVersionId;
		}
		else {
			$newVersionId = (int)$lastVersionId + 1;
		}

		/* OPEN TRANSACTION */
		$connexion = $factory->getConnexion();
		$connexion->beginTransaction();

		try {
			/* CLONE DOCUMENT AND DOCFILES */
			$newDocument = clone ($document);
			$newDocument->lock(AccessCode::FREE);
			$newDocument->version = $newVersionId;
			$newDocument->setUid($newDocument->getNumber() . '.' . $newVersionId);
			$document->dao->save($newDocument);
			foreach( $docfiles as $docfile ) {
				$newDocfile = clone ($docfile);
				$newDocfile->lock(AccessCode::FREE);
				$newDocfile->version = $newVersionId;
				$newDocfile->setUid($newDocfile->getName() . '.' . $newVersionId);
				$newDocument->addDocfile($docfile);

				/* CLONE DATAS */
				$record = $docfile->getData();
				$newDocfile->setData(clone ($record));
				$newDocfile->getData()->setFsdata(clone ($record->getFsdata()));

				/* Re-set record to docfile after move to version reposit */
				$record = $tovault->depriveToVersion($record, $docfile->version);
				$docfile->setData($record);

				/* Save old docfile first to prevent unicity violation */
				$docfile->dao->save($docfile);
				$docfile->dao->save($newDocfile);
			}
			$connexion->commit();

			/* SEND SIGNAL */
			Signal::trigger($document::SIGNAL_POST_NEWVERSION, $document);
		}
		catch( \Exception $e ) {
			$connexion->rollBack();
			throw $e;
		}
	}

	/**
	 */
	public function addFileToDocument($fsdata, $document)
	{
		$factory = $this->factory;
		$container = $document->getParent();
		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		/* Check access on document */
		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if ( !($aCode == AccessCode::FREE or ($aCode == AccessCode::CHECKOUT and $coBy == CurrentUser::get()->getId())) ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'access_code' => $aCode
			));
		}

		/* CREATE DOCFILE */
		$docfile = Docfile\Version::init()->setName($fsdata->getName())
			->setNumber($fsdata->getName());
		$docfile->setUid($fsdata->getName() . '.1');
		$docfile->setParent($document);

		/* SAVE RECORDS IN VAULT */
		try {
			$record = $vault->record(Record::init(), $fsdata, $fsdata->getName());
		}
		catch( ExistingException $e ) {}

		$docfile->setData($record);
		$factory->getDao($docfile)->save($docfile);
	}

	/**
	 * Put file in wildspace
	 */
	public function putinws($document, $prefix = 'consult_')
	{
		$factory = $this->factory;
		$dao = $factory->getDao(Docfile\Version::$classId);
		if ( !isset($this->wildspace) ) {
			$this->wildspace = Wildspace::get(CurrentUser::get());
		}
		$toPath = $this->wildspace->getPath();

		foreach( $document->getDocfiles() as $docfile ) {
			$docfile->getData()
				->getFsData()
				->copy($toPath . '/' . $prefix . $docfile->getData()
				->getName(), 0777, false);
		}
	}

	/**
	 * This method can be used to move a document from container to another.
	 * Return true or false.
	 *
	 * A document can be move only between container of the same space.
	 * You can not move a document of a bookshop container to a cadlib container.
	 * This method modify the document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move version files and the previous indice document.
	 *
	 * @param
	 *        	$document
	 * @param $toContainer the
	 *        	target container.
	 */
	function moveDocument($document, $toContainer)
	{
		$aCode = $document->checkAccess();
		if ( $aCode >= 100 ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'accessCode' => $aCode
			));
		}

		// TODO : revoir la transaction en utilisant rollback pour autoriser les validations intermediaires
		$toDir = $toContainer->path;
		$fromNumber = $document->getNumber();
		$toPath = $toContainer->path;

		$dao = $this->factory->getDao($document->cid);
		$dfDao = $this->factory->getDao(Docfile\Version::$classId);

		$reposit = Reposit::init($toContainer->path);

		$conn = $this->factory->getConnexion();
		$conn->beginTransaction();

		/* SET PARENT */
		$document->setParent($toContainer);

		/* SEND SIGNAL */
		Signal::trigger($document::SIGNAL_PRE_MOVE, $document);

		/* MOVE FSDATA AND RESET TO DOCFILES */
		foreach( $document->getDocfiles() as $docfile ) {
			/* MOVE FSDATA IN VAULT */
			$data = $docfile->getData();
			$fsdata = $data->getFsdata();
			$fsdata->rename($reposit->getPath() . '/' . $fsdata->getName());

			/* RESET DATA TO DOCFILE */
			$data->setFsdata($fsdata);
			$docfile->setData($data);

			/* TRY TO SAVE DOCFILE */
			try {
				$dfDao->save($docfile);
			}
			catch( \Exception $e ) {
				$conn->rollBack();
				throw $e;
			}
		}

		/* TRY TO SAVE DOCUMENT */
		try {
			$dao->save($document);
		}
		catch( \Exception $e ) {
			$conn->rollBack();
			throw $e;
		}

		$conn->commit();

		/* SEND SIGNAL */
		Signal::trigger($document::SIGNAL_POST_MOVE, $document);

		// @todo Copy picture file
		// @todo Copy the doclinks
		// @todo Copy the relationship

		return $toDocument;
	}

	/**
	 * This method can be used to copy a document from container to another.
	 *
	 * A document can be copy only between container of the same space.
	 *
	 * @param
	 *        	$document
	 * @param
	 *        	$file
	 */
	function associateFile($document, $file)
	{
		$aCode = $document->checkAccess();
		if ( $aCode >= 100 ) {
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array(
				'accessCode' => $aCode
			));
		}

		$factory = $this->factory;

		$container = $document->getParent();
		$dao = $factory->getDao(Docfile\Version::$classId);

		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		/* PUT FILE IN VAULT */
		try {
			$fsdata = new \Rbplm\Sys\Fsdata($file);
			$docfile = Docfile\Version::init();
			$docfile->setName($fsdata->getName());
			$record = $vault->record(Record::init(), $fsdata, $docfile->getName());
			$docfile->setParent($document);
			$docfile->setData($record);
			$dao->save($docfile);
			$this->feedbacks[] = 'the file ' . $record->getName() . ' has been associated to document ' . $document->getUid();
			$document->addDocfile($docfile);
		}
		catch( \Exception $e ) {
			$this->errors[] = 'The file ' . $fsdata->getName() . ' can not be associated to the document';
			$this->errors[] = $e->getMessage();
			throw $e;
		}
		$fsdata->delete();
		return $this;
	}

	/**
	 * This method can be used to copy a document from container to another.
	 *
	 * A document can be copy only between container of the same space.
	 *
	 * @param Document\Version $document
	 * @param $toContainer the
	 *        	target container.
	 * @param $toNumber number
	 *        	of the new document to create.
	 * @param $toVersion version
	 *        	for new document.
	 */
	function copyDocument($document, $toContainer, $toNumber, $toVersion = 1)
	{
		// TODO : revoir la transaction en utilisant rollback pour autoriser les validations intermediaires
		$toDir = $toContainer->path;
		$fromNumber = $document->getNumber();
		$toPath = $toContainer->path;

		Signal::trigger($document::SIGNAL_PRE_COPY, $document);

		$dao = $this->factory->getDao($document->cid);
		$dfDao = $this->factory->getDao(Docfile\Version::$classId);

		$reposit = Reposit::init($toContainer->path);
		$vault = new Vault($reposit, $this->factory->getDao(Record::$classId));

		$conn = $this->factory->getConnexion();
		$conn->beginTransaction();
		$vault->beginTransaction();

		$toDocument = clone ($document);
		$toDocument->setName($toNumber)->setNumber($toNumber);
		$toDocument->version = $toVersion;
		$toDocument->iteration = 1;
		$toDocument->lifeStage = 'init';
		$toDocument->setCreated(new DateTime());
		$toDocument->setUpdated(new DateTime());
		$toDocument->setCreateBy(CurrentUser::get());
		$toDocument->setUpdateBy(CurrentUser::get());
		$toDocument->setOwner(CurrentUser::get());
		$toDocument->unlock();
		$toDocument->setFrom($document);
		$toDocument->setUid($toNumber . '.' . $toVersion);

		/* TRY TO SAVE DOCUMENT */
		try {
			$dao->save($toDocument);
		}
		catch( \Exception $e ) {
			$conn->rollBack();
			throw $e;
		}

		foreach( $document->getDocfiles() as $docfile ) {
			$todocfile = clone ($docfile);
			$todocfile->setParent($toDocument);
			$todocfile->version = 1;
			$todocfile->iteration = 1;
			$todocfile->lifeStage = 'init';
			$todocfile->setCreated(new DateTime());
			$todocfile->setUpdated(new DateTime());
			$todocfile->setCreateBy(CurrentUser::get());
			$todocfile->setUpdateBy(CurrentUser::get());
			$todocfile->setOwner(CurrentUser::get());
			$todocfile->unlock();
			$todocfile->setFrom($docfile);
			$toDocument->addDocfile($todocfile);

			/* CLONE RECORD DATA */
			$todata = clone ($docfile->getData());

			/* rename docfile by replacing fromDocument Name by toDocument Name in docfile name, else rename with md5 */
			$fromDfName = $docfile->getName();
			$toDfName = str_replace($fromNumber, $toNumber, $fromDfName);
			if ( $toDfName == $fromDfName ) {
				$toDfName = uniqid() . $todata->extension;
			}

			if ( $vault->exist($toDfName) ) {
				$conn->rollBack();
				$vault->rollBack();
				throw new \Rbplm\Vault\ExistingDataException('Data %data% is yet existing in vault %vault%', Error::ERROR, array(
					'data' => $toDfName,
					'vault' => $toPath
				));
				die();
			}

			/* COPY FSDATA IN VAULT */
			$fromfsdata = $todata->getFsdata();
			$vault->record($todata, $fromfsdata, $toDfName, $md5 = null);

			/* SET DATA TO DOCFILE */
			$todocfile->setData($todata);

			/* TRY TO SAVE DOCFILE */
			try {
				$todocfile->setName($toDfName)
					->setUid($toDfName)
					->setNumber($toDfName);
				$dfDao->save($todocfile);
			}
			catch( \Exception $e ) {
				$conn->rollBack();
				$vault->rollBack();
				throw $e;
			}
		}

		$conn->commit();

		Signal::trigger($document::SIGNAL_POST_COPY, $document);

		// @todo Copy picture file
		// @todo Copy the doclinks
		// @todo Copy the relationship

		return $toDocument;
	}
}
