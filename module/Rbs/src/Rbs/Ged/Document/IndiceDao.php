<?php

namespace Rbs\Ged\Document;

/** SQL_SCRIPT>>
 CREATE TABLE `document_indice` (
 `document_indice_id` int(11) NOT NULL DEFAULT '0',
 `indice_value` varchar(8) NOT NULL DEFAULT '',
 PRIMARY KEY (`document_indice_id`),
 UNIQUE KEY `UC_indice_value` (`indice_value`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class IndiceDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='document_indice';
	public static $vtable='document_indice';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_indice_id'=>'id',
		'indice_value'=>'name',
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->metaModel = static::$sysToApp;
		$this->_table = static::$table;
	} //End of function


	/**
	 * Return an array indice_id=>indice_number if success, false else.
	 *
	 * @param  $filters
	 * @return PDOStatement
	 */
	function getIndices($filters=null)
	{
		$sql = 'SELECT document_indice_id AS id, indice_value AS name FROM '.$this->_table;
		if($filters){
			$sql.= ' WHERE '.$filters;
		}

		$stmt = $this->connexion->query($sql);
		return $stmt;
	}//End of method

	/**
	 * @see Rbplm\Dao.DaoInterface::toSys()
	 *
	 * Translate current row data from model to db semantic
	 *
	 * @param array|string		 $in	Application property name or array (key as model property name => value)
	 * @return array					System attributs
	 */
	public static function toSys($in)
	{
		$out = array();
		$translator = array_flip( static::$sysToApp );
		if( is_array($in) ){
			foreach( $in as $appName=>$value ){
				if( isset($translator[$appName]) ){
					$out[$translator[$appName]] = $value;
				}
				else{
					$out[$appName] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$out = $translator[$in];
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}

	/**
	 * @see Rbplm\Dao.DaoInterface::toApp()
	 *
	 * Translate property name and value to model semantic
	 *
	 * @param array|string		$in		Db properties name or array (key as db column name => value)
	 * @return array			Model properties
	 */
	public static function toApp($in)
	{
		$translator = static::$sysToApp;
		if( is_array($in) ){
			$out = array();
			foreach($in as $asSys=>$value){
				if( isset($translator[$asSys]) ){
					$out[$translator[$asSys]] = $value;
				}
				else{
					$out[$asSys] = $value;
				}
			}
		}
		else{
			if( isset($translator[$in]) ){
				$out = $translator[$in];
			}
			else{
				$out = $in;
			}
		}
		return $out;
	}


}