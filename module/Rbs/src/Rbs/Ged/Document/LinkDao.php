<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;


/** SQL_SCRIPT>>
CREATE TABLE `workitem_doc_rel` (
 `dr_link_id` int(11) NOT NULL default '0',
 `dr_document_id` int(11) default NULL,
 `dr_l_document_id` int(11) default NULL,
 `dr_access_code` int(11) default NULL,
 hash char(32) default NULL,
 data text default NULL,
 PRIMARY KEY  (`dr_link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`)
);

CREATE TABLE `workitem_doc_rel_seq` (
 `id` int(11) NOT NULL
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `workitem_doc_rel`
 ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;

 ALTER TABLE `bookshop_doc_rel`
 ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE;

 ALTER TABLE `cadlib_doc_rel`
 ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE;

 ALTER TABLE `mockup_doc_rel`
 ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE;
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends DaoSier
{
	/**
	 * @var string
	 */
	public static $table='workitem_doc_rel';
	/**
	 * @var string
	 */
	public static $vtable='workitem_doc_rel';
	/**
	 * @var string
	 */
	public static $sequenceName='workitem_doc_rel_seq';

	/**
	 * @var string
	 */
	public static $childTable = 'workitem_documents';
	/**
	 * @var string
	 */
	public static $childForeignKey = 'document_id';
	/**
	 * @var string
	 */
	public static $parentTable = 'workitem_documents';
	/**
	 * @var string
	 */
	public static $parentForeignKey = 'document_id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'dr_link_id'=>'id',
		'dr_document_id'=>'parentId',
		'dr_l_document_id'=>'childId',
		'dr_access_code'=>'accessCode',
		'hash'=>'hash',
		'data'=>'data',
	);

	public function __construct($conn=null)
	{
		parent::__construct($conn);

		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::save()
	 */
	public function save(MappedInterface $mapped)
	{
		$parentId = $mapped->getParent(true);
		if ($parentId){
			$mapped->parentId = $parentId;
		}

		$childId = $mapped->getChild(true);
		if ($childId){
			$mapped->childId = $childId;
		}

		if(!$parentId OR !$childId){
			throw new Exception('REFERENCE_NOT_SAVED', Error::BAD_UID);
		}

		$parentUid = $mapped->getParentUid();
		$childUid = $mapped->getChildUid();

		$mapped->hash = md5($parentUid.$childUid);
		parent::save($mapped);
	}

	/**
	 * @throws Exception
	 */
	public function bind($mapped)
	{
		return array(
			':uid'=>$mapped->getUid(),
			':parentId'=>(int)$mapped->getParent(true),
			':childId'=>(int)$mapped->getChild(true),
			':name'=>$mapped->getName(),
			':lindex'=>(int)$mapped->index,
			':data'=>\json_encode($mapped->data)
		);
	}

	/**
	 * Update a link between document
	 * Return true or false
	 *
	 * This method search in the _doc_rel table the document linked with id $l_document_id and replace it
	 * by the the $_new_l_document_id
	 * This function is used when the document is indice upgraded for always keeps the links.
	 * The link is replace only if is not explicitly locked. A link is locked if the value of dr_access_code is > to 14.
	 *
	 * @param integer $childId	 Id of the linked document.
	 * @param integer $newChildId(	 New id of the linked document.
	 */
	function replace($childId , $newChildId)
	{
		$query = "UPDATE $this->_table
					SET `dr_l_document_id` = REPLACE(`dr_l_document_id`, $childId, $newChildId)
							WHERE `dr_l_document_id` = '$childId'
							AND (`dr_access_code` < '15' OR `dr_access_code` IS NULL)";
	}//End of method

	/**
	 * @param int $parentId
	 * @param array $childMetamodel
	 * @param string $filter
	 *
	 * @return \PDOStatement
	 */
	public function getChildren($parentId, $filter=null, $bind=array())
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('childId');
		$rightTable = $this->_childTable;
		$rightKey = $this->_childForeignKey;
		$select = array();

		if(!$this->loadChildrenStmt){
			if($filter){
				$filterStr = 'AND '.$filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else{
				$filterStr = '';
				$selectStr = '*';
			}

			$sql="SELECT $selectStr FROM $leftTable AS lt";
			if($rightTable && $rightKey){
				$sql.=" JOIN $rightTable AS jt ON lt.$leftKey=jt.$rightKey";
			}
			$sql.=" WHERE lt.dr_document_id=:parentId $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}

		$bind[':parentId'] = $parentId;
		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	} //End of function

	/**
	 * Get documents father links of one document
	 * Return a array with properties of the linked documents
	 *
	 * This method return a array with properties of documents calling by link the document with id document_id.
	 *
	 * @param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function getParents($childId, $filter=null, $bind=array())
	{
		$leftTable = $this->_table;
		$leftKey = $this->toSys('parentId');
		$rightTable = $this->_parentTable;
		$rightKey = $this->_parentForeignKey;
		$select = array();

		if(!$this->loadParentStmt){
			if($filter){
				$filterStr = 'AND '.$filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else{
				$filterStr = '';
				$selectStr = '*';
			}

			$sql="SELECT $selectStr FROM $leftTable AS lt";
			if($rightTable && $rightKey){
				$sql.=" JOIN $rightTable AS jt ON lt.$leftKey=jt.$rightKey";
			}
			$sql.=" WHERE lt.dr_l_document_id=:childId $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentStmt = $stmt;
		}

		$bind[':childId'] = $childId;
		$this->loadParentStmt->execute($bind);
		return $this->loadParentStmt;
	}//End of method

	/**
	 * Lock all links of the document document_id with code access_code
	 * When doclink is locked, he is no more following indice upgrade.
	 * Return false or true if success.
	 *
	 * @param $code(integer) Code access of the lock
	 */
	function lockAll($code = 15)
	{
		$data['dr_access_code'] = $code;
		//"$this->OBJECT_TABLE , $data , 'UPDATE' , "dr_document_id = ".$this->document->GetId())) { //Update table
	}//End of method

	/**
	 * Lock one links with code access_code
	 * Return false or true if success.
	 *
	 * @param $linkId(integer) id of link to lock
	 * @param $code(integer) Code access of the lock
	 */
	function lock($linkId, $code = 15)
	{
		$data['dr_access_code'] = $code;
		//if(!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "dr_link_id = ".$linkId)) { //Update table
	}//End of method

	/**
	 *
	 * @param unknown_type $linkId
	 * @param unknown_type $space
	 * @return boolean|unknown
	 */
	static function getFatherDocFromLinkId($linkId, $space)
	{
		$dbranchbe = Ranchbe::getDb();
		$OBJECT_TABLE  = $space->SPACE_NAME . '_doc_rel';

		$query = "SELECT dr_document_id FROM $OBJECT_TABLE WHERE dr_link_id = $linkId";
		if(!$docId = $dbranchbe->GetOne($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}else{
			return $docId;
		}
	}

	/**
	 *
	 * @param unknown_type $linkId
	 * @param unknown_type $space
	 */
	static function getChildDocFromLinkId($linkId, $space)
	{
		$dbranchbe = Ranchbe::getDb();
		$OBJECT_TABLE  = $space->SPACE_NAME . '_doc_rel';

		$query = "SELECT dr_l_document_id FROM $OBJECT_TABLE WHERE dr_link_id = $linkId";
		if(!$docId = $dbranchbe->GetOne($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}else{
			return $docId;
		}
	}
}
