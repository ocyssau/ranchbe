<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
abstract class VersionDao extends DaoSier
{
	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'open_date'=>'date',
		'check_out_date'=>'date',
		'update_date'=>'date',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function save(MappedInterface $mapped)
	{
		$parent = $mapped->getParent();
		if ($parent){
			$mapped->parentId = $parent->id;
		}

		$updateBy = $mapped->getUpdateBy();
		if ($updateBy){
			$mapped->updateById = $updateBy->id;
		}

		$createBy = $mapped->getCreateBy();
		if ($createBy){
			$mapped->createById = $createBy->id;
		}

		$lockBy = $mapped->getLockBy();
		if ($lockBy){
			$mapped->lockById = $lockBy->id;
		}

		$doctype = $mapped->getDoctype();
		if ($doctype){
			$mapped->doctypeId = $doctype->id;
		}

		$category = $mapped->getCategory();
		if ($category){
			$mapped->categoryId = $category->id;
		}

		$spaceName = explode('_',$this->_table);
		$mapped->spaceName = $spaceName[0];

		parent::save($mapped);
	} //End of function

	/**
	 * @see DaoInterface::loadFromName()
	 *
	 * Load the last version of this named document
	 */
	public function loadFromName(MappedInterface $mapped, $name, $versionId=null)
	{
		if($versionId){
			$filter = 'document_name=:name AND document_version=:versionId LIMIT 1';
			$bind = array(':name'=>$number, ':versionId'=>$versionId);
		}
		else{
			$filter = "document_name=:name ORDER BY document_version DESC";
			$bind = array(':name'=>$number);
		}
		return $this->load($mapped, $filter, $bind);
	} //End of function

	/**
	 * If $versionId is null, load the last version
	 *
	 * @param MappedInterface $mapped
	 * @param string $number
	 * @param integer $versionId
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromNumber(MappedInterface $mapped, $number, $versionId=null)
	{
		if($versionId){
			$filter = 'document_number=:number AND document_version=:versionId LIMIT 1';
			$bind = array(':number'=>$number, ':versionId'=>$versionId);
		}
		else{
			$filter = "document_number=:number ORDER BY document_version DESC";
			$bind = array(':number'=>$number);
		}
		return $this->load($mapped, $filter, $bind );
	} //End of function

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::hydrate()
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		parent::hydrate($mapped, $row, $fromApp);
		$mapped->spaceName = $row['doc_space'];
		return $mapped;
	} //End of function

	/**
	 *
	 * @param integer $documentId
	 */
	public function loadPreviousVersion($previous, $documentId)
	{
		$table = $this->_table;
		$filter = "document_number=(SELECT document_number FROM $table WHERE document_id=:id)
					AND document_indice_id<(SELECT document_indice_id FROM $table WHERE document_id=:id)
					ORDER BY document_indice_id DESC";

		//Get the previous indice document_id
		$this->load($previous, $filter, array(':id'=>$documentId));
	}

	/**
	 *
	 * @param unknown_type $uid
	 */
	public function getLastVersion($number)
	{
		if(!$this->getlastversionStmt){
			$table = $this->_table;
			$sql = "SELECT MAX(document_indice_id) FROM $table WHERE document_number=:number GROUP BY document_number";
			$this->getlastversionStmt = $this->connexion->prepare($sql);
		}
		$this->getlastversionStmt->execute(array(':number'=>$number));
		$lastVersionId = $this->getlastversionStmt->fetchColumn(0);
		return $lastVersionId;
	}

}
