<?php
namespace Rbs\Ged\Document;

use \Rbplm\Signal;

class Listener
{
	/**
	 *
	 * @var \Rbs\Ged\Document\History
	 */
	public $history;

	/**
	 *
	 * @var \Rbs\Notification\Notification
	 */
	public $notification;

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @param \Rbs\Space\Factory $factory
	 */
	public function __construct($document, $factory)
	{
		$this->factory = $factory;
		$this->document = $document;
	}

	/**
	 *
	 * @param string $eventName
	 * @param string $args
	 * @return Listener
	 */
	public function init($eventName, $arg1=null)
	{
		$history = $this->getHistory();
		$notification = $this->getNotification();
		$document = $this->document;

		Signal::connect($document, $eventName, array($history,'onEvent'), $arg1);
		Signal::connect($document, $eventName, array($notification,'onEvent'));
		return $this;
	}

	/**
	 * @return \Rbs\Ged\Document\History
	 */
	public function getHistory()
	{
		if(!$this->history){
			$history =  \Rbs\Ged\Document\History::init();
			$history->setDao($this->factory->getDao($history::$classId));
			$this->history = $history;
		}
		return $this->history;
	}

	/**
	 * @return \Rbs\Notification\Notification
	 */
	public function getNotification()
	{
		if(!$this->notification){
			$notification = \Rbs\Notification\Notification::init();
			$notification->factory = $this->factory;
			$this->notification = $notification;
		}
		return $this->notification;
	}

}

