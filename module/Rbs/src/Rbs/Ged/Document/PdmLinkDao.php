<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Document;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;


/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * Read Only DAO
 */
class PdmLinkDao extends DaoSier
{
	/**
	 * @var string
	 */
	public static $table='workitem_documents';
	/**
	 * @var string
	 */
	public static $vtable='workitem_documents';
	/**
	 * @var string
	 */
	public static $sequenceName='';
	/**
	 * @var string
	 */
	public static $parentTable = 'pdm_product_version';
	/**
	 * @var string
	 */
	public static $parentForeignKey = 'document_id';
	/**
	 * @var string
	 */
	public static $childTable = 'pdm_product_instance';
	/**
	 * @var string
	 */
	public static $childForeignKey = '';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
	);

	/**
	 *
	 * @param \Rbplm\Dao\Connexion $conn
	 */
	public function __construct($conn=null)
	{
		parent::__construct($conn);

		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}


	/**
	 * Sql request Alias :
	 * 	.document, .instance, .product
	 *
	 *
	 * @param int $parentId Parent Product Id
	 * @param \Rbs\Dao\Sier\Filter $filter
	 * @return \PDOStatement
	 */
	public function getChildren($productId, $filter=null, $bind=array())
	{
		$instanceTable = $this->_childTable;
		$productTable = $this->_parentTable;
		$documentTable = $this->_table;

		if(!$this->loadChildrenStmt){
			if($filter){
				$filterStr = 'AND '.$filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else{
				$filterStr = '';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			JOIN $productTable as product ON instance.of_product_id=product.id
			JOIN $documentTable as document ON product.document_id=document.document_id
			WHERE instance.parent_id=:parentId $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}

		$bind[':parentId'] = $productId;
		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	} //End of function

	/**
	 * Get documents father links of one document
	 * Return a array with properties of the linked documents
	 *
	 * This method return a array with properties of documents calling by link the document with id document_id.
	 *
	 * @param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function getParents($productId, $filter=null, $bind=array())
	{
		$instanceTable = $this->_childTable;
		$productTable = $this->_parentTable;
		$documentTable = $this->_table;

		if(!$this->loadChildrenStmt){
			if($filter){
				$filterStr = 'AND '.$filter->__toString();
				$select = $filter->getSelect();
				$selectStr = implode($select, ',');
			}
			else{
				$filterStr = '';
				$selectStr = '*';
			}

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			JOIN $productTable as product ON product.id=instance.parent_id
			JOIN $documentTable as document ON document.document_id=product.document_id
			WHERE instance.of_product_id=:productId $filterStr";

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
		}

		$bind[':productId'] = $productId;
		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}//End of method
}
