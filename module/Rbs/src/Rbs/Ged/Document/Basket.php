<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Document;

use Rbplm\Dao\Connexion;
use Rbplm\Sys\Exception as Exception;
use Rbplm\Dao\NotExistingException;
use Rbplm\Sys\Error as Error;
use Rbplm\Ged\Document\Version as DocumentVersion;

/** SQL_SCRIPT>>
 CREATE TABLE `docbasket` (
 `user_id` int(11) NOT NULL,
 `doc_id` int(11) NOT NULL,
 `space_id` int(11) NOT NULL,
 PRIMARY KEY  (`user_id`, `doc_id`, `space_id`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Job to execute in batch mode.
 *
 */
class Basket
{

	/**
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var unknown_type
	 */
	protected $usrId;

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'docbasket';
	protected $_table;

	/**
	 *
	 * @param unknown_type $user
	 */
	public function __construct($user, $factory)
	{
		$this->connexion = Connexion::get();
		$this->_table = self::$table;
		$this->usrId = $user->getId();
		$this->factory = $factory;
	}//End of method

	/**
	 * Return list of document ids of user box
	 * @return array key=>document_id
	 * @param integer $userId
	 */
	public function get()
	{
		$table = $this->_table;

		$dao = $this->factory->getDao(DocumentVersion::$classId);
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[]="doc.`$asSys` AS `$asApp`";
		}
		$select = implode($select, ', ');

		$sql = "SELECT $select FROM workitem_documents AS doc JOIN $table AS ubox ON doc.document_id = ubox.doc_id WHERE user_id =:userId";
		$bind = array(':userId'=>$this->usrId);

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$ret = $stmt->fetchAll();
		if($ret){
			return $ret;
		}
		else{
			throw new NotExistingException(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql));
		}
	}

	/**
	 *
	 * Add a index to user box
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function add($documentId, $spaceId)
	{
		($this->connexion->inTransaction()==true) ? $withTrans = false : null;

		$table = $this->_table;
		$data = array(
			'user_id'=>':userId',
			'doc_id'=>':documentId',
			'space_id'=>':spaceId'
		);

		$bind = array(
			':userId'=>$this->usrId,
			':documentId'=>$documentId,
			':spaceId'=>$spaceId
		);

		if($withTrans) $this->connexion->beginTransaction();
		try{
			//Init statement
			if(!$this->insertStmt){
				foreach($data as $asSys=>$asApp)
				{
					$sysSet[] = $asSys;
					$appSet[] = $asApp;
				}

				$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
				$this->insertStmt = $this->connexion->prepare($sql);
			}

			$this->insertStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
	}

	/**
	 *
	 * Remove a document from user box
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function remove($documentId, $spaceId)
	{
		$table = $this->_table;
		$userId = $this->usrId;

		($this->connexion->inTransaction()==true) ? $withTrans = false : null;
		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->deleteStmt){
			$sql = "DELETE FROM $table WHERE user_id=:userId AND doc_id=:documentId AND space_id=:spaceId";
			$this->deleteStmt = $this->connexion->prepare($sql);
		}

		$bind = array(
			':userId'=>$userId,
			':documentId'=>$documentId,
			':spaceId'=>$spaceId
		);

		try{
			$this->deleteStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

	/**
	 *
	 * Clean all content of user box
	 * @param integer $documentId
	 * @param integer $spaceId
	 * @return boolean
	 */
	public function clean()
	{
		$table = $this->_table;
		$userId = $this->usrId;

		($this->connexion->inTransaction()==true) ? $withTrans = false : null;
		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->cleanStmt){
			$sql = "DELETE FROM $table WHERE user_id=:userId";
			$this->cleanStmt = $this->connexion->prepare($sql);
		}

		$bind = array(
			':userId'=>$userId,
		);

		try{
			$this->cleanStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}

		return $this;
	}

}
