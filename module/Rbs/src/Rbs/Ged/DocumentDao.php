<?php
// %LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/**
 * SQL_SCRIPT>>
 * CREATE TABLE IF NOT EXISTS `workitem_documents` (
 * `document_id` int(11) NOT NULL DEFAULT '0',
 * `document_number` varchar(128) COLLATE latin1_general_ci NOT NULL DEFAULT '',
 * `document_state` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT 'init',
 * `document_access_code` int(11) NOT NULL DEFAULT '0',
 * `document_version` int(11) NOT NULL DEFAULT '1',
 * `workitem_id` int(11) NOT NULL DEFAULT '0',
 * `document_indice_id` int(11) DEFAULT NULL,
 * `instance_id` int(11) DEFAULT NULL,
 * `doctype_id` int(11) NOT NULL DEFAULT '0',
 * `default_process_id` int(11) DEFAULT NULL,
 * `category_id` int(11) DEFAULT NULL,
 * `check_out_by` int(11) DEFAULT NULL,
 * `check_out_date` int(11) DEFAULT NULL,
 * `designation` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
 * `issued_from_document` int(11) DEFAULT NULL,
 * `update_date` int(11) DEFAULT NULL,
 * `update_by` decimal(10,0) DEFAULT NULL,
 * `open_date` int(11) DEFAULT NULL,
 * `open_by` int(11) DEFAULT NULL,
 * `ci` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 * `type_gc` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 * `work_unit` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 * `father_ds` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 * `need_calcul` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
 * PRIMARY KEY (`document_id`),
 * UNIQUE KEY `IDX_workitem_documents_1` (`document_number`,`document_indice_id`),
 * KEY `FK_workitem_documents_2` (`doctype_id`),
 * KEY `FK_workitem_documents_3` (`document_indice_id`),
 * KEY `FK_workitem_documents_4` (`workitem_id`),
 * KEY `FK_workitem_documents_5` (`category_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
 *
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 */
class DocumentDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'workitems_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable = 'workitems_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitems_documents_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id' => 'id',
		'document_number' => 'number',
		'document_state' => 'lifeStage',
		'document_access_code' => 'accessCode',
		'document_version' => 'iteration',
		'workitem_id' => 'parentId',
		'document_indice_id' => 'version',
		'doctype_id' => 'doctypeId',
		'check_out_by' => 'lockById',
		'check_out_date' => 'locked',
		'designation' => 'designation',
		'issued_from_document' => 'fromId',
		'update_date' => 'updated',
		'update_by' => 'updateById',
		'open_date' => 'created',
		'open_by' => 'createById'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'open_date' => 'date',
		'check_out_date' => 'date',
		'update_date' => 'date'
	);

	/**
	 * Constructor
	 *
	 * @param
	 *        	\PDO
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ( $parent ) {
			$mapped->parentId = $parent->id;
		}

		$process = $mapped->getProcess();
		if ( $process ) {
			$mapped->processId = $process->id;
		}

		$updateBy = $mapped->getUpdateBy();
		if ( $updateBy ) {
			$mapped->updateById = $updateBy->id;
		}

		$createBy = $mapped->getCreateBy();
		if ( $createBy ) {
			$mapped->createById = $createBy->id;
		}

		$lockBy = $mapped->getLockBy();
		if ( $lockBy ) {
			$mapped->lockById = $lockBy->id;
		}

		$doctype = $mapped->getDoctype();
		if ( $doctype ) {
			$mapped->doctypeId = $doctype->id;
		}

		$category = $mapped->getCategory();
		if ( $category ) {
			$mapped->categoryId = $category->id;
		}

		parent::save($mapped, $options);
	} // End of function
}
