<?php
//%LICENCE_HEADER%

namespace Rbs\Ged;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbs\Dao\Factory as DaoFactory;
use Rbplm\Ged\Docfile\Version as DocfileVersion;

use Rbplm\Sys\Fsdata;
use Rbplm\Ged\Exception;
use Rbplm\Sys\Error;
use Rbplm\Signal;
use Rbplm\Ged\AccessCode;

/**
 * @brief Helper class for create history copy of ged components.
 *
 *
 */
class Historize
{

	/**
	 *
	 * @param Vault $vault
	 * @param DaoFactory $factory
	 */
	public function __construct($vault, $factory)
	{
		$this->vault = $vault;
		$this->factory = $factory;
	}

	/**
	 * Create a new docfile as a history copy of $headDocfile.
	 * Return new docfile.
	 *
	 * @param DocfileVersion $headDocfile
	 */
	public function depriveDocfileToIteration(DocfileVersion $headDocfile)
	{
		$docfile = clone($headDocfile);
		$docfile->newUid();
		$docfile->lock(AccessCode::ITERATION);
		$docfile->setLifeStage('ITERATION');
		$docfile->setOfDocfile($headDocfile);

		$record = clone($docfile->getData());
		$this->vault->depriveToIteration($record, $docfile->iteration);
		$docfile->setData($record);
		$dao = $this->factory->getDao(\Rbplm\Ged\Docfile\Iteration::$classId)->save($docfile);

		return $docfile;
	}

} //End of class

