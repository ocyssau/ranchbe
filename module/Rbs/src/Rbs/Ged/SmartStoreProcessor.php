<?php
namespace Rbs\Ged;

use Rbs\Extended;
use Rbs\Dao\Loader;
use Rbplm\Sys\Exception;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Doctype;
use Rbplm\Ged\AccessCode;
use Rbs\Ged\Document\History as DocumentHistory;
use Rbplm\Signal;

class SmartStoreProcessor
{

	/**
	 *
	 * @var array
	 */
	public $feedbacks = array();

	/**
	 *
	 * @var array
	 */
	public $errors = array();

	/**
	 *
	 * @var array
	 */
	public $actions = array();

	/**
	 *
	 * @var \Rbplm\Ged\Document\Version
	 */
	public $document;

	/**
	 *
	 * @var \Rbplm\Org\Container
	 */
	public $container;

	/**
	 * string path to wildspace
	 * @var string
	 */
	public $wildspacePath;

	/**
	 * @var int
	 */
	public $areaId;

	/**
	 *
	 * @var array
	 */
	public $actionDefinition = array(
		'create_doc' => 'Create a new document',
		'update_doc_file' => 'Update the document and files',
		'update_doc' => 'Update the document',
		'update_file' => 'Update the file only',
		'add_file' => 'Add the file to current document',
		'ignore' => 'Ignore',
		'upgrade_doc' => 'Create a new indice'
	);

	/**
	 *
	 * @var boolean
	 */
	protected $keepCo = false;

	/**
	 *
	 * @var boolean
	 */
	protected $suppressFile = true;

	/**
	 * @var \Rbs\Dao\Loader
	 */
	protected $loader;

	/**
	 *
	 * @var \Rbs\Space\Factory
	 */
	protected $factory;

	/**
	 *
	 * @param \Rbs\Dao\Loader $loader
	 * @param \Rbplm\Org\Container $container
	 */
	function __construct($loader, $container)
	{
		$this->loader = $loader;
		$this->factory = $loader->getFactory();
		$this->spaceName = $this->factory->getName();
		$this->container = $container;
	}

	/**
	 * @param \Rbplm\People\User\Wildspace|string $ws
	 */
	public function setWildspace($ws)
	{
		if($ws instanceof \Rbplm\People\User\Wildspace){
			$this->wildspacePath = $ws->getPath();
		}
		elseif(is_string($ws)){
			$this->wildspacePath = $ws;
		}
		$this->file = $this->wildspacePath . '/' . $this->fileName;
	}

	/**
	 *
	 * @param
	 *        	string
	 */
	public function setFile($file)
	{
		$this->file = $file;
	}

	/**
	 * Add Pdm properties to document
	 *
	 * @param array $inputElemt
	 */
	public function setPdmData($inputElmt)
	{
		$setter = new \Pdm\Input\PdmData\Setter($this->factory);
		$setter->setDocument($this->document, $inputElmt);
		return $this;
	}

	/**
	 * Determine available actions from file name
	 * and run many assertions
	 *
	 * Return a array with document, container, docfile... initialized
	 *
	 * @param array $files
	 */
	public function fromFile($fileName, $basePath)
	{
		$this->fileName = $fileName;
		$this->wildspacePath = $basePath;
		$this->file = $this->wildspacePath . '/' . $this->fileName; // file path from form

		$output = array(
			'feedback' => array(),
			'errors' => array(),
			'actions' => array(),
			'document' => null, // Document object
			'docfile' => null, // Docfile object
			'fsdata' => null, // Fsdata object
			'container' => null, // Container object
			'file' => '', // input file full path
			'spaceName' => ''
		);

		/**
		 * Liste of permits actions available for current $file
		 *
		 * @var array
		 */
		$availableActions = array(); // init var

		/**
		 * Returned errors
		 *
		 * @var array
		 */
		$errors = array();

		/**
		 * Returned messages
		 *
		 * @var array
		 */
		$feedbacks = array();

		$tmplist = array();

		$factory = $this->factory;
		$document = DocumentVersion::init();

		/* Set history listener */
		$history = DocumentHistory::init()->setDao($factory->getDao(DocumentHistory::$classId));
		Signal::connect($document, $document::SIGNAL_POST_CREATE, array(
			$history,
			'onEvent'
		));
		Signal::connect($document, $document::SIGNAL_POST_CHECKIN, array(
			$history,
			'onEvent'
		));

		$document->description = 'undefined';
		$docfile = DocfileVersion::init();
		$fsdata = new \Rbplm\Sys\Fsdata($this->file);

		$docfile->dao = $factory->getDao($docfile);
		$document->dao = $factory->getDao($document);

		/* Set fsdata property used by Document/Service to define the data to store */
		$docfile->fsdata = $fsdata;

		/* ---------- Check if file exist in wildspace */
		if ( !$fsdata ) {
			$errors[] = $this->file . ' is not existing.';
			$availableActions['ignore'] = 'Ignore';
			$output['file'] = $fileName;
			$output['errors'] = $errors;
			$output['actions'] = $availableActions;
			return $output;
		}

		/* LOAD DOCFILE */
		try {
			$docfile->dao->loadFromName($docfile, $fileName);
			$documentId = $docfile->getParent(true);
			$document->dao->loadFromId($document, $documentId);
			$containerId = $document->getParent(true);

			$docfileIsExisting = true;
			$documentIsExisting = true;
			$errors[] = 'file ' . $fileName . ' exist in this database. ID=' . $docfile->getId();
			$errors[] = 'file ' . $fileName . ' is linked to document ' . $document->getUid() . ' id=' . $document->getId();
			$output['docfile'] = $docfile;
			$output['document'] = $document;
		}
		catch( \Exception $e ) {
			$container = $this->container;
			$containerId = $container->getId();
			$docfileIsExisting = false;
			$documentIsExisting = false;
		}

		/* LOAD DOCUMENT FROM ROOT NAME */
		if ( $docfileIsExisting == false ) {
			try {
				$rootName = $fsdata->getRootname();
				$document->dao->loadFromNumber($document, $rootName);
				$containerId = $document->getParent(true);

				$documentIsExisting = true;
				$errors[] = 'document ' . $rootName . ' exist in this database. ID=' . $document->getId();
				$output['document'] = $document;
			}
			catch( \Exception $e ) {
				$container = $this->container;
				$containerId = $container->getId();
				$documentIsExisting = false;
			}
		}

		/* LOAD CONTAINER */
		if ( $this->container->getId() == $containerId ) {
			$container = $this->container;
		}
		else {
			try {
				$container = $factory->getModel(Workitem::$classId)->newUid();
				$container->dao = $factory->getDao($container);
				$container->dao->loadFromId($container, $containerId);
				$errors[] = 'Document is in Container ' . $container->getUid();
			}
			catch( \Exception $e ) {
				$container = $this->container;
			}
		}

		/* Extended properties */
		$extendsloader = new Extended\Loader($factory);
		$extendsloader->loadFromContainerId($containerId, $document->dao);

		/* DOCUMENT IS FREE */
		if ( $docfileIsExisting && $document->checkAccess() == AccessCode::FREE ) {
			/* Compare md5 */
			if ( $docfile->md5 == $fsdata->getMd5() ) {
				$md5IsEqual = true;
			}
			/* if document and docfile are co by current user, update is permit */
			$access = $docfile->checkAccess();
			if ( $access != AccessCode::FREE ) {
				$docfileIsFree = false;
				$errors[] = 'docfile ' . $docfile->getUid() . ' is not free ';
				if ( $access == AccessCode::CHECKOUT ) {
					$coUser = $docfile->getLockBy(true);
					$errors[] = 'file ' . $docfile->getUid() . ' is checkouted by ' . $coUser;
					if ( $coUser == $this->user->getId() ) {
						if ( $md5IsEqual ) {
							$errors[] = 'files ' . $fileName . ' from wildspace and from vault are strictly identicals';
							$availableActions['update_doc'] = 'Update the document metadas only';
						}
						else {
							$availableActions['update_doc_file'] = 'Update the document and file';
							$availableActions['upgrade_doc'] = 'Create a new indice';
						}
					}
				}
			}
		}

		// CREATE A NEW DOCUMENT
		if ( $documentIsExisting == false && $docfileIsExisting == false ) {
			// Generate the document number from the root name of the file
			$docUid = $fsdata->getRootname();
			$document->setUid($docUid . '.1');
			$document->setName($docUid);
			$document->setNumber($docUid);

			try {
				// Check the doctype for the new document
				$doctype = new Doctype();
				$doctype->dao = $factory->getDao($doctype);
				$fileExtension = $fsdata->getExtension();
				$fileType = $fsdata->getType();
				$doctype->dao->loadFromDocument($doctype, $document, $fileExtension, $fileType);
				$document->setDoctype($doctype);
				$feedbacks[] = 'doctype: ' . $doctype->getName();
			}
			catch( \Exception $e ) {
				$errors[] = 'This document has a bad doctype';
				unset($availableActions); // Reinit all actions
			}
			$availableActions['create_doc'] = 'Create a new document';
		}

		// DOCUMENT IS CHECKOUTED
		if ( $documentIsExisting && $document->checkAccess() == AccessCode::CHECKOUT ) {
			$errors[] = 'document ' . $document->getUid() . ' is checkouted';
		}

		// DOCUMENT IS NOT FREE
		if ( $documentIsExisting && $document->checkAccess() > AccessCode::CHECKOUT ) {
			$errors[] = 'document ' . $document->getUid() . ' is not free';
		}

		// DOCFILE IS CHECKOUTED
		if ( $docfileIsExisting && $docfile->checkAccess() == AccessCode::CHECKOUT && !$documentIsExisting ) {
			$errors[] = 'docfile ' . $docfile->getUid() . ' is checkouted';
		}

		// DOCFILE IS NOT FREE
		if ( $docfileIsExisting && $docfile->checkAccess() > AccessCode::CHECKOUT && !$documentIsExisting ) {
			$errors[] = 'docfile ' . $docfile->getUid() . ' is not free';
		}

		// ADD FILE TO EXISTING DOCUMENT
		if ( $documentIsExisting && $docfileIsExisting == false && $document->checkAccess() == AccessCode::FREE ) {
			$availableActions['add_file'] = 'Add this file to document';
		}

		// CREATE A NEW VERSION
		if ( $documentIsExisting && $docfileIsExisting && ($document->checkAccess() == AccessCode::FREE || $document->checkAccess() >= AccessCode::LOCKED) ) {
			$availableActions['upgrade_doc'] = 'Create a new indice of ' . $document->getUid();
		}

		// @todo: case files is twices with same root name = must be put in same document

		// Check double documents
		if ( in_array($document->getUid(), $tmplist) ) {
			$errors[] = $document->getUid() . ' is not unique in this package';
			$availableActions = array();
		}
		// Document is uniq in inputs
		else {
			$tmplist[] = $document->getUid();
		}

		// Add common actions
		$availableActions['ignore'] = 'Ignore';

		if ( !$docfile ) throw new \Exception('$docfile is not set');
		if ( !$container ) throw new \Exception('$container is not set');

		$document->setParent($container);
		$document->addDocfile($docfile);
		$document->spaceName = $this->spaceName;

		$this->document = $document;
		$this->fsdata = $fsdata;
		$this->feedbacks = $feedbacks;
		$this->errors = $errors;
		$this->actions = $availableActions;
		$this->action = $selectedAction;

		$output = array(
			'feedbacks' => $feedbacks,
			'errors' => $errors,
			'actions' => $availableActions,
			'actions' => $selectedAction,
			'document' => $document, // Document object
			'docfile' => $docfile, // Docfile object
			'fsdata' => $fsdata, // Fsdata object
			'container' => $container, // Container object
			'file' => $file, // input file full path
			'spaceName' => $this->spaceName
		);

		return $output;
	}

	/**
	 * validate the form
	 *
	 * @param string $todo
	 */
	public function run($todo)
	{
		switch ($todo) {
			case "ignore": // Ignore
				return true;
				break;
			case "update_doc": // Update the document only
				try {
					$document = $this->document;
					$document->dao->save($document);
					$this->feedbacks[] = 'the document =' . $this->document->getUid() . ' has been updated.';
				}
				catch( \Exception $e ) {
					$this->errors[] = 'Can not update the document = ' . $this->document->getUid();
					$this->errors[] = $e->getMessage();
					return false;
				}
				break;
			case "update_doc_file": // Update the document and the file if specified
				throw new \Exception('NOT IMPLEMENTED');
				break;
			case "update_file": // Update the associated file only
				throw new \Exception('NOT IMPLEMENTED');
				break;
			case "add_file": // Add the file to the document
				$service = new \Rbs\Ged\Document\Service($this->factory);
				$service->addFileToDocument($this->fsdata, $this->document);
				break;
			case "create_doc":
				$lock = false;

				/* Create a new document */
				$service = new \Rbs\Ged\Document\Service($this->factory);
				$service->createDocument($this->document, $lock);

				/* Create a new product */
				if ( isset($this->document->product) ) {
					$this->document->product->setDocument($this->document);
					$service = new \Rbs\Pdm\Product\Service($this->factory);
					$service->create($this->document->product, $lock);
				}

				break;
			case "upgrade_doc": // Create a new indice and update the files if specifed
				$service = new \Rbs\Ged\Document\Service($this->factory);
				$service->upgradeDocument($this->document);
				break;
		} // End of switch
		return true;
	}

	/**
	 *
	 * @param boolean $bool
	 */
	public function setKeepCo($bool)
	{
		$this->keepCo = $bool;

		if ( $this->keepCo == true ) {
			$this->suppressFile = false;
		}
		else {
			$this->suppressFile = true;
		}
	}
} //End of class
