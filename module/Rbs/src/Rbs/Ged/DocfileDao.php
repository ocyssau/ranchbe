<?php
// %LICENCE_HEADER%
namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/**
 * SQL_SCRIPT>>
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * <<
 */

/**
 * @brief Dao class for \Rbplm\Ged\Docfile
 *
 * See the examples: Rbplm/Ged/DocfileTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\DocfileTest
 *
 */
class DocfileDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = '';

	/**
	 *
	 * @var unknown_type
	 */
	public static $sysToApp = array();

	/**
	 * Getter for versions.
	 * Return a list.
	 *
	 * @param
	 *        	\Rbplm\Dao\MappedInterface
	 * @param
	 *        	boolean
	 * @return \Rbplmpg\Dao\DaoList
	 */
	public function getVersions($mapped)
	{}
} //End of class

