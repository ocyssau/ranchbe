<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Docfile;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends DaoSier
{

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'file_id'=>'id',
		'uid'=>'uid',
		'document_id'=>'parentId',
		'file_name'=>'name',
		'file_version'=>'iteration',
		'file_access_code'=>'accessCode',
		'file_checkout_by'=>'lockById',
		'file_checkout_date'=>'locked',
		'file_open_date'=>'created',
		'file_open_by'=>'createById',
		'file_update_date'=>'updated',
		'file_update_by'=>'updateById',
		'file_state'=>'lifeStage',
		'file_path'=>'path',
		'file_root_name'=>'rootname',
		'file_extension'=>'extension',
		'file_type'=>'type',
		'file_size'=>'size',
		'file_mtime'=>'mtime',
		'file_md5'=>'md5',
		'mainrole'=>'mainrole',
		'roles'=>'roles',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'file_open_date'=>'date',
		'file_checkout_date'=>'date',
		'file_update_date'=>'date',
		'roles'=>'json',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function save(MappedInterface $mapped)
	{
		$parent = $mapped->getParent();
		if ($parent){
			$mapped->parentId = $parent->getId();
		}

		$updateBy = $mapped->getUpdateBy();
		if ($updateBy){
			$mapped->updateById = $updateBy->getId();
		}

		$createBy = $mapped->getCreateBy();
		if ($createBy){
			$mapped->createById = $createBy->getId();
		}

		$lockBy = $mapped->getLockBy();
		if ($lockBy){
			$mapped->lockById = $lockBy->getId();
		}

		$record = $mapped->getData();
		$mapped->name = $record->name;
		$mapped->extension = $record->extension;
		$mapped->fullpath = $record->fullpath;
		$mapped->md5 = $record->md5;
		$mapped->mtime = $record->mtime;
		$mapped->path = $record->path;
		$mapped->rootname = $record->rootname;
		$mapped->size = $record->size;
		$mapped->type = $record->type;

		parent::save($mapped);
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\Ged\Docfile\Version    	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		$record = $mapped->getData();
		$properties = array();

		if($fromApp){
			$mapped->hydrate($row);
			$record->hydrate($row);
		}
		else{
			$sysToAppFilter = $this->metaModelFilters;
			foreach($this->metaModel as $asSys=>$asApp){
				if(isset($sysToAppFilter[$asSys])){
					$filterMethod = $sysToAppFilter[$asSys].'ToApp';
					$properties[$asApp] = static::$filterMethod($row[$asSys]);
				}
				else{
					$properties[$asApp] = $row[$asSys];
				}
			}

			$properties['fullpath']	= $properties['path'].'/'.$properties['rootname'].$properties['extension'];

			$mapped->hydrate($properties);
			$record->hydrate($properties);
		}

		/*
		$record->setId($row['file_id']);
		$record->setUid($row['file_id']);
		$record->setName($row['file_root_name'].$row['file_extension']);
		$record->created = $row['file_open_date'];
		$record->extension 	= $row['file_extension'];
		$record->path 		= $row['file_path'];
		$record->rootname 	= $row['file_root_name'];
		$record->fullpath 	= $row['file_path'].'/'.$row['file_root_name'].$row['file_extension'];
		$record->size 		= $row['file_size'];
		$record->mtime 		= $row['file_mtime'];
		$record->type 		= $row['file_type'];
		$record->md5 		= $row['file_md5'];
		$mapped->uid = $mapped->id;
		*/
	}

	/**
	 * @see DaoInterface::loadFromName()
	 */
	public function loadFromName(MappedInterface $mapped, $name, $versionId=null)
	{
		if($versionId){
			$filter = 'file_name=:name AND file_version=:versionId LIMIT 1';
			$bind = array(':name'=>$number, ':versionId'=>$versionId);
		}
		else{
			$filter = "file_name=:name ORDER BY file_version DESC LIMIT 1";
			$bind = array(':name'=>$number);
		}
		return $this->load($mapped, $filter, $bind);
	}

	/**
	 *
	 * @param integer $documentId
	 * @return \PDOStatement
	 */
	public function getFromParent($parentId)
	{
		$table = $this->_table;
		$sql = "SELECT * FROM $table WHERE document_id=:documentId";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		$bind = array(':documentId'=>$parentId);
		$stmt->execute($bind);
		return $stmt;
	}

	/**
	 * @param \Rbplm\Ged\Document\Version
	 */
	public function loadDocfiles($document, $filter='1=1', $bind=array())
	{
		$table = $this->_table;
		$sql = "SELECT * FROM $table WHERE $filter AND document_id=:documentId ORDER BY mainrole,document_id ASC";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		$bind[':documentId'] = $document->getId();
		$stmt->execute($bind);

		while($entry = $stmt->fetch()){
			$docfile = new \Rbplm\Ged\Docfile\Version();
			$this->hydrate($docfile, $entry);
			$docfile->dao = $this;
			$document->addDocfile($docfile);
		}
		return $this;
	}

}
