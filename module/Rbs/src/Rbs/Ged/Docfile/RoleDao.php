<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Docfile;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 CREATE TABLE ged_docfile_role(
 name varchar(255),
 file_uid uuid NOT NULL,
 role_id integer,
 description text
 );

 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (52, '\Rbplm\Ged\Docfile\Role', 'ged_docfile_role');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE ged_docfile_role ADD PRIMARY KEY (role_id);
 CREATE INDEX INDEX_ged_docfile_role_name ON ged_docfile_role USING btree (name);
 CREATE INDEX INDEX_ged_docfile_role_file_uid ON ged_docfile_role USING btree (file_uid);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE ged_docfile_role ADD FOREIGN KEY (file_uid) REFERENCES ged_docfile_version (uid) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/**
 * @brief Dao class for \Rbplm\Ged\Docfile_Role
 *
 * See the examples: Rbplm/Ged/Docfile/RoleTest.php
 *
 * @see \Rbplmpg\Dao
 * @see \Rbplm\Ged\Docfile_RoleTest
 *
 */
class RoleDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'ged_docfile_role';
	public static $vtable = 'ged_docfile_role';
	public static $sequenceName = 'workitem_doc_files_seq';

	public static $sysToApp = array(
		'name'=>'name',
		'file_uid'=>'fileId',
		'role_id'=>'roleId',
		'description'=>'description'
	);

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbplm\Ged\Docfile_Role	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		if($fromApp){
			$mapped->setName($row['name']);
			$mapped->setFileId($row['fileId']);
			$mapped->setRoleId($row['roleId']);
			$mapped->setDescription($row['description']);
		}
		else{
			$mapped->setName($row['name']);
			$mapped->setFileId($row['file_uid']);
			$mapped->setRoleId($row['role_id']);
			$mapped->setDescription($row['description']);
		}
	}

	/**
	 * @param \Rbplm\Ged\Docfile_Role   $mapped
	 * @return void
	 * @throws \Rbplm\Ged\Exception
	 *
	 */
	private function __NOTUSE_saveObject($mapped)
	{
		$table = $this->_table;
		$sql = "INSERT INTO $table (name,file_uid,role_id,description) VALUES (:name,:fileId,:roleId,:description)";
		$bind = array(
			':name'=>$mapped->getName(),
			':fileId'=>$mapped->getFileId(),
			':roleId'=>$mapped->getRoleId(),
			':description'=>$mapped->getDescription()
		);
		$stmt = $this->_connexion->prepare($sql);
		$stmt->execute($bind);
	}


} //End of class

