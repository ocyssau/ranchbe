<?php
namespace Rbs\Ged\Docfile;

use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Docfile\Iteration as DocfileIteration;
use Rbplm\Ged\AccessCode;
use Rbs\Ged\Historize;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Ged\AccessCodeException;
use Rbplm\People\CurrentUser;
use Rbplm\People\User\Wildspace;
use Rbplm\Signal;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;

class Service
{
	protected $factory;

	/**
	 *
	 * @param Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * @param DocfileVersion $docfile
	 * @throws Exception
	 * @return \Rbs\Ged\Docfile\Service
	 *
	 */
	public function create($docfile, $vault)
	{
		$factory = $this->factory;
		$wilspace = Wildspace::get(CurrentUser::get());

		if(!isset($docfile->fsdata)){
			throw new Exception('$docfile->fsdata is not set; Set fsdata with the fsdata to store in vault');
		}

		$fsdata = $docfile->fsdata;

		if(!isset($docfile->fsdata)){
			throw new \Exception('$docfile->fsdata is not set');
		}

		$docfile->setName($fsdata->getName());
		$docfile->setUid($docfile->getName().'.'.$docfile->version);

		$record = $vault->record(Record::init(), $fsdata, $docfile->getName());
		$docfile->setData($record);
		$dfDao = $factory->getDao($docfile);
		$dfDao->save($docfile);

		try{
			$fsdata->delete();
		}
		catch(\Exception $e){
			$this->errors[] = $e->getMessage();
			null;
		}

		return $this;
	}

	/**
	 *
	 * @param DocfileVersion $docfile
	 * @param bool $replace			replace file in ws
	 * @param bool $getFiles		copy file in ws
	 * @throws Exception
	 * @return \Rbs\Ged\Docfile\Service
	 */
	public function checkout($docfile, $replace=true, $getFiles=true)
	{
		$factory = $this->factory;
		$wilspace = Wildspace::get(CurrentUser::get());

		/* check if access is free, valide codes : 0-1 */
		$aCode = $docfile->checkAccess();
		if($aCode <> AccessCode::FREE){
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION accessCode='.$aCode, Error::ERROR, array('accessCode'=>$aCode));
		}
		else{
			$docfile->lock(AccessCode::CHECKOUT, CurrentUser::get());
		}

		/* Notiy observers on event */
		Signal::trigger($docfile::SIGNAL_PRE_CHECKOUT, $docfile);

		/* get list of associated files and checkout */
		if($getFiles){
			$data = $docfile->getData();
			$wsFile = $wilspace->getPath().'/'.$data->getName();
			/* COPY TO WS */
			if( is_file($wsFile) || is_dir($wsFile) ){
				if($replace){
					$data->getFsdata()->copy($wsFile, $mode=0766, true);
				}
			}
			else{
				$data->getFsdata()->copy($wsFile, $mode=0766, false);
			}
		}

		$docfile->dao->save($docfile);

		Signal::trigger($docfile::SIGNAL_POST_CHECKOUT, $docfile);

		return $this;
	}

	/**
	 * Replace a file checkout in the vault and unlock it.
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 * If the file has been changed(check by md5 code comparaison), create a new iteration
	 * $docfile->fsdata must be set for update data in vault from this fsdata.
	 *
	 * @param Record $docfile			The docfile to update. Must have a fsdata property to update data.
	 * @param boolean $releasing		if true, release the docfile after replace
	 * @param boolean $updateData		if true, the file in vault will be replaced by new data
	 * @param boolean $checkAccess		if true, check if access code is right
	 * @throws Exception
	 * @return Version
	 */
	public function checkin($docfile, $releasing=true, $updateData=true, $checkAccess=true)
	{
		$factory = $this->factory;

		$aCode = $docfile->checkAccess();
		$coBy = $docfile->lockById;
		if( ($aCode!=1 OR $coBy!=CurrentUser::get()->getId()) AND $checkAccess)
		{
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION. Code='.$aCode.' by '.$coBy, Error::ERROR, array($aCode));
		}

		Signal::trigger($docfile::SIGNAL_PRE_CHECKIN, $this);

		/* Update data vault */
		if($updateData){
			if(!isset($docfile->fsdata)){
				throw new Exception('$docfile->fsdata is not set. Is must be set with the new fsdata');
			}

			$container = $docfile->getParent()->getParent();
			$reposit = Reposit::init($container->path);
			$vault = new Vault($reposit, $factory->getDao(Record::$classId));
			$historize = new Historize($vault, $factory);

			$fsdata = $docfile->fsdata;

			/* If md5 are equals, dont upate files, dont increment iteration */
			if( $fsdata->getMd5() == $docfile->getData()->md5 ){
				if($releasing){
					$docfile->unLock();
					$docfile->dao->save($docfile);
				}
			}
			else{
				/* Copy old data as iteration */
				$historize->depriveDocfileToIteration($docfile, $docfile->iteration);

				/* Put new data in vault and attach to docfile */
				$record = $vault->record($docfile->getData(), $fsdata, $docfile->getName());
				$docfile->setData($record);
				$docfile->iteration = $docfile->iteration+1;

				/* Set the iteration id of document to the max iteration of docfiles */
				if($docfile->getParent()->iteration < $docfile->iteration){
					$docfile->getParent()->iteration = $docfile->iteration;
				}
				if($releasing){
					$docfile->unLock();
				}
				$docfile->dao->save($docfile);
			}
		}
		/* Reset docfile */
		else{
			if($releasing){
				$docfile->unLock();
				$docfile->dao->save($docfile);
			}
		}

		Signal::trigger($docfile::SIGNAL_POST_CHECKIN, $this);
		return $this;
	}

	/**
	 *
	 * @param DocfileVersion $docfile
	 * @param boolean $releasing
	 * @param boolean $deleteFileInWs
	 * @param boolean $checkAccess
	 */
	public function checkinFromWs($docfile, $releasing=true, $updateData=true, $deleteFileInWs=true, $checkAccess=false)
	{
		/* Get fsdata in wildspace */
		$wildspace = Wildspace::get(CurrentUser::get());
		$fileName = $docfile->getData()->getName();
		$wsFile = $wildspace->getPath().'/'.$fileName;

		if(is_file($wsFile) || is_dir($wsFile)){
			$fsdata = new Fsdata($wsFile);
			$docfile->fsdata = $fsdata;
			$this->checkin($docfile, $releasing, $updateData, $checkAccess);

			if($releasing==true && $deleteFileInWs==true){
				$fsdata->delete();
			}
		}
		else{
			throw new Exception('Unable to find file %s', Error::ERROR, array($wsFile));
		}
	}

	/**
	 * @param DocfileVersion $docfile
	 * @throws Exception
	 * @return Service
	 */
	function delete(DocfileVersion $docfile, $withfiles=false, $withiterations=false)
	{
		$factory = $this->factory;
		$dao = $factory->getDao($docfile);

		/* Init Record and Fsdata */
		$record = $docfile->getData();
		$fsdata = $record->getFsdata();

		/* Check if access is free */
		$access = $docfile->checkAccess();
		if(!($access==AccessCode::FREE || $access==AccessCode::SUPPRESS || $access >= 100) || $access==AccessCode::HISTORY){
			throw new AccessCodeException('Docfile %element% is locked with code %element1%, suppress is not permit', ERROR, array('element'=>$docfile->getUid(),'element1'=>$access));
		}

		/* Delete record of associated files */
		$dao->deleteFromId($docfile->getId());

		if($withfiles === true){ /* Delete the file if $withfiles = true */
			if( $fsdata->isExisting() ){ /* Delete the file if $withfiles = true */
				if( !$fsdata->putInTrash() ){
					throw new Exception("Can not delete file ".$fsdata->getFullpath());
				}
			}
		}

		/* Delete the iterations of file */
		if($withiterations){
			/* Get list of associated files and check access */
			try{
				$iterationList = $factory->getList(DocfileIteration::$classId);
				$iterationDao = $factory->getDao(DocfileIteration::$classId);
				$filter = $iterationDao->toSys('parentId').'='.$docfile->getId();
				$iterationList->load($filter);

				foreach($iterationList as $entry){
					$iteration = new DocfileIteration();
					$iterationDao->hydrate($iteration, $entry);
					$this->delete($iteration, true, false);
				}
			}
			catch(\Exception $e){
				throw $e;
			}
		}

		return $this;
	}

	/**
	 * @return boolean
	 */
	public function updateFile($docfile, $fsdata)
	{
		$document = $docfile->getParent();
		$container = $document->getParent();

		$reposit = Reposit::init($container->path);
		$vault = new Vault($reposit, $factory->getDao(Record::$classId));

		$aCode = $document->checkAccess();
		$coBy = $document->lockById;
		if( !($aCode == AccessCode::FREE OR ($aCode==AccessCode::CHECKOUT AND $coBy==CurrentUser::get()->getId())) ){
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
		}

		$aCode = $docfile->checkAccess();
		$coBy = $docfile->lockById;
		if( !($aCode == AccessCode::FREE OR ($aCode==AccessCode::CHECKOUT AND $coBy==CurrentUser::get()->getId())) ){
			throw new AccessCodeException('LOCKED_TO_PREVENT_THIS_ACTION', Error::ERROR, array('access_code'=>$aCode));
		}

		$oldMd5 = $docfile->getData()->md5;
		$newMd5 = $fsdata->md5;
		if($oldMd5 == $newMd5){
			$feedback[] = 'New file And Old File are identicals, update is not perfomed';
		}
		else{
			/* Deprive old file */
			$oldrecord = $this->docfile->getData();
			$docfileIteration->setData($oldrecord);
			$vault->depriveToIteration($oldrecord, $docfile->iteration);

			/* Put new file in vault */
			$newfsdata = $this->fsdata;
			$newRecord = Record::init();
			$vault->record($newRecord, $newfsdata, $docfile->getName());

			/* Set iteration docfile */
			$docfileIteration = Historize::depriveDocfileToIteration($docfile);
			$docfile->iteration = $docfile->iteration+1;
			$docfile->setData($newRecord);

			/* save */
			$this->factory->getDao($docfile)->save($docfile);
			$this->factory->getDao(\Rbplm\Ged\Docfile\Iteration::$classId)->save($docfileIteration);
		}
		return $docfileIteration;
	}

	/**
	 * @param DocfileVersion $docfile
	 * @throws Exception
	 * @return Service
	 */
	function restoreVersion(DocfileVersion $docfile)
	{
		$factory = $this->factory;
		$dao = $factory->getDao($docfile);

		/* get container */
		$container = $docfile->getParent()->getParent();
		$reposit = Reposit::init($container->path);

		/* init Record and Fsdata */
		$record = $docfile->getData();

		Historize::restoreRecordToHeadline($reposit, $record);
	}
}

