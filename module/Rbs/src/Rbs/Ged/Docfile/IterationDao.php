<?php
//%LICENCE_HEADER%

namespace Rbs\Ged\Docfile;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends VersionDao
{
	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'file_id'=>'id',
		'uid'=>'uid',
		'document_id'=>'parentId',
		'of_file_id'=>'ofDocfileId',
		'file_name'=>'name',
		'file_version'=>'iteration',
		'file_access_code'=>'accessCode',
		'file_checkout_by'=>'lockById',
		'file_checkout_date'=>'locked',
		'file_open_date'=>'created',
		'file_open_by'=>'createById',
		'file_update_date'=>'updated',
		'file_update_by'=>'updateById',
		'file_state'=>'lifeStage',
		'file_path'=>'path',
		'file_root_name'=>'rootname',
		'file_extension'=>'extension',
		'file_type'=>'type',
		'file_size'=>'size',
		'file_mtime'=>'mtime',
		'file_md5'=>'md5',
		'mainrole'=>'mainrole',
		'roles'=>'roles',
	);
}
