<?php
namespace Rbs\Ged;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Docfile\Iteration as DocfileIteration;
use Rbplm\Vault\Vault;


/**
 * @author olivier
 *
 */
class Archiver
{

	/**
	 *
	 * @var document
	 */
	protected $document;


	/**
	 *
	 * @var string
	 */
	protected $repositFolder;


	/**
	 *
	 * @var string
	 */
	public static $state = 'archived';

	/**
	 * Value of the offset to apply to access_code
	 *
	 * @var integer
	 */
	public static $accessCode = 100;

	/**
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * Get the archive reposit from the container
	 *
	 * @param Workitem $container
	 * @param string $spaceName
	 */
	public function getReposit($container)
	{
		$spaceName = $this->factory->getName();
		$basePath = \Ranchbe::getConfig('default.archive.'.$spaceName.'.dir');
		$wiName = $container->getUid();
		$path = $basePath.'/'.$wiName;
		$reposit = \Rbs\Vault\Reposit\Archive::init($path);
		return $reposit;
	}

	/**
	 * All Docfiles of document are move in archiving reposit.*
	 * Set the accessCode to current access code of docfile + self::$accessCode value
	 *
	 * The docfiles must be loaded in document before.
	 * The parent container must be loaded in document before.
	 * The document->dao must be set
	 *
	 * If $cleaniterations=true, delete iterations files
	 *
	 * @param DocumentVersion $document
	 * @param bool $cleaniterations
	 *
	 */
	public function archive($document, $cleaniterations=true)
	{
		$container = $document->getParent();
		$docfiles = $document->getDocfiles();
		$archiveReposit = $this->getReposit($container);
		$factory = $this->factory;

		$vault = new Vault($archiveReposit);

		foreach($docfiles as $docfile){
			if($docfile->checkAccess() > self::$accessCode ){
				continue;
			}

			$archivedRecord = $vault->depriveToArchive($docfile->getData(), $archiveReposit, $docfile->version);
			$docfile->setData($archivedRecord);
			$docfile->lock($docfile->checkAccess() + self::$accessCode);
			$docfile->dao->save($docfile);

			if($cleaniterations){ //Delete the iterations of file
				//get list of associated files and check access
				try{
					$iterationList = $factory->getList(DocfileIteration::$classId);
					$iterationDao = $factory->getDao(DocfileIteration::$classId);
					$filter = $iterationDao->toSys('parentId').'='.$docfile->getId();
					$iterationList->load($filter);

					foreach($iterationList as $entry){
						$iteration = new DocfileIteration();
						$iterationDao->hydrate($iteration, $entry);
						$this->delete($iteration, true, false);
					}
				}
				catch(\Exception $e){
					throw $e;
				}
			}
		}

		$accessCode = $document->checkAccess() + self::$accessCode;
		$document->lock($accessCode);
		$document->dao->save($document);
		return $this;
	}

	/**
	 * @param container $container
	 * @return string
	 *
	 */
	public function getFolder(container $container)
	{
	}
}
