<?php
//%LICENCE_HEADER%

namespace Rbs\Ged;

use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class CategoryDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitem_categories';
	public static $vtable='workitem_categories';
	public static $sequenceName='workitem_categories_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'category_id'=>'id',
		'category_number'=>'name',
		'category_description'=>'description',
		'category_icon'=>'icon',
	);
}
