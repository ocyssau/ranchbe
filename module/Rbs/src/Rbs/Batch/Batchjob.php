<?php
//%LICENCE_HEADER%

require_once('./class/common/basic.php');

/*
 DROP TABLE `batchjob`;
 CREATE TABLE `batchjob` (
 `id` int(11) NOT NULL default '0',
 `name` varchar(258) default NULL,
 `runner` varchar(258) NOT NULL,
 `submit` int(11) NOT NULL,
 `submit_by` int(11) default NULL,
 `planned` int(11) NOT NULL,
 `state` varchar(32)  NOT NULL default 'init',
 `data` text,
 `start` int(11) NOT NULL,
 `end` int(11) NOT NULL,
 `duration` int(11) NOT NULL,
 `hashkey` varchar(40) NOT NULL,
 PRIMARY KEY  (`id`),
 INDEX `I_batchjob_1` (`planned`),
 INDEX `I_batchjob_2` (`state`),
 INDEX `I_batchjob_3` (`runner`(64))
 ) ENGINE=InnoDB ;
 
ALTER TABLE `batchjob` ADD UNIQUE (`hashkey`);
 */

/*
ALTER TABLE `batchjob` ADD INDEX `I_batchjob_3` ( `runner` ( 64 ) ) 
 */

/**
 * @brief Job to execute in batch mode.
 *
 */
class Rbplm_Batchjob extends basic
{
	protected $_id = null; //(Integer)
	protected $_usr = null; //(Object)
	protected $dbranchbe = null; //(Object)
	
	protected $_planned = null;
	protected $_name = null;
	protected $_runner = null;
	protected $_data = null;
	protected $_state = null;
	protected $_submit = null;
	protected $_submitBy = null;
	protected $_start = null;
	protected $_end = null;
	protected $_duration = 0;
	protected $_hashkey = null;
	
	const STATE_INIT = 'init';
	const STATE_SUCCESS = 'success';
	const STATE_FAILED = 'failed';
	const STATE_WARNING = 'warning';
	const STATE_RUNNING = 'running';
	const STATE_UNCOMPLETE = 'uncomplete';
	
	const RUNNER_FILEIMPORT = 'file_import';
	
	function __construct( $properties = array() )
	{
		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();
		
		$this->OBJECT_TABLE  = 'batchjob';

		$this->FIELDS_MAP_ID = 'id';
		$this->FIELDS_MAP_NUM = 'name';
		$this->FIELDS_MAP_DESC = '';
		$this->FIELDS_MAP_STATE = 'state';
		
		if($properties){
			$this->loadFromArray($properties);
		}
	}//End of method
	
	/**
	 * 
	 */
	public function __get($name)
	{
		$name = '_' . $name;
		if( isset($this->$name) ){
			return $this->$name;
		}
	}
	

	/**
	 *
	 * @param array $params
	 */
	function create()
	{
		$datas['state'] = 'init';
		$datas['submit'] = time();
		$datas['submit_by'] = $this->_usr->getid();
		$datas['planned'] = $this->_planned;
		$datas['name'] = $this->_name;
		$datas['runner'] = trim($this->_runner);
		$datas['data'] = serialize($this->_data);
		$datas['hashkey'] = md5($datas['data'].$datas['runner']);
		
		$this->_hashkey = $datas['hashkey'];
		$this->_id = $this->BasicCreate($datas);
		
		if( !$this->_id ){
			return false;
		}
		else{
			$this->_state = $datas['state'];
			$this->_submit = $datas['submit'];
			$this->_submitBy = $datas['submit_by'];
			return $this->_id;
		}
	}//End of method

	/**
	 *
	 * @param array $params
	 */
	function update()
	{
		$datas['state'] = $this->_state;
		$datas['planned'] = $this->_planned;
		$datas['name'] = trim($this->_name);
		$datas['runner'] = trim($this->_runner);
		$datas['data'] = serialize($this->_data);
		$datas['hashkey'] = md5($datas['data'].$datas['runner']);
		$datas['start'] = $this->_start;
		$datas['end'] = $this->_end;
		$datas['duration'] = $this->_duration;
		$this->_hashkey = $datas['hashkey'];
		return $this->BasicUpdate($datas, $this->_id);
	}//End of method

	/**
	 * @param array $params
	 */
	function suppress($id)
	{
		return $this->BasicSuppress($id);
	}//End of method
	
	/**
	 * @param array $params
	 */
	function getAll( $params )
	{
		return $this->GetAllBasic($params);
	}//End of method
	
	
	/**
	 * @param array $params
	 */
	function loadFromArray($infos)
	{
		$this->_id = $infos['id'];
		$this->_name = $infos['name'];
		$this->_planned = $infos['planned'];
		$this->_runner = $infos['runner'];
		$this->_state = $infos['state'];
		$this->_submitBy = $infos['submit_by'];
		$this->_submit = $infos['submit'];
		$this->_start = $infos['start'];
		$this->_end = $infos['end'];
		$this->_duration = $infos['duration'];
		$this->_hashkey = $infos['hashkey'];
		if($infos['data']){
			$this->setData( $infos['data'] );
		}
	}//End of method
	
	
	function getProperties()
	{
		foreach($this as $pname=>$val){
			$pname = trim($pname, '_');
			if( !is_object($val) ){
				$ret[$pname] = $val;
			}
			else{
				//$ret[$pname] = '[link]';
			}
		}
		return $ret;
	}
	

	/**
	 * @param integer
	 */
	function setPlanned($date)
	{
		$this->_planned = $date;
	}//End of method
	
	/**
	 * @param string
	 */
	function setName($str)
	{
		$this->_name = $str;
	}//End of method
	
	/**
	 * @param string
	 */
	function setRunner($str)
	{
		$this->_runner = $str;
	}//End of method
	
	/**
	 * @param array|string
	 */
	function setData($mixed)
	{
		if(is_array($mixed)){
			$this->_data = $mixed;
		}
		else if(is_string($mixed)){
			$this->_data = unserialize($mixed);
		}
	}//End of method
	
	/**
	 * @param string
	 */
	function setState($str)
	{
		$this->_state = $str;
	}//End of method
	
	/**
	 * @param integer
	 */
	function setStart($int)
	{
		$this->_start = $int;
	}//End of method
	
	/**
	 * @param integer
	 */
	function setEnd($int)
	{
		$this->_end = $int;
		$this->_duration = $this->_end - $this->_start;
	}//End of method
	
	
	/**
	 * 
	 */
	function getProperty($pname){
		$pname = '_' . $pname;
		return $this->$pname;
	}
	
	
} //End of class
