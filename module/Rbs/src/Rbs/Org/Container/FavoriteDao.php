<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Container;

use Rbplm\Sys\Exception;
use Rbplm\Dao\NotExistingException;

/** SQL_SCRIPT>>
 CREATE TABLE `container_favorite` (
 	`user_id` int(11) NOT NULL,
 	`container_id` int(11) NOT NULL,
 	`container_number` varchar(512) NOT NULL,
 	`space_id` int(11) NOT NULL,
 	`space_name` varchar(128) NOT NULL,
 	PRIMARY KEY  (`user_id`, `container_id`, `space_id`)
 ) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class FavoriteDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table='container_favorite';

	/**
	 * @var string
	 */
	public static $vtable='container_favorite';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'user_id'=>'ownerId',
		'container_id'=>'containerId',
		'container_number'=>'containerNumber',
		'space_id'=>'spaceId',
		'space_name'=>'spaceName'
	);

	/**
	 * Return list of containers
	 * @return array key=>container_id
	 * @param integer $userId
	 */
	public function getFromUserId($userId)
	{
		try{
			$select = array('*');
			$filter = 'user_id=:userId';
			$bind = array(':userId'=>$userId );
			$stmt = $this->query($select, $filter, $bind);
			return $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}
		catch(NotExistingException $e){
		}
	}

	/**
	 * Add a index to user box
	 * @return boolean
	 */
	public function add($bind)
	{
		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->addStmt){
			$table = static::$table;
			$sysToApp = $this->metaModel;

			foreach($sysToApp as $asSys=>$asApp)
			{
				$sysSet[] = $asSys;
				$appSet[] = ':'.$asApp;
			}
			$sql = 'INSERT INTO ' . $table . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
			$this->addStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->addStmt->execute($bind);
			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function remove($userId, $contId, $spaceId)
	{
		$filter = 'user_id=:userId AND container_id=:containerId AND space_id=:spaceId';
		$bind = array(
			':userId'=>$userId,
			':containerId'=>$contId,
			':spaceId'=>$spaceId,
		);
		return $this->_deleteFromFilter($filter, $bind, false, true);
	}

	/**
	 * @return boolean
	 */
	public function clean($userId)
	{
		$filter = 'user_id=:userId';
		$bind = array(
			':userId'=>$userId,
		);
		return $this->_deleteFromFilter($filter, $bind, false, true);
	}

}
