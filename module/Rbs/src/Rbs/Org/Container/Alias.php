<?php 
namespace Rbs\Org\Container;

use Rbplm\Any;
use Rbplm\Dao\MappedInterface;

class Alias extends Any implements MappedInterface
{
	use \Rbplm\Mapped;

	public static $classId = '45c84a5zalias';

	public $aliasOfId;
	public $description;

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['aliasOfId'])) ? $this->aliasOfId=$properties['aliasOfId'] : null;
		(isset($properties['description'])) ? $this->description=$properties['description'] : null;
	}
}
