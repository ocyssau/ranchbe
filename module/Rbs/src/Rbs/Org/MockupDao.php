<?php
//%LICENCE_HEADER%

namespace Rbs\Org;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * Example and tests: Rbplm/Org/UnitTest.php
 */
class MockupDao extends WorkitemDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'mockups';
	public static $vtable = 'mockups';
	public static $sequenceName = 'mockups_seq';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'mockup_id'=>'id',
		'mockup_number'=>'name',
		'mockup_description'=>'description',
		'mockup_state'=>'status',
		'mockup_indice_id'=>'versionId',
		'project_id'=>'parentId',
		'default_file_path'=>'repositPath',
		'default_process_id'=>'processId',
		'forseen_close_date'=>'forseenCloseDate',
		'close_date'=>'closed',
		'open_date'=>'created',
		'open_by'=>'createById',
		'alias_id'=>'aliasId',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'forseen_close_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
	);
}
