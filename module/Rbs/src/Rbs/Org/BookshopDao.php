<?php
//%LICENCE_HEADER%

namespace Rbs\Org;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class BookshopDao extends WorkitemDao
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'bookshops';
	public static $vtable = 'bookshops';
	public static $sequenceName = 'bookshops_seq';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'bookshop_id'=>'id',
		'bookshop_number'=>'name',
		'bookshop_description'=>'description',
		'bookshop_state'=>'status',
		'bookshop_indice_id'=>'versionId',
		'project_id'=>'parentId',
		'default_file_path'=>'repositPath',
		'default_process_id'=>'processId',
		'forseen_close_date'=>'forseenCloseDate',
		'close_date'=>'closed',
		'open_date'=>'created',
		'open_by'=>'createById',
		'alias_id'=>'aliasId',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'forseen_close_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
	);
}
