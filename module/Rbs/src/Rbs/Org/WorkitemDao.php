<?php
//%LICENCE_HEADER%

namespace Rbs\Org;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 CREATE TABLE `workitems` (
 `workitem_id` int(11) NOT NULL DEFAULT '0',
 `workitem_number` varchar(16) NOT NULL DEFAULT '',
 `name` VARCHAR( 128 ) DEFAULT NOT NULL DEFAULT '',
 `uid` VARCHAR( 128 ) DEFAULT NOT NULL DEFAULT '',
 `workitem_state` varchar(16) NOT NULL DEFAULT 'init',
 `workitem_description` varchar(128) DEFAULT NULL,
 `file_only` tinyint(1) NOT NULL DEFAULT '0',
 `access_code` int(11) DEFAULT NULL,
 `workitem_indice_id` int(11) DEFAULT NULL,
 `project_id` int(11) DEFAULT NULL,
 `default_process_id` int(11) DEFAULT NULL,
 `default_file_path` text NOT NULL,
 `open_date` int(11) DEFAULT NULL,
 `open_by` int(11) DEFAULT NULL,
 `forseen_close_date` int(11) DEFAULT NULL,
 `close_date` int(11) DEFAULT NULL,
 `close_by` int(11) DEFAULT NULL,
 `container_type` varchar(10) NOT NULL DEFAULT 'workitem',
 PRIMARY KEY (`workitem_id`),
 UNIQUE KEY `UC_workitem_number` (`workitem_number`),
 KEY `FK_workitems_1` (`workitem_indice_id`),
 KEY `FK_workitems_2` (`project_id`),
 CONSTRAINT `FK_workitems_1` FOREIGN KEY (`workitem_indice_id`) REFERENCES `container_indice` (`indice_id`),
 CONSTRAINT `FK_workitems_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class WorkitemDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitems';

	/**
	 *
	 * @var string
	 */
	public static $vtable='workitems';

	/**
	 * @var string
	 */
	public static $sequenceName = 'workitems_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'workitem_id'=>'id',
		'workitem_number'=>'number',
		'uid'=>'uid',
		'name'=>'name',
		'workitem_description'=>'description',
		'workitem_state'=>'status',
		'workitem_indice_id'=>'versionId',
		'project_id'=>'parentId',
		'default_file_path'=>'path',
		'default_process_id'=>'processId',
		'forseen_close_date'=>'forseenCloseDate',
		'close_date'=>'closed',
		'open_date'=>'created',
		'open_by'=>'createById',
	);
	//'uuid'=>'uid',
	//'workitem_version'=>'versionId',
	//'workitem_iteration'=>'iterationId',
	//'access_code'=>'access'
	//'owned_by'=>'ownerId',

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'forseen_close_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ($parent){
			$mapped->parentId = $parent->getId();
		}

		$process = $mapped->getProcess();
		if ($process){
			$mapped->processId = $process->getId();
		}

		$updateBy = $mapped->getUpdateBy();
		if ($updateBy){
			$mapped->updateById = $updateBy->getId();
		}

		$createBy = $mapped->getCreateBy();
		if ($createBy){
			$mapped->createById = $createBy->getId();
		}

		$closeBy = $mapped->getCloseBy();
		if ($closeBy){
			$mapped->closeById = $closeBy->getId();
		}

		$alias = $mapped->getAlias();
		if ($alias){
			$mapped->aliasId = $alias->getId();
		}

		(isset($mapped->repositPath)) ? $mapped->path = $mapped->repositPath : null;

		parent::save($mapped, $options);
	} //End of function

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::hydrate()
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		parent::hydrate($mapped, $row, $fromApp);
		(isset($row['default_file_path'])) ? $mapped->path=$row['default_file_path'] : null;
		return $mapped;
	} //End of function


}
