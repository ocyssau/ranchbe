<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project;

/** SQL_SCRIPT>>
CREATE TABLE `project_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_by` varchar(32) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `project_number` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_description` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `project_state` varchar(16) COLLATE latin1_general_ci DEFAULT NULL,
  `project_indice_id` int(11) DEFAULT NULL,
  `default_process_id` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `open_date` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\History\HistoryDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='project_history';
	public static $vtable='project_history';

	public static $sequenceName = 'project_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'action_id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_date'=>'action_created',
		'comment'=>'comment',
		'data'=>'data',

		'project_id'=>'data_id',
		'project_number'=>'data_name',
		'project_description'=>'data_description',
		'project_state'=>'data_status',
		'project_indice_id'=>'data_indice',
		'default_process_id'=>'data_processId',
		'open_by'=>'data_createBy',
		'open_date'=>'data_created',
		'forseen_close_date'=>'data_forseenCloseDate',
		'close_by'=>'data_closeBy',
		'close_date'=>'data_closed',
		'area_id'=>'data_areaId',
	);

	public static $sysToAppFilter = array(
		'action_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
	);
}
