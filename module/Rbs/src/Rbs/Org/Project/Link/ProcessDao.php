<?php
//%LICENCE_HEADER%

namespace Rbs\Org\Project\Link;

/** SQL_SCRIPT>>
CREATE TABLE `project_mockup_rel`(
	`project_id` int(11) DEFAULT NULL,
	`mockup_id` int(11) DEFAULT NULL,
	UNIQUE KEY `UC_project_mockup_rel` (`project_id`,`mockup_id`),
	KEY `FK_project_mockup_rel_1` (`mockup_id`),
	CONSTRAINT `FK_project_mockup_rel_1` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
	CONSTRAINT `FK_project_mockup_rel_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`)
) ENGINE=InnoDB;

CREATE TABLE `project_rel_seq` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;

UPDATE `project_rel_seq` SET `id`='10' LIMIT 1;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


class MockupDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'project_mockup_rel';
	public static $vtable = 'project_mockup_rel';

	public static $parentTable = 'projects';
	public static $parentForeignKey = 'project_id';

	public static $childTable = 'mockups';
	public static $childForeignKey = 'mockup_id';

	public static $sequenceName = 'project_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'id'=>'id',
		'project_id'=>'parentId',
		'mockup_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
