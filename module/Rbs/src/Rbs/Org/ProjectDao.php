<?php
//%LICENCE_HEADER%

namespace Rbs\Org;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `projects` (
 `project_id` int(11) NOT NULL DEFAULT '0',
 `uid` varchar(32) NOT NULL DEFAULT '',
 `project_number` varchar(64) NOT NULL DEFAULT '',
 `name` varchar(16) NOT NULL DEFAULT '',
 `project_description` varchar(128) DEFAULT NULL,
 `project_state` varchar(16) NOT NULL DEFAULT 'init',
 `default_process_id` int(11) DEFAULT NULL,
 `project_indice_id` int(11) DEFAULT NULL,
 `area_id` int(11) DEFAULT NULL,
 `open_date` int(11) DEFAULT NULL,
 `open_by` int(11) DEFAULT NULL,
 `forseen_close_date` int(11) DEFAULT NULL,
 `close_by` int(11) DEFAULT NULL,
 `close_date` int(11) DEFAULT NULL,
 `link_id` int(11) DEFAULT NULL,
 PRIMARY KEY (`project_id`),
 UNIQUE KEY `UC_project_uid` (`uid`),
 UNIQUE KEY `UC_project_number` (`project_number`),
 KEY `FK_projects_1` (`project_indice_id`),
 KEY `FK_projects_2` (`link_id`)
 ) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Postgresql Dao class for \Rbplm\Org\Project.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class ProjectDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='projects';
	public static $vtable='projects';
	public static $sequenceName='projects_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'project_id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'project_number'=>'number',
		'project_description'=>'description',
		'project_state'=>'status',
		'default_process_id'=>'processId',
		'project_indice_id'=>'versionId',
		'area_id'=>'areaId',
		'open_date'=>'created',
		'open_by'=>'createById',
		'forseen_close_date'=>'forseenCloseDate',
		'close_by'=>'closedById',
		'close_date'=>'closed',
		'link_id'=>'linkId',
	);

	public static $sysToAppFilter = array(
		'open_date'=>'date',
		'forseen_close_date'=>'date',
		'close_date'=>'date',
	);

	/**
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		$parent = $mapped->getParent();
		if ($parent){
			$mapped->parentId = $parent->id;
		}

		$process = $mapped->getProcess();
		if ($process){
			$mapped->processId = $process->id;
		}

		$createBy = $mapped->getCreateBy();
		if ($createBy){
			$mapped->createById = $createBy->id;
		}

		$closeBy = $mapped->getCloseBy();
		if ($closeBy){
			$mapped->closeById = $closeBy->id;
		}

		$area = $mapped->area;
		if ($area){
			$mapped->areaId = $area->id;
		}

		parent::save($mapped, $options);
	} //End of function

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::hydrate()
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		(isset($row['area_id'])) ? $mapped->areaId = (int)$row['area_id']: null;
		parent::hydrate( $mapped, $row, $fromApp );
	}


}
