<?php
//%LICENCE_HEADER%

namespace Rbs\Org;
use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class for \Rbplm\Org\Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\Org\UnitTest
 *
 */
class UnitDao extends DaoSier
{
} //End of class

