<?php

namespace Rbs\Space;

/** This class manage the space: Set database parameters.
 *
 * space is use to define database fields name for each family of datas.
 * space bookshop, cadlib, mockup, workitem have specific table and field name.
 * Exemple : bookshops -> bookshop_id ; workitems -> workitem_id ; workitem_document ; bookshop_document...
 * Properties record in space permit to define common methods to access to data in database.
 */
class Space
{
	/**
	 * @var string
	 */
	public $cid = '64bdd156af045';


	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $id;

	/**
	 *
	 * @param unknown_type $objectType
	 * @throws Exception
	 */
	function __construct($name)
	{
		$this->name = $name;
		$this->id = self::getIdFromName($name);
	} //End of method

	/**
	 *
	 * @param integer $id
	 */
	public static function getNameFromId($id)
	{
		$idToName = array(
			1=>'Product',
			10=>'Workitem',
			15=>'Mockup',
			20=>'Bookshop',
			25=>'Cadlib'
		);
		return $idToName[$id];
	}

	/**
	 *
	 * @param string $name
	 */
	public static function getIdFromName($name)
	{
		$nameToId = array(
			'product'=>1,
			'workitem'=>10,
			'mockup'=>15,
			'bookshop'=>20,
			'cadlib'=>25
		);
		return $nameToId[($name)];
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 *
	 */
	public function getId()
	{
		return $this->id;
	}

}//End of class

