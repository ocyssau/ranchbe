<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Document;

/** SQL_SCRIPT>>
CREATE TABLE `workitem_documents_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32) NOT NULL DEFAULT '',
  `action_by` varchar(64) NOT NULL DEFAULT '',
  `action_date` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) DEFAULT NULL,
  `document_number` varchar(128) DEFAULT NULL,
  `document_state` varchar(32) DEFAULT NULL,
  `document_version` int(11) DEFAULT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `document_indice_id` int(11) DEFAULT NULL,
  `comment` text,
  `data` text,
  PRIMARY KEY (`histo_order`),
  KEY `workitem_documents_history_i001` (`instance_id`)
) ENGINE=InnoDB;

CREATE TABLE `workitem_documents_history_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB ;
 <<*/

/** SQL_INSERT>>
INSERT INTO `workitem_documents_history_seq` (`id`) VALUES (10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends \Rbs\History\HistoryDao
{
	/**
	 *
	 * @var string
	 */
	public static $table='workitem_documents_history';
	public static $vtable='workitem_documents_history';

	public static $sequenceName = 'workitem_documents_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'action_id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_date'=>'action_created',
		'action_comment'=>'action_comment',
		'data'=>'data',

		'document_id'=>'data_id',
		'document_number'=>'data_name',
		'designation'=>'data_description',
		'document_state'=>'data_lifeStage',
		'document_version'=>'data_iteration',
		'document_indice_id'=>'data_version',
	);

	public static $sysToAppFilter = array(
		'action_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
	);

	/**
	 * Unset some data properties to dont save in history
	 *
	 * (non-PHPdoc)
	 * @see Rbs\History.HistoryDao::_bind()
	 */
	protected function _bind($mapped)
	{
		//Unset some properties to dont save in history
		$data = $mapped->getData();
		unset($data['dao'], $data['docfiles'], $data['factory']);
		$mapped->setData($data);

		return parent::_bind($mapped);
	}
}
