<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE `workitem_documents` (
 `document_id` int(11) NOT NULL DEFAULT '0',
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_number` varchar(128)  NOT NULL DEFAULT '',
 `document_name` varchar(255)  DEFAULT NULL,
 `document_state` varchar(32)  NOT NULL DEFAULT 'init',
 `document_access_code` int(11) NOT NULL DEFAULT '0',
 `document_version` int(11) NOT NULL DEFAULT '1',
 `workitem_id` int(11) NOT NULL DEFAULT '0',
 `document_indice_id` int(11) DEFAULT NULL,
 `doc_space` varchar(8)  NOT NULL DEFAULT 'workitem',
 `instance_id` int(11) DEFAULT NULL,
 `doctype_id` int(11) NOT NULL DEFAULT '0',
 `default_process_id` int(11) DEFAULT NULL,
 `category_id` int(11) DEFAULT NULL,
 `check_out_by` int(11) DEFAULT NULL,
 `check_out_date` int(11) DEFAULT NULL,
 `designation` varchar(128)  DEFAULT NULL,
 `issued_from_document` int(11) DEFAULT NULL,
 `update_date` int(11) DEFAULT NULL,
 `update_by` decimal(10,0) DEFAULT NULL,
 `open_date` int(11) DEFAULT NULL,
 `open_by` int(11) DEFAULT NULL,
 `ci` varchar(255)  DEFAULT NULL,
 `type_gc` varchar(255)  DEFAULT NULL,
 `work_unit` varchar(255)  DEFAULT NULL,
 `father_ds` varchar(255)  DEFAULT NULL,
 `need_calcul` varchar(255)  DEFAULT NULL,
 `ata` varchar(255)  DEFAULT NULL,
 `receptionneur` varchar(32)  DEFAULT NULL,
 `indice_client` varchar(255)  DEFAULT NULL,
 `recept_date` int(11) DEFAULT NULL,
 `observation` varchar(255)  DEFAULT NULL,
 `date_de_remise` int(11) DEFAULT NULL,
 `sub_ata` varchar(255)  DEFAULT NULL,
 `work_package` varchar(255)  DEFAULT NULL,
 `subject` varchar(255)  DEFAULT NULL,
 `weight` double DEFAULT NULL,
 `cost` double DEFAULT NULL,
 `fab_process` varchar(255)  DEFAULT NULL,
 `material` varchar(255)  DEFAULT NULL,
 `materiau_mak` varchar(255)  DEFAULT NULL,
 `masse_mak` double DEFAULT NULL,
 `rt_name` varchar(255)  DEFAULT NULL,
 `rt_type` varchar(255)  DEFAULT NULL,
 `rt_reason` varchar(255)  DEFAULT NULL,
 `rt_emitted` int(11) DEFAULT NULL,
 `rt_apply_to` int(11) DEFAULT NULL,
 `modification` varchar(255)  DEFAULT NULL,
 PRIMARY KEY (`document_id`),
 UNIQUE KEY `IDX_workitem_documents_1` (`document_number`,`document_indice_id`),
 KEY `FK_workitem_documents_2` (`doctype_id`),
 KEY `FK_workitem_documents_3` (`document_indice_id`),
 KEY `FK_workitem_documents_4` (`workitem_id`),
 KEY `FK_workitem_documents_5` (`category_id`),
 KEY `document_name` (`document_name`),
 CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
 CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`),
 CONSTRAINT `FK_workitem_documents_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`),
 CONSTRAINT `FK_workitem_documents_5` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`)
 );

 CREATE TABLE `workitem_documents_seq` (
 `id` int(11) NOT NULL
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO `workitem_documents_seq` (`id`) VALUES (10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitem_documents';
	protected $_table='workitem_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable='workitem_documents';
	protected $_vtable='workitem_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitem_documents_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id'=>'id',
		'uid'=>'uid',
		'document_number'=>'number',
		'document_name'=>'name',
		'document_state'=>'lifeStage',
		'document_access_code'=>'accessCode',
		'document_version'=>'iteration',
		'workitem_id'=>'parentId',
		'document_indice_id'=>'version',
		'doctype_id'=>'doctypeId',
		'category_id'=>'categoryId',
		'check_out_by'=>'lockById',
		'check_out_date'=>'locked',
		'designation'=>'description',
		'issued_from_document'=>'fromId',
		'update_date'=>'updated',
		'update_by'=>'updateById',
		'open_date'=>'created',
		'open_by'=>'createById',
		'instance_id'=>'instanceId',
		'doc_space'=>'spaceName',
	);
}
