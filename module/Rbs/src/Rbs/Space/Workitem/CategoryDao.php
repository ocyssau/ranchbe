<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem;

use Rbs\Ged\CategoryDao as BaseDao;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `workitem_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) DEFAULT NULL,
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `UC_category_number` (`category_number`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `workitem_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  AUTO_INCREMENT=10;
INSERT INTO workitem_categories_seq SET id=10;

CREATE TABLE IF NOT EXISTS `workitem_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_workitem_categories_rel_1` (`category_id`,`workitem_id`),
  KEY `K_workitem_categories_rel_1` (`category_id`),
  KEY `K_workitem_categories_rel_2` (`workitem_id`)
) ENGINE=InnoDB;
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class CategoryDao extends BaseDao
{
    /**
     *
     * @var string
     */
    public static $table='workitem_categories';
    public static $vtable='workitem_categories';
    public static $sequenceName = 'workitem_categories_seq';

}
