<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem;

use Rbs\History\HistoryDao as BaseDao;

/** SQL_SCRIPT>>
CREATE TABLE `workitem_history` (
  `histo_order` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(32)  DEFAULT NULL,
  `action_by` varchar(32)  DEFAULT NULL,
  `action_date` int(11) DEFAULT NULL,
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  `workitem_number` varchar(16)  DEFAULT NULL,
  `workitem_state` varchar(16)  DEFAULT NULL,
  `workitem_description` varchar(128)  DEFAULT NULL,
  `workitem_indice_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `default_file_path` text,
  `open_date` int(11) DEFAULT NULL,
  `open_by` int(11) DEFAULT NULL,
  `forseen_close_date` int(11) DEFAULT NULL,
  `close_date` int(11) DEFAULT NULL,
  `close_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`histo_order`)
);

CREATE TABLE `workitem_history_seq` (
  `id` int(11) NOT NULL
);
 <<*/

/** SQL_INSERT>>
INSERT INTO `workitem_history_seq` (`id`) VALUES (10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 */
class HistoryDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='workitem_history';
	public static $vtable='workitem_history';

	public static $sequenceName = 'workitem_history_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'histo_order'=>'id',
		'action_name'=>'action_name',
		'action_by'=>'action_ownerId',
		'action_date'=>'action_created',
		'workitem_id'=>'data_id',
		'workitem_number'=>'data_name',
		'workitem_description'=>'data_description',
		'workitem_state'=>'data_status',
		'workitem_indice_id'=>'data_versionId',
		'project_id'=>'data_projectId',
		'open_by'=>'data_createBy',
		'open_date'=>'data_created',
		'forseen_close_date'=>'data_forseenCloseDate',
		'close_by'=>'data_closeBy',
		'close_date'=>'data_closed',
		'default_file_path'=>'data_defaultFilePath',
	);

	public static $sysToAppFilter = array(
		'action_date'=>'date',
		'open_date'=>'date',
		'close_date'=>'date',
		'forseen_close_date'=>'date',
	);
}
