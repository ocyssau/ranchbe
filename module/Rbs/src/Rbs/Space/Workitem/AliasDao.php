<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem;

/** SQL_SCRIPT>>
 CREATE TABLE `workitem_alias` (
 `alias_id` int(11) NOT NULL ,
 `workitem_id` int(11) NOT NULL,
 `workitem_number` varchar(16)  NOT NULL,
 `workitem_description` varchar(128)  default NULL,
 `object_class` varchar(10)  NOT NULL default 'workitemAlias',
 PRIMARY KEY  (`alias_id`),
 UNIQUE KEY `UC_workitem_alias` (`alias_id`,`workitem_id`),
 KEY `K_workitem_alias_1` (`workitem_id`)
 KEY `K_workitem_alias_2` (`workitem_number`),
 ) ENGINE=InnoDB ;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
UPDATE workitem_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AliasDao extends \Rbs\Org\Container\AliasDao
{

	/**
	 * @var string
	 */
	public static $table='workitem_alias';
	public static $vtable='workitem_alias';
	public static $sequenceName = 'workitems_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'alias_id'=>'id',
		'workitem_id'=>'aliasOfId',
		'name'=>'name',
		'workitem_number'=>'uid',
		'workitem_description'=>'description',
		'object_class'=>'cid'
	);
}
