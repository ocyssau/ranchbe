<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Docfile;

use Rbs\Ged\Docfile\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `workitem_doc_files` (
 `file_id` int(11) NOT NULL,
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_id` int(11) NOT NULL,
 `file_name` varchar(128) NOT NULL,
 `file_path` text NOT NULL,
 `file_version` int(11) NOT NULL DEFAULT '1',
 `file_access_code` int(11) NOT NULL DEFAULT '0',
 `file_root_name` varchar(128) NOT NULL,
 `file_extension` varchar(16) DEFAULT NULL,
 `file_state` varchar(16) NOT NULL DEFAULT 'init',
 `file_type` varchar(128) NOT NULL,
 `file_size` int(11) DEFAULT NULL,
 `file_mtime` int(11) DEFAULT NULL,
 `file_md5` varchar(128) DEFAULT NULL,
 `file_checkout_by` int(11) DEFAULT NULL,
 `file_checkout_date` int(11) DEFAULT NULL,
 `file_open_date` int(11) NOT NULL,
 `file_open_by` int(11) NOT NULL,
 `file_update_date` int(11) DEFAULT NULL,
 `file_update_by` int(11) DEFAULT NULL,
 `mainrole` int(2) NOT NULL DEFAULT 4,
 `roles` VARCHAR(512) DEFAULT NULL,
 PRIMARY KEY (`file_id`),
 UNIQUE KEY `uniq_file_name` (`file_name`,`file_path`(512)),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_workitem_doc_files_1` (`document_id`),
  KEY `IK_workitem_doc_files_2` (`file_name`),
  KEY `IK_workitem_doc_files_3` (`file_path`(128)),
  KEY `IK_workitem_doc_files_4` (`file_version`,`document_id`),
  KEY `IK_workitem_doc_files_5` (`mainrole`),
  KEY `IK_workitem_doc_files_5` (`roles`(32))
 ) ENGINE=InnoDB;

CREATE TABLE `workitem_doc_files_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;

 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{
	/**
	 *
	 * @var string
	 */
	public static $table='workitem_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $vtable='workitem_doc_files';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'workitem_doc_files_seq';

}
