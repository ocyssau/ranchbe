<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `workitem_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `workitem_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_workitem_categories_rel_1` (`category_id`,`workitem_id`),
  KEY `K_workitem_categories_rel_1` (`category_id`),
  KEY `K_workitem_categories_rel_2` (`workitem_id`),
  CONSTRAINT `workitem_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `workitem_category_rel_ibfk_2` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `workitem_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `workitem_category_rel_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 UPDATE workitem_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/





class CategoryDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * @var string
	 */
	public static $table = 'workitem_category_rel';
	public static $vtable = 'workitem_category_rel';

	public static $parentTable = 'workitems';
	public static $parentForeignKey = 'workitem_id';

	public static $childTable = 'workitem_categories';
	public static $childForeignKey = 'category_id';

	public static $sequenceName = 'workitem_category_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'workitem_id'=>'parentId',
		'category_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
