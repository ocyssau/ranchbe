<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib;

/** SQL_SCRIPT>>
 CREATE TABLE `cadlib_alias` (
 `alias_id` int(11) NOT NULL ,
 `cadlib_id` int(11) NOT NULL,
 `cadlib_number` varchar(16)  NOT NULL,
 `cadlib_description` varchar(128)  default NULL,
 `object_class` varchar(10)  NOT NULL default 'cadlibAlias',
 PRIMARY KEY  (`alias_id`),
 UNIQUE KEY `UC_cadlib_alias` (`alias_id`,`cadlib_id`),
 KEY `K_cadlib_alias_1` (`cadlib_id`)
 KEY `K_cadlib_alias_2` (`cadlib_number`),
 ) ENGINE=InnoDB ;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
UPDATE cadlib_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AliasDao extends \Rbs\Org\Container\AliasDao
{

	/**
	 * @var string
	 */
	public static $table='cadlib_alias';
	public static $vtable='cadlib_alias';
	public static $sequenceName = 'cadlibs_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'alias_id'=>'id',
		'cadlib_id'=>'aliasOfId',
		'name'=>'name',
		'cadlib_number'=>'uid',
		'cadlib_description'=>'description',
		'object_class'=>'cid'
	);
}
