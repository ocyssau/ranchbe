<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `cadlib_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `cadlib_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_cadlib_categories_rel_1` (`category_id`,`cadlib_id`),
  KEY `K_cadlib_categories_rel_1` (`category_id`),
  KEY `K_cadlib_categories_rel_2` (`cadlib_id`),
  CONSTRAINT `cadlib_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `cadlib_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cadlib_category_rel_ibfk_2` FOREIGN KEY (`cadlib_id`) REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `cadlib_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `cadlib_category_rel_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 UPDATE cadlib_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/





class CategoryDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * @var string
	 */
	public static $table = 'cadlib_category_rel';
	public static $vtable = 'cadlib_category_rel';

	public static $parentTable = 'cadlibs';
	public static $parentForeignKey = 'cadlib_id';

	public static $childTable = 'cadlib_categories';
	public static $childForeignKey = 'category_id';

	public static $sequenceName = 'cadlib_category_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'cadlib_id'=>'parentId',
		'category_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
