<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib;

/** SQL_SCRIPT>>
CREATE TABLE `cadlib_metadata` (
  `id` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `appname` varchar(30) DEFAULT NULL,
  `extendedCid` varchar(32) DEFAULT NULL,
  `label` varchar(30) DEFAULT NULL,
  `field_description` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(32) NOT NULL DEFAULT '',
  `field_regex` varchar(255) DEFAULT NULL,
  `field_required` int(1) NOT NULL DEFAULT '0',
  `field_multiple` int(1) NOT NULL DEFAULT '0',
  `field_size` int(11) DEFAULT NULL,
  `return_name` int(1) NOT NULL DEFAULT '0',
  `field_list` varchar(255) DEFAULT NULL,
  `field_where` varchar(255) DEFAULT NULL,
  `is_hide` int(1) NOT NULL DEFAULT '0',
  `table_name` varchar(32) DEFAULT NULL,
  `field_for_value` varchar(32) DEFAULT NULL,
  `field_for_display` varchar(32) DEFAULT NULL,
  `date_format` varchar(32) DEFAULT NULL,
  `adv_select` tinyint(1) NOT NULL DEFAULT '0',
  `getter` varchar(30) DEFAULT NULL,
  `setter` varchar(30) DEFAULT NULL,
  `min` decimal(10,0) DEFAULT NULL,
  `max` decimal(10,0) DEFAULT NULL,
  `step` decimal(10,0) DEFAULT NULL,
  `dbfilter` varchar(255) DEFAULT NULL,
  `dbquery` varchar(255) DEFAULT NULL,
  `attributes` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fname-cid` (`field_name`,`extendedCid`),
  KEY `extendedCid_key` (`extendedCid`),
  KEY `field_name_key` (`field_name`),
  KEY `appname_key` (`appname`),
  KEY `label_key` (`label`)
) ENGINE=InnoDB;

CREATE TABLE `cadlib_metadata_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
 <<*/

/** SQL_INSERT>>
INSERT INTO `cadlib_metadata_seq` (`id`) VALUES (10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Entity of a property definition.
 */
class PropertyDao extends \Rbs\Extended\PropertyDao
{

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'cadlib_metadata';
	public static $sequenceName='cadlib_metadata_seq';
	public static $sequenceKey='id';
}
