<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib\Docfile;

/** SQL_SCRIPT>>
CREATE TABLE cadlib_doc_files_versions LIKE cadlib_doc_files;
ALTER TABLE cadlib_doc_files_versions
	ADD of_file_id int(11) DEFAULT NULL,
	ADD INDEX of_file_id;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends \Rbs\Ged\Docfile\IterationDao
{
	/**
	 * @var string
	 */
	public static $table='cadlib_doc_files_versions';
	public static $vtable='cadlib_doc_files_versions';
	public static $sequenceName = 'cadlib_doc_files_seq';
}
