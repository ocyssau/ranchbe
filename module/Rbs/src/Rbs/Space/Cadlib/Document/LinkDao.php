<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Cadlib\Document;

use Rbs\Ged\Document\LinkDao as BaseDao;


/** SQL_SCRIPT>>
 CREATE TABLE `cadlib_doc_rel` (
 `dr_link_id` int(11) NOT NULL default '0',
 `dr_document_id` int(11) default NULL,
 `dr_l_document_id` int(11) default NULL,
 `dr_access_code` int(11) default NULL,
 PRIMARY KEY  (`dr_link_id`),
 UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

 CREATE TABLE `cadlib_doc_rel_seq` (
 `id` int(11) NOT NULL
 );


 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `cadlib_doc_rel`
 ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE;

 ALTER TABLE `cadlib_doc_rel` ADD COLUMN `hash` char(32) default NULL AFTER `dr_access_code`;
 ALTER TABLE `cadlib_doc_rel` ADD COLUMN `data` varchar(512) default NULL AFTER `hash`;
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class LinkDao extends BaseDao
{

	/**
	 * @var string
	 */
	public static $table='cadlib_doc_rel';
	public static $vtable='cadlib_doc_rel';
	public static $sequenceName='cadlib_doc_rel_seq';

}
