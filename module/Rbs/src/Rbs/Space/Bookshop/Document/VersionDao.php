<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `bookshop_documents` (
 `document_id` int(11) NOT NULL DEFAULT '0',
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_number` varchar(128) NOT NULL DEFAULT '',
 `document_state` varchar(32) NOT NULL DEFAULT 'init',
 `document_access_code` int(11) NOT NULL DEFAULT '0',
 `document_version` int(11) NOT NULL DEFAULT '1',
 `bookshop_id` int(11) NOT NULL DEFAULT '0',
 `document_indice_id` int(11) DEFAULT NULL,
 `instance_id` int(11) DEFAULT NULL,
 `doctype_id` int(11) NOT NULL DEFAULT '0',
 `default_process_id` int(11) DEFAULT NULL,
 `category_id` int(11) DEFAULT NULL,
 `check_out_by` int(11) DEFAULT NULL,
 `check_out_date` int(11) DEFAULT NULL,
 `designation` varchar(128) DEFAULT NULL,
 `issued_from_document` int(11) DEFAULT NULL,
 `update_date` int(11) DEFAULT NULL,
 `update_by` decimal(10,0) DEFAULT NULL,
 `open_date` int(11) DEFAULT NULL,
 `open_by` int(11) DEFAULT NULL,
 `ci` varchar(255) DEFAULT NULL,
 `type_gc` varchar(255) DEFAULT NULL,
 `work_unit` varchar(255) DEFAULT NULL,
 `father_ds` varchar(255) DEFAULT NULL,
 `need_calcul` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`document_id`),
 UNIQUE KEY `IDX_bookshop_documents_1` (`document_number`,`document_indice_id`),
 KEY `FK_bookshop_documents_2` (`doctype_id`),
 KEY `FK_bookshop_documents_3` (`document_indice_id`),
 KEY `FK_bookshop_documents_4` (`bookshop_id`),
 KEY `FK_bookshop_documents_5` (`category_id`)
 ) ENGINE=InnoDB;
CREATE TABLE `cadlib_documents_seq`(
`id` int(11) NOT NULL
);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='bookshop_documents';
	protected $_table='bookshop_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable='bookshop_documents';
	protected $_vtable='bookshop_documents';

	/**
	 * @var string
	 */
	public static $sequenceName = 'bookshop_documents_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id'=>'id',
		'uid'=>'uid',
		'document_number'=>'number',
		'document_name'=>'name',
		'document_state'=>'lifeStage',
		'document_access_code'=>'accessCode',
		'document_version'=>'iteration',
		'bookshop_id'=>'parentId',
		'document_indice_id'=>'version',
		'doctype_id'=>'doctypeId',
		'category_id'=>'categoryId',
		'check_out_by'=>'lockById',
		'check_out_date'=>'locked',
		'designation'=>'description',
		'issued_from_document'=>'fromId',
		'update_date'=>'updated',
		'update_by'=>'updateById',
		'open_date'=>'created',
		'open_by'=>'createById',
		'instance_id'=>'instanceId',
	);
}
