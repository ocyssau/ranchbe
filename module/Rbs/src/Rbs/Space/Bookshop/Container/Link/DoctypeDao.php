<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `bookshop_doctype_process` (
`link_id` int(11) NOT NULL DEFAULT '0',
`doctype_id` int(11) DEFAULT NULL,
`process_id` int(11) DEFAULT NULL,
`bookshop_id` int(11) DEFAULT NULL,
`category_id` int(11) DEFAULT NULL,
PRIMARY KEY (`link_id`),
KEY `FK_bookshop_doctype_process_1` (`doctype_id`),
KEY `FK_bookshop_doctype_process_3` (`category_id`),
KEY `FK_bookshop_doctype_process_4` (`bookshop_id`),
CONSTRAINT `FK_bookshop_doctype_process_1` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
CONSTRAINT `FK_bookshop_doctype_process_3` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`),
CONSTRAINT `FK_bookshop_doctype_process_4` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`)
) ENGINE=InnoDB;

CREATE TABLE `bookshop_doctype_process_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `bookshop_doctype_process_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @author olivier
 *
 */
class DoctypeDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'bookshop_doctype_process';
	public static $vtable = 'bookshop_doctype_process';

	public static $parentTable = 'bookshops';
	public static $parentForeignKey = 'bookshop_id';

	public static $childTable = 'doctypes';
	public static $childForeignKey = 'doctype_id';

	public static $sequenceName = 'bookshop_doctype_process_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'bookshop_id'=>'parentId',
		'doctype_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
