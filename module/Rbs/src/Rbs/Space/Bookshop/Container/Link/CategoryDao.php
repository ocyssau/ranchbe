<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `bookshop_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `bookshop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_bookshop_categories_rel_1` (`category_id`,`bookshop_id`),
  KEY `K_bookshop_categories_rel_1` (`category_id`),
  KEY `K_bookshop_categories_rel_2` (`bookshop_id`),
  CONSTRAINT `bookshop_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `bookshop_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bookshop_category_rel_ibfk_2` FOREIGN KEY (`bookshop_id`) REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `bookshop_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `bookshop_category_rel_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 UPDATE bookshop_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/





class CategoryDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * @var string
	 */
	public static $table = 'bookshop_category_rel';
	public static $vtable = 'bookshop_category_rel';

	public static $parentTable = 'bookshops';
	public static $parentForeignKey = 'bookshop_id';

	public static $childTable = 'bookshop_categories';
	public static $childForeignKey = 'category_id';

	public static $sequenceName = 'bookshop_category_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'bookshop_id'=>'parentId',
		'category_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
