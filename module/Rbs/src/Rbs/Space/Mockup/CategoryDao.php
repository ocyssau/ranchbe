<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup;

use Rbs\Ged\CategoryDao as BaseDao;


/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `mockup_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `category_number` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  `category_description` text COLLATE latin1_general_ci,
  `category_icon` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `UC_category_number` (`category_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS `mockup_categories_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=10;
INSERT INTO mockup_categories_seq SET id=10;

CREATE TABLE IF NOT EXISTS `mockup_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_mockup_categories_rel_1` (`category_id`,`mockup_id`),
  KEY `K_mockup_categories_rel_1` (`category_id`),
  KEY `K_mockup_categories_rel_2` (`mockup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class CategoryDao extends BaseDao
{
    /**
     *
     * @var string
     */
    public static $table='mockup_categories';
    public static $vtable='mockup_categories';
    public static $sequenceName='mockup_categories_seq';
}
