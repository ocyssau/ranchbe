<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Document;

use Rbs\Ged\Document\VersionDao as BaseDao;

/** SQL_SCRIPT>>
 CREATE TABLE `mockup_documents` (
 `document_id` int(11) NOT NULL DEFAULT '0',
 `uid` VARCHAR(140) NOT NULL UNIQUE,
 `document_number` varchar(64) NOT NULL DEFAULT '',
 `document_state` varchar(32) NOT NULL DEFAULT 'init',
 `document_access_code` int(11) NOT NULL DEFAULT '0',
 `document_version` int(11) NOT NULL DEFAULT '1',
 `mockup_id` int(11) DEFAULT NULL,
 `instance_id` int(11) DEFAULT NULL,
 `supplier_id` int(11) DEFAULT NULL,
 `cad_model_supplier_id` int(11) DEFAULT NULL,
 `document_indice_id` int(11) DEFAULT NULL,
 `doc_space` varchar(6) NOT NULL DEFAULT 'mockup',
 `doctype_id` int(11) DEFAULT NULL,
 `category_id` int(11) DEFAULT NULL,
 `default_process_id` int(11) DEFAULT NULL,
 `designation` varchar(128) DEFAULT NULL,
 `check_out_date` int(11) DEFAULT NULL,
 `check_out_by` int(11) DEFAULT NULL,
 `update_date` int(11) DEFAULT NULL,
 `update_by` decimal(10,0) DEFAULT NULL,
 `open_date` int(11) DEFAULT NULL,
 `open_by` int(11) DEFAULT NULL,
 `issued_from_document` int(11) DEFAULT NULL,
 PRIMARY KEY (`document_id`),
 UNIQUE KEY `IDX_mockup_documents_1` (`document_number`,`document_indice_id`),
 KEY `FK_mockup_documents_1` (`cad_model_supplier_id`),
 KEY `FK_mockup_documents_2` (`supplier_id`),
 KEY `FK_mockup_documents_3` (`mockup_id`),
 KEY `FK_mockup_documents_4` (`category_id`),
 KEY `FK_mockup_documents_5` (`doctype_id`),
 KEY `FK_mockup_documents_6` (`document_indice_id`),
 CONSTRAINT `FK_mockup_documents_1` FOREIGN KEY (`cad_model_supplier_id`) REFERENCES `partners` (`partner_id`),
 CONSTRAINT `FK_mockup_documents_2` FOREIGN KEY (`supplier_id`) REFERENCES `partners` (`partner_id`),
 CONSTRAINT `FK_mockup_documents_3` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`),
 CONSTRAINT `FK_mockup_documents_4` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`),
 CONSTRAINT `FK_mockup_documents_5` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
 CONSTRAINT `FK_mockup_documents_6` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE `mockup_documents_seq` (
 `id` int(11) NOT NULL
 );
 <<*/

/** SQL_INSERT>>
 INSERT INTO `mockup_documents_seq` (`id`) VALUES (10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{

	/**
	 *
	 * @var string
	 */
	public static $table='mockup_documents';
	protected $_table='mockup_documents';

	/**
	 *
	 * @var string
	 */
	public static $vtable='mockup_documents';
	protected $_vtable='mockup_documents';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName = 'mockup_documents_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'document_id'=>'id',
		'uid'=>'uid',
		'document_number'=>'number',
		'document_name'=>'name',
		'document_state'=>'lifeStage',
		'document_access_code'=>'accessCode',
		'document_version'=>'iteration',
		'mockup_id'=>'parentId',
		'document_indice_id'=>'version',
		'doctype_id'=>'doctypeId',
		'category_id'=>'categoryId',
		'check_out_by'=>'lockById',
		'check_out_date'=>'locked',
		'designation'=>'description',
		'issued_from_document'=>'fromId',
		'update_date'=>'updated',
		'update_by'=>'updateById',
		'open_date'=>'created',
		'open_by'=>'createById',
		'instance_id'=>'instanceId',
	);
}
