<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Bookshop\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `mockup_category_rel` (
  `link_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `mockup_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `UNIQ_mockup_categories_rel_1` (`category_id`,`mockup_id`),
  KEY `K_mockup_categories_rel_1` (`category_id`),
  KEY `K_mockup_categories_rel_2` (`mockup_id`),
  CONSTRAINT `mockup_category_rel_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `mockup_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mockup_category_rel_ibfk_2` FOREIGN KEY (`mockup_id`) REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `mockup_category_rel_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `mockup_category_rel_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 UPDATE mockup_alias SET object_class='45c84a5zalias';
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/





class CategoryDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * @var string
	 */
	public static $table = 'mockup_category_rel';
	public static $vtable = 'mockup_category_rel';

	public static $parentTable = 'mockups';
	public static $parentForeignKey = 'mockup_id';

	public static $childTable = 'mockup_categories';
	public static $childForeignKey = 'category_id';

	public static $sequenceName = 'mockup_category_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'mockup_id'=>'parentId',
		'category_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
