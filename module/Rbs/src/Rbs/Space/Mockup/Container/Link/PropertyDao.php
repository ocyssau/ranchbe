<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Workitem\Container\Link;

/** SQL_SCRIPT>>
CREATE TABLE `mockup_metadata_rel` (
`link_id` int(11) NOT NULL DEFAULT '0',
`mockup_id` int(11) NOT NULL DEFAULT '0',
`property_id` int(1) NOT NULL,
PRIMARY KEY (`link_id`),
UNIQUE KEY `mockup_metadata_rel_uniq` (`mockup_id`,`property_id`),
KEY `mockup_id_2` (`mockup_id`,`property_id`)
) ENGINE=InnoDB;

CREATE TABLE `mockup_metadata_rel_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
UPDATE `mockup_metadata_rel_seq` SET `id`='10' LIMIT 1;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/





class PropertyDao extends \Rbs\Dao\Sier\Link
{
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $table = 'mockup_metadata_rel';
	public static $vtable = 'mockup_metadata_rel';

	public static $parentTable = 'mockups';
	public static $parentForeignKey = 'mockup_id';

	public static $childTable = 'mockup_metadata';
	public static $childForeignKey = 'id';
	public static $sequenceName = 'mockup_metadata_rel_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	static $sysToApp = array(
		'link_id'=>'id',
		'mockup_id'=>'parentId',
		'property_id'=>'childId',
	);

	/**
	 *
	 * @var array
	 */
	static $sysToAppFilter = array(
	);
}
