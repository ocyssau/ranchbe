<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Docfile;

/** SQL_SCRIPT>>
CREATE TABLE mockup_doc_files_versions LIKE mockup_doc_files;
ALTER TABLE mockup_doc_files_versions
	ADD of_file_id int(11) DEFAULT NULL,
	ADD INDEX of_file_id;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class IterationDao extends \Rbs\Ged\Docfile\IterationDao
{
	/**
	 * @var string
	 */
	public static $table='mockup_doc_files_versions';
	public static $vtable='mockup_doc_files_versions';
	public static $sequenceName = 'mockup_doc_files_seq';
}
