<?php
//%LICENCE_HEADER%

namespace Rbs\Space\Mockup\Docfile;

use Rbs\Ged\Docfile\VersionDao as BaseDao;

/** SQL_SCRIPT>>
CREATE TABLE `mockup_doc_files` LIKE `workitem_doc_files`;
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class VersionDao extends BaseDao
{
    /**
     *
     * @var string
     */
    public static $table='mockup_doc_files';

    /**
     *
     * @var string
     */
    public static $vtable='mockup_doc_files';
}
