<?php

namespace Rbs\Space;

use Rbplm\Dao\DaoInterface;
use Rbplm\Dao\DaoListInterface;
use Rbplm\Dao\ConnexionInterface;
use Rbplm\Dao\FilterInterface;
use Rbs\Dao\Loader;

class Factory
{
	/**
	 * The name of the last used factory
	 * @var string
	 */
	public static $last;

	/**
	 *
	 * @var SpaceId as define in Space
	 */
	protected $id;

	/**
	 * SpaceName
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var array
	 */
	protected $registry;

	/**
	 * @var Loader
	 */
	protected $loader;

	/**
	 * @var ConnexionInterface
	 */
	protected static $connexion;

	/**
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * map key is the class id
	 * value is array where
	 * - key 0 is dao class name
	 * - key 1 is table name
	 * - key 2 is model class
	 *
	 * @var array
	 */
	protected static $map = array();
	protected $spacemap = array();

	/**
	 * Multi-Singleton
	 * @var Factory
	 */
	protected static $instances;

	/**
	 * Array of map buizness object to Dao class to instanciate and connexion to use.
	 * @var array
	 */
	protected static $options = array('listclass'=>'', 'filterclass'=>'');

	/**
	 * @param string $name
	 */
	public function __construct($name)
	{
		$lowername = strtolower($name);
		$this->name = ucfirst($lowername);
		$this->id = Space::getIdFromName($name);
		$this->spacemap = array_merge(static::$map['All'], static::$map[$this->name]);
		$this->registry = array();
		self::$instances[$lowername] = $this;
		self::$last = $lowername;
	}

	/**
	 *
	 * @param string $name
	 */
	public static function getIdFromName($name)
	{
		$nameToId = array(
			'product'=>1,
			'workitem'=>10,
			'mockup'=>15,
			'bookshop'=>20,
			'cadlib'=>25
		);
		return $nameToId[strtolower($name)];
	}

	/**
	 * Multi-singleton
	 *
	 * @param string $name
	 */
	public static function get($name='default')
	{
		$name = strtolower($name);
		if(!isset(self::$instances[$name])){
			new self($name);
		}
		self::$last = $name;
		return self::$instances[$name];
	}

	/**
	 * @param $conn ConnexionInterface
	 */
	public static function setConnexion($conn)
	{
		static::$connexion = $conn;
	}

	/**
	 * @return \PDO
	 */
	public static function getConnexion()
	{
		return static::$connexion;
	}

	/**
	 * @param array $config		Is array of map buizness object to Dao class to instanciate and connexion to use.
	 */
	public static function setOption( $key, $value )
	{
		$key = strtolower($key);
		static::$options[$key] = $value;
	}

	/**
	 * @param array $config		Is array of map buizness object to Dao class to instanciate and connexion to use.
	 */
	public static function setMap( array $map )
	{
		static::$map = $map;
	}

	/**
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string|Any $anyOrId
	 * @return DaoInterface
	 */
	public function getDao($anyOrId)
	{
		if(is_object($anyOrId)){
			$classId = $anyOrId->cid;
		}
		else{
			$classId = $anyOrId;
		}

		$spacename = $this->name;
		$map = $this->spacemap;

		if(!isset($this->registry[$classId])){
			$daoClass = $map[$classId][0];
			$dao  = new $daoClass(static::$connexion);

			/*keep a reference to the factory*/
			$dao->factory = $this;
			$this->registry[$classId] = $dao;
		}

		return $this->registry[$classId];
	}

	/**
	 * Return a instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return \Rbplm\Model\AnyPermanent
	 */
	public function getModel( $classId )
	{
		$class = $this->spacemap[$classId][2];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @return \Rbplm\Model\AnyPermanent
	 */
	public function getNewModel( $classId )
	{
		$class = $this->spacemap[$classId][2];
		return $class::init();
	}

	/**
	 * @return DaoListInterface
	 */
	public function getList( $classId )
	{
		$listClass = static::$options['listclass'];
		$table = $this->spacemap[$classId][1];
		$daoClass = $this->spacemap[$classId][0];
		$list = new $listClass($table, static::$connexion);
		$list->setOption('dao', $daoClass);
		/*keep a reference to the factory*/
		$list->factory = $this;
		return $list;
	}

	/**
	 * @return FilterInterface
	 */
	public function getFilter( $classId )
	{
		$daoClass = $this->spacemap[$classId][0];
		$filterClass = static::$options['filterclass'];
		$filter = new $filterClass($daoClass);
		return $filter;
	}

	/**
	 * @return string
	 */
	public function getDaoClass( $classId )
	{
		return $this->spacemap[$classId][0];
	}

	/**
	 * @return string
	 */
	public function getModelClass( $classId )
	{
		return $this->spacemap[$classId][2];
	}

	/**
	 * @return string
	 */
	public function getTable( $classId )
	{
		return $this->spacemap[$classId][1];
	}

	/**
	 *
	 * @param integer $cid
	 */
	public function getMap($cid=null)
	{
		if($cid){
			return $this->spacemap[$cid];
		}
		else{
			return $this->spacemap;
		}
	}

	/**
	 * @return Loader
	 */
	public function getLoader()
	{
		if(!isset($this->loader)){
			$this->loader = new Loader($this);
		}
		return $this->loader;
	}

	/**
	 * @param integer $cid
	 */
	public function getExtended($classId)
	{
		$loader = new Extended\Loader();
		$table =$this->spacemap[$classId][1];
	}

	/**
	 * Erase all objects recorded in registry, or just the $object.
	 *
	 * @param DaoInterface
	 * @return void
	 */
	public function reset( $object = null )
	{
		if( $object ){
			foreach( static::$registry as $key=>$in ){
				if( $in == $object){
					unset(static::$registry[$key]);
					break;
				}
			}
		}
		else{
			static::$registry = array();
		}
	}
}
