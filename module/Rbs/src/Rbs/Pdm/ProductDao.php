<?php
//%LICENCE_HEADER%

namespace Rbs\Pdm;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Pdm;

/** SQL_SCRIPT>>
CREATE TABLE `pdm_product` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`uid`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`)
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class ProductDao extends DaoSier
{
	/**
	 * @var string
	 */
	public static $table='pdm_product';
	public static $vtable='pdm_product';

	public static $sequenceName = 'pdm_seq';
	public static $sequenceKey = 'id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
			'id'=>'id',
			'uid'=>'uid',
			'name'=>'name',
			'description'=>'description',
	);

} //End of class
