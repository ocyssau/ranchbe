<?php
//%LICENCE_HEADER%

namespace Rbs\Pdm;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Pdm\Usage;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 * @see \Rbs\Dao\Sier
 *
 */
class UsageDao extends DaoSier
{
	/**
	 *
	 * @var string
	 */
	public static $table='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(self::$sysToApp, DaoSier::$sysToApp);
	} //End of function
} //End of class