<?php
// %LICENCE_HEADER%
namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Pdm;
use Rbplm\Dao\MappedInterface;

/**
 * SQL_SCRIPT>>
 * CREATE TABLE `pdm_product_instance` (
 * `id` int(11) NOT NULL,
 * `uid` varchar(32) NOT NULL,
 * `cid` varchar(32) NOT NULL DEFAULT '569e971bb57c0',
 * `name` varchar(128) NOT NULL,
 * `number` varchar(128) NOT NULL,
 * `nomenclature` varchar(128) NULL,
 * `description` varchar(512) NULL,
 * `position` varchar(512) NULL,
 * `quantity` int(11) NULL,
 * `parent_id` int(11) NULL,
 * `path` varchar(256) NULL,
 * `of_product_id` int(11) NULL,
 * `of_product_uid` varchar(32) NULL,
 * `context_id` varchar(32) NULL,
 * `type` varchar(64) NULL,
 * PRIMARY KEY (`id`),
 * UNIQUE KEY `pdm_product_instance_uniq1` (`uid`),
 * KEY `INDEX_pdm_product_instance_1` (`cid`),
 * KEY `INDEX_pdm_product_instance_2` (`number`),
 * KEY `INDEX_pdm_product_instance_3` (`description`),
 * KEY `INDEX_pdm_product_instance_4` (`nomenclature`),
 * KEY `INDEX_pdm_product_instance_5` (`uid`),
 * KEY `INDEX_pdm_product_instance_6` (`name`),
 * KEY `INDEX_pdm_product_instance_7` (`type`),
 * KEY `INDEX_pdm_product_version_8` (`of_product_uid`),
 * KEY `INDEX_pdm_product_version_9` (`of_product_id`)
 * KEY `INDEX_pdm_product_version_10` (`path`)
 * );
 *
 * CREATE TABLE `pdm_seq`(
 * `id` int(11) NOT NULL AUTO_INCREMENT,
 * PRIMARY KEY (`id`)
 * )ENGINE=MyISAM AUTO_INCREMENT=10;
 * INSERT INTO pdm_seq(id) VALUES(10);
 *
 * <<
 */

/**
 * SQL_INSERT>>
 * <<
 */

/**
 * SQL_ALTER>>
 * <<
 */

/**
 * SQL_FKEY>>
 * <<
 */

/**
 * SQL_TRIGGER>>
 * <<
 */

/**
 * SQL_VIEW>>
 * <<
 */

/**
 * SQL_DROP>>
 * DROP TABLE pdm_product_instance;
 * <
 */

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class InstanceDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table = 'pdm_product_instance';

	/**
	 * @var string
	 */
	public static $vtable = 'pdm_product_instance';

	/**
	 * @var string
	 */
	public static $sequenceName = 'pdm_seq';

	/**
	 * @var string
	 */
	public static $sequenceKey = 'id';

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public static $childTable = 'pdm_product_version';

	/**
	 * @var string
	 */
	public static $childForeignKey = 'id';

	/**
	 * @var string
	 */
	public static $parentTable = 'pdm_product_version';

	/**
	 * @var string
	 */
	public static $parentForeignKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id' => 'id',
		'uid' => 'uid',
		'cid' => 'cid',
		'name' => 'name',
		'number' => 'number',
		'description' => 'description',
		'nomenclature' => 'nomenclature',
		'quantity' => 'quantity',
		'position' => 'position',
		'of_product_id' => 'ofProductId',
		'of_product_uid' => 'ofProductUid',
		'parent_id' => 'parentId',
		'path' => 'path',
		'context_id' => 'contextId',
		'type' => 'type'
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'position' => 'json'
	);

	/**
	 * @param \PDO $conn
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);

		$this->_childTable = static::$childTable;
		$this->_childForeignKey = static::$childForeignKey;
		$this->_parentTable = static::$parentTable;
		$this->_parentForeignKey = static::$parentForeignKey;
	}

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param string $instanceName
	 * @param string $parentNumber
	 */
	public function loadFromNameAndParent($mapped, $instanceName, $parentNumber)
	{
		$filter = 'obj.name=:name AND parent.number=:parentNumber';
		$bind = array(
			':name' => $instanceName,
			':parentNumber' => $parentNumber
		);
		$table = $this->_table;

		$sql = "SELECT child.* FROM $table as child
		JOIN $table as parent ON parent.id=child.parent_id
		WHERE child.name=:name AND parent.number=:parentNumber";

		return $this->loadFromSql($mapped, $sql, $bind);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Rbplm\Dao.DaoInterface::save()
	 */
	public function save(MappedInterface $mapped)
	{
		if ( !isset($mapped->ofProductId) ) {
			try {
				$ofProduct = $mapped->getOfProduct();
				if ( $ofProduct ) {
					$mapped->setOfProduct($mapped->getOfProduct());
				}
			}
			catch( \Exception $e ) {
				throw new Exception('productId is not set');
			}
		}

		if ( !isset($mapped->parentId) ) {
			try {
				$parent = $mapped->getParent();
				if ( $parent ) {
					$mapped->setParent($mapped->getParent());
				}
			}
			catch( \Exception $e ) {
				throw new Exception('parentId is not set');
			}
		}

		return parent::save($mapped);
	}

	/**
	 * Get children instances of a product from a user defined filter
	 *
	 * In filter You may use alias instance for select instance table :
	 * 		You may use alias "parent." for select parent product table
	 * 		You may use alias "child." for select child product table (product of the instance)
	 *
	 * @param int $parentId
	 *        	product version id OR product instance id
	 * @param array $select
	 * @return \PDOStatement
	 */
	public function getChildrenFromFilter($select = null, $filter = null, $bind=array())
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;

		if ( !$this->loadChildrenStmt || $this->loadChildrenFilter != $filter ){
			if ( !$select ) {
				$productMetaModel = array(
					'description' => 'description',
					'document_uid' => 'documentUid',
					'document_id' => 'documentId',
					'spacename' => 'spaceName',
					'version_id' => 'version',
					'number' => 'productNumber'
				);
				foreach( $this->metaModel as $asSys => $asApp ) {
					$select[] = 'instance.' . $asSys . ' AS ' . $asApp;
				}
				foreach( $productMetaModel as $asSys => $asApp ) {
					$select[] = 'child.' . $asSys . ' AS ' . $asApp;
				}
			}

			$selectStr = implode($select, ',');

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			LEFT OUTER JOIN $productTable as parent ON instance.parent_id=parent.id
			LEFT OUTER JOIN $productTable as child ON instance.of_product_id=child.id";

			if ( $filter ) {
				$sql .= ' WHERE ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadChildrenStmt = $stmt;
			$this->loadChildrenFilter = $filter;
		}

		$this->loadChildrenStmt->execute($bind);
		return $this->loadChildrenStmt;
	}

	/**
	 * Get children instances of a product
	 *
	 * You may use alias instance.
	 * for select instance table
	 * You may use alias parent. for select parent product table
	 * You may use alias child. for select child product table (product of the instance)
	 *
	 * @param int $parentId
	 *        	product version id OR product instance id
	 * @param array $select
	 * @return \PDOStatement
	 */
	public function getChildren( $parentId, $select=null, $filter=null, $bind=array() )
	{
		if ( $filter ) {
			$filter = 'parent_id=:parentId AND ' . $filter;
		}
		else{
			$filter = 'parent_id=:parentId';
		}

		$bind[':parentId'] = $parentId;
		return $this->getChildrenFromFilter($select, $filter, $bind);
	}

	/**
	 * Get all children instance of a product including the ou children
	 *
	 * You may use alias instance.
	 * for select instance table
	 * You may use alias parent. for select parent product table
	 * You may use alias child. for select child product table (product of the instance)
	 *
	 * @param int $parentId
	 *        	product version id OR product instance id
	 * @param array $select
	 * @return \PDOStatement
	 */
	public function getFullChildren( $parentId, $select=null, $filter=null, $bind=array() )
	{
		if ( $filter ) {
			$filter = 'instance.path LIKE :path AND ' . $filter;
		}
		else{
			$filter = 'instance.path LIKE :path';
		}
		$bind[':path'] = $parentId . '.%';
		return $this->getChildrenFromFilter($select, $filter, $bind);
	}

	/**
	 *
	 * @param int $parentId
	 * @param array $select
	 * @return \PDOStatement
	 */
	public function getParents($childId, $select = null, $filter = null)
	{
		$instanceTable = $this->_table;
		$productTable = $this->_parentTable;

		if ( !$this->loadParentsStmt ) {
			if ( !$select ) {
				$productMetaModel = array(
					'description' => 'description',
					'document_uid' => 'documentUid',
					'document_id' => 'documentId',
					'spacename' => 'spaceName',
					'version_id' => 'version',
					'number' => 'productNumber'
				);
				foreach( $this->metaModel as $asSys => $asApp ) {
					$select[] = 'instance.' . $asSys . ' AS ' . $asApp;
				}
				foreach( $productMetaModel as $asSys => $asApp ) {
					$select[] = 'child.' . $asSys . ' AS ' . $asApp;
				}
			}

			$selectStr = implode($select, ',');

			$sql = "SELECT $selectStr
			FROM $instanceTable as instance
			JOIN $productTable as product ON product.id=instance.of_product_id
			JOIN $productTable as parent ON parent.id=instance.parent_id
			WHERE product.id=:childId";

			if ( $filter ) {
				$sql .= ' AND ' . $filter;
			}

			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->loadParentsStmt = $stmt;
		}

		$this->loadParentsStmt->execute(array(
			':childId' => $childId
		));
		return $this->loadParentsStmt;
	}

	/**
	 * @return Dao
	 */
	public function deleteFromParentId($parentId, $withChildren=false, $withTrans=null)
	{
		$toSysId = $this->toSys('parentId');
		$filter = "$toSysId=:parentId";
		$bind = array(':parentId'=>$parentId);

		$this->_deleteFromFilter($filter, $bind, $withChildren, $withTrans);
		return $this;
	}



} //End of class
