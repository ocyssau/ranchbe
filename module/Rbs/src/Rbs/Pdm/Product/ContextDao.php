<?php
//%LICENCE_HEADER%

namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Pdm;

/** SQL_SCRIPT>>
CREATE TABLE `pdm_context_application` (
 `id` int(11) NOT NULL,
 `uid` varchar(32) NOT NULL,
 `cid` varchar(32) NOT NULL DEFAULT '569e9714da9aa',
 `name` varchar(128) NULL,
 PRIMARY KEY  (`id`),
 UNIQUE KEY `INDEX_pdm_context_application_1` (`uid`),
 KEY `INDEX_pdm_context_application_2` (`cid`),
 KEY `INDEX_pdm_context_application_3` (`name`)
);
<<*/

/** SQL_INSERT>>
INSERT INTO pdm_context_application(`id`, `uid`, `name`)
VALUES
	(1,'mecanical', 'mecanical'),
	(2,'design', 'design'),
	(3,'marketing', 'marketing');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class ContextDao extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table='pdm_context_application';
	public static $vtable='pdm_context_application';

	public static $sequenceName = 'pdm_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
	);

} //End of class
