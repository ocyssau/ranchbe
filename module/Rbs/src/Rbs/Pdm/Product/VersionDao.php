<?php
//%LICENCE_HEADER%

namespace Rbs\Pdm\Product;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Pdm;
use Rbplm\Dao\Exception;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE `pdm_product_version` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `number` varchar(128) NOT NULL,
  `description` varchar(512) NULL,
  `version_id` int(11) NOT NULL,
  `of_product_id` int(11) NULL,
  `of_product_uid` varchar(32) NULL,
  `document_id` int(11) NULL,
  `document_uid` varchar(32) NULL,
  `spacename` varchar(32) NULL,
  `type` varchar(64) NULL,
  `materials` varchar(512) NULL,
  `weight` float NULL,
  `volume` float NULL,
  `wetsurface` float NULL,
  `density` float NULL,
  `gravitycenter` varchar(512) NULL,
  `inertiacenter` varchar(512) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdm_product_version_uniq1` (`of_product_uid`,`version_id`),
  UNIQUE KEY `pdm_product_version_uniq2` (`uid`),
  KEY `INDEX_pdm_product_version_1` (`of_product_uid`),
  KEY `INDEX_pdm_product_version_6` (`of_product_id`),
  KEY `INDEX_pdm_product_version_2` (`version_id`),
  KEY `INDEX_pdm_product_version_3` (`description`),
  KEY `INDEX_pdm_product_version_4` (`uid`),
  KEY `INDEX_pdm_product_version_5` (`name`),
  KEY `INDEX_pdm_product_version_7` (`document_id`),
  KEY `INDEX_pdm_product_version_8` (`document_uid`),
  KEY `INDEX_pdm_product_version_9` (`spacename`),
  KEY `INDEX_pdm_product_instance_10` (`type`)
);

CREATE TABLE `pdm_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO pdm_seq(id) VALUES(10);

<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE pdm_product_version;
 <<*/

/**
 * @brief Dao class
 *
 * @see \Rbs\Dao\Sier
 *
 */
class VersionDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='pdm_product_version';
	public static $vtable='pdm_product_version';

	public static $sequenceName = 'pdm_seq';
	public static $sequenceKey = 'id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'name'=>'name',
		'number'=>'number',
		'description'=>'description',
		'of_product_uid'=>'ofProductUid',
		'of_product_id'=>'ofProductId',
		'document_uid'=>'documentUid',
		'document_id'=>'documentId',
		'spacename'=>'spaceName',
		'type'=>'type',
		'version_id'=>'version',
		'materials'=>'materials',
		'weight'=>'weight',
		'volume'=>'volume',
		'wetsurface'=>'wetSurface',
		'density'=>'density',
		'gravitycenter'=>'gravityCenter',
		'inertiacenter'=>'inertiaCenter',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'materials'=>'json',
		'gravitycenter'=>'json',
		'inertiacenter'=>'json',
	);

	/**
	 * If $versionId is null, load the last version
	 *
	 * @param MappedInterface $mapped
	 * @param string $number
	 * @param integer $versionId
	 * @return \Rbplm\Dao\MappedInterface
	 */
	public function loadFromNumber(MappedInterface $mapped, $number, $versionId=null)
	{
		if($versionId){
			$filter = 'obj.number=:number AND obj.version_id=:versionId LIMIT 1';
			$bind = array(':number'=>$number, ':versionId'=>$versionId);
		}
		else{
			$filter = 'obj.number=:number ORDER BY version_id DESC LIMIT 1';
			$bind = array(':number'=>$number);
		}
		return $this->load($mapped, $filter, $bind );
	} //End of function

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.DaoInterface::save()
	 */
	public function save( MappedInterface $mapped )
	{

		if(!isset($mapped->documentId)){
			try{
				$document = $mapped->getDocument();
				if($document){
					$mapped->setDocument($document);
				}
			}
			catch(\Exception $e){
				throw new Exception('documentId is not set');
			}
		}

		if(!isset($mapped->ofProductId)){
			try{
				$product = $mapped->getOfProduct();
				if($product){
					$mapped->setOfProduct($product);
				}
			}
			catch(\Exception $e){
				throw new Exception('productId is not set');
			}
		}

		return parent::save($mapped);
	} //End of function

} //End of class

