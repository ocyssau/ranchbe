<?php

namespace Rbs\Pdm\Product;

use Rbplm\Pdm\Product;

class Service
{
	public $factory;
	public $result;

	/**
	 *
	 * @param Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
		$this->result = new \Rbs\Pdm\Service\Result();
	}

	/**
	 * @throw Exception
	 * @return Service
	 *
	 */
	public function checkout($product)
	{
		$factory = $this->factory;
		return $this;
	}

	/**
	 * @throw Exception
	 * @return Service
	 */
	public function checkin($product, $releasing=true, $updateData=true, $checkAccess=true)
	{
		$factory = $this->factory;

		/* @var \Rbs\Pdm\Product\VersionDao $productDao */
		$productDao = $factory->getDao($product);

		/* @var \Rbs\Pdm\Product\InstanceDao $instanceDao */
		$instanceDao = $factory->getDao(Product\Instance::$classId);

		/* OPEN TRANSACTION */
		/** @var \PDO $connexion */
		$connexion = $factory->getConnexion();
		$inTrans = $connexion->inTransaction();
		if($inTrans==false){
			$connexion->beginTransaction();
		}

		/* DELETE PREVIOUS INSTANCES FOR THE PRODUCT */
		try{
			/* load previous instance children */
			//$previous = $instanceDao->getChildren($product->getId(), array('instance.id'));
			$instanceDao->deleteFromParentId($product->getId());
		}
		catch(\Exception $e){
			throw $e;
		}

		/* SAVE PRODUCT VERSION */
		if( !$product->getId() ){
			$this->result->feedback('create product : '.$product->getNumber());
		}
		else{
			$this->result->feedback('save product : '.$product->getNumber());
		}

		try{
			$productDao->save($product);
			$this->result->feedback('The product '.$product->getNumber().' has been saved');
		}
		catch(\Exception $e){
			$this->result->error('Can not save the product '.$product->getNumber() . ' : ' . $e->getMessage());
			$connexion->rollBack();
			throw $e;
		}

		/* SAVE CHILDREN INSTANCES */
		if($product->getChildren()){
			/* @var \Rbplm\Pdm\Product\Instance $child */
			foreach($product->getChildren() as $child){
				$number = $child->getNumber();
				$child->setParent($product);
				$ofProduct = $child->getOfProduct();
				$productDao->save($ofProduct);

				$child->setOfProduct($ofProduct);
				$instanceDao->save($child);
			}
		}

		if($inTrans==false){
			$connexion->commit();
		}

		return $this;
	}

	/**
	 * @throw Exception
	 * @return Service
	 */
	function delete($product)
	{
		$factory = $this->factory;
		return $this;
	}

	/**
	 */
	public function create($product, $lock=false)
	{
		$factory = $this->factory;
		$productDao = $factory->getDao($product);

		//OPEN TRANSACTION
		$connexion = $factory->getConnexion();
		$connexion->beginTransaction();

		/* SAVE PRODUCT VERSION */
		if( !$product->getId() ){
			$this->result->feedback('create product : '.$product->getNumber());
			try{
				$productDao->save($product);
				$this->result->feedback('The product '.$product->getNumber().' has been created');
			}
			catch(\Exception $e){
				$this->result->error('Can not create the doc '.$product->getNumber() . ' : ' . $e->getMessage());
				$connexion->rollBack();
				return $this;
			}
		} //End of create doc

		/* SAVE CHILDREN INSTANCES */
		if($product->getChildren()){
			$instanceDao = $factory->getDao(Product\Instance::$classId);
			foreach($product->getChildren() as $child){
				$child->setParent($product);
				$instanceDao->save($child);
			}
		}

		$connexion->commit();
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function newVersion($product)
	{
		$factory = $this->factory;
		return $this;
	}
}
