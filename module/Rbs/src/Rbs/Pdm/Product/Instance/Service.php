<?php

namespace Rbs\Pdm\Product\Instance;

class Service
{

	/**
	 *
	 * @param Factory $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * @throw Exception
	 * @return Service
	 *
	 */
	public function checkout($product)
	{
		$factory = $this->factory;
		return $this;
	} //End of method

	/**
	 * @throw Exception
	 * @return Service
	 */
	public function checkin($product, $releasing=true, $updateData=true, $checkAccess=true)
	{
		$factory = $this->factory;
		return $this;
	}

	/**
	 * @throw Exception
	 * @return Service
	 */
	function delete($product)
	{
		$factory = $this->factory;
		return $this;
	}//End of method

	/**
	 */
	public function create($product, $lock=false)
	{
		$factory = $this->factory;
		return $this;
	} //End of function

	/**
	 *
	 * @return boolean
	 */
	public function newVersion($product)
	{
		$factory = $this->factory;
		return $this;
	} //End of function
}
