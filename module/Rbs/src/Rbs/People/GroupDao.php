<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
CREATE TABLE `liveuser_groups` (
	`group_id` int(11) NOT NULL,
	`uid` varchar(30) NOT NULL,
	`group_type` int(11) DEFAULT '0',
	`group_define_name` varchar(32) DEFAULT NULL,
	`group_description` varchar(64) DEFAULT NULL,
	`is_active` tinyint(1) DEFAULT '1',
	`owner_user_id` int(11) DEFAULT '0',
	`owner_group_id` int(11) DEFAULT '0',
	PRIMARY KEY (`group_id`),
	UNIQUE KEY `uid_UNIQUE` (`uid`),
	UNIQUE KEY `define_name_i_idx` (`group_define_name`)
);
<<*/

/** SQL_INSERT>>
 INSERT INTO `TABLE` (`group_id`,`group_type`,`group_define_name`,`group_description`,`is_active`,`owner_user_id`,`owner_group_id`) VALUES (1,0,'concepteurs','Concepteurs CAO',1,0,0);
 INSERT INTO `TABLE` (`group_id`,`group_type`,`group_define_name`,`group_description`,`is_active`,`owner_user_id`,`owner_group_id`) VALUES (2,0,'groupLeader','chef de groupe de conception',1,0,0);
 INSERT INTO `TABLE` (`group_id`,`group_type`,`group_define_name`,`group_description`,`is_active`,`owner_user_id`,`owner_group_id`) VALUES (3,0,'Leader','Charg? d\'affaire',1,0,0);
 INSERT INTO `TABLE` (`group_id`,`group_type`,`group_define_name`,`group_description`,`is_active`,`owner_user_id`,`owner_group_id`) VALUES (4,0,'checkers','verificateurs des projets CAO',1,0,0);
 <<*/

/** SQL_ALTER>>
<<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/**
 * @brief Dao class for \Rbplm\People\Group
 *
 * See the examples: Rbplm/People/GroupTest.php
 *
 * @see \Rbplm\Dao\Pg
 * @see \Rbplm\People\GroupTest
 *
 */
class GroupDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'liveuser_groups';

	public static $sequenceName='liveuser_groups_seq';
	public static $sequenceKey='sequence';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'group_id'=>'id',
		'uid'=>'uid',
		'group_define_name'=>'name',
		'group_description'=>'description',
		'is_active'=>'active',
	);

} //End of class