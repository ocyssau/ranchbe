<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;

/** SQL_SCRIPT>>
CREATE TABLE `acl_users` (
`id` varchar(32) NOT NULL,
`uid` varchar(30) NOT NULL,
`login` varchar(32) NOT NULL,
`lastname` VARCHAR(64) DEFAULT NULL,
`firstname` VARCHAR(64) DEFAULT NULL,
`password` varchar(32) DEFAULT NULL,
`mail` varchar(128) DEFAULT NULL,
`owner_id` int(11) DEFAULT NULL,
`primary_role_id` int(11) DEFAULT NULL,
`lastlogin` DATETIME DEFAULT '1970-01-01 00:00:00',
`is_active` tinyint(1) DEFAULT '1',
PRIMARY KEY (`id`),
KEY `login_idx` (`login`),
UNIQUE KEY `uid_idx` (`uid`)
);

CREATE TABLE `acl_users_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class UserDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='acl_users';
	public static $vtable='acl_users';

	/**
	 * @var string
	 */
	public static $sequenceName='acl_users_seq';
	public static $sequenceKey='id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'login'=>'login',
		'lastname'=>'lastname',
		'firstname'=>'firstname',
		'password'=>'password',
		'mail'=>'mail',
		'owner_id'=>'ownerId',
		'primary_role_id'=>'primaryRoleId',
		'lastlogin'=>'lastLogin',
		'is_active'=>'active',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'lastlogin'=>'datetime',
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	} //End of function

	/**
	 * @see DaoInterface::loadFromName()
	 */
	public function loadFromLogin(MappedInterface $mapped, $name)
	{
		$filter = 'obj.'.$this->toSys('login').'=:name';
		$bind = array(':name'=>$name);
		return $this->load($mapped, $filter, $bind );
	} //End of function


	/**
	 * @param string $in
	 * @return \DateTime
	 */
	public static function datetimeToApp($string)
	{
		if($string == ''){
			return new \Rbplm\Sys\Date();
		}
		else{
			return new \Rbplm\Sys\Date($string);
		}
	}

	/**
	 * @param \DateTime $in
	 */
	public static function datetimeToSys($in)
	{
		if($in instanceof \DateTime){
			return $in->format();
		}
		elseif($in == 0){
			return null;
		}
		elseif(is_string($in)){
			$date = new \Rbplm\Sys\Date($in);
			return $date->format(self::DATE_FORMAT);
		}
	}

}
