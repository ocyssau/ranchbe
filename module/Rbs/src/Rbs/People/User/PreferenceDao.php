<?php

namespace Rbs\People\User;

/* Version 1
 CREATE TABLE `user_prefs` (
 	`pref_id` int(11) NOT NULL,
 	`user_id` int(11) NOT NULL,
 	`pref_name` varchar(32)  NOT NULL default '',
 	`pref_value` varchar(128)  default NULL,
 	PRIMARY KEY  (`pref_id`),
 	UNIQUE KEY `UC_user_prefs` (`user_id`,`pref_name`)
 ) ENGINE=InnoDB;
*/

/* Version 2 = actual
 CREATE TABLE `user_prefs` (
 	`user_id` int(11) NOT NULL,
 	`css_sheet` varchar(32)  NOT NULL default 'default',
 	`lang` varchar(32)  NOT NULL default 'default',
 	`long_date_format` varchar(32)  NOT NULL default 'default',
 	`short_date_format` varchar(32)  NOT NULL default 'default',
 	`hour_format` varchar(32)  NOT NULL default 'default',
 	`time_zone` varchar(32)  NOT NULL default 'default',
 	`wildspace_path` varchar(128)  NOT NULL default 'default',
 	`max_record` varchar(128)  NOT NULL default 'default',
 	PRIMARY KEY  (`user_id`)
 ) ENGINE=InnoDB;

ALTER TABLE `user_prefs`
ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) REFERENCES `liveuser_perm_users` (`perm_user_id`);
*/

/**
 * manage user preferences
*/
class PreferenceDao extends \Rbs\Dao\Sier
{

	/**
	 *
	 * @var string
	 */
	public static $table='user_prefs';
	public static $vtable='user_prefs';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'user_id'=>'ownerId',
		'css_sheet'=>'cssSheet',
		'lang'=>'lang',
		'long_date_format'=>'longDateFormat',
		'short_date_format'=>'shortDateFormat',
		'hour_format'=>'hourFormat',
		'time_zone'=>'timeZone',
		'max_record'=>'maxRecord'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	} //End of function

	/**
	 * Load the preferences from the owner.
	 *
	 * @param $mapped
	 * @param $ownerUid
	 * @return Preference
	 */
	public function loadFromOwnerId($mapped, $ownerId)
	{
		$filter = 'obj.user_id=:owner';
		$bind = array(':owner'=>$ownerId);
		return $this->load($mapped, $filter, $bind );
	} //End of function

} //End of class
