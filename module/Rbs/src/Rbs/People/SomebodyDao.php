<?php
//%LICENCE_HEADER%

namespace Rbs\People;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Dao\Exception;
use Rbplm\Dao\ExistingException;

/** SQL_SCRIPT>>
CREATE TABLE `partners` (
`partner_id` int(11) NOT NULL DEFAULT '0',
`partner_number` varchar(32) NOT NULL DEFAULT '',
`partner_type` enum('customer','supplier','staff') DEFAULT NULL,
`first_name` varchar(64) DEFAULT NULL,
`last_name` varchar(64) DEFAULT NULL,
`adress` varchar(64) DEFAULT NULL,
`city` varchar(64) DEFAULT NULL,
`zip_code` int(11) DEFAULT NULL,
`phone` varchar(64) DEFAULT NULL,
`cell_phone` varchar(64) DEFAULT NULL,
`mail` varchar(64) DEFAULT NULL,
`web_site` varchar(64) DEFAULT NULL,
`activity` varchar(64) DEFAULT NULL,
`company` varchar(64) DEFAULT NULL,
PRIMARY KEY (`partner_id`),
UNIQUE KEY `UC_partner_number` (`partner_number`)
) ENGINE=InnoDB;

CREATE TABLE `partners_seq` (
`id` int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id`)
)ENGINE=MyIsam AUTO_INCREMENT=10;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class SomebodyDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='partners';
	public static $vtable='partners';

	/**
	 * @var string
	 */
	public static $sequenceName='partners_seq';
	public static $sequenceKey='id';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'partner_id'=>'id',
		'partner_number'=>'uid',
		'first_name'=>'firstname',
		'last_name'=>'lastname',
		'adress'=>'adress',
		'mail'=>'mail',
		'city'=>'city',
		'zip_code'=>'zipcode',
		'web_site'=>'website',
		'phone'=>'phone',
		'cell_phone'=>'cellphone',
		'activity'=>'activity',
		'company'=>'company',
		'partner_type'=>'type',
	);

	/**
	 * @var array
	 */
	public static $sysToAppFilter = array(
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	} //End of function

	/**
	 * Compose the partner number from the first and last name.
	 * return number, else return false.
	 *
	 * @param string $firstName
	 * @param string $lastName
	 */
	static function composeNumber($firstName , $lastName)
	{
		if(!empty($firstName) && !empty($lastName) ){
			$separator = '_';
		}
		return $partner_number = self::noAccent($firstName . $separator . $lastName);
	}//End of method

	/**
	 * Suppress accent of the input string.
	 * return the input without accents, else return false.
	 *
	 * @param string $in input string.
	 */
	static function noAccent($in)
	{
		//thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
		$search = array ('@[����]@','@[���]@','@[��]@','@[���]@','@[��]@','@[����]@','@[���]@','@[��]@','@[��]@','@[��]@','@[�]@i','@[�]@i','@[ ]@i','@[^a-zA-Z0-9_]@');
		$replace = array ('e','a','i','u','o','E','A','I','U','O','c','C','_','');
		return preg_replace($search, $replace, $in);
	}//End of method

}
