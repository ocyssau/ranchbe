<?php
//%LICENCE_HEADER%

namespace Rbs\History;

use Rbplm\Any;
use Rbplm\People;

use Rbplm\Mapped;
use Rbplm\Dao\MappedInterface;

/**
 * @brief Organizational unit.
 *
 * Xtract from wikipedia:
 *
 * OU provides a way of classifying objects located in directories, or names in a digital certificate hierarchy,
 * typically used either to differentiate between objects with the same name
 * (John Doe in OU "marketing" versus John Doe in OU "customer service"),
 * or to parcel out authority to create and manage objects (for example: to give rights for user-creation to local
 * technicians instead of having to manage all accounts from a single central group).
 *
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 * @Entity
 * @Table(name="org_unit")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="cid", type="integer")
 *
 */
class AbstractHistory extends Any implements MappedInterface
{
	use Mapped;


	static $classId = '598a45cf2hist';

	/**
	 *
	 * @var Action
	 */
	protected $action;

	/**
	 *
	 * @var array
	 */
	protected $data;

	/**
	 *
	 * @var Dao
	 */
	public $dao;

	/**
	 *
	 * @param array $properties
	 * @param Any $parent
	 */
	public function __construct($properties=null, $parent=null)
	{
		parent::__construct($properties, $parent);
		$this->action = new Action();
	}

	/**
	 * @param string $name
	 * @return AnyObject
	 */
	public static function init()
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$action = $obj->getAction();
		$action->newUid();
		$action->setCreated(new \DateTime);
		$action->setOwner(People\CurrentUser::get());
		$name = uniqid();
		$obj->setName($name);
		$action->setName($name);
		return $obj;
	}

	/**
	 * @param string|array $event
	 * @param Any $sender
	 * @param string $actionName
	 * @return AnyObject
	 */
	public function onEvent($event='', $sender, $actionName=null)
	{
		if(!$actionName){
			if(is_string($event)){
				$actionName = explode('.', $event)[0];
			}
			else{
				$actionName = explode('.', $event->name)[0];
			}
		}

		$this->getAction()->setName($actionName);
		$this->setData($sender->getArrayCopy());

		if($this->dao){
			$this->dao->save($this);
		}
		else{
			throw new \Exception('$this->dao is not set');
		}
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['action'])) ? $this->action->hydrate($properties['action']) : null;
		(isset($properties['data'])) ? $this->data=$properties['data'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		return $this;
	} //End of function

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		$ret = $this->__serialize();
		$ret['action'] = $this->action->__serialize();
		return $ret;
	} //End of function

	/**
	 * @param Action $action
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 *
	 * @param array $data
	 */
	public function setData($data)
	{
		/*Clean references*/
		$return=array();
		foreach($data as $name=>$value){
			if($value instanceof Any){
				$return[$name] = $value->getUid();
			}
			elseif(is_scalar($value)){
				$return[$name] = $value;
			}
		}
		$this->data = $return;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 *
	 * @param Dao $dao
	 */
	public function setDao($dao)
	{
		$this->dao = $dao;
		return $this;
	}

}//End of class
