<?php

//%LICENCE_HEADER%

namespace Rbs\History;

use Rbplm\Any;
use Rbplm\Owned;
use DateTime;

/**
 *
 */
class Action extends Any
{

	use Owned;

	static $classId = '598a45cf2acti';

	/**
	 * @var DateTime
	 */
	protected $created = null;

	/**
	 * @var string
	 */
	protected $comment = null;

	/**
	 * @param DateTime $date
	 * @return Any
	 */
	public function setCreated(\DateTime $date)
	{
		$this->created = $date;
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getCreated($format=null)
	{
		if(!$this->created instanceof DateTime){
			$this->created = new DateTime();
		}

		if($format){
			return $this->created->format($format);
		}
		else{
			return $this->created;
		}
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate( array $properties )
	{
		$this->ownedHydrate($properties);
		(isset($properties['created'])) ? $this->setCreated( new DateTime($properties['created']) ) : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['comment'])) ? $this->comment=$properties['comment'] : null;
		return $this;
	} //End of function

	/**
	 *
	 * @param array $datas
	 */
	public function setDatas($datas)
	{
		$this->datas = $datas;
		return $this;
	}

	/**
	 *
	 * @return array $datas
	 */
	public function getDatas()
	{
		return $this->datas;
	}

	/**
	 * @param string
	 */
	public function setComment($string)
	{
		$this->comment = $string;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

}//End of class
