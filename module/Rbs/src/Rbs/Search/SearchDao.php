<?php
//%LICENCE_HEADER%

namespace Rbs\Search;

use Rbs\Dao\Sier as DaoSier;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW `objects` AS
	SELECT
		`document_id` as `id`,
		`document_name` as `name`,
		`document_number` as `number`,
		`uid` as `uid`,
		`designation` as `designation`,
		"workitem" as `spacename`,
		"569e92709feb6" as `cid`,
		 `check_out_by` as lockById,
		 `check_out_date` as locked,
		 `update_date` as updated,
		 `update_by` as updateById,
		 `open_date` as created,
		 `open_by` as createById
		FROM workitem_documents
	UNION
	SELECT
		`document_id` as `id`,
		`document_name` as `name`,
		`document_number` as `number`,
		`uid` as `uid`,
		`designation` as `designation`,
		"bookshop" as `spacename`,
		"569e92709feb6" as `cid`,
		 `check_out_by` as lockById,
		 `check_out_date` as locked,
		 `update_date` as updated,
		 `update_by` as updateById,
		 `open_date` as created,
		 `open_by` as createById
		FROM bookshop_documents
	UNION
	SELECT
		`document_id` as `id`,
		`document_name` as `name`,
		`document_number` as `number`,
		`uid` as `uid`,
		`designation` as `designation`,
		"cadlib" as `spacename`,
		"569e92709feb6" as `cid`,
		 `check_out_by` as lockById,
		 `check_out_date` as locked,
		 `update_date` as updated,
		 `update_by` as updateById,
		 `open_date` as created,
		 `open_by` as createById
		FROM cadlib_documents
	UNION
	SELECT
		`document_id` as `id`,
		`document_name` as `name`,
		`document_number` as `number`,
		`uid` as `uid`,
		`designation` as `designation`,
		"mockup" as `spacename`,
		"569e92709feb6" as `cid`,
		 `check_out_by` as lockById,
		 `check_out_date` as locked,
		 `update_date` as updated,
		 `update_by` as updateById,
		 `open_date` as created,
		 `open_by` as createById
		FROM mockup_documents
	-- DOCFILES
	UNION
	SELECT
		`file_id` as `id`,
		`file_name` as `name`,
		`file_name` as `number`,
		`uid` as `uid`,
		"" as `designation`,
		"workitem" as `spacename`,
		"569e92b86d248" as `cid`,
		`file_checkout_by` as `lockById`,
		`file_checkout_date` as `locked`,
		`file_update_date` as `updated`,
		`file_update_by` as `updateById`,
		`file_open_date` as `created`,
		`file_open_by` as `createById`
	FROM workitem_doc_files
	UNION
	SELECT
		`file_id` as `id`,
		`file_name` as `name`,
		`file_name` as `number`,
		`uid` as `uid`,
		"" as `designation`,
		"bookshop" as `spacename`,
		"569e92b86d248" as `cid`,
		`file_checkout_by` as `lockById`,
		`file_checkout_date` as `locked`,
		`file_update_date` as `updated`,
		`file_update_by` as `updateById`,
		`file_open_date` as `created`,
		`file_open_by` as `createById`
	FROM bookshop_doc_files
	UNION
	SELECT
		`file_id` as `id`,
		`file_name` as `name`,
		`file_name` as `number`,
		`uid` as `uid`,
		"" as `designation`,
		"cadlib" as `spacename`,
		"569e92b86d248" as `cid`,
		`file_checkout_by` as `lockById`,
		`file_checkout_date` as `locked`,
		`file_update_date` as `updated`,
		`file_update_by` as `updateById`,
		`file_open_date` as `created`,
		`file_open_by` as `createById`
	FROM cadlib_doc_files
	UNION
	SELECT
		`file_id` as `id`,
		`file_name` as `name`,
		`file_name` as `number`,
		`uid` as `uid`,
		"" as `designation`,
		"cadlib" as `spacename`,
		"569e92b86d248" as `cid`,
		`file_checkout_by` as `lockById`,
		`file_checkout_date` as `locked`,
		`file_update_date` as `updated`,
		`file_update_by` as `updateById`,
		`file_open_date` as `created`,
		`file_open_by` as `createById`
		FROM mockup_doc_files

	-- CONTAINERS
	UNION
	SELECT
		`workitem_id` as `id`,
		`name` as `name`,
		`workitem_number` as `number`,
		`workitem_number` as `uid`,
		`workitem_description` as `designation`,
		"workitem" as `spacename`,
		"569e94192201a" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		`open_date` as `created`,
		`open_by` as `createById`
	FROM workitems
	UNION
	SELECT
		`bookshop_id` as `id`,
		`name` as `name`,
		`bookshop_number` as `number`,
		`bookshop_number` as `uid`,
		`bookshop_description` as `designation`,
		"bookshop" as `spacename`,
		"569e94192201a" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		`open_date` as `created`,
		`open_by` as `createById`
		FROM bookshops
	UNION
	SELECT
		`cadlib_id` as `id`,
		`name` as `name`,
		`cadlib_number` as `number`,
		`cadlib_number` as `uid`,
		`cadlib_description` as `designation`,
		"cadlib" as `spacename`,
		"569e94192201a" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		`open_date` as `created`,
		`open_by` as `createById`
		FROM cadlibs
	UNION
	SELECT
		`mockup_id` as `id`,
		`name` as `name`,
		`mockup_number` as `number`,
		`mockup_number` as `uid`,
		`mockup_description` as `designation`,
		"mockup" as `spacename`,
		"569e94192201a" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		`open_date` as `created`,
		`open_by` as `createById`
	FROM mockups

	-- PROJECT
	UNION
	SELECT
		`project_id` as `id`,
		`name` as `name`,
		`project_number` as `number`,
		`project_number` as `uid`,
		`project_description` as `designation`,
		"default" as `spacename`,
		"569e93c6ee156" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		`open_date` as `created`,
		`open_by` as `createById`
	FROM projects

	-- PRODUCT
	UNION
	SELECT
		`id` as `id`,
		`name` as `name`,
		`number` as `number`,
		`uid` as `uid`,
		`description` as `designation`,
		`spacename` as `spacename`,
		"569e972dd4c2c" as `cid`,
		"" as `lockById`,
		"" as `locked`,
		"" as `updated`,
		"" as `updateById`,
		"" as `created`,
		"" as `createById`
	FROM pdm_product_version;
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class SearchDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='objects';
	public static $vtable='objects';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'number'=>'number',
		'designation'=>'designation',
		'spacename'=>'spacename',
	);
}
