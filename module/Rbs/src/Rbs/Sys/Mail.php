<?php
//%LICENCE_HEADER%

namespace Rbs\Sys;

use Rbs\Sys\Mail\Transport;

/**
 * @brief Extends \Zend\Mail\Message to manip mails in RanchBE.
 */
class Mail extends \Zend\Mail\Message
{
	/**
	 *
	 * @return \Rbs\Sys\Mail
	 */
	public function send()
	{
		$transport = Transport::get();
		$transport->send($this);
		return $this;
	}
} //End of class
