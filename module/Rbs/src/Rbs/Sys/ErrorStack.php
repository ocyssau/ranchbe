<?php
//%LICENCE_HEADER%

namespace Rbs\Sys;

use Rbplm\Sys\Error;
use Rbs\Sys\FlashMessenger;
use Rbplm\Sys\Logger;

/**
 */
class ErrorStack
{
	protected $flash;
	protected $logger;
	private static $instance;

	/**
	 *
	 */
	public function __construct()
	{
		$this->flash = new FlashMessenger();
		$this->logger = Logger::singleton();
	}

	/**
	 * Singleton method.
	 * @return ErrorStack
	 */
	public static function singleton()
	{
		if (self::$instance === null) {
			self::$instance = new ErrorStack();
		}
		return self::$instance;
	}

	/**
	 *
	 * @param string $message
	 * @param array $args
	 * @return string
	 */
	protected static function formatMessage($message, $args)
	{
		if(is_array($args)){
			foreach($args as $key=>$val){
				$message = str_replace('%'.$key.'%', $val, $message);
			}
		}
		else if(is_string($args)){
			$message = str_replace('%0%', $args, $message);
		}
		return $message;
	}

	/**
	 * @param string $message
	 * @param integer $priority must be an integer >= 0 and < 8
	 */
	public function log($message, $priority=1)
	{
		if($this->logger){
			$this->logger->log($message, $priority);
		}
	}

	/**
	 * Compatibility with PEAR error stack
	 */
	public function push($code, $level = Error::ERROR, $args = array(), $message = false)
	{
		if($level == Error::ERROR){
			$this->error($message);
		}
		elseif($level == Error::WARNING){
			$this->warning($message);
		}
		elseif($level == Error::NOTICE){
			$this->log(self::formatMessage($message,$args));
		}
		return $this;
	}

	/**
	 *
	 */
	public function error($message = false, $args=null)
	{
		$message = self::formatMessage($message,$args);
		$this->log($message);
		$this->flash->addError($message);
		return $this;
	}

	/**
	 */
	public function warning($message = false, $args=null)
	{
		$message = self::formatMessage($message,$args);
		$this->log($message);
		$this->flash->addWarning($message);
		return $this;
	}

	/**
	 */
	public function feedback($message = false, $args=null)
	{
		$message = self::formatMessage($message,$args);
		$this->log($message);
		$this->flash->addMessage($message);
		return $this;
	}

	/**
	 */
	public function success($message = false, $args=null)
	{
		$message = self::formatMessage($message,$args);
		$this->log($message);
		$this->flash->addSuccess($message);
		return $this;
	}

	/**
	 *
	 */
	public function hasErrors()
	{
		return $this->flash->hasError();
	} //End of method

	/**
	 *
	 */
	public function getFlash()
	{
		return $this->flash;
	} //End of method
}
