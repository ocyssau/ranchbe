<?php
namespace Rbs\Sys;

class DateUtility
{

	/**
	 *
	 */
	public static function getNowdate()
	{
		//Take current date en return the date in lisible format
		$now['TimeStamp']=time();
		$format=LONG_DATE_FORMAT;
		$now['formated'] = strftime($format,$now['TimeStamp']);
		return $now;
	} //End

	/**
	 *
	 * @param unknown_type $timestamp
	 * @return boolean
	 */
	public static function formatDate($timestamp)
	{
		//Take current date en return the date in lisible format
		if(!is_null($timestamp)){
			$format=LONG_DATE_FORMAT;
			return strftime($format,$timestamp);
		}else return false;
	} //End

	/**
	 *
	 * @param timestamp $TimeStamp
	 */
	public static function getDateFromTS($TimeStamp)
	{
		$date['d']   = date(d , $TimeStamp );
		$date['M'] = date(M , $TimeStamp );
		$date['Y']  = date(Y , $TimeStamp );
		return $date;
	} //End

	/**
	 *
	 * @param string $SqlDate
	 * @return unknown
	 */
	public static function getDateFromMysql($SqlDate)
	{
		//mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
		mktime ($SqlDate);
		$date['d']   = date(d , $TimeStamp );
		$date['M'] = date(M , $TimeStamp );
		$date['Y']  = date(Y , $TimeStamp );
		return $date;
	} //End

	/**
	 *
	 * @param unknown_type $date
	 */
	public static function getTSFromDate( $date )
	{
		//  [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]]
		$timeStamp = mktime ( 0 , 0 , 0 , $date['M'] ,  $date['d'] , $date['Y'] );
		return $timeStamp;
	} //End

	/**
	 *
	 * @param unknown_type $string
	 * @return number
	 */
	public static function makeTimestamp($string)
	{
		if(empty($string)) {
			$time = time(); // Use now

		}
		elseif (preg_match('/^\d{14}$/', $string)) {
			// it is mysql timestamp format of YYYYMMDDHHMMSS?
			$time = mktime(substr($string, 8, 2),substr($string, 10, 2),substr($string, 12, 2),
				substr($string, 4, 2),substr($string, 6, 2),substr($string, 0, 4));

		}
		elseif (is_numeric($string)) {
			$time = (int)$string; // it is a numeric string, we handle it as timestamp

		}
		else {
			// strtotime should handle it
			$time = strtotime($string);
			if ($time == -1 || $time === false) {
				// strtotime() was not able to parse $string, use "now":
				$time = time();
			}
		}
		return $time;

	}
}
