<?php
//%LICENCE_HEADER%

namespace Rbs\Sys\Message;

use Rbs\Sys\MessageDao;

/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `message_mailbox` (
`id` int(14) NOT NULL,
`owner` varchar(40) NOT NULL,
`from` varchar(200) NOT NULL,
`to` varchar(255),
`cc` varchar(255),
`bcc` varchar(255),
`subject` varchar(255)  DEFAULT NULL,
`body` text,
`hash` varchar(32)  DEFAULT NULL,
`replyto_hash` varchar(32)  DEFAULT NULL,
`date` int(14) DEFAULT NULL,
`isRead` char(1)  DEFAULT NULL,
`isReplied` char(1)  DEFAULT NULL,
`isFlagged` char(1)  DEFAULT NULL,
`priority` int(2) DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX ( `owner` )
) ENGINE=InnoDB;

CREATE TABLE `message_seq`(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;
INSERT INTO message_seq(id) VALUES(10);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class MailboxDao extends MessageDao
{

	/**
	 * @var string
	 */
	public static $table = 'message_mailbox';
	public static $vtable = 'message_mailbox';
	public static $archiveTable = 'message_archive';
	public static $sequenceName = 'message_seq';
	public static $sequenceKey = 'id';
}
