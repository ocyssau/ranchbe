<?php
// %LICENCE_HEADER%
namespace Rbs\Sys\Mail;

use Ranchbe;

/**
 * @brief Dao for message.
 * This dao may be use as replacement of \Zend_Mail_Transport object to use a postgresql db
 * as mail transfert system.
 *
 * Dao for postgresql. See schema Rbplm/Dao/Shemas/Pgsql.
 *
 * Example and tests: Rbplm/Sys/MessageTest.php
 */
class Transport
{
	/**
	 * @var \Zend\Mail\Transport\Smtp
	 */
	protected static $transport;

	/**
	 * Public constructor
	 */
	protected function __construct()
	{
		$config = Ranchbe::get()->getConfig('mail.smtp');
		$transport = new \Zend\Mail\Transport\Smtp();
		$transport->setOptions(new \Zend\Mail\Transport\SmtpOptions($config));
		self::$transport = $transport;
	}

	/**
	 * @return \Zend\Mail\Transport\Smtp
	 */
	public static function get()
	{
		if(!isset(self::$transport)){
			new self();
		}
		return self::$transport;
	}
}
