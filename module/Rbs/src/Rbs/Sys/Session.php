<?php
//%LICENCE_HEADER%

namespace Rbs\Sys;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\CurrentUser;


/** SQL_SCRIPT>>
CREATE TABLE `user_sessions` (
	`user_id` int(11) NOT NULL,
	`datas` LONGTEXT,
	PRIMARY KEY  (`user_id`)
) ENGINE=MyIsam;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE `user_sessions`;
 <<*/

/**
 * this class is used to store all properties defined by project and container like :
 * containers links
 * doctypes
 * current space
 * current container
 * current project
 */
class Session
{
	/**
	 * Name of the namespace
	 * @var string
	 */
	private $namespace = 'context';

	/**
	 * @var Session
	 */
	private static $instance;

	/**
	 * @var Connexion
	 */
	private static $connexion;

	/**
	 *
	 * @return void
	 */
	public function __construct($namespace='context')
	{
		$this->namespace = $namespace;
	} //End of method

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function __isset($name)
	{
		return isset($_SESSION[$this->namespace][$name]) ? true : false;
	}


	/**
	 * @param string $name
	 * @param string $value
	 */
	public function __set($name, $value)
	{
		$_SESSION[$this->namespace][$name]=$value;
		return $this;
	}

	/**
	 * @param string $name
	 */
	public function __get($name)
	{
		return isset($_SESSION[$this->namespace][$name]) ? $_SESSION[$this->namespace][$name] : null;
	}

	/**
	 * Clean all content of session
	 */
	public function unsetAll()
	{
		$_SESSION[$this->namespace] = null;
		return $this;
	}

	/**
	 */
	public static function setConnexion($conn)
	{
		self::$connexion = $conn;
	} //End of method

	/**
	 * Singleton
	 * @return Session
	 */
	public static function get()
	{
		if(!self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	} //End of method

	/**
	 *
	 * @return void
	 */
	public function init()
	{
		$_SESSION[$this->namespace] = null;
		return $this;
	} //End of method

	/**
	 * Record selected container id/number/type and eventualy his project id/number in session.<br>
	 *
	 *  Create var in session :<br>
	 *  SelectedContainer : id of the current container<br>
	 *  SelectedContainerNum : Number of the current container<br>
	 *  SelectedContainerType : Type of the current container(workitem, bookshop, cadlib, mockup)<br>
	 *  SelectedProject : id of the father project if current container is a workitem<br>
	 *  SelectedProjectNum : number of the father project if current container is a workitem<br>
	 *  area_id : id of the functionnal area. Use by LiveUser to set permissions<br>
	 *  objectList : List of the objects linked to current project(only if selected container is type workitem)<br>
	 *  projectInfos : Infos about current project(only if selected container is type workitem)<br>
	 */
	public function activate($containerId, $spaceName)
	{
		$factory = DaoFactory::get($spaceName);
		$container = $factory->getModel(Workitem::$classId);
		$contDao = $factory->getDao(Workitem::$classId);
		$contDao->loadFromId($container, $containerId);
		$projDao = $factory->getDao(Project::$classId);

		if ( $spaceName != $_SESSION[$this->namespace]['spaceName']
			|| $containerId != $_SESSION[$this->namespace]['containerId'] ){

			unset($_SESSION[$this->namespace]);
			$_REQUEST['resetf'] = true; //For reinit filter record in session

			$projectId = $container->getParent(true);
			if($projectId){
				$project = new Project();
				$projDao->loadFromId($project, $projectId);
				$container->setParent($project);
				$_SESSION[$this->namespace]['projectId']     = $project->getId();
				$_SESSION[$this->namespace]['projectNumber'] = $project->getUid();
				$_SESSION[$this->namespace]['areaId']        = $project->areaId;
				$_SESSION[$this->namespace]['projectInfos']  = $project->getArrayCopy();
			}

			$_SESSION[$this->namespace]['containerId'] = $containerId;
			$_SESSION[$this->namespace]['containerNumber'] = $container->getUid();
			$_SESSION[$this->namespace]['spaceName'] = $spaceName;

			$this->save(CurrentUser::get()->getId());
		}

		return $container;
	}//End of method

	/**
	 * Load session from db
	 * @param $userId
	 */
	public function load($userId)
	{
		$connexion = self::$connexion;

		$sql = 'SELECT datas FROM user_sessions WHERE user_id =' . $userId;
		$datasStr = $connexion->query($sql);
		if(!$datasStr){
			return false;
		}

		$datas = unserialize( $datasStr->fetchColumn() );
		if($datas){
			foreach($datas as $pname=>$pvalue){
				$_SESSION[$this->namespace][$pname] = $pvalue;
			}
		}
	}

	/**
	 * Save session in db
	 * @param $userId
	 */
	public function save($userId)
	{
		$sdatas = array();
		$sdatas['links'] = $_SESSION[$this->namespace]['links'];
		$sdatas['projectId'] = $_SESSION[$this->namespace]['projectId'];
		$sdatas['projectNumber'] = $_SESSION[$this->namespace]['projectNumber'];
		$sdatas['areaId'] = $_SESSION[$this->namespace]['areaId'];
		$sdatas['objectList'] = $_SESSION[$this->namespace]['objectList'];
		$sdatas['projectInfos'] = $_SESSION[$this->namespace]['projectInfos'];
		$sdatas['containerId'] = $_SESSION[$this->namespace]['containerId'];
		$sdatas['containerNumber'] = $_SESSION[$this->namespace]['containerNumber'];
		$sdatas['spaceName'] = $_SESSION[$this->namespace]['spaceName'];
		$sdatas['documentManagerUrl'] = $_SESSION[$this->namespace]['documentManagerUrl'];

		$connexion = self::$connexion;
		$connexion->beginTransaction();

		try{
			$sql = "DELETE FROM user_sessions WHERE user_id=" . $userId;
			$connexion->exec($sql);

			$data = serialize($sdatas);
			$sql = "INSERT INTO user_sessions (`user_id`, `datas`) VALUES ('$userId', '$data')";
			$connexion->exec($sql);
			$connexion->commit();
		}
		catch(\Exception $e){
			$connexion->rollBack();
			throw $e;
		}
	}

	/**
	 * Clean all content of session
	 * @param $user LiveUser object
	 * @return boolean
	 */
	public function clean($userId)
	{
		$connexion = self::$connexion;
		$connexion->beginTransaction();

		try{
			$query = "DELETE FROM user_sessions WHERE user_id=" . $userId;
			$connexion->exec($query);
			$connexion->commit();
		}
		catch(\Exception $e){
			$connexion->rollBack();
			throw $e;
		}
		$_SESSION[$this->namespace] = null;
	}
}
