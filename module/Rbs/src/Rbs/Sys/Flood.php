<?php
namespace Rbs\Sys;

class Flood
{
	/**
	 *
	 * @var string
	 */
	protected static $_ticket = '';

	/**
	 *
	 * @var array
	 */
	protected static $_ticketStack = array();

	/**
	 * True to keep validity after supression of session ticket
	 * @var boolean
	 */
	protected static $_isValid = false;


	//------------------------------------------------------------------------
	/**
	 * Anti-Flood V3. Condition existance du ticket en session. Tous les tickets
	 * sont conservés en session jusqu'a leur utilisation.
	 * Inspired by principle described in
	 *   http://www.developpez.net/forums/archive/index.php/t-104783.html
	 * If session jeton is egal to jeton record in form, generate a new jeton.
	 * session_register('jeton');
	 *
	 * @param string
	 * @return boolean
	 *
	 */
	public static function checkFlood($ticket)
	{
		if(self::$_isValid){
			return true;
		}

		if(Ranchbe::get()->getConfig('flood.check') == false){
			return true;
		}

		$ticketStack = & self::$_ticketStack;

		//Return array key of the ticket or false if not a valid ticket
		$ticketKey = array_search( $ticket, $ticketStack );

		if($ticketKey) {
			unset( $ticketStack [$ticketKey] ); // Suppress ticket in session
			self::setTicket(); // generate a new ticket
			self::$_isValid = true; //keep validity
			return true;
		}
		else {
			self::setTicket(); // generate a new ticket
			return false;
		}

	} //End of check Flood

	/**
	 * Check time interval between 2 requests
	 *
	 * @return boolean
	 */
	public static function checkTime()
	{
		/* init time tracker */
		if(! $_SESSION['timeTracker']) {
			$_SESSION['timeTracker'] = time();
			return true;
		}

		/* init Flood tracker */
		if(! $_SESSION['floodTracker']) {
			$_SESSION['floodTracker'] = 1;
			return true;
		}

		$ranchbe = Ranchbe::get();
		$timeInterval = $ranchbe->getConfig('flood.timeinterval');
		$maxRequest = $ranchbe->getConfig('flood.maxrequest');
		$waitTime = $ranchbe->getConfig('flood.wait');


		/* If the page is recall after time < to one second */
		if((time() - $_SESSION['timeTracker']) < $timeInterval ){
			$_SESSION['floodTracker'] ++;
			if( $_SESSION['floodTracker'] > $maxRequest ){
				$msg = "You can not doing $maxRequest requests in $timeInterval seconds.Now, wait $waitTime seconds and refresh this page...";
				$_SESSION['timeTracker'] = time() + $waitTime;
				die($msg);
			}
		}
		else {
			$_SESSION['floodTracker'] = 1;
			$_SESSION['timeTracker'] = time();
			return true;
		}

	} //End of methode


	/** Generate the ticket. If $string is set, generate the ticket from this string
	 *  md5 encode, else, generate a unpredictible random string
	 *
	 * @param String String to encode with md5
	 * @return String Ticket
	 *
	 */
	public static function setTicket($string = '')
	{
		//Get the ticket stack from session
		self::_initStack();
		$ticketStack = & self::$_ticketStack;

		//generate a random string if none string
		if(!$string) {
			self::$_ticket = md5( uniqid( mt_rand(), true ) );
		}
		else {
			self::$_ticket = md5($string);
			//check if ticket is yet in stack
			if(array_search( self::$_ticket, $_SESSION['ticket_gf'] ) !== false) {
				Ranchbe::getView()->assign('ticket', self::$_ticket);
				return self::$_ticket;
			}
		}

		//add ticket to stack begining
		array_unshift($ticketStack, self::$_ticket);

		//assign ticket to view
		Ranchbe::getView()->assign('ticket', self::$_ticket);

		//clean older ticket
		$maxsize = $ranchbe->getConfig('flood.maxsize');
		if(count( $ticketStack ) > $maxsize) {
			array_pop( $ticketStack );
		}

		return self::$_ticket;
	} //End of methode


	/** Return ticket for current request
	 *
	 * @return string
	 */
	public static function getTicket()
	{
		return self::$_ticket;
	} //End of methode

	/** Init the ticketStack
	 *
	 * @return unknown_type
	 */
	protected static function _initStack()
	{
		if(! self::$_ticketStack) {
			if(! $_SESSION['ticket_gf'])
				$_SESSION['ticket_gf'] = array();
			self::$_ticketStack =& $_SESSION['ticket_gf'];
		}
	} //End of methode


} //End of class
