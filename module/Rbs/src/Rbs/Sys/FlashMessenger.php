<?php
//%LICENCE_HEADER%

namespace Rbs\Sys;

/**
 */
class FlashMessenger
{
	/**
	 * Name of the namespace
	 * @var string
	 */
	private $namespace = 'messengers';

	/**
	 * @var Session
	 */
	private static $instance;

	/**
	 *
	 * @return void
	 */
	public function __construct($namespace='messengers')
	{
		$this->namespace = $namespace;
		self::$instance = $this;
	} //End of method

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function __isset($name)
	{
		return isset($_SESSION[$this->namespace][$name]) ? true : false;
	}


	/**
	 * @param string $name
	 * @param string $value
	 */
	public function __set($name, $value)
	{
		$_SESSION[$this->namespace][$name]=$value;
		return $this;
	}

	/**
	 * @param string $name
	 */
	public function __get($name)
	{
		return isset($_SESSION[$this->namespace][$name]) ? $_SESSION[$this->namespace][$name] : null;
	}

	/**
	 * Singleton
	 * @return Session
	 */
	public static function get()
	{
		if(!self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	} //End of method

	/**
	 *
	 * @return void
	 */
	public function clear()
	{
		$_SESSION[$this->namespace] = null;
		return $this;
	} //End of method

	/**
	 *
	 */
	public function hasError()
	{
		return (isset($_SESSION[$this->namespace]['error'][0]));
	} //End of method

	/**
	 *
	 */
	public function hasWarning()
	{
		return (isset($_SESSION[$this->namespace]['warning'][0]));
	} //End of method

	/**
	 *
	 */
	public function hasSuccess()
	{
		return (isset($_SESSION[$this->namespace]['success'][0]));
	} //End of method

	/**
	 *
	 */
	public function hasMessage()
	{
		return (isset($_SESSION[$this->namespace]['message'][0]));
	} //End of method

	/**
	 *
	 */
	public function addMessage($msg)
	{
		$_SESSION[$this->namespace]['message'][]=$msg;
	} //End of method

	/**
	 *
	 */
	public function addSuccess($msg)
	{
		$_SESSION[$this->namespace]['success'][]=$msg;
	} //End of method

	/**
	 *
	 */
	public function addWarning($msg)
	{
		$_SESSION[$this->namespace]['warning'][]=$msg;
	} //End of method

	/**
	 *
	 */
	public function addError($msg)
	{
		$_SESSION[$this->namespace]['error'][] = $msg;
	} //End of method

	/**
	 *
	 */
	public function getMessages()
	{
		return $_SESSION[$this->namespace]['message'];
	} //End of method

	/**
	 *
	 */
	public function getSuccess()
	{
		return $_SESSION[$this->namespace]['success'];
	} //End of method

	/**
	 *
	 */
	public function getErrors()
	{
		return $_SESSION[$this->namespace]['error'];
	} //End of method

	/**
	 *
	 */
	public function getWarnings()
	{
		return $_SESSION[$this->namespace]['warning'];
	} //End of method

}
