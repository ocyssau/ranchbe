<?php
//%LICENCE_HEADER%

namespace Rbs\Model;


/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{

	/**
	 * Registry of instanciated DAO.
	 *
	 * @var array
	 */
	private static $_registry = array();

	/**
	 * Assiciate for each Component Class ID a DAO CLASS
	 * ClassId 200 return the super class DAO Component/Component
	 * @var array
	 */
	private static $_map = array(
		420=>array('\Workflow\Model\Wf\Process', 'wf_process'),
		421=>array('\Workflow\Model\Wf\Activity\Activity', 'wf_activity'),
		422=>array('\Workflow\Model\Wf\Activity\Start', 'wf_activity'),
		423=>array('\Workflow\Model\Wf\Activity\End', 'wf_activity'),
		424=>array('\Workflow\Model\Wf\Activity\Join', 'wf_activity'),
		425=>array('\Workflow\Model\Wf\Activity\Split', 'wf_activity'),
		426=>array('\Workflow\Model\Wf\Activity\Aswitch', 'wf_activity'),
		427=>array('\Workflow\Model\Wf\Activity\Standalone', 'wf_activity'),
		428=>array('\Workflow\Model\Wf\Activity', 'wf_activity'),
		430=>array('\Workflow\Model\Wf\Transition', 'wf_transition'),
		440=>array('\Workflow\Model\Wf\Instance', 'wf_instance'),
		441=>array('\Workflow\Model\Wf\Instance\Activity', 'wf_activity_instance'),
		442=>array('\Workflow\Model\Link', ''),
		450=>array('\Workflow\Model\Wf\Instance\Standalone', 'wf_activity_instance'),
	);

	/**
	 * Return a instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return Dao
	 */
	public static function get($classId)
	{
		$class = self::$_map[$classId][0];
		return new $class();
	}

	/**
	 * Return a initialized instance of class specified by id.
	 *
	 * @param integer $classId
	 * @return Dao
	 */
	public static function getNew($classId)
	{
		$class = self::$_map[$classId][0];
		return $class::init();
	}

	/**
	 *
	 * @param integer $cid
	 */
	public static function getMap($cid=null)
	{
		if($cid){
			return self::$_map[$cid];
		}
		else{
			return self::$_map;
		}
	}
}
