<?php
namespace Rbs\Notification;

class Observer
{

	/**
	 *
	 * @param unknown $event
	 * @param unknown $document
	 */
	public function notify($event, &$document)
	{
		if ( !is_a($document, 'Document') ) return false;

		$list = Rb_Container_Notification::get($document->getSpaceName())->getAll($document->getContainer()
			->getId(), null, $event);
		if ( !$list ) return true;
		$toUsers = array();
		foreach( $list as $notification ) {
			if ( $notification['user_id'] == Rb_User::getCurrentUser()->getId() ) continue;
			if ( !$notification['Rb_Condition'] ) {
				$toUsers[] = $notification['user_id'];
				continue;
			}
			$condition = unserialize($notification['Rb_Condition']);
			if ( $condition->isValid() ) {
				$toUsers[] = $notification['user_id'];
				continue;
			}
		}
		$this->_sendNotification($toUsers, $document);
		return true;
	}

	/**
	 *
	 * @param array $toUsers
	 * @param Rb_Document $document
	 */
	protected function _sendNotification(array $toUsers, &$document)
	{
		// Send message to next users
		$message = new Message(Ranchbe::getDb());
		$from = Rb_User::getCurrentUser()->getName();
		$cc = '';
		$priority = 3;

		switch ($this->_eventName) {
			case ('doc_post_store'):
				$subject = sprintf(tra('New document %s in container %s by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_update'):
				$subject = sprintf(tra('Document %s in container %s has been updated by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_reset'):
				$subject = sprintf(tra('Document %s in container %s has been reseted by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_checkout'):
				$subject = sprintf(tra('Document %s in container %s is check-out by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_checkin'):
				$subject = sprintf(tra('Document %s in container %s is check-in by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_move'):
				$subject = sprintf(tra('Document %s has been moved in container %s by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ('doc_post_suppress'):
				$subject = sprintf(tra('Document %s in container %s has been suppressed by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
		}

		$body = '<a href="javascript:popupP(\'document/detail/get/document_id/' . $document->getId() . '/space/' . $document->getSpaceName() . '\',\'documentDetailWindow\', 600 , 1024)">' . 'link to ' . $document->getNumber() . '</a><br>';

		foreach( $toUsers as $to ) {
			$message->postMessage($to, $from, $to, $cc, $subject, $body, $priority);
		}
		return true;
	}
}
