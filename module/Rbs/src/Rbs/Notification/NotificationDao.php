<?php
namespace Rbs\Notification;

/** SQL_SCRIPT>>
 CREATE TABLE `notifications` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `ownerUid` VARCHAR (128) NOT NULL,
 `ownerId` VARCHAR (128) NOT NULL,
 `referenceUid` VARCHAR (128) NOT NULL,
 `spacename` VARCHAR (64) NOT NULL,
 `events` VARCHAR (256) NOT NULL,
 `condition` BLOB,
 PRIMARY KEY (`id`),
 KEY `INDEX_workitem_cont_notifications_1` (`ownerId`),
 KEY `INDEX_workitem_cont_notifications_2` (`referenceUid`),
 KEY `INDEX_workitem_cont_notifications_3` (`events`),
 UNIQUE KEY `UNIQ_workitem_cont_notifications_1` (`ownerId`,`referenceUid`,`events`)
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 DROP TABLE `notifications`;
 <<*/

class NotificationDao extends \Rbs\Dao\Sier
{

	/**
	 * @var string
	 */
	public static $table='notifications';

	/**
	 * @var string
	 */
	public static $vtable='notifications';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'ownerUid'=>'ownerUid',
		'ownerId'=>'ownerId',
		'referenceUid'=>'referenceUid',
		'spacename'=>'spaceName',
		'events'=>'events',
		'condition'=>'condition'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'condition'=>'json',
		'events'=>'json'
	);

	/**
	 * @see DaoInterface::loadFromId()
	 */
	public function loadFromReferenceUidAndOwnerUid($mapped, $uid, $ownerUid)
	{
		$filter = 'obj.referenceUid=:uid AND obj.ownerUid=:ownerUid';
		$bind = array(':uid'=>$uid, ':ownerUid'=>$ownerUid);
		return $this->load($mapped, $filter, $bind );
	}
}
