<?php
namespace Rbs\Notification;

use Rbplm\People;
use Rbplm\Dao\Filter\Op;

class Notification extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped, \Rbplm\Owned;

	/**
	 *
	 * @var string
	 */
	static $classId = '479v169a42a96';

	/**
	 *
	 * @var \Rbs\Notification\Condition
	 */
	protected $condition = false;

	/**
	 *
	 * @var array
	 */
	protected $events;

	/**
	 *
	 * @var Any
	 */
	protected $reference;

	/**
	 *
	 * @var string
	 */
	public $referenceUid;

	/**
	 *
	 * @var integer
	 */
	public $referenceId;

	/**
	 *
	 * @var string
	 */
	public $spaceName;

	/**
	 *
	 * @param array $properties
	 */
	public function __construct($properties = null)
	{
		if ( $properties ) {
			$this->hydrate($properties);
		}
		$this->cid = static::$classId;
	}

	/**
	 *
	 * @param string|array $event
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return Notification
	 */
	public function onEvent($event = '', $document)
	{
		if ( !$actionName ) {
			if ( is_string($event) ) {
				$actionName = explode('.', $event)[0];
			}
			elseif ( $event instanceof \Rbplm\Event ) {
				$actionName = explode('.', $event->name)[0];
			}
		}

		if ( !$this->factory ) {
			throw new \Exception('$this->factory is not set');
		}

		/* @var \Rbs\Dao\DaoList $list */
		$list = $this->factory->getList(\Rbs\Notification\Notification::$classId);
		$dao = $this->factory->getDao(\Rbs\Notification\Notification::$classId);

		/* @var \Rbs\Dao\Filter $filter */
		$filter = $this->factory->getFilter(\Rbs\Notification\Notification::$classId);
		$filter->andfind($document->getUid(), $dao->toSys('referenceUid'), Op::EQUAL);
		$filter->andfind($event->name, $dao->toSys('events'), Op::CONTAINS);
		$filter->select(array(
			$dao->toSys('ownerUid') . ' AS ' . 'ownerUid',
		));
		$list->load($filter);

		/* Send message to next users */
		$message = new \Rbplm\Sys\Message(\Rbplm\Sys\Message::TYPE_SEND);
		$from = People\CurrentUser::get();
		$cc = '';
		$priority = 3;

		$message->setFrom($from->getUid().'@ranchbe.local', $from->getName());
		$message->ownerUid = $from->getUid();

		switch ($event) {
			case ($document::SIGNAL_POST_CREATE):
				$subject = sprintf(tra('New document %s in container %s by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ($document::SIGNAL_POST_CHECKIN):
				$subject = sprintf(tra('Document %s in container %s has been updated by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ($document::SIGNAL_POST_CHECKOUT):
				$subject = sprintf(tra('Document %s in container %s is check-out by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ($document::SIGNAL_POST_MOVE):
				$subject = sprintf(tra('Document %s has been moved in container %s by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
			case ($document::SIGNAL_POST_DELETE):
				$subject = sprintf(tra('Document %s in container %s has been suppressed by %s'), $document->getNumber(), $document->getContainer()->getNumber(), $from);
				break;
		}
		$baseurl = BASEURL;
		$body = '<a class="rb-popup btn btn-default" href="' . $baseurl . 'rbdocument/detail/index?documentid=' . $document->getId() . '&spacename=' . $this->factory->getName() . '">' . 'Get Details of ' . $document->getNumber() . '</a>';

		$message->setBody($body);
		$message->setSubject($subject);

		foreach( $list as $notification ) {
			$to = $notification[$dao->toSys('ownerUid')];
			$toRbmessage = $to.'@ranchbe.local';
			$message->addTo($toRbmessage, $to);
		}

		$dao = $this->factory->getDao($message::$classId);
		$dao->save($message);

		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @return AnyObject
	 */
	public static function init($name = null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		if ( !$name ) {
			$name = $obj->getUid();
		}
		$obj->setName($name);
		$obj->ownerId = People\CurrentUser::get()->getUid();
		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return \Rbs\Notification\Notification
	 */
	public function hydrate(array $properties)
	{
		// ANY
		(isset($properties['name'])) ? $this->name = $properties['name'] : null;
		(isset($properties['uid'])) ? $this->uid = $properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid = $properties['cid'] : null;
		// MAPPED
		$this->mappedHydrate($properties);
		// OWNED
		$this->ownedHydrate($properties);
		// NOTIFICATION
		(isset($properties['events'])) ? $this->events = $properties['events'] : null;
		(isset($properties['referenceId'])) ? $this->referenceId = $properties['referenceId'] : null;
		(isset($properties['referenceUid'])) ? $this->referenceUid = $properties['referenceUid'] : null;
		(isset($properties['spaceName'])) ? $this->spaceName = $properties['spaceName'] : null;

		if ( isset($properties['condition']) ) {
			$this->condition = new Condition($properties['condition']);
		}
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Notification\Condition
	 */
	public function getCondition()
	{
		return $this->condition;
	}

	/**
	 *
	 * @param \Rbs\Notification\Condition $condition
	 * @return \Rbs\Notification\Notification
	 */
	public function setCondition(Condition $condition)
	{
		$this->condition = $condition;
		return $this;
	}

	/**
	 *
	 * @return \Rbs\Notification\Notification
	 */
	public function unsetCondition()
	{
		$this->condition = false;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getEvents()
	{
		return $this->events;
	}

	/**
	 *
	 * @return array
	 */
	public function setEvents($array)
	{
		return $this->events = $array;
	}

	/**
	 *
	 * @param string $event
	 * @return \Rbs\Notification\Notification
	 */
	public function addEvent($event)
	{
		$this->events[] = $event;
		return $this;
	}

	/**
	 *
	 * @param [OPTIONAL]boolean $asId
	 * @return \Rbplm\Any
	 */
	public function getReference($asId = false)
	{
		if ( $asId ) {
			return $this->referenceUid;
		}
		return $this->reference;
	}

	/**
	 *
	 * @param \Rbplm\Any $reference
	 * @return \Rbs\Notification\Notification
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;
		$this->referenceId = $reference->getId();
		$this->referenceUid = $reference->getUid();
		return $this;
	}
}

