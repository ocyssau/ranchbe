<?php
namespace Rbs\Notification;

class Condition
{

	/**
	 * array of object Condition, subcondtion
	 *
	 * @var array
	 */
	protected $conditions = array();

	/**
	 * array, tests to verify by Condition
	 *
	 * @var array
	 */
	protected $assertions = array();

	/**
	 * or|and
	 * @var string
	 */
	protected $operator = 'or';

	/**
	 * @param array $properties
	 */
	public function __construct($properties=null)
	{
		if($properties){
			foreach($properties as $name=>$value){
				$this->$name = $value;
			}
		}
	}

	/**
	 * @param string $operator
	 * @return Condition
	 */
	public function setOperator($operator)
	{
		$this->operator = $operator;
		return $this;
	}

	/**
	 * @param Condition $condition
	 * @return Condition
	 */
	public function addCondition(Condition $condition)
	{
		$this->conditions[] = $condition;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getConditions()
	{
		return $this->conditions;
	}

	/**
	 * @param string $name
	 * @param string $operator
	 * @param string $value
	 * @return Condition
	 */
	public function addAssertion($name, $operator, $value)
	{
		$this->assertions[] = array(
			$operator,
			$name,
			$value
		);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAssertions()
	{
		return $this->assertions;
	}

	/**
	 * @param Any $object
	 * @return boolean
	 */
	public function isValid($object)
	{
		foreach($this->conditions as $condition) {
			$result[] = $condition->isValid($object);
		}
		foreach( $this->assertions as $assertion ) {
			switch ($assertion[0]) {
				case '==':
					$result[] = $assertion[1] == $assertion[2];
					break;
				case '>':
					$result[] = $assertion[1] > $assertion[2];
					break;
				case '<':
					$result[] = $assertion[1] < $assertion[2];
					break;
				case '>=':
					$result[] = $assertion[1] >= $assertion[2];
					break;
				case '<=':
					$result[] = $assertion[1] <= $assertion[2];
					break;
			}
		}

		if ( $this->operator == 'and' ) {
			$prod = (int)array_product($result);
			return $prod ? true : false;
		}
		else {
			$sum = (int)array_sum($result);
			return $sum == 0 ? false : true;
		}
	}
}
