<?php
//%LICENCE_HEADER%

namespace Rbs\Notification;

use Rbplm\Dao\Connexion;

/*
 CREATE TABLE `notification_index` (
 `ownerId` int(11) NOT NULL,
 KEY  (`ownerId`)
 ) ENGINE=InnoDB ;
*/

/**
 * @brief Notification is used to create Notification system wich not depends of read/noread
 * states. Create a index when a message is send and notify user when this index is not empty
 *
 */
class IndexDao
{
	protected static $instance = false;
	protected static $table = 'notification_index';
	protected static $vtable = 'notification_index';

	/**
	 *
	 * @var \PDO
	 */
	protected static $connexion;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'ownerId'=>'ownerId',
	);

	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}

		$this->_table = static::$table;
		$this->_vtable = static::$vtable;
	}

	/**
	 *
	 * @param $ownerId
	 * @return IndexDao
	 */
	public static function notify($ownerId)
	{
		if(!isset($this->insertStmt)){
			$table = $this->_table;
			$sql = "INSERT INTO $table (ownerId) VALUES (:ownerId);";
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		($this->connexion->inTransaction()==true) ? $withTrans = false : $withTrans = true;
		if($withTrans) $this->connexion->beginTransaction();
		$bind = array('ownerId'=>$ownerId);
		$this->insertStmt->execute($bind);
		if($withTrans) $this->connexion->commit();
		return $this;
	}

	/**
	 * @param $ownerId
	 * @return integer
	 */
	public static function hasMessage($ownerId)
	{
		$sql = "SELECT ownerId FROM $table WHERE ownerId = $ownerId";
		$stmt = $this->connexion->query($sql);
		return $stmt->rowCount();
	}

	/**
	 *
	 * @param $ownerId
	 * @return unknown_type
	 */
	public static function clear($ownerId)
	{
		($this->connexion->inTransaction()==true) ? $withTrans = false : $withTrans = true;
		if($withTrans) $this->connexion->beginTransaction();
		$sql = "DELETE FROM $table WHERE ownerId = $ownerId";
		$stmt = $this->connexion->exec($sql);
		if($withTrans) $this->connexion->commit();
		return $this;
	}

}
