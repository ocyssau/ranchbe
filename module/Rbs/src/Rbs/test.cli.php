#!/usr/bin/php
<?php

namespace testcli;

use Rbplm\Dao\Connexion As Connexion;
use Rbplm\Dao\Factory as DaoFactory;
use Rbs\Dao\Sier\ClassDao;
use Rbs\Dao\Sier\Loader as DaoLoader;
use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Sys\Trash;

/**
 *
 */
function optionh()
{
    $display = 'Utilisation :

    php tests.php
    [-c <class>]
    [-m <method>]
    [-a]
    [--nodao,]
    [--croot,]
    [-i]
    [-lf]
    [-lc]
    [--doctype]
    [-help, -h, -?]

    Avec:
    -c <class>
    : Le nom de la classe de test a executer. Il peut y avoir plusieur option -c

    -m <method>
    : La methode a executer, ou si omis, execute tous les tests de la classe.

    -a
    : Execute tous les tests, annule l\'option -c

    -t
    : Execute mes tests, annule l\'option -c

    -p
    : Active le profiling des tests

    --nodao
    : Ignore les methodes testDao*

    --croot
    : Creer le noeud root

    -i
    : initialise les données de test.

    --initdb
    : initialise les données de test.

    --lf
    : Affiche la liste des functions de test disponibles

    --lc
    : Affiche la liste des classes de test disponibles

    -s
    : Extrait les scripts sql depuis les classes

    -h
    : Affiche cette aide.

    Examples:
    Execute tous les tests de la classe \Rbplm\AnyObjectTest et Rbplm\Org\Test:
    php tests.php -c \Rbplm\AnyObjectTest -c Rbplm\Org\Test

    Execute la methode \Rbplm\AnyObjectTest::testDao
    php tests.php -c \Rbplm\AnyObjectTest -m testDao

    Avec les options -help, -h vous obtiendrez cette aide

    -t : Execute la function test <fonction> parmis les fonctions suivantes :';
    echo $display;
    displaylisttest();
}

function displaylisttest()
{
    echo $display='
    ';
}


function run()
{
    //go to app root directory
    chdir(__DIR__.'/../../../../.');

    define('APP_DIR', getcwd());
    echo getcwd() ."\n";

    /*
    set_include_path(get_include_path() . PATH_SEPARATOR . realpath(__DIR__ . '/..'));
    spl_autoload_register(function ($class) {
        include (str_replace('\\', '/', $class . '.php'));
    });
    */

    error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
    ini_set ( 'display_errors', 1 );
    ini_set ( 'display_startup_errors', 1 );

    ini_set('xdebug.collect_assignments', 1);
    ini_set('xdebug.collect_params', 4);
    ini_set('xdebug.collect_return', 1);
    ini_set('xdebug.collect_vars', 1);
    ini_set('xdebug.var_display_max_children', 1000);
    ini_set('xdebug.var_display_max_data', 5000);
    ini_set('xdebug.var_display_max_depth', 10);
    //ini_set('xdebug.auto_trace', false); //Not definable in scripts, only php.ini
    //ini_set('xdebug.trace_output_dir', 'C:/htdocs/rbPlm/unitsTests/Api');
    //putenv('APPLICATION_ENV=testing');

    $loader = include 'vendor/autoload.php';
    $loader->set('RbsTest', realpath(APP_DIR.'/module/Rbs/src'));

    /*
     ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
    ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
    ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
    ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
    ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
    */
    assert_options( ASSERT_ACTIVE, 1);
    assert_options( ASSERT_WARNING, 1 );
    assert_options( ASSERT_BAIL, 0 );
    //assert_options( ASSERT_CALLBACK , 'myAssertCallback');

    /*
     ob_start();
    phpinfo();
    file_put_contents('.phpinfos.out.txt', ob_get_contents());
    */

    echo 'include path: ' . get_include_path ()  . PHP_EOL;

    /*
     $zver=new \Zend\Version\Version();
    echo 'ZEND VERSION : ' . $zver::VERSION . "\n";
    */

    //For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
    /*
    $i=1;
    while($ENV['arg'.$i]){
    $argv[$i] = $ENV['arg'.$i];
    $i++;
    }
    */

    $shortopts = '';
    $shortopts .= "c:"; // La classe <class> de test a lancer
    $shortopts .= "m:"; // La methode <method> a executer
    $shortopts .= "a";  // Execute tous les tests
    $shortopts .= "h";  // affiche l'aide
    $shortopts .= "t:"; // call test function <function>
    $shortopts .= "i"; //initialise les données de test
    $shortopts .= "s"; //Extrait les scripts sql depuis les classes
    $shortopts .= "p"; //Active le profiling des tests

    $longopts  = array(
        'initdb',
        'nodao',
        'croot',
        'lc', //Affiche la liste des classes de test disponibles
        'lf:',//Affiche la liste des methode de la <classe> de test
    );

    $norun = array('nodao','m', 'p');

    $options = getopt($shortopts, $longopts);

    foreach(array_keys($options) as $o){
        if(!in_array($o, $norun)){
            call_user_func(__NAMESPACE__.'\option'.$o, $options);
        }
    }
    return;
}

/**
 * execute la fonction test spécifiée
 */
function optiont($options)
{
    $suffix = $options['t'];
    if($suffix==''){
        $suffix='my';
    }
    call_user_func(__NAMESPACE__.'\test_'.$suffix, array());
}

/**
 * execute tous les test
 */
function optiona($options)
{
    boot();

    $test=new \Rbplm\Test\Test();

    if ( isset($options['nodao']) ) {
        $test->withDao(false);
    }
    else{
        $test->withDao(true);
    }

    if ( isset($options['p']) ) {
        $test->withProfiling(true);
    }
    else{
        $test->withProfiling(false);
    }

    $libPath=__DIR__;
    return $test->runAllClasses($libPath);
}

/**
 * liste les fonctions de la classe spécifiée
 */
function optionlf($options)
{
    echo 'List of test function:' .CRLF;
    $class=$options['lf'];

    $methods = get_class_methods($class);
    foreach($methods as $method){
        $t = explode('_', $method);
        if($t[0] == 'Test'){
            echo $method . CRLF;
        }
    }
    return;
}

/**
 * liste les classes de tests
 */
function optionlc($options)
{
    echo 'List of test class:' . CRLF;
    $test = new \Rbplm\Test\Test();
    $libPath = __DIR__.'/vendor/Rbplm';
    var_dump($libPath);
    foreach($test->getTestsFiles($libPath) as $path){
        $class = substr( $path, strlen($libPath) );
        $class = trim($class, '/');
        $class = trim($class, '.php');
        $class = str_replace('/', '\\', $class);
        if($class == 'Test\Test'){
            continue;
        }
        $class = '\Rbplm\\'.$class;
        echo $class . CRLF;
    }
    return;
}

/**
 * create root node
 */
function optioncroot($options)
{
    //$Schema = new \Rbplm\Dao\Schema( Connexion::get() );
    //$Schema->createUsers();
    //$Schema->createRootNode();
    echo "please, import data directly from SQL scripts" . PHP_EOL;
}

/**
 * Initialise la base de données
 */
function optioninitdb($options)
{
    boot();

    $RbplmLibPath = realpath(__DIR__);
    $Schema = new \Rbplm\Dao\Schema( Connexion::get() );
    $Schema->sqlInScripts=array(
        'sequence.sql',
        'create.sql',
        'insert.sql',
    );
    $sqlCompiledFile=$Schema->compileSchema($RbplmLibPath);
    $Schema->createBdd(file_get_contents($sqlCompiledFile));
}

/**
 * Execute les test de la classe ou des classes spécifiées
 */
function optionc($options)
{
    boot();

    $method = $options['m'];

    if ( isset($options['nodao']) ) {
        $withDao = false;
    }
    else{
        $withDao = true;
    }

    if ( isset($options['p']) ) {
        $withProfiling=true;
    }
    else{
        $withProfiling=false;
    }

    if(is_array($options['c'])){
        foreach($options['c'] as $class){
            \Rbplm\Test\Test::runOne($class, $method, $withDao, $continue=false, $withProfiling);
        }
    }
    else{
        \Rbplm\Test\Test::runOne($options['c'], $method, $withDao, $continue=false, $withProfiling);
    }
}

/**
 * Extrait les scripts SQL depuis les classes DAO
 */
function options($options)
{
    $RbplmPath = realpath(__DIR__.'/vendor/Rbplm');
    $Extractor = new SqlExtractor($RbplmPath.'/Dao/Schemas/pgsql/extracted');
    $Extractor->parse($RbplmPath);
    echo 'See result in ' . $Extractor->resultPath . CRLF;
}

/**
 *
 */
function optioni()
{
	//initialize
	boot();

	$test = new \Rbplm\Test\Test();
	$test->withProfiling(false);
	$test->loop = 1;
	$test->withDao(true);
	$test->add('\Rbs\InitDataTest');
	$test->run();
	die;
}

/**
 * Initialize
 */
function boot()
{
	include('conf/boot.php');
	require_once('class/Ranchbe.php');
	require_once('conf/local.conf');
	require_once('conf/default.conf');

	\rbinit_includepath();
	\rbinit_autoloader();
	\rbinit_rbservice();

    //initialize user
    $user = new User();
    DaoFactory::get()->getDao($user)->loadFromName($user, 'admin');
    CurrentUser::set($user);
}

run();

