<?php
//%LICENCE_HEADER%

namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Sys\Exception;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (600, '\Rbplm\Vault\Record', 'vault_record');
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Sier Dao class for \Rbplm\Vault\Record.
 *
 */
class RecordDao extends DaoSier
{

	protected $_table = null;
	protected $_sequence_name = null;

	protected static $_sysToApp = array(
	);

	/**
	 */
	public function save(MappedInterface $mapped, $options = array())
	{
		throw new Exception('SaveIsNotPermit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::load()
	 */
	public function load( MappedInterface $mapped, $filter=null, $bind=null )
	{
		throw new Exception('LoadIsNotPermit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbs\Dao.Sier::loadFromArray()
	 */
	public function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		throw new Exception('LoadIsNotPermit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::suppress()
	 */
	public function suppress(MappedInterface $mapped, $withChilds = false )
	{
		throw new Exception('SuppressIsNotPermit');
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.Mysql::updateFromArray()
	 */
	public function updateFromArray($id, $data)
	{
		throw new Exception('SaveIsNotPermit');
	}

} //End of class

