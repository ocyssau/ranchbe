<?php
//%LICENCE_HEADER%

namespace Rbs\Vault;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Dao\Exception;
use Rbplm\Sys\Error;


/** SQL_SCRIPT>>
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for \Rbplm\Vault\Reposit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 * @see \Rbplm\Vault\RepositTest
 *
 */
class RepositDao extends DaoSier
{
	/**
	 *
	 * @var string
	 */
	public static $table='';

	/**
	 *
	 * @var string
	 */
	public static $vtable='';

	/**
	 *
	 * @var string
	 */
	public static $sequenceName='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		//'uid'=>'uid',
		'description'=>'description',
		'name'=>'name',
		'number'=>'number',
		'default_file_path'=>'path',
		'type'=>'type',
		'mode'=>'mode',
		'state'=>'isActive',
		'priority'=>'priority',
		'maxsize'=>'maxsize',
		'maxcount'=>'maxcount',
		'open_by'=>'createBy',
		'open_date'=>'created'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
			'open_date'=>'date',
			'check_out_date'=>'date',
			'update_date'=>'date',
	);

	/**
	 *
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param integer Type of reposit, one of constants \Rbplm\Vault\Reposit::TYPE_* or let blank to get type from $mapped object.
	 * @return void
	 */
	public function loadActive($mapped, $type=null)
	{
		if(is_null($type)){
			$type = $mapped->getType();
		}
		$filter = "state=1 AND type= $type ORDER BY priority ASC";
		return $this->load( $mapped, $filter );
	}

	/**
	 *
	 * @param \Rbplm\Vault\Reposit $mapped
	 * @param string	$number
	 * @return void
	 */
	public function loadFromNumber($mapped, $number)
	{
		$filter = "number='$number'";
		return $this->load( $mapped, $filter );
	}

} //End of class
