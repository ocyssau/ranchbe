<?php
namespace Rbs\Acl;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `liveuser_areas` (
  `area_id` int(11) DEFAULT '0',
  `application_id` int(11) DEFAULT '0',
  `area_define_name` varchar(32) COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `area_id_idx` (`area_id`),
  UNIQUE KEY `define_name_i_idx` (`application_id`,`area_define_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class AreaDao extends DaoSier
{
	/**
	 * @var string
	 */
	public static $table='liveuser_areas';
	public static $vtable='liveuser_areas';
	public static $sequenceName='liveuser_areas_seq';
	public static $sequenceKey='sequence';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'area_id'=>'id',
		'area_define_name'=>'name',
		'application_id'=>'applicationId',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
	} //End of function
}