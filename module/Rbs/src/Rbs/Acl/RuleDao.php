<?php
namespace Rbs\Acl;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
CREATE TABLE `acl_rule` (
	`role_id` int(11) NOT NULL,
	`right_id` int(11) NOT NULL,
	`resource_id` int(11) NOT NULL,
	`rule` varchar(16) DEFAULT 'allow',
	PRIMARY KEY (`role_id`,`right_id`, `resource_id`),
	KEY role_idx(`role_id`),
	KEY right_idx(`right_id`),
	KEY resource_idx(`resource_id`)
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class RuleDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='acl_rule';
	public static $vtable='acl_rule';

	public static $sequenceName='';
	public static $sequenceKey='';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'role_id'=>'roleId',
		'right_id'=>'rightId',
		'resource_id'=>'resourceId',
		'rule'=>'rule'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
		$this->initStatement($this->metaModel);
	} //End of function

	/**
	 *
	 */
	public function check($userId, $right, $area)
	{
		if(!$this->checkStmt){
			$sql = "SELECT rights.right_id, rights.area_id FROM liveuser_users AS users
			LEFT OUTER JOIN liveuser_perm_users AS permusers ON permusers.auth_user_id=users.auth_user_id
			LEFT OUTER JOIN liveuser_groupusers AS groupusers ON groupusers.perm_user_id=permusers.perm_user_id
			LEFT OUTER JOIN liveuser_grouprights AS grouprights ON grouprights.group_id=groupusers.group_id
			LEFT OUTER JOIN liveuser_rights AS rights ON rights.right_id=grouprights.right_id
			WHERE (users.auth_user_id=:role OR 1=:role) AND rights.right_define_name=:right AND rights.area_id=:area";

			$this->checkStmt = $this->connexion->prepare($sql);
			$this->checkStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$this->checkStmt->execute(array(
			':role'=>$userId,
			':right'=>$right,
			':area'=>$area,
			));
		$row = $this->checkStmt->fetch();
		return $row;
	}

	/**
	 *
	 * @param MappedInterface $mapped
	 * @param array $options
	 */
	public function save(MappedInterface $mapped)
	{
		($this->connexion->inTransaction()==true) ? $withTrans = false : $withTrans = true;
		if($withTrans) $this->connexion->beginTransaction();

		$bind = array(
			':roleId'=>$mapped->getRoleId(),
			':resourceId'=>$mapped->getResourceId(),
		);

		try{
			foreach($mapped->getRules() as $rule){
				$bind[':rightId'] = $rule['rightId'];
				$bind[':rule'] = $rule['rule'];
				try{
					$this->insertStmt->execute($bind);
				}
				catch(\PDOException $e){
					$this->updateStmt->execute($bind);
					continue;
				}
			}
			if($withTrans) $this->connexion->commit();
			$mapped->isLoaded(true);
			$mapped->isSaved(true);
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
		return $mapped;
	}

	/**
	 *
	 * @return PDOStatement
	 */
	protected function initStatement($sysToApp)
	{
		//Init statement
		$table = $this->_table;

		foreach($sysToApp as $asSys=>$asApp)
		{
			$sysSet[] = $asSys;
			$appSet[] = ':'.$asApp;
			$set[] = $asSys . '=:' . $asApp;
		}

		$sql = "INSERT INTO $table " . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ');';
		$this->insertStmt = $this->connexion->prepare($sql);

		$sql = "UPDATE $table SET " . implode(',', $set) . ' WHERE resource_id=:resourceId AND right_id=:rightId AND role_id=:roleId;';
		$this->updateStmt = $this->connexion->prepare($sql);
	}

}