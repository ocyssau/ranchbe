<?php
namespace Rbs\Acl;

use Rbplm\Dao\MappedInterface;
use Rbplm\AnyPermanent;
use Rbs\Acl\Area;

class Right extends AnyPermanent
{
	public static $classId = '56a3fd3eac1ff';

	public $id;
	public $description;
	public $name;
	public $hasImplied;

	protected $area;
	public $areaId;

	protected $rights;

	/**
	 * @return void
	 */
	public function __construct(Area $area)
	{
		$this->area = $area;
		$this->areaId = $area->Id;
		$this->hasImplied = 0;
		$this->cid = self::$classId;

		$this->rights=array(
			'container_document_get'=> 'container_document_get',
			'container_document_manage'=> 'container_document_manage',
			'container_document_suppress'=> 'container_document_suppress',
			'container_document_move' => 'container_document_move',
			'container_document_copy' => 'container_document_copy',
			'container_document_assoc'=> 'container_document_assoc',
			'container_document_unlock'=> 'container_document_unlock',
			'container_document_archive'=> 'container_document_archive',
			'container_document_change_indice'=> 'container_document_change_indice',
			'container_document_change_state'=> 'container_document_change_state',
			'container_document_change_number'=> 'container_document_change_number',
			'container_document_link_file'=> 'container_document_link_file',
			'container_document_history'=> 'container_document_history',
			'container_document_version'=> 'container_document_version',
			'container_document_doctype_reset' => 'container_document_doctype_reset',
			);
	} //End of method

	/**
	 * @return Rbs\Lu\Right
	 */
	public function setArea(Area $area)
	{
		$this->area = $area;
		$this->areaId = $area->Id;
		return $this;
	} //End of method

	/**
	 * @return Rbs\Acl\Area
	 */
	public function getArea()
	{
		return $this->area;
	} //End of method

	/**
	 * @return Rbs\Acl\Right
	 */
	public function setRights(array $rights)
	{
		$this->rights = $rights;
		return $this;
	} //End of method

	/**
	 * @return array
	 */
	public function getRights()
	{
		return $this->rights;
	} //End of method

}
