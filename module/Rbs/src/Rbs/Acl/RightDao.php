<?php
namespace Rbs\Acl;

use Rbplm\Dao\NotExistingException;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
CREATE TABLE `acl_rights` (
	`id` int(11) NOT NULL,
	`area_id` int(11) NOT NULL,
	`name` varchar(32) NOT NULL,
	`description` varchar(64) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `name_i_idx` (`name`,`area_id`),
	KEY `name_idx` (`name`),
	KEY `area_idx` (`area_id`)
) ENGINE=InnoDB;

CREATE TABLE `acl_rights` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=500;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class RightDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='acl_rights';
	public static $vtable='acl_rights';

	public static $sequenceName='acl_rights_seq';
	public static $sequenceKey='id';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'area_id'=>'areaId',
		'name'=>'name',
		'description'=>'description',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
	} //End of function

	/**
	 *
	 */
	public function check($userId, $rightName, $areaId, $resourceId)
	{
		if(!$this->checkStmt){
			$sql = "
			SELECT rights.id, rights.area_id AS areaId, rule.resource_id AS resourceId, rule.rule AS rule
			FROM acl_users AS user
			LEFT OUTER JOIN acl_role AS role ON user.id=role.user_id
			LEFT OUTER JOIN acl_rule AS rule ON role.role_id=rule.role_id
			LEFT OUTER JOIN acl_rights AS rights ON rights.id=rule.right_id
			WHERE (user.id=:userId OR 1=:userId)
			AND rights.name=:rightName
			AND rights.area_id=:areaId
			AND rule.resource_id=:resourceId";
			$this->checkStmt = $this->connexion->prepare($sql);
			$this->checkStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$bind = array(
			':userId'=>$userId,
			':rightName'=>$rightName,
			':resourceId'=>$resourceId,
			':areaId'=>$areaId,
		);
		$this->checkStmt->execute($bind);

		$row = $this->checkStmt->fetch();

		if(!$row){
			throw new NotExistingException('check rights failed for this credentials');
		}

		return $row;
	}

	/**
	 * @return PDOStatement
	 */
	public function getFromRoleAndResource($roleId, $resourceId, $areaId)
	{
		if(!$this->getFromRoleStmt){
			$rightTable = $this->_table;
			$ruleTable = 'acl_rule';

			$sql = "
			SELECT
			`right`.id,
			`right`.area_id AS areaId,
			`right`.name,
			`right`.description,
			CASE ISNULL(`role_id`) WHEN 1 THEN \"inherit\" WHEN 0 THEN `rule`.`rule` END AS rule
			FROM
			(SELECT * FROM `$rightTable` WHERE `area_id` = :areaId) AS `right`
			LEFT OUTER JOIN
			(SELECT `right_id`, `role_id`, `rule` FROM `$ruleTable` WHERE `role_id`=:roleId AND `resource_id` = :resourceId) AS `rule`
			ON `right`.id=`rule`.right_id
			ORDER BY name ASC";

			$this->getFromRoleStmt = $this->connexion->prepare($sql);
			$this->getFromRoleStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$bind = array(
			':roleId'=>$roleId,
			':resourceId'=>$resourceId,
			':areaId'=>$areaId,
		);
		$this->getFromRoleStmt->execute($bind);
		return $this->getFromRoleStmt;
	}


}