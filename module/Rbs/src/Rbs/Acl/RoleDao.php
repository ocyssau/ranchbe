<?php
//%LICENCE_HEADER%

namespace Rbs\Acl;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
CREATE TABLE `acl_role` (
	`user_id` int(11) NOT NULL,
	`role_id` int(11) NOT NULL,
	PRIMARY KEY `id_i_idx` (`user_id`,`role_id`),
	KEY `user_index` (`user_id`),
	KEY `group_index` (`role_id`)
) ENGINE=InnoDB;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/
/** TESTS>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class RoleDao extends \Rbs\Dao\Sier\Link
{

	public static $vtable = 'acl_role';
	public static $table = 'acl_role';
	public static $parentTable = 'acl_users';
	public static $parentForeignKey = 'id';
	public static $childTable = 'liveuser_groups';
	public static $childForeignKey = 'group_id';

	public static $sysToApp = array(
		'user_id'=>'parentId',
		'role_id'=>'childId',
	);

	/**
	 *
	 * @param integer $userId
	 * @param integer $roleId
	 */
	public function assign($userId, $roleId)
	{
		$table = $this->_table;
		$sql = "INSERT INTO $table (user_id,role_id) VALUES (:userId,:roleId)";
		$stmt = $this->connexion->prepare($sql);
		$bind = array(
			':userId'=>$userId,
			':roleId'=>$roleId,
		);
		$stmt->execute($bind);
	} //End of function

	/**
	 * @param integer $userId
	 * @param integer $roleId
	 */
	public function deleteRole($userId, $roleId)
	{
		$table = $this->_table;
		$sql = "DELETE FROM $table WHERE user_id=:userId AND role_id=:roleId";
		$stmt = $this->connexion->prepare($sql);
		$bind = array(
			':userId'=>$userId,
			':roleId'=>$roleId,
		);
		$stmt->execute($bind);
	} //End of function

} //End of class
