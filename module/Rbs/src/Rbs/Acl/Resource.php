<?php
namespace Rbs\Lu;

use Rbplm\Dao\MappedInterface;
use Rbplm\AnyPermanent;

class Resource extends \Zend\Permissions\Acl\Resource\GenericResource
{
	public static $classId = '56a3fd3eac1ee';

	protected $prefix;
	public $applicationId;

	/**
	 * @return void
	 */
	public function __construct($prefix=null)
	{
		$this->prefix = $prefix;
		$this->applicationId = 1;
		$this->name = $prefix . md5(uniqid(rand(), true));
		$this->cid = static::$classId;
	} //End of method
}
