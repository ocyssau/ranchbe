<?php
//%LICENCE_HEADER%

namespace Rbs\Acl;

use Zend\Permissions\Acl\Role as ZfRole;
use Zend\Permissions\Acl\Resource as ZfResource;

/**
 * @brief A rule apply to a user/role pair.
 *
 */
class Role extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{
	use \Rbplm\Mapped;

	public static $classId = 'aclrole3zc547';

	/**
	 * The User Id
	 * @var integer
	 */
	protected $parentId;

	/**
	 * The Role Id
	 * @var integer
	 */
	protected $childId;

	/**
	 *
	 */
	public function __construct()
	{
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['userId'])) ? $this->parentId = $properties['userId'] : null;
		(isset($properties['roleId'])) ? $this->childId = $properties['roleId'] : null;
		(isset($properties['parentId'])) ? $this->parentId = $properties['parentId'] : null;
		(isset($properties['childId'])) ? $this->childId = $properties['childId'] : null;
	}

	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setRoleId($uid)
	{
		$this->childId = $uid;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->childId;
	}

	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setUserId($uid)
	{
		$this->parentId = $uid;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getUserId()
	{
		return $this->parentId;
	}

} //End of class
