<?php
//%LICENCE_HEADER%

namespace Rbs\Acl;

use Zend\Permissions\Acl\Role;
use Zend\Permissions\Acl\Resource;

/**
 * @brief A rule apply to a resource/role pair.
 */
class Rule extends \Rbplm\Any implements \Rbplm\Dao\MappedInterface
{

	use \Rbplm\Mapped;

	public static $classId = '89a3rhd5ad456';

	protected $resourceId;
	protected $roleId;
	protected $rightId;
	protected $rules=array();

	/**
	 *
	 */
	public function __construct()
	{
	}

	/**
	 * (non-PHPdoc)
	 * @see Rbplm\Dao.MappedInterface::hydrate()
	 */
	public function hydrate(array $properties)
	{
		(isset($properties['resourceId'])) ? $this->resourceId = $properties['resourceId'] : null;
		(isset($properties['roleId'])) ? $this->roleId = $properties['roleId'] : null;
		(isset($properties['rightId'])) ? $this->rightId = $properties['rightId'] : null;
		(isset($properties['rules'])) ? $this->rules = $properties['rules'] : null;
	}

	/**
	 *
	 * @param int $rightId
	 * @param string $rule	inherit|allow|deny
	 * @return void
	 */
	public function addRule($rightId,$rule)
	{
		$this->rules[] = array('rightId'=>$rightId, 'rule'=>$rule);
	}

	/**
	 * Getter
	 * @return string
	 */
	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setRightId($id)
	{
		$this->rightId = $id;
	}

	/**
	 * Getter
	 * @return string
	 */
	public function getRightId()
	{
		return $this->rightId;
	}


	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setResourceId($uid)
	{
		$this->resourceId = $uid;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getResourceId()
	{
		return $this->resourceId;
	}

	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setRoleId($uid)
	{
		$this->roleId = $uid;
	}

	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->roleId;
	}

} //End of class
