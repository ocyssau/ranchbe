<?php
//%LICENCE_HEADER%

namespace Rbs\Acl;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/
/** TESTS>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 * @brief Dao class for \Rbs\Acl\Acl
 *
 * See the examples: Rbplm/Acl/AclTest.php
 *
 * @see \Rbs\Dao\Sier
 * @see \Rbplm\Acl\AclTest
 *
 * About shema:
 *
 *
 *
 */
class AclDao extends DaoSier
{

	public static $vtable = 'acl_rules';
	public static $table = 'acl_rules';
	public static $sequenceName='acl_seq';


	public static $sysToApp = array(
		'privilege'=>'privilege',
		'resource_id'=>'resourceId',
		'role_id'=>'roleId',
		'rule'=>'rule'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(self::$sysToApp, DaoSier::$sysToApp);
		$this->_stdLoadSelect = 'privilege, resource_id, role_id, rule';
	} //End of function


	/**
	 */
	public function save(MappedInterface $mapped)
	{
		$table = 'liveuser_grouprights';

		$sql = "INSERT INTO $table (group_id,right_id,right_level)
		VALUES (:roleId,:privilege,:level)";

		$stmt = $this->connexion->prepare($sql);
		$cleanStmt = $this->connexion->prepare( "DELETE FROM $table WHERE right_id=:roleId AND group_id=:resourceId" );

		foreach($mapped->getAllRules() as $resourceId=>$byRuleId)
		{
			foreach($byRuleId['byRoleId'] as $roleId=>$byPrivilegeId)
			{
				$cleanStmt->execute( array(':roleId'=>$roleId, ':resourceId'=>$resourceId) );
				foreach($byPrivilegeId['byPrivilegeId'] as $privilegeId=>$rule){
					$bind = array(
						':roleId'=>$roleId,
						':privilege'=>$privilegeId,
						':level'=>3
					);
					$stmt->execute($bind);
				}
			}
		}

	} //End of function


	/**
	 * Add role of current object and parent lchild roles objects to ACL.
	 *
	 * Create Role for current object and for each of his parents groups.
	 * As it is a recusrsive method, create too Role for each parent groups of groups.
	 * Add each Role to Acl and set correctly parent in accordance to groups links relations.
	 *
	 * @param $roleId
	 * @return void
	 */
	public function initRole($roleId, $stmt=null)
	{
		//get parents roles
		if(!$stmt){
			$sql="SELECT lparent FROM anyobject_links WHERE lchild=:child";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$stmt->execute(array(':child'=>$roleId));
		$parents=array();
		while ($row = $stmt->fetch()) {
			$parents[]=$row['lparent'];
			$this->initRole($row['lparent'], $stmt);
		}
		$Acl = Acl::singleton();
		if (!$Acl->hasRole($roleId)){
			$Acl->addRole($roleId, $parents);
		}
	}

	/**
	 * Init Acl resource of a \Rbplm\AnyObject.
	 *
	 * @param $ressourceId
	 * @return void
	 */
	public function initResource($ressourceId, $stmt=null)
	{
		//get parents ressource
		if(!$stmt){
			$sql = "SELECT lparent FROM anyobject_links WHERE lchild=:child";
			$stmt = $this->connexion->prepare($sql);
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		}

		$stmt->execute(array(':child'=>$ressourceId));
		$parents = array();
		while ($row = $stmt->fetch()) {
			$parents[] = $row['lparent'];
			$this->initResource($row['lparent'], $stmt);
		}
		$Acl = Acl::singleton();

		if( !$Acl->has( $ressourceId ) ){
			$Acl->addResource( $ressourceId, $parents );
		}
	}


} //End of class
