<?php
namespace Rbs\Acl;

use Rbplm\Dao\MappedInterface;
use Rbplm\AnyPermanent;

class Area
{
	public static $classId = '56a3fd3eac1ee';

	protected $prefix;
	public $applicationId;

	const APPLICATION = 1;
	const RANCHBE = 1;
	const PROJECT = 5;
	const WORKITEM = 10;
	const MOCKUP = 15;
	const BOOKSHOP = 20;
	const CADLIB = 25;

	public static function getName($roleId)
	{
		switch($areaId){
			case self::APPLICATION:
				return 'APPLICATION';
				break;
			case self::RANCHBE:
				return 'RANCHBE';
				break;
			case self::PROJECT:
				return 'PROJECT';
				break;
			case self::WORKITEM:
				return 'WORKITEM';
				break;
			case self::MOCKUP:
				return 'MOCKUP';
				break;
			case self::BOOKSHOP:
				return 'BOOKSHOP';
				break;
			case self::CADLIB:
				return 'CADLIB';
				break;
			default:
				throw new \Exception('Unknow role id');
		}
	}

	public static function toArray()
	{
		return array(
			self::APPLICATION=>'APPLICATION',
			self::RANCHBE=>'RANCHBE',
			self::PROJECT=>'PROJECT',
			self::WORKITEM=>'WORKITEM',
			self::MOCKUP=>'MOCKUP',
			self::BOOKSHOP=>'BOOKSHOP',
			self::CADLIB=>'CADLIB',
		);
	}

	/**
	 * @return void
	 */
	public function __construct($prefix=null)
	{
		$this->prefix = $prefix;
		$this->applicationId = 1;
		$this->name = $prefix . md5(uniqid(rand(), true));
		$this->cid = static::$classId;
	} //End of method

}
