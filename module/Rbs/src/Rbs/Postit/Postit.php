<?php
//%LICENCE_HEADER%

namespace Rbs\Postit;

use Rbplm\Any;

class Postit extends Any
{
	use \Rbplm\Owned;

	static $classId = '56c4876b1e741';

	/**
	 * @var string
	 */
	protected $body;

	/**
	 * The object concerned by this postit
	 * @var string
	 */
	protected $parent = null;
	public $parentId = null;
	public $parentUid = null;
	public $parentCid = null;

	public $spaceName = null;

	/**
	 * @param array $properties
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		(isset($properties['parentCid'])) ? $this->parentCid=$properties['parentCid'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId=$properties['ownerId'] : null;
		(isset($properties['body'])) ? $this->body=$properties['body'] : null;
		(isset($properties['spaceName'])) ? $this->spaceName=$properties['spaceName'] : null;
		return $this;
	} //End of function

	/**
	 * @param string
	 */
	public function setBody( $string )
	{
		$this->body = $string;
		return $this;
	} //End of function

	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	} //End of function

	/**
	 * Set the discussion id.
	 *
	 * @param string
	 */
	public function setParent( $parent )
	{
		$this->parentId = $parent->getId();
		$this->parentUid = $parent->getUid();
		$this->parentCid = $parent->cid;
		return $this;
	} //End of function

	/**
	 * Get the discussion id.
	 *
	 * @return string
	 */
	public function getParent($asId=false)
	{
		if($asId){
			return $this->parentId;
		}
		return $this->parent;
	} //End of function

}
