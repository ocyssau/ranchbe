<?php
//%LICENCE_HEADER%

namespace Rbs\Postit;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;


/** SQL_SCRIPT>>
CREATE TABLE `postit` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `cid` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `parentId` int(11) NOT NULL,
  `parentUid` varchar(32) DEFAULT NULL,
  `parentCid` varchar(32) NOT NULL,
  `ownerId` varchar(32) DEFAULT NULL,
  `spaceName` varchar(32) NOT NULL,
  `body` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `U_uid` (`uid`),
  KEY `K_parentId` (`parentId`),
  KEY `K_parentUid` (`parentUid`),
  KEY `K_parentCid` (`parentCid`),
  KEY `K_spaceName` (`spaceName`),
  KEY `K_ownerId` (`ownerId`)
) ENGINE=InnoDB;

CREATE TABLE `postit_seq`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)ENGINE=MyISAM AUTO_INCREMENT=10;

 <<*/

/** SQL_INSERT>>
INSERT INTO postit_seq(id) VALUES(10);
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class PostitDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table='postit';
	public static $vtable='postit';
	public static $sequenceName='postit_seq';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'cid',
		'name'=>'name',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'parentCid'=>'parentCid',
		'spaceName'=>'spaceName',
		'ownerId'=>'ownerId',
		'body'=>'body',
	);

	/**
	 * @param integer $parentId
	 * @return PDOStatement
	 */
	public function getFromParent($parentId)
	{
		$spaceName = $this->factory->getName();

		$table = $this->_table;
		$sql = "SELECT * FROM $table WHERE parentId=:parentId AND spaceName=:spaceName";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		$bind = array(
			':parentId'=>$parentId,
			':spaceName'=>$spaceName
		);

		$stmt->execute($bind);
		return $stmt;
	}

}
