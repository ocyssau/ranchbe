<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Wf\ActivityDao;

/** SQL_SCRIPT>>
CREATE TABLE `galaxia_transitions` (
	`uid` varchar(32) NOT NULL,
	`processId` int(14) NOT NULL DEFAULT '0',
	`parentId` int(14) NOT NULL DEFAULT '0',
	`parentUid` varchar(32) NOT NULL,
	`childId` int(14) NOT NULL DEFAULT '0',
	`childUid` varchar(32) NOT NULL,
	`name` varchar(32) DEFAULT NULL,
	`attributes` text,
	`lindex` int(7) DEFAULT '0',
	PRIMARY KEY (`parentId`,`childId`),
	UNIQUE KEY `uid` (`uid`),
	KEY `parentUid` (`parentUid`),
	KEY `childUid` (`childUid`)
) ENGINE=InnoDB;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class TransitionDao extends \Rbs\Dao\Sier\Link
{

	/**
	 *
	 * @var unknown_type
	 */
	static $sysToApp = array(
		'uid'=>'uid',
		'parentId'=>'parentId',
		'childId'=>'childId',
		'parentUid'=>'parentUid',
		'childUid'=>'childUid',
		'name'=>'name',
		'lindex'=>'index',
		'attributes'=>'attributes',
	);

	/**
	 * @var string
	 */
	public static $table = 'galaxia_transitions';
	public static $vtable = 'galaxia_transitions';
	public static $childTable = 'galaxia_activities';
	public static $childForeignKey = 'activityId';
	public static $parentTable = 'galaxia_activities';
	public static $parentForeignKey = 'activityId';

	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$activityTable = ActivityDao::$table;
			$sql = "DELETE FROM trans USING $table AS trans LEFT JOIN $activityTable AS act ON (trans.parentId = act.activityId) WHERE pId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

	/**
	 *
	 */
	public function loadChildren($mapped)
	{
		$table = static::$table;
		if(!$this->loadChildrenStmt){
			$sql="SELECT * FROM $table WHERE lparent=:parentId";
			$this->loadChildrenStmt = $this->connexion->prepare($sql);
			$this->loadChildrenStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		$this->loadChildrenStmt->execute(array(':parentId'=>$mapped->id));
		while($row = $this->loadChildrenStmt->fetch()){
			$lnk = new Link();
			$mapped->links[$lnk->uid]=$lnk;
		}
		return $mapped;
	}

	/**
	 * Getter for activities. Return a list.
	 *
	 * @param Model\Any
	 * @return Dao\DaoList
	 */
	public function getFromProcess($processId)
	{
		$table = static::$table;
		$childTable = static::$childTable;
		$childForeignKey = static::$childForeignKey;
		$parentTable = static::$parentTable;
		$parentForeignKey = static::$parentForeignKey;

		$sql = "SELECT trans.* FROM $table AS trans";
		$sql .= " JOIN $parentTable AS parent ON parent.$parentForeignKey = trans.parentId";
		$sql .= " WHERE parent.pId=:processId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':processId'=>$processId));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

} //End of class
