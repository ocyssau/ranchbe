<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;
use Rbplm\Dao\MappedInterface;
use Rbplm\Signal;
use Workflow\Model\Wf;

/** SQL_SCRIPT>>

CREATE TABLE IF NOT EXISTS `galaxia_instances` (
`instanceId` int(14) NOT NULL AUTO_INCREMENT,
`pId` int(14) NOT NULL DEFAULT '0',
`started` int(14) DEFAULT NULL,
`name` varchar(200) COLLATE latin1_general_ci DEFAULT 'No Name',
`owner` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
`nextActivity` int(14) DEFAULT NULL,
`nextUser` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
`ended` int(14) DEFAULT NULL,
`status` enum('active','exception','aborted','completed') COLLATE latin1_general_ci DEFAULT NULL,
`properties` longblob,
PRIMARY KEY (`instanceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class InstanceDao extends DaoSier
{
	/**
	 *
	 * @var integer
	 */
	public static $classId = 421;

	/**
	 *
	 * @var string
	 */
	public static $table='galaxia_instances';
	protected $_table;

	/**
	 *
	 * @var string
	 */
	public static $vtable='galaxia_instances';
	protected $_vtable;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
			'instanceId'=>'id',
			'pId'=>'processId',
			'started'=>'started',
			'name'=>'name',
			'owner'=>'ownerId',
			'nextActivity'=>'nextActivities',
			'nextUser'=>'nextUsers',
			'ended'=>'ended',
			'status'=>'status',
			'properties'=>'attributes',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
			'started'=>'date',
			'ended'=>'date',
			'properties'=>'json',
			'nextActivity'=>'json',
			'nextUser'=>'json',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = self::$sysToApp;
		$this->sequenceName = null;
		$this->metaModelFilters = self::$sysToAppFilter;
	} //End of function

	/**
	 * Load a instance from Instance JOIN with Activity
	 * @todo Escape sql injection
	 *
	 * @param Any $mapped
	 * @param string $filter
	 * @return Any
	 * @throws Exception
	 */
	public function load( MappedInterface $mapped, $filter=null, $bind=null )
	{
		if( $mapped->isLoaded() == true && $force == false ){
			Rbplm::log('Object ' . $mapped->getUid() . ' is yet loaded');
			return;
		}

		Signal::trigger(self::SIGNAL_PRE_LOAD, $this);

		//select field of activity
		$select = implode(',', array(
				'obj.*',
				'proc.isActive',
				'proc.isValid',
				'proc.normalized_name as normalizedName',
				'proc.version',
		));
		$sql = "SELECT $select FROM galaxia_instances AS obj JOIN galaxia_processes AS proc ON obj.pId=proc.pId";

		if(is_a($filter, '\Rbplm\Dao\Filter\FilterInterface')){
			$filter = $filter->__toString();
		}
		if($filter){
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute($bind);
		$row = $stmt->fetch();

		if($row){
			$this->hydrate($mapped, $row);
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql) );
		}
		$mapped->isLoaded(true);
		Signal::trigger(self::SIGNAL_POST_LOAD, $this);
		return $mapped;
	} //End of method



	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE pId=:processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

	/**
	 * Getter for runningActivities.
	 * Return also properties of activity object as :
	 * type, isComment, isInteractive, isAutomatic
	 *
	 * @param integer $processInstanceId
	 * @return array
	 */
	public function getRunningActivities($processInstanceId)
	{
		$status = Wf\Instance\Activity::STATUS_RUNNING;

		$sql = "SELECT inst.*, act.type, act.isComment, act.isInteractive, act.isAutomatic
		FROM galaxia_instance_activities AS inst
		JOIN galaxia_activities AS act ON inst.activityId=act.activityId
		WHERE inst.status=:status AND inst.instanceId = :instanceId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':status'=>$status, ':instanceId'=>$processInstanceId));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Get Activities children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * Return all properties of Wf\Activity object in App sementic
	 * 	with nextStatus attach to transition
	 *  and process instance id as instanceId
	 *
	 *  $selectActivities may contain a list of id of Activity to load
	 *  $selectTransition may contain a list of name of transitions
	 *
	 * @param integer $activityId
	 * @param array $selectActivities
	 * @param array $selectTransition
	 * @return array
	 */
	public function getNextCandidates($actInstId, $selectActivities=null, $selectTransition=null)
	{
		$filter = null;

		if($selectActivities){
			$filter = ' AND act.id IN ('.implode(',', $selectActivities).')';
		}

		if($selectTransition){
			$filter = ' AND trans.name IN ('.implode(',', $selectTransition).')';
		}

		$sql = "SELECT act.*, trans.name AS nextStatus, actinst.instanceId AS instanceId
		FROM galaxia_activities AS act
		JOIN galaxia_transitions AS trans ON act.activityId = trans.childId
		JOIN (SELECT activityId, instanceId FROM galaxia_instance_activities WHERE id=:actinstId) AS actinst
		WHERE trans.parentId = actinst.activityId" . $filter;

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':actinstId'=>$actInstId));

		//Apply filters to data and convert to app sementic
		$asApp = array();
		foreach($stmt->fetchAll(\PDO::FETCH_ASSOC) as $asSys){
			$asApp[] = ActivityDao::toApp($asSys);
		}

		return $asApp;
	}


	/**
	 * Get Transition children of Activity linked to Instance Activity identified by $actInstId
	 *
	 * @param integer $activityId
	 * @return array
	 */
	public function getNextTransitions($actInstId)
	{
		$sql = "SELECT trans.*, actinst.instanceId AS instanceId, act.name as actName
		FROM galaxia_activities AS act
		JOIN galaxia_activities AS trans ON act.activityId = trans.actToId
		JOIN (SELECT activityId, instanceId FROM wf_instance_activities WHERE activityId=:activityId) AS actinst
		WHERE trans.parentId = actinst.activityId";

		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':activityId'=>$actInstId));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}


	/**
	 *
	 */
	public function getNextActivitiesFromDocumentId($documentId)
	{
		//get instances
		$sql  = "SELECT
				inst.instanceId,
				act.activityId,
				act.name,
				act.type as type
		FROM workitem_documents AS doc
		JOIN galaxia_instance_activities AS actInst ON doc.instance_id = actInst.instanceId
		JOIN galaxia_activities AS act ON actInst.activityId = act.activityId
		JOIN galaxia_instances AS inst ON inst.instanceId=actInst.instanceId";
		$sql .= " WHERE doc.document_id = :documentId AND actInst.status='running' AND inst.status='running'";
		$sql .= " ORDER BY inst.instanceId ASC";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':documentId'=>$documentId));

		return $list;
	}

	/**
	 *
	 */
	public function getInstanceFromDocumentId($documentId, $status = 'running')
	{
		//get standalone activities
		$sql  = "SELECT inst.* FROM workitem_documents AS doc";
		$sql .= " JOIN galaxia_instances AS inst ON doc.instance_id = inst.instanceId";
		$sql .= " WHERE doc.document_id = :documentId AND inst.status=:status";
		$sql .= " ORDER BY inst.instanceId ASC";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':documentId'=>$documentId, ':status'=>$status));

		return $list;
	}



}
