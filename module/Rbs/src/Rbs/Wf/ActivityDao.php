<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>
CREATE TABLE IF NOT EXISTS `galaxia_activities` (
  `activityId` int(14) NOT NULL AUTO_INCREMENT,
  `cid` INT NOT NULL DEFAULT '421' AFTER `activityId`,
  `uid` VARCHAR( 32 ) NULL AFTER `activityId`,
  `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `type` enum('start','end','split','switch','join','activity','standalone') COLLATE latin1_general_ci DEFAULT NULL,
  `ownerId` varchar(255) NULL,
  `parentId` int NULL,
  `pId` int(14) NOT NULL DEFAULT '0',
  `parentUid` varchar(255) NULL,
  `updateById` varchar(255) NULL,
  `updated` datetime NULL,
  `lastModif` int(14) DEFAULT NULL,
  `expirationTime` int(6) unsigned NOT NULL DEFAULT '0',
  `isAutoRouted` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isInteractive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isAutomatic` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `isComment` char(1) COLLATE latin1_general_ci DEFAULT NULL,
  `flowNum` int(10) DEFAULT NULL,
  `progression` float NULL,
  `roles` text NULL,
  `attributes` TEXT NULL,
  PRIMARY KEY (`activityId`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
 ALTER TABLE `galaxia_activities`
		ADD UNIQUE (`uid`),
		ADD INDEX (roles(255)),
		ADD INDEX ( `ownerId` ),
		ADD INDEX ( `parentId` ),
		ADD INDEX ( `parentUid` );
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ActivityDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'galaxia_activities';

	/**
	 *
	 * @var string
	 */
	public static $vtable='galaxia_activities';

	/**
	 * @var array
	 */
	static $sysToApp = array(
			'activityId'=>'id',
			'uid'=>'uid',
			'cid'=>'cid',
			'name'=>'name',
			'lastModif'=>'updated',
			'description'=>'title',
			'normalized_name'=>'normalizedName',
			'pId'=>'processId',
			'isInteractive'=>'isInteractive',
			'isAutoRouted'=>'isAutorouted',
			'isAutomatic'=>'isAutomatic',
			'isComment'=>'isComment',
			'type'=>'type',
			'expirationTime'=>'expirationTime',
			'flowNum'=>'flowNum',
			'progression'=>'progression',
			'attributes'=>'attributes',
			'roles'=>'roles'
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
			'lastModif'=>'date',
			'expirationTime'=>'date',
			'isInteractive'=>'yesOrNo',
			'isAutoRouted'=>'yesOrNo',
			'isAutomatic'=>'yesOrNo',
			'isComment'=>'yesOrNo',
			'attributes'=>'json',
			'roles'=>'json'
	);

	/**
	 * Delete all activities of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$sql = "DELETE FROM $table WHERE pId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

} //End of class

