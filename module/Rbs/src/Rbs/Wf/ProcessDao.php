<?php
//%LICENCE_HEADER%

namespace Rbs\Wf;

use Rbs\Dao\Sier as DaoSier;
use Workflow\Model\Wf;

/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `galaxia_processes` (
 `pId` int(14) NOT NULL AUTO_INCREMENT,
 `name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
 `isValid` char(1) COLLATE latin1_general_ci DEFAULT NULL,
 `isActive` char(1) COLLATE latin1_general_ci DEFAULT NULL,
 `version` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
 `description` text COLLATE latin1_general_ci,
 `lastModif` int(14) DEFAULT NULL,
 `normalized_name` varchar(80) COLLATE latin1_general_ci DEFAULT NULL,
 PRIMARY KEY (`pId`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ProcessDao extends DaoSier
{
	/**
	 *
	 * @var integer
	 */
	public static $classId = 420;

	/**
	 *
	 * @var string
	 */
	public static $table='galaxia_processes';

	/**
	 *
	 * @var string
	 */
	public static $vtable='galaxia_processes';

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'pId'=>'id',
		'name'=>'name',
		'isValid'=>'isValid',
		'isActive'=>'isActive',
		'version'=>'version',
		'description'=>'title',
		'lastModif'=>'lastModif',
		'normalized_name'=>'normalizedName',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'lastModif'=>'date',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		parent::__construct($conn);
		$this->metaModel = array_merge(self::$sysToApp, DaoSier::$sysToApp);
		$this->sequenceName=null;
		$this->metaModelFilters = self::$sysToAppFilter;
	} //End of function

	/**
	 *
	 */
	public function getDefaultProcessFromDocumentId($documentId)
	{
		$sql = "SELECT proc.* FROM workitem_documents AS doc
				JOIN workitems AS cont ON doc.workitem_id = cont.workitem_id
				JOIN galaxia_processes AS proc ON proc.pId = cont.default_process_id";
		$sql .= " WHERE doc.document_id = :documentId";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':documentId'=>$documentId));

		return $list;
	}

	/**
	 *
	 */
	public function getActivities($processId, $type)
	{
		$sql = "SELECT act.* FROM galaxia_activities AS act";
		$sql .= " JOIN galaxia_processes AS proc ON proc.pId = act.pId";
		$sql .= " WHERE proc.pId = :processId AND act.type=:type";

		$list = new \Rbs\Dao\Sier\DaoList(null);
		$list->loadFromSql($sql, array(':processId'=>$processId, ':type'=>$type));

		return $list;
	}

	/**
	 * Getter for activities. Return a list.
	 *
	 * @param Model\Any
	 * @return Dao\DaoList
	 */
	public function getTransitions($processId)
	{
		$sql  = "SELECT trans.* FROM galaxia_transitions AS trans";
		$sql .= " JOIN galaxia_activities AS act ON act.activityId = trans.parentId";
		$sql .= " WHERE act.pId=:processId";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':processId'=>$processId));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Getter for activities. Return a list.
	 *
	 * @param Model\Any
	 * @return Dao\DaoList
	 */
	public function getVersions($processName)
	{
		$table = self::$table;

		$sql  = "SELECT version FROM $table WHERE name=:processName ORDER BY version DESC";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute(array(':processName'=>$processName));
		return $stmt->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * @param integer $processId
	 * @param \Rbs\Dao\Factory $factory
	 */
	public function getGraphAsArray($processId, $factory)
	{
		//load activitities
		$activities = $factory->getList(Wf\Activity\Activity::$classId);
		$activities->load('pId=:processId', array(':processId'=>$processId));
		$activities->setOption('asapp', true);
		$activities->dao = $factory->getDao(Wf\Activity\Activity::$classId);

		/* load transitions */
		$sql = "SELECT trans.parentId, trans.childId, trans.name, trans.attributes
		FROM galaxia_transitions AS trans
		JOIN galaxia_activities AS act ON act.activityId = trans.parentId";
		$sql .= " WHERE act.pId=:processId";

		$transitions = $factory->getList(Wf\Transition::$classId);
		$transitions->loadFromSql(trim($sql), array(':processId'=>$processId));

		foreach($transitions as $transition){
			$attributes = json_decode($transition['attributes'], true);

			$return['transitions'][] = array(
				'name'=>$transition['name'],
				'fromid'=>$transition['parentId'],
				'toid'=>$transition['childId'],
				'attributes'=>$attributes,
			);
		}

		foreach($activities as $activity){
			$attributes = $activity['attributes'];
			$roles = $activity['roles'];

			if(!is_array($attributes)){
				$attributes = json_decode($attributes, true);
			}
			if(!is_array($roles)){
				$roles = json_decode($roles, true);
			}

			$return['activities'][] = array(
				'id'=>$activity['id'],
				'name'=>$activity['name'],
				'title'=>$activity['title'],
				'type'=>$activity['type'],
				'isAutomatic'=>(int)$activity['isAutomatic'],
				'isComment'=>(int)$activity['isComment'],
				'isInteractive'=>(int)$activity['isInteractive'],
				'progression'=>$activity['progression'],
				'attributes'=>$attributes,
				'roles'=>$roles
			);
		}

		return $return;
	}


	/**
	 * Return the activities setted with parentId and parentUid from transitions
	 * @param integer
	 * @return \Rbplm\Dao\ListInterface
	 */
	public function getGraphWithTransitions($filter, $bind)
	{
		$factory = $this->factory;
		if(!$factory){
			throw new \Exception('the factory public property must be set before');
		}

		/* populate activities with parentId property */
		$sql = "SELECT
		trans.parentId,
		trans.parentUid,
		trans.name as status,
		act.activityId as id,
		act.uid,
		act.cid,
		act.name,
		act.normalized_name as normalizedName,
		act.description as title,
		act.ownerId,
		act.updateById,
		act.updated,
		act.isInteractive,
		act.isAutorouted,
		act.isAutomatic,
		act.isComment,
		act.type,
		act.pId as processId,
		act.progression,
		act.expirationTime,
		act.roles,
		act.attributes
		FROM galaxia_activities AS act
		LEFT OUTER JOIN galaxia_transitions as trans ON act.activityId=trans.childId
		WHERE $filter";
		$list = $factory->getList(Wf\Activity\Activity::$classId)->loadFromSql($sql, $bind);
		return $list;
	}
}
