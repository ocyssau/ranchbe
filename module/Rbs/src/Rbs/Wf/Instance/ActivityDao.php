<?php
//%LICENCE_HEADER%

namespace Rbs\Wf\Instance;

use Rbs\Dao\Sier as DaoSier;
use Workflow\Model\Wf;


/** SQL_SCRIPT>>
 CREATE TABLE IF NOT EXISTS `galaxia_instance_activities` (
 `id` int(14) NOT NULL AUTO_INCREMENT,
 `instanceId` int(14) NOT NULL DEFAULT '0',
 `activityId` int(14) NOT NULL DEFAULT '0',
 `started` int(14) NOT NULL DEFAULT '0',
 `ended` int(14) NOT NULL DEFAULT '0',
 `user` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
 `status` enum('running','completed') COLLATE latin1_general_ci DEFAULT NULL,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE `galaxia_instance_activities` ADD INDEX ( `instanceId` , `activityId` );
 ALTER TABLE `galaxia_instance_activities` ADD INDEX ( `status` );
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

/**
 *
 */
class ActivityDao extends DaoSier
{

	/**
	 *
	 * @var string
	 */
	public static $table = 'galaxia_instance_activities';
	public $_table;

	/**
	 *
	 * @var string
	 */
	public static $vtable='galaxia_instance_activities';
	public $_vtable;

	/**
	 * @var array
	 */
	static $sysToApp = array(
		'id'=>'id',
		'instanceId'=>'instanceId',
		'activityId'=>'activityId',
		'started'=>'started',
		'ended'=>'ended',
		'user'=>'ownerId',
		'status'=>'status',
	);

	/**
	 *
	 * @var array
	 */
	public static $sysToAppFilter = array(
		'started'=>'date',
		'ended'=>'date',
		'expirationTime'=>'date',
		'isInteractive'=>'yesOrNo',
		'isAutoRouted'=>'yesOrNo',
		'isAutomatic'=>'yesOrNo',
		'isComment'=>'yesOrNo',
		'actattributes'=>'json',
		'roles'=>'json'
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function loadWithActivity($mapped, $filter=null, $bind=null)
	{
		$table = $this->_table;

		//select field of activity
		$select = implode(',', array(
			'obj.*',
			'act.pId',
			'act.type',
			'act.isInteractive',
			'act.isAutoRouted',
			'act.isAutomatic',
			'act.isComment',
			'act.expirationTime',
			'act.roles',
			'act.attributes AS actattributes',
			'act.name as actname'
		));

		$sql = "SELECT $select  FROM $table AS obj JOIN galaxia_activities AS act ON obj.activityId = act.activityId";

		if($filter){
			$sql .= " WHERE $filter";
		}
		return $this->loadFromSql($mapped, $sql, $bind);
	} //End of function

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * Rewrite the Dao hydrator to specify properties different when load and when save
	 * ActivityInstance is populate with instance and activity properties
	 *
	 * @param Any    				$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbplm\AnyObject
	 */
	public function hydrate( $mapped, array $row, $fromApp = false )
	{
		$properties = array();
		$metaModel = $this->metaModel;
		$sysToAppFilter = $this->metaModelFilters;

		$metaModel = array_merge($metaModel, array(
			'pId'=>'processId',
			'isInteractive'=>'isInteractive',
			'isAutoRouted'=>'isAutorouted',
			'isAutomatic'=>'isAutomatic',
			'isComment'=>'isComment',
			'expirationTime'=>'expirationTime',
			'roles'=>'roles',
			'actAttributes'=>'attributes',
			'actname'=>'name',
		));

		if($fromApp){
			foreach($metaModel as $asSys=>$asApp){
				$properties[$asApp] = $row[$asApp];
			}
		}
		else{
			foreach($metaModel as $asSys=>$asApp){
				if(isset($sysToAppFilter[$asSys])){
					$filterMethod = $sysToAppFilter[$asSys].'ToApp';
					$properties[$asApp] = static::$filterMethod($row[$asSys]);
				}
				else{
					$properties[$asApp] = $row[$asSys];
				}
			}
		}

		$mapped->setUid($properties['id']);
		$mapped->hydrate($properties);
		return $mapped;
	} //End of function


	/**
	 * Getter for previous. Return a list.
	 *
	 * @param Any
	 * @return array
	 */
	public function getPrevious($mapped)
	{
	}

	/**
	 * Getter for comments. Return a list.
	 *
	 * @param Any
	 * @return array
	 */
	public function getComments($mapped)
	{
	}

	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId, $withChilds=false, $withTrans=false)
	{
		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$activityTable = \Rbs\Wf\ActivityDao::$table;
			$sql  = "DELETE FROM actInst USING $table AS actInst";
			$sql .= " LEFT JOIN $activityTable AS act ON (actInst.activityId = act.activityId)";
			$sql .= " WHERE act.pId = :processId";

			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}

		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromInstance($instanceId, $withChilds=false, $withTrans=true)
	{
		$filter = "instanceId=:instanceId";
		$bind = array( ':instanceId'=>$instanceId );
		$this->_deleteFromFilter($filter, $bind, $withChilds, $withTrans);
		return $this;
	}

} //End of class

