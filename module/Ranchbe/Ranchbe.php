<?php

use Rbs\Sys\ErrorStack;

class Ranchbe
{
	/**
	 * Current version of Ranchbe
	 * @var string
	 */
	const VERSION = '1.0';

	/**
	 * Build number of Ranchbe
	 * @var string
	 */
	const BUILD = '';

	/**
	 * Copyright
	 * @var string
	 */
	const COPYRIGHT = '&#169;2007-2015 By Olivier CYSSAU For SIER';

	/**
	 * Web Site
	 * @var string
	 */
	const WEBSITE = 'https://bitbucket.org/ocyssau/rbsier';

	/**
	 *
	 * @var \Rbplm\Sys\Logger
	 */
	private static $logger = null;

	/**
	 *
	 * @var \View\View | \Application\View\ViewModel
	 */
	private static $view = null;

	/**
	 * OBSOLETE
	 * @var unknown
	 */
	private static $db = null;

	/**
	 *
	 * @var \Rbs\Sys\ErrorStack
	 */
	private static $errorStack = null;

	/**
	 *
	 * @var \Rbplm\People\User $currentUser
	 */
	private static $currentUser = null;

	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface $auth
	 */
	private static $auth = null;

	/**
	 *
	 * @var array
	 */
	protected static $fileTypes;

	/**
	 * authorized extensions
	 * @var array
	 */
	protected static $fileExtensions = null;

	/**
	 * authorized file extensions for visu files
	 * @var array
	 */
	protected static $visuFileExtensions = null;

	/**
	 *
	 * @var string
	 */
	public static $lang = 'en';

	/**
	 *
	 * @var array
	 */
	public static $traductions;

	/**
	 *
	 * @var array
	 */
	public static $registry = array();

	/**
	 * @var Ranchbe
	 */
	protected static $instance;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 *
	 */
	public function __construct()
	{
		self::$instance = $this;
	}

	/**
	 *
	 * @return Rbcs
	 */
	public static function get()
	{
		if(!isset(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * @param array $array
	 * @return Rbcs
	 */
	public function setConfig($array)
	{
		$this->config = $array;
		return $this;
	}

	/**
	 * Return the value of configuration constant value.
	 * $name may be under form prefix.value in lower case
	 *
	 * @return array
	 */
	public function getConfig($name=null)
	{
		if($name){
			if(isset($this->config[$name])){
				return $this->config[$name];
			}
			else{
				$constName = strtoupper(str_replace('.', '_', $name));
				if(defined($constName)){
					return constant($constName);
				}
			}
		}
		else{
			return $this->config;
		}
	}

	/**
	 * @return string
	 */
	public static function getVersion()
	{
		return 'RanchBE ver:'. self::VERSION .' Build:'. self::BUILD. ' '.self::COPYRIGHT;
	}

	/**
	 * @return \Rbplm\Sys\Logger
	 */
	public static function getLogger()
	{
		return self::$logger;
	}

	/**
	 *
	 * @param \Rbplm\Sys\Logger $logger
	 */
	public static function setLogger($logger)
	{
		self::$logger = $logger;
	}

	/**
	 *
	 * @param string $msg
	 */
	public static function log($msg)
	{
		if(self::$logger){
			self::$logger->log( $msg );
		}
	}

	/**
	 * @return Smarty
	 */
	public static function getView()
	{
		if(!self::$view){
			rbinit_view();
		}
		return self::$view;
	}

	/**
	 *
	 * @param \View\View | \Application\View\ViewModel $view
	 */
	public static function setView($view)
	{
		self::$view = $view;
	}

	/**
	 * @todo DELETE OBSOLETE
	 * @return ADODB_mysqli
	 */
	private static function _____getDb()
	{
		return self::$db;
	}

	/**
	 * @todo DELETE OBSOLETE
	 * @param unknown $db
	 */
	private static function _______setDb($db)
	{
		self::$db = $db;
	}

	/**
	 * @return \Rbs\Sys\ErrorStack
	 */
	public static function getErrorStack()
	{
		return self::$errorStack;
	}

	/**
	 *
	 * @param \Rbs\Sys\ErrorStack $errorStack
	 */
	public static function setErrorStack($errorStack)
	{
		self::$errorStack = $errorStack;
	}

	/**
	 * @return \Rbplm\People\User
	 */
	public static function getCurrentUser()
	{
		if(!isset(self::$currentUser)){
			rbinit_currentUser();
		}
		return self::$currentUser;
	}

	/**
	 *
	 * @param \Rbplm\People\User $user
	 */
	public static function setCurrentUser($user)
	{
		self::$currentUser = $user;
	}

	/**
	 * @return \Zend\Authentication\Adapter\AdapterInterface
	 */
	public static function getAuthservice()
	{
		return self::$auth;
	}

	/**
	 *
	 * @param \Zend\Authentication\Adapter\AdapterInterface $auth
	 */
	public static function setAuthservice($auth)
	{
		self::$auth = $auth;
	}

	/**
	 * Define path mapping
	 * $path_mapping is used to translate path from server to path use by the client.
	 * You can use %user% wich will be replaced by the username.
	 */
	public static function getPathMap()
	{
		switch (CLIENT_OS) {
			// Windows
			case 'WINDOWS':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR=>MOCKUP_WINDOWS_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_WINDOWS_PATH_MAPPING,
					DEFAULT_CADLIB_DIR=>CADLIB_WINDOWS_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR=>WORKITEM_WINDOWS_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR=>WILSPACE_WINDOWS_PATH_MAPPING
				);
				break;
				// Mac
			case 'MACINTOSH':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR=>MOCKUP_MAC_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_MAC_PATH_MAPPING,
					DEFAULT_CADLIB_DIR=>CADLIB_MAC_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR=>WORKITEM_MAC_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR=>WILSPACE_MAC_PATH_MAPPING
				);
				break;
				// Unix
			case 'UNIX':
				$path_mapping = array(
					DEFAULT_MOCKUP_DIR=>MOCKUP_UNIX_PATH_MAPPING,
					DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_UNIX_PATH_MAPPING,
					DEFAULT_CADLIB_DIR=>CADLIB_UNIX_PATH_MAPPING,
					DEFAULT_WORKITEM_DIR=>WORKITEM_UNIX_PATH_MAPPING,
					DEFAULT_WILDSPACE_DIR=>WILSPACE_UNIX_PATH_MAPPING
				);
				break;
		}
		return $path_mapping;
	}

	/**
	 *
	 */
	public static function tra($txt)
	{
		if( is_null(self::$traductions) ){
			$lg = self::$lang;
			include("lang/$lg/language.php");
			self::$traductions = $lang;
		}
		$key = strtolower($txt);
		if (isset(self::$traductions[$key])) {
			return self::$traductions[$key];
		}
		else {
			if(DEBUG){
				if($key <> ''){
					$lg = self::$lang;
					$handle=fopen("lang/$lg/toTraduct.txt", 'a');
					fwrite($handle, $key.PHP_EOL);
					fclose($handle);
				}
			}
			return $txt;
		}
	}

	/**
	 * Set Authorized files types.
	 * 	Extract list of type from mimes.conf file and populate
	 *  static array var self::$fileTypes and self::$fileExtensions
	 *
	 * @return void
	 */
	protected static function _getFilesTypes()
	{
		$validFileExt = array (); //init var
		$mimeFile = 'config/mimes.csv';

		if (! $handle = fopen ( $mimeFile, "r" )) {
			print 'cant open mime definition file :'.$mimeFile;
			die ();
		}
		else {
			while ( ! feof ( $handle ) ) {
				$data = fgetcsv ( $handle, 1000, ";" );
				if (@implode ( ' ', $data )) { //Test for ignore empty lines
					$extensions = explode ( ' ', $data [2] );
					$validFileExt = array_merge( $validFileExt, $extensions );
				}
			}
			fclose ( $handle );
		}

		self::$fileTypes ['file'] = 'file';
		self::$fileExtensions ['file'] = array_combine($validFileExt, $validFileExt); //Valid extension for type file
		sort( self::$fileExtensions ['file'] );
		unset ( $extensions );
		unset ( $valid_file_ext );

		//Define extension to determine no file type
		self::$fileTypes ['nofile'] = 'nofile'; //Never change this value
		self::$fileExtensions ['nofile'] = array ('NULL' => '' ); //Valid extension for type nofile

		$visuFileExtensions = self::get()->getConfig('visu.fileExtension');
		self::$visuFileExtensions = array_combine ( $visuFileExtensions, $visuFileExtensions );
		sort( self::$visuFileExtensions );

		return $this;
	}

	/**
	 * Get file type from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileTypes()
	{
		if (self::$fileTypes === null) {
			self::_getFilesTypes ();
		}
		return self::$fileTypes;
	}

	/**
	 * Get file extensions from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getFileExtensions()
	{
		if (self::$fileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$fileExtensions;
	}

	/**
	 * Get visu file extensions list from _getFilesTypes() method
	 *
	 * @return array
	 */
	public static function getVisuFileExtensions()
	{
		if (self::$visuFileExtensions === null) {
			self::_getFilesTypes ();
		}
		return self::$visuFileExtensions;
	}

}
