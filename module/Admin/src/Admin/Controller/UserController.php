<?php
namespace Admin\Controller;

use Zend\View\Model\ViewModel;

use Application\Controller;
use Application\Model;
use Application\Dao;
use Admin\Form;

use Rbplm\People;

class UserController extends Controller\AbstractController
{
	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$list = Dao\Factory::get()->getList(Model\People\User::$classId);
		$list->load();
		$view->list = $list;
		$view->headers = array(
			'firstname'=>'firstname',
			'lastname'=>'lastname',
			'login'=>'login',
			'mail'=>'mail',
			'lastlogin'=>'lastlogin',
			'role'=>'memberof'
		);
		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		$form = new Form\UserForm($acl);
		$model = Model\People\User::init();
		$form->bind($model);
		$factory = Dao\Factory::get();

		if($request->isPost()){
			$form->setData($request->getPost());
			if($form->isValid()){
				$pass1 = $form->get('password1')->getValue();
				$pass2 = $form->get('password2')->getValue();
				if($pass1==$pass2 && $pass1){
					$model->setPassword(md5($pass1));
				}
				$factory->getDao($model)->save($model);
				$this->redirect()->toRoute('adminuser', array('action'=>'index'));
			}
		}
		$view->title = 'Add user';
		$view->setTemplate('admin/user/edit');
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		$form = new Form\UserForm($acl);
		$model = new Model\People\User();
		$factory = Dao\Factory::get();

		$id = (int)$this->params()->fromRoute('id');
		$factory->getDao($model)->loadFromId($model, $id);
		$form->bind($model);

		if($request->isPost()){
			$form->setData($request->getPost());
			if($form->isValid()){
				$pass1 = $form->get('password1')->getValue();
				$pass2 = $form->get('password2')->getValue();
				if($pass1==$pass2 && $pass1){
					$model->setPassword( md5($pass1) );
				}
				$factory->getDao($model)->save($model);
				$this->redirect()->toRoute('adminuser', array('action'=>'index'));
			}
		}
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_ADMIN)){
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$request = $this->getRequest();
		$model = new Model\People\User();
		$factory = Dao\Factory::get();

		$id = (int)$this->params()->fromRoute('id');
		$factory->getDao($model)->loadFromId($model, $id);
		$factory->getDao($model)->deleteFromLogin($model->getLogin());

		$this->redirect()->toRoute('adminuser', array('action'=>'index'));
		$view->return = json_encode($model);
		return $view;
	}
}
