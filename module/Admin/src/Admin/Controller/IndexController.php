<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;

use Application\Controller;

use Rbplm\People;

class IndexController extends Controller\AbstractController
{
    public function indexAction()
    {
    	$appAcl = People\CurrentUser::get()->appacl;
    	if(!$appAcl->hasRight($appAcl::RIGHT_ADMIN)){
    		return $this->notauthorized();
    	}

        return new ViewModel();
    }

    /**
     * (non-PHPdoc)
     * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
     */
    public function databaseAction()
    {
    	$appAcl = People\CurrentUser::get()->appacl;

    	if($appAcl->hasRight($appAcl::RIGHT_MANAGE)){
    		$config =  $this->getServiceLocator()->get('Configuration');
    		$url = $config['externalLinks']['phpmyadmin']['url'];
    		header('location: ' . $url);
    		//header('Authorization: Basic c2llcmdhdGU6Y3F4MzhqNldh',false);
    		die;
    	}
    	else{
    		return $this->notauthorized();
    	}
    }

}
