<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'adminuser' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/admin/user/[:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Admin\Controller\User',
						'action' => 'index',
					),
				),
			),
			'database' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/admin/index/database',
					'defaults' => array(
						'controller' => 'Admin\Controller\Index',
						'action' => 'database',
					),
				),
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'admin' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/admin',
					'defaults' => array(
						'__NAMESPACE__' => 'Admin\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Admin\Controller\Index'=>'Admin\Controller\IndexController',
			'Admin\Controller\User'=>'Admin\Controller\UserController',
			),
	),
	'view_manager' => array(
		'template_map' => array(
			'layout/admin'   => __DIR__ . '/../view/layout/layout.phtml',
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
);
