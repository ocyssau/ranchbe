<?php
namespace Docflow\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;

class BatchItemFieldset extends Fieldset implements InputFilterProviderInterface
{

	protected $inputFilter;

	/**
	 */
	public function __construct()
	{
		parent::__construct('itemform');
		$this->setHydrator(new Hydrator(false));
		$this->setObject(new \Docflow\Model\BatchDocflow\Item()); /* because bug https://github.com/zendframework/zendframework/issues/3373 */

		$this->add(array(
			'type' => 'hidden',
			'name' => 'documentId'
		));

		$this->add(array(
			'type' => 'text',
			'name' => 'fullId',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled'
			),
			'options' => array(
				'label' => 'Number'
			)
		));

		$this->add(array(
			'type' => 'text',
			'name' => 'lifeStage',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled'
			),
			'options' => array(
				'label' => 'Life Stage'
			)
		));

		$this->add(array(
			'type' => 'textArea',
			'name' => 'result',
			'attributes' => array(
				'class' => 'form-control',
				'disabled' => 'disabled',
				'rows' => 1,
				'cols' => 50,
			),
			'options' => array(
				'label' => 'Feedbacks'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'documentId' => array(
				'required' => false
			)
		);
	}
}
