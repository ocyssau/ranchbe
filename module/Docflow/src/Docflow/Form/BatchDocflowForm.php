<?php
namespace Docflow\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class BatchDocflowForm extends Form implements InputFilterProviderInterface
{

	protected $inputFilter;

	/**
	 *
	 * @param unknown_type $name
	 */
	public function __construct()
	{
		// we want to ignore the name passed
		parent::__construct('docflowform');

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false));

		$this->add(array(
			'type' => 'Docflow\Form\BatchDocflowFieldset',
			'options' => array(
				'use_as_base_fieldset' => true
			)
		));

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Validate',
				'class' => 'btn btn-success'
			)
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Cancel',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}
}