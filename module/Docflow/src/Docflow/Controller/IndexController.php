<?php
namespace Docflow\Controller;

// Zend
use Application\Controller\AbstractController;

// models and Dao
use Rbs\Space\Factory as DaoFactory;
use Rbs\Dao\Sier\Filter;
use Rbs\Sys\Session;
use Rbplm\Ged\Document;
use Rbplm\Org\Workitem;
use Workflow\Model\Wf;

class IndexController extends AbstractController
{

	public $pageId = 'docflow_index';
	public $ifSuccessForward = '/docflow/index/index';
	public $ifFailedForward = '/docflow/index/index';

	/**
	 */
	public function init()
	{
		parent::init();
		$this->workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);

		$space = null;
		$pageId = $_REQUEST['page_id'];

		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['checked']) ? $this->documentIds = $_REQUEST['checked'] : $this->documentIds = array();
		isset($_REQUEST['documentid']) ? $this->documentIds[] = $_REQUEST['documentid'] : null;
		isset($_REQUEST['cancel']) ? $cancel = $_REQUEST['cancel'] : null;

		$this->session = new Session('changeStateCaddie');

		if ( !$spaceName ) {
			$spaceName = $this->session->spaceName;
		}
		else {
			$this->session->spaceName = $spaceName;
		}

		$this->spaceName = $spaceName;

		/* to retrieve previous selection -- nice. */
		if ( !$this->documentIds && $this->session->caddie ) {
			foreach( $this->session->caddie as $elemt ) {
				$this->documentIds[] = $elemt['documentId'];
			}
		}

		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('document');
		$this->layout()->tabs = $tabs;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$cancel = $this->params()->fromPost('cancel', false);
		$validate = $this->params()->fromPost('validate', false);
		$actTag = $this->params()->fromPost('4dcb6a55582ce745a1826edd6126cc0a', false);

		if ( $cancel ) {
			return $this->successForward();
		}

		/* Inits */
		$view = $this->view;
		$request = $this->getRequest();
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$workflow = $this->workflow->setFactory($factory);

		$documentDao = $factory->getDao(Document\Version::$classId);
		$containerDao = $factory->getDao(Workitem::$classId);
		$activityInstDao = $factory->getDao(Wf\Instance\Activity::$classId);

		/* @var \Docflow\Model\BatchDocflow\Item $item */

		/* Form */
		$form = new \Docflow\Form\BatchDocflowForm();
		/* Model */
		$batch = new \Docflow\Model\BatchDocflow($workflow, $factory);

		/* Create element a recap line in the select manager for each document */
		$documentCaddie = array();
		$validInstance = null;
		$batchItem = null;
		foreach( $this->documentIds as $documentId ) {
			$batchItem = new \Docflow\Model\BatchDocflow\Item($batch);
			$document = new Document\Version();
			$documentDao->loadFromId($document, $documentId);
			$container = new Workitem();
			$containerDao->loadFromId($container, $document->parentId);
			$document->setParent($container);
			$batchItem->setDocument($document);

			if ( $batchItem->validate() ) {
				$batch->add($batchItem);
				$process = $batchItem->getProcess();
				$instance = $batchItem->getInstance();
				$itemCaddie = array(
					'processId' => $process->getId(),
					'documentId' => $documentId
				);
				if ( $instance ) {
					$itemCaddie['instanceId'] = $instance->getId();
				}
				$documentCaddie[] = $itemCaddie;
			}
			else {
				$msg = implode(' - ', $batchItem->result->getErrors());
				$this->errorStack()->error($msg);
			}
		}

		/**/
		if($batch->getProcess()==null){
			return $this->successForward();
			//throw new \Exception('None Process is found, you must set a process to this container');
		}

		$this->session->caddie = $documentCaddie;
		$batchForm = $form->get('batchform');
		$batchForm->setObject($batch);
		$batchForm->get('activityId')->setValueOptions($batchForm->getActivitiesOptions($batch));
		$form->bind($batch);
		$view->form = $form;

		if ( $validate ) {
			if ( $request->isPost() ) {
				$form->setData($request->getPost());
				if ( $form->isValid() ) {
					$activityId = $batch->activityId;
					$workflow = $batch->getDocflow();
					$process = $batch->getProcess();
					$workflow->process = $process;
					$connexion = $factory->getConnexion();
					$this->session->activityId = $activityId;

					foreach( $batch->getItems() as $item ) {
						/* Start transaction */
						$connexion->beginTransaction();
						$document = $item->getDocument();

						$workflow->document = $document;
						$intance = $item->getInstance();

						/* process must be started */
						if ( $intance == null ) {
							try {
								/* start process */
								$startActInst = $workflow->startProcess()->startInstance;

								/* translate to next activities */
								$workflow->translateActivity($startActInst->getId());

								/* Now process instance is set in workflow : */
								$instanceId = $workflow->instance->getId();

								/* create link between process instance and workunit object */
								$documentDao->updateFromArray($document->getId(), array(
									'instanceId' => $instanceId
								));
							}
							catch( \Exception $e ) {
								$connexion->rollBack();
								$msg = sprintf('change state failed for document %s: ', $document->getName());
								$msg .= $e->getMessage();
								$this->errorStack()->error($msg, $r);
								throw $e;
							}
						}
						/* run the activity */
						else {
							$intanceId = $intance->getId();
							$filter = 'obj.' . $activityInstDao->toSys('activityId') . '=:activityId AND obj.' . $activityInstDao->toSys('instanceId') . '=:instanceId';
							$bind = array(
								':activityId' => $activityId,
								':instanceId' => $intanceId
							);
							$actInstance = $activityInstDao->loadWithActivity(new Wf\Instance\Activity(), $filter, $bind);
							$act = $workflow->runActivity($actInstance)->lastActivity;
						}
						$connexion->commit();
					}
					return $this->successForward();
				}
			}
		}
		/* Run after activity form validation */
		elseif( $actTag ){
			$activityId = $this->session->activityId;
			$workflow = $batch->getDocflow();
			$process = $batch->getProcess();
			$workflow->process = $process;
			$connexion = $factory->getConnexion();

			foreach( $batch->getItems() as $item ) {
				/* Start transaction */
				$connexion->beginTransaction();

				$document = $item->getDocument();
				$workflow->document = $document;
				$intance = $item->getInstance();
				$workflow->instance = $intance;

				$intanceId = $intance->getId();
				$filter = 'obj.' . $activityInstDao->toSys('activityId') . '=:activityId AND obj.' . $activityInstDao->toSys('instanceId') . '=:instanceId';
				$bind = array(
					':activityId' => $activityId,
					':instanceId' => $intanceId
				);
				$actInstance = $activityInstDao->loadWithActivity(new Wf\Instance\Activity(), $filter, $bind);
				/* Run activity */
				$act = $workflow->runActivity($actInstance)->lastActivity;
				$connexion->commit();
			}
			return $this->successForward();
		}

		$process = $batch->getProcess();
		$graph = \Docflow\Service\Workflow\Code::getGraphPath($process). '/' . $process->getNormalizedName();
		$graphUrl = str_replace(getcwd().'/public/', '', $graph);

		$view->graphImg = $graphUrl . '.' . \Docflow\Service\Workflow\GraphViz::$imageType;
		$view->graphMap = $graphUrl . '.map';

		return $view;
	}
}
