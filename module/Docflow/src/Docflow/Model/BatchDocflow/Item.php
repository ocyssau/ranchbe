<?php

namespace Docflow\Model\BatchDocflow;

use Workflow\Model\Wf;

class Item
{
	/**
	 * @var Result
	 */
	public $result;

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * @var \Rbplm\Ged\Document\Version
	 */
	protected $document;

	/**
	 * @var \Workflow\Model\Wf\Instance
	 */
	protected $instance;

	/**
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	protected $workflow;

	/**
	 * @param \Docflow\Model\BatchDocflow $batch
	 */
	public function __construct($batch=null)
	{
		$this->result = new Result();
		if($batch){
			$this->batch = $batch;
			$this->factory = $batch->factory;
		}
	}

	/**
	 *
	 */
	public function getArrayCopy()
	{
		return array(
			'documentId'=>$this->document->getId(),
			'uid'=>$this->document->getUid(),
			'lifeStage'=>$this->document->getLifeStage(),
			'name'=>$this->document->getName(),
			'number'=>$this->document->getNumber(),
			'version'=>$this->document->version,
			'iteration'=>$this->document->iteration,
			'owner'=>$this->document->getOwner(true),
			'accessCode'=>$this->document->checkAccess(),
			'fullId' => $this->document->getNumber().' v'.$this->document->version.'.'.$this->document->iteration,
			'feedbacks' => $this->result->getFeedback(),
			'errors' => $this->result->getErrors(),
			'instance' => $this->instance,
			'document' => $this->document
		);
	}

	/**
	 * @return array
	 */
	public function populate($datas)
	{
		foreach($datas as $name=>$value){
			$this->$name = $value;
		}
	}

	/**
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	public function setDocument($document)
	{
		$this->document = $document;
		$documentId = $document->getId();
		$batch = $this->batch;

		$factory = $this->factory;
		$processDao = $factory->getDao(Wf\Process::$classId);
		$instanceDao = $factory->getDao(Wf\Instance::$classId);

		/* get process associated to document */
		$processDefinition = $processDao->getDefaultProcessFromDocumentId($documentId)->current();
		if($processDefinition){
			$process = new Wf\Process();
			$processDao->hydrate($process, $processDefinition);
			$this->setProcess($process);
		}

		/* get instance associated to document */
		$instanceDefinition = $instanceDao->getInstanceFromDocumentId($documentId)->current();
		if($instanceDefinition){
			$instance = Wf\Instance::init();
			$instanceDao->hydrate($instance, $instanceDefinition);
			$runningActivities = $instanceDao->getRunningActivities($instance->getId());
			$instance->setRunningActivities($runningActivities);
			$this->setInstance($instance);
			if(!isset($batch->runningActivities)){
				$batch->runningActivities = $runningActivities;
			}
		}

		/*
		//Display document infos
		$html = '<b><font color='.$fontColor.'>'.$document->getName().'</font></b><br />'.
			'<i>'.$document->description.'</i><br />'.
			$document->getLockName().
			' <b>'.$document->lifeStage.'</b>';
			$this->form->addElement('static', '', $html );


			//Display infos about running instance
			if($this->instance){
				$instance = $this->instance;
				$html =
				'<i>'.tra('Instance').'</i>: '.$instance->getName().'('.$instance->getTitle().')<br />'.
				'<i>'.tra('Created').'</i>: '.$instance->getStarted($dateFormat).'<br />'.
				'<i>'.tra('Status').'</i>: '.$instance->getStatus().'<br />'.
				'<i>'.tra('Owner').'</i>: '.$instance->getOwner(true).'<br />'.
				'<i>'.tra('Next user').'</i>: '.$instance->getNextUsers().'<br />'
					;
			}
			*/

		return $this;
	}

	/**
	 *
	 */
	public function validate()
	{
		$batch = $this->batch;
		$process = $this->process;
		$instance = $this->instance;
		$document = $this->document;
		$previous = $batch->getItem(0);

		/* VALIDATION OF THIS ITEM */
		/* If a workflow instance is running */
		if( $instance ){
			/* Check that activity is identical to previous selected document process : to ensure coherence between selected documents */
			if( $previous &&  $previous->instance){
				$runningActivity = $this->instance->getRunningActivities()[0]['activityId'];
				$previousActivity = $previous->instance->getRunningActivities()[0]['activityId'];
				if($runningActivity != $previousActivity){
					$msg = 'document %s have not same state that %s. it is not selected';
					$e1 = $document->getName();
					$e2 = $previous->getDocument()->getName();
					$this->result->error(sprintf( tra($msg), $e1, $e2));
					return false;
				}
			}
		}
		/* If no instances get the process and display activities */
		else{
			/* Check that this item has access free */
			$accessCode = $document->checkAccess();
			if(!($accessCode == 0 || ( $accessCode > 1 && $accessCode <= 5 ) ) ){
				$this->result->error(tra('This item is not free'));
				return false;
			}

			/* Check if there is a process linked to doctype, container, project */
			if( !$process ){
				$this->result->error(tra('no default process is found'));
				return false;
			}
			else{
				/* check that process is valid */
				if( !$this->process->isValid() ){
					$this->result->error(tra('This process is not valid'));
					return false;
				}
				else{
					/* Check that process is identical to previous document process : to ensure coherence between documents */
					if( $previous && $previous->getProcess()->getId() != $this->process->getId() ){
						$msg = 'document %s is rejected : have not same process that other selected document';
						$e1 = $document->getName();
						$this->result->error(sprintf( tra($msg), $e1));
						return false;
					}
				}
			}
		}

		$batch->setProcess($process);
		return true;
	}

	/**
	 * @param \Workflow\Model\Wf\Process $process
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		return $this;
	}

	/**
	 * A working process instance is running, set to Item
	 *
	 * @param \Workflow\Model\Wf\Instance $instance
	 */
	public function setInstance($instance)
	{
		$this->instance = $instance;
		return $this;
	}

	/**
	 * @return \Rbplm\Ged\Document\Version
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * @return \Workflow\Model\Wf\Process
	 */
	public function getProcess()
	{
		return $this->process;
	}

	/**
	 * @return \Workflow\Model\Wf\Instance
	 */
	public function getInstance()
	{
		return $this->instance;
	}

}
