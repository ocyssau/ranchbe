<?php
namespace Docflow\Model;

/**
 * Factory to create form definition of workflow to run on document.
 */
class BatchDocflow extends Batch
{

	/**
	 * @var string
	 */
	public $spaceName;

	/**
	 * @var string
	 */
	public $containerId;

	/**
	 * @var array
	 */
	public $items = array();

	/**
	 * @var \Rbs\Space\Factory
	 */
	public $factory;

	/**
	 * @var \Docflow\Model\BatchDocflow\Result
	 */
	protected $result;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	protected $workflow;

	/**
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process;

	/**
	 * @var array
	 */
	public $runningActivities;

	/**
	 * @var array
	 */
	public $activityId;

	/**
	 *
	 * @param \Docflow\Service\Workflow $workflow
	 * @param \Rbs\Space\Factory $factory
	 */
	function __construct($workflow, $factory)
	{
		$this->workflow = $workflow;
		$this->factory = $factory;
		$this->result = new BatchDocflow\Result();
		$this->spaceName = $factory->getName();
	}

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		return array(
			'items' => $this->items,
			'process' => $this->process->getNormalizedName(),
			'activityId' => $this->activityId,
			'spaceName' => $this->spaceName
		);
	}

	/**
	 * @param \Docflow\Model\BatchDocflow\Item $item
	 * @return \Docflow\Model\BatchDocflow
	 */
	public function add($item)
	{
		$this->items[] = $item;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @return \Docflow\Model\BatchDocflow\Item
	 * @param integer $key
	 */
	public function getItem($key)
	{
		return $this->items[$key];
	}

	/**
	 * @return \Docflow\Service\Workflow
	 */
	public function getDocflow()
	{
		return $this->workflow;
	}

	/**
	 * @param \Workflow\Model\Wf\Process $process
	 * @return \Docflow\Model\BatchDocflow
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		return $this;
	}

	/**
	 * @return \Workflow\Model\Wf\Process
	 */
	public function getProcess()
	{
		return $this->process;
	}
}

