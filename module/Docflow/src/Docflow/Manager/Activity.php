<?php
namespace Workflow\Manager;

use Workflow\Manager\Base;

/**
 *
 * This class is used to add,remove,modify and list
 * activities used in the Workflow engine.
 * 
 * Activities are managed in a per-process level, each
 * activity belongs to some process.
 */
class Activity
{
	public $error = '';

	/**
	 *
	 * @param PDO $db
	 */
	function __construct($db)
	{
		if(! $db) {
			die( "Invalid db object passed to activityManager constructor" );
		}
		$this->connexion = $db;
	}

	/**
	 *
	 * @return string
	 */
	function getError()
	{
		return $this->error;
	}

	/*!
	 */
	/**
	 * Builds the graph
	 *
	 * @param integer $pId
	 * @return boolean
	 *
	 * @todo build the real graph
	 */
	function buildGraph($pId)
	{
		$attributes = Array();
		$graph = new \Workflow\Manager\GraphViz( true, $attributes );
		$pm = new \Workflow\Manager\Process( $this->db );
		$name = $pm->_getNormalizedName( $pId );
		$graph->set_pid( $name );

		// Nodes are process activities so get
		// the activities and add nodes as needed
		$nodes = $this->getActivities( $pId );

		foreach( $nodes as $node ) {
			if($node ['isInteractive'] == 'y') {
				$color = 'blue';
			} else {
				$color = 'black';
			}
			$auto [$node ['name']] = $node ['isAutoRouted'];
			$graph->addNode( $node ['name'], array('URL' => "foourl?activityId=" . $node ['activityId'], 'label' => $node ['name'], 'shape' => $this->_getActivityShape( $node ['type'] ), 'color' => $color )

			);
		}

		// Now add edges, edges are transitions,
		// get the transitions and add the edges
		$edges = $this->getTransitions( $pId );
		foreach( $edges as $edge ) {
			if($auto [$edge ['actFromName']] == 'y') {
				$color = 'red';
			} else {
				$color = 'black';
			}
			$graph->addEdge( array($edge ['actFromName'] => $edge ['actToName'] ), array('color' => $color ) );
		}

		// Save the map image and the image graph
		$graph->imageAndMap();
		unset( $graph );
		return true;
	}

	/**
	 * Validates if a process can be activated checking the
	 * process activities and transitions the rules are:
	 * 0) No circular activities
	 * 1) Must have only one a start and end activity
	 * 2) End must be reachable from start
	 * 3) Interactive activities must have a role assigned
	 * 4) Roles should be mapped
	 * 5) Standalone activities cannot have transitions
	 * 6) Non intractive activities non-auto routed must have some role
	 * so the user can "send" the activity
	 *
	 * @param integer $pId
	 * @return boolean
	 */
	function validateActivities($pId)
	{
		if(!$pId){
			throw new \Exception('pid is not set');
		}
		
		$transTable = GALAXIA_TABLE_PREFIX.'transitions';
		$activityTable = GALAXIA_TABLE_PREFIX.'activities';
		$transitionTable = GALAXIA_TABLE_PREFIX.'transitions';
		$roleTable = GALAXIA_TABLE_PREFIX.'roles';
		$activityRoleTable = GALAXIA_TABLE_PREFIX.'activity_roles';
		$userRoleTable = GALAXIA_TABLE_PREFIX.'user_roles';
		$processTable = GALAXIA_TABLE_PREFIX.'processes';
		$errors = Array();
		
		// Pre rule no cricular activities
		$cant = $this->connexion->query("SELECT COUNT(*) AS count FROM $transTable WHERE pId=$pId AND actFromId=actToId" )->fetchColumn(0);
		if($cant > 0) {
			$errors [] = tra( 'Circular reference found some activity has a transition leading to itself' );
		}
		
		// Rule 1 must have exactly one start
		$cant = $this->connexion->query("SELECT COUNT(*) AS count FROM $activityTable WHERE pId=$pId AND type='start'" )->fetchColumn(0);
		if($cant < 1) {
			$errors [] = tra( 'Process does not have a start activity' );
		}

		// Rule 2 end must be reachable from start
		$nodes = Array();
		$endId = $this->connexion->query( "SELECT activityId FROM $activityTable WHERE pId=$pId and type='end'" )->fetchColumn(0);
		$aux['id'] = $endId;
		$aux['visited'] = false;
		$nodes[] = $aux;

		$startId = $this->connexion->query( "SELECT activityId FROM $activityTable WHERE pId=$pId and type='start'" )->fetchColumn(0);
		$startNode['id'] = $startId;
		$startNode['visited'] = true;

		while( $this->_listHasUnvisitedNodes($nodes) && ! $this->_nodeInList($startNode, $nodes ) ) {
			for($i = 0; $i < count( $nodes ); $i ++) {
				$node = &$nodes[$i];
				if(! $node ['visited']) {
					$node ['visited'] = true;
					$query = "SELECT actFromId FROM $transitionTable WHERE actToId=" . $node['id'];
					$result = $this->connexion->query( $query );
					$ret = Array();
					while( $res = $result->fetch(\PDO::FETCH_ASSOC) ) {
						$aux ['id'] = $res ['actFromId'];
						$aux ['visited'] = false;
						if(! $this->_nodeInList( $aux, $nodes )) {
							$nodes [] = $aux;
						}
					}
				}
			}
		}

		if(! $this->_nodeInList( $startNode, $nodes )) {
			// Start node is NOT reachable from the end node
			$errors [] = tra( 'End activity is not reachable from start activity' );
		}

		//Rule 3: interactive activities must have a role assigned.
		//Rule 5: standalone activities can't have transitions
		$query = "SELECT * FROM $activityTable WHERE pId = $pId";
		$result = $this->connexion->query( $query );
		while( $res = $result->fetch(\PDO::FETCH_ASSOC) ) {
			$aid = $res['activityId'];
			
			if($res['isInteractive'] == 'y') {
				$cant = $this->connexion->query( "SELECT count(*) FROM $activityRoleTable WHERE activityId=".$res['activityId'] )->fetchColumn(0);
				if(! $cant) {
					$errors [] = tra( 'Activity' ) . ': ' . $res ['name'] . tra( ' is interactive but has no role assigned' );
				}
			} 
			else {
				if($res['type'] != 'end' && $res ['isAutoRouted'] == 'n') {
					$cant = $this->connexion->query( "SELECT count(*) FROM $activityRoleTable WHERE activityId=".$res['activityId'] )->fetchColumn(0);
					if(! $cant) {
						$errors [] = tra( 'Activity' ) . ': ' . $res ['name'] . tra( ' is non-interactive and non-autorouted but has no role assigned' );
					}
				}
			}
			
			if($res['type'] == 'standalone') {
				$cant = $this->connexion->query("SELECT count(*) FROM $transitionTable WHERE actFromId=$aid OR actToId=$aid")->fetchColumn(0);
				if($cant > 0) {
					$errors [] = tra( 'Activity' ) . ': ' . $res ['name'] . tra( ' is standalone but has transitions' );
				}
			}
		}

		//Rule4: roles should be mapped
		$query = "SELECT * FROM $roleTable WHERE pId = $pId";
		$result = $this->connexion->query( $query );
		while( $res = $result->fetch(\PDO::FETCH_ASSOC) ) {
			$cant = $this->connexion->query("SELECT count(*) FROM $userRoleTable WHERE roleId=".$res['roleId'])->fetchColumn(0);
			if($cant==0) {
				$errors [] = tra( 'Role' ) . ': ' . $res ['name'] . tra( ' is not mapped' );
			}
		}
		// End of rules

		// Validate process sources
		//$serrors = $this->validateSources( $pId );
		//$errors = array_merge( $errors, $serrors );

		$this->error = $errors;

		$isValid =(count( $errors ) == 0) ? 'y' : 'n';

		$query = "UPDATE $processTable SET isValid='$isValid' WHERE pId=$pId";
		$this->connexion->exec($query);
		
		$this->_labelNodes( $pId );

		return($isValid == 'y');
	}

	/**
	 * Validate process sources
	 * Rules:
	 * 1) Interactive activities(non-standalone) must use complete()
	 * 2) Standalone activities must not use $instance
	 * 3) Switch activities must use setNextActivity
	 * 4) Non-interactive activities cannot use complete()
	 *
	 * @param integer $pId
	 * @return boolean
	 */
	function validateSources($pid)
	{
		$transTable = GALAXIA_TABLE_PREFIX.'transitions';
		$activityTable = GALAXIA_TABLE_PREFIX.'activities';
		$transitionTable = GALAXIA_TABLE_PREFIX.'transitions';
		$roleTable = GALAXIA_TABLE_PREFIX.'activity_roles';
		$userRoleTable = GALAXIA_TABLE_PREFIX.'user_roles';
		$processTable = GALAXIA_TABLE_PREFIX.'processes';
		$errors = array();
		
		$procname = $this->connexion->query("SELECT normalized_name FROM $processTable WHERE pId=$pid")->fetchColumn(0);

		$query = "SELECT * FROM $activityTable WHERE pId=$pid";
		$result = $this->connexion->query( $query );
		while( $res = $result->fetchRow() ) {
			$actname = $res ['normalized_name'];
			$sourcefile = GALAXIA_PROCESSES . "/$procname/code/activities/$actname" . '.php';
			if(! file_exists( $sourcefile )) {
				continue;
			}
			$data = str_ireplace(array("'", " ", '"'), "", file_get_contents($sourcefile));
			if($res ['type'] == 'standalone') {
				if(strstr( $data, '$instance->(' )) {
					$errors [] = tra( 'Activity ' . $res ['name'] . ' is standalone and is using the $instance object' );
				}
			}
			else {
				if($res ['isInteractive'] == 'y') {
					if(! strstr( $data, '$instance->complete(' )) {
						$errors [] = tra( 'Activity ' . $res ['name'] . ' is interactive so it must use the $instance->complete() method' );
					}
				}
				else {
					if(strstr( $data, '$instance->complete(' )) {
						$errors [] = tra( 'Activity ' . $res ['name'] . ' is non-interactive so it must not use the $instance->complete() method' );
					}
				}
				if($res ['type'] == 'switch') {
					if(! strstr( $data, '$instance->setNextActivity(' )) {
						$errors [] = tra( 'Activity ' . $res ['name'] . ' is switch so it must use $instance->setNextActivity($actname) method' );
					}
				}
			}
		}
		return $errors;
	}

	/**
	 * Compiles activity
	 *
	 * @param $pId
	 * @param $activityId
	 * @return void
	 */
	function compile($pId, $activityId)
	{
		$activity = $this->getActivity( $pId, $activityId );
		$actname = $activity ['normalized_name'];

		$pm = new Process( $this->db );
		$process = $pm->getProcess( $pId );

		$compiledFilePath = GALAXIA_PROCESSES . '/' . $process['normalized_name'] . '/compiled/' . $activity['normalized_name'] . '.php';
		$templateSrcFilePath = GALAXIA_PROCESSES . '/' . $process['normalized_name'] . '/code/templates/' . $actname . '.tpl';
		$templateFilePath = GALAXIA_TEMPLATES . '/' . $process ['normalized_name'] . $actname . '.tpl';
		$userFilePath = GALAXIA_PROCESSES . '/' . $process['normalized_name'] . '/code/activities/' . $actname . '.php';
		$preFilePath = GALAXIA_LIBRARY . '/compiler/' . $activity['type'] . '_pre.php';
		$posFilePath = GALAXIA_LIBRARY . '/compiler/' . $activity['type'] . '_pos.php';
		$posSharedFilePath = GALAXIA_LIBRARY . '/compiler/_shared_pos.php';
		$preSharedFilePath = GALAXIA_LIBRARY . '/compiler/_shared_pre.php';
		$sharedFilePath = GALAXIA_PROCESSES . '/' . $process['normalized_name'] . '/code/shared.php';

		$compiledFile = fopen( $compiledFilePath, "wb" );
		// First of all add an include to to the shared code
		fwrite( $compiledFile, '<' . "?php include_once('$sharedFilePath'); ?" . '>' . "\n" );

		// Before pre shared
		$fp = fopen( $preSharedFilePath, "rb" );
		if(!$fp){
			trigger_error($preSharedFilePath .'is unreachable',  E_USER_ERROR);
			return;
		}
		while( !feof( $fp ) ) {
			$data = fread( $fp, 4096 );
			fwrite( $compiledFile, $data );
		}
		fclose( $fp );


		// Now get pre and pos files for the activity
		$fp = fopen( $preFilePath, "rb" );
		if(!$fp){
			trigger_error($preFilePath .'is unreachable',  E_USER_ERROR);
			return;
		}
		while( ! feof( $fp ) ) {
			$data = fread( $fp, 4096 );
			fwrite( $compiledFile, $data );
		}
		fclose( $fp );

		// Get the user data for the activity
		if(! is_readable( $userFilePath )) {
			// Should create the code file
			$fp = fopen( $userFilePath, 'w' );
			fwrite( $fp, '<' . '?' . 'php' . "\n" . '?' . '>' );
			fclose( $fp );
		}
		$fp = fopen( $userFilePath, "rb" );
		if(!$fp){
			trigger_error($userFilePath .'is unreachable',  E_USER_ERROR);
			return;
		}
		if($fp) {
			while( ! feof( $fp ) ) {
				$data = fread( $fp, 4096 );
				fwrite( $compiledFile, $data );
			}
			fclose( $fp );
		}

		// Get pos and write
		$fp = fopen( $posFilePath, "rb" );
		if(!$fp){
			trigger_error($posFilePath .'is unreachable',  E_USER_ERROR);
			return;
		}
		while( ! feof( $fp ) ) {
			$data = fread( $fp, 4096 );
			fwrite( $compiledFile, $data );
		}
		fclose( $fp );

		// Shared pos
		$fp = fopen( $sharedFilePath, "rb" );
		if(!$fp){
			trigger_error($sharedFilePath .'is unreachable',  E_USER_ERROR);
			return;
		}
		while( ! feof( $fp ) ) {
			$data = fread( $fp, 4096 );
			fwrite( $compiledFile, $data );
		}
		fclose( $fp );

		//Copy the templates
		if($activity['isInteractive'] == 'y' && ! file_exists( $templateSrcFilePath ))
		{
			$fw = fopen( $templateSrcFilePath, 'w' );
			if(defined( 'GALAXIA_TEMPLATE_HEADER' ) && GALAXIA_TEMPLATE_HEADER) {
				fwrite( $fw, GALAXIA_TEMPLATE_HEADER . "\n" );
			}
			fclose( $compiledFile );
		}
		else{
			fclose( $compiledFile );

		}

		if($act_info ['isInteractive'] == 'y' && file_exists( $templateSrcFilePath ))
		{
			copy( $templateSrcFilePath, $templateFilePath );
		}
		else{
			//unlink( $templateSrcFilePath );
			/*
			 if( file_exists($templateFilePath)) {
			unlink( $templateFilePath );
			}
			*/
		}
	}

	/**
	 * Returns the activity shape
	 *
	 * @param unknown_type $type
	 * @return string
	 */
	protected function _getActivityShape($type)
	{
		switch($type) {
			case "start" :
				return "circle";
			case "end" :
				return "doublecircle";
			case "activity" :
				return "box";
			case "split" :
				return "triangle";
			case "switch" :
				return "diamond";
			case "join" :
				return "invtriangle";
			case "standalone" :
				return "hexagon";
			default :
				return "egg";

		}

	}

	/*!
	 \private Returns true if a list contains unvisited nodes
	list members are asoc arrays containing id and visited
	*/
	protected function _listHasUnvisitedNodes($list)
	{
		foreach( $list as $node ) {
			if(! $node ['visited'])
				return true;
		}
		return false;
	}

	/**
	 * Returns true if a node is in a list
	 * list members are asoc arrays containing id and visited
	 */
	protected function _nodeInList($node, $list)
	{
		foreach( $list as $aNode ) {
			if($node['id'] == $aNode ['id'])
				return true;
		}
		return false;
	}

	/**
	 * Labels nodes
	 *
	 * @param integer $pId
	 * @return void
	 */
	protected function _labelNodes($pId)
	{
		///an empty list of nodes starts the process
		$nodes = Array();

		$activityTable = GALAXIA_TABLE_PREFIX . 'activities';
		$transitionTable = GALAXIA_TABLE_PREFIX . 'transitions';
		
		// the end activity id
		$endId = $this->connexion->query( "SELECT activityId FROM $activityTable WHERE pId=$pId and type='end'" )->fetchColumn(0);
		
		// and the number of total nodes(=activities)
		$cant = $this->connexion->query( "SELECT count(*) FROM $activityTable WHERE pId=$pId" )->fetchColumn(0);
		
		$nodes [] = $endId;
		$label = $cant;
		$num = $cant;

		$query = "UPDATE $activityTable SET flowNum=$cant+1 WHERE pId=$pId";
		$this->connexion->exec( $query );

		$seen = array();
		while( count( $nodes ) ) {
			$newnodes = Array();
			foreach( $nodes as $node ) {
				// avoid endless loops
				if(isset( $seen [$node] )){
					continue;
				}
				$seen [$node] = 1;
				if(is_null( $node ))
					continue;
				$query = "UPDATE $activityTable SET flowNum=$num WHERE activityId=$node";
				$this->connexion->exec( $query );
				
				$query = "SELECT actFromId FROM $transitionTable WHERE actToId=" . $node;
				$result = $this->connexion->query( $query );
				$ret = array();
				while( $res = $result->fetch(\PDO::FETCH_ASSOC) ) {
					$newnodes [] = $res ['actFromId'];
				}
			}
			$num --;
			$nodes = Array();
			$nodes = $newnodes;
		}

		$min = $this->connexion->query( "SELECT min(flowNum) FROM $activityTable WHERE pId=$pId" )->fetchColumn(0);
		if($min){
			$query = "UPDATE $activityTable SET flowNum=flowNum-$min WHERE pId=$pId";
			$this->connexion->exec( $query );
		}
	}

	/**
	 * Make ending day - this makes the ending date for an instance
	 * to be done taking the date when it was created an
	 * the expirationTime from the activity
	 *
	 * @param integer $initTime
	 * @param integer $expirationTime
	 */
	public static function makeEndingDate($initTime, $expirationTime)
	{
		if($expirationTime == 0) {
			return 0;
		}
		$years =( int )($expirationTime / 535680);
		$months =( int )(($expirationTime -($years * 53680)) / 44640);
		$days =( int )(($expirationTime -($years * 53680) -($months * 44640)) / 1440);
		$hours =( int )(($expirationTime -($years * 53680) -($months * 44640) -($days * 1440)) / 60);
		$minutes =( int )($expirationTime -($years * 53680) -($months * 44640) -($days * 1440) -($hours * 60));
		$endingDate = $initTime;
		$endingDate = strtotime( "+ $years year", $endingDate );
		$endingDate = strtotime( "+ $months month", $endingDate );
		$endingDate = strtotime( "+ $days day", $endingDate );
		$endingDate = strtotime( "+ $hours hour", $endingDate );
		$endingDate = strtotime( "+ $minutes minute", $endingDate );
		return $endingDate;
	}

}
