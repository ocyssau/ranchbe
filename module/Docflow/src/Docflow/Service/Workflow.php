<?php
namespace Docflow\Service;

use Rbplm\People;
use Rbplm\Signal;
use Workflow\Model\Wf;

/**
 *
 * @author olivier
 *
 */
class Workflow extends \Workflow\Service\Workflow
{

	/**
	 *
	 * @var unknown_type
	 */
	public $serviceManager;

	/**
	 *
	 * @return Workflow
	 */
	public function connect()
	{
		// connect events listner
		Signal::connect($this, static::SIGNAL_RUNACTIVITY_PRE, array(
			$this,
			'onRunActivity'
		));
		Signal::connect($this, static::SIGNAL_DELETEACTIVITY_POST, array(
			$this,
			'onRunActivityPost'
		));
		Signal::connect($this, static::SIGNAL_TRANSLATE_POST, array(
			$this,
			'onTranslateActivity'
		));
		Signal::connect($this, static::SIGNAL_SAVEPROCESS_POST, array(
			'Docflow\Service\Workflow\Code',
			'onSaveProcess'
		));
		Signal::connect($this, static::SIGNAL_DELETEPROCESS_POST, array(
			'Docflow\Service\Workflow\Code',
			'onDeleteProcess'
		));
		Signal::connect($this, static::SIGNAL_SAVEACTIVITY_POST, array(
			'Docflow\Service\Workflow\Activity\Code',
			'onSaveActivity'
		));
		Signal::connect($this, static::SIGNAL_DELETEACTIVITY_POST, array(
			'Docflow\Service\Workflow\Activity\Code',
			'onDeleteActivity'
		));

		return $this;
	}

	/**
	 *
	 * @param Event $e
	 * @param Workflow $workflow
	 */
	public function onRunActivity($e)
	{
		$actInst = $this->lastActivity;

		$instanceOwner = $this->instance->getOwner(true);
		$currentUser = People\CurrentUser::get();
		$currentUserLogin = $currentUser->getLogin();

		/*
		 * if($currentUserLogin != $instanceOwner){
		 * //check role
		 * $roles = $actInst->getRoles();
		 * if($roles){
		 * $acl = $currentUser->acl;
		 * $ok = false;
		 * foreach($roles as $role){
		 * if($currentUserLogin == $role){
		 * $ok = true;
		 * }
		 * if($acl && $acl->getAcl()->hasRole($role)){
		 * if($acl->getAcl()->inheritsRole($currentUserLogin, $role)){
		 * $ok = true;
		 * }
		 * }
		 * }
		 *
		 * if($ok==false){
		 * $e->error = 'Current user is not authorized to execute this activity';
		 * return $e;
		 * }
		 * }
		 * }
		 */

		$attr = $actInst->actAttributes;
		$callbackclass = $attr['callbackclass'];
		$callbackmethod = $attr['callbackmethod'];

		if ( $callbackclass == '' ) {
			$code = new \Docflow\Service\Workflow\Activity\Code($this->instance, $this->lastActivity);
			$callbackclass = $code->getModelClass();
			$callbackmethod = 'trigger';
			$formName = $code->getFormClass();
			$template = $code->getTemplate();
		}

		if ( class_exists($callbackclass, true) ) {
			$model = new $callbackclass($this);

			/* get associated document */
			$model->document = $this->document;

			if ( $actInst->isInteractive() ) {
				if ( class_exists($formName, true) ) {
					$form = new $formName($this);
					return $model->runForm($form, $template);
				}
			}
			else {
				return $model->$callbackmethod();
			}
		}
	}

	public function onRunActivityPost($e)
	{
		$history = \Rbs\Ged\Document\History::init();
		$history->setDao($this->factory->getDao($history::$classId));
		$actionName = '';
		$history->onEvent($e, $this->document, $actionName);
	}

	/**
	 *
	 * @param Event $e
	 */
	public function onTranslateActivity($e)
	{
		if ( isset($this->nextStatus) ) {
			$this->document->lifeStage = $this->nextStatus;
			$this->document->dao->updateFromArray($this->document->getId(), array(
				'lifeStage' => $this->nextStatus
			));
		}
	}

	/**
	 *
	 * @param integer $processId
	 * @param integer $activityId
	 * @return Code @throw Exception
	 */
	public function getCode($processId, $activityId)
	{
		/* Load the process definition */
		if ( !$this->process ) {
			$procDao = $this->factory->getDao(Wf\Process::$classId);
			$this->process = new Wf\Process();
			$procDao->loadFromId($this->process, $processId);
		}

		/* Load the activity definition */
		if ( !$this->activity ) {
			$actDao = $this->factory->getDao(Wf\Activity::$classId);
			$this->activity = new Wf\Activity();
			$actDao->loadFromId($this->activity, $activityId);
		}

		$code = new \Docflow\Service\Workflow\Activity\Code($this->process, $this->activity);
		return $code;
	}
}
