<?php
// %LICENCE_HEADER%
namespace Docflow\Service\Workflow\Activity;

use Workflow\Model\Wf;
use Docflow\Service\Workflow;
use Rbs\Dao\Loader as DaoLoader;
use Exception;
use Zend\Code\Reflection;
use Zend\Code\Generator;

class Code
{

	/**
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 *
	 * @var string
	 */
	protected $templateBasePath;

	/**
	 *
	 * @var string
	 */
	protected $template;

	/**
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 *
	 * @var string
	 */
	protected $modelClassName;

	/**
	 *
	 * @var string
	 */
	protected $modelClassFile;

	/**
	 *
	 * @var string
	 */
	protected $formClassName;

	/**
	 *
	 * @var string
	 */
	protected $formClassFile;

	/**
	 *
	 * @var string
	 */
	protected $templateFile;

	/**
	 *
	 * @var Wf\Activity
	 */
	protected $activity;

	/**
	 *
	 * @var Wf\Process
	 */
	protected $process;

	/**
	 */
	public function __construct(Wf\Process $process, Wf\Activity $activity)
	{
		$this->process = $process;
		$this->activity = $activity;
		$this->basePath = Workflow\Code::getBasePath($process);
		$this->templateBasePath = Workflow\Code::getTemplatePath($process);

		/* to prevent the first characters of class name as number */
		$actPrefix = 'Act';
		$formPrefix = 'Form';

		$normalizedName = str_replace('_', 'v', $activity->getNormalizedName());

		$this->modelClassFile = $this->basePath . '/' . $actPrefix . ucfirst($normalizedName) . '.php';
		$this->formClassFile = $this->basePath . '/' . $formPrefix . ucfirst($normalizedName) . '.php';
		$this->templateFile = $this->templateBasePath . '/' . $normalizedName . '.phtml';

		/* string length of the processPath */
		$strLen = strlen(Workflow\Code::$processPath);

		$modelClassName = substr($this->modelClassFile, $strLen, -4);
		$this->namespace = str_replace('/', '\\', trim(dirname($modelClassName), '/'));
		$this->modelClassName = str_replace('/', '\\', $modelClassName);

		$formClassName = substr($this->formClassFile, $strLen, -4);
		$this->formClassName = str_replace('/', '\\', $formClassName);

		$template = substr($this->templateFile, $strLen, -6);
		$this->template = trim($template, '/');
	}

	/**
	 *
	 * @param Event $event
	 */
	public static function onDeleteActivity($event)
	{
		$activity = $event->sender->activity;
		$process = DaoLoader::get()->loadFromId($activity->getProcess(true), Wf\Process::$classId);
		$code = new self($process, $activity);
		$code->delete();
	}

	/**
	 *
	 * @param Event $event
	 * @param Workflow $workflow
	 * @param Wf\Activity $activity
	 */
	public static function onSaveActivity($event, $workflow, $activity)
	{
		try {
			$process = $event->sender->process;
			$code = new self($process, $activity);
			$code->generateFormCode();
			$code->generateModelCode();
			$event->code = $code;
		}
		catch( \Exception $e ) {
			$event->error = $e->getMessage();
			throw $e;
		}

		return $code;
	}

	/**
	 */
	public function delete()
	{
		if ( is_file($this->modelClassFile) ) {
			unlink($this->modelClassFile);
		}
		if ( is_file($this->formClassFile) ) {
			unlink($this->formClassFile);
		}
		if ( is_file($this->templateFile) ) {
			unlink($this->templateFile);
		}
	}

	/**
	 */
	public function generateModelCode()
	{
		$classFile = $this->modelClassFile;

		if ( !is_file($classFile) ) {
			$activityName = $this->activity->getNormalizedName();
			$processName = str_replace('_', 'v', $this->process->getNormalizedName());
			$currentUserName = '';
			$namespace = $this->namespace;

			$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Docflow\Service\Workflow\Prototype\ActivityModel'));

			$docblock = new Generator\DocBlockGenerator();
			$docblock->setShortDescription('A class for activity ' . $activityName);
			$date = new \DateTime();

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('package')
				->setDescription($processName));

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('generated')
				->setDescription($date->format(\DateTime::ISO8601)));

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('author')
				->setDescription($currentUserName));

			$className = explode('\\', $this->modelClassName);
			$class->setName(end($className))
				->setExtendedClass('ActivityTrigger')
				->addUse('Docflow\Service\Workflow\Prototype\ActivityTrigger')
				->setDocblock($docblock)
				->setNamespaceName($namespace);

			$file = new Generator\FileGenerator();

			file_put_contents($classFile, $file->setClass($class)->generate());
			chmod($classFile, 0777);
		}
	}

	/**
	 */
	public function generateFormCode()
	{
		$classFile = $this->formClassFile;

		if ( !is_file($classFile) ) {
			$baseDir = dirname($classFile);
			if ( !is_dir($baseDir) ) {
				throw new Exception("Directory $baseDir is not existing");
			}

			$activityName = $this->activity->getNormalizedName();
			$processName = str_replace('_', 'v', $this->process->getNormalizedName());
			$currentUserName = '';
			$namespace = $this->namespace;

			$class = Generator\ClassGenerator::fromReflection(new Reflection\ClassReflection('Docflow\Service\Workflow\Prototype\ActivityFormPrototype'));
			$docblock = new Generator\DocBlockGenerator();
			$docblock->setShortDescription('A class for activity form ' . $activityName);
			$date = new \DateTime();

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('package')
				->setDescription($processName));

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('generated')
				->setDescription($date->format(\DateTime::ISO8601)));

			$tag = new Generator\DocBlock\Tag();
			$docblock->setTag($tag->setName('author')
				->setDescription($currentUserName));

			$className = explode('\\', $this->formClassName);
			$class->setName(end($className))
				->setDocblock($docblock)
				->setExtendedClass('ActivityForm')
				->addUse('Docflow\Service\Workflow\Prototype\ActivityForm')
				->setNamespaceName($namespace);

			$file = new Generator\FileGenerator();

			$ok = file_put_contents($classFile, $file->setClass($class)->generate());
			if ( !$ok ) {
				throw new Exception(sprintf('Unable to write in file %s'), $classFile);
			}
			chmod($classFile, 0777);
		}

		$templateFile = $this->templateFile;
		if ( !is_file($templateFile) ) {
			$baseDir = dirname($templateFile);
			if ( !is_dir($baseDir) ) {
				throw new Exception("Directory $baseDir is not existing");
			}
			copy('module/Docflow/src/Docflow/Service/Workflow/Prototype/ActivityView.phtml', $templateFile);
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getModelFile()
	{
		return $this->modelClassFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getModelClass()
	{
		return $this->modelClassName;
	}

	/**
	 *
	 * @return string
	 */
	public function getFormFile()
	{
		return $this->formClassFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getFormClass()
	{
		return $this->formClassName;
	}

	/**
	 *
	 * @return string
	 */
	public function getTemplateFile()
	{
		return $this->templateFile;
	}

	/**
	 *
	 * @return string
	 */
	public function getTemplate()
	{
		return $this->template;
	}
}
