<?php

namespace Docflow\Service\Workflow\Prototype;

use Zend\View\Model\ViewModel;

class ActivityTrigger
{
	/**
	 *
	 * @var \Docflow\Service\Workflow
	 */
	public $workflow;

	/**
	 *
	 * @var \Zend\Mvc\Controller
	 */
	public $controller;

	/**
	 *
	 * @var \Zend\View\Model\ViewModel
	 */
	public $view;

	/**
	 *
	 * @var \Docflow\Service\Workflow\Prototype\ActivityForm
	 */
	public $form;

	/**
	 *
	 * @param unknown_type $workflow
	 * @param unknown_type $controller
	 */
	public function __construct($workflow)
	{
		$this->workflow = $workflow;
		$this->serviceManager = $workflow->serviceManager;
	}

	/**
	 *
	 * @param unknown_type $view
	 */
	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 *
	 */
	public function trigger()
	{
		/* @var \Rbplm\Ged\Document\Version $document */
		$document = $this->document;
	}

	/**
	 *
	 * @param unknown_type $form
	 */
	public function runForm($form, $template)
	{
		$view = new ViewModel();
		/* @var \Zend\Http\PhpEnvironment\Request $request */
		$request = $this->serviceManager->get('request');
		$validate = $request->getPost('4dcb6a55582ce745a1826edd6126cc0a', false);

		$this->form = $form;
		$form->bind($this);

		if ($request->isPost() && $validate){
			$form->setData( $request->getPost() );
			if ( $form->isValid() ){
				$nextTransition = $request->getPost('next');
				$comment = $request->getPost('comment');
				$this->workflow->setNextTransition($nextTransition);
				$this->workflow->lastActivity->setComment($comment);

				$this->trigger();
				return true;
			}
		}

		$view->setTemplate($template);
		$view->title = 'Select Next For Switch Activity';
		$view->form = $form;
		$this->render($view);
	}

	/**
	 *
	 */
	public function render($view)
	{
		$viewRender = $this->workflow->serviceManager->get('ViewRenderer');
		$layout = new ViewModel();
		$layout->setTemplate('layout/ranchbe');
		$layout->setVariable('content', $viewRender->render($view));

		echo $viewRender->render($layout);
		die;
	}

	/**
	 * Implement arrayObject interface
	 * @param array $properties
	 */
	public function getArrayCopy()
	{
		$return=array();
		foreach($this as $name=>$value){
			if($value instanceof DateTime){
				$return[$name] = $value->format('d-m-Y');
			}
			elseif(is_scalar($value)){
				$return[$name] = $value;
			}
		}
		return $return;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return AbstractComponent
	 */
	public function populate( array $properties )
	{
		foreach($properties as $name=>$value){
			$this->$name = $value;
		}
	} //End of function

}
