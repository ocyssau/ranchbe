<?php
namespace Docflow\Service\Workflow\Prototype;

use Zend\Form\Form;
use Workflow\Model\Wf;
use Exception;

class ActivityForm extends Form
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	public $workflow;

	/**
	 *
	 * @param \Docflow\Service\Workflow $workflow
	 */
	public function __construct($workflow)
	{
		/* we want to ignore the name passed */
		parent::__construct('activity-act1');
		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;

		$this->add(array(
			'name' => '4dcb6a55582ce745a1826edd6126cc0a',
			'attributes' => array(
				'type' => 'hidden',
				'value' => '4dcb6a55582ce745a1826edd6126cc0a'
			)
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		$this->add(array(
			'name' => 'next',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Next Status',
				'value_options' => $this->_getNext(),
				'empty_option' => 'select next status'
			)
		));

		$this->add(array(
			'name' => 'comment',
			'attributes' => array(
				'type' => 'textarea',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Comments'
			)
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
	}

	/**
	 */
	protected function _getNext()
	{
		$factory = $this->workflow->getFactory();

		$ret = array();
		$actInstId = $this->workflow->lastActivity->getId();
		$procInstDao = $factory->getDao(Wf\Instance::$classId);
		$nexts = $procInstDao->getNextCandidates($actInstId);

		foreach( $nexts as $next ) {
			$nextStatus = $next['nextStatus'];
			if ( $nextStatus == null ) {
				throw (new Exception('The status of next transitions must be set!'));
			}
			$ret[$nextStatus] = $nextStatus;
		}

		return $ret;
	}
}
