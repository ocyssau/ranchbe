<?php
// %LICENCE_HEADER%
namespace Docflow\Service\Workflow;

use Workflow\Model\Wf;
use Docflow\Service\Workflow\Shape;
use Exception;
use Rbplm\Sys\Error;
use Rbs\Space\Factory as DaoFactory;

class Code
{

	/**
	 * Base path where to create folders for store acitivities scripts
	 *
	 * @var String
	 */
	public static $processPath = '';

	/**
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 *
	 * @var string
	 */
	protected $templateBasePath;

	/**
	 *
	 * @var Wf\Activity
	 */
	protected $activity;

	/**
	 *
	 * @var Wf\Process
	 */
	protected $process;

	/**
	 *
	 * @var string
	 */
	protected $modelClassName;

	/**
	 *
	 * @var string
	 */
	protected $modelClassFile;

	/**
	 *
	 * @var string
	 */
	protected $formClassName;

	/**
	 *
	 * @var string
	 */
	protected $formClassFile;

	/**
	 *
	 * @var string
	 */
	protected $templateFile;

	/**
	 */
	public function __construct(Wf\Process $process)
	{
		$this->process = $process;
		$this->basePath = self::getBasePath($process);
		$this->templateBasePath = self::getTemplatePath($process);
		$this->graphBasePath = self::getGraphPath($process);

		$this->folders = array(
			'form' => $this->basePath . '',
			'model' => $this->basePath . '',
			'view' => $this->templateBasePath,
			'graph' => $this->graphBasePath
		);
	}

	/**
	 * Trigger
	 *
	 * @param Event $process
	 * @param Process $process
	 */
	public static function onSaveProcess($event, $worflow)
	{
		try {
			$process = $event->sender->process;
			$code = new self($process);
			$code->init();
			$code->saveGraph();
		}
		catch( \Exception $e ) {
			$event->error = $e->getMessage();
		}
	}

	/**
	 * Trigger
	 *
	 * @param Wf\Process $process
	 */
	public static function onDeleteProcess($event, $worflow)
	{
		$process = $event->sender->process;
		$code = new self($process);
		$code->delete();
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getBasePath(Wf\Process $process)
	{
		$processFolderName = 'PROC' . str_replace('_', 'v', $process->getNormalizedName());
		$processPath = self::$processPath; /* where process definition is store */
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/' . $processFolderName;
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getTemplatePath(Wf\Process $process)
	{
		$processFolderName = 'PROC' . str_replace('_', 'v', $process->getNormalizedName());
		$processPath = self::$processPath; // where process definition is store
		if ( $processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}
		return $processPath . '/view/' . $processFolderName;
	}

	/**
	 *
	 * @param Wf\Process $process
	 * @return string
	 */
	public static function getGraphPath(Wf\Process $process)
	{
		$processFolderName = $process->getNormalizedName();

		/* put img in public folder */
		/* anti collision name */
		$publicImg = realpath('public/img/Graph568ce3c4d4590');
		if ( $publicImg == "" ) {
			throw new Exception('public/img/Graph568ce3c4d4590 is not existing :', Error::WARNING);
		}

		return $publicImg . '/' . $processFolderName;
	}

	/**
	 * Create directories to store the classes of the activities triggers.
	 */
	public function init()
	{
		if ( self::$processPath == "" ) {
			throw new Exception('$processPath is not set :', Error::WARNING);
		}

		foreach( $this->folders as $path ) {
			if ( is_dir($path) ) {
				continue;
			}
			if ( !mkdir($path, 0777, true) ) {
				throw new Exception('DIRECTORY_CREATION_FAILED :' . $path, Error::WARNING);
			}
		}
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string $type
	 * @return array | string
	 */
	public function delete()
	{
		foreach( $this->folders as $folder ) {
			/* Prevent a disaster */
			if ( trim($folder) == '/' || trim($folder) == '.' || trim($folder) == 'templates' || trim($folder) == 'templates/' ) {
				return false;
			}
			\Rbplm\Sys\Directory::removeDir($folder, true);
		}
		return true;
	}

	/**
	 * Get the folders where store process files definition.
	 * $type maybe null, compiledCode, sourceCode, compiledTemplate, sourceTemplate, shared.
	 * If $type is null or false, return array with all folders
	 *
	 * @param string $type
	 * @return array | string
	 */
	public function saveGraph()
	{
		$factory = DaoFactory::get();
		$process = $this->process;
		$processId = $process->getId();
		$graphViz = new \Docflow\Service\Workflow\GraphViz();
		$dao = $factory->getDao($process);
		$actDao = $factory->getDao(Wf\Activity::$classId);

		/* Set name of graph */
		$graphViz->setPid($process->getNormalizedName());

		$interactiveFontColor = 'pink';
		$notinteractiveFontColor = 'black';
		$automaticColor = 'blue';
		$notautomaticColor = 'purple';
		$nodebgColor = 'white';
		$nodeEdgesColor = 'blue';

		$dao = $factory->getDao(Wf\Process::$classId);
		$filter = $actDao->toSys('processId') . '=:processId';
		$bind = array(
			':processId' => $processId
		);
		$list = $dao->getGraphWithTransitions($filter, $bind);

		foreach( $list as $activity ) {
			if ( $activity['isInteractive'] ) {
				$fontcolor = $interactiveFontColor;
			}
			else {
				$fontcolor = $notinteractiveFontColor;
			}

			$nodeId = $activity['uid'];
			$label = $activity['name'];
			$label = $activity['title'];
			$shape = Shape::getShapeFromType($activity['type']);
			$url = '#';
			$graphViz->addNode($nodeId, array(
				'URL' => $url,
				'label' => $label,
				'shape' => $shape,
				'fontcolor' => $fontcolor,
				'style' => 'filled',
				'fillcolor' => $nodebgColor,
				'color' => $nodeEdgesColor
			));

			$nodeParentId = $activity['parentUid'];
			if ( $nodeParentId ) {
				if ( $activity['isAutomatic'] ) {
					$transitionColor = $automaticColor;
				}
				else {
					$transitionColor = $notautomaticColor;
				}

				$graphViz->addEdge(array(
					$nodeParentId => $nodeId
				), array(
					'color' => $transitionColor,
					'taillabel' => $activity['status'],
					'decorate' => 1,
					'arrowhead' => 'open',
					'arrowtail' => 'none',
					'dir' => 'both'
				));
			}
		}

		$toDirectory = $this->folders['graph'];
		$graphViz->imageAndMap($toDirectory, $graphViz::$imageType);

		return true;
	}
}
