<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'custom' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Custom\Controller\Index',
						'action' => 'index',
					),
				),
			),
			'airbusmanual' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom/airbusmanual[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Custom\Controller\Airbusmanual\Index',
						'action' => 'index',
					),
				),
			),
			'am' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom/aidememoire[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Custom\Controller\Docsier\Aidememoire',
						'action' => 'index',
					),
				),
			),
			'docsier' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom/docsier[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Custom\Controller\Docsier\Index',
						'action' => 'index',
					),
				),
			),
			'rt' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/custom/rt[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Custom\Controller\Rt\Index',
						'action' => 'index',
					),
				),
			),
		), //routes
	), //router
	'controllers' => array(
		'invokables' => array(
			'Custom\Controller\Index' => 'Custom\Controller\IndexController',
			'Custom\Controller\Airbusmanual\Index' => 'Custom\Controller\Airbusmanual\IndexController',
			'Custom\Controller\Docsier\Aidememoire' => 'Custom\Controller\Docsier\AidememoireController',
			'Custom\Controller\Docsier\Index' => 'Custom\Controller\Docsier\IndexController',
			'Custom\Controller\Rt\Index' => 'Custom\Controller\Rt\IndexController',
		),
	), //controller
	'view_manager' => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	), //view_manager
);
