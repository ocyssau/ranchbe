<?php
namespace Form\Rt;

use Ranchbe;
use HTML_QuickForm;

class ApplyToDocumentForm extends HTML_QuickForm
{
	
	/**
	 * 
	 * Enter description here ...
	 * @var document
	 */
	public $document;
	
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Visualiser les refus d\'un document');
		
		$lsSelectOptions = array('elementId' => 'documentls', //element id, name must be same that method in .class.php
                          'printStyle' => 0, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
                          'autoComplete' => 0, //if 0 the autocomplete attribute will not be set. Default not set;
                          'autoserverPath' => 'DocumentLiveSearchServer.php?q=%string%&inputid=documentls', //path to server scripts. Special word %string% indicate the input string parameter to search.
		);
		$this->addElement('livesearch_select', 'document', 'Document', $lsSelectOptions, array('size' => 32) );
		
		$this->addElement('hidden', 'space', 'workitem');
		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('submit', 'next', 'Next >>');
	}

	/**
	 * @return ADORecordSet
	 */
	function process()
	{
		$Rt = new Rt();
		$document_id = $this->getSubmitValue('document');
		$space_name = $this->getSubmitValue('space');
		
		$space = new space($space_name);
		$this->document = new document($space, $document_id);
		
		$select = array('DISTINCT rt_id, rt_number, rt_status, rt_access_code, rt_open_date, from_number, from_version');
		$filter = 'rt_access_code < 15';
		return $Rt->GetRtApplyToDocument($this->document, false, true, $select, $filter);
	} // end func process
}
