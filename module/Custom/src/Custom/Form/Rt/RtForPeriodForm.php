<?php
namespace Form\Rt;

use Ranchbe;
use HTML_QuickForm;

class RtForPeriodForm extends HTML_QuickForm
{
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Visualiser les refus sur la p�riode');

		$date_params = array(
		            'field_name' => 'from_date',
		            'default_value' => time() - ( 3600 * 24 * 31 ),
		            'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$date_params = array(
		            'field_name' => 'to_date',
		            'default_value' => time(),
		            'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('submit', 'next', 'Next >>');
	}

	/**
	 * @return ADORecordSet
	 */
	function process()
	{
		$Rt = new Rt();
		$fromDate = $this->getSubmitValue('from_date');
		$toDate = $this->getSubmitValue('to_date');
		
		//Re-set value for display correct date in freeze form
		$this->setConstants(array('from_date' => $fromDate ));
		$this->setConstants(array('to_date' => $toDate ));
		
		$select = array('DISTINCT rt_id, rt_number, rt_status, rt_access_code, rt_open_date, from_number, from_version');
		$filter = 'rt_access_code < 15';
		return $Rt->GetRtForPeriod($fromDate, $toDate, false, true, $select, $filter);
	} // end func process
}
