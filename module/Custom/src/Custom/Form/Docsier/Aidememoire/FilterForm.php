<?php
namespace Custom\Form\Docsier\Aidememoire;

use Rbplm\Dao\Filter\Op;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Application\Form\AbstractFilterForm;
use Rbplm\Ged\Document;

class FilterForm extends AbstractFilterForm
{
	/**
	 *
	 */
	public function __construct($factory=null, $name=null, $containerId)
	{
		parent::__construct($factory, $name);

		$this->template = 'custom/docsier/aidememoire/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$elemtFactory = $this->getElementFactory();

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Designation',
				'class'=>'form-control',
				'data-where'=>'description',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false,
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input here',
				'class'=>'form-control',
				'data-where'=>'number',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false,
		));

		/* Category */
		$elemtFactory->selectCategory(array(
			'containerId'=>$containerId,
			'name'=>'find_category',
			'label'=>'categorie',
			'size'=>1,
			'returnName'=>false,
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		//NUMBER
		if($datas['find_number']){
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::CONTAINS);
		}

		//DESIGNATION
		if($datas['find_designation']){
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::CONTAINS);
		}

		//CATEGORY
		if($datas['find_category']){
			$filter->andFind($datas['find_category'], $dao->toSys('categoryId'), Op::CONTAINS);
		}

		return $this;
	}

}
