<?php
namespace Custom\Form\Docsier\Index;

use Rbplm\Dao\Filter\Op;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Application\Form\AbstractFilterForm;
use Rbplm\Ged\Document;

class FilterForm extends AbstractFilterForm
{
	/**
	 *
	 */
	public function __construct($factory=null, $name=null, $containerId)
	{
		parent::__construct($factory, $name);

		$this->template = 'custom/docsier/index/filterform.phtml';
		$inputFilter = $this->getInputFilter();
		$elemtFactory = $this->getElementFactory();

		/* Quick search */
		$this->add(array(
			'name' => 'quick_search',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class'=>'form-control',
			),
			'options' => array(
				'label' => 'Recherche Rapide',
			),
		));
		$inputFilter->add(array(
			'name' => 'quick_search',
			'required' => false,
		));

		/* KeyWord */
		$this->add(array(
			'name' => 'docsier_keyword',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class'=>'form-control',
			),
			'options' => array(
				'label' => 'Mot clé',
			),
		));
		$inputFilter->add(array(
			'name' => 'docsier_keyword',
			'required' => false,
		));

		/* Construct select field for docsier_fournisseur */
		$elemtFactory->selectFromDb(array(
			'name'=>'docsier_fournisseur',
			'label'=>'Fournisseur',
			'size'=>1,
			'returnName'=>false,
			'dbFieldForName'=>'partner_number',
			'dbFieldForValue'=>'partner_id',
			'dbtable'=>'partners',
			'dbfilter'=>'',
		));
		$inputFilter->add(array(
			'name' => 'docsier_fournisseur',
			'required' => false,
		));

		/* Construct select field for docsier_famille */
		$elemtFactory->selectFromDb(array(
			'name'=>'docsier_famille',
			'label'=>'Famille',
			'size'=>1,
			'returnName'=>false,
			'dbFieldForName'=>'partner_number',
			'dbFieldForValue'=>'partner_id',
			'dbtable'=>'partners',
			'dbfilter'=>'',
		));
		$inputFilter->add(array(
			'name' => 'docsier_famille',
			'required' => false,
		));

		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$this->prepare()->isValid();
		$datas = $this->getData();

		//QUICK SEARCH
		if($datas['quick_search']){
			$filter->andFind($datas['quick_search'], $dao->toSys('number'), Op::CONTAINS);
		}

		//KEYWORD
		if($datas['docsier_keyword']){
			$filter->andFind($datas['docsier_keyword'], $dao->toSys('description'), Op::CONTAINS);
		}

		//SUPPLIER
		if($datas['docsier_fournisseur']){
			$filter->andFind($datas['docsier_fournisseur'], $dao->toSys('categoryId'), Op::CONTAINS);
		}

		//FAMILLE
		if($datas['docsier_famille']){
			$filter->andFind($datas['docsier_famille'], $dao->toSys('categoryId'), Op::CONTAINS);
		}

		return $this;
	}

}
