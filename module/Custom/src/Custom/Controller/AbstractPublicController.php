<?php
namespace Custom\Controller;

use Application\Controller\AbstractController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

/**
 *
 *
 */
class AbstractPublicController extends AbstractController
{
	/**
	 * Squeeze access control
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		if ( $request->isXmlHttpRequest() ) {
			$this->initAjax();
		}
		else{
			$this->init();
		}

		return AbstractActionController::dispatch($request, $respons);
	}

	/**
	 * Download file
	 */
	public function viewdocumentAction()
	{
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : $documentId = null;

		$document = new Document\Version();
		$factory = $this->factory;
		$factory->getDao($document)->loadFromId($document, $documentId);

		$viewer = new \Rbs\Ged\Document\Viewer();
		$viewer->initFromDocument($document);
		return $viewer->push();
	}
}
