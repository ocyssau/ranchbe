<?php
namespace Custom\Controller\Rt;

use Application\Controller\AbstractController;

require_once 'class/Rt.php';
require_once 'class/RtCode.php';

use Custom\Rt;

class CodeController extends AbstractController
{
	public $pageId = 'rt_code';
	public $defaultSuccessForward = 'custom/rt/code/display';
	public $defaultFailedForward = 'custom/rt/code/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$pageId = $_REQUEST['page_id'];
		$this->areaId = 1;

		$this->rt = new Rt(); //Create new manager

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('home')->activate();

		$this->view->icons_dir = DEFAULT_DOCTYPES_ICONS_DIR;
	}

	/**
	 *
	 */
	public function displayAction()
	{
		//Process the code for pagination and display management
		$filterForm = new \Form\Filter\StandardSimple($this->view, $this->pageId);
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$this->bindSortOrder($filter, 'code');
		$paginator = new \Form\Application\PaginatorForm($this->view, $this->pageId, '#filterf');
		$filterForm->bind($filter)->save();
		$currentSpace = 'RtCodes';

		//Display the users list of system
		$params = array (
				'orders' => array($filter->getSort() => $filter->getOrder()),
				'limit' => $filter->getLimit(),
				'offset' => $filter->getOffset(),
				'select'=>array('code', 'type', 'description'),
		);
		$list = $Manager->GetAll($params);
		$smarty->assign_by_ref('list', $list);

		$imgBaseUrl = $smarty->get_template_vars('baseImgUrl');

		//Define select option for "find"
		$allField = array (
				'code' => 'Number',
				'type' => 'Type',
				'description' => 'Description'
				);
		$smarty->assign('all_field', $allField);

		if( $list ){
			$smarty->assign( 'headers', array_keys($list[0]) );
			$smarty->assign( 'pkeyName', 'code' );
		}

		$actions = array(
				'<button class="mult_submit" type="submit" name="action" value="create" title="Cr�er" onclick="pop_it(checkform, 340 , 550)">Cr�er</button>',
				'<button class="mult_submit" type="submit" name="action" value="suppress" title="Supprimer" onclick="document.checkform.action=\'RtCodes.php\'; pop_no(checkform)"><img class="icon" src="'.$imgBaseUrl.'/icons/trash.png" title="Supprimer" width="16" height="16" /></button>',
				'<button class="mult_submit" type="submit" name="action" value="edit" title="Editer" onclick="document.checkform.action=\'RtCodes.php\'; pop_it(checkform, 340 , 550)"><img class="icon" src="'.$imgBaseUrl.'/icons/edit.png" title="Editer" width="16" height="16" /></button>',
		);
		$smarty->assign('actions', implode('', $actions));

		$smarty->assign( 'list', $list );
		$smarty->assign( 'pageTitle', 'Codes des Refus A350' );
		$smarty->assign( 'mid', 'GenericList.tpl' );
		$smarty->display('layouts/ranchbe.tpl');
	}

	/**
	 *
	 */
	public function suppressAction()
	{
		//Confirmation
		if($_REQUEST['confirm'] == 'ok'){
			if (!empty($_REQUEST['checked'])) {
				foreach ($_REQUEST['checked'] as $id) {
					$RtCode = new RtCode($id);
					$RtCode->Suppress();
				}
			}
			RtCodes_action_display($Rt, $smarty);
		}
		else{
			$list = array();
			foreach($_REQUEST['checked'] as $id){
				$list[] = array('checked'=>$id);
			}
			$smarty->assign('title', 'Confirmation de suppression de CODEs');
			$smarty->assign('msg', 'Etes vous sur de vouloir supprimer ces codes?');
			$smarty->assign('confirmation_redirect', 'RtCodes.php');
			$smarty->assign('confirmation_return', 'confirm');
			$smarty->assign('action', 'suppress');
			$smarty->assign('list', $list);
			$smarty->assign('mid', 'confirmation.tpl');
			$smarty->display('layouts/layout1.tpl');
		}
	}

	/**
	 *
	 */
	public function createAction()
	{
		$form = RtCodes_getCUform();

		// Try to validate the form
		if ($_REQUEST['submit'] && $form->validate()) {
			$form->freeze(); //and freeze it
			$form->process('RtCodes_formprocess_create', true);
		} //End of validate form

		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('layouts/layout1.tpl');
	}


	/**
	 *
	 */
	public function editAction()
	{
		$id = $_REQUEST['checked'][0];
		$Code = new RtCode($id);
		$desc = $Code->getDescription();
		$code = $Code->getCode();

		$form = RtCodes_getCUform();
		$form->setDefaults(array(
				'description'=>$Code->getDescription(),
				'code'=>$Code->getCode(),
				'type'=>$Code->getType()
		));

		// Try to validate the form
		if ($_REQUEST['submit'] && $form->validate()) {
			$form->freeze(); //and freeze it
			$form->process('RtCodes_formprocess_edit', true);
		} //End of validate form

		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('layouts/layout1.tpl');
	}

	/**
	 * @return \Controller\Rt\HTML_QuickForm
	 */
	protected function getCUform()
	{
		$form = new HTML_QuickForm('createform', 'post');
		$form->addElement('static', 'title', 'Cr�er un nouveau code:');
		$form->addElement('text', 'code', tra('Code'));
		$form->addElement('textarea', 'description', tra('Description'), array('size'=>60));
		$select = $form->addElement('select', 'type', array('type', 'note'=>'' ), array('B'=>'Bloquant' ,'I'=>'Informatif') );
		$select->setSize(1);
		$select->setMultiple(false);

		$form->addElement('hidden', 'action', 'create');
		$form->addElement('submit', 'submit', 'Save');
		$form->addRule('code', 'Le code est requis', 'required');
		$form->addRule('description', 'La description est requise', 'required');
		return $form;
	}

	/**
	 *
	 * @param array $values
	 */
	function create($values)
	{
		$RtCode = new RtCode();
		$RtCode->setDescription($values['description']);
		$RtCode->setCode($values['code']);
		$RtCode->setType($values['type']);
		$RtCode->Save();
	}

	/**
	 *
	 * @param array $values
	 */
	function edit($values)
	{
		$RtCode = new RtCode($values['code']);
		$RtCode->setDescription($values['description']);
		$RtCode->setCode($values['code']);
		$RtCode->setType($values['type']);
		$RtCode->Save();
	}

}

