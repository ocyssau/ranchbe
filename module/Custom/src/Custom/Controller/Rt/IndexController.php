<?php
namespace Controller\Rt;

class IndexController extends \Controller\Controller
{
	public $pageId = 'rt_index';
	public $defaultSuccessForward = 'rt/index/display';
	public $defaultFailedForward = 'rt/index/display';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$pageId = $_REQUEST['page_id'];
		$this->areaId = 1;

		$this->checkFlood = \check_flood($pageId);

		// Record url for page and Active the tab
		\View\Helper\MainTabBar::get($this->view)->getTab('myspace')->activate('container');

		$this->view->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
	}

	/**
	 *
	 */
	public function displayAction()
	{
		$this->view->assign('mid', 'rt/display.tpl');
		$this->view->display($this->layout);
	}

	/**
	 *
	 */
	function getrtforperiodAction()
	{
		$form = new \Custom\Form\Rt\RtForPeriodForm();
		$form->buildForm();
		$list=array();

		// Try to validate the form
		if ( !$form->validate() ) {
			ob_start();
			$form->display();
			$htmlForm = ob_get_clean();
			$smarty->assign('midcontent', $htmlForm);
			$smarty->display('layouts/ranchbe.tpl');
			return;
		}

		$rs = $form->process();

		if($rs != false){
			$i = 0;
			$list = array();
			while( $row = $rs->FetchRow() ){
				$list[$i] = $row;
				$list[$i]['rt_number'] = '<a href="{$baseurl}/application/index/viewfile?document_id='.$row['rt_id'].'&space=workitem">' . $list[$i]['rt_number'] . '</a>';
				$list[$i]['rt_open_date'] = formatDate($row['rt_open_date']);
				$list[$i]['apply_to'] = $row['from_number'] . '.v' . $row['from_version'];
				unset($list[$i]['applyto_number'], $list[$i]['applyto_indice'], $list[$i]['applyto_version'], $list[$i]['applyto_id']);
				$i++;
			}
			$smarty->assign( 'headers', array_keys($list[0]) );
		}

		$smarty->assign( 'list', $list );
		$smarty->assign( 'pageTitle', 'Refus sur la periode' );
		$smarty->assign( 'mid', 'custom/rtmanager/getrtforperiod.tpl' );
		$smarty->display('layouts/ranchbe.tpl');
	}

	/**
	 *
	 */
	function getrtapplytodocumentAction()
	{
		$form = new \Custom\Form\Rt\ApplyToDocumentForm();
		$form->buildForm();
		$list = array();

		// Try to validate the form
		if ( !$form->validate() ) {
			ob_start();
			$form->display();
			$htmlForm = ob_get_clean();
			$smarty->assign('midcontent', $htmlForm);
			$smarty->display('layouts/ranchbe.tpl');
			return;
		}

		$rs = $form->process();

		if($rs != false){
			$i = 0;
			$list = array();
			while( $row = $rs->FetchRow() ){
				$list[$i] = $row;
				$list[$i]['rt_number'] = '<a href="{$baseurl}/application/index/viewfile?document_id='.$row['rt_id'].'&space=workitem">' . $list[$i]['rt_number'] . '</a>';
				$list[$i]['rt_open_date'] = formatDate($row['rt_open_date']);
				$list[$i]['apply_to'] = $row['applyto_number'] .'.v'.$row['applyto_indice'].'.'.$row['applyto_version'] . '('.$row['applyto_id'].')';
				unset($list[$i]['applyto_number'], $list[$i]['applyto_indice'], $list[$i]['applyto_version'], $list[$i]['applyto_id']);
				$i++;
			}
			$smarty->assign( 'headers', array_keys($list[0]) );
		}

		$smarty->assign( 'list', $list );
		$smarty->assign( 'pageTitle', 'Refus pour le document '. $form->document->GetProperty('document_normalized_name') );
		$smarty->assign( 'mid', 'custom/rtmanager/getrtforperiod.tpl' );
		$smarty->display('layouts/ranchbe.tpl');
	}
}

