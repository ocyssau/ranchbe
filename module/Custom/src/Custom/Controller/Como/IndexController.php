<?php
namespace Custom\Controller\Docsier;

use Custom\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;

class IndexController extends AbstractPublicController
{

	public $pageId = 'como_index';
	public $defaultSuccessForward = 'custom/rt/index';
	public $defaultFailedForward = 'custom/rt/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$DOCSIERcontainerIds[] = 9;
		$this->containerIds = $DOCSIERcontainerIds;

		$spaceName = 'bookshop';
		$this->factory = DaoFactory::get($spaceName);

		$this->layout()->setTemplate('custom/layout/layout');
		$this->areaId = 1;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('custom/como/index/index');
		$factory = $this->factory;
		$request = $this->getRequest();
		$list = null;

		return $view;
	}

	/**
	 *
	 */
	public function getRtApplyToDocumentAction()
	{
		$form = new Form\Como\ApplyToDocument('getRtApplyToDocument');

		// Try to validate the form
		if ( !$form->validate() ) {
			ob_start();
			$form->display();
			$htmlForm = ob_get_clean();
			$this->view->assign('midcontent', $htmlForm);
			$this->view->assign('mid', 'como/import.tpl');
			$this->view->display($this->layout);
			return;
		}

		$rs = $form->process();

		$i = 0;
		$list = array();
		while( $row = $rs->FetchRow() ){
			$list[$i] = $row;
			$list[$i]['como_number'] = '<a href="{$baseurl}/application/index/viewfile?document_id='.$row['como_id'].'&space=workitem">' . $list[$i]['como_number'] . '</a>';
			$list[$i]['como_open_date'] = formatDate($row['como_open_date']);
			$list[$i]['apply_to'] = $row['applyto_number'] .'.v'.$row['applyto_indice'].'.'.$row['applyto_version'] . '('.$row['applyto_id'].')';
			unset($list[$i]['applyto_number'], $list[$i]['applyto_indice'], $list[$i]['applyto_version'], $list[$i]['applyto_id']);
			$i++;
		}

		$this->view->assign( 'headers', array_keys($list[0]) );
		$this->view->assign( 'list', $list );
		$this->view->assign( 'pageTitle', 'Refus pour le document '. $form->document->GetProperty('document_normalized_name') );

		$this->view->assign('mid', 'como/display.tpl');
		$this->view->display($this->layout);
	}

}
