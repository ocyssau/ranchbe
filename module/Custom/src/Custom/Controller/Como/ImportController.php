<?php


/** Create the properties
 'rt_name' : string
 'rt_type' : string
 'rt_reason' : string
 'rt_emitted' : date
 'rt_apply_to' : integer
 */

require_once './conf/ranchbe_setup.php';
require_once './class/common/document.php';
require_once './class/common/container.php';
require_once './class/common/space.php';
require_once './class/doctype.php';
require_once './class/Rt.php';
// Load the controller
require_once('HTML/QuickForm/Controller.php');
// Load the base Action class (we will subclass it later)
require_once('HTML/QuickForm/Action.php');
require_once 'HTML/QuickForm/Action/Next.php';
require_once 'HTML/QuickForm/Action/Back.php';
//require_once 'HTML/QuickForm/Action/Jump.php';
//require_once 'HTML/QuickForm/Action/Display.php';
require_once('./GUI/GUI.php');

$Manager = container::_factory( 'workitem' );
$smarty = Ranchbe::getView();

// Class representing a form
class FirstPage extends HTML_QuickForm_Page
{
	public static $_container_id = 1;

	function buildForm()
	{
		$this->_formBuilt = true;

		$_SESSION['ImportRt'] = array();

		$this->addElement('header', null, 'Enregistrer le refus technique');
		$this->addElement('header', null, 'Lier à');
		$this->addElement('file', 'file', 'File', array('size'=>32));

		require_once 'HTML/QuickForm/livesearch_select.php';

		$lsSelectOptions = array('elementId' => 'documentls', //element id, name must be same that method in .class.php
		//							'callback' => array('Test', 'getTestName'),//callback function to retrieve value from ID selection
		//							'dbh' => $dbranchbe, //optional handler for callback function
		//                          'style' => '',//optional class for style not set or '' ==> default
		//                          'divstyle' => '',//optional class for style not set or '' ==> default
		//                          'searchZeroLength' => 1, //enable the search request with 0 length keyword(Default 0)
		//                          'buffer' => 350, //set the interval single buffer send time (ms)
                          'printStyle' => 0, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
                          'autoComplete' => 0, //if 0 the autocomplete attribute will not be set. Default not set;
                          'autoserverPath' => 'DocumentLiveSearchServer.php?q=%string%&inputid=documentls', //path to server scripts. Special word %string% indicate the input string parameter to search.
		);

		$this->addElement('livesearch_select', 'document', 'Document', $lsSelectOptions, array('size' => 32) );

		$this->addElement('hidden', 'space', 'workitem');
		$this->addElement('submit', $this->getButtonName('next'), 'Next >>');

		// Define filters and validation rules
		$this->applyFilter('document', 'trim');
		$this->addRule('file', 'Please select a file', 'required', null, 'client');
		$this->addRule('document', 'Please select a document', 'required', null, 'client');

		$this->setDefaultAction('submit');
	}
}

class FirstPageActionNext extends HTML_QuickForm_Action_Next
{
	function perform(&$page, $actionName)
	{
		// save the form values and validation status to the session
		$page->isFormBuilt() or $page->buildForm();
		$pageName =  $page->getAttribute('id');
		$data = $page->controller->container();
		$nextName = 'secondPage';
		$previousName = 'firstPage';

		$prev = $page->controller->getPage($previousName);
		$next = $page->controller->getPage($nextName);

		$file = $page->getElement('file');
		$document = $page->getElement('document');
		$space_name = $page->getElement('space')->getValue();

		$document_id = $document->getValue();

		$space = new space($space_name);
		$document = new document($space);
		if($document_id > 0){
			$document->init($document_id);
			$container_id = $document->GetProperty('container_id');
			$document_number = $document->GetProperty('document_number');
			$document_indice = $document->GetProperty('document_indice_id');
			$document_version = $document->GetProperty('document_version');
		}

		$wildspacePath = $space->getWildspace();
		if ($file->isUploadedFile()) {
			$file->moveUploadedFile( $wildspacePath );
		}
		else {
			echo "Fichier non téléchargé";
			return $prev->handle('display');
		}

		if(!$document_id || !$document){
			echo "Fichier non téléchargé";
			return $prev->handle('display');
		}

		$file_attrs = $file->getValue();
		$file_size = $file_attrs['size'];
		$file_name = $file_attrs['name'];

		$session =& $_SESSION['ImportRt'];
		$session['document_number'] = $document_number;
		$session['document_indice_id'] = $document_indice;
		$session['document_version'] = $document_version;
		$session['document_id'] = $document_id;
		$session['container_id'] = $container_id;
		$session['space_name'] = $space_name;
		$session['file_name'] = $file_name;
		$session['file_path'] = $wildspacePath;

		$next->handle('display');
	}
}


class FailedPageActionBack extends HTML_QuickForm_Action_Next
{
	function perform(&$page, $actionName)
	{
		$nextName = 'secondPage';
		$next = $page->controller->getPage($nextName);
		$next->handle('display');
	}
}

class SuccessPageActionNext extends HTML_QuickForm_Action_Next
{
	function perform(&$page, $actionName)
	{
		$nextName = 'firstPage';
		$next = $page->controller->getPage($nextName);
		$next->handle('display');
	}
}


// Class representing a form
class SecondPage extends HTML_QuickForm_Page
{
	function buildForm()
	{
		$this->_formBuilt = true;

		$session =& $_SESSION['ImportRt'];

		//var_dump($session);

		$document_number = $session['document_number'];
		$document_id = $session['document_id'];
		$container_id = $session['container_id'];
		$space_name = $session['space_name'];
		$file_name = $session['file_name'];
		$file_path = $session['file_path'];

		$space = new space( $space_name );
		$container = container::_factory( $space_name, $container_id );
		$session['rt_container_number'] = $container->getProperty('container_number');
		$document = new document( $space, $document_id );

		$normalized_file_name = substr($file_name, 0, strrpos($file_name, '.'));
		$normalized_file_name = trim($normalized_file_name);
		$normalized_file_name = str_replace(' ', '_', $normalized_file_name);

		$document_indice_id = $document->GetProperty('document_indice_id');
		$document_version = $document->GetProperty('document_version');
		$document_normalized_number = $document->GetNumber() . '_v' . $document_indice_id . '.' . $document_version;

		$rt_number = $document_normalized_number . '_RT_' . $normalized_file_name;
		$rt_name = $file_name;

		$this->addElement('header', null, 'Enregistrer le refus technique');
		$this->addElement('header', null, 'Propriétés');

		$this->addElement('static',null , 'Container : ' . $container->GetName() );
		$this->addElement('static',null , 'Document associé : ' . $document->GetNumber() );

		$this->addElement('text', 'rt_number', 'Numéro', array('size'=>64, 'value'=>$rt_number));
		$this->addElement('text', 'rt_name', 'Nom', array('size'=>64, 'value'=>$rt_name));
		$select = $this->addElement('select', 'rt_type', 'Type');
		$select->addOption('bloquant', 'bloquant');
		$select->addOption('informatif', 'informatif');

		//$this->addElement('textarea', 'rt_reason', 'Causes du refus', array('size'=>32));

		$rt_reason_params = array(
			            'field_type'=>'selectFromDB',
			            'field_name'=>'rt_reason',
			            'field_description'=>'Causes du refus',
						'table_name'=>'rt_aif_code_view1',
						'field_for_display'=>'code_title',
			            'field_for_value'=>'code',
			            'default_value'=>'',
			            'field_multiple'=>true,
			            'return_name'=>false,
			            'field_required'=>false,
			            'adv_select'=>true,
			            'display_both'=>false,
			            'disabled'=>false,
			            'field_id'=>'rt_reason'
			            );
			            construct_element($rt_reason_params, $this);

			            /*
			             $date_params = array(
			             'field_name'=>'rt_emitted',
			             'field_description'=>'Date d\'�mission',
			             'field_type'=>'date',
			             'date_language'=>'fr');
			             construct_element($date_params, $this);
			             */

			            $this->addElement('submit', $this->getButtonName('next'), 'Next >>');

			            // Define filters and validation rules
			            $this->applyFilter('rt_number', 'trim');
			            $this->applyFilter('rt_name', 'trim');
			            $this->addRule('rt_number', 'Please input a number', 'required', null, 'client');
			            $this->addRule('rt_name', 'Please input a name', 'required', null, 'client');

			            $this->setDefaultAction('submit');
	}
}


class SecondPageActionNext extends HTML_QuickForm_Action_Next
{
	function perform(&$page, $actionName)
	{
		$page->isFormBuilt() or $page->buildForm();

		$session =& $_SESSION['ImportRt'];

		$category_id = $session['category_id'];
		$doctype_id = Rt::$_doctype_id;
		$document_number = $session['document_number'];
		$document_normalizedName = $session['document_number'].'-v'.$session['document_indice_id'] . '.' . $session['document_version'];
		$document_id = $session['document_id'];
		$container_id = $session['container_id'];
		$space_name = $session['space_name'];
		$file_path = $session['file_path'];
		$fromfile_name = $session['file_name'];
		$fromfile = $file_path . '/' . $fromfile_name;

		$rt_number = $page->getElement('rt_number')->getValue();
		$rt_name = $page->getElement('rt_name')->getValue();
		$rt_type = $page->getElement('rt_type')->getValue();
		$rt_reason = $page->getElement('rt_reason')->getValue();
		//$rt_emitted = $page->getElement('rt_emitted')->getValue();
		$designation = $rt_name;

		$rt_reason = json_encode($rt_reason);

		$session['rt_number'] = $rt_number;
		$session['rt_name'] = $rt_name;
		$session['rt_type'] = $rt_type;
		$session['rt_reason'] = $rt_reason;
		$session['rt_emitted'] = $rt_emitted;
		$session['rt_apply_to'] = $document_id;

		$file_ext = substr( $fromfile_name, strrpos($fromfile_name, '.') );
		$file_name = $rt_number . '' . $file_ext;
		$file = $file_path . '/' . $file_name;
		if( is_file($fromfile) ){
			rename($fromfile, $file);
		}

		$session['rt_file_name'] = $file_name;

		$space = new space($space_name);
		//$container = container::_factory( $space_name, $container_id );

		$rt = new document($space, 0);
		$inputs = array('container_id'=>$container_id,
    					'document_number'=>$rt_number,
    					'designation'=>$designation,
    					'rt_name'=>$rt_name,
    					'rt_type'=>$rt_type[0],
    					'rt_reason'=>$rt_reason,
    					'rt_emitted'=>$rt_emitted,
						'rt_apply_to'=>$document_id,
    					'category_id'=>$category_id,
    					'doctype_id'=>$doctype_id,
		);

		$ok = $rt->Store($inputs, false);
		if($ok){
			$rt->AssociateFile($file , true);
			$rt->SetDocProperty( 'doctype_id', $doctype_id );
			//$rt->UpdateDocument( $rt->GetProperties() );
			$next = $page->controller->getPage('successPage');
			$session['errors'] = $rt->errorStack->getErrors();
		}else{
			$session['errors'] = $rt->errorStack->getErrors();
			$next = $page->controller->getPage('failedPage');
		}


		//----------------------------------------------------------------------
		//Send mail to conceptor and checker users
		require_once('class/common/Dbview.php');
		$dbView = new Rb_Common_Dbview('view_instances_activities');
		$filter = " document_id = $document_id
					 AND (act_name LIKE 'a_verifier'
					 		OR act_name LIKE 'Verifier')";
		$rs = $dbView->getFromFilter( $filter, array('user_email', 'act_user') );

		if($rs != false && $rs->RecordCount() > 0 ){
			require_once './class/Mail.php';
			$mail = new Rb_Mail();

			while (!$rs->EOF) {
				$mail->addTo($rs->fields[0], $rs->fields[1]);
				$rs->MoveNext();
			}
			$rs->Close();

			$subject = "Un refus technique a été enregistré pour le document $document_normalizedName";
			$htmlMessage = "Un refus technique a été enregistré pour le document $document_normalizedName";
			$htmlMessage .= "Vous êtes destinataire de ce message car vous êtes impliqué dans la validation de ce document.";
			$htmlMessage .= "Veuillez faire le nécessaire pour répondre à ce refus<br />";
			$htmlMessage .= '<ul>';
			$htmlMessage .= "<li>Appliqué au document: $document_normalizedName</li>";
			$htmlMessage .= "<li>Refus: $rt_number</li>";
			$htmlMessage .= "<li>Type: $rt_type</li>";
			$htmlMessage .= "<li>Affaire: ".$session['rt_container_number']."</li>";
			$htmlMessage .= '</ul>';

			$mail->setBodyHtml($htmlMessage);
			$mail->setSubject($subject);
			$mail->send();
		}

		$next->handle('display');
	}

}

// Class representing a form
class SuccessPage extends HTML_QuickForm_Page
{
	function buildForm()
	{
		$this->_formBuilt = true;

		$session =& $_SESSION['ImportRt'];

		$this->addElement('header', null, 'Success');

		foreach ($session['errors'] as $msg){
			$this->addElement('static', null, '<font color="green">' . $msg['message'] . '</font>' );
		}

		$this->addElement('static', null, 'Created file : ' . $session['rt_file_name'] );
		$this->addElement('static', null, 'Created document : ' . $session['rt_number'] );
		$this->addElement('static', null, 'In Container : ' . $session['rt_container_number'] );

		$this->addElement('static', null, 'Associate to document : ' . $session['document_number'] . '.v' . $session['document_indice_id'] .'.'. $session['document_version']);

		$session = array();

		$this->addElement('submit', $this->getButtonName('next'), 'Next >>');

		$this->setDefaultAction('submit');
	}
}

// Class representing a form
class FailedPage extends HTML_QuickForm_Page
{
	function buildForm()
	{
		$this->_formBuilt = true;

		$session =& $_SESSION['ImportRt'];

		$this->addElement( 'header', null, 'Failed');
		$this->addElement( 'static', null, 'Error is occured during saving' );

		foreach ($session['errors'] as $msg){
			//var_dump( $msg );
			$this->addElement('static', null, '<font color="red">' . $msg['message'] . '</font>' );
		}

		$this->addElement('static',null , 'File : ' . $session['file_name'] );
		$this->addElement('static',null , 'Document : ' . $session['document_id'] );
		$this->addElement('static',null , 'Number : ' . $session['document_number'] );
		$this->addElement('static',null , 'Name : ' . $session['file_path'] );

		$this->addElement('submit', $this->getButtonName('next'), 'Next >>');
		$this->setDefaultAction('submit');
	}
}

// Instantiate the Controller
$controller = new HTML_QuickForm_Controller('rtimport');

// Set defaults for the form elements
$controller->setDefaults(array(
    'document' => ''
));

$firstPage = new FirstPage('firstPage');
$secondPage = new SecondPage('secondPage');
$successPage = new SuccessPage('successPage');
$failedPage = new FailedPage('failedPage');

// Add the page to Controller
$controller->addPage($firstPage);
$controller->addPage($secondPage);
$controller->addPage($successPage);
$controller->addPage($failedPage);

$firstPage->addAction('next', new FirstPageActionNext());
$secondPage->addAction('next', new SecondPageActionNext());

$successPage->addAction('next', new SuccessPageActionNext());
$failedPage->addAction('next', new SuccessPageActionNext());
$failedPage->addAction('back', new FailedPageActionBack());

// Process the request
ob_start();
$controller->run();

// Display the template
$smarty->assign('midcontent', ob_get_clean());
$smarty->display('ImportRt.tpl');

