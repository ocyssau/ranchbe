<?php
namespace Custom\Controller\Docsier;

use Custom\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;

class IndexController extends AbstractPublicController
{

	public $pageId = 'docsier_index';
	public $defaultSuccessForward = 'custom/docsier/index';
	public $defaultFailedForward = 'custom/docsier/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$DOCSIERcontainerIds[] = 9;
		$this->containerIds = $DOCSIERcontainerIds;

		$spaceName = 'bookshop';
		$this->factory = DaoFactory::get($spaceName);

		$this->layout()->setTemplate('custom/layout/layout');
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('custom/docsier/index/index');
		$factory = $this->factory;
		$request = $this->getRequest();
		$containerId = $this->containerIds[0];
		$list = null;

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Custom\Form\Docsier\Index\FilterForm($factory, $this->pageId, $containerId);
		$filterForm->setAttribute('id', 'filterform');
		$filterForm->setData($request->getPost());
		$filterForm->bindToFilter($filter);

		if ( $request->isPost() ) {
			if ( $filterForm->isValid() ) {
				$dao = $factory->getDao(Document\Version::$classId);
				$select = array();
				$select[] = 'docsier_date';
				$select[] = 'docsier_famille';
				$select[] = 'docsier_keyword';
				$select[] = $dao->toSys('description'). ' as description';
				$select[] = $dao->toSys('number'). ' as number';
				$select[] = $dao->toSys('id'). ' as id';

				$filter->select($select);
				foreach( $this->containerIds as $containerId ) {
					$filter->orfind($containerId, $dao->toSys('containerId'), Op::EQUAL);
				}

				$list = $factory->getList(Document\Version::$classId);
				$list->countAll = $list->countAll($filter);
				$list->load($filter);
			}
		}

		$view->list = $list;
		$view->filter = $filterForm;
		return $view;
	}
}
