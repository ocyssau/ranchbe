<?php
namespace Custom\Controller;

/**
 *
 *
 */
class IndexController extends AbstractPublicController
{
	public $pageId = 'custom_index'; //(string)
	public $defaultSuccessForward = 'custom';
	public $defaultFailedForward = 'custom';

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
	} //End of method
}
