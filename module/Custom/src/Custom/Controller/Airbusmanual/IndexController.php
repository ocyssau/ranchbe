<?php
namespace Custom\Controller\Airbusmanual;

use Custom\Controller\AbstractPublicController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbplm\Dao\Filter\Op;

class IndexController extends AbstractPublicController
{
	public $pageId = 'airbus_manual'; //(string)
	public $defaultSuccessForward = 'custom/airbusmanual/index';
	public $defaultFailedForward = 'custom/airbusmanual/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		$DOCSIERcontainerIds[] = 3; // Id du conteneur NormesAirbus
		$DOCSIERcontainerIds[] = 5; // Id du conteneur DirectivesFAL-GSE
		$DOCSIERcontainerIds[] = 4; // Id du conteneur MemosAirbus
		$DOCSIERcontainerIds[] = 7; // Id du conteneur NormesDassaultBreguet
		$this->containerIds = $DOCSIERcontainerIds;

		$spaceName = 'bookshop';
		$this->factory = DaoFactory::get($spaceName);

		$this->layout()->setTemplate('custom/layout/layout');
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('custom/airbusmanual/index');
		$factory = $this->factory;
		$request = $this->getRequest();
		$list = null;

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Custom\Form\Airbusmanual\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'filterform');
		$filterForm->setData($request->getPost());
		$filterForm->bindToFilter($filter);

		if ($request->isPost()){
			if ( $filterForm->isValid() ) {
				$dao = $factory->getDao(Document\Version::$classId);
				$select = array();
				foreach($dao->metaModel as $asSys=>$asApp){
					$select[] = $asSys.' as '. $asApp;
				}
				$select[] = 'maj_int_ind';
				$select[] = 'min_int_ind';

				$filter->select($select);
				foreach($this->containerIds as $containerId){
					$filter->orfind( $containerId, $dao->toSys('containerId'), Op::EQUAL );
				}

				$list = $factory->getList(Document\Version::$classId);
				$list->countAll = $list->countAll($filter);
				$list->load($filter);

			}
		}

		$view->list = $list;
		$view->filter = $filterForm;
		return $view;
	}
}
