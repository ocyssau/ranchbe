<?php
namespace Custom\Model\Rt;

/*
 CREATE TABLE IF NOT EXISTS `rt_aif_code_a350` (
 `code` char(15) COLLATE latin1_general_ci NOT NULL,
 `itp_cluster` char(4) COLLATE latin1_general_ci DEFAULT NULL,
 `check` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `mistake` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `type` enum('B','I','OS') COLLATE latin1_general_ci NOT NULL,
 `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
 `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
 PRIMARY KEY (`code`)
 ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Codes erreurs pour le programme A350';
 */

/*
	CREATE VIEW `view_rt` AS
	SELECT
	`rt`.`document_id` AS `rt_id`,
	`rt`.`document_number` AS `rt_number`,
	`rt`.`document_state` AS `rt_status`,
	`rt`.`document_access_code` AS `rt_access_code`,
	`rt`.`open_date` AS `rt_open_date`,
	`rt`.`rt_reason` AS `rt_reason`,
	`rt`.`rt_type` AS `rt_type`,
	`applydocs`.`document_id` AS `applyto_id`,
	`applydocs`.`document_number` AS `applyto_number`,
	`applydocs`.`document_indice_id` AS `applyto_indice`,
	`applydocs`.`document_version` AS `applyto_version`,
	`applydocs`.`document_state` AS `applyto_status`,
	`linked_docs`.`document_number` AS `from_number`,
	`linked_docs`.`document_indice_id` AS `from_version`,
	`linked_docs`.`workitem_id` AS `container_id`
	FROM (
		(`workitem_documents` `rt` LEFT JOIN `workitem_documents` `linked_docs`
			ON((`linked_docs`.`document_id` = `rt`.`rt_apply_to`)))
				JOIN `workitem_documents` `applydocs`
				ON((`linked_docs`.`document_number` = `applydocs`.`document_number`)
		)
	);

	CREATE VIEW `view_rt_b` AS
	SELECT
	`rt`.`document_id` AS `rt_id`,
	`rt`.`document_number` AS `rt_number`,
	`rt`.`document_state` AS `rt_status`,
	`rt`.`document_access_code` AS `rt_access_code`,
	`rt`.`open_date` AS `rt_open_date`,
	`rt`.`rt_reason` AS `rt_reason`,
	`rt`.`rt_type` AS `rt_type`,
	`linked_docs`.`document_id` AS `applyto_id`,
	`linked_docs`.`document_number` AS `applyto_number`,
	`linked_docs`.`document_indice_id` AS `applyto_indice`,
	`linked_docs`.`document_version` AS `applyto_version`,
	`linked_docs`.`document_state` AS `applyto_status`,
	`linked_docs`.`document_number` AS `from_number`,
	`linked_docs`.`document_indice_id` AS `from_version`,
	`linked_docs`.`workitem_id` AS `container_id`
	FROM
	(`workitem_documents` `rt` LEFT JOIN `workitem_documents` `linked_docs`
		ON(`linked_docs`.`document_id` = `rt`.`rt_apply_to`)
	);
*/



Class Rt extends objects
{

	public static $_doctype_id = 188;
	public static $_appliedStatus = 'rt_applied';
	public static $_confirmedStatus = 'rt_confirmed';

	function __construct($pkey = null, $table = 'rt_aif_code_a350'){
		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();

		$this->OBJECT_TABLE  = $table;
		$this->FIELDS_MAP_ID = 'code';
		$this->FIELDS_MAP_NUM = 'code';
		$this->FIELDS_MAP_DESC = 'description';

		if( !is_null($pkey) ){
			$this->pkey = $pkey;
			$this->_props = $this->GetBasicInfos($this->pkey);
		}
	}//End of method


	/**
	 * @param integer
	 * @param integer
	 * @param boolean
	 * @param array
	 * @return ADORecordSet
	 *
	 */
	function GetRtForPeriod($fromDate, $toDate, $onlyCount=false, $rset_return=false, $select=array()){
		//$fromDate = ;
		//$toDate = ;

		$select = implode(',', $select);
		if(!$select) $select = '*';

		$view_name = 'view_rt';

		if($onlyCount){
			$sql = 'SELECT COUNT(rt_id) FROM ' . $view_name;
		}else{
			$sql = 'SELECT '.$select.' FROM ' . $view_name;
		}

		//$sql .= ' WHERE doctype_id='.self::$_doctype_id;
		$sql .= ' WHERE rt_open_date > '. $fromDate;
		$sql .= ' AND rt_open_date < '. $toDate;

		//echo $sql;
		if($onlyCount){
			$one = $this->dbranchbe->GetOne($sql);
			return $one;
		}else{
			$rs = $this->dbranchbe->execute($sql);
			if($rset_return) return $rs;
			return $rs->GetArray();
		}
	}//End of method

	/**
	 * @param integer
	 * @param integer
	 * @param boolean
	 * @param array
	 * @return ADORecordSet
	 */
	function GetRtApplyToDocument(document $document, $onlyCount=false, $rset_return=false, $select=array(), $filter=''){

		//$select = array_merge( array('rt_id', 'rt_number'), $select);
		$select = implode(',', $select);
		//if(!$select) $select = 'rt_id, rt_number, rt_status, rt_access_code, rt_open_date, applyto_id, applyto_number, applyto_indice, applyto_version, applyto_status, from_number, from_version';
		if(!$select) $select = '*';

		$document_number = $document->GetProperty('document_number');

		$view_name 	= 'view_rt_b';

		if($onlyCount){
			$sql = 'SELECT COUNT(rt_id) FROM '. $view_name;
		}else{
			$sql = 'SELECT '.$select.' FROM '.$view_name;
		}
		$sql .= ' WHERE applyto_number=\'' . $document_number . '\'';

		if($filter){
			$sql .= ' AND ' . $filter;
		}

		if($onlyCount){
			$one = $this->dbranchbe->GetOne($sql);
			return $one;
		}else{
			$rs = $this->dbranchbe->execute($sql);
			if($rset_return){
				return $rs;
			}
			return $rs->GetArray();
		}
	}//End of method



	//----------------------------------------------------------
	/*! \brief Return properties of current object
	 *  Return array or false
	 */
	function GetProperties(){
		if(!isset($this->_props)) return false;
		else return $this->_props;
	}//End of method


	//----------------------------------------------------------
	/*! \brief Get the property of the document by the property name. init() must be call before
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function GetProperty($property_name){
		return $this->_props[$property_name];
	}//End of method

	//----------------------------------------------------------
	/*! \brief Set property of the document by the property name.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 * \param $property_value(string)
	 */
	function SetProperty($property_name, $property_value){
		$this->_props[$property_name] = $property_value;
		return true;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Unset a property of the document.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function UnSetProperty($property_name){
		unset($this->_props[$property_name]);
		return true;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Return the document id
	 * else return false.
	 *
	 */
	function GetId(){
		return $this->pkey;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 *
	 */
	function GetAll( $params = array() ){
		return $this->Get($this->OBJECT_TABLE, $params);
	}//End of method

}

