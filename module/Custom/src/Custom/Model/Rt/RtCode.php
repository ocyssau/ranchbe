<?php
namespace Custom\Model\Rt;
/*
 CREATE TABLE IF NOT EXISTS `rt_aif_code_a350` (
 `code` char(15) COLLATE latin1_general_ci NOT NULL,
 `itp_cluster` char(4) COLLATE latin1_general_ci DEFAULT NULL,
 `check` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `mistake` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `type` enum('B','I','OS') COLLATE latin1_general_ci NOT NULL,
 `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
 `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
 PRIMARY KEY (`code`)
 ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Codes erreurs pour le programme A350';
*/

Class RtCode extends basic
{

	function __construct($pkey = null, $table = 'rt_aif_code_a350'){
		$this->dbranchbe = Ranchbe::getDb();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();

		$this->OBJECT_TABLE  = $table;
		//$this->FIELDS_MAP_ID = 'code';
		$this->FIELDS_MAP_NUM = 'code';
		$this->FIELDS_MAP_DESC = 'description';

		if( !is_null($pkey) ){
			$this->FIELDS_MAP_ID = 'code';
			$this->pkey = $pkey;
			$this->load($pkey);
		}
	}//End of method

	public function loadFromArray($props)
	{
		$this->_props = $props;
	}


	//--------------------------------------------------------
	/**
	 * Return primary id
	 */
	function getId(){
		return $this->pkey;
	}//End of method

	/**
	 * Setter
	 */
	public function setDescription($str)
	{
		$this->_props['description'] = $str;
	}

	/**
	 * Setter
	 */
	public function setCode($str)
	{
		$this->_props['code'] = $str;
	}

	/**
	 * Setter
	 * Enum B, I, OS
	 */
	public function setType($str)
	{
		$this->_props['type'] = $str;
	}

	/**
	 * Getter
	 */
	public function getDescription()
	{
		return $this->_props['description'];
	}

	/**
	 * Getter
	 */
	public function getCode()
	{
		return $this->_props['code'];
	}

	/**
	 * Getter
	 */
	public function getType()
	{
		return $this->_props['type'];
	}

	/**
	 *
	 */
	function GetAll( $params = array() ){
		return $this->Get($this->OBJECT_TABLE, $params);
	}//End of method


	/**
	 *
	 */
	public function save()
	{
		if(!$this->pkey){
			$this->BasicCreate($this->_props);
		}
		else{
			$data['description'] =  $this->_props['description'];
			$data['code'] =  $this->_props['code'];
			$data['type'] =  $this->_props['type'];
			$this->BasicUpdate($data, $this->pkey);
		}
	}

	/**
	 *
	 */
	public function suppress()
	{
		if($this->pkey){
			$this->BasicSuppress($this->pkey);
		}
	}
}

