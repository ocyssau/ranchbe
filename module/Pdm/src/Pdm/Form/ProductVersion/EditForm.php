<?php
namespace Pdm\Form\ProductVersion;

use Zend\Form\Form;

use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class EditForm extends Form implements InputFilterProviderInterface
{
	protected $inputFilter;
	public $template;

	/**
	 * @param unknown_type $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('workunit');

		$this->template = 'pdm/product-version/editForm';

		$this
		->setAttribute('method', 'post')
		->setHydrator(new Hydrator(false))
		->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'name',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Name',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Name',
			),
		));

		$this->add(array(
			'name' => 'description',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Description',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Description',
			),
		));

		$this->add(array(
			'name' => 'contextId',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Select Context',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Context',
				'value_options' => $this->_getContextOptions(),
			),
		));

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Cancel',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			),
			'name' => array(
				'required' => true,
			),
			'description' => array(
				'required' => true,
			),
		);
	}

	/**
	 *
	 */
	protected function _getContextOptions()
	{
		return array(
			1 => 'Mechanical',
			2 => 'Electric',
			3 => 'Buizness',
		);
	}
}
