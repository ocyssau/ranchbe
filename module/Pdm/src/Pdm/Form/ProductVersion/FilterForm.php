<?php
namespace Pdm\Form\ProductVersion;

use Application\Form\AbstractFilterForm;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends AbstractFilterForm
{
	/**
	 * @param unknown_type $name
	 */
	public function __construct($factory=null, $name=null)
	{
		parent::__construct($factory, $name);

		$this->template = 'pdm/product-version/filterform.phtml';
		$inputFilter = $this->getInputFilter();

		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type'  => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => '',
			),
			'attributes' => array(
				'onChange'=>'',
				'class'=>'form-control',
			)
		));
		$inputFilter->add(array(
			'name' => 'stdfilter-searchInput',
			'required' => false,
		));

		$this->add(array(
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Filter',
				'id' => 'stdfilter-submit',
				'class'=>'btn btn-default'
			),
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		$this->prepare()->isValid();
		$search = $this->get('stdfilter-searchInput')->getValue();
		if($search){
			$splitted = explode(' ', $search);
			foreach($splitted as $word){
				$paramName = ':'.uniqid();
				$filter->orFind($paramName, $this->key, Op::LIKE);
				$this->bind[$paramName] = '%'.$word.'%';
			}
		}
		return $this;
	}
}
