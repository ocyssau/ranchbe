<?php
namespace Pdm\Form\ProductInstance;

use Zend\Form\Form;

use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class SetReferenceForm extends Form implements InputFilterProviderInterface
{
	protected $inputFilter;
	public $template;

	/**
	 * @param unknown_type $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('QuantityForm');

		$this->template = 'pdm/product-instance/setreferenceForm';

		$this
			->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* PRODUCT INSTANCE ID*/
		$this->add(array(
			'name' => 'instanceid',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'fromobjectclass',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'From',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'From',
				'value_options'=>array('select a type', 'productVersion'=>'ProductVersion','documentVersion'=>'DocumentVersion'),
			),
		));

		$this->add(array(
			'name' => 'spacename',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'From SpaceName',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'From SpaceName',
				'value_options'=>array('workitem'=>'Workitem','bookshop'=>'Bookshop','cadlib'=>'Cadlib'),
			),
		));

		$this->add(array(
			'name' => 'product',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'=>'text',
				'placeholder' => 'Number',
				'class'=>'productnumber-autocomplet form-control'
			),
			'options' => array(
				'label' => 'Product Number',
			),
		));

		$this->add(array(
			'name' => 'document',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'=>'text',
				'placeholder' => 'Number',
				'class'=>'documentnumber-autocomplet form-control'
			),
			'options' => array(
				'label' => 'Document Number',
			),
		));

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbtn',
			),
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbtn',
			),
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'instanceid' => array(
				'required' => true,
			),
			'spacename' => array(
				'required' => false,
			),
			'product' => array(
				'required' => false,
			),
			'document' => array(
				'required' => false,
			),
		);
	}
}
