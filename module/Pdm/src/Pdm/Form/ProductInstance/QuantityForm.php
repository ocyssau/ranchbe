<?php
namespace Pdm\Form\ProductInstance;

use Zend\Form\Form;

use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;


class QuantityForm extends Form implements InputFilterProviderInterface
{
	protected $inputFilter;
	public $template;

	/**
	 * @param unknown_type $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('QuantityForm');

		$this->template = 'pdm/product-instance/quantityForm';

		$this
			->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		/* PRODUCT INSTANCE ID*/
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'quantity',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'min'  => '0',
				'max'  => '10000',
				'step' => '1', // default step interval is 1
				'type'  => 'text',
				'placeholder' => 'Quantity',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Quantity',
			),
		));

		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbtn',
			),
		));

		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Cancel',
				'id' => 'cancelbtn',
			),
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			),
			'quantity' => array(
				'required' => true,
			),
		);
	}
}
