<?php
namespace Pdm\Controller;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm;
use Rbplm\Pdm\Product;
use Rbplm\Dao\NotExistingException;

class ProductInstanceController extends AbstractController
{

	/**
	 */
	public function init($view = null, $errorStack = null)
	{
		parent::init();
		\View\Helper\MainTabBar::get()->getTab('product')->activate();
	}

	/**
	 * Display instance of all process
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;

		$instanceId = $this->params()->fromQuery('instanceid');
		$productId = $this->params()->fromQuery('productid');
		$spaceName = $this->params()->fromQuery('spacename');
		$cancel = $this->params()->fromPost('cancel', false);

		$request = $this->getRequest();
		if ( $spaceName ) {
			$factory = DaoFactory::get($spaceName);
		}
		else {
			$factory = DaoFactory::get();
		}

		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		/* Load Instance */
		try {
			if ( $instanceId ) {
				$instance = new Product\Instance();
				$instanceDao->loadFromId($instance, $instanceId);
				$productId = $instance->ofProductId;
				$view->instance = $instance;
			}
		}
		catch( NotExistingException $e ) {}

		/* Load Product */
		try {
			if ( $productId ) {
				$product = new Product\Version();
				$productDao->loadFromId($product, $productId);
				if ( $instance ) {
					$instance->setOfProduct($product);
				}
				$view->product = $product;
			}
		}
		catch( NotExistingException $e ) {}

		/* Load Document */
		try {
			$documentId = $product->documentId;
			if ( $documentId ) {
				$spaceName = $product->spaceName;
				$factory = DaoFactory::get($spaceName);
				$documentDao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);

				$document = new \Rbplm\Ged\Document\Version();
				$documentDao->loadFromId($document, $documentId);
				if ( $product ) {
					$product->setDocument($document);
				}
				$view->document = $document;
			}
		}
		catch( NotExistingException $e ) {}

		$view->pageTitle = 'Product Details';
		return $view;
	}

	/**
	 * Ajax Style
	 */
	public function quantityAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$id = $this->params()->fromRoute('id');
		$cancel = $this->params()->fromPost('cancel', false);

		if ( $cancel ) {
			return $this->redirect()->toRoute('pdm/default', array(
				'controller' => 'productinstance'
			));
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);
		$productInstance = new Product\Instance();
		$dao->loadFromId($productInstance, $id);

		/* Form */
		$form = new \Pdm\Form\ProductInstance\QuantityForm();
		$form->bind($productInstance);
		$view->setTemplate($form->template);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($productInstance);
				if ( $this->isAjax ) {
					$this->serviceReturn();
				}
			}
		}
		else {
			$view->form = $form;
			$view->pageTitle = 'Edit Quantity';
			return $view;
		}
	}

	/**
	 */
	public function contextAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$id = $this->params()->fromRoute('id');
		$cancel = $this->params()->fromPost('cancel', false);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);
	}

	/**
	 */
	public function positionAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$id = $this->params()->fromRoute('id');
		$cancel = $this->params()->fromPost('cancel', false);

		if ( $cancel ) {
			return $this->redirect()->toRoute('pdm/default', array(
				'controller' => 'productinstance'
			));
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);
		$productInstance = new Product\Instance();
		$dao->loadFromId($productInstance, $id);

		/* Form */
		$form = new \Pdm\Form\ProductInstance\PositionForm();
		$form->bind($productInstance);
		$view->setTemplate($form->template);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($productInstance);
				if ( $this->isAjax ) {
					$this->serviceReturn();
				}
				else {
					return $this->redirect()->toRoute('pdm/default', array(
						'controller' => 'productinstance'
					));
				}
			}
		}
		else {
			$view->form = $form;
			$view->pageTitle = 'Edit Position Of ' . $productInstance->getName();
			return $view;
		}
	}

	/**
	 */
	public function reconciliateAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$productId = $this->params()->fromQuery('productid');
		$cancel = $this->params()->fromPost('cancel', false);

		if ( $cancel ) {
			return $this->redirect()->toRoute('pdm/default', array(
				'controller' => 'productinstance'
			));
		}

		$factory = DaoFactory::get();
		$productDao = $factory->getDao(Product\Version::$classId);
		$instanceDao = $factory->getDao(Product\Instance::$classId);

		$productVersion = new Product\Version();
		$productDao->loadFromId($productVersion, $productId);

		return $view;
	}

	/**
	 */
	public function setreferenceAction()
	{
		$view = $this->view;
		$request = $this->getRequest();

		isset($_REQUEST['instanceid']) ? $instanceId = $_REQUEST['instanceid'] : $instanceId = null;

		$cancel = $this->params()->fromPost('cancel', false);
		$from = $this->params()->fromPost('fromobjectclass', false);
		$productNumber = $this->params()->fromPost('product');
		$documentNumber = $this->params()->fromPost('document');
		$spaceName = $this->params()->fromPost('spacename', 'default');
		$validate = $this->params()->fromPost('validate', false);

		if ( $cancel ) {
			return $this->redirect()->toRoute('pdm/default', array(
				'controller' => 'productinstance'
			));
		}

		/* init factory with space */
		$factory = DaoFactory::get($spaceName);

		/* LOAD INSTANCE PRODUCT */
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productInstance = new Product\Instance();
		$instanceDao->loadFromId($productInstance, $instanceId);

		/* Form */
		$form = new \Pdm\Form\ProductInstance\SetReferenceForm();
		$form->bind($productInstance);
		$view->setTemplate($form->template);
		$datas = array();

		/* IF REFERENCE IS SET, LOAD IT */
		if($validate==false && $productInstance->ofProductId){
			/* LOAD REFERENCE PRODUCT*/
			$productDao = $factory->getDao(Product\Version::$classId);
			$productVersion = new Product\Version();
			$productDao->loadFromId($productVersion, $productInstance->ofProductId);
			$datas['spacename'] = $productVersion->spaceName;
			$datas['product'] = $productVersion->getNumber().'.v'.$productVersion->version;
			$datas['instanceid'] = $instanceId;
		}
		else{
			$datas['instanceid'] = $instanceId;
		}

		$form->setData($datas);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());

			if ( $form->isValid() ) {
				/* FROM PRODUCT */
				if ( $from == 'productVersion' ) {
					/* GET PRODUCT */
					$productDao = $factory->getDao(Product\Version::$classId);
					/* get versionId and number from autompleted number data */
					$productVersionId = str_replace('.v', '', substr($productNumber, strrpos($productNumber, '.')));
					$productNumber = substr($productNumber, 0, strrpos($productNumber, '.'));
					$productVersion = new Product\Version();
					$productDao->loadFromNumber($productVersion, $productNumber, $productVersionId);

					/* SET PRODUCT TO INSTANCE */
					$productInstance->setOfProduct($productVersion);

					/* SAVE INSTANCE */
					$instanceDao->save($productInstance);
				}
				/* FROM DOCUMENT */
				elseif ( $from == 'documentVersion' ) {
					/* GET DOCUMENT */
					$documentDao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
					/* get versionId and number from autompleted number data */
					$documentVersionId = str_replace('.v', '', substr($documentNumber, strrpos($documentNumber, '.')));
					$documentNumber = substr($documentNumber, 0, strrpos($documentNumber, '.'));
					/* Load Document */
					$document = new \Rbplm\Ged\Document\Version();
					$documentDao->loadFromNumber($document, $documentNumber, $documentVersionId);

					/* GET PRODUCT OF DOCUMENT */
					$productDao = $factory->getDao(Product\Version::$classId);
					try {
						$productVersion = new Product\Version();
						$productDao->load($productVersion, $productDao->toSys('documentId') . '=:documentId', array(
							':documentId' => $document->getId()
						));
					}
					catch( NotExistingException $e ) {
						/* IF PRODUCT IS NOT EXISTING, CREATE IT */
						$productVersion = Product\Version::initFromDocument($document);
						$productVersion->setDocument($document);
						/* SAVE PRODUCT */
						$productDao->save($productVersion);
					}

					/* SET PRODUCT TO INSTANCE */
					$productInstance->setOfProduct($productVersion);

					/* SAVE INSTANCE */
					$instanceDao->save($productInstance);
				}

				if ( $this->isAjax ) {
					$return = array(
						'instance'=>$productInstance->getArrayCopy(),
						'product'=>$productVersion->getArrayCopy(),
					);
					if($document){
						$return['document'] = $document->getArrayCopy();
					}
					return $this->serviceReturn($return);
				}
				else {
					return $this->redirect()->toRoute('pdm/default', array(
						'controller' => 'productinstance'
					));
				}
			}
		}

		$view->form = $form;
		$view->pageTitle = 'Set Reference Of ' . $productInstance->getName();
		return $view;
	}

	/**
	 * Input id = id of parent product version
	 */
	public function loadtreeAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$parentId = $this->params()->fromQuery('parentid');
		$productId = $this->params()->fromQuery('productid');

		if ( !$parentId ) {
			$parentId = $this->params()->fromRoute('id');
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);
		$instanceDao = $factory->getDao(Product\Instance::$classId);

		/* Load Root Product */
		if ( $parentId == '#' ) {
			$rootProduct = new Product\Version();
			$dao->loadFromId($rootProduct, $productId);
			$view->rootProduct = $rootProduct;
		}
		else {
			$stmt = $instanceDao->getChildren($parentId);
			$list = $stmt->fetchAll();
			$view->tree = $list;
		}

		return $view;
	}

	/**
	 */
	public function savetreeAction()
	{
		$view = $this->view;
		$request = $this->getRequest();
		$jsonTree = $this->params()->fromPost('tree');
		$parentId = $this->params()->fromPost('parentid');
		$cancel = $this->params()->fromPost('cancel', false);

		if ( $cancel ) {
			return $this->redirect()->toRoute('pdm/default', array(
				'controller' => 'productinstance'
			));
		}

		/* get tree as objects */
		$tree = json_decode($jsonTree, true);
		// var_dump($tree,$parentId);die;

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Instance::$classId);

		$ret = $this->_savenodeAsArray($tree, $parentId, $factory);
		$view->tree = $ret;
		return $view;
	}

	/**
	 */
	protected function _savenodeAsObject(&$node, $parentId, $dao)
	{
		$nodeData = & $node->data;
		$nodeName = & $node->text;

		isset($nodeData->productInstanceId) ? $id = $nodeData->productInstanceId : $id = null;

		if ( $parentId ) {
			$parent = new Product\Version();
			$dao->loadFromId($parent, $parentId);
		}
		else {
			$parent = new Product\Version();
		}

		if ( !$id ) {
			/* Must be create */
			$productInstance = Product\Instance::init();
			$productInstance->setName($nodeName)->setNumber($nodeName);
			$productInstance->setParent($parent);
			$dao->save($productInstance);
			$nodeData->productInstanceId = $productInstance->getId();
		}
		else {
			$productInstance = new Product\Instance();
			$dao->loadFromId($productInstance, $id);
			$productInstance->setName($nodeName);
			$productInstance->setParent($parent);
			$dao->save($productInstance);
		}

		if ( $node->children ) {
			$parentId = $productInstance->getId();
			foreach( $node->children as $child ) {
				$this->_savenodeAsObject($child, $parentId, $dao);
			}
		}
	}

	/**
	 */
	protected function _savenodeAsArray(&$node, $parent = null, $factory)
	{
		// var_dump($node);die;
		$children = &$node['children'];
		$nodeData = &$node['data'];
		$nodeName = $node['text'];
		$nodeType = $node['type'];
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		(isset($nodeData['id'])) ? $id = $nodeData['id'] : $id = null;
		(isset($nodeData['cid'])) ? $cid = $nodeData['cid'] : $cid = null;

		/* Node is a root product or an instance ? */
		($cid == Product\Version::$classId) ? $isRootProduct = true : $isRootProduct = false;

		/* Node is a instance */
		if ( $isRootProduct == false ) {
			/* Instance is existing ? */
			if ( $id ) {
				$instance = new Product\Instance();
				$instanceDao->loadFromId($instance, $id);
				$productId = $instance->ofProductId;
				if ( $productId ) {
					$product = new Product\Version();
					$productDao->loadFromId($product, $productId);
				}
				else {
					$product = Product\Version::init($nodeName);
					$productDao->save($product);
					$productId = $product->getId();
				}

				/* Populate and Save instance */
				$instance->type = $nodeType;
				$instance->setName($nodeName);
				$instanceDao->save($instance);
			}
			/* Instance is not yet existing, create it */
			else {
				/* try to load product from name */
				try {
					$product = new Product\Version();
					$productDao->loadFromNumber($product, $nodeName);
				}
				catch( NotExistingException $e ) {
					$product = Product\Version::init($nodeName);
					$productDao->save($product);
				}

				$instance = Product\Instance::init($nodeName);
				$instance->setOfProduct($product);
				$instance->setParent($parent);
				$instance->type = $nodeType;
				$instanceDao->save($instance);
				$id = $instance->getId();
				$productId = $product->getId();
			}

			/* Populate and Save product */

			/* Populate tree with instance and product data */
			$node['data'] = $instance->getArrayCopy();
			$node['previousId'] = $node['id'];
			$node['id'] = $id;

			if ( $children ) {
				foreach( $children as &$child ) {
					$this->_savenodeAsArray($child, $product, $factory);
				}
			}
		}
		/* Node is a product */
		elseif ( $isRootProduct == true ) {
			$productId = $id;
			$product = new Product\Version();
			$productDao->loadFromId($product, $productId);
			// $product->hydrate($nodeData);

			/* Populate tree with instance and product data */
			$node['data'] = $product->getArrayCopy();
			$node['previousId'] = $node['id'];
			$node['id'] = $id;

			if ( $children ) {
				foreach( $children as &$child ) {
					$this->_savenodeAsArray($child, $product, $factory);
				}
			}
		}

		return $node;
	}
}

