<?php

namespace Pdm\Controller;

use Zend\View\Model\ViewModel;
use Rbs\Space\Factory as DaoFactory;
use Application\Form\PaginatorForm;
use Rbplm\Pdm;
use Rbplm\Pdm\Product;
use Rbplm\People;

class ProductVersionController extends AbstractController
{

	/**
	 *
	 */
	public function init($view=null, $errorStack=null)
	{
		parent::init();
		\View\Helper\MainTabBar::get()->getTab('product')->activate();
	}

	/**
	 * AJAX Method to search in product version table
	 */
	public function searchAction()
	{
		$view = $this->view;
		$factory = DaoFactory::get();
		$request = $this->getRequest();

		$what = $this->params()->fromQuery('what');
		$where = $this->params()->fromQuery('where');
		$select = $this->params()->fromQuery('select');
		$op = $this->params()->fromQuery('op');

		/* is a ajax method*/
		$view->setTerminal(true);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(Product\Version::$classId);
		$list = $factory->getList(Product\Version::$classId);

		$filter->andfind($what, $dao->toSys($where), $op);

		$reducedSelect = array();
		if(is_string($select)){
			$select = array($select);
		}
		if(is_array($select)){
			foreach($dao->metaModel as $asSys=>$asApp){
				if(in_array($asApp, $select)){
					$reducedSelect[] = $asSys.' as '. $asApp;
				}
			}
			$filter->select($reducedSelect);
		}

		$list->load($filter);
		$view->list = $list;
		return $view;
	}

	/**
	 * Display instance of all process
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$factory = DaoFactory::get();

		$request = $this->getRequest();

		$userId = People\CurrentUser::get()->getLogin();
		$userLogin = People\CurrentUser::get()->getLogin();

		/** @var \Rbs\Dao\Sier\DaoList $list */
		$list = $factory->getList(Product\Version::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Pdm\Form\ProductVersion\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->key = 'CONCAT(id,uid,name,description)';
		$filterForm->prepare()->bindToFilter($filter)->save();
		$filterStr = $filter->__toString();
		$list->countAll = $list->countAll($filter, $filterForm->bind);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll);
		$paginator->setData($_POST);
		$paginator->setData($_GET);
		$paginator->prepare()->bindToView($this->view)->bindToFilter($filter)->save();
		$list->load($filter, $filterForm->bind);

		$view->list = $list;

		$view->headers = array(
			'#'=>'id',
			'Uid'=>'uid',
			'Name'=>'name',
			'Description'=>'description',
			'Version'=>'version',
		);

		$view->filter = $filterForm;
		$view->pageTitle = 'My products';
		return $view;
	}

	/**
	 *
	 */
	public function createAction()
	{
		$request = $this->getRequest();
		$view = new ViewModel();
		$factory = DaoFactory::get();

		$cancel = $this->params()->fromPost('cancel', false);
		$validate = $this->params()->fromPost('validate', false);

		if($cancel){
			return $this->redirect()->toRoute('pdm/default', array('controller'=>'productversion'));
		}

		$productVersion = Product\Version::init();
		$productVersion->ofProductUid = $this->params()->fromQuery('ofproduct', $productVersion->getUid());
		$productInstance = Product\Instance::init();

		$form = new \Pdm\Form\ProductVersion\EditForm();
		$form->bind($productVersion);

		if ($request->isPost()){
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$factory->getDao(Product\Version::$classId)->save($productVersion);

				//create a instance of this product version
				$productInstance->setProduct($productVersion);
				$productInstance->contextId = $form->get('contextId')->getValue();
				$productInstance->nomenclature = 0;
				$productInstance->setName($productVersion->getName().'.'.uniqid());
				$factory->getDao(Product\Instance::$classId)->save($productInstance);

				return $this->redirect()->toRoute('pdm/default', array('controller'=>'productversion'));
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = 'New Product Version';
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$view = $this->view;
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);

		if($cancel){
			return $this->redirect()->toRoute('pdm/default', array('controller'=>'productversion'));
		}

		$productVersion = Product\Version::init();
		$dao->loadFromId($productVersion, $id);

		$form = new \Pdm\Form\ProductVersion\EditForm();
		$form->bind($productVersion);

		if ($request->isPost()){
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$dao->save($productVersion);
				return $this->redirect()->toRoute('pdm/default', array('controller'=>'productversion'));
			}
		}

		$view->setTemplate($form->template);
		$view->form = $form;
		$view->pageTitle = 'Edit Product Version';
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$view = new ViewModel();
		if ($this->getRequest()->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);
		$id = $this->params()->fromRoute('id');
		$dao->deleteFromId($id);

		return $this->redirect()->toRoute('pdm/default', array('controller'=>'productversion'));
	}
}

