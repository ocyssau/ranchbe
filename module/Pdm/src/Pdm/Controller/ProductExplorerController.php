<?php

namespace Pdm\Controller;

use Rbs\Space\Factory as DaoFactory;
use Rbplm\Pdm;
use Rbplm\Pdm\Product;
use Rbplm\Ged\Document;
use Application\Form\PaginatorForm;

/**
 *
 */
class ProductExplorerController extends AbstractController
{
	public $pageId = 'product_explorer';
	public $defaultSuccessForward = '/pdm/productexplorer/index';
	public $defaultFailedForward = '/pdm/productexplorer/index';

	/**
	 *
	 */
	public function init($view=null, $errorStack=null)
	{
		parent::init();

		(isset($_REQUEST['page_id'])) ? $pageId = $_REQUEST['page_id'] : $pageId=$this->pageId;
		(isset($_REQUEST['basecamp'])) ? $basecamp = $_REQUEST['basecamp'] : $basecamp=null;

		\View\Helper\MainTabBar::get()->getTab('product')->activate();
	}

	/**
	 * Load or init a new product from a document
	 */
	public function fromdocumentAction()
	{
		$documentId = $this->params()->fromQuery('documentid');
		$spaceName = $this->params()->fromQuery('spacename');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$factory = DaoFactory::get($spaceName);
		$productDao = $factory->getDao(Product\Version::$classId);

		/* Load document */
		$document = new \Rbplm\Ged\Document\Version();
		$document->dao = $factory->getDao(\Rbplm\Ged\Document\Version::$classId);
		$document->dao->loadFromId($document, $documentId);

		/* Load product from document id */
		try {
			$product = new Product\Version();
			$filter = $productDao->toSys('documentId').'=:documentId';
			$productDao->load($product, $filter, array(':documentId'=>$documentId));
			$product->setDocument($document);
		}
		catch( \Exception $e ) {
			/* If none product found, create it */
			$product = Product\Version::init();
			$product->setName($document->getName())->setNumber($document->getName());
			$product->setDocument($document);
			$productDao->save($product);
		}

		/* Get index action */
		return $this->redirect()->toRoute('pdm/default', array('controller'=>'productexplorer', 'action'=>'index','id'=>$product->getId()));
	}


	/**
	 * Display instance of all process
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;

		$productId = $this->params()->fromRoute('id');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Product\Version::$classId);
		$rootProduct = new Product\Version();

		/* Load Root Product */
		$dao->loadFromId($rootProduct, $productId);

		$view->rootProduct = $rootProduct;
		$view->pageTitle = 'My products';
		return $view;
	}

	/**
	 * @param string $direction
	 * @param DaoFactory $factory
	 * @param integer $productId
	 */
	protected function _list($direction='children', $factory, $productId)
	{
		$select = array();
		$headers = array();

		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$filterForm = new \Pdm\Form\ProductVersion\FilterForm($factory, $this->pageId);
		$filterForm->setData($_POST);
		$filterForm->key = 'CONCAT(name,description)';
		$filterForm->passThrough = true;
		$filterForm->prepare()->bindToFilter($filter)->save();
		$filterStr = $filter->__toString();

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit(1000);
		$paginator->setData($_POST);
		$paginator->setData($_GET);
		$paginator->prepare()->bindToView($this->view)->bindToFilter($filter)->save();

		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);
		if($direction=='children'){
			/* LOAD PRODUCT PROPERTIES ONLY*/
			foreach($productDao->metaModel as $asSys=>$asApp){
				$select[] = 'child.'.$asSys . ' AS ' . $asApp;
				$headers[$asApp] = $asSys;
			}
			$stmt = $instanceDao->getChildren($productId, $select, $filterStr);
		}
		else{
			/* LOAD PRODUCT PROPERTIES ONLY*/
			foreach($productDao->metaModel as $asSys=>$asApp){
				$select[] = 'parent.'.$asSys . ' AS ' . $asApp;
				$headers[$asSys] = $asApp;
			}
			$stmt = $instanceDao->getParents($productId, $select, $filterStr);
		}

		$list = $stmt->fetchAll();
		$this->view->headers = $headers;
		$this->view->list = $list;
		$this->view->filter = $filterForm;
	}

	/**
	 *
	 */
	function getchildrenproductAction()
	{
		$spaceName = $this->params()->fromQuery('spacename');
		$documentId = $this->params()->fromQuery('documentid');
		$productId = $this->params()->fromQuery('productid');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$factory = DaoFactory::get($spaceName);

		$this->view->spacename = $spaceName;

		/* Load product of document */
		if(!$productId && $documentId){
			$productDao = $factory->getDao(Product\Version::$classId);
			$productId = $productDao->query(array('id'), $productDao->toSys('documentId').'=:documentId', array(':documentId'=>$documentId))->fetchColumn(0);
		}

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'pdm/productexplore/getchildrenproduct?documentid='.$documentId.'&spacename='.$spaceName;
		$this->basecamp()->save(
			$this->defaultSuccessForward,
			$this->ifFailedForward
		);

		$this->_list('children', $factory, $productId);

		$this->view->pageTitle = tra('Get Children');
		return $this->view;
	}

	/**
	 *
	 */
	function getchildrendocumentAction()
	{
		$spaceName = $this->params()->fromQuery('spacename');
		$documentId = $this->params()->fromQuery('documentid');
		$productId = $this->params()->fromQuery('productid');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$factory = DaoFactory::get($spaceName);

		$view = $this->view;
		$view->setTemplate('ged/document/relation/index');

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'pdm/productexplorer/getchildrendocument?documentid='.$documentId.'&spacename='.$spaceName;
		$this->basecamp()->save($this->defaultSuccessForward,$this->ifFailedForward,$view);

		/* Load product Id for document */
		if(!$productId && $documentId){
			$productDao = $factory->getDao(Product\Version::$classId);
			$productId = $productDao->query(array('id'), $productDao->toSys('documentId').'=:documentId', array(':documentId'=>$documentId))->fetchColumn(0);
		}

		$this->_getDocumentPdmRelation('children', $factory, $productId);

		$view->pageTitle = tra('Get Pdm Children Documents');
		$view->spacename = $spaceName;
		return $view;
	}

	/**
	 *
	 */
	function getparentdocumentAction()
	{
		$spaceName = $this->params()->fromQuery('spacename');
		$documentId = $this->params()->fromQuery('documentid');
		$productId = $this->params()->fromQuery('productid');
		$cancel = $this->params()->fromPost('cancel', false);
		$request = $this->getRequest();
		$factory = DaoFactory::get($spaceName);

		$view = $this->view;
		$view->setTemplate('ged/document/relation/index');

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'pdm/productexplorer/getparentdocument?documentid='.$documentId.'&spacename='.$spaceName;
		$this->basecamp()->save($this->defaultSuccessForward,$this->ifFailedForward,$view);

		/* Load product Id for document */
		if(!$productId && $documentId){
			$productDao = $factory->getDao(Product\Version::$classId);
			$productId = $productDao->query(array('id'), $productDao->toSys('documentId').'=:documentId', array(':documentId'=>$documentId))->fetchColumn(0);
		}

		$this->_getDocumentPdmRelation('parent', $factory, $productId);

		$view->pageTitle = tra('Get Pdm Parent Documents');
		$view->spacename = $spaceName;
		return $view;
	}

	/**
	 * @param string $direction
	 * @param DaoFactory $factory
	 * @param integer $productId
	 */
	protected function _getDocumentPdmRelation($direction='children', $factory, $productId)
	{
		/* VAR INIT*/
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$view = $this->view;
		$select = array();
		$headers = array();

		/* PAGINATOR INIT*/
		$paginator = new PaginatorForm();
		$paginator->setMaxLimit(1000);
		$paginator->setData($_POST);
		$paginator->setData($_GET);
		$paginator->prepare()->bindToView($view)->bindToFilter($filter)->save();

		/* DAO INIT*/
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);
		$documentDao = $factory->getDao(Document\Version::$classId);
		$linkDao = $factory->getDao(Document\PdmLink::$classId);

		/* get extended metadatas */
		$loader = new \Rbs\Extended\Loader($factory);
		$extendedMetadatas = $loader->load(Document\Version::$classId, $documentDao);
		$childMetamodel = array_merge($documentDao->metaModel, $documentDao->extendedModel);

		/* SELECT DOCUMENT PROPERTIES AS APP */
		foreach($childMetamodel as $asSys=>$asApp){
			$select[] = 'document.'.$asSys . ' AS ' . $asApp;
			$headers[$asApp] = $asSys;
		}
		$filter->select($select);

		if($direction=='children'){
			$stmt = $linkDao->getChildren($productId, $filter);
		}
		else{
			$stmt = $linkDao->getParents($productId, $filter);
		}

		$list = $stmt->fetchAll();
		$this->view->headers = $headers;
		$this->view->list = $list;
	}
}

