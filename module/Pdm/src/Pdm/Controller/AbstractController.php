<?php

namespace Pdm\Controller;

class AbstractController extends \Application\Controller\AbstractController
{
	/**
	 *
	 */
	public function init($view=null, $errorStack=null)
	{
		\Application\Controller\AbstractController::init();

		// Record url for page and Active the tab
		$layout = $this->layout();
		$tabs = \View\Helper\MainTabBar::get();
		$this->layout()->tabs = $tabs;
	}
}
