<?php
namespace Pdm\Input;

class PdmData implements \Iterator
{

	const IN_FORMAT_JSON = 'json';
	const IN_FORMAT_ARRAY = 'array';

	/**
	 *
	 * @param
	 *        	Loader
	 */
	function __construct($inputStr, $format = 'json')
	{
		if ( $format == self::IN_FORMAT_JSON ) {
			$pdmDatas = json_decode($inputStr, true);
			$feedbacks = $pdmDatas['feedback'];
			$errors = $pdmDatas['errors'];
			$pdmDatas = $pdmDatas['datas'];
		}

		if($pdmDatas){
			$this->datas = $pdmDatas;
			$this->keys = array_keys($pdmDatas);
		}
		else{
			$this->datas = array();
			$this->keys = array();
		}

		$this->feedbacks = $feedbacks;
		$this->errors = $errors;
	}

	/**
	 *
	 * @param array $datas
	 */
	protected function _getElement($datas, $key)
	{
		switch ($datas['fromType']) {
			case 'product':
			case 'catproduct':
				return new PdmData\Product($datas, $key);
				break;
			case 'part':
			case 'catpart':
				return new PdmData\Part($datas, $key);
				break;
			case 'drawing':
			case 'catdrawing':
				return new PdmData\Drawing($datas, $key);
				break;
			default:
				return new PdmData\Element($datas, $key);
				break;
		}
	}

	/**
	 * @param string $offset
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
		return (isset($this->datas[$offset]));
	}

	/**
	 * @param string $offset
	 * @return PdmData\Element
	 */
	public function offsetGet($offset)
	{
		$datas = $this->datas[$offset];
		$elemt = $this->_getElement($datas, $offset);
		unset($this->datas[$offset]);
		return $elemt;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see Iterator::current()
	 */
	public function current()
	{
		return $this->_current;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see Iterator::key()
	 */
	public function key()
	{
		return $this->_key;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see Iterator::next()
	 */
	public function next()
	{
		$this->_key = array_shift($this->keys);
		$datas = array_shift($this->datas);
		$this->_current = $this->_getElement($datas, $this->_key);
		return $this->_current;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see Iterator::rewind()
	 */
	public function rewind()
	{
		return;
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see Iterator::valid()
	 */
	public function valid()
	{
		if ( count($this->datas) > 0 ) {
			return true;
		}
		else {
			return false;
		}
	}

}

