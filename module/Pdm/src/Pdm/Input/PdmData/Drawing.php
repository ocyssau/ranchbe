<?php

namespace Pdm\Input\PdmData;

class Drawing extends Element
{
	/**
	 * @param array $datas
	 * @param string $key
	 */
	function __construct($datas, $key)
	{
		parent::__construct($datas, $key);
		$this->type = 'drawing';
	}
}
