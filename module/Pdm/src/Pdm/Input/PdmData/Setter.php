<?php

namespace Pdm\Input\PdmData;

use Rbplm\Dao\NotExistingException;
use Rbplm\Pdm\Product;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Rbplm\Geometric;

class Setter
{

	/**
	 *
	 * @param unknown $factory
	 */
	public function __construct($factory)
	{
		$this->factory = $factory;
	}

	/**
	 * Set product from input datas and attach to document
	 * Set attachments from input data to document
	 * Populate instance from input and set product children
	 *
	 * @param Document\Version $document
	 * @param Element $inputElemt
	 */
	public function setDocument(Document\Version $document, $inputElmt)
	{
		/* INIT VAR*/
		$factory = $this->factory;
		$type = $inputElmt->getType();

		/* INIT DAOS */
		$productDao = $factory->getDao(Product\Version::$classId);

		/* GET PROPERTIES */
		$properties = $inputElmt->getProperties();
		isset($properties['product']) ? $productData = $properties['product'] : $productData = array();
		isset($properties['physicalProperties']) ? $physicalProperties = $properties['physicalProperties'] : $physicalProperties = array();
		isset($properties['gravityCenter']) ? $gravityCenter = $properties['gravityCenter'] : $gravityCenter = array();
		isset($properties['inertiaCenter']) ? $inertiaCenter = $properties['inertiaCenter'] : $inertiaCenter = array();
		isset($properties['materials']) ? $materials = $properties['materials'] : $materials = array();
		isset($productData['name']) ? $productName = $productData['name'] : $productName = null;

		/* SET ATTACHMENTS */
		$file = $inputElmt->getPicture();
		if(is_file($file)){
			$this->addAttachmentToDocument($document, $file, Docfile\Role::PICTURE);
		}

		$file = $inputElmt->getThreedim();
		if(is_file($file)){
			$this->addAttachmentToDocument($document, $file, Docfile\Role::VISU);
		}

		$file = $inputElmt->getConvert();
		if(is_file($file)){
			$this->addAttachmentToDocument($document, $file, Docfile\Role::CONVERT);
		}

		/* LOAD PRODUCT */
		if(isset($document->product)){
			$product = $document->product;
		}
		else{
			/* Try to load the product of the document */
			try {
				$documentId = $document->getId();
				$product = new Product\Version();
				if($documentId){
					$productDao->load($product, $productDao->toSys('documentId').'=:documentId', array(':documentId'=>$documentId));
				}
				else{
					$productDao->loadFromNumber($product, $productName);
				}
			}
			catch( NotExistingException $e ) { /* Product is not not existing, create a new*/
				$product = Product\Version::init();
			}
		}

		/* Update product data with input datas */
		$product->setDocument($document);
		$product->hydrate($productData);
		$product->spaceName = strtolower($factory->getName());
		$product->type = $type;
		$document->product = $product;

		if ( $materials ) {
			/*@todo: delete previous materials before add*/
			foreach( $materials as $material ) {
				$product->addMaterial($material['name']);
			}
		}

		$physicals = $product->getPhysicalProperties();
		if ( $physicalProperties ) {
			$physicals->hydrate($physicalProperties);
		}
		if ( $gravityCenter ) {
			$gravityCenterPoint = new Geometric\Point($gravityCenter);
			$physicals->setGravityCenter($gravityCenterPoint);
		}
		if ( $inertiaCenter ) {
			$inertiaCenterPoint = new Geometric\Point($inertiaCenter);
			$physicals->setInertiaCenter($inertiaCenter);
		}
		$product->setPhysicalProperties($physicals);

		if($type == 'product'){
			$children = $inputElmt->getChildren();
			if ( $children ) {
				$this->structureBuild($product, $children);
			}
		}

		return $product;
	}

	/**
	 * @param Product $parent
	 * @param array $children
	 */
	protected function structureBuild($parent, $children)
	{
		/* INIT DAOS */
		$factory = $this->factory;
		$instanceDao = $factory->getDao(Product\Instance::$classId);
		$productDao = $factory->getDao(Product\Version::$classId);

		foreach( $children as $child ) {
			$ofProductName = $child['ofProductName'];

			/* Try to load Instance */
			try {
				$instance = new Product\Instance();
				$instanceDao->loadFromNameAndParent($instance, $child['name'], $parent->getNumber());
			}
			catch( NotExistingException $e ) {
				$instance = Product\Instance::init();
			}

			/* Hydrate instance */
			$instance->hydrate($child);
			$instance->setParent($parent);

			/* Try to load the product of the instance */
			try {
				$product = new Product\Version();
				$productDao->loadFromNumber($product, $ofProductName);
			}
			catch( NotExistingException $e ) {
				$product = Product\Version::init($ofProductName);
			}
			$instance->setOfProduct($product);

			/* set number as name */
			$instance->setNumber($instance->getName());

			if($child['children']){
				$this->structureBuild($product, $child['children']);
			}
		}
	}

	/**
	 *
	 * @param array $data
	 */
	public function addAttachmentToDocument($document, $file, $role)
	{
		$factory = $this->factory;

		$docfile = Docfile\Version::init();
		$docfile->setMainrole($role);
		$fsdata = new \Rbplm\Sys\Fsdata($file);
		$docfile->fsdata = $fsdata;
		$docfile->setName(uniqid());
		$document->addDocfile($docfile);
		return $this;
	}

}
