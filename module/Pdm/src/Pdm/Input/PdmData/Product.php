<?php

namespace Pdm\Input\PdmData;

class Product extends Element
{
	/**
	 * @param array $datas
	 * @param string $key
	 */
	function __construct($datas, $key)
	{
		parent::__construct($datas, $key);
		$this->type = 'product';
	}

	/**
	 *
	 */
	public function getChildren()
	{
		if(isset($this->datas['children'])){
			return $this->datas['children'];
		}
	}
}

