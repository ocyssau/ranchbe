<?php

namespace Pdm\Input\PdmData;

class Element
{
	protected $datas;
	protected $key;
	protected $workingDir;

	/**
	 * @param Loader
	 */
	function __construct($datas, $key)
	{
		$this->datas = $datas;
		$this->key = $key;
		$this->type = $datas['fromtype'];
	}

	/**
	 *
	 * @param string $path
	 */
	public function setWorkingDir($path)
	{
		$this->workingDir = rtrim($path, '/');
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getFeedback()
	{
		if(isset($this->datas['feedback'])){
			return $this->datas['feedback'];
		}
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		if(isset($this->datas['errors'])){
			return $this->datas['errors'];
		}
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @return array
	 */
	public function getProperties()
	{
		return $this->datas['properties'];
	}

	/**
	 * @return string
	 */
	public function getPicture()
	{
		if(isset($this->datas['picture'])){
			$file = $this->workingDir.'/'.trim(basename($this->datas['picture']['file']), '/');
		}
		return $file;
	}

	/**
	 * @return string
	 */
	public function getThreedim()
	{
		if(isset($this->datas['3d'])){
			$file = $this->workingDir.'/'.trim(basename($this->datas['3d']['file']), '/');
		}
		return $file;
	}

	/**
	 * @return string
	 */
	public function getConvert()
	{
		if(isset($this->datas['convert'])){
			$file = $this->workingDir.'/'.trim(basename($this->datas['convert']['file']), '/');
		}
		return $file;
	}
}

