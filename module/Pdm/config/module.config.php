<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'pdm' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/pdm',
					'defaults' => array(
						'__NAMESPACE__' => 'Pdm\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Pdm\Controller\Index' => 'Pdm\Controller\IndexController',
			'Pdm\Controller\Product'=>'Pdm\Controller\ProductController',
			'Pdm\Controller\ProductVersion'=>'Pdm\Controller\ProductVersionController',
			'Pdm\Controller\ProductInstance'=>'Pdm\Controller\ProductInstanceController',
			'Pdm\Controller\ProductContext'=>'Pdm\Controller\ProductContextController',
			'Pdm\Controller\ProductConcept'=>'Pdm\Controller\ProductConceptController',
			'Pdm\Controller\ProductExplorer'=>'Pdm\Controller\ProductExplorerController',
			),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Pdm'=>__DIR__ . '/../view',
		),
	),
);
