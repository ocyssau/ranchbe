<?php
namespace ControllerTest\Document;

use Controller\Document\Smartstore as SmartstoreController;
use Ranchbe;

class SmartstoreTest extends \Rbplm\Test\Test
{
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$factory = \Rbs\Space\Factory::get('Workitem');
		$config = 	Ranchbe::get()->getConfig();
		//var_dump($config);die;
		rbinit_web();
		rbinit_authservice($config);
		rbinit_currentUser($config);
		rbinit_view($config);
		rbinit_errorStack($config);
		rbinit_filesystem($config);

		$authService = \Ranchbe::getAuthservice();
		$storage = $authService->getStorage();

		/* check authentication...*/
		$authService->getAdapter()
			->setIdentity('admin')
			->setCredential('admin00');
		$result = $authService->authenticate();
		$identity = $result->getIdentity();
		$storage->write($identity);

		$user = new \Rbplm\People\User();
		$factory->getDao($user)->loadFromLogin($user, 'admin');
		\Rbplm\People\CurrentUser::set($user);

		$this->controller = new SmartstoreController();
		$this->controller->init();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/**
	 *
	 */
	public function Test_ProductVersionDao()
	{
		$_POST = array(
			'spacename'=>"workitem",
			'containerId'=>90,
			'loop'=>1,
			'actions'=>array('create_doc'),
			'file'=>array('document200.doc'),
			'number'=>array('document200'),
			'version'=>array(1),
			'description'=>array("FOR TEST"),
			'categoryId'=>array(),
			'save'=>'save',
			'pdmdata'=>array(),
		);
		$_REQUEST = $_POST;

		$this->controller->storeAction();
	}

}
