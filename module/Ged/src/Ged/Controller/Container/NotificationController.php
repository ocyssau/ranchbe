<?php
namespace Ged\Controller\Container;

use Application\Controller\AbstractController;
use Rbs\Notification;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People;

/**
 *
 *
 */
class NotificationController extends AbstractController
{

	public $pageId = 'ged_notification';
	public $defaultSuccessForward = 'ged/notification/index';
	public $defaultFailedForward = 'ged/notification/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$factory = DaoFactory::get();
		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$dao = $factory->getDao(Notification\Notification::$classId);
		$list = $factory->getList(Notification\Notification::$classId);
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->load($filter);
		$view->list = $list;

		$view->PageTitle = tra('My notifications');
		return $this->view;

		/*
		if ( $list ) foreach( $list as $notification ) {
			if ( !$notification['condition'] ) {
				$i++;
				continue;
			}
			$condition = unserialize($notification['condition']);
			$tests = $condition->getTests();
			foreach( $tests as $test ) {
				$list[$i]['test'][] = $test;
			}
			$i++;
		}
		*/
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;

		isset($_REQUEST['spacename']) ? $spacename=$_REQUEST['spacename'] : $spacename=null;
		isset($_REQUEST['containerid']) ? $containerId=$_REQUEST['containerid'] : $containerId=null;
		isset($_REQUEST['referenceuid']) ? $referenceUid=$_REQUEST['referenceuid'] : $referenceUid=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->successForward();
		}

		$this->checkRight('edit', $this->areaId, 0);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Notification\Notification::$classId);
		$owner = People\CurrentUser::get();

		if($containerId){
			$container = new \Rbplm\Org\Workitem();
			$factory->getDao($container->cid)->loadFromId($container, $containerId);
			$referenceUid = $container->getUid();
		}

		try {
			$notification = new Notification\Notification();
			$dao->loadFromReferenceUidAndOwnerUid($notification, $referenceUid, $owner->getLogin());
			$view->pageTitle = 'Edit Notification '.$notification->getName();
		}
		catch (\Rbplm\Dao\NotExistingException $e) {
			$notification = Notification\Notification::init();
			$notification->setName($referenceUid.'-'.$owner->getUid());
			$notification->referenceUid = $referenceUid;
			$notification->setOwner($owner);
			$view->pageTitle = 'Create Notification';
		}

		$form = new \Ged\Form\Notification\EditForm($view);
		$form->bind($notification);

		/* Try to validate the form */
		if ($validate){
			$form->setData($_POST);
			if ( $form->isValid() ) {
				if(!isset($_POST['events'])){
					$notification->setEvents(array());
				}
				$dao->save($notification);
				return $this->successForward();
			}
		}

		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}
}
