<?php
namespace Ged\Controller\Container;

class MockupController extends ManagerController
{
	public $pageId = 'container_mockup';
	public $defaultSuccessForward = 'ged/mockup/index';
	public $defaultFailedForward = 'ged/mockup/index';
	public $spaceName = 'mockup';
}
