<?php
namespace Ged\Controller\Container;

class BookshopController extends ManagerController
{
	public $pageId = 'container_bookshop';
	public $defaultSuccessForward = 'ged/bookshop/index';
	public $defaultFailedForward = 'ged/bookshop/index';
	public $spaceName = 'bookshop';
}
