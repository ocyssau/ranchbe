<?php
namespace Ged\Controller\Container;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Org\Container\Favorite;
use Rbplm\Org\Workitem;
use Rbplm\People;

abstract class ManagerController extends AbstractController
{
	public $spaceName = 'container';

	/**
	 * Run controller
	 */
	public function init()
	{
		parent::init();

		$spaceName = $this->spaceName;
		$this->factory = DaoFactory::get($spaceName);
		$this->view->spacename = $spaceName;
		$this->areaId = 1;

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab($spaceName)->activate();
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$this->checkRight('read', $this->areaId, 0, $this->ifSuccessForward, array(
			'spacename' => $this->spaceName,
		));

		$factory = $this->factory;
		$spaceName = $this->spaceName;
		$currentUser = People\CurrentUser::get();
		$areaId = $factory->getId();
		$view = $this->view;
		$view->setTemplate('ged/container/manager/index');
		$this->getLayoutselector()->clear($this);
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$container = $factory->getModel(Workitem::$classId);
		$dao = $factory->getDao($container->cid);
		$list = $factory->getList($container->cid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setData($_POST)->setData($_GET);
		$paginator->setMaxLimit($list->countAll);

		$filterForm = new \Ged\Form\Container\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'containerfilterform');
		$filterForm->load();

		$count = $list->countAll($filter);
		$count = (2*$count);
		$paginator->setMaxLimit($count);

		/* Select main table */
		$select = array(
			$dao->toSys('id') . ' as id',
			$dao->toSys('uid') . ' as uid',
			$dao->toSys('name') . ' as name',
			$dao->toSys('number') . ' as number',
			$dao->toSys('description') . ' as description',
			'object_class as cid',
			'"" as aliasOfId',
			'"" as aliasOfName',
			$dao->toSys('versionId') . ' as versionId',
			$dao->toSys('status') . ' as status',
			'project_id as parentId',
			'default_process_id as processId',
			'file_only as file_only',
			'default_file_path as default_file_path',
			'open_date as created',
			'open_by as createById',
			'forseen_close_date as forseenCloseDate',
			'close_date as closed',
			'close_by as closeById'
		);

		$loader = new Extended\Loader($factory);
		$extended = $loader->load($container->cid, $dao);
		if(is_array($dao->extendedModel)){
			foreach($dao->extendedModel as $asSys=>$asApp){
				$select[] = $asSys . ' as '. $asApp;
			}
		}

		/* select in alias table */
		$select2 = $select;
		$select2[0]='alias.alias_id as id';
		$select2[1]='alias.'.$select[1]; /*uid*/
		$select2[2]='alias.'.$select[2]; /*name*/
		$select2[3]='alias.'.$select[3]; /*number*/
		$select2[4]='alias.'.$select[4]; /*description*/
		$select2[5]='alias.'.$select[5]; /*cid*/
		$select2[6]='alias.workitem_id as aliasOfId';
		$select2[7]='wi.workitem_number as aliasOfName';

		$table = $factory->getTable($container->cid);
		$aliasTable = $factory->getTable(\Rbs\Org\Container\Alias::$classId);

		$filterForm->setExtended($optionalFields,$view);
		$filterForm->bindToFilter($filter)->save();
		$paginator->prepare()->bindToView($view)->bindToFilter($filter)->save();

		$sql1 = "SELECT ".implode(',',$select)." FROM $table";
		$idSysName = $dao->toSys('id');
		$sql2 = "SELECT ".implode(',',$select2)." FROM $aliasTable AS alias JOIN $table as wi ON alias.$idSysName=wi.$idSysName";
		$sql = "SELECT * FROM ($sql1 UNION $sql2) AS uni";
		$sql .= " WHERE ".$filter->__toString();

		$list->loadFromSql($sql);

		/*
		$reducelist = array();
		if(is_array($list)){
		foreach($list as $element ){
		$this->container->init( $element[$this->container->GetFieldName('id')] );
		if( check_ticket( 'container_document_get' , $this->container->AREA_ID , false) ){
		$reducelist[] = $element;
		}
		}
		}
		$this->view->assign('list', $reducelist);
		unset($list);
		*/

		/* list of favorites */
		$favorites = $factory->getDao(Favorite::$classId);
		$favoritesList = $favorites->getFromUserId($currentUser->getId());
		$this->view->favorites = $favoritesList;
		$view->list = $list->toArray();
		$view->filter = $filterForm;
		$view->extended = $extended;
		$view->randWindowName = 'container_'.uniqid();
		$view->pageTitle = tra($spaceName . ' manager');
		return $view;
	}
}