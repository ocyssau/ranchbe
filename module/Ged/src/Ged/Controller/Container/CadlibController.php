<?php
namespace Ged\Controller\Container;

class CadlibController extends ManagerController
{
	public $pageId = 'container_cadlib';
	public $defaultSuccessForward = 'ged/cadlib/index';
	public $defaultFailedForward = 'ged/cadlib/index';
	public $spaceName = 'cadlib';
}
