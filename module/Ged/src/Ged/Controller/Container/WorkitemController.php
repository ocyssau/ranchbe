<?php
namespace Ged\Controller\Container;

class WorkitemController extends ManagerController
{
	public $pageId = 'container_workitem';
	public $defaultSuccessForward = 'ged/workitem/index';
	public $defaultFailedForward = 'ged/workitem/index';
	public $spaceName = 'workitem';
}
