<?php

namespace Ged\Controller;

use Application\Controller\AbstractController;

class IndexController extends AbstractController
{
	protected $pageId = 'ged_index'; //(string)
	protected $ifSuccessForward = 'rbdocument/index/index';
	protected $ifFailedForward = 'rbdocument/index/index';

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		return $view;
	}
}

