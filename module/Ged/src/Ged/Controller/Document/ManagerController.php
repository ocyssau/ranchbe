<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Dao\Filter\Op;
use Rbs\Sys\Session;
use Rbs\Extended;

class ManagerController extends AbstractController
{
	protected $areaId = 10;
	public $pageId = 'document_manager'; //(string)
	public $defaultSuccessForward = 'rbdocument/manager/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;
		isset($_REQUEST['pageid']) ? $pageId = $_REQUEST['pageid'] : $pageId=null;

		if(!$containerId){
			$containerId = Session::get()->containerId;
			$spaceName = Session::get()->spaceName;
		}
		if(!$spaceName){
			$spaceName = Session::get()->spaceName;
		}

		$this->spaceName = $spaceName;
		$this->containerId = $containerId;

		//Assign name to particular fields
		$this->view->containerid = $containerId;
		$this->view->spacename = $spaceName;
		$this->view->sameurl_elements = array('space', 'containerid');

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->basecamp()->setForward($this);
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/manager/index');

		$this->checkRight('read', $this->areaId, $this->containerId, $this->ifSuccessForward, array(
			'spacename' => $this->spaceName,
			'containerid' => $this->containerId
		));

		$this->getLayoutselector()->clear($this);

		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);

		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Document\FilterForm($factory, $this->pageId, $this->containerId);
		$filterForm->setAttribute('id', 'documentfilterform');
		$filterForm->load();
		$displayThumb = $filterForm->getValue('displayThumbs');
		$filterForm->bindToFilter($filter)->save();

		$list = $this->getList($filter, $factory);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setData($_POST)->setData($_GET);
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()->bindToView($view)->bindToFilter($filter)->save();
		$list->load($filter,$filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Get Extended Properties */
		$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Property::$classId);
		$extended = $lnkDao->getChildren($this->containerId, array(
			'child.field_name AS name, child.appName, child.label'
		))->fetchAll();
		$view->extended = $extended;

		$session = Session::get();
		if ( $session->fixedDocument ) {
			$documentId = $session->fixedDocument;
			$dao = $factory->getDao(DocumentVersion::$classId);
			$fixedDocument = $dao->loadFromId(new DocumentVersion(), $documentId);
			$view->fixedDocument = $fixedDocument;
		}

		$view->pageTitle = tra('Documents Manager');
		return $view;
	}

	/**
	 *
	 * @param unknown_type $filter
	 * @param unknown_type $factory
	 * @param unknown_type $filterForm
	 */
	public function getList($filter, $factory, $filterForm=null)
	{
		$dao = $factory->getDao(DocumentVersion::$classId);

		$list = $factory->getList(DocumentVersion::$classId);
		$select = array();
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[] = $asSys.' as '. $asApp;
		}

		/* Get Extended Properties */
		$loader = new Extended\Loader($factory);
		$extendedMetadatas = $loader->load(DocumentVersion::$classId, $dao);
		if(is_array($dao->extendedModel)){
			foreach($dao->extendedModel as $asSys=>$asApp){
				$select[] = $asSys.' as '. $asApp;
			}
		}

		$filter->select($select);
		if($filterForm){
			$filterForm->setExtended($extendedMetadatas);
		}

		$filter->andfind( $this->containerId, $dao->toSys('parentId'), Op::OP_EQUAL );
		return $list;
	}

}

