<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Ged\Document\Basket as Basket;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\People\CurrentUser;
use Rbplm\Dao\NotExistingException;

/**
 *
 */
class BasketController extends AbstractController
{
	public $pageId = 'document_basket';
	public $defaultSuccessForward = 'rbdocument/basket/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		isset($_REQUEST['containerid']) ? $containerId = $_REQUEST['containerid'] : null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : $spaceName=null;

		$this->spaceName = $spaceName;
		$this->view->spacename = $spaceName;
		$this->checkRight('read', $this->areaId, 0);

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;

		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 * {@inheritDoc}
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/basket/index');

		$defaultFwd = $this->defaultSuccessForward.'?spacename='.$this->spaceName;
		$this->basecamp()->save($defaultFwd, $this->ifFailedForward, $view);

		$factory = DaoFactory::get($this->spaceName);
		try{
			$docBasket = new Basket(CurrentUser::get(), $factory);
			$list = $docBasket->get();
			$view->list = $list;
			$view->pageTitle = tra('My Documents Basket');
		}
		catch(NotExistingException $e){
			$this->errorStack()->error('Basket is empty');
			return $this->errorForward(array('spacename'=>$this->spaceName));
		}
		return $view;
	}

} //End of class
