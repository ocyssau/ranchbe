<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbs\Extended;
use Rbplm\Ged\Document;
use Rbplm\Ged\Docfile;
use Discussion\Model\Comment;
use Rbplm\Dao\Filter\Op;

class DetailController extends AbstractController
{

	/**
	 *
	 * @var integer
	 */
	protected $areaId = 10;

	public $pageId = 'document_detail';

	public $defaultSuccessForward = 'rbdocument/detail/index';

	public $defaultFailedForward = 'rbdocument/detail/index';

	/**
	 */
	public function init()
	{
		parent::init();

		$spaceName = null;
		$documentId = null;

		$input = $_REQUEST;
		isset($input['space']) ? $spaceName = $input['space'] : null;
		isset($input['spacename']) ? $spaceName = $input['spacename'] : null;
		isset($input['documentid']) ? $documentId = $input['documentid'] : null;

		$this->spaceName = $spaceName;
		$this->documentId = $documentId;

		$this->view->spacename = $spaceName;
		$this->view->documentid = $documentId;

		// Record url for page and Active the tab
		// $tabs = \View\Helper\MainTabBar::get($this->view);
		// $tabs->getTab('myspace')->activate('container');
		// $this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
		$this->basecamp()->setForward($this);
	}

	/**
	 */
	public function indexAction()
	{
		isset($_REQUEST['tab']) ? $tab = $_REQUEST['tab'] : $tab = null;

		$spaceName = $this->spaceName;
		$documentId = $this->documentId;
		$return = array(
			'errors',
			'feedbacks'
		);
		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Document\Version::$classId);

		/* Load extended properties in Dao */
		$loader = new Extended\Loader($factory);
		$loader->loadFromDocumentId($documentId, $dao);

		/* @var \Rbplm\Ged\Document\Version $document */
		$document = $dao->loadFromId(new Document\Version(), $documentId);
		$parent = new \Rbplm\Org\Workitem();
		$factory->getDao($parent->cid)->loadFromId($parent, $document->parentId);
		$document->setParent($parent);

		$this->checkRight('read', $this->areaId, $document->getParent(true), $this->ifSuccessForward, array(
			'spacename' => $this->spaceName,
			'documentid' => $this->documentId
		));

		$extended = $dao->extended;
		$this->view->extended = $extended;
		$this->view->document = $document;

		if ( $tab == 'files' ) {
			$list = $this->_getAssocFiles($document);
			$this->view->associatedfiles = $list->toArray();
			$this->view->setTemplate('document/detail/associatedfiles.phtml');
		}
		elseif ( $tab == 'product' ) {
			$product = $this->_getProduct($document);
			if ( $product ) {
				$this->view->product = $list->getArrayCopy();
				$this->view->setTemplate('document/detail/product.phtml');
			}
		}
		elseif ( $tab == 'children' ) {
			$stmt = $this->_getChildren($document);
			$this->view->doclink_list = $list->fetchAll();
			$this->view->setTemplate('document/detail/relation.phtml');
		}
		elseif ( $tab == 'docparent' ) {
			$list = $this->_getParents($document);
			$this->view->father_documents = $list->toArray();
			$this->view->setTemplate('document/detail/parent.phtml');
		}
		elseif ( $tab == '' ) {
			$this->view->associatedfiles = $this->_getAssocFiles($document)->toArray();
			$this->view->children = $this->_getChildren($document)->fetchAll();
			$this->view->parents = $this->_getParents($document)->fetchAll();
			$this->view->discussion = $this->_getDiscussion($document);

			$product = $this->_getProduct($document);
			if ( $product ) {
				$this->view->product = $product->getArrayCopy();
				$this->view->pdmchildren = $this->_getPdmChildren($product)->fetchAll();
			}
			$this->view->setTemplate('ged/document/detail/index.phtml');
		}

		return $this->view;
	}

	/**
	 * Associated files
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \Rbplm\Dao\ListInterface
	 */
	protected function _getAssocFiles($document)
	{
		// get all file associated to the selected document
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$list = $factory->getList(Docfile\Version::$classId);
		$dao = $factory->getDao(Docfile\Version::$classId);

		foreach( $dao->metaModel as $asSys => $asApp ) {
			$select[] = $asSys . ' as ' . $asApp;
		}
		$list->select($select);

		$list->load($dao->toSys('parentId') . '=:parentId', array(
			':parentId' => $documentId
		));

		// Build roles list for select
		$roles = \Rbplm\Ged\Docfile\Role::toArray();
		$this->view->roles = $roles;

		return $list;
	}

	/**
	 * Relationships
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \PDOStatement
	 */
	protected function _getChildren($document)
	{
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$linkDao = $factory->getDao(Document\Link::$classId);
		$docDao = $factory->getDao($document->cid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* BUILD SELECT */
		foreach( $docDao->metaModel as $asSys => $asApp ) {
			$select[] = 'jt.' . $asSys . ' AS ' . $asApp;
		}
		foreach( $linkDao->metaModel as $asSys => $asApp ) {
			$select[] = 'lt.' . $asSys . ' AS link_' . $asApp;
		}
		$filter->select($select);

		$stmt = $linkDao->getChildren($documentId, $filter);
		return $stmt;
	}

	/**
	 * Relationships
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \PDOStatement
	 */
	protected function _getParents($document)
	{
		$documentId = $document->getId();
		$factory = $document->dao->factory;
		$linkDao = $factory->getDao(Document\Link::$classId);
		$docDao = $factory->getDao($document->cid);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* BUILD SELECT */
		foreach( $docDao->metaModel as $asSys => $asApp ) {
			$select[] = 'jt.' . $asSys . ' AS ' . $asApp;
		}
		foreach( $linkDao->metaModel as $asSys => $asApp ) {
			$select[] = 'lt.' . $asSys . ' AS link_' . $asApp;
		}
		$filter->select($select);

		$stmt = $linkDao->getParents($documentId, $filter);
		return $stmt;
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 * @return \Rbplm\Pdm\Product\Version|NULL
	 */
	protected function _getProduct($document)
	{
		$documentId = $document->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = $document->dao->factory;

		/** @var \Rbs\Pdm\Product\VersionDao $dao */
		$dao = $factory->getDao(\Rbplm\Pdm\Product\Version::$classId);

		$filter = $dao->toSys('documentId') . "=:documentId";
		$bind = array(
			':documentId' => $documentId
		);
		$product = new \Rbplm\Pdm\Product\Version();

		try {
			$dao->load($product, $filter, $bind);
			return $product;
		}
		catch( \Exception $e ) {
			return null;
		}
	}

	/**
	 *
	 * @param \Rbplm\Ged\Document\Version $document
	 */
	protected function _getDiscussion($document)
	{
		/** @var \Rbs\Space\Factory $factory */
		$factory = $document->dao->factory;
		$discussionUid = $document->getId();

		$list = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussionUid', Op::EQUAL);
		$list->load($filter->__toString());
		return $list;
	}

	/**
	 *
	 * @param \Rbplm\Pdm\Product\Version $product
	 * @return \PDOStatement
	 */
	protected function _getPdmChildren($product)
	{
		$productId = $product->getId();

		/** @var \Rbs\Space\Factory $factory */
		$factory = $product->dao->factory;

		/** @var \Rbs\Pdm\Product\InstanceDao $dao */
		$dao = $factory->getDao(\Rbplm\Pdm\Product\Instance::$classId);

		try {
			/** @var \PDOStatement $stmt */
			$stmt = $dao->getFullChildren($productId);
			return $stmt;
		}
		catch( \Exception $e ) {
			throw $e;
			return null;
		}
	}

	/**
	 */
	public function _getDetail()
	{}
} //End of class

