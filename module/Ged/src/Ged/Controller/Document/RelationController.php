<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Document;
use Rbs\Sys\Session;

class RelationController extends AbstractController
{
	public $pageId = 'document_relation';
	public $defaultSuccessForward = 'rbdocument/relation/index';
	public $defaultFailedForward = 'rbdocument/manager/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		$spaceName = null;

		(isset($_REQUEST['pageid'])) ? $pageId = $_REQUEST['pageid'] : $pageId=$this->pageId;
		(isset($_REQUEST['basecamp'])) ? $basecamp = $_REQUEST['basecamp'] : $basecamp=null;
		(isset($_REQUEST['spacename'])) ? $spaceName = $_REQUEST['spacename'] : null;
		(isset($_REQUEST['documentid'])) ? $documentId = $_REQUEST['documentid'] : null;

		$this->spaceName = $spaceName;
		$this->documentId = $documentId;

		$this->view->spacename = $spaceName;
		$this->view->documentid = $documentId;

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('myspace')->activate('container');
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
	}

	/**
	 */
	function getchildrenAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/relation/index');

		$this->checkRight('read', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'containerid'=>$this->containerId
			));

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'rbdocument/relation/getchildren?documentid='.$this->documentId.'&spacename='.$this->spaceName;
		$this->basecamp()->save($this->defaultSuccessForward,$this->ifFailedForward,$view);

		$this->_list('children');

		$view->pageTitle = tra('Get Document Relations Children');
		return $view;
	}

	/**
	 */
	function getparentAction()
	{
		$view = $this->view;
		$view->setTemplate('ged/document/relation/index');

		$this->checkRight('read', $this->areaId, $this->containerId,
			$this->ifSuccessForward,
			array(
				'spacename'=>$this->spaceName,
				'containerid'=>$this->containerId
			));

		/* To Saved in basecamp in postDispatch */
		$this->defaultSuccessForward = 'rbdocument/relation/getparent?documentid='.$this->documentId.'&spacename='.$this->spaceName;
		$this->basecamp()->save($this->defaultSuccessForward,$this->ifFailedForward,$view);

		$this->_list('parent');

		$view->pageTitle = tra('Get Document Relations Parents');
		return $view;
	}

	/**
	 */
	function _list($direction='children')
	{
		$spaceName = $this->spaceName;
		$factory = DaoFactory::get($spaceName);
		$filter = new \Rbs\Dao\Sier\Filter('', false);

		/* DAO INIT*/
		$linkDao = $factory->getDao(Document\Link::$classId);
		$documentDao = $factory->getDao(Document\Version::$classId);

		/* get extended metadatas */
		$loader = new \Rbs\Extended\Loader($factory);
		$extendedMetadatas = $loader->load(Document\Version::$classId, $documentDao);
		$childMetamodel = array_merge($documentDao->metaModel, $documentDao->extendedModel);

		/* BUILD SELECT */
		if($childMetamodel){
			foreach($childMetamodel as $asSys=>$asApp){
				$select[] = 'jt.'.$asSys . ' AS ' . $asApp;
				$headers[$asApp] = $asSys;
			}
		}
		foreach($linkDao->metaModel as $asSys=>$asApp){
			$select[] = 'lt.'.$asSys . ' AS link_' . $asApp;
		}
		$filter->select($select);

		if($direction=='children'){
			$stmt = $linkDao->getChildren($this->documentId, $filter);
		}
		else{
			$stmt = $linkDao->getParents($this->documentId, $filter);
		}

		$list = $stmt->fetchAll();
		$this->view->list = $list;

		//Get Extended Properties
		$this->view->extended = $extendedMetadatas;

		$session = Session::get();
		if ( $session->fixedDocument ) {
			$documentId = $session->fixedDocument;
			$documentDao = $factory->getDao(Document\Version::$classId);
			$fixedDocument = $documentDao->loadFromId(new Document\Version(), $documentId);
			$this->view->fixedDocument = $fixedDocument;
		}
	}
} //End of class
