<?php
namespace Ged\Controller\Document;

use Application\Controller\AbstractController;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Ged\Docfile;
use Rbplm\Dao\Filter\Op;

class ViewerController extends AbstractController
{
	/**
	 *
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * Display instance of all process
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function threedAction()
	{
		$view = $this->view;
		isset($_REQUEST['documentid']) ? $documentId = $_REQUEST['documentid'] : $documentId = null;
		isset($_REQUEST['spacename']) ? $spaceName = $_REQUEST['spacename'] : null;

		if(!$documentId || !$spaceName) return $view;
		$factory = DaoFactory::get($spaceName);

		/** @var \Rbs\Ged\Docfile\VersionDao $dfDao */
		$dfDao = $factory->getDao(Docfile\Version::$classId);

		/** @var \Rbs\Dao\Sier\Filter $filter */
		$filter = $factory->getFilter(Docfile\Version::$classId);

		try {
			$threedDf = new Docfile\Version();
			$filter->andfind(Docfile\Role::VISU, $dfDao->toSys('mainrole'), Op::EQUAL);
			$filter->andfind($documentId, $dfDao->toSys('parentId'), Op::EQUAL);
			$dfDao->load($threedDf, $filter);
			$view->threedDf = $threedDf;
		}
		catch (\Exception $e) {
			$view->threedDf = null;
		}
		return $view;
	}
}

