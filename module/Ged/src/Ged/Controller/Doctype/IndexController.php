<?php
namespace Ged\Controller\Doctype;

use Application\Controller\AbstractController;
use Rbplm\Ged\Doctype;
use Rbs\Space\Factory as DaoFactory;

class IndexController extends AbstractController
{
	public $pageId = 'doctype_manage';
	public $defaultSuccessForward = 'ged/doctype/index';
	public $defaultFailedForward = 'ged/doctype/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();
		$this->areaId = 1;
		$this->basecamp()->setForward($this);

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('Admin')->activate('doctype');
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$this->checkRight('read', $this->areaId, 0);
		$view = $this->view;
		$view->setTemplate('ged/doctype/index/index');
		$this->basecamp()->save($this->defaultSuccessForward, $this->defaultFailedForward, $view);

		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Ged\Form\Doctype\FilterForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'doctypefilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list = $factory->getList(Doctype::$classId);
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[] = "`$asSys` AS `$asApp`";
		}
		$filter->select($select);
		$list->countAll = $list->countAll($filter);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->limit = 50;
		$paginator->setData($_POST)->setData($_GET);
		$paginator->setMaxLimit($list->countAll);
		$paginator->prepare()->bindToView($view)->bindToFilter($filter)->save();

		$list->load($filter, $filterForm->bind);

		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* Display the template */
		$view->pageTitle = 'Doctypes Manager';
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		isset($_REQUEST['checked']) ? $doctypeIds=$_REQUEST['checked'] : $doctypeIds=array();
		$spaceName = $this->spaceName;

		$this->checkRight('delete', $this->areaId, 0);

		$factory = DaoFactory::get($spaceName);
		$dao = $factory->getDao(Category::$classId);

		foreach ($doctypeIds as $doctypeId){
			try{
				$dao->deleteFromId($doctypeId);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
				continue;
			}
		}
		$this->successForward();
	}

	/**
	 *
	 */
	public function editAction()
	{
		$view = $this->view;

		isset($_REQUEST['doctypeid']) ? $doctypeId=$_REQUEST['doctypeid'] : $doctypeId=null;
		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->successForward();
		}

		$this->checkRight('edit', $this->areaId, 0);

		$doctype = new Doctype();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);
		$dao->loadFromId($doctype, $doctypeId);

		$form = new \Ged\Form\Doctype\EditForm($view);
		$form->bind($doctype);

		/* Try to validate the form */
		if ($validate){
			$form->setData($_POST);
			if ( $form->isValid() ) {
				$dao->save($doctype);
				return $this->successForward();
			}
		}

		$view->pageTitle = 'Edit Doctype '.$doctype->getName();
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function createAction()
	{
		$view = $this->view;

		isset($_REQUEST['cancel']) ? $cancel=$_REQUEST['cancel'] : $cancel=false;
		isset($_REQUEST['validate']) ? $validate=$_REQUEST['validate'] : $validate=false;

		if($cancel){
			return $this->successForward();
		}

		$this->checkRight('create', $this->areaId, 0);

		$doctype = Doctype::init();
		$factory = DaoFactory::get();
		$dao = $factory->getDao(Doctype::$classId);

		$form = new \Ged\Form\Doctype\EditForm($view);
		$form->bind($doctype);

		/* Try to validate the form */
		if ($validate) {
			$form->setData($_POST);
			if ( $form->isValid() ) {
				$dao->save($doctype);
				return $this->successForward();
			}
		}
		$view->pageTitle = 'Create Category';
		$view->form = $form;
		$view->setTemplate($form->template);
		return $view;
	}

	/**
	 *
	 */
	public function csvimport()
	{
		$this->checkRight('admin', $this->areaId, 0);

		$this->doctype->ImportDoctypesCsv($_FILES['csvlist']['tmp_name'] , $_REQUEST['overwrite']);
		return $this->displayAction();
	}

	/**
	 *
	 */
	public function buildiconsAction()
	{
		$this->checkRight('admin', $this->areaId, 0);

		$factory = DaoFactory::get();
		$list = $factory->getList(Doctype::$classId);
		$dao = $factory->getDao(Doctype::$classId);

		$list->setOption('asapp', true);
		$list->setOption('dao', $dao);
		$list->load();

		foreach($list as $doctype){
			try{
				$srcIconFile = DEFAULT_DOCTYPES_ICONS_DIR .'/'. $doctype['icon'];
				Doctype::compileIcon($srcIconFile, $doctype['id']);
			}
			catch(\Exception $e){
				$this->errorStack()->error($e->getMessage());
			}
		}
		return $this->successForward();
	}
} //End of class
