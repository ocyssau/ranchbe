<?php
namespace Ged\Form\Notification;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class EditForm extends Form implements InputFilterProviderInterface
{

	public $template;

	protected $inputFilter;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('notificationEdit');

		$this->template = 'ged/notification/editform';

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$inputFilter = $this->getInputFilter();

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'spaceName',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceId',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Add hidden fields */
		$this->add(array(
			'name' => 'referenceCid',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Event */
		$this->add(array(
			'name' => 'events',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker form-control',
				'multiple' => 'multiple',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Events'),
				'value_options' => $this->_getEvents(),
			)
		));
		$inputFilter->add(array(
			'name' => 'events',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'Null',
					'options' => array(
						'type'=>'all',
					)
				)
			)
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'spaceName' => array(
				'required' => false
			),
			'referenceId' => array(
				'required' => false
			),
			'referenceCid' => array(
				'required' => false
			),
		);
	}

	/**
	 * @return array
	 */
	protected function _getEvents()
	{
		if ( !isset($this->eventsSet) ) {
			$selectSet = array(
				'post_update'=>tra('document update'),
				'post_store'=>tra('document creation'),
				'post_checkout'=>tra('document checkout'),
				'post_checkin'=>tra('document checkin'),
				'post_reset'=>tra('document checkout reset'),
				'post_suppress'=>tra('document suppression'),
				'post_move'=>tra('document move')
			);
			$this->eventsSet = $selectSet;
		}
		return $this->eventsSet;
	}
}
