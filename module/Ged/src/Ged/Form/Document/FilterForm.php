<?php
namespace Ged\Form\Document;

use Rbplm\Dao\Filter\Op;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Application\Form\AbstractFilterForm;
use Rbplm\People;
use Rbplm\Ged\Document;
use Rbplm\Ged\AccessCode;

class FilterForm extends AbstractFilterForm
{
	/**
	 */
	public function __construct($factory=null, $name=null, $containerId)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/document/filterform.phtml';
		$elemtFactory = $this->getElementFactory();
		$inputFilter = $this->getInputFilter();

		/* spacename */
		$this->add(array(
			'name' => 'spacename',
			'type'  => 'Zend\Form\Element\Hidden',
		));

		/* document id */
		$this->add(array(
			'name' => 'documentid',
			'type'  => 'Zend\Form\Element\Hidden',
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input Number here',
				'class'=>'form-control',
				'data-where'=>'number',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false,
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Designation',
				'class'=>'form-control',
				'data-where'=>'description',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false,
		));

		/* Find what */
		$this->add(array(
			'name' => 'find',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Find',
				'class'=>'form-control',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Find',
			),
		));
		$inputFilter->add(array(
			'name' => 'find',
			'required' => false,
		));

		/* Find in field */
		$list = array(
			'category_number'=>'Category',
			'indice_value'=>'Indice',
			'doctype_number'=>'Doctype',
			'designation' => 'Designation',
			'indice_value' => 'Indice',
			'document_version' => 'Version',
		);
		$elemtFactory->select($list, array(
			'name'=>'find_field',
			'label'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'size'=>1,
		));

		/* Select action */
		$actions = array(
			'check_out_by'=>'Checkout By',
			'update_by'=>'Update By',
			'open_by'=>'Created By',
		);
		$elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'label'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
		/* Select action by user */
		$elemtFactory->selectUser(array(
			'name'=>'f_action_user_name',
			'label'=>'By',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		/* displayHistory */
		$this->add(array(
			'name' => 'displayHistory',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control submitOnClick',
			),
			'options' => array(
				'label' => 'Display history',
			),
		));
		$inputFilter->add(array(
			'name' => 'displayHistory',
			'required' => false,
		));

		/* onlyMy */
		$this->add(array(
			'name' => 'onlyMy',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control submitOnClick'
			),
			'options' => array(
				'label' => 'Only me',
			),
		));
		$inputFilter->add(array(
			'name' => 'onlyMy',
			'required' => false,
		));

		/* checkByMe */
		$this->add(array(
			'name' => 'checkByMe',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control submitOnClick'
			),
			'options' => array(
				'label' => 'Only check by me',
			),
		));
		$inputFilter->add(array(
			'name' => 'checkByMe',
			'required' => false,
		));

		/* displayThumbs */
		$this->add(array(
			'name' => 'displayThumbs',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control submitOnClick'
			),
			'options' => array(
				'label' => 'Display thumbnails',
			),
		));
		$inputFilter->add(array(
			'name' => 'displayHistory',
			'required' => false,
		));

		/* Doctype */
		$this->add(array(
			'name' => 'find_doctype',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Doctype',
				'class'=>'form-control',
				'data-where'=>'doctypeId',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Doctype',
				'size' => 8
			),
		));
		$inputFilter->add(array(
			'name' => 'find_doctype',
			'required' => false,
		));

		/* State */
		$this->add(array(
			'name' => 'find_state',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Life Stage Status',
				'class'=>'form-control',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Status',
				'size' => 8
			),
		));
		$inputFilter->add(array(
			'name' => 'find_state',
			'required' => false,
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Advanced',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false,
		));

		/* DateAndTime CheckBox */
		$this->add(array(
			'name' => 'f_dateAndTime_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Date And Time',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_dateAndTime_cb',
			'required' => false,
		));

		/* CheckOut date */
		$this->add(array(
			'name' => 'f_check_out_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'CheckOut date',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_cb',
			'required' => false,
		));
		/* Superior to Date */
		$this->add(array(
			'name' => 'f_check_out_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_min',
			'required' => false,
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_check_out_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_max',
			'required' => false,
		));

		/* Update date selector */
		$this->add(array(
			'name' => 'f_update_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Update date',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_cb',
			'required' => false,
		));

		/* Superior to date */
		$this->add(array(
			'name' => 'f_update_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_min',
			'required' => false,
		));

		/* Inferior to date */
		$this->add(array(
			'name' => 'f_update_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_max',
			'required' => false,
		));

		/* Open date */
		$this->add(array(
			'name' => 'f_open_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Open date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_open_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_open_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_max',
			'required' => false,
		));

		/* Close date */
		$this->add(array(
			'name' => 'f_close_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Close date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_close_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_close_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_max',
			'required' => false,
		));

		/* FsClose date */
		$this->add(array(
			'name' => 'f_fsclose_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Forseen Close date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_fsclose_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_fsclose_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_max',
			'required' => false,
		));

		/* select category */
		$elemtFactory->selectCategory(array(
			'name'=>'find_category',
			'label'=>'Category',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'category_number',
			'sortOrder'=>'ASC',
			'containerId'=>$containerId
		));

		/* select access */
		$list=array(
			null=>'Click to select',
			'free'=>'Free',
			'1'=>'CheckedOut',
			'5'=>'InWorkflow',
			'10'=>'Validated',
			'11'=>'Locked',
			'12'=>'Marked to suppress',
			'15'=>'Historical',
		);
		$elemtFactory->select($list, array(
			'name'=>'find_access_code',
			'label'=>'Access',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
	}

	/**
	 *
	 * @param array $optionalFields
	 *
	 */
	public function setExtended($optionalFields, $view)
	{
		if(is_array($optionalFields)){
			foreach($optionalFields as $val){
				/* Inferior to date */
				$this->add(array(
					'name' => $val['appName'],
					'type'  => 'Zend\Form\Element\Text',
					'attributes' => array(
						'placeholder' => 'Click to select date',
						'class'=>'form-control',
						'size'=>$val['size']
					),
					'options' => array(
						'label' => $val['description'],
					),
				));
			}
		}
		$view->extended = $optionalFields;
		return $this;
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$me = People\CurrentUser::get()->getid();
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NORMALS OPTIONS */
		//ONLY ME
		if($datas['onlyMy']){
			$subFilter = new DaoFilter('', false);
			$subFilter->orFind($me, $dao->toSys('lockById'), Op::OP_EQUAL);
			$subFilter->orFind($me, $dao->toSys('updateById'), Op::OP_EQUAL);
			$subFilter->orFind($me, $dao->toSys('createById'), Op::OP_EQUAL);
			$filter->subor($subFilter);
		}

		//CHECKOUT BY ME
		if($datas['checkByMe']){
			$filter->andFind($me, $dao->toSys('lockById'), Op::OP_EQUAL);
			$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::OP_EQUAL);
		}

		//DISPLAY HISTORY
		if(!$datas['displayHistory']){
			$filter->andfind( array(15, 20), $dao->toSys('accessCode'), Op::OP_NOTBETWEEN );
		}

		//NUMBER
		if($datas['find_number']){
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::OP_CONTAINS);
		}

		//DESIGNATION
		if($datas['find_designation']){
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::OP_CONTAINS);
		}

		//DOCTYPE
		if($datas['find_doctype']){
			$filter->with(array(
				'table'=>'doctypes',
				'on'=>'doctype_id',
				'alias'=>'doctype',
				'select'=>array('doctype_number'),
				'direction'=>'outer'
			));
			$filter->andFind($datas['find_doctype'], 'doctype_number', Op::OP_CONTAINS);
		}

		//ACCESS CODE
		if($datas['find_access_code']){
			if ($datas['find_access_code'] == 'free'){
				$acode = 0;
			}
			else{
				$acode = $datas['find_access_code'];
			}
			$filter->andFind($acode, $dao->toSys('accessCode'), Op::OP_EQUAL);
		}

		//STATE
		if($datas['find_document_state']){
			$filter->andFind($datas['find_document_state'], $dao->toSys('lifeStage'), Op::OP_EQUAL);
		}

		//CATEGORY
		if($datas['find_category']){
			$filter->andFind($datas['find_category'], $dao->toSys('categoryId'), Op::OP_EQUAL);
		}

		//ADVANCED SEARCH
		if($datas['f_adv_search_cb']){
			//FIND IN
			if($datas['find'] && $datas['find_field']){
				$filter->with(array(
					'table'=>$factory->getName().'_categories',
					'on'=>'category_id',
					'alias'=>'category',
					'select'=>array('category_number'),
					'direction'=>'outer'
				));
				$filter->andFind($datas['find'], $datas['find_field'], Op::CONTAINS);
			}

			//ACTION USER
			if($datas['f_action_field'] && $datas['f_action_user_name']){
				$filter->andFind($datas['f_action_user_name'], $datas['f_action_field'], Op::CONTAINS);
			}

			//DATE AND TIME
			if($datas['f_dateAndTime_cb']){
				//CHECKOUT
				if($datas['f_check_out_date_cb']){
					if($datas['f_check_out_date_min']){
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::SUP);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
					if($datas['f_check_out_date_max']){
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::INF);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
				}
				//UPDATE
				if($datas['f_update_date_cb']){
					if($datas['f_update_date_min']){
						$filter->andFind($this->dateToSys($datas['f_update_date_min']), $dao->toSys('updated'), Op::SUP);
					}
					if($datas['f_update_date_max']){
						$filter->andFind($this->dateToSys($datas['f_update_date_max']), $dao->toSys('updated'), Op::INF);
					}
				}
				//OPEN
				if($datas['f_open_date_cb']){
					if($datas['f_open_date_min']){
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $dao->toSys('created'), Op::SUP);
					}
					if($datas['f_open_date_max']){
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $dao->toSys('created'), Op::INF);
					}
				}
				//CLOSE
				if($datas['f_close_date_cb']){
					if($datas['f_close_date_min']){
						$dateAsSys = \DateTime::createFromFormat(SHORT_DATE_FORMAT, $datas['f_close_date_min'])->getTimestamp();
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $dao->toSys('closed'), Op::SUP);
					}
					if($datas['f_close_date_max']){
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $dao->toSys('closed'), Op::INF);
					}
				}
				//FORSEEN CLOSE
				if($datas['f_fsclose_date_cb']){
					if($datas['f_fsclose_date_min']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), 'forseen_close_date', Op::SUP);
					}
					if($datas['f_fsclose_date_max']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), 'forseen_close_date', Op::INF);
					}
				}
			}
		}

		//EXTENDED
		if(is_array($this->extended)){
			foreach($this->extended as $val){
				$fieldName = $val['appname'];
				if($datas[$fieldName]){
					$filter->andFind($datas[$fieldName], $fieldName, Op::CONTAINS);
				}
			}
		}

		return $this;
	}
}
