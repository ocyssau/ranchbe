<?php
namespace Ged\Form\Doctype;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class EditForm extends Form implements InputFilterProviderInterface
{

	public $template;

	public $ranchbe;

	protected $inputFilter;

	protected $scriptsSelect;

	protected $iconsSelect;

	protected $fileExtensionSelect;

	protected $visufileExtensionSelect;

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view)
	{
		/* we want to ignore the name passed */
		parent::__construct('categoryEdit');

		$this->template = 'ged/doctype/editform';
		$this->ranchbe = \Ranchbe::get();

		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter());

		$inputFilter = $this->getInputFilter();

		/* Add hidden fields */
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));

		/* Name */
		$mask = '/'.$this->ranchbe->getConfig('doctype.name.mask').'/i';
		$help = $this->ranchbe->getConfig('doctype.name.mask.help');
		$view->numberHelp = $help;
		$this->add(array(
			'name' => 'name',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Name')
			)
		));
		$inputFilter->add(array(
			'name' => 'name',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'Regex',
					'options' => array(
						'pattern' => $mask,
						'messages' => array(
							\Zend\Validator\Regex::INVALID => $help
						)
					)
				)
			)
		));

		/* Description */
		$this->add(array(
			'name' => 'description',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Description')
			)
		));

		/* Regex */
		$this->add(array(
			'name' => 'recognitionRegexp',
			'type' => 'Zend\Form\Element',
			'attributes' => array(
				'type' => 'textarea',
				'placeholder' => '',
				'class' => 'form-control',
				'cols' => 60
			),
			'options' => array(
				'label' => tra('Regex')
			)
		));

		/* May be composite */
		$this->add(array(
			'name' => 'mayBeComposite',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 1
			),
			'options' => array(
				'label' => tra('May Be Composite'),
				'value_options' => array(
					0 => 'No',
					1 => 'Yes'
				),
				'multiple' => false
			)
		));

		/* Script Pre Store */
		$this->add(array(
			'name' => 'scriptPreStore',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control script-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Pre Store'),
				'value_options' => $this->_getScripts(),
				'multiple' => false
			)
		));

		/* Script Post Store */
		$this->add(array(
			'name' => 'scriptPostStore',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control script-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Post Store'),
				'value_options' => $this->_getScripts(),
				'multiple' => false
			)
		));

		/* Script Pre Update */
		$this->add(array(
			'name' => 'scriptPreUpdate',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control script-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Pre Update'),
				'value_options' => $this->_getScripts(),
				'multiple' => false
			)
		));

		/* Script Post Update */
		$this->add(array(
			'name' => 'scriptPostUpdate',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control script-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Post Update'),
				'value_options' => $this->_getScripts(),
				'multiple' => false
			)
		));

		/* Icon */
		$this->add(array(
			'name' => 'icon',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control icon-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Icon'),
				'value_options' => $this->_getIcons(),
				'multiple' => false
			)
		));

		/* File Extension */
		$this->add(array(
			'name' => 'fileExtensions',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'selectpicker fileext-select',
				'multiple' => 'multiple',
				'data-size' => 10
			),
			'options' => array(
				'label' => tra('File Extension'),
				'value_options' => $this->_getFileExtensions(),
			)
		));
		$inputFilter->add(array(
			'name' => 'fileExtensions',
			'required' => true,
			'filters' => array(
				array(
					'name' => 'Null',
					'options' => array(
						'type'=>'all',
					)
				)
			)
		));

		/* Visu File Extension */
		$this->add(array(
			'name' => 'visuFileExtension',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control visufile-select',
				'size' => 1
			),
			'options' => array(
				'label' => tra('Visu File Type'),
				'value_options' => $this->_getVisuFileExtensions(),
				'multiple' => false
			)
		));
		$inputFilter->add(array(
			'name' => 'fileExtension',
			'required' => false,
			'filters' => array(
				array(
					'name' => 'Null',
					'options' => array(
						'type'=>'all',
					)
				)
			)
		));

		/* File type */
		$fileTypes = $this->ranchbe->getFileTypes();
		foreach( $fileTypes as $filetype ) {
			$this->add(array(
				'name' => 'fileType',
				'type' => 'Zend\Form\Element\Select',
				'options' => array(
					'label' => tra('Has Associated Files'),
					'value_options' => $fileTypes,
					'multiple' => true
				),
				'attributes' => array(
					'value' => 'yes',
					'class' => 'form-control',
				)
			));
		}

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false
			),
			'description' => array(
				'required' => false
			),
			'recognitionRegexp' => array(
				'required' => false
			),
			'mayBeComposite' => array(
				'required' => false
			),
			'scriptPreStore' => array(
				'required' => false
			),
			'scriptPostStore' => array(
				'required' => false
			),
			'scriptPreUpdate' => array(
				'required' => false
			),
			'scriptPostUpdate' => array(
				'required' => false
			),
			'icon' => array(
				'required' => false
			),
			'visuFileExtension' => array(
				'required' => false
			),
			'fileType' => array(
				'required' => false
			)
		);
	}

	/**
	 * @return array
	 */
	protected function _getScripts()
	{
		if ( !isset($this->scriptsSelect) ) {
			/* Scripts list */
			$scriptsReposit = $this->ranchbe->getConfig('path.scripts.doctype');
			$scriptsList = glob($scriptsReposit . '/*.php');
			$selectSet = array(
				0 => null
			);
			foreach( $scriptsList as $scripts ) {
				$scripts = basename($scripts);
				$selectSet[$scripts] = $scripts;
			}
			$this->scriptsSelect = $selectSet;
		}

		return $this->scriptsSelect;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getIcons()
	{
		if ( !isset($this->iconsSelect) ) {
			$iconsReposit = $this->ranchbe->getConfig('icons.doctype.source.path');
			$iconsList = glob($iconsReposit . '/*.gif');
			$iconsList = array_merge($iconsList, glob($iconsReposit . '/*.png'));
			sort($iconsList);
			$selectSet = array(
				0 => null
			);
			foreach( $iconsList as $icon ) {
				$icon = basename($icon);
				$selectSet[$icon] = $icon;
			}
			$this->iconsSelect = $selectSet;
		}

		return $this->iconsSelect;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getFileExtensions()
	{
		if ( !isset($this->fileExtensionSelect) ) {
			$fileExtensions = $this->ranchbe->getFileExtensions();
			$fileExtSelectSet = array_combine($fileExtensions['file'], $fileExtensions['file']);
			array_unshift($fileExtSelectSet, 'select one or many extension');
			$this->fileExtensionSelect = $fileExtSelectSet;
		}

		return $this->fileExtensionSelect;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getVisuFileExtensions()
	{
		if ( !isset($this->visuFileExtensionSelect) ) {
			$visuFileExtensions = $this->ranchbe->getVisuFileExtensions();
			$vfileExtSelectSet = array_combine($visuFileExtensions, $visuFileExtensions);
			array_unshift($vfileExtSelectSet, 'select one extension');
			$this->visufileExtensionSelect = $vfileExtSelectSet;
		}

		return $this->visufileExtensionSelect;
	}

	/**
	 * Get the data from the form and format it
	 */
	protected function _formgetValues($values)
	{
		$fileExtensions = \Ranchbe::getFileExtensions();

		/* unselect file extension list if type file is not selected */
		if ( !isset($values['file_type']['file']) ) $values['file_extension'] = array();

		/* if type file is selected check if file_extension is set */
		if ( isset($values['file_type']['file']) && !is_array($values['file_extension']) ) {
			print(tra('Extension is required'));
			return false;
		}

		/* Reformat the array send by form from the checkbox selection for file_type */
		$values['file_type'] = array_keys($values['file_type']);

		/* Add valid extension to file_extension list for type cadds, ps ...etc */
		foreach( $values['file_type'] as $filetype ) {
			if ( $filetype != 'file' ) {
				$values['file_extension'] = array_merge($values['file_extension'], array_values($fileExtensions[$filetype]));
			}
		}
		return $values;
	}
}
