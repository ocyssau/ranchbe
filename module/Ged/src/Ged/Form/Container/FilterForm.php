<?php
namespace Ged\Form\Container;

use Application\Form\AbstractFilterForm;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;

class FilterForm extends AbstractFilterForm
{
	/**
	 *
	 * @var string
	 */
	public $spaceName;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($factory=null, $name=null)
	{
		parent::__construct($factory, $name);

		$this->template = 'ged/container/filterform.phtml';
		$elemtFactory = $this->getElementFactory();
		$inputFilter = $this->getInputFilter();

		/* spacename */
		$this->add(array(
			'name' => 'spacename',
			'type'  => 'Zend\Form\Element\Hidden',
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Input Number here',
				'class'=>'form-control',
				'data-where'=>'number',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false,
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Designation',
				'class'=>'form-control',
				'data-where'=>'description',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false,
		));

		/* Find what */
		$this->add(array(
			'name' => 'find',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => 'Find',
				'class'=>'form-control',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Find',
			),
		));
		$inputFilter->add(array(
			'name' => 'find',
			'required' => false,
		));

		/* Find in field */
		$list = array(
			'id'=>'id',
		);
		$elemtFactory->select($list, array(
			'name'=>'find_field',
			'label'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'size'=>1,
		));

		/* Select action */
		$actions = array(
			'closeById'=>'Close By',
			'createById'=>'Created By',
		);
		$elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'label'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
		/* Select action by user */
		$elemtFactory->selectUser(array(
			'name'=>'f_action_user_name',
			'label'=>'By',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Advanced',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false,
		));

		/* DateAndTime CheckBox */
		$this->add(array(
			'name' => 'f_dateAndTime_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Date And Time',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_dateAndTime_cb',
			'required' => false,
		));

		/* Open date */
		$this->add(array(
			'name' => 'f_open_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Open date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_open_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_open_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_max',
			'required' => false,
		));

		/* Close date */
		$this->add(array(
			'name' => 'f_close_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Close date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_close_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_close_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_max',
			'required' => false,
		));

		/* FsClose date */
		$this->add(array(
			'name' => 'f_fsclose_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Forseen Close date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_fsclose_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_fsclose_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_fsclose_date_max',
			'required' => false,
		));



	}

	/**
	 *
	 * @param array $optionalFields
	 *
	 */
	public function setExtended($optionalFields, $view)
	{
		if(is_array($optionalFields)){
			foreach($optionalFields as $val){
				/* Inferior to date */
				$this->add(array(
					'name' => $val['appName'],
					'type'  => 'Zend\Form\Element\Text',
					'attributes' => array(
						'placeholder' => 'Click to select date',
						'class'=>'form-control',
						'size'=>$val['size']
					),
					'options' => array(
						'label' => $val['description'],
					),
				));
			}
		}
		$view->extended = $optionalFields;
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		$dao = $this->daoFactory->getDao(\Rbplm\Org\Workitem::$classId);
		$me = People\CurrentUser::get()->getid();
		$this->prepare()->isValid();
		$datas = $this->getData();

		//NUMBER
		if($datas['find_number']){
			$filter->andFind($datas['find_number'], 'number', Op::CONTAINS);
		}

		//DESIGNATION
		if($datas['find_designation']){
			$filter->andFind($datas['find_designation'], 'description', Op::CONTAINS);
		}

		//ADVANCED SEARCH
		if($datas['f_adv_search_cb']){
			//FIND IN
			if($datas['find'] && $datas['find_field']){
				$filter->andFind($datas['find'], $datas['find_field'], Op::CONTAINS);
			}

			//ACTION USER
			if($datas['f_action_field'] && $datas['f_action_user_name']){
				$filter->andFind($datas['f_action_user_name'], $datas['f_action_field'], Op::CONTAINS);
			}

			//DATE AND TIME
			if($datas['f_dateAndTime_cb']){
				//OPEN
				if($datas['f_open_date_cb']){
					if($datas['f_open_date_min']){
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $dao->toSys('created'), Op::SUP);
					}
					if($datas['f_open_date_max']){
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $dao->toSys('created'), Op::INF);
					}
				}
				//CLOSE
				if($datas['f_close_date_cb']){
					if($datas['f_close_date_min']){
						$dateAsSys = \DateTime::createFromFormat(SHORT_DATE_FORMAT, $datas['f_close_date_min'])->getTimestamp();
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $dao->toSys('closed'), Op::SUP);
					}
					if($datas['f_close_date_max']){
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $dao->toSys('closed'), Op::INF);
					}
				}
				//FORSEEN CLOSE
				if($datas['f_fsclose_date_cb']){
					if($datas['f_fsclose_date_min']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), 'forseen_close_date', Op::SUP);
					}
					if($datas['f_fsclose_date_max']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), 'forseen_close_date', Op::INF);
					}
				}
			}
		}

		//EXTENDED
		if(is_array($this->extended)){
			foreach($this->extended as $val){
				$fieldName = $val['appname'];
				if($datas[$fieldName]){
					$filter->andFind($datas[$fieldName], $fieldName, Op::CONTAINS);
				}
			}
		}

		return $this;
	}

} //End of class
