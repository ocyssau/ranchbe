<?php
/**
 *
 */
return array(
	'router' => array(
		'routes' => array(
			'ged' => array(
				'type'=> 'Literal',
				'options' => array(
					'route'=> '/ged',
					'defaults' => array(
						'__NAMESPACE__' => 'Ged\Controller',
						'controller'=> 'Index',
						'action'=> 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type'=> 'Segment',
						'options' => array(
							'route'=> '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
							),
							'defaults' => array(
							),
						),
					),
				),
			),
			'rbdocument' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbdocument/manager[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Manager',
						'action' => 'index',
					),
				),
			),
			'viewer' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbdocument/viewer[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Viewer',
						'action' => 'index',
					),
				),
			),
			'basket' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbdocument/basket[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Basket',
						'action' => 'index',
					),
				),
			),
			'relation' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbdocument/relation[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Relation',
						'action' => 'index',
					),
				),
			),
			'detail' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/rbdocument/detail[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Document\Detail',
						'action' => 'index',
					),
				),
			),
			'doctype' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/doctype[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Doctype\Index',
						'action' => 'index',
					),
				),
			),
			'notification' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/notification[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Notification',
						'action' => 'index',
					),
				),
			),
			'workitem' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/workitem[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Workitem',
						'action' => 'index',
					),
				),
			),
			'cadlib' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/cadlib[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Cadlib',
						'action' => 'index',
					),
				),
			),
			'bookshop' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/bookshop[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Bookshop',
						'action' => 'index',
					),
				),
			),
			'mockup' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/ged/mockup[/][:action]',
					'defaults' => array(
						'controller' => 'Ged\Controller\Container\Mockup',
						'action' => 'index',
					),
				),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Ged\Controller\Index' => 'Ged\Controller\IndexController',
			'Ged\Controller\Document\Manager'=>'Ged\Controller\Document\ManagerController',
			'Ged\Controller\Document\Viewer'=>'Ged\Controller\Document\ViewerController',
			'Ged\Controller\Document\Basket'=>'Ged\Controller\Document\BasketController',
			'Ged\Controller\Document\Relation'=>'Ged\Controller\Document\RelationController',
			'Ged\Controller\Document\Detail'=>'Ged\Controller\Document\DetailController',
			'Ged\Controller\Doctype\Index'=>'Ged\Controller\Doctype\IndexController',
			'Ged\Controller\Container\Notification'=>'Ged\Controller\Container\NotificationController',
			'Ged\Controller\Container\Workitem'=>'Ged\Controller\Container\WorkitemController',
			'Ged\Controller\Container\Cadlib'=>'Ged\Controller\Container\CadlibController',
			'Ged\Controller\Container\Bookshop'=>'Ged\Controller\Container\BookshopController',
			'Ged\Controller\Container\Mockup'=>'Ged\Controller\Container\MockupController',
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Ged'=>__DIR__ . '/../view',
		),
	),
);
