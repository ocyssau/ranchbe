<?php
namespace Workflow\Controller;

use Rbs\Dao\Factory as DaoFactory;
use Rbplm\People;
use Application\Form\PaginatorForm;
use Workflow\Model\Wf;
use Workflow\Form\InstanceFilterForm as Filter;

class InstanceActivityController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$request = $this->getRequest();

		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();

		$instanceId = $this->params()->fromRoute('id');

		$filter = $this->_getFilter('workflow/instanceActivity/filter');
		$filter->get('stdfilter-id')->setValue($instanceId);
		$filter->prepare();
		$filter->saveToSession('workflow/instanceActivity/filter');

		$bind = array();
		$factory = DaoFactory::get();
		$list = $factory->getList(Wf\Instance\Activity::$classId);
		$table = $factory->getTable(Wf\Instance\Activity::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql = "SELECT * FROM $table";

		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$list->setOption('asapp', true);
		$view->list = $list;
		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Description' => 'title',
			'Owner' => 'ownerId',
			'Started' => 'started',
			'Ended' => 'ended',
			'Status' => 'status',
			'Process Instance' => 'instanceId'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$factory = DaoFactory::get();

		$aInstanceId = $this->params()->fromRoute('id');
		$aInstanceDao = $factory->getDao(Wf\Instance\Activity::$classId);
		$aInstanceDao->deleteFromId($aInstanceId);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	public function editAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = $this->view;

		$actInstId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new Filter();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = 'CONCAT_WS(name, title, status, id, ownerId)';
		$filter->key2 = 'instanceId';
		return $filter;
	}
}
