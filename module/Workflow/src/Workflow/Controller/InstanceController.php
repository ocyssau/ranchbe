<?php
namespace Workflow\Controller;

use Rbplm\People;
use Application\Form\PaginatorForm;
use Workflow\Model\Wf;
use Rbs\Dao\Factory as DaoFactory;
use Workflow\Form\InstanceFilterForm as Filter;

class InstanceController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$request = $this->getRequest();

		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();

		$processId = $this->params()->fromRoute('id');

		$filter = $this->_getFilter('workflow/instance/filter');
		$filter->get('stdfilter-id')->setValue($processId);
		$filter->prepare();
		$filter->saveToSession('workflow/instance/filter');

		// search from header search area :
		$bind = array();
		$factory = DaoFactory::get();
		$list = $factory->getList(Wf\Instance::$classId);
		$table = $factory->getTable(Wf\Instance::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql = "SELECT * FROM $table";

		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$list->dao = $factory->getDao(Wf\Instance::$classId);
		$list->setOption('asapp', true);
		$view->list = $list;
		$view->headers = array(
			'#' => 'id',
			'Name' => 'name',
			'Description' => 'title',
			'Owner' => 'ownerId',
			'Started' => 'started',
			'Ended' => 'ended',
			'ProcessId' => 'processId',
			'Status' => 'status'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = $this->view;

		$instanceId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$workflow->deleteInstance($instanceId);

		return $this->redirect()->toRoute('instance');
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new Filter();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = 'CONCAT_WS(name, title, status, id, ownerId)';
		$filter->key2 = 'pId';
		return $filter;
	}
}
