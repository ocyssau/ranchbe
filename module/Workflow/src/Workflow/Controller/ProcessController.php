<?php
namespace Workflow\Controller;

use Zend\View\Model\ViewModel;
use Rbplm\People;
use Application\Form\PaginatorForm;
use Rbs\Dao\Factory as DaoFactory;
use Workflow\Model\Wf;
use Workflow\Form;
use Workflow\Form\InstanceFilterForm as stdFilterForm;

class ProcessController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 *
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$request = $this->getRequest();
		$factory = DaoFactory::get();

		$userId = People\CurrentUser::get()->getId();
		$userLogin = People\CurrentUser::get()->getLogin();

		$filter = $this->_getFilter('workflow/process/filter');
		$filter->get('stdfilter-id')->setValue($processId);
		$filter->prepare();
		$filter->saveToSession('workflow/process/filter');

		// search from header search area :
		$bind = array();
		$list = $factory->getList(Wf\Process::$classId);

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($list->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$table = $factory->getTable(Wf\Process::$classId);
		$sql = "SELECT * FROM $table";

		if ( $filter->where ) {
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$list->loadFromSql($sql, $bind);
		$list->setOption('asapp', true);
		$list->dao = $factory->getDao(Wf\Process::$classId);
		$view->list = $list;

		$view->list = $list;
		$view->headers = array(
			'#' => 'pId',
			'Name' => 'normalizedName',
			'Description' => 'title',
			'Version' => 'version',
			'isValid' => 'isValid',
			'isActive' => 'isActive'
		);

		$view->filter = $filter;
		$view->paginator = $paginator;

		return $view;
	}

	/**
	 */
	public function createAction()
	{
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		$view = new ViewModel();
		$factory = DaoFactory::get();

		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$process = Wf\Process::init();
		$form = new Form\ProcessForm();
		$form->bind($process);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$process->getNormalizedName(); // to init normalized name
				$factory->getDao($process)->save($process);
				return $this->redirect()->toRoute('process');
			}
		}

		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');
		$request = $this->getRequest();
		$view = new ViewModel();
		$factory = DaoFactory::get();

		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$process = new Wf\Process();
		$factory->getDao($process)->loadFromId($process, $id);

		$form = new Form\ProcessForm();
		$form->bind($process);

		if ( $request->isPost() ) {
			$form->setData($request->getPost());
			if ( $form->isValid() ) {
				$factory->getDao($process)->save($process);
				return $this->redirect()->toRoute('process');
			}
		}

		$view->setTemplate($form->template);
		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function deleteAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = new ViewModel();
		if ( $this->getRequest()->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$process = $workflow->deleteProcess($processId);

		return $this->redirect()->toRoute('process');
	}

	/**
	 */
	public function newmajorAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$rawdata = $workflow->export($processId);
		$rawdata = json_encode($rawdata);
		$process = $workflow->process;
		$factory = DaoFactory::get();

		$versions = $factory->getDao($process)->getVersions($process->getName());
		$lastVersion = $versions[0];

		// var_dump($versions);die;

		$version = explode('.', $lastVersion);
		$major = (int)$version[0];
		$minor = (string)$version[1];
		$newVersion = (string)($major + 1) . '.' . $minor;

		$workflow->import($rawdata, $newVersion);
		return $this->redirect()->toRoute('process');
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function newminorAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$rawdata = $workflow->export($processId);
		$rawdata = json_encode($rawdata);
		$process = $workflow->process;
		$currentVersion = explode('.', $process->getVersion());
		$currentMajor = (string)$currentVersion[0];
		$factory = DaoFactory::get();

		$versions = $factory->getDao($process)->getVersions($process->getName());
		foreach( $versions as $v ) {
			$v = explode('.', $v);
			if ( (int)$v[0] == $currentMajor ) {
				$lastMinor = (int)$v[1];
				break;
			}
		}

		$newVersion = $currentMajor . '.' . (string)($lastMinor + 1);

		$workflow->import($rawdata, $newVersion);
		return $this->redirect()->toRoute('process');
	}

	/**
	 */
	public function exportAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = new ViewModel();
		$view->setTerminal(true);
		$factory = DaoFactory::get();

		$processId = $this->params()->fromRoute('id');
		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$rawdata = $workflow->export($processId);
		$process = $workflow->process;

		$rawdata = json_encode($rawdata);

		$inCharset = iconv_get_encoding(); // Get current encoding option
		$filename = 'processExport_' . $process->getNormalizedName() . '.json';
		$view->content = iconv($inCharset['output_encoding'], "ISO-8859-1//TRANSLIT", $rawdata);

		$this->getResponse()
			->getHeaders()
			->addHeaderLine('Content-Type', 'text/json; charset=utf-8')
			->addHeaderLine('Content-Transfer-Encoding', 'Binary')
			->addHeaderLine('Content-Disposition', "attachment; filename=$filename");
		// ->addHeaderLine( 'Pragma', 'no-cache' )
		// ->addHeaderLine( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0, public' )
		// ->addHeaderLine( 'Expires', 0 );

		return $view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function importAction()
	{
		$request = $this->getRequest();
		$view = new ViewModel();

		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$form = new Form\ImportProcessForm();

		if ( $request->isPost() ) {
			$post = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
			$form->setData($post);
			if ( $form->isValid() ) {
				$data = $form->getData();
				$file = $data['file']['tmp_name'];
				$fileContent = file_get_contents($file);

				$workflow = $this->getServiceLocator()
					->get('Workflow')
					->connect($this);
				$workflow->import($fileContent);

				return $this->redirect()->toRoute('process');
			}
		}

		$view->form = $form;
		return $view;
	}

	/**
	 */
	public function editgraphAction()
	{
		$view = new ViewModel();
		$view->admin = false;

		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}
		if ( $appAcl->hasRight($appAcl::RIGHT_ADMIN) ) {
			$view->admin = true;
		}

		$view->admin = true;
		$processId = $this->params()->fromRoute('id', null);
		$view->processId = $processId;
		$factory = DaoFactory::get();

		$process = new Wf\Process();
		$dao = $factory->getDao($process);
		$dao->loadFromId($process, $processId);
		$view->process = $process;

		$graph = $dao->getGraphAsArray($processId, $factory);
		$view->graph = json_encode($graph);
		return $view;
	}

	/**
	 */
	private function _getFilter($sessionKey)
	{
		$request = $this->getRequest();

		$filter = new stdFilterForm();
		$filter->loadFromSession($sessionKey);
		$filter->setData($request->getPost());
		$filter->key1 = 'CONCAT_WS(name, title, id, ownerId)';
		$filter->key2 = 'id';
		return $filter;
	}

	/**
	 * query graph =
	 * array (size=3)
	 * 'activities' =>
	 * array (size=3)
	 * 0 =>
	 * array (size=8)
	 * 'id' => string 'sample' (length=6)
	 * 'name' => string 'sample' (length=6)
	 * 'title' => string 'SAMPLE' (length=6)
	 * 'type' => string 'activity' (length=8)
	 * 'isAutomatic' => string 'false' (length=5)
	 * 'isComment' => string 'false' (length=5)
	 * 'isInteractive' => string 'false' (length=5)
	 * 'attributes' =>
	 * array (size=2)
	 * 'positionx' => string '630px' (length=5)
	 * 'positiony' => string '90.5px' (length=6)
	 * 1 =>
	 * 'transitions' =>
	 * array (size=2)
	 * 0 =>
	 * array (size=3)
	 * 'id' => string 'con_12' (length=6)
	 * 'fromid' => string 'sample' (length=6)
	 * 'toid' => string '50' (length=2)
	 * 'processid' => string '8' (length=1)
	 */
	public function savegraphAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$factory = DaoFactory::get();

		$this->workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);

		$processId = $this->params()->fromPost('processid', null);
		$activities = $this->params()->fromPost('activities', array());
		$transitions = $this->params()->fromPost('transitions', array());

		$process = new Wf\Process();
		$factory->getDao($process)->loadFromId($process, $processId);

		$return = $this->workflow->saveProcess($process, $activities, $transitions);

		$view->activities = $return['activities'];
		$view->transitions = $return['transitions'];
		return $view;
	}

	/**
	 */
	public function savetemplateAction()
	{
		// check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		return new ViewModel();
	}

	/**
	 */
	public function getscriptsrcAction()
	{
		$processId = (int)$this->params()->fromPost('processid', null);
		$activityId = (int)$this->params()->fromPost('activityid', null);

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$this->workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$code = $this->workflow->getCode($processId, $activityId);
		$modelFile = $code->getModelFile();
		$tplFile = $code->getTemplateFile();
		$formFile = $code->getFormFile();

		$view->modelFile = $modelFile;
		$view->templateFile = $tplFile;
		$view->formFile = $formFile;

		$view->modelSrc = file_get_contents($modelFile, true);
		$view->templateSrc = file_get_contents($tplFile, true);
		$view->formSrc = file_get_contents($formFile, true);

		return $view;
	}

	/**
	 */
	public function savescriptAction()
	{
		/* check authorization */
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_MANAGE) ) {
			return $this->notauthorized();
		}

		$request = $this->getRequest();
		$view = new ViewModel();
		if ( $request->isXmlHttpRequest() ) {
			$view->setTerminal(true);
		}

		$activityId = (int)$this->params()->fromPost('activityid', null);
		$processId = (int)$this->params()->fromPost('processid', null);
		$src = trim($this->params()->fromPost('scriptsrc', null));
		if ( $src != "" ) {
			$this->workflow = $this->getServiceLocator()
				->get('Workflow')
				->connect($this);
			$code = $this->workflow->getCode($processId, $activityId);
			$modelFile = $code->getModelFile();

			file_put_contents($modelFile, $src);
		}

		return $view;
	}
}
