<?php
namespace Workflow\Controller;

use Rbplm\People;
use Workflow\Model;

class ActivityController extends AbstractController
{

	const SIGNAL_DELETEACTIVITY_PRE = 'deleteactivity.pre';

	const SIGNAL_DELETEACTIVITY_POST = 'deleteactivity.post';

	/**
	 *
	 * {@inheritDoc}
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		return $this->view;
	}

	/**
	 */
	public function editAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_ADMIN) ) {
			return $this->notauthorized();
		}
		return $this->view;
	}

	/**
	 */
	public function createAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_ADMIN) ) {
			return $this->notauthorized();
		}
		return $this->view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function deleteAction()
	{
		$view = $this->view;

		$activityId = $this->params()->fromRoute('id');

		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_ADMIN) ) {
			return $this->notauthorized();
		}

		$workflow = $this->getServiceLocator()
			->get('Workflow')
			->connect($this);
		$activity = $workflow->deleteActivity($activityId);

		$view->object = $activity;
		return $view;
	}

	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function runAction()
	{
		$appAcl = People\CurrentUser::get()->appacl;
		if ( !$appAcl->hasRight($appAcl::RIGHT_CONSULT) ) {
			return $this->notauthorized();
		}

		$view = $this->view;
		$actInstId = $this->params()->fromRoute('id');

		/* instanciate Workflow service */
		$workflow = new \Workflow\Service\Workflow();

		/* RUN ACTIVITY: */
		$act = $workflow->runActivity($actInstId)->lastActivity;
		if ( $act->getType() == 'end' ) {
			$workflow->complete($workflow->instance->getId());
		}
		else {
			/* TRANSLATE : */
			$workflow->translateActivity($actInstId);
		}

		return $this->redirect()->toRoute('workflow');
	}
}
