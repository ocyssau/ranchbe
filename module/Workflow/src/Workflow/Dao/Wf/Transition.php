<?php
//%LICENCE_HEADER%

namespace Workflow\Dao\Wf;

use Workflow\Dao\Link;

/** SQL_SCRIPT>>
CREATE TABLE wf_transition(
	`uid` VARCHAR(32) NOT NULL,
	`parentId` INTEGER NOT NULL,
	`childId` INTEGER NOT NULL,
	`parentUid` VARCHAR(255) NOT NULL,
	`childUid` VARCHAR(255) NOT NULL,
	`name` VARCHAR(255) NULL,
	`lindex` INTEGER DEFAULT 0,
	`attributes` TEXT NULL,
PRIMARY KEY (`parentId`, `childId`)
);
<<*/

/** SQL_INSERT>>
<<*/

/** SQL_ALTER>>
ALTER TABLE wf_transition ADD UNIQUE (uid);

ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_uid` (`uid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_name` (`name` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_parentId` (`parentId` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_childId` (`childId` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_parentUid` (`parentUid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_childUid` (`childUid` ASC);
ALTER TABLE `wf_transition` ADD INDEX `WFTRANSITION_lindex` (`lindex` ASC);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
DROP TABLE wf_transition CASCADE;
 <<*/

/**
 *
 */
class Transition extends Link
{

	/**
	 * 
	 * @var string
	 */
	public static $table = 'wf_transition';
	public static $childTable = 'wf_activity';
	public static $childForeignKey = 'id';
	public static $parentTable = 'wf_activity';
	public static $parentForeignKey = 'id';
	
	/**
	 * Delete all transitions of a process
	 * @param integer $processId
	 */
	public function deleteFromProcess($processId)
	{
		if(!$this->deleteFromProcessStmt){
			$table = static::$table;
			$activityTable = Activity::$table;
			$sql = "DELETE FROM trans USING $table AS trans LEFT JOIN $activityTable AS act ON (trans.parentId = act.id) WHERE processId = :processId";
			$this->deleteFromProcessStmt = $this->connexion->prepare($sql);
		}
		$this->deleteFromProcessStmt->execute(array(':processId'=>$processId));
		return $this;
	}

} //End of class
