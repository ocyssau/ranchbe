<?php
//%LICENCE_HEADER%

namespace Workflow\Dao\Wf\Instance;

/**
 * @brief Dao class for \Workflow\Dao\Wf\Instance\Activity
 * 
 * @see \Workflow\Model\Dao\Pg
 *
 */
class Standalone extends Activity
{
} //End of class