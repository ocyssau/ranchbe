<?php
//%LICENCE_HEADER%

namespace Workflow\Dao;

use Workflow\Model;
use Workflow\Model\Any as AnyObject;
use Workflow\Model\Link;

use Application\Dao\Connexion;

use Workflow\Dao\Exception As Exception;
use Workflow\Dao\Error As Error;

/**
 * @brief Abstract Dao for sier database with shemas using ltree to manage trees.
 *
 */
class Factory
{
    const GENERIC = 200;
    
    /**
     * Registry of instanciated DAO.
     *
     * @var array
     */
    private static $_registry = array();
    
    /**
     * Associate for each Component Class ID a DAO CLASS
     * ClassId 200 return the super class DAO Component/Component
     * @var array
     */
    private static $_map = array(
    	420=>array('Wf\Process', 'wf_process'),
    	421=>array('Wf\Activity\Activity', 'wf_activity'),
    	422=>array('Wf\Activity\Start', 'wf_activity'),
    	423=>array('Wf\Activity\End', 'wf_activity'),
    	424=>array('Wf\Activity\Join', 'wf_activity'),
    	425=>array('Wf\Activity\Split', 'wf_activity'),
    	426=>array('Wf\Activity\Aswitch', 'wf_activity'),
    	427=>array('Wf\Activity\Standalone', 'wf_activity'),
    	428=>array('Wf\Activity', 'wf_activity'),
    	430=>array('Wf\Transition', 'wf_transition'),
    	440=>array('Wf\Instance', 'wf_instance'),
    	441=>array('Wf\Instance\Activity', 'wf_instance_activity'),
    	442=>array('Link', ''),
    	450=>array('Wf\Instance\Standalone', 'wf_instance_activity'),
    );
    
    /**
     * Return a dao object for the mapped object.
     * If $ComponentOrId = 200, return the supre class dao Component\Component
     *
	 * @param Any|integer $ComponentOrId
     * @return Dao
     */
    public static function getDao($ComponentOrId)
    {
    	if($ComponentOrId instanceof AnyObject || $ComponentOrId instanceof Link){
    		$id = $ComponentOrId->cid;
    	}
    	else{
    		$id = $ComponentOrId;
    	}

        if(!isset(self::$_registry[$id])){
        	$daoClass = __NAMESPACE__ . '\\' . self::$_map[$id][0];
            self::$_registry[$id] = new $daoClass();
        }
        
        return self::$_registry[$id];
    }
    
    /**
     * Return a dao object for the mapped object.
     *
     * @param Integer
     * @return DaoList
     */
    public static function getList($classId)
    {
    	$id = $classId;
		$table = self::$_map[$id][1];
		return new \Application\Dao\DaoList($table);
    }
}
