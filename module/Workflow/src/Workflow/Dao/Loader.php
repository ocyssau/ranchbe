<?php

namespace Workflow\Dao;

use Workflow\Model;
use Workflow\Dao;

class Loader extends \Application\Dao\Loader
{
	
	/**
	 *
	 * @param integer $classId
	 */
	protected static function _getDaoFactory()
	{
		return new Dao\Factory();
	}
	
	/**
	 *
	 * @param integer $classId
	 */
	protected static function _getModelFactory()
	{
		return new Model\Factory();
	}
}
