<?php
//%LICENCE_HEADER%

namespace Workflow\Dao;

use Application\Dao\Dao;
use Application\Dao\Connexion;
use Rbplm\Signal;

/** SQL_SCRIPT>>
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
CREATE OR REPLACE VIEW anywf AS
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_process
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_activity
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_instance
	UNION
	SELECT id, uid, cid, name, title, ownerId, parentId, parentUid, updateById, updated FROM wf_instance_activity;
 <<*/

/** SQL_DROP>>
 <<*/

class Any extends Dao
{
	/**
	 * @var string
	 */
	public static $table='wf_any';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updateById'=>'updateById',
		'updated'=>'updated',
	);

	/**
	 * Constructor
	 * @param \PDO
	 */
	public function __construct($conn = null)
	{
		if( $conn ){
			$this->connexion = $conn;
		}
		else{
			$this->connexion = Connexion::get();
		}
	} //End of function

	/**
	 * @param Any $mapped
	 * @return array
	 */
	public function bind($mapped)
	{
		return array(
			':id'=>$mapped->getId(),
			':cid'=>$mapped->cid,
			':uid'=>$mapped->getUid(),
			':name'=>$mapped->getName(),
			':ownerId'=>$mapped->getOwner(true),
			':parentId'=>$mapped->getParent(true),
			':parentUid'=>$mapped->getParentUid(),
			':updateById'=>$mapped->getUpdateBy(true),
			':updated'=>$mapped->getUpdated(self::DATE_FORMAT),
		);
	}

	/**
	 * Recursive function
	 * @return Dao
	 */
	public function delete($uid, $withChilds=true)
	{
		Signal::trigger(self::SIGNAL_PRE_DELETE, $this);

		if(!$this->deleteStmt){
			$sql = 'DELETE FROM '.static::$table.' WHERE `uid`=:uid';
			$this->deleteStmt = $this->connexion->prepare($sql);
			$sql = 'SELECT `uid` FROM '.static::$table.' WHERE `parentUid`=:uid';
			$this->selectToDeleteStmt = $this->connexion->prepare($sql);
		}

		$bind = array(':uid'=>$uid);
		$this->selectToDeleteStmt->execute($bind);
		$result = $this->selectToDeleteStmt->fetchAll(\PDO::FETCH_COLUMN);
		foreach($result as $childUid){
			$this->delete($childUid, true);
		}
		$this->deleteStmt->execute($bind);

		Signal::trigger(self::SIGNAL_POST_DELETE, $this);
		return $this;
	} //End of method


	/**
	 * Recursive function
	 * @return Dao
	 */
	public function deleteFromId($id)
	{
		Signal::trigger(self::SIGNAL_PRE_DELETE, $this);

		if(!$this->deleteFromIdStmt){
			$sql = 'DELETE FROM '.static::$table.' WHERE `id`=:id';
			$this->deleteFromIdStmt = $this->connexion->prepare($sql);
		}

		$bind = array(':id'=>$id);
		$this->deleteFromIdStmt->execute($bind);

		Signal::trigger(self::SIGNAL_POST_DELETE, $this);
		return $this;
	} //End of method

}
