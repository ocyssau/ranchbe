<?php
//%LICENCE_HEADER%

namespace Workflow\Model;

use Rbplm\Dao\MappedInterface;
use Rbplm\People;
use DateTime;

class Any extends \Rbplm\Any implements MappedInterface
{
	use \Rbplm\Item, \Rbplm\LifeControl, \Rbplm\Owned, \Rbplm\Mapped;

	/**
	 * @var string
	 */
	protected $title='';

	/**
	 * @param Any $parent
	 */
	public function __construct($properties=null)
	{
		if($properties){
			$this->hydrate($properties);
		}
		$this->cid = static::$classId;
	}

	/**
	 * @param string $name
	 * @return Any
	 */
	public static function init($name="", $parent=null)
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->newUid();
		$obj->updated = new DateTime();

		if( !$name ){
			$name = uniqid();
		}

		$obj->setName($name);
		$obj->setOwner(People\CurrentUser::get());

		if($parent){
			$obj->setParent($parent);
		}

		return $obj;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties \PDO fetch result to load
	 * @return Any
	 */
	public function hydrate( array $properties )
	{
		(isset($properties['id'])) ? $this->id=$properties['id'] : null;
		(isset($properties['uid'])) ? $this->uid=$properties['uid'] : null;
		(isset($properties['cid'])) ? $this->cid=$properties['cid'] : null;
		(isset($properties['name'])) ? $this->name=$properties['name'] : null;
		(isset($properties['parentId'])) ? $this->parentId=$properties['parentId'] : null;
		(isset($properties['parentUid'])) ? $this->parentUid=$properties['parentUid'] : null;
		(isset($properties['ownerId'])) ? $this->ownerId=$properties['ownerId'] : null;
		(isset($properties['updateById'])) ? $this->updateById=$properties['updateById'] : null;
		(isset($properties['updated'])) ? $this->setUpdated( new DateTime($properties['updated']) ) : null;
		(isset($properties['title'])) ? $this->title=$properties['title'] : null;
		return $this;
	}

	/**
	 * @param string $string
	 * @return Any
	 */
	public function setTitle($string)
	{
		$this->title = $string;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
}
