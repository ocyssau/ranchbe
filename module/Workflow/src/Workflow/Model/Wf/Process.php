<?php
// %LICENCE_HEADER%
namespace Workflow\Model\Wf;

use Workflow\Model\Any;
use Exception;

/**
 */
class Process extends Any
{

	/**
	 * @var integer
	 */
	public static $classId = '56acc299ecd6d';

	/**
	 * @var string
	 */
	protected $version = '1.0';

	/**
	 * @var string
	 */
	protected $normalizedName = null;

	/**
	 * @var boolean
	 */
	protected $isValid = false;

	/**
	 * @var boolean
	 */
	protected $isActive = false;

	/**
	 * @var array
	 */
	protected $activities = null;

	/**
	 *
	 * @return string
	 */
	public function getVersion()
	{
		return $this->version;
	}

	/**
	 *
	 * @param string $version
	 * @return Process
	 */
	public function setVersion($version)
	{
		$this->version = $version;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNormalizedName()
	{
		if ( !$this->normalizedName ) {
			$name = str_replace(' ', '_', $this->name . '_' . $this->version);
			$this->normalizedName = preg_replace('/[^0-9A-Za-z\_]/', '', $name);
		}
		return $this->normalizedName;
	}

	/**
	 *
	 * @param string $NormalizedName
	 * @return Process
	 */
	public function setNormalizedName($normalizedName)
	{
		$this->normalizedName = $normalizedName;
		return $this;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isValid($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isValid = $bool;
		}
		else {
			return $this->isValid;
		}
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isActive($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isActive = $bool;
		}
		else {
			return $this->isActive;
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getActivities()
	{
		return $this->activities;
	}

	/**
	 *
	 * @param
	 *        	Activity
	 * @param
	 *        	boolean
	 * @return Process
	 */
	public function addActivity($activity, $bidirectionnal = true)
	{
		if ( !$activity instanceof Activity ) {
			throw new Exception('BAD PARAMETER: $activity must be of type Activity');
		}
		$this->activities[$activity->getUid()] = $activity;
		if ( $bidirectionnal ) {
			$activity->setProcess($this, false);
		}
		return $this;
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Process
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['isActive'])) ? $this->isActive = $properties['isActive'] : null;
		(isset($properties['isValid'])) ? $this->isValid = $properties['isValid'] : null;
		(isset($properties['normalizedName'])) ? $this->normalizedName = $properties['normalizedName'] : null;
		(isset($properties['version'])) ? $this->version = $properties['version'] : null;
		return $this;
	}
} //End of class