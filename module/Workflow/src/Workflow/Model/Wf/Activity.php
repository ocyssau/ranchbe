<?php
// %LICENCE_HEADER%
namespace Workflow\Model\Wf;

use Workflow\Model\Wf\Activity;
use Workflow\Model\Any;
use Rbplm\Sys\Error;

/**
 *
 *
 */
class Activity extends Any
{

	const TYPE_ABSTRACT = 'abstract';

	const TYPE_ACTIVITY = 'activity';

	const TYPE_END = 'end';

	const TYPE_JOIN = 'join';

	const TYPE_SPLIT = 'split';

	const TYPE_STANDALONE = 'standalone';

	const TYPE_START = 'start';

	const TYPE_SWITCH = 'switch';

	/**
	 *
	 * @var integer
	 */
	public static $classId = '56acc299ed0e4';

	/**
	 *
	 * @var string
	 */
	protected $normalizedName = null;

	/**
	 *
	 * @var boolean
	 */
	protected $isInteractive = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isAutorouted = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isAutomatic = false;

	/**
	 *
	 * @var boolean
	 */
	protected $isComment = false;

	/**
	 *
	 * @var float
	 */
	protected $progression = 0;

	/**
	 *
	 * @var array
	 */
	protected $roles = array();

	/**
	 *
	 * @var array
	 */
	protected $outbound = null;

	/**
	 *
	 * @var array
	 */
	protected $inbound = null;

	/**
	 *
	 * @var string
	 */
	protected $type = self::TYPE_ABSTRACT;

	/**
	 *
	 * @var integer
	 */
	protected $expirationTime = null;

	/**
	 * $var array
	 */
	protected $attributes = array();

	/**
	 *
	 * @var \Workflow\Model\Wf\Process
	 */
	protected $process = null;

	protected $processId = null;

	protected $processUid = null;

	/**
	 * Factory method to build activity with type set in $properties.
	 *
	 * @param array $properties
	 * @param \Workflow\Model\Model\CompositComponentInterface $parent
	 * @return Activity
	 */
	public static function factory($type, $init = true)
	{
		switch ($type) {
			case Activity::TYPE_ACTIVITY:
				if ( $init ) return Activity\Activity::init();
				else return new Activity\Activity();
				break;
			case Activity::TYPE_END:
				if ( $init ) return Activity\End::init();
				else return new Activity\End();
				break;
			case Activity::TYPE_JOIN:
				if ( $init ) return Activity\Join::init();
				else return new Activity\Join();
				break;
			case Activity::TYPE_SPLIT:
				if ( $init ) return Activity\Split::init();
				else return new Activity\Split();
				break;
			case Activity::TYPE_STANDALONE:
				if ( $init ) return Activity\Standalone::init();
				else return new Activity\Standalone();
				break;
			case Activity::TYPE_START:
				if ( $init ) return Activity\Start::init();
				else return new Activity\Start();
				break;
			case Activity::TYPE_SWITCH:
				if ( $init ) return Activity\Aswitch::init();
				else return new Activity\Aswitch();
				break;
			case Activity::TYPE_ABSTRACT:
				return self::init();
				break;
			default:
				throw new Exception(Error::BAD_PARAMETER_TYPE . $type, E_USER_WARNING);
		}
	}

	/**
	 * Hydrator.
	 * Load the properties in the mapped object.
	 *
	 * @param array $properties
	 * @return Activity
	 */
	public function hydrate(array $properties)
	{
		parent::hydrate($properties);
		(isset($properties['type'])) ? $this->type = $properties['type'] : null;
		(isset($properties['isAutomatic'])) ? $this->isAutomatic = (bool)$properties['isAutomatic'] : null;
		(isset($properties['isAutorouted'])) ? $this->isAutorouted = (bool)$properties['isAutorouted'] : null;
		(isset($properties['isComment'])) ? $this->isComment = (bool)$properties['isComment'] : null;
		(isset($properties['isInteractive'])) ? $this->isInteractive = (bool)$properties['isInteractive'] : null;
		(isset($properties['normalizedName'])) ? $this->normalizedName = $properties['normalizedName'] : null;
		(isset($properties['processId'])) ? $this->processId = $properties['processId'] : null;
		(isset($properties['processUid'])) ? $this->processUid = $properties['processUid'] : null;
		(isset($properties['progression'])) ? $this->progression = $properties['progression'] : null;
		(isset($properties['expirationTime'])) ? $this->expirationTime = $properties['expirationTime'] : null;

		if ( isset($properties['attributes']) ) {
			$fc = $properties['attributes'][0];
			if ( $fc == '[' || $fc == '{' ) {
				$properties['attributes'] = json_decode($properties['attributes'], true);
			}
			$this->attributes = $properties['attributes'];
		}

		if ( isset($properties['roles']) ) {
			$fc = $properties['roles'][0];
			if ( $fc == '[' || $fc == '{' ) {
				$properties['roles'] = json_decode($properties['roles'], true);
			}
			$this->roles = $properties['roles'];
		}

		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 *
	 * @return string
	 */
	public function getExpirationTime($format = null)
	{
		return $this->expirationTime;
	}

	/**
	 *
	 * @param integer $time
	 *        	Time before expiration in seconds
	 * @return string
	 */
	public function setExpirationTime($time)
	{
		$this->expirationTime = $time;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNormalizedName()
	{
		if ( !$this->normalizedName ) {
			$this->normalizedName = preg_replace('/[^A-Za-z0-9_]/', '', str_replace(' ', '_', $this->name));
		}
		return $this->normalizedName;
	}

	/**
	 *
	 * @param string $string
	 * @return Activity
	 */
	public function setNormalizedName($string)
	{
		$this->normalizedName = $string;
		return $this;
	}

	/**
	 *
	 * @return \Workflow\Model\Wf\Process
	 */
	public function getProcess($asId = false)
	{
		if ( $asId ) {
			return $this->processId;
		}
		else {
			return $this->process;
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 *
	 * @param array $array
	 * @return Activity
	 */
	public function setAttributes($array)
	{
		$this->attributes = $array;
		return $this;
	}

	/**
	 *
	 * @return Activity
	 */
	public function addAttribute($name, $value)
	{
		$this->attributes[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param Process $process
	 * @return void
	 */
	public function setProcess(Process $process, $bidirectionnal = true)
	{
		$this->process = $process;
		$this->processUid = $process->getUid();
		$this->processId = $process->getId();
		if ( $bidirectionnal ) {
			$this->process->addActivity($this, false);
		}
		return $this;
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isInteractive($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isInteractive = $bool;
		}
		else {
			return $this->isInteractive;
		}
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isAutorouted($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isAutorouted = $bool;
		}
		else {
			return $this->isAutorouted;
		}
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isAutomatic($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isAutomatic = $bool;
		}
		else {
			return $this->isAutomatic;
		}
	}

	/**
	 *
	 * @param boolean $bool
	 * @return boolean
	 */
	public function isComment($bool = null)
	{
		if ( is_bool($bool) ) {
			return $this->isComment = $bool;
		}
		else {
			return $this->isComment;
		}
	}

	/**
	 *
	 * @param float $float
	 */
	public function setProgression($float)
	{
		$this->progression = $float;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getProgression()
	{
		return $this->progression;
	}

	/**
	 *
	 * @return array
	 */
	public function getRoles()
	{
		return $this->roles;
	}

	/**
	 *
	 * @param string $role
	 */
	public function addRole($role)
	{
		$this->roles[] = $role;
		return $this;
	}
}

