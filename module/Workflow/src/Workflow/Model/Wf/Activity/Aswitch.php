<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * This class handles activities of type 'switch'
 */
class Aswitch extends Wf\Activity
{

	/**
	 *
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_SWITCH;


	/**
	 * @var string
	 */
	public static $classId = '56acc299ed00a';

	/**
	 * Shape use to generate graph.
	 *
	 * @var string
	 */
	protected $shape = 'diamond';
}
