<?php
namespace Workflow\Model\Wf\Activity;

use Workflow\Model\Wf;

/**
 * This class handles activities of type 'standalone'
 */
class Standalone extends Wf\Activity
{

	/**
	 *
	 * @var string
	 */
	protected $type = Wf\Activity::TYPE_STANDALONE;

	/**
	 *
	 * @var string
	 */
	public static $classId = '56acc299ed078';

	/**
	 * Shape use to generate graph.
	 *
	 * @var string
	 */
	protected $shape = 'hexagon';
}


