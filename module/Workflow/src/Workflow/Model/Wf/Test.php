<?php
//%LICENCE_HEADER%

namespace Workflow\Model\Wf;

use Workflow\Model\Wf;
use Workflow\Dao;

/**
 * @brief __test class for Wf librairy.
 * @include Rbplm/Wf/__test.php
 * 
 */
class Test extends \Application\Test\Test
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a __test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    	define(GALAXIA_COMPILER_SOURCE, RBPLM_LIB_PATH . '/Rbplm/Wf/Compiler');
    }
    
    /**
     */
    function __testRaw(){
    	
    	/*Construct a new process*/
    	$Process = new \Workflow\Model\Wf\Process();
    	$Process->setName('Process__test');
    	$pId = $Process->getUid();
		assert( !empty($pId) );
    	
    	/*Activities definition*/
		$aProperties = array( 
						'name'=>'activity001',
						'normalized_name'=>'activity001-0.1', 
						'description'=>'Start activity', 
						'isInteractive'=>true, 
						'isAutoRouted'=>false, 
						'isComment'=>false);
    	
    	$Activity001 = new Wf\Activity\Start($aProperties);
    	$Activity002 = new Wf\Activity\Activity( array('name'=>'activity002') );
    	$Activity012 = new Wf\Activity\Activity( array('name'=>'activity012') );
    	$Activity022 = new Wf\Activity\Split( array('name'=>'activity022') );
    	$Activity023 = new Wf\Activity\Activity( array('name'=>'activity023') );
    	$Activity024 = new Wf\Activity\Activity( array('name'=>'activity024') );
    	$Activity025 = new Wf\Activity\Join( array('name'=>'activity025') );
    	$Activity003 = new Wf\Activity\End( array('name'=>'activity003') );
    	
    	/*Create bi-directionnal agregation relation*/
    	echo "Create bi-directionnal agregation relation".CRLF;
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	
    	/*Create transitions*/
    	echo "Create transitions".CRLF;
    	$transtion1to2 = new Wf\Transition( $Activity001, $Activity002 );
    	$transtion2to12 = new Wf\Transition( $Activity002, $Activity012 );
    	$transtion12to22 = new Wf\Transition( $Activity012, $Activity022 );
    	$transtion22to23 = new Wf\Transition( $Activity022, $Activity023 );
    	$transtion22to24 = new Wf\Transition( $Activity022, $Activity024 );
    	$transtion23to25 = new Wf\Transition( $Activity023, $Activity025 );
    	$transtion24to25 = new Wf\Transition( $Activity024, $Activity025 );
    	$transtion25to3 = new Wf\Transition( $Activity025, $Activity003 );
    	
    	/*with use of transitions
    	$Process->getTransitions()->add($transtion1to2);
    	$Process->getTransitions()->add($transtion2to12);
    	$Process->getTransitions()->add($transtion12to22);
    	$Process->getTransitions()->add($transtion22to23);
    	$Process->getTransitions()->add($transtion22to24);
    	$Process->getTransitions()->add($transtion23to25);
    	$Process->getTransitions()->add($transtion24to25);
    	$Process->getTransitions()->add($transtion25to3);
    	*/

    	/*Create needed folders for store scripts files*/
    	$Process->initFolders();
    	$Activity001->compile(true);
		$recursiveIterator = new \RecursiveIteratorIterator( $Activity001->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . "\n";
    		$script = $child->getScripts('compiledCode');
    		echo 'Code: ' . $script . "\n";
    		$child->compile();
		}
		
    	/*Create instance from instance constructor*/
    	$Instance = \Workflow\Model\Wf\Instance::start( $Process, new \Workflow\Model\Ged\Document\Version() );
    	$Activity002->isAutomatic(true);
    	$Instance->execute($Activity001);
    	
		$recursiveIterator = new \RecursiveIteratorIterator( $Instance->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
    	
		/*Get runnings activities*/
    	foreach( $Instance->getActivities() as $ra){
			if( $ra->status == \Workflow\Model\Wf\Instance_Activity::STATUS_RUNNING ){
				$running = $ra;
				break;
			}
		}
		$running->execute();
		
		/*Get runnings activities*/
		/*
		$startTime = microtime(true);
		foreach( $Instance->getActivities() as $ra){
			if( $ra->status == \Workflow\Model\Wf\Instance_Activity::STATUS_RUNNING ){
				//echo 'Runnings activity: ' . $ra->getName() . "\n";
				$running = $ra;
			}
		}
		echo microtime(true) - $startTime . " S \n";
		*/
		//$startTime = microtime(true);
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$ra = $queue->dequeue();
    		echo 'Runnings activity: ' . $ra->getName() . "\n";
			$ra->execute();
		}
		//echo microtime(true) - $startTime . " S \n";
		
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$queue = $Instance->getRunningActivities();
		while( !$queue->isEmpty() ){
			$a = $queue->dequeue();
			echo 'Runnings activity: ' . $a->getName() . "\n";
		}
		
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity003')->execute();
		
		var_dump( $Instance->status, $Instance->getUid() );
		
		try{
			$Instance->execute( $Activity001 );
		}catch(\Exception $e){
			echo $e->getMessage() . "\n";
		}
		
    } //End of method
    
    
    /**
     */
    function _Test_Tutorial(){
    	
    	/*
    	 * First, construct a new process.
    	 */
    	$Process = Wf\Process::init();
    	$Process->setName('Process__test');
    	$Process->initFolders();
    	
    	/* And add some activities.
    	 * Activities, as other Rbplm objects, have type \Workflow\Model\AnyObject, and as anyobject, take array of properties in first parameter
    	 * and parent on second.
    	 * But, basicaly, parent is not used, and may be use to put workflow anyobject in organizational unit for example.
    	 */
    	$Activity001 = Wf\Activity\Start::init( 'activity001' );
    	$Activity002 = Wf\Activity\Activity::init( 'activity002' );
    	$Activity012 = Wf\Activity\Activity::init( 'activity012' );
    	$Activity022 = Wf\Activity\Split::init( 'activity022' );
    	$Activity023 = Wf\Activity\Activity::init( 'activity023' );
    	$Activity024 = Wf\Activity\Activity::init( 'activity024' );
    	$Activity025 = Wf\Activity\Join::init( 'activity025' );
    	$Activity003 = Wf\Activity\End::init( 'activity003' );
    	
    	/*
    	 * Create bi-directionnal agregation relation.
    	 * The activities must be attached to a process.
    	 * The method setProcess create a relation from activities to process but too from
    	 * process to activity.
    	 * You may retrieve activities from a process with method \Workflow\Model\Wf\Process::getActivities()
    	 * 
    	 */
    	echo "Create bi-directionnal agregation relation".CRLF;
    	$Activity001->setProcess($Process)->compile();
    	$Activity002->setProcess($Process)->compile();
    	$Activity012->setProcess($Process)->compile();
    	$Activity022->setProcess($Process)->compile();
    	$Activity023->setProcess($Process)->compile();
    	$Activity024->setProcess($Process)->compile();
    	$Activity025->setProcess($Process)->compile();
    	$Activity003->setProcess($Process)->compile();
    	
    	/*Create transitions*/
    	echo "Create transitions".CRLF;
    	$transtion1to2 = Wf\Transition::factory( $Activity001, $Activity002 );
    	$transtion2to12 = Wf\Transition::factory( $Activity002, $Activity012 );
    	$transtion12to22 = Wf\Transition::factory( $Activity012, $Activity022 );
    	$transtion22to23 = Wf\Transition::factory( $Activity022, $Activity023 );
    	$transtion22to24 = Wf\Transition::factory( $Activity022, $Activity024 );
    	$transtion23to25 = Wf\Transition::factory( $Activity023, $Activity025 );
    	$transtion24to25 = Wf\Transition::factory( $Activity024, $Activity025 );
    	$transtion25to3 = Wf\Transition::factory( $Activity025, $Activity003 );
    	
    	/*The activities list may be retrieve from process : */
    	foreach($Process->getActivities() as $Activity){
    		var_dump($Activity->getName());
    	}
    	
    	echo '------------------------------------------' . "\n";
		
		/*
		 * HOW TO RUN THE PROCESS.
		 */
		
    	/* First create a process instance.
    	 * The process instance is attach to a process definition.
    	 * It add informations about execution, like start date, owner, and running activities.
    	 * To create a new instance, use start method. Dont call directly the constructor.
    	 * Here the instance is attached to a document.
    	 * 
    	 * A instance of workflow may be attach to any object with type \Workflow\Model\AnyObject.
    	 * To get a attach anyobject use method getanyobject.
    	 * 
    	 */
    	$Instance = Wf\Instance::start($Process);
    	$attachedanyobject = $Instance->getanyobject();
    	$attachedanyobject->setName('my attached document');
    	
    	/* A new process instance has name set to "noname".
    	 * You must set a name.
    	 */
    	echo 'Initiale name of a new process: ' . $Instance->getName() . "\n";
    	echo '------------------------------------------' . "\n";
    	
    	
    	$Instance->setName( $attachedanyobject->getName() );
    	
    	/*
    	 * Activity may be set as automatic. In this case, when previous activity is completed, the activity is automaticly executed.
    	 */
    	$Activity002->isAutomatic(true);
    	
    	/*
    	 * To execute a activity:
    	 * Execute Activity001 and automatic activity Activity002
    	 */
    	$Activity001Instance = $Instance->execute($Activity001);
    	
    	/*
    	 * Add a comment to the activity instance.
    	 * Comment provide a fluent interface to chain setters.
    	 */
    	$Activity001Instance->getComment()->setTitle('A new comment')->setBody('My comment body');
    	
    	echo '------------------------------------------' . "\n";
    	echo 'Comment title: ' . $Activity001Instance->getComment()->getTitle() . "\n";
    	echo 'Comment body: ' . $Activity001Instance->getComment()->getBody() . "\n";
    	
    	/*
    	 * Each executed activity is added to a second structure in process instance.
    	 * Show it:
    	 */
    	echo '------------------------------------------' . "\n";
    	echo 'Tree of process activities instances execution:' . "\n";
		$recursiveIterator = new \RecursiveIteratorIterator( $Instance->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $child){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $child->getName() . ' : ' . $child->status . "\n";
		}
		
    	echo '------------------------------------------' . "\n";
		/*
		 * To get runnings activities, simply do a foreach.
		 * The runnings activities are put in a queue
		 * 
		 */
		$queue = $Instance->getRunningActivities();
    	foreach( $queue as $a){
    		echo 'Is running: '. $a->getName() . "\n";
		}
		
		/*
		 * Its easy to execute all activities that must be.
		 */
		while( !$queue->isEmpty() ){
			echo 'Execute :' . "\n";
			$queue->dequeue()->execute();
		}
		
		echo '------------------------------------------' . "\n";
		/* 
		 * In followings code, execute each activity one to one.
		 */
		$Instance->getActivities()->getByName('activity022')->execute();
		$Instance->getActivities()->getByName('activity023')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		$Instance->getActivities()->getByName('activity024')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		/*
		 * If try to re-execute a completed activity, no effects!
		 */
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		$Instance->getActivities()->getByName('activity025')->execute();
		
		
		/*
		 * The end activity set process to completed and set the ended date
		 */
		$Instance->getActivities()->getByName('activity003')->execute();
		
		echo '------------------------------------------' . "\n";
		echo 'status of process after end activity :' . $Instance->status . "\n";
		echo 'Ended date of process :' . \Workflow\Model\Sys\Date::format($Instance->ended) . "\n";
		
		/*
		 * If try to execute activity on completed instance, a exception is throws.
		 */
		try{
			echo '------------------------------------------' . "\n";
			echo 'Try to execute activity on completed process:' . "\n";
			$Instance->execute( $Activity001 );
		}catch(\Exception $e){
			echo 'Error message:' . "\n";
			echo $e->getMessage() . "\n";
		}
    } //End of method
    
    
    /**
     * How to define roles and resource ACLs on activities.
     * 
     */
    public function __testRoleAcl(){
    	/*Process definition*/
    	/*For use acl it is require to define a tree attached to the root node \Workflow\Model\Org\Root*/
		$RootOu = \Workflow\Model\Org\Root::singleton();
    	$Process = new \Workflow\Model\Wf\Process( array('name'=>'Process__test'), $RootOu );
    	$Activity001 = new \Workflow\Model\Wf\Activity\Start( array('name'=>'activity001'), $Process);
    	$Activity002 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity002'), $Process);
    	$Activity012 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity012'), $Process);
    	$Activity022 = new \Workflow\Model\Wf\Activity\Split( array('name'=>'activity022'), $Process);
    	$Activity023 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity023'), $Process);
    	$Activity024 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity024'), $Process );
    	$Activity025 = new \Workflow\Model\Wf\Activity\Join( array('name'=>'activity025'), $Process );
    	$Activity003 = new \Workflow\Model\Wf\Activity\End( array('name'=>'activity003'), $Process );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	/*Init roles and resources*/
    	$CurrentUser = \Workflow\Model\People\CurrentUser::get();
    	\Workflow\Model\Acl\Initializer::initRole( $CurrentUser );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity001 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity002 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity012 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity022 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity023 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity024 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity025 );
    	\Workflow\Model\Acl\Initializer::initResource( $Activity003 );
    	
		//Get ACL object from singleton to get rules define internaly to People objects
		$Acl = \Workflow\Model\Acl\Acl::singleton();
		
		/* __test acl on inherit.
		 * Note that, because $Process is parent of Activities, it may be use as a resource too without need to init it.
		 */
		$Acl->deny($CurrentUser->getUid(), array($Process->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Activity001->getUid(), 'exec') == false);
		
		$Acl->allow($CurrentUser->getUid(), array($Activity001->getUid()), 'exec');
		assert($Acl->isAllowed($CurrentUser->getUid(), $Activity001->getUid(), 'exec') == true);
    }
    
    
    
	/**
	 * HOW TO GENERATE A GRAPH WITH GRAPHVIZ.
	 * 
	 */
    public function __testTutorialGraphviz(){
    	/*Process definition*/
		$RootOu = \Workflow\Model\Org\Root::singleton();
    	$Process = new \Workflow\Model\Wf\Process( array('name'=>'Process__test'), $RootOu );
    	$Activity001 = new \Workflow\Model\Wf\Activity\Start( array('name'=>'activity001'), $Process);
    	$Activity002 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity002'), $Activity001);
    	$Activity012 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity012'), $Activity002);
    	$Activity022 = new \Workflow\Model\Wf\Activity\Split( array('name'=>'activity022'), $Activity012);
    	$Activity023 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity023'), $Activity022);
    	$Activity024 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity024'), $Activity022 );
    	$Activity025 = new \Workflow\Model\Wf\Activity\Join( array('name'=>'activity025'), $Activity023 );
    	$Activity003 = new \Workflow\Model\Wf\Activity\End( array('name'=>'activity003'), $Activity025 );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	$Activity023->isInteractive(true);
    	$Activity002->isAutomatic(true);
    	
    	/*Instanciate a new graph*/
		if(defined('GRAPHVIZ_BIN_DIR') && GRAPHVIZ_BIN_DIR){
	    	$graphViz = new \Workflow\Model\Wf\GraphViz(array(), GRAPHVIZ_BIN_DIR);
		}
		else{
	    	$graphViz = new \Workflow\Model\Wf\GraphViz();
		}
    	$graphViz->setPid ( $Process->getNormalizedName() );
    	
    	$interactiveFontColor = 'pink';
    	$notinteractiveFontColor = 'black';
    	$automaticColor = 'blue';
    	$notautomaticColor = 'black';
    	$nodebgColor = 'white';
    	$nodeEdgesColor = 'black';
    	$bgColor = 'transparent';
    	$graphViz->addAttributes(array('bgcolor'=>$bgColor));
    	
    	/*Create an new iterator to parse the graph of activities*/
		$recursiveIterator = new \RecursiveIteratorIterator( $Process->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		$parents = array();
		foreach($recursiveIterator as $activity){
			if( $activity->isInteractive() ){
				$fontcolor = $interactiveFontColor;
			}
			else{
				$fontcolor = $notinteractiveFontColor;
			}
			$name = $activity->getName();
			$shape = $activity->shape;
			$url = 'foourl?uid=' . $activity->getUid();
			$graphViz->addNode ( $name, array ('URL'=>$url, 'label'=>$name, 'shape'=>$shape, 'fontcolor'=>$fontcolor, 'style'=>'filled', 'fillcolor'=>$nodebgColor, 'color'=>$nodeEdgesColor) );
			
			$level = $recursiveIterator->getDepth();
			if( $parents[$level - 1] ){
				$parentName = $parents[$level - 1];
				if( $activity->isAutomatic() ){
					$color = $automaticColor;
				}
				else{
					$color = $notautomaticColor;
				}
				$graphViz->addEdge( array( $parentName => $activity->getName() ), array ('color' => $color ) );
			}
			$parents[$level] = $activity->getName();
			echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName()  . ' Parent: ' . $parentName ."\n";
		}
    	
		$toDirectory = $Process->getFolders('graph');
		$graphViz->imageAndMap($toDirectory, 'png');
    }
    
    public function __testSerialize(){
		$RootOu = \Workflow\Model\Org\Root::singleton();
    	$Process = new \Workflow\Model\Wf\Process( array('name'=>'Process__test'), $RootOu );
    	$actOu = new \Workflow\Model\Org\Unit( array('name'=>'activities'), $Process);
    	$Activity001 = new \Workflow\Model\Wf\Activity\Start( array('name'=>'activity001'), $actOu);
    	$Activity002 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity002'), $actOu);
    	$Activity012 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity012'), $actOu);
    	$Activity022 = new \Workflow\Model\Wf\Activity\Split( array('name'=>'activity022'), $actOu);
    	$Activity023 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity023'), $actOu);
    	$Activity024 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>'activity024'), $actOu );
    	$Activity025 = new \Workflow\Model\Wf\Activity\Join( array('name'=>'activity025'), $actOu );
    	$Activity003 = new \Workflow\Model\Wf\Activity\End( array('name'=>'activity003'), $actOu );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	//var_dump( serialize($Activity025) );
    	//var_dump( serialize($Process) );
    	
    	\Workflow\Model\Wf\Serializer::export($Process, './tmp/serialize__test.xml');
    	$Process2 = \Workflow\Model\Wf\Serializer::import( './tmp/serialize__test.xml', $actOu );
    	
    	assert( $Process2->getUid() ==  $Process->getUid() );
    	assert( $Process2->getName() ==  $Process->getName() );
    	assert( $Process2->getActivities()->count() ==  $Process->getActivities()->count() );
    	assert( $Process2->getActivities()->getByIndex(0)->getName() ==  $Process->getActivities()->getByIndex(0)->getName() );
    	assert( $Process2->getActivities()->getByIndex(7)->getUid() ==  $Process->getActivities()->getByIndex(7)->getUid() );
    	
		$recursiveIterator = new \RecursiveIteratorIterator( $Process2->getChild(), \RecursiveIteratorIterator::SELF_FIRST );
		foreach($recursiveIterator as $activity){
    		echo str_repeat("  ", $recursiveIterator->getDepth() ) . $activity->getName() . "\n";
		}
    	
		\Workflow\Model\Wf\Graph\Generator::generate($Process2);
    	
    }
    
    
    public function __testDao(){
    	\Workflow\Model\Dao\Pg\ClassDao::singleton()->setConnexion( \Workflow\Model\Dao\Connexion::get() );
    	
		$RootOu = \Workflow\Model\Org\Root::singleton();
		
		$processName = uniqid('Process__test');
    	$Process = new \Workflow\Model\Wf\Process( array('name'=>$processName), $RootOu );
    	$actOu = new \Workflow\Model\Org\Unit( array('name'=>uniqid('activities')), $RootOu);
    	
    	$Activity001 = new \Workflow\Model\Wf\Activity\Start( array('name'=>uniqid('activity001')), $actOu);
    	$Activity002 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>uniqid('activity002')), $actOu);
    	$Activity012 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>uniqid('activity012')), $actOu);
    	$Activity022 = new \Workflow\Model\Wf\Activity\Split( array('name'=>uniqid('activity022')), $actOu);
    	$Activity023 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>uniqid('activity023')), $actOu);
    	$Activity024 = new \Workflow\Model\Wf\Activity\Activity( array('name'=>uniqid('activity024')), $actOu );
    	$Activity025 = new \Workflow\Model\Wf\Activity\Join( array('name'=>uniqid('activity025')), $actOu );
    	$Activity003 = new \Workflow\Model\Wf\Activity\End( array('name'=>uniqid('activity003')), $actOu );
    	$Activity001->setProcess($Process);
    	$Activity002->setProcess($Process);
    	$Activity012->setProcess($Process);
    	$Activity022->setProcess($Process);
    	$Activity023->setProcess($Process);
    	$Activity024->setProcess($Process);
    	$Activity025->setProcess($Process);
    	$Activity003->setProcess($Process);
    	$Activity001->getChild()->add($Activity002);
    	$Activity002->getChild()->add($Activity012);
    	$Activity012->getChild()->add($Activity022);
    	$Activity022->getChild()->add($Activity023);
    	$Activity022->getChild()->add($Activity024);
    	$Activity023->getChild()->add($Activity025);
    	$Activity024->getChild()->add($Activity025);
    	$Activity025->getChild()->add($Activity003);
    	$Activity025->getParents()->add($Activity023);
    	$Activity025->getParents()->add($Activity024);
    	$Process->getChild()->add($Activity001);
    	
    	/*BE CAREFUL to order of save. Parents must be always save first*/
    	
    	try{
	    	$RootOuDao = new \Workflow\Model\Org\RootDaoPg();
			$RootOuDao->setConnexion( \Workflow\Model\Dao\Connexion::get() );
			$RootOuDao->save( $RootOu );
    	}
    	catch(\Exception $e){
    		//nothing
    	}
    	
    	$OuDao = new \Workflow\Model\Org\UnitDaoPg( array(), \Workflow\Model\Dao\Connexion::get() );
    	
    	/*Clean previous activities*/
    	$List = $OuDao->newList();
    	$List->suppress("path::ltree <@ '" . \Workflow\Model\Dao\Pg::pathToSys($actOu->getPath()) . "'");
    	
    	/*Save OU*/
		$OuDao->save( $actOu );
		
    	
    	$ProcessDao = new \Workflow\Model\Wf\ProcessDaoPg();
		$ProcessDao->setConnexion( \Workflow\Model\Dao\Connexion::get() );
		$ProcessDao->save( $Process );
    	
    	foreach( $Process->getLinks() as $link ){
    		var_dump($link->getName(), get_class($link) );
    		if( is_a($link, '\Workflow\Model\Model\Collection') ){
    			foreach($link as $subLink){
		    		var_dump($subLink->getName(), get_class($subLink));
		    		$this->_saveActivity($subLink);
    			}
    		}
    		else{
    			$this->_saveActivity($link);
    		}
    	}
		$uid = $Process->getUid();
		
		/***************************** __test LOAD ********************************/
    	$ProcessDao = new \Workflow\Model\Wf\ProcessDaoPg();
		$ProcessDao->setConnexion( \Workflow\Model\Dao\Connexion::get() );
    	
		$Process = new \Workflow\Model\Wf\Process();
		$ProcessDao->loadFromUid($Process, $uid, array(), true);
		assert( $Process->getName() == $processName );
		assert( $Process->isLoaded() == true );
		assert( \Workflow\Model\Uuid::compare($Process->getUid(), $uid) );
		assert( \Workflow\Model\Uuid::compare( $Process->parentId, $RootOu->getUid() ) );
		
		/** LOAD THE PARENT **/
		\Workflow\Model\Dao\Pg\Loader::loadParent($Process);
		assert( $Process->getParent()->getName() == $RootOu->getName() );
		
		/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/		
		$List = new \Workflow\Model\Dao\Pg\DaoList( array('table'=>'view_wf_Activity\links') );
		$List->setConnexion( \Workflow\Model\Dao\Connexion::get() );
		$List->load("lparent='$uid'");
		
		//convert list to collection
		$collection = new \Workflow\Model\Model\Collection();
		$List->loadInCollection( $Process->getActivities() );
		
		foreach($Process->getActivities() as $A){
			var_dump( $A->getName() );
		}
		
		/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
		$Process = new \Workflow\Model\Wf\Process();
		$ProcessDao->loadFromUid($Process, $uid, array(), true);
		\Workflow\Model\Dao\Pg\Loader::loadParent($Process);
		$List = $ProcessDao->getActivities($Process);
		
		$ActivityDao = new \Workflow\Model\Wf\ProcessDaoPg( array(), \Workflow\Model\Dao\Connexion::get() );
		
		$CollectionListBridge = new \Workflow\Model\Model\CollectionListBridge($Process->getActivities(), $List);
		foreach($CollectionListBridge as $A){
			\Workflow\Model\Dao\Pg\Loader::loadParent($A);
			var_dump( $A->getName() , $A->getParent()->getName() );
		}
    }
    
    protected function _saveActivity( $Activity )
    {
    	$ActivityDao = new \Workflow\Model\Wf\ActivityDaoPg();
		$ActivityDao->setConnexion( \Workflow\Model\Dao\Connexion::get() );
		$ActivityDao->save( $Activity );
    }
    
    public function Test_Tutorial()
    {
    	$processName = "Process5537df860d77b";
    	$Process = new Wf\Process();
    	Dao\Factory::get()->getDao($Process)->loadFromName($Process, $processName);
    	
    	//Create instance of process
    	$processInstance = Wf\Instance::start($Process);
    	Dao\Factory::get()->getDao($processInstance)->save($processInstance);
    	
    	//Find Start activity and run it
    	$Start = new Activity\Start();
    	Dao\Factory::get()->getDao($Start)->load($Start, "type='start' AND processId=".$Process->getId());
    	$ActivityInstance = $processInstance->execute($Start);
    	Dao\Factory::get()->getDao($ActivityInstance)->save($ActivityInstance);

    	//add a listener to this instance
    	//Signal::connect($ActivityInstance, 'runactivity.pre', array($code, 'onRun'));
    	
    	//Get next candidates activities
    	$nextList = Dao\Factory::get()->getDao($processInstance)->getNextCandidates($Start->getId());

    	//Create activities instance and set running status on next activities
    	foreach($nextList as $properties){
    		$nextActivity = new Activity($properties);
    		$nextStatus = $properties['nextStatus'];
    		$nextInstance = Instance\Activity::start($nextActivity, $processInstance);
    		Dao\Factory::get()->getDao($nextInstance)->save($nextInstance);
    	}
    	
    	//now get the runnings activities and execute it
    	$runningList = Dao\Factory::get()->getDao($processInstance)->getRunningActivities($processInstance->getId());
    	foreach($runningList as $properties){
    		$runningInstanceActivity = new Instance\Activity($properties);
    		$type = $properties['type'];
    		
    		//set the source activity object
    		$activity = Activity::factory($type, false);
    		Dao\Factory::get()->getDao($activity)->loadFromId($activity, $properties['activityId']);
    		$runningInstanceActivity->setActivity($activity);
    		$runningInstanceActivity->execute();
    		
    		//save the state of instance
    		Dao\Factory::get()->getDao($runningInstanceActivity)->save($runningInstanceActivity);
    		
    		var_dump($runningInstanceActivity);
    	}
    	
    	
    	die;
    	 
    	/*BE CAREFUL to order of save. Parents must be always save first*/
    	 
    	/***************************** __test LOAD ********************************/
    	$ProcessDao = new \Workflow\Model\Wf\ProcessDaoPg();
    	$ProcessDao->setConnexion( \Workflow\Model\Dao\Connexion::get() );
    	 
    	$Process = new \Workflow\Model\Wf\Process();
    	$ProcessDao->loadFromUid($Process, $uid, array(), true);
    	assert( $Process->getName() == $processName );
    	assert( $Process->isLoaded() == true );
    	assert( \Workflow\Model\Uuid::compare($Process->getUid(), $uid) );
    	assert( \Workflow\Model\Uuid::compare( $Process->parentId, $RootOu->getUid() ) );
    
    	/** LOAD THE PARENT **/
    	\Workflow\Model\Dao\Pg\Loader::loadParent($Process);
    	assert( $Process->getParent()->getName() == $RootOu->getName() );
    
    	/** LOAD THE lchild ACTIVITY, EXPLICIT STYLE **/
    	$List = new \Workflow\Model\Dao\Pg\DaoList( array('table'=>'view_wf_Activity\links') );
    	$List->setConnexion( \Workflow\Model\Dao\Connexion::get() );
    	$List->load("lparent='$uid'");
    
    	//convert list to collection
    	$collection = new \Workflow\Model\Model\Collection();
    	$List->loadInCollection( $Process->getActivities() );
    
    	foreach($Process->getActivities() as $A){
    		var_dump( $A->getName() );
    	}
    
    	/** LOAD THE lchild ACTIVITY, WITH A CollectionListBridge **/
    	$Process = new \Workflow\Model\Wf\Process();
    	$ProcessDao->loadFromUid($Process, $uid, array(), true);
    	\Workflow\Model\Dao\Pg\Loader::loadParent($Process);
    	$List = $ProcessDao->getActivities($Process);
    
    	$ActivityDao = new \Workflow\Model\Wf\ProcessDaoPg( array(), \Workflow\Model\Dao\Connexion::get() );
    
    	$CollectionListBridge = new \Workflow\Model\Model\CollectionListBridge($Process->getActivities(), $List);
    	foreach($CollectionListBridge as $A){
    		\Workflow\Model\Dao\Pg\Loader::loadParent($A);
    		var_dump( $A->getName() , $A->getParent()->getName() );
    	}
    }
    
    
    
    	
} //End of class

