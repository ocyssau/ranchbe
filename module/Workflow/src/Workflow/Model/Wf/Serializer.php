<?php


namespace Workflow\Model\Wf;

/**
 * @brief Export/Import a process and his anyobjects.
 * 
 * Serializer class to save full process definition (process, activities, codes, templates) into xml file.
 *
 */
class Serializer{
	

	/**
	 * Creates an XML representation of a process.
	 * 
	 * @param \Workflow\Model\Wf\Process $Process
	 */
	public static function export( \Workflow\Model\Wf\Process $Process, $file ) 
	{
		$XmlDoc = new \DOMDocument('1.0');
		$XmlDoc->formatOutput = true;
		$processNode = $XmlDoc->appendChild( $XmlDoc->createElement( 'process', $Process->getUid() ) );

		/*Serialize process*/
		$sxmlNode = dom_import_simplexml( new \SimpleXMLElement( $Process->xmlSerialize() ) );
		$importSxmlNode = $XmlDoc->importNode($sxmlNode, true);
		$importSxmlNode = $processNode->appendChild( $importSxmlNode );
		
		/*Include shared code*/
		$sharedCodeNode = $processNode->appendChild( $XmlDoc->createElement( 'sharedCode', htmlspecialchars($Process->description) ) );
		
		/*Include activities*/
		$Activities = $Process->getActivities();
		$Activity = $Activities->getByIndex(0);
		
		/**/
		$sharedCodeFileName = $Activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_SHARED);
		$sharedCode = file_get_contents($sharedCodeFileName);
		$sharedCdata = $XmlDoc->createCDATASection( $sharedCode );
		$sharedCodeNode->appendChild($sharedCdata);
		
		$transitionsNode = $processNode->appendChild( $XmlDoc->createElement( 'transitions' ) );
		
		/* Get all activities and serialize it */
		foreach($Activities as $activity){
			$activityNode = $processNode->appendChild( $XmlDoc->createElement( 'activity', $activity->getUid() ) );
			
			$codeNode = $activityNode->appendChild( $XmlDoc->createElement( 'code' ) );
			$codeFileName = $activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_CODE_SOURCE);
			$codeCode = file_get_contents($codeFileName);
			$codeNode->appendChild( $XmlDoc->createCDATASection( $codeCode ) );
			
			if( $activity->isInteractive() ){
				$tplNode = $activityNode->appendChild( $XmlDoc->createElement( 'template' ) );
				$tplFileName = $activity->getScripts(\Workflow\Model\Wf\Activity::SCRIPTS_TEMPLATE_SOURCE);
				$tplCode = file_get_contents($tplFileName);
				$tplNode->appendChild( $XmlDoc->createCDATASection( $tplCode ) );
			}
			
			/*Serialize activity*/
			$sxmlNode = dom_import_simplexml( new \SimpleXMLElement( $activity->xmlSerialize() ) );
			$importSxmlNode = $XmlDoc->importNode($sxmlNode, true);
			$importSxmlNode = $activityNode->appendChild( $importSxmlNode );
			
			/*Include transitions*/
			foreach( $activity->getChild() as $child ){
				$transitionNode = $transitionsNode->appendChild( $XmlDoc->createElement( 'transition' ) );
				$transitionNode->appendChild( $XmlDoc->createElement( 'from', $activity->getUid() ) );
				$transitionNode->appendChild( $XmlDoc->createElement( 'to', $child->getUid() ) );
			}
		}
		
		return $XmlDoc->save($file);
		
	}

	/**
	 * Creates an XML representation of a process.
	 * 
	 * @param string				$file	Input Xml file
	 * @param \Workflow\Model\AnyObject $Parent	Parent anyobject container where put the process and activities.
	 */
	public static function import( $file, \Workflow\Model\AnyObject $Parent=null ) {
		$XmlDoc = new \DOMDocument();
		$XmlDoc->load( $file );
		$xpath = new \DOMXPath($XmlDoc);
		
		/* Build the process */
		$entries = $xpath->query( '//process/properties/*' );
		$properties = array();
		foreach($entries as $property){
			$properties[$property->tagName] = $property->nodeValue;
		}
		$Process = new \Workflow\Model\Wf\Process($properties, $Parent);
		
		/* Build each activity */
		$activities = $xpath->query( '//process/activity' );
		for ($i = 1; $i <= $activities->length; $i++) {
			$properties = array();
			$property = null;
			foreach( $xpath->query( "//process/activity[$i]/properties/*" ) as $property ){
				$properties[$property->tagName] = $property->nodeValue;
			}

			$Activity = \Workflow\Model\Wf\Activity::factory($properties, $Parent);
			$Activity->setProcess($Process);
			
			if( $Activity->type == \Workflow\Model\Wf\Activity::TYPE_JOIN ){
		        foreach( $Process->getActivities() as $act ){
		        	if($act->getChild()->contains($Activity)){
		        		$Activity->getParents()->add($act);
		        	}
		        }
			}
		}
		
		$transitions = $xpath->query( '//process/transitions/transition' );
		for ($i = 1; $i <= $transitions->length; $i++) {
			$properties = array();
			$property = null;
			foreach( $xpath->query( "//process/transitions/transition[$i]/*" ) as $property ){
				$properties[$property->tagName] = $property->nodeValue;
			}
			
			$from = $properties['from'];
			$to = $properties['to'];
			
			$fromActivity = $Process->getActivities()->getByUid($from);
			$toActivity = $Process->getActivities()->getByUid($to);
			$fromActivity->getChild()->add($toActivity);
		}
		
		return $Process;
	}
	
	
} //End of class


