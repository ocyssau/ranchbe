<?php
namespace Workflow\Model\Wf\Instance;

use Workflow\Model\Wf;
use Rbplm\Signal;

/**
 * This class handles activities of type 'standalone'
 */
class Standalone extends Activity
{

	/**
	 *
	 * @var integer
	 */
	public static $classId = '56acc299ed295';

	/**
	 * Prepare and execute code of activity
	 * this method is call by instance::sendTo methods to execute automatic activity
	 * she dont must be directly call to create a new instance.
	 *
	 * @return Standalone
	 */
	public function execute()
	{
		if ( $this->status != self::STATUS_RUNNING ) {
			return;
		}

		$e = Signal::trigger(self::SIGNAL_RUN_PRE, $this);
		if ( $e->hasError() ) {
			return $e;
		}

		$this->ended = new DateTime();
		$this->status = self::STATUS_COMPLETED;

		$e = Signal::trigger(self::SIGNAL_RUN_POST, $this);
		if ( $e->hasError() ) {
			return $e;
		}

		return $this;
	}
}


