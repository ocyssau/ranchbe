<?php
namespace Zend\Filter;

class StringToArray extends AbstractFilter
{

	/**
	 * Input is a string with phrase/words separate by ","
	 * If the value provided is non-scalar, the value will remain unfiltered
	 *
	 * @param string $value
	 * @return int|mixed
	 */
	public function filter($value)
	{
		if ( !is_scalar($value) ) {
			return $value;
		}
		$value = (string)$value;
		$to = preg_split('/\s*(,|\s)\s*/', $values);
		return $to;
	}
}
