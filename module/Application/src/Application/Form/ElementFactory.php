<?php
namespace Application\Form;

use Rbs\Space\Factory as DaoFactory;
use Workflow\Model\Wf;
use Rbplm\Org\Project;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Category;
use Rbplm\People\User;
use Rbs\Ged\Document\IndiceDao as DocIndices;

class ElementFactory
{

	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @var DaoFactory
	 */
	protected $form;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct($form, $factory = null)
	{
		$this->form = $form;
		$this->factory = $factory;
	}

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function setDaoFactory($factory)
	{
		$this->factory = $factory;
	}

	/**
	 *
	 * $params = array(
	 * 'where',
	 * 'name',
	 * 'label',
	 * 'value',
	 * 'multiple',
	 * 'returnName',
	 * 'required',
	 * 'advSelect',
	 * 'display_both',
	 * 'disabled',
	 * 'fieldId',
	 * );
	 *
	 * @param array $list
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function select($list, $params)
	{
		$form = $this->form;

		(isset($params['name'])) ? $name = $params['name'] : $name = uniqid();
		(isset($params['fieldId'])) ? $fieldId = $params['fieldId'] : $fieldId = $name;
		(isset($params['label'])) ? $label = $params['label'] : $label = $name;
		(isset($params['class'])) ? $class = $params['class'] : $class = 'form-control rb-select';
		(isset($params['size'])) ? $size = $params['size'] : $size = 1;
		(isset($params['disabled'])) ? $disabled = $params['disabled'] : $disabled = 'null';
		(isset($params['multiple'])) ? $multiple = $params['multiple'] : $multiple = false;
		(isset($params['required'])) ? $required = $params['required'] : $required = false;
		(isset($params['value'])) ? $value = $params['value'] : $value = null;
		(isset($params['displayBoth'])) ? $displayBoth = $params['displayBoth'] : $displayBoth = false;
		(isset($params['filters'])) ? $filters = $params['filters'] : $filters = array();
		(isset($params['validators'])) ? $validators = $params['validators'] : $validators = array();

		$selectSet = array();
		$selectSet[null] = ''; // Leave a blank option for default none selected. Not useful for advanced select

		if ( is_array($list) ) {
			foreach( $list as $key => $values ) {
				if ( $displayBoth ) {
					$selectSet[$key] = $key . '-' . $values;
				}
				else {
					$selectSet[$key] = $values;
				}
			}
		}

		// Construct object for normal select
		$form->add(array(
			'name' => $name,
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => 'Click to select',
				'class' => $class,
				'data-where' => '',
				'data-op' => '',
				'id' => $fieldId,
				$disabled => $disabled,
				'size' => $size,
				'multiple' => $multiple,
			),
			'options' => array(
				'label' => $label,
				'value_options' => $selectSet,
			)
		));

		$form->getInputFilter()->add(array(
			'name' => $name,
			'required' => $required,
			'filters'=>$filters,
			'validators'=>$validators
		));
	}
	// End of method


	/**
	 * @param array $params
	 * @param Form $form
	 * @param unknown_type $spaceName
	 * @param string $validation
	 */
	function selectDocumentVersion($params)
	{
		$form = $this->form;

		// Get list of indice
		$factory = $this->factory;
		$dao = new DocIndices($factory->getConnexion());
		if ( empty($params['value']) ) $params['value'] = 1;
		$stmt = $dao->getIndices();
		while( $current = $stmt->fetch() ) {
			$list[$current['id']] = $current['name'];
		}
		$this->select($list, $params, $form);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectUser($params)
	{
		$form = $this->form;
		$factory = $this->factory;
		$list = $factory->getList(User::$classId);
		$list->load("1=1 LIMIT 100");
		$selectSet = array();

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;

		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['id']] = $item['login'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['login']] = $item['login'];
			}
		}

		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectProcess($params)
	{
		$form = $this->form;

		$factory = $this->factory;
		$list = $factory->getList(Wf\Process::$classId);
		$processList = $list->load("1=1 LIMIT 100");

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;

		if ( !$params['returnName'] ) {
			foreach( $processList as $process ) {
				$name = $process['name'] . '_' . $process['version'];
				$processSelectSet[$process['pId']] = $name;
			}
		}
		else {
			foreach( $processList as $process ) {
				$name = $process['name'] . '_' . $process['version'];
				$processSelectSet[$name] = $name;
			}
		}

		$this->select($processSelectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectProject($params)
	{
		$form = $this->form;

		// Construct array for set select project options
		$factory = $this->factory;
		$list = $factory->getList(Project::$classId);
		$list->load("1=1 LIMIT 100");

		$selectSet = array();
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;

		if ( $returnName ) {
			foreach( $list as $item ) {
				$selectSet[$item['project_number']] = $item['project_number'];
			}
		}
		else {
			foreach( $list as $item ) {
				$selectSet[$item['project_id']] = $item['project_number'];
			}
		}
		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param container $container
	 * @param string $validation
	 */
	function selectCategory($params)
	{
		$form = $this->form;
		$factory = $this->factory;
		$dao = $factory->getDao(\Rbplm\Ged\Category::$classId);

		(isset($params['containerId'])) ? $containerId = $params['containerId'] : $containerId = null;
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;

		if ( !$containerId ) {
			$select = array(
				$dao->toSys('id').' AS id',
				$dao->toSys('name').' AS name'
			);
			$list = $factory->getList(Category::$classId);
			$list->select($select);
			$list->load("1=1 LIMIT 100");
		}
		else {
			$select = array(
				'child.'.$dao->toSys('id').' AS id',
				'child.'.$dao->toSys('name').' AS name'
			);
			$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Category::$classId);
			$list = $lnkDao->getChildren($containerId, $select)->fetchAll();
		}

		$selectSet = array();
		if ( $returnName ) {
			foreach( $list as $item ) {
				$selectSet[$item['name']] = $item['name'];
			}
		}
		else {
			foreach( $list as $item ) {
				$selectSet[$item['id']] = $item['name'];
			}
		}
		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param unknown_type $spaceName
	 * @param string $validation
	 */
	function selectContainer($params)
	{
		$form = $this->form;

		// Construct array for set select project options
		$factory = $this->factory;
		$spaceName = strtolower($factory->getName());
		$list = $factory->getList(Workitem::$classId);
		$list->select(array(
			$spaceName . '_number AS uid',
			$spaceName . '_id AS id'
		));
		$list->load("1=1 LIMIT 500");

		$selectSet = array();
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;
		if ( $returnName ) {
			foreach( $list as $item ) {
				$selectSet[$item['uid']] = $item['uid'];
			}
		}
		else {
			foreach( $list as $item ) {
				$selectSet[$item['id']] = $item['uid'];
			}
		}

		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param metadata $property
	 * @param string $validation
	 */
	function selectProperty($params, $property)
	{
		$form = $this->form;

		// $list array
		// $params array
		// $manager is the object from to get the method GetIndices
		/*
		 * $params = array(
		 * 'where',
		 * 'name',
		 * 'label'
		 * 'value',
		 * 'multiple',
		 * 'returnName',
		 * 'required',
		 * 'size',
		 * 'sortBy' => '',
		 * 'sortOrder' => '',
		 */

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;
		// Get list of categories
		$p = array(
			'select' => array(
				'property_id',
				'label'
			)
		);
		$list = $property->GetMetadata($p);

		if ( !$params['returnName'] ) {
			foreach( $list as $value ) { // Rewrite result array for quickform convenance
				$select[$value['property_id']] = $value['label'];
			}
		}
		else {
			foreach( $list as $value ) { // Rewrite result array for quickform convenance
				$select[$value['label']] = $value['label'];
			}
		}

		$this->select($select, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param metadata $property
	 * @param string $validation
	 */
	function selectFromDb($params)
	{
		(isset($params['dbFieldForName'])) ? $dbFieldForName = $params['dbFieldForName'] : $dbFieldForName = null;
		(isset($params['dbFieldForValue'])) ? $dbFieldForValue = $params['dbFieldForValue'] : $dbFieldForValue = null;
		(isset($params['dbtable'])) ? $dbtable = $params['dbtable'] : $dbtable = null;
		(isset($params['dbfilter'])) ? $dbfilter = $params['dbfilter'] : $dbfilter = null;
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName = false;

		$sortBy = $dbFieldForName;
		$select = implode(', ', array(
			$dbFieldForValue .' AS value',
			$dbFieldForName .' AS name'
		));
		$connexion = \Rbplm\Dao\Connexion::get();
		$stmt = $connexion->query("SELECT $select FROM $dbtable ORDER BY $sortBy ASC LIMIT 1000");
		$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		// Construct array for set select options
		if ( !$returnName ) {
			foreach( $list as $key => $values ) {
				$selectSet[$values['value']] = $values['name'];
			}
		}
		else {
			foreach( $list as $key => $values ) {
				$selectSet[$values['name']] = $values['name'];
			}
		}
		return $this->select($selectSet, $params);
	}



	/**
	 * $field = array(
	 * ["name"]=>string
	 * ["appname"]=>string
	 * ["label"]=>string
	 * ["type"]=>string
	 * ["regex"]=>string
	 * ["required"]=>string
	 * ["multiple"]=>string
	 * ["size"]=>string
	 * ["returnName"]=>string
	 * ["list"]=>string
	 * ["where"]=>string
	 * ["regexMessage"]=>string
	 * ["value"]=>string
	 * ["sortOrder"]=>string
	 * ["sortBy"]=>string
	 * ["tableName"]=>string
	 * ["fieldforValue"]=>string
	 * ["fieldforDisplay"]=>string
	 * ["dateFormat"]=>string
	 * ["dateLanguage"]=>string / default 'fr'
	 * ["displayBoth"]=>bool / default false
	 * ["disabled"]=>string
	 * ["position"]=>int
	 *
	 * @param array $field
	 * @param Form $form
	 * @param string $validation
	 */
	function element($property)
	{
		$form = $this->form;
		extract($property);

		(isset($name)) ? $sysName = $name : null;
		(isset($appName)) ? $name = $appName : null;

		switch ($type) {
			case 'text':
				if ( !isset($size) || is_null($size) ) {
					$size = 20;
				}

				$form->addElement('text', $name, $label, array(
					'value' => $value,
					'size' => $size,
					'maxlength' => $size,
					$disabled,
					'id' => $id,
					'class' => $class
				));
				$form->addRule($name, tra('should be less than or equal to') . ' ' . $size . ' characters', 'maxlength', $size, $validation);

				if ( !empty($regex) ) { // Add rule with regex
					if ( empty($regexMessage) ) {
						$regexMessage = tra('invalid format');
					}
					$form->addRule($name, $regexMessage, 'regex', '/' . $regex . '/', $validation);
				}

				if ( $required ) { // Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				break;

			case 'long_text':
			case 'html_area':
				if ( !isset($size) ) {
					$size = 20;
				}

				$form->addElement('textarea', $name, $label, array(
					'value' => $value,
					'cols' => $size,
					$disabled,
					'id' => $id,
					'class' => $class
				));
				if ( !empty($regex) ) {
					if ( empty($regexMessage) ) $regexMessage = tra('invalid format');
				}

				$form->addRule($name, $regexMessage, 'regex', '/' . $regex . '/m', $validation);
				if ( $required ) { // Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				break;

			case 'partner':
				$this->selectPartner($property);
				break;

			case 'doctype':
				$this->selectDoctype($property);
				break;

			case 'document_indice':
				$this->selectDocumentVersion($property);
				break;

			case 'container_indice':
				break;

			case 'user':
				$this->selectUser($property);
				break;

			case 'process':
				$this->selectProcess($property);
				break;

			case 'category':
				$this->selectCategory($property);
				break;

			case 'date':
				$this->selectDate($property);
				break;

			case 'integer':
			case 'decimal':
				if ( !isset($size) ) {
					$size = 20;
				}
				$form->addElement('text', $name, $label, array(
					'value' => $value,
					'size' => $size,
					$disabled,
					'id' => $id,
					'class' => $class
				));
				if ( $required ) { // Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				$form->addRule($name, tra('should be numeric'), 'numeric', null, $validation);
				break;

			case 'select':
				$list = explode('#', $list);
				$list = array_combine($list, $list);
				$this->select($list, $property);
				break;

			case 'selectFromDB':
				$this->selectFromDb($list, $property);
				break;
			default:
		} // End of switch

		// Set the default value of the field from field['value
		  // -- if its a multiple field, explode the string value into array
		if ( $multiple && isset($value) ) {
			$value = explode('#', $value);
		}
	} // End of method
}

