<?php
namespace Application\Form;

use Zend\Form\Form;
use Rbs\Dao\Sier\Filter as DaoFilter;

class PaginatorForm extends Form //implements InputFilterAwareInterface
{
	public $maxLimit = 100;
	public $limit = 50;
	public $page = 1;
	public $maxPage = 2;
	public $offset = 0;
	public $order = null;
	public $orderby = 'asc';
	public $nameSpace;

	/**
	 * @param string $name
	 */
	public function __construct($name = null, $formId='paginator')
	{
		// we want to ignore the name passed
		parent::__construct('paginator');
		$this->setAttribute('id', $formId);
		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'form-inline');
		$this->nameSpace = $name;

		$this->add(array(
			'name' => 'orderby',
			'type'  => 'Zend\Form\Element\Hidden',
		));

		$this->add(array(
			'name' => 'order',
			'type'  => 'Zend\Form\Element\Hidden',
		));

		$this->add(array(
			'name' => 'paginator-limit',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Result per page',
			),
			'attributes' => array(
				'onChange'=>"$('#$formId').submit();",
				'options'=>array(
					5=>5,
					20=>20,
					50=>50,
					100=>100,
					1000=>1000
				),
				'value' => 50,
				'class'=>'form-control form-control-paginator',
			)
		));

		$this->add(array(
			'name' => 'paginator-page',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Page',
			),
			'attributes' => array(
				'id'=>'paginator-page',
				'onChange'=>"$('#$formId').submit();",
				'options'=>array(
					1=>1,
				),
				'value' => 1,
				'class'=>'form-control form-control-paginator',
			)
		));

		$this->add(array(
			'name' => 'paginator-next',
			'type'  => 'Zend\Form\Element\Button',
			'options' => array(
				'label' => '>',
				'value' => 'next',
			),
			'attributes' => array(
				'onClick'=>"var page=$('#$formId-page').val();page++;$('#$formId-page').val(page);$('#$formId').submit();",
				'class'=>'btn btn-default btn-paginator-next',
			)
		));

		$this->add(array(
			'name' => 'paginator-prev',
			'type'  => 'Zend\Form\Element\Button',
			'options' => array(
				'label' => '<',
				'value' => 'prev',
			),
			'attributes' => array(
				'onClick'=>"var page=$('#$formId-page').val();page--;$('#$formId-page').val(page);$('#$formId').submit();",
				'class'=>'btn btn-default btn-paginator-prev',
			)
		));
	}

	/**
	 * @param integer $maxLimit
	 */
	public function setMaxLimit($maxLimit)
	{
		$this->maxLimit = $maxLimit;
	}

	/**
	 * @see Zend\Form.Form::prepare()
	 */
	public function prepare()
	{
		parent::prepare();

		$pPageElmt = $this->get('paginator-page');

		$this->page = $pPageElmt->getValue();
		$this->limit = $this->get('paginator-limit')->getValue();
		if(!$this->limit){
			$this->limit=50;
		}
		$this->maxPage = ceil($this->maxLimit / $this->limit);

		$options = array(1=>1);
		for($i=2;$i<=$this->maxPage;$i++){
			$options[$i]=$i;
		}
		$pPageElmt->setValueOptions($options);
		$this->offset = ($this->page-1) * $this->limit;
		$this->order = $this->get('order')->getValue();
		$this->orderby = $this->get('orderby')->getValue();

		return $this;
	}

	/**
	 * @param $view
	 */
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= $view->formRow($this->get('orderby'));
		$html .= $view->formRow($this->get('order'));
		$html .= '<div class="form-group">';
		$html .= $view->formRow($this->get('paginator-limit'));
		$html .= $view->formRow($this->get('paginator-prev')) ;
		$html .= $view->formRow($this->get('paginator-page'));
		$html .= $view->formRow($this->get('paginator-next'));
		$html .= '</div>';
		$html .= $view->form()->closeTag();
		return $html;
	}

	/**
	 * Set view variables from paginator datas
	 *
	 * @param $view
	 */
	public function bindToView($view)
	{
		$view->paginator = $this;
		$view->orderby = $this->orderby;
		$view->order = $this->order;
		return $this;
	}

	/**
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		if(!$this->isPrepared){
			$this->prepare();
		}

		$filter->page($this->page, $this->limit);
		$filter->sort($this->orderby, $this->order);
		$this->filter = $filter;
		return $this;
	}

	/**
	 *
	 */
	public function toSql()
	{
		$sql = "";
		if($this->orderby){
			$sql .= " ORDER BY $this->orderby $this->order";
		}
		$sql .= " LIMIT $this->limit OFFSET $this->offset";
		return $sql;
	}

	/**
	 *
	 */
	public function save()
	{
		$_SESSION['paginator'][$this->nameSpace] = $this->extract();
		return $this;
	}

	/**
	 *
	 */
	public function reset()
	{
		$_SESSION['paginator'][$this->nameSpace] = null;
		return $this;
	}

}
