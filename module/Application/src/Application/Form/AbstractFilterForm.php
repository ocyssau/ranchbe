<?php
namespace Application\Form;

use Zend\Form\Form;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Application\Form\ElementFactory;

abstract class AbstractFilterForm extends Form
{
	/**
	 * BE CAREFULL: Prevent name collision with Zend\Form\Fieldset\$Factory var
	 * @var Rbs\Space\Factory
	 */
	protected $daoFactory;

	/**
	 * Name of session var to store filters
	 * @var string
	 */
	public $nameSpace;

	/**
	 * @var string
	 */
	protected $template;

	/**
	 * @var ViewModel
	 */
	protected $view;

	/**
	 * @var Application\Form\ElementFactory
	 */
	protected $elemtFactory;

	/**
	 * @param Smarty $view
	 * @param \Rbs\Space\Factory $factory
	 * @param string $sessionNamespace Name of the session key where store the filter parameters
	 */
	public function __construct($factory, $sessionNamespace)
	{
		parent::__construct();

		$this
			->setAttribute('id', 'filterForm')
			->setAttribute('method', 'get')
			->setAttribute('class','form-inline');

		$this->daoFactory = $factory;
		$this->nameSpace = $sessionNamespace;
	}

	/**
	 *
	 */
	public function getElementFactory()
	{
		if(!isset($this->elemtFactory)){
			$this->elemtFactory = new ElementFactory($this, $this->daoFactory);
		}
		return $this->elemtFactory;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		foreach($this->getElements() as $element){
			$search = $element->getValue();
			$where = $element->getAttribute('data-where');
			if($where){
				$op = $element->getAttribute('data-op');
				if(!$op){
					$op = Op::CONTAINS;
				}
				$splitted = explode(' ', $search);
				foreach($splitted as $word){
					$paramName = ':'.uniqid();
					$word = str_replace('*','%',$word);
					$filter->orFind($paramName, $this->key, $op);
					$this->bind[$paramName] = '%'.$word.'%';
				}
			}
		}
		return $this;
	}

	/**
	 * @param string $date
	 * @return timeStamp
	 */
	public function dateToSys($date)
	{
		return \DateTime::createFromFormat(SHORT_DATE_FORMAT, $date)->getTimestamp();
	}

	/**
	 *
	 */
	public function load()
	{
		if($_REQUEST['resetf']){
			$this->reset();
			return $this;
		}

		(isset($_SESSION['filter'][$this->nameSpace])) ? $data1 = $_SESSION['filter'][$this->nameSpace] : $data1=array();
		$data2 = $_REQUEST;
		$data = array_merge($data1, $data2);
		$this->setData($data);
		return $this;
	}

	/**
	 *
	 */
	public function save()
	{
		$_SESSION['filter'][$this->nameSpace] = $this->getData();
		return $this;
	}

	/**
	 *
	 */
	public function reset()
	{
		$_SESSION['filter'][$this->nameSpace] = null;
		$this->setData(array());
		return $this;
	}

	/**
	 * @param Renderer $renderer
	 */
	public function render($renderer)
	{
		$view = new \Zend\View\Model\ViewModel();
		$view->setTemplate($this->template);
		$view->setTerminal(true);
		$view->filterForm = $this;
		$html = $renderer->render($view);
		return $html;
	}

	/**
	 * Set the session namespace for save filters
	 */
	public function setNameSpace($string)
	{
		$this->nameSpace = $string;
		return $this;
	}
}
