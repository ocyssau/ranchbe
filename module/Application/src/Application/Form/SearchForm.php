<?php
namespace Application\Form;

use Rbplm\Dao\Filter\Op;
use Rbs\Dao\Sier\Filter as DaoFilter;

/**
 *
 *
 */
class SearchForm extends AbstractFilterForm
{

	/**
	 *
	 * @param unknown $factory
	 */
	public function __construct($factory)
	{
		parent::__construct($factory, 'search');

		$this->template = 'application/search/filterform.phtml';
		$elemtFactory = $this->getElementFactory();
		$inputFilter = $this->getInputFilter();

		/*SPACENAME*/
		$list = array(
			'cadlib'=>tra('cadlib'),
			'bookshop'=>tra('bookshop'),
			'mockup'=>tra('mockup'),
			'workitem'=>tra('workitem')
		);
		$elemtFactory->select($list, array(
			'name'=>'spacename',
			'label'=>tra('In'),
			'multiple'=>false,
			'returnName'=>false,
		));

		/* Designation */
		$this->add(array(
			'name' => 'find_designation',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class'=>'form-control',
				'data-where'=>'description',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Designation',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_designation',
			'required' => false,
		));

		/* Number */
		$this->add(array(
			'name' => 'find_number',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class'=>'form-control',
				'data-where'=>'number',
				'data-op'=>Op::CONTAINS
			),
			'options' => array(
				'label' => 'Number',
			),
		));
		$inputFilter->add(array(
			'name' => 'find_number',
			'required' => false,
		));

		/* Advanced search CheckBox */
		$this->add(array(
			'name' => 'f_adv_search_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Advanced',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_adv_search_cb',
			'required' => false,
		));

		/* DateAndTime CheckBox */
		$this->add(array(
			'name' => 'f_dateAndTime_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Date And Time',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_dateAndTime_cb',
			'required' => false,
		));

		/* CheckOut date */
		$this->add(array(
			'name' => 'f_check_out_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'CheckOut date',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_cb',
			'required' => false,
		));
		/* Superior to Date */
		$this->add(array(
			'name' => 'f_check_out_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_min',
			'required' => false,
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_check_out_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_check_out_date_max',
			'required' => false,
		));

		/* Update date selector */
		$this->add(array(
			'name' => 'f_update_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Update date',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_cb',
			'required' => false,
		));

		/* Superior to date */
		$this->add(array(
			'name' => 'f_update_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_min',
			'required' => false,
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_update_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_update_date_max',
			'required' => false,
		));

		/* Open date */
		$this->add(array(
			'name' => 'f_open_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Open date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_open_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_open_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_open_date_max',
			'required' => false,
		));

		/* Close date */
		$this->add(array(
			'name' => 'f_close_date_cb',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class'=>'form-control optionSelector',
			),
			'options' => array(
				'label' => 'Close date',
			),
		));
		/* Superior to date */
		$this->add(array(
			'name' => 'f_close_date_min',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Superior to',
			),
		));
		/* Inferior to date */
		$this->add(array(
			'name' => 'f_close_date_max',
			'type'  => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Click to select date',
				'class'=>'form-control datepicker',
				'data-where'=>'',
				'data-op'=>''
			),
			'options' => array(
				'label' => 'Inferior to',
			),
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_cb',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_min',
			'required' => false,
		));
		$inputFilter->add(array(
			'name' => 'f_close_date_max',
			'required' => false,
		));

		/*Create user*/
		$elemtFactory->selectUser(array(
			'name'=>'f_createby',
			'label'=>tra('Create By'),
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		/*Update user*/
		$elemtFactory->selectUser(array(
			'name'=>'f_updateby',
			'label'=>tra('Update By'),
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		/*Checkout user*/
		$elemtFactory->selectUser(array(
			'name'=>'f_checkoutby',
			'label'=>tra('Checkout By'),
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));
	}

	/**
	 * {@inheritDoc}
	 * @see \Application\Form\AbstractFilterForm::bindToFilter()
	 */
	public function bindToFilter(DaoFilter $filter)
	{
		return $this;
		$dao = $this->daoFactory->getDao(Document\Version::$classId);
		$me = People\CurrentUser::get()->getid();
		$this->prepare()->isValid();
		$datas = $this->getData();

		/* NORMALS OPTIONS */
		//NUMBER
		if($datas['find_number']){
			$filter->andFind($datas['find_number'], $dao->toSys('number'), Op::OP_CONTAINS);
		}

		//DESIGNATION
		if($datas['find_designation']){
			$filter->andFind($datas['find_designation'], $dao->toSys('description'), Op::OP_CONTAINS);
		}

		/* ADVANCED SEARCH */
		if($datas['f_adv_search_cb']){
			//FIND IN
			if($datas['find'] && $datas['find_field']){
				$filter->with(array(
					'table'=>$factory->getName().'_categories',
					'on'=>'category_id',
					'alias'=>'category',
					'select'=>array('category_number'),
					'direction'=>'outer'
				));
				$filter->andFind($datas['find'], $datas['find_field'], Op::CONTAINS);
			}

			//ACTION USER
			if($datas['f_createby']){
				$filter->andFind($datas['f_createby'], 'createById', Op::EQUAL);
			}
			if($datas['f_updateby']){
				$filter->andFind($datas['f_updateby'], 'updateById', Op::EQUAL);
			}
			if($datas['f_checkoutby']){
				$filter->andFind($datas['f_checkoutby'], 'checkoutById', Op::EQUAL);
			}

			//DATE AND TIME
			if($datas['f_dateAndTime_cb']){
				//CHECKOUT
				if($datas['f_check_out_date_cb']){
					if($datas['f_check_out_date_min']){
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::SUP);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
					if($datas['f_check_out_date_max']){
						$filter->andFind($this->dateToSys($datas['f_check_out_date_min']), $dao->toSys('locked'), Op::INF);
						$filter->andFind(AccessCode::CHECKOUT, $dao->toSys('accessCode'), Op::EQUAL);
					}
				}
				//UPDATE
				if($datas['f_update_date_cb']){
					if($datas['f_update_date_min']){
						$filter->andFind($this->dateToSys($datas['f_update_date_min']), $dao->toSys('updated'), Op::SUP);
					}
					if($datas['f_update_date_max']){
						$filter->andFind($this->dateToSys($datas['f_update_date_max']), $dao->toSys('updated'), Op::INF);
					}
				}
				//OPEN
				if($datas['f_open_date_cb']){
					if($datas['f_open_date_min']){
						$filter->andFind($this->dateToSys($datas['f_open_date_min']), $dao->toSys('created'), Op::SUP);
					}
					if($datas['f_open_date_max']){
						$filter->andFind($this->dateToSys($datas['f_open_date_max']), $dao->toSys('created'), Op::INF);
					}
				}
				//CLOSE
				if($datas['f_close_date_cb']){
					if($datas['f_close_date_min']){
						$dateAsSys = \DateTime::createFromFormat(SHORT_DATE_FORMAT, $datas['f_close_date_min'])->getTimestamp();
						$filter->andFind($this->dateToSys($datas['f_close_date_min']), $dao->toSys('closed'), Op::SUP);
					}
					if($datas['f_close_date_max']){
						$filter->andFind($this->dateToSys($datas['f_close_date_max']), $dao->toSys('closed'), Op::INF);
					}
				}
				//FORSEEN CLOSE
				if($datas['f_fsclose_date_cb']){
					if($datas['f_fsclose_date_min']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_min']), 'forseen_close_date', Op::SUP);
					}
					if($datas['f_fsclose_date_max']){
						$filter->andFind($this->dateToSys($datas['f_fsclose_date_max']), 'forseen_close_date', Op::INF);
					}
				}
			}
		}

		return $this;
	}

}
