<?php
namespace Application\Form\Extended;

use Zend\InputFilter\InputFilter;

class TextForm extends PropertyForm
{
	/**
	 *
	 * @param unknown_type $tdim
	 */
	public function __construct($name='propertyFieldset')
	{
		parent::__construct($name);
		//$this->template = 'application/extendedproperties/textform.phtml';

		$this->additionalsElements = array('size', 'regex');

		$this->add(array(
			'name' => 'size',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '1',
				'min'=> '5',
				'placeholder' => 'Size',
			),
			'options' => array(
				'label' => 'Size',
			),
		));

		$this->add(array(
			'name' => 'regex',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Regex',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Regex',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		$parentFilter = parent::getInputFilterSpecification();
		$filter = array(
			'regex' => array(
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
				),
			),
		);

		return array_merge($parentFilter,$filter);
	}

}
