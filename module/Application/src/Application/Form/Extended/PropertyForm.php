<?php 
namespace Application\Form\Extended;

use Application\Dao;
use Application\Model;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class PropertyForm extends Form implements InputFilterProviderInterface
{
	/**
	 *
	 * @param unknown_type $tdim
	 */
	public function __construct($name='Text')
	{
		parent::__construct($name);
		$this->template = 'application/extendedproperties/propertyform.phtml';

		$this
		->setAttribute('method', 'post')
		->setHydrator(new Hydrator(false))
		->setInputFilter(new InputFilter());

		$this->add(array(
			'name' => 'id',
			'type'  => 'Hidden',
		));

		$this->add(array(
			'name' => 'type',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
				'placeholder' => 'Type',
				'class'=>'form-control',
				'id'=>'propertyType'
			),
			'options' => array(
				'label' => 'Type',
				'value_options'=> $this->_getTypeOptions(),
				'empty_option'=>'Please, select a property type'
			),
		));

		$this->add(array(
			'name' => 'extendedCid',
			'type'  => 'Zend\Form\Element\Select',
			'attributes' => array(
				'type'  => 'select',
				'placeholder' => 'Object',
				'class'=>'form-control',
				'id'=>'extendedCid'
			),
			'options' => array(
				'label' => 'Object Class',
				'value_options'=> $this->_getExtendedCidOptions(),
				'empty_option'=>'Please, select a object class'
			),
		));

		$this->add(array(
			'name' => 'name',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Name',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Name',
			),
		));

		$this->add(array(
			'name' => 'getter',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Getter',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Getter',
			),
		));

		$this->add(array(
			'name' => 'setter',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Setter',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Setter',
			),
		));

		$this->add(array(
			'name' => 'label',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
				'placeholder' => 'Label',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Label',
			),
		));

		$this->add(array(
			'name' => 'required',
			'type'  => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'type'  => 'checkbox',
				'placeholder' => 'Require',
				'class'=>'form-control'
			),
			'options' => array(
				'label' => 'Require',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton',
			),
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			),
			'type' => array(
				'required' => true,
			),
			'name' => array(
				'required' => true,
				'filters'  => array(
					array('name' => 'Zend\Filter\StringTrim'),
					array('name' => 'Application\Model\Sys\String'),
				),
			),
		);
	}

	/**
	 *
	 */
	protected function _getTypeOptions()
	{
		return array('text'=>'Text',
			'number'=>'Number',
			'range'=>'Range',
			'select'=>'Select',
			'selectfromdb'=>'Select From a Database',
			'date'=>'Date');
	}

	/**
	 *
	 */
	protected function _getExtendedCidOptions()
	{
		$map = \Application\Model\Factory::getMap();
		ksort($map);
		$return = array();

		foreach($map as $cid=>$def){
			if($cid>299 && $cid <399){
				$return[$cid]=$cid.'-'.$def[0];
			}
		}

		return $return;
	}

}
