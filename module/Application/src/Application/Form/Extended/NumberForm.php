<?php 
namespace Application\Form\Extended;

use Application\Dao;
use Application\Model;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
//use Zend\Stdlib\Hydrator\ClassMethods as Hydrator;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class NumberForm extends PropertyForm
{
	/**
	 *
	 * @param unknown_type $tdim
	 */
	public function __construct($name='NumberProperty')
	{
		parent::__construct($name);
		//$this->template = 'application/extendedproperties/numberform.phtml';

		$this->additionalsElements = array('min', 'max', 'step');

		$this->add(array(
			'name' => 'min',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '1',
				'placeholder' => 'Min Value',
			),
			'options' => array(
				'label' => 'Min Value',
			),
		));

		$this->add(array(
			'name' => 'max',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '1',
				'placeholder' => 'Max Value',
			),
			'options' => array(
				'label' => 'Max Value',
			),
		));

		$this->add(array(
			'name' => 'step',
			'type'  => 'Zend\Form\Element\Number',
			'attributes' => array(
				'type'  => 'number',
				'step'=> '1',
				'placeholder' => 'Step Between Value',
			),
			'options' => array(
				'label' => 'Step Between Value',
			),
		));

	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			),
			'type' => array(
				'required' => true,
			),
			'name' => array(
				'required' => true,
			),
			'min' => array(
				'required' => false,
			),
			'max' => array(
				'required' => false,
			),
			'step' => array(
				'required' => false,
			),
		);
	}

}
