<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class ProcessIdToName extends AbstractHelper
{

	public function __invoke($id)
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['process'][$id]) ) {
			return $cacheUsualName['process'][$id];
		}

		$table = 'galaxia_processes';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT normalized_name FROM $table WHERE pId = '$id'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['process'][$id] = $ret;
		return $ret;
	}
}
