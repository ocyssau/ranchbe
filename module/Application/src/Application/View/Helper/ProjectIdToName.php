<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class ProjectIdToName extends AbstractHelper
{

	public function __invoke($id)
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['project'][$id]) ) {
			return $cacheUsualName['project'][$id];
		}

		$table = 'projects';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT project_number FROM $table WHERE project_id = '$id'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['project'][$id] = $ret;
		return $ret;
	}
}
