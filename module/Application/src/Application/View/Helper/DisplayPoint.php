<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DisplayPoint extends AbstractHelper
{
	public function __invoke($point)
	{
		$html = '<ul class="list-group">
				<li class="list-group-item">'.round($point[0], 4).' mm</li>
				<li class="list-group-item">'.round($point[1], 4).' mm</li>
				<li class="list-group-item">'.round($point[2], 4).' mm</li>
		</ul>';
		return $html;
	}
}
