<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FilesizeFormater extends AbstractHelper
{
	public function __invoke($int)
	{
		if($int == 0) return 0;
		else if($int <= 1024)				$format = $int."oct";
		else if($int <= (10*1024))			$format = sprintf ("%.2f k%s",($int/1024),"oct");
		else if($int <= (100*1024))			$format = sprintf ("%.1f k%s",($int/1024),"oct");
		else if($int <= (1024*1024))		$format = sprintf ("%d k%s",($int/1024),"oct");
		else if($int <= (10*1024*1024))		$format = sprintf ("%.2f M%s",($int/(1024*1024)),"oct");
		else if($int <= (100*1024*1024))	$format = sprintf ("%.1f M%s",($int/(1024*1024)),"oct");
		else $format = sprintf ("%d M%s",($int/(1024*1024)),"oct");
		return $format;
	}
}
