<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DoctypeFormater extends AbstractHelper
{
	public function __invoke($typeId)
	{
		$cacheUsualName =& \Ranchbe::$registry;
		if( isset($cacheUsualName['type'][$typeId]) ) {
			return $cacheUsualName['type'][$typeId];
		}

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$typeId'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if(!$ret){
			$ret = tra('undefined');
		}
		else{
			$ranchbe = \Ranchbe::get();
			$doctypeIconBaseUrl = $ranchbe->getConfig('icons.doctype.compiled.url');
			$doctypeIconBasePath = $ranchbe->getConfig('icons.doctype.compiled.path');
			$doctypeIconType = $ranchbe->getConfig('icons.doctype.type');

			$iconFile = $doctypeIconBasePath.'/'.$typeId.'.'.$doctypeIconType;
			$iconUrl = BASEURL.'/'.$doctypeIconBaseUrl.'/'.$typeId.'.'.$doctypeIconType;

			if(is_file($iconFile)){
				$html = "<img src=\"$iconUrl\" />";
				$html .= '<div class="doctype-label">'.$ret.'</div>';
				$ret = $html;
			}
		}

		$cacheUsualName['type'][$typeId] = $ret;
		return $ret;
	}
}
