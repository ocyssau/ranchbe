<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class DateFormater extends AbstractHelper
{

	/**
	 * @param string $timestamp
	 * @param string $format
	 */
	public function __invoke($timestamp, $format=SHORT_DATE_FORMAT)
	{
		if( $timestamp != null ){
			$dateTime = new \DateTime();
			$dateTime->setTimeStamp($timestamp);
			return $dateTime->format($format);
		}
		else{
			return;
		}
	}
}
