<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FileIcon extends AbstractHelper
{
	public function __invoke($type, $iconType='.gif')
	{
		$type = trim($type , ".");
		$ranchbe = \Ranchbe::get();

		$imgBasePath = $ranchbe->getConfig('icons.file.path');
		$imgBaseUrl = $ranchbe->getConfig('icons.file.url');
		$iconfile = $imgBasePath .'/'. $type . $imgExtension;

		if ( !is_file($iconfile) ){
			$iconUrl = $imgBaseUrl .'/_default.gif';
		}
		else{
			$iconUrl = $imgBaseUrl .'/'. $type . $imgExtension;
		}
		return '<img alt="no icon" src="' . $iconUrl . '" />';
	}
}
