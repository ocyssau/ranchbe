<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class VersionFormater extends AbstractHelper
{

	public function __invoke($versionId)
	{
		$cacheUsualName =& Ranchbe::$registry;

		if( isset($cacheUsualName['indice'][$versionId]) ){
			return $cacheUsualName['indice'][$versionId];
		}

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT `indice_value` FROM `document_indice` WHERE `document_indice_id` = '$versionId'";
		$stmt = $conn->query($sql);
		$ret  = $stmt->fetchColumn(0);

		if(!$ret){
			$ret = tra('undefined');
		}

		$cacheUsualName['indice'][$versionId] = $ret;
		return $ret;
	}
}
