<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetPostit extends AbstractHelper
{
	public $postitDao;

	public function __invoke($documentId, $spaceName='workitem')
	{
		if(!isset($this->postitDao)){
			$this->postitDao = \Rbs\Space\Factory::get($spaceName)->getDao(\Rbs\Postit\Postit::$classId);
		}
		$dao = $this->postitDao;

		$postits = $dao->getFromParent($documentId)->fetchAll();
		if(!$postits){
			return;
		}

		$html .= '<div class="rb-postit">';
		$html .= '<div class="rb-postit-header"><img src="'.$baseUrl.'/img/icons/comment/comment.png" title="postit"  alt="postit" /></div>';

		foreach($postits as $postit ){
			$id = $postit['id'];
			$spaceName = $postit['spaceName'];

			$html .= "<div class=\"rb-postit-item ui-corner-all\" data-id=\"$id\" data-spacename=\"$spaceName\" >";
			$html .= '<i>By '.$this->getView()->UserIdToName($postit['ownerId']).'</i><br />';
			$text = iconv('ISO-8859-2', "ISO-8859-1//TRANSLIT", $postit['body'] ); //convert $rawdata in ISO-8859-1
			$text = str_replace("\r\n", '<br />',$text);
			$text = str_replace("\n", '<br />',$text);
			$text = str_replace("\r", '<br />',$text);
			$text = str_replace('\'', '\\\'',$text);
			$html .= $text . '<br />';
			$html .= '<a href="#" class="deletepostit-btn">Delete</a>';
			$html .= '</div>';
		}

		$html .= '</div>';

		return $html;
	}
}
