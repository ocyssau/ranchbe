<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class CategoryIdToName extends AbstractHelper
{

	public function __invoke($categoryId, $spaceName='workitem')
	{
		$cacheUsualName = & \Ranchbe::$registry;
		if ( isset($cacheUsualName['category'][$categoryId]) ) {
			return $cacheUsualName['category'][$categoryId];
		}

		$table = $spaceName . '_categories';

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT category_number FROM $table WHERE category_id = '$categoryId'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['category'][$categoryId] = $ret;
		return $ret;
	}
}
