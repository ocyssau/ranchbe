<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Rbs\Space\Factory as DaoFactory;

class CidToClass extends AbstractHelper
{
	public function __invoke($classId, $spaceName)
	{
		(!$spaceName) ? $spaceName='default' : null;
		$class = DaoFactory::get($spaceName)->getModelClass($classId);
		return $class;
	}
}
