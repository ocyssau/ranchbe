<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class AccessCodeFormater extends AbstractHelper
{
	public function __invoke($acode)
	{
		switch($acode){
			case 0:
				return $prefix . '<font color="green">'.tra('<b>F</b>ree').'</font>';
				break;
			case 1:
				return $prefix . '<font color="OrangeRed">'.tra('<b>C</b>heckout').'</font>';
				break;
			case 5:
				return $prefix . '<font color="Blue">'.tra('In<b>W</b>orkflow').'</font>';
				break;
			case 10:
				return $prefix . '<font color="Orange">'.tra('<b>V</b>alidate').'</font>';
				break;
			case 11:
				return $prefix . '<font color="red">'.tra('<b>L</b>ocked').'</font>';
				break;
			case 12:
				return $prefix . '<font color="red">'.tra('<b>M</b>arked to suppress').'</font>';
				break;
			case 13:
				return $prefix . '<font color="red">'.tra('<b>I</b>teration').'</font>';
				break;
			case 15:
				return $prefix . '<font color="red">'.tra('<b>H</b>istoric').'</font>';
				break;
			default:
				if( $acode > 99 ){
					return '<font color="red">'.tra('<b>A</b>rchived') . '(Code='.$acode.')' .'</font>';
				}
				return $prefix . '(Code='.$acode.')';
				break;
		}
	}
}
