<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetThumbnail extends AbstractHelper
{

	public function __invoke($documentId, $spaceName='workitem')
	{
		//$imgBaseUrl = $smarty->get_template_vars('baseCustomImgUrl').'/thumbnails';
		$imgBasePath = 'data/img/thumbnails';
		$imgExtension = '.gif';
		$thumbfile = $imgBasePath .'/'. $documentId . $imgExtension;
		if ( is_file($thumbfile) ){
			$thumbUrl = $imgBaseUrl .'/'. $documentId . $imgExtension;
			return '<img border="0" alt="no thumbs" src="' . $thumbUrl . '" />';
		}
		else{
			return '<!--no thumbs-->';
		}
	}
}
