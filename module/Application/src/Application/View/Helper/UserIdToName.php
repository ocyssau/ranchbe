<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Ranchbe;

class UserIdToName extends AbstractHelper
{

	public function __invoke($userId)
	{
		$cacheUsualName =& Ranchbe::$registry;
		if ( isset($cacheUsualName['username'][$userId]) ) {
			return $cacheUsualName['username'][$userId];
		}

		$conn = \Rbplm\Dao\Connexion::get();
		$sql = "SELECT handle FROM liveuser_users WHERE auth_user_id = '$userId'";
		$stmt = $conn->query($sql);
		$ret = $stmt->fetchColumn(0);

		if ( !isset($cacheUsualName['usernameStmt']) ) {
			$sql = "SELECT handle FROM liveuser_users WHERE auth_user_id = :userid";
			$stmt = $conn->prepare($sql);
			$cacheUsualName['usernameStmt'] = $stmt;
		}
		else {
			$stmt = $cacheUsualName['usernameStmt'];
		}

		$stmt->execute(array(
			'userid' => $userId
		));
		$ret = $stmt->fetchColumn(0);

		if ( !$ret ) {
			$ret = tra('undefined');
		}

		$cacheUsualName['username'][$userId] = $ret;
		return $ret;
	}
}
