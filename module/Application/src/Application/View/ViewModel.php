<?php
namespace Application\View;

/**
 * Adapter for smarty view
 *
 * @category Ranchbe
 * @package View
 * @copyright
 *
 * @license
 *
 */
class ViewModel extends \Zend\View\Model\ViewModel
{

	/**
	 * Constructor.
	 *
	 * @param array $config
	 *        	Configuration key-value pairs.
	 */
	public function __construct($ranchbe)
	{
		// Assign value of the version to smarty
		$this->ranchbe = $ranchbe;
		$config = $ranchbe->getConfig();

		$this->charset = $config['view.charset'];
		$this->shortcutIcon = $config['view.url.shortcuticon'];
		$this->logo = $config['view.url.logo'];
		$this->allowUserPrefs = $config['user.allowprefs'];
		$this->lang = $config['view.lang'];

		$this->baseurl = trim(BASEURL, '/');
		$this->baseJsUrl = $this->baseurl . '/public/js';
		$this->baseCssUrl = $this->baseurl . '/public/css';
		$this->baseImgUrl = $this->baseurl . '/img';
		$this->baseCustomImgUrl = $this->baseurl . '/data/img';
		$this->doctypeIconBaseUrl = $this->baseurl .'/'. $config['icons.doctype.compiled.url'];
		$this->doctypeSrcIconBaseUrl = $this->baseurl .'/'. $config['icons.doctype.source.url'];
		$this->thumbnailBaseurl = $config['thumbnails.url'];
		$this->iconsFileBaseurl = $config['icons.file.url'];
		$this->rbgateServerUrl = $config['rbgate.server.url'];

		$dateformat = str_replace('-Y', '-yy', $config['view.shortdate.format']);
		$dateformat = str_replace('d-', 'dd-', $dateformat);
		$dateformat = str_replace('-m', '-mm', $dateformat);
		$this->shortDateformat = str_replace('%', '', $dateformat);

		$dateformat = str_replace('-Y', '-yy', $config['view.longdate.format']);
		$dateformat = str_replace('d-', 'dd-', $dateformat);
		$dateformat = str_replace('-m', '-mm', $dateformat);
		$this->longDateformat = str_replace('%', '', $dateformat);

		// compilation parameters
		if ( array_key_exists('compile', $config) ) {}

		// template parameters
		if ( array_key_exists('template', $config) ) {}

		// cache parameters
		if ( array_key_exists('cache', $config) ) {}

		// config parameters
		if ( array_key_exists('config', $config) ) {}

		// debug parameters
		if ( array_key_exists('debug', $config) ) {}

		// plugins parameters
		if ( array_key_exists('plugin', $config) ) {}
	}

	/**
	 *
	 * @param mixed $var
	 * @param mixed $value
	 * @return ViewModel
	 */
	public function assign($var, $value)
	{
		$this->$var = $value;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function baseUrl($path)
	{
		$path = trim($path, '/');
		return $this->baseurl . '/' . $path;
	}
}
