<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\View\ViewModel;

use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

use Rbplm\People;
use Application\Model\Acl;
use Application\Model;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Acl\AccessRestrictionException;
use Rbplm\Dao\NotExistingException;

use Zend\Http\Response as HttpResponse;

abstract class AbstractController extends AbstractActionController
{
	/**
	 *
	 * @var \Application\Model\Auth\Storage
	 */
	protected $storage;

	/**
	 *
	 * @var \Zend\Authentication\Adapter\AdapterInterface
	 */
	protected $authservice;

	/**
	 * @var boolean
	 */
	protected $isService = false;

	/**
	 * @var boolean
	 */
	protected $isAjax = false;

	/**
	 *
	 * @var \Rbs\Sys\ErrorStack
	 */
	protected $errorStack;

	/**
	 * @var ViewModel
	 */
	public $view;

	/**
	 * @var string $pageId
	 */
	public $pageId;

	/**
	 * @var string
	 */
	public $defaultSuccessForward;

	/**
	 * @var string
	 */
	public $defaultFailedForward;

	/**
	 * @var string
	 */
	public $ifSuccessForward;

	/**
	 * @var string
	 */
	public $ifFailedForward;

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authservice = $this->getServiceLocator()->get('AuthService');
		$this->storage = $this->authservice->getStorage();

		\Ranchbe::setAuthservice($this->authservice);

		if (!$this->authservice->hasIdentity()){
			return $this->redirect()->toUrl('/auth/login');
		}

		if ( $request->isXmlHttpRequest() ) {
			$this->initAjax();
			parent::dispatch($request, $respons);
		}
		else{
			$this->init();
			parent::dispatch($request, $respons);

			/* Layout must be set after dispatch */
			$this->getLayoutselector()->setLayout($this);
		}
	}

	/**
	 *
	 */
	public function init($view=null, $errorStack=null)
	{
		($view==null) ? $this->view = new ViewModel(\Ranchbe::get()) : $this->view = $view;
		($errorStack==null) ? $this->errorStack = \Ranchbe::getErrorStack() : $this->errorStack = $errorStack;

		$this->isService = false;
		$this->isAjax = false;
		$this->view->isService = false;
		$this->view->isAjax = false;

		\Ranchbe::setView($this->view);

		isset($_REQUEST['pageid']) ? $this->pageId = $_REQUEST['pageid'] : null;
		$this->view->pageid = $this->pageId;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;
	}

	/**
	 *
	 */
	public function initAjax($view=null, $errorStack=null)
	{
		($view==null) ? $this->view = new ViewModel(\Ranchbe::get()) : $this->view = $view;
		\Ranchbe::setView($this->view);
		$this->view->setTerminal(true);
		$this->isService = true;
		$this->isAjax = true;
		$this->view->isService = true;
		$this->view->isAjax = true;
	}

	/**
	 *
	 * @param array $return
	 */
	protected function serviceReturn($return=array())
	{
		http_response_code(200);
		echo json_encode($return);
		die;
	}

	/**
	 *
	 * @param  \Exception
	 * @return string json
	 */
	public function exceptionToJson($e)
	{
		$return = array(
			'error'=>array(
				'message'=>$e->getMessage(),
				'code'=>$e->getCode(),
				'file'=>$e->getFile(),
				'line'=>$e->getLine(),
			));
		echo json_encode($return);
	}

	/**
	 * @return Acl
	 */
	public function getAcl()
	{
		$user = People\CurrentUser::get();
		return $user->appacl;
	}

	/**
	 * @param string $rightName
	 * @param integer $areaId
	 * @param integer $resourceId
	 * @return boolean
	 * @throws AccessRestrictionException
	 */
	public function checkRight($rightName, $areaId=1, $resourceId=0)
	{
		$dao = DaoFactory::get()->getDao(\Rbs\Acl\Right::$classId);
		$userId = People\CurrentUser::get()->getId();

		if($userId==1){
			return true;
		}

		try{
			try{
				$result = $dao->check($userId, $rightName, $areaId, $resourceId);
			}
			catch(NotExistingException $e){
				throw new AccessRestrictionException('check rights failed for this credentials');
			}

			if($result['rule']<>'allow'){
				throw new AccessRestrictionException('check rights failed for this credentials');
			}
		}
		catch(AccessRestrictionException $e){
			$this->errorStack->warning($e->getMessage());
			$this->redirect()->toUrl('/application/home/home');
		}
	}

	/**
	 * @return HttpResponse
	 */
	protected function notauthorized()
	{
		$response = new HttpResponse();
		$response->setStatusCode(HttpResponse::STATUS_CODE_403);

		$view = new viewModel();
		$view->setTemplate('error/403');
		$view->message = "You are not authorized to execute this request. May be its time to presents a coffee to your administrator.";

		$viewRender = $this->getServiceLocator()->get('ViewRenderer');
		$html = $viewRender->render($view);

		$response->setContent($html);
		return $response;
	}

	/**
	 * Get the authservice. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService()
	{
		return $this->authservice;
	}

	/**
	 *
	 * @return \Application\Controller\LayoutSelector
	 */
	public function getLayoutselector()
	{
		if(!isset($this->layoutSelector)){
			$layoutSelector = new \Application\Controller\LayoutSelector();
			$this->layoutSelector = $layoutSelector;
		}
		return $this->layoutSelector;
	}


	/**
	 * Get the auth storage object. Use factory define in Module::getServiceConfig()
	 *
	 * @return \Application\Model\Auth\Storage
	 */
	public function getSessionStorage()
	{
		return $this->storage;
	}

	/**
	 *
	 * @return \Application\Controller\Basecamp
	 */
	public function basecamp()
	{
		if(!isset($this->basecamp)){
			$this->basecamp = new \Application\Controller\Basecamp();
		}
		return $this->basecamp;
	}

	/**
	 * @return \Rbs\Sys\ErrorStack
	 */
	public function errorStack()
	{
		return $this->errorStack;
	}

	/**
	 *
	 * @param string $fwd
	 * @param array $params
	 */
	public function redirectTo($fwd, $params=null)
	{
		$baseUrl = $this->getRequest()->getBasePath();
		$location = ( strtolower($baseUrl.'/'.trim($fwd,'/')));
		if(is_array($params)){
			$assigns = array();
			foreach($params as $name=>$value){
				$assign[] = $name.'='.$value;
			}
			$t = strpos($fwd, '?');
			if($t===false){
				$location = $location.'?'.implode('&', $assign);
			}
			else{
				$location = $location.'&'.implode('&', $assign);
			}
		}
		header("Location: $location");
		die;
	}

	/**
	 * @param array
	 */
	public function errorForward($params=null)
	{
		$this->redirectTo($this->ifFailedForward, $params);
	}

	/**
	 * @param array
	 */
	public function successForward($params=null)
	{
		$this->redirectTo($this->ifSuccessForward, $params);
	}
}
