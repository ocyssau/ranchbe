<?php
namespace Application\Controller;

use Rbs\Space\Factory as DaoFactory;
use Rbs\Search\Search as SearchEntity;

/**
 *
 *
 */
class SearchController extends AbstractController
{
	public $pageId = 'application_search';
	public $defaultSuccessForward = 'search/index';
	public $defaultFailedForward = 'search/index';

	/**
	 *
	 */
	public function init()
	{
		parent::init();

		// Record url for page and Active the tab
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;

		$this->ifSuccessForward = $this->defaultSuccessForward;
		$this->ifFailedForward = $this->defaultFailedForward;

		$this->basecamp()->setForward($this);
	}

	/**
	 *
	 */
	public function indexAction()
	{
		$factory = DaoFactory::get();
		$request = $this->getRequest();
		$view = $this->view;

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filterForm = new \Application\Form\SearchForm($factory, $this->pageId);
		$filterForm->setAttribute('id', 'searchfilterform');
		$filterForm->load();
		$filterForm->bindToFilter($filter);

		/* @var \Rbs\Search\SearchDao $dao */
		$dao = $factory->getDao(SearchEntity::$classId);
		$list = $factory->getList(SearchEntity::$classId);

		$select = array();
		foreach($dao->metaModel as $asSys=>$asApp){
			$select[] = $asSys;
			$header[] = array($asSys,$asApp);
		}

		//$filter->select($select);
		$view->headers = $header;
		$view->select = $select;
		$list->load($filter);
		$view->list = $list;
		$view->pageTitle = 'Search';
		$view->filter = $filterForm;
		return $view;
	}
}
