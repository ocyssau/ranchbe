<?php
namespace Application\Controller;

//Zend
use Zend\View\Model\ViewModel;

//models and Dao
use Application\Dao;
use Application\Model\Application;
use Application\Model\Extended\Property;
use Rbplm\People;
use Application\Form\PaginatorForm;
use Application\Form\StdFilterForm;
use Application\Form;

class ExtendedpropertiesController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		//get Acls for application from SESSION, see dispatch method
		$appAcl = People\CurrentUser::get()->appacl;

		if($appAcl->hasRight($appAcl::RIGHT_ADMIN)){
			return $this->manage();
		}
		elseif($appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->manage();
		}
		else{
			return $this->notauthorized();
		}
	}

	/**
	 *
	 */
	public function manage()
	{
		$view = new ViewModel();
		$view->setTemplate('application/extendedproperties/index');
		$request = $this->getRequest();

		//get Acls for application
		$appAcl = People\CurrentUser::get()->appacl;
		$userId = People\CurrentUser::get()->getId();

		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->key = 'CONCAT(obj.name,obj.id)';
		$filter->passThrough=true;
		$filter->prepare();

		$bind = array();
		$cid = Property::$classId;
		$List = Dao\Factory::get()->getList($cid);
		$table = $List->table;

		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($List->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->setData($request->getQuery());
		$paginator->prepare()->bindToView($view);

		$sql  = " SELECT obj.* FROM $table AS obj";

		if($filter->where){
			$sql .= ' WHERE ' . $filter->where;
			$bind = array_merge($bind, $filter->bind);
		}
		$sql .= $paginator->toSql();

		$List->loadFromSql($sql, $bind);
		$view->myList = $List;

		$view->myHeaders = array(
			'#'=>'id',
			'ClassID'=>'extendedCid',
			'Name'=>'name',
			'Label'=>'label',
			'Type'=>'type',
		);

		$view->filter = $filter;
		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		//init view
		$view = new ViewModel();
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$factory = Dao\Factory::get();

		$type = strtolower($this->params()->fromQuery('type'));
		switch($type){
			case 'text':
				$type = 'Text';
				break;
			case 'number':
				$type = 'Number';
				break;
			case 'range':
				$type = 'Range';
				break;
			case 'select':
				$type = 'Select';
				break;
			case 'selectfromdb':
				$type = 'SelectFromDb';
				break;
			case 'date':
				$type = 'Date';
				break;
			default:
				$type = 'Text';
		}

		//init model
		$class = '\Application\Model\Extended\Property\\'.$type;
		$Property = new $class();

		//init form
		$class = '\Application\Form\Extended\\'.$type.'Form';
		$form = new $class($ptype);
		$form->bind($Property);

		//validation
		if ($request->isPost()) {
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				//save
				$factory->getDao($Property)->insert($Property);
				return $this->redirect()->toRoute('extendedproperties');
			}
		}

		$view->title = 'Create New Property For Type';
		$view->setTemplate($form->template);
		$view->model = $Property;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = $this->params()->fromRoute('id');

		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		//init view
		$view = new ViewModel();
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$factory = Dao\Factory::get();

		//get type from id
		$Property = new Property\Text();
		$factory->getDao($Property->cid)->load($Property, 'id='.$id);

		//reload model in right class
		if($Property->type <> 'text'){
			$cid = $Property->cid;
			$map = \Application\Model\Factory::getMap($cid);
			$class = $map[0];
			$Property = new $class();
			$factory->getDao($Property->cid)->load($Property, 'id='.$id);
		}

		//init form
		$type = ucfirst($Property->type);
		$class = '\Application\Form\Extended\\'.$type.'Form';
		$form = new $class($ptype);

		$form->remove('type');
		$form->remove('extendedCid');

		$form->bind($Property);

		if ($request->isPost()) {
			$form->setData( $request->getPost() );
			if ( $form->isValid() ) {
				//save
				$factory->getDao($Property)->update($Property);
				return $this->redirect()->toRoute('extendedproperties');
			}
		}

		$view->title = 'Edit Property ' . $Property->name;
		$view->setTemplate($form->template);
		$view->model = $Property;
		$view->form = $form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$id = $this->params()->fromRoute('id');

		//check authorization
		$appAcl = People\CurrentUser::get()->appacl;
		if(!$appAcl->hasRight($appAcl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$property = new Property\Text(); //generic class
		$dao = Dao\Factory::get()->getDao($property);
		$dao->deleteFromId($id);

		return $this->redirect()->toRoute('extendedproperties');
	}



}
