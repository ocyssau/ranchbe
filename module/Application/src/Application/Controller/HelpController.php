<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

class HelpController extends AbstractController
{

	/**
	 * 
	 */
    public function indexAction()
    {
    	$config =  $this->getServiceLocator()->get('Configuration');
    	$url = $config['externalLinks']['wiki']['url'];
    	header('location: ' . $url);
		die;
    }
}
