<?php

namespace Application\Controller;

class LayoutSelector
{
	public $pageId;

	/**
	 *
	 * @param unknown_type $pageId
	 */
	public function __construct($controller=null)
	{
	}

	/**
	 *
	 */
	public function setLayout($controller)
	{
		$pageId = $controller->pageId;
		isset($_SESSION['layout'][$pageId]) ? $layout = $_SESSION['layout'][$pageId] : $layout=null;
		isset($_REQUEST['layout']) ? $layout = $_REQUEST['layout'] : null;

		if($layout=='popup'){
			$controller->layout('layout/popup');
		}
		else if($layout=='fragment'){
			$controller->layout('layout/fragment');
		}
		else if($layout=='dialog'){
			$controller->layout('layout/dialog');
		}
		else if($layout=='viewer'){
			$controller->layout('layout/viewer');
		}
		else{
			$controller->layout('layout/ranchbe');
		}

		if($controller->view){
			$controller->view->layout = $layout;
		}

		$_SESSION['layout'][$pageId] = $layout;
		$this->controller = $controller;
		return $this;
	}

	/**
	 *
	 */
	public function clear($controller=null)
	{
		if(is_null($controller)){
			$_SESSION['layout'] = null;
		}
		else{
			$pageId = $controller->pageId;
			$_SESSION['layout'][$pageId] = null;
		}
		return $this;
	}

}

