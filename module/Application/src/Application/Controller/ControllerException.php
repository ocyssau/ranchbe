<?php
namespace Application\Controller;

class ControllerException extends \Rbplm\Sys\Exception
{
	/**
	 * @param string $message
	 * @param array $args
	 * @param Controller $controller
	 * @param integer $code = E_USER_WARNING|E_USER_ERROR|E_USER_NOTICE
	 */
	function __construct($message, $args=null, Controller $controller, $code=E_USER_WARNING)
	{
		parent::__construct($message, $code, $arg);
	}
}
