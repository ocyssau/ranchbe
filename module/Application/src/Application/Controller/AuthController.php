<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

class AuthController extends AbstractController
{
	protected $form;
	protected $successRedirect = 'index';

	/**
	 *
	 * {@inheritDoc}
	 * @see \Application\Controller\AbstractController::dispatch()
	 */
	public function dispatch(Request $request, Response $respons = null)
	{
		$this->authservice = $this->getServiceLocator()->get('AuthService');
		$this->storage = $this->authservice->getStorage();
		$this->init();
		$this->layout('layout/ranchbe');

		return AbstractActionController::dispatch($request,$respons);
	}

	/**
	 * @return \Application\Form\Auth
	 */
	protected function _getForm()
	{
		if (! $this->form) {
			$this->form = new \Application\Form\Auth();
			$url = $this->url()->fromRoute( 'auth/authenticate' );
			$this->form->setAttribute('action', $url);
		}
		return $this->form;
	}

	/**
	 * Login
	 */
	public function loginAction()
	{
		/* if already login, redirect to success page */
		if ($this->getAuthService()->hasIdentity()){
			return $this->redirect()->toRoute($this->successRedirect);
		}

		$view = $this->view;
		$form = $this->_getForm();

		$view->form = $form;
		$view->messages = $this->flashmessenger()->getMessages();
		return $view;
	}

	/**
	 * Authenticate
	 */
	public function authenticateAction()
	{
		$form = $this->_getForm();
		$redirect = 'auth/login';

		$request = $this->getRequest();
		$authService = $this->authservice;
		$storage = $authService->getStorage();

		if ($request->isPost()){
			$form->setData($request->getPost());
			if ($form->isValid()){
				/* check authentication... */
				$authService->getAdapter()
					->setIdentity($request->getPost('username'))
					->setCredential($request->getPost('password'));
				$result = $authService->authenticate();

				foreach($result->getMessages() as $message)
				{
					/* save message temporary into flashmessenger */
					$this->errorStack()->warning($message);
				}

				if ($result->isValid()) {
					$redirect = $this->successRedirect;
					/* check if it has rememberMe : */
					if ($request->getPost('rememberme') == 1 ) {
						$storage->setRememberMe(1);
					}
					$storage->write($result->getIdentity());
				}
			}
		}
		return $this->redirect()->toRoute($redirect, array());
	}

	/**
	 *
	 */
	public function logoutAction()
	{
		$this->authservice->clearIdentity();
		$this->storage->forgetMe();

		$this->errorStack()->feedback("You've been logged out");

		return $this->redirect()->toRoute('auth/login');
	}
}
