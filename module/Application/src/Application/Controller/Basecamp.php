<?php
namespace Application\Controller;

class Basecamp
{

	/**
	 */
	public function __construct()
	{
	}

	/**
	 *
	 */
	public function save($ifSuccessForward, $ifFailedForward, $view=null)
	{
		$session =& $_SESSION['navigation'];
		$session['from']['ifSuccessForward'] = $ifSuccessForward;
		$session['from']['ifFailedForward'] = $ifFailedForward;
		$session['basecamp'] = $ifSuccessForward;
		if($view){
			$view->basecamp = urlencode($ifSuccessForward);
		}
	}

	/**
	 *
	 */
	public function setForward($controller)
	{
		if(isset($_REQUEST['basecamp'])){
			$basecamp = $_REQUEST['basecamp'];
			$controller->view->basecamp = $basecamp;
			if($basecamp == 'default'){
				$_SESSION['navigation']['basecamp'] = $basecamp;
				$basecamp = null;
			}
			else if($basecamp){
				$controller->ifSuccessForward = urldecode($basecamp);
			}
			$_SESSION['navigation']['basecamp'] = $basecamp;
		}
		else if(isset($_SESSION['navigation']['basecamp'])){
			$basecamp = $_SESSION['navigation']['basecamp'];
			if($basecamp!=''){
				$controller->ifSuccessForward = urldecode($basecamp);
			}
		}
		else if(isset($_SESSION['navigation']['from'])){
			$session =& $_SESSION['navigation']['from'];
			$controller->ifSuccessForward = $session['ifSuccessForward'];
			$controller->ifFailedForward = $session['ifFailedForward'];
		}
		$this->controller = $controller;
	}

	/**
	 *
	 */
	public function clear()
	{
		$_SESSION['navigation'] = null;
	}

}
