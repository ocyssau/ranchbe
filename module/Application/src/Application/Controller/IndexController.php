<?php
namespace Application\Controller;

//Zend
use Zend\View\Model\ViewModel;
use Ranchbe;

class IndexController extends AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function init()
	{
		parent::init();
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$this->layout()->tabs = $tabs;
	}

	/**
	 *
	 */
	public function userpreferencesAction()
	{
		$view = new ViewModel();
	}

	/**
	 *
	 */
	public function aboutAction()
	{
		$this->layout()->tabs->getTab('Ranchbe')->activate('about');
		return array(
			'version' => array(
				'ver'=>Ranchbe::VERSION,
				'build'=>Ranchbe::BUILD,
				'copyright'=>Ranchbe::COPYRIGHT
				),
		);
	}

	/**
	 *
	 */
	public function licenceAction()
	{
		$lang = Ranchbe::get()->getConfig('view.lang');
		$file = './licence_'.$lang.'.txt';
		if(!is_file($file)){
			$file = './licence_en.txt';
		}
		$this->view->licence = file_get_contents($file);
		return $this->view;
	}
}
