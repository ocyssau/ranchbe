<?php
namespace Application\Model\Acl;

use Application\Model;
use Application\Dao;
use Rbplm\People\User;
use Application\Model\Acl\NoRoleException;

final class Application extends Acl
{
	static $classId = 501;

	CONST ROLE_MANAGER = 8; //8
	CONST ROLE_DOMAINUSER = 16; //16
	CONST ROLE_USER = 32; //32

	/**
	 * @param string $ressourceId
	 */
	public function __construct()
	{
		$this->cid = static::$classId;

		$this->rules = array(
			self::ROLE_DOMAINUSER=>self::RIGHT_CONSULT,
			self::ROLE_USER=>self::RIGHT_CONSULT,
			self::ROLE_MANAGER=>array(
				self::RIGHT_MANAGE,
				self::RIGHT_CONSULT,
				self::RIGHT_UPDATE,
				self::RIGHT_DELETE
			),
			self::ROLE_ADMIN=>self::RIGHT_ADMIN,
		);

		/*
		 * Array ROLE_ID=>array('name', array of parents)
		*/
		$this->roles = array(
			self::ROLE_GUEST=>array('GUEST',array()),
			self::ROLE_USER=>array('USER',array(self::ROLE_GUEST)),
			self::ROLE_DOMAINUSER=>array('DOMAIN USER',array(self::ROLE_USER)),
			self::ROLE_MANAGER=>array('MANAGER',array(self::ROLE_DOMAINUSER)),
			self::ROLE_ADMIN=>array('ADMINISTRATEUR'),
		);

		$ressourceId = Model\Application::$id;

		$this->acl = new \Zend\Permissions\Acl\Acl();
		$this->mainResource = new Resource( $ressourceId );
		$this->acl->addResource($this->mainResource);

		foreach($this->roles as $id=>$def){
			$this->acl->addRole(new Role($id),$def[1]);
		}

		foreach($this->rules as $roleName=>$privileges){
			$this->acl->allow( $roleName, $this->mainResource, $privileges );
		}
	}

	/**
	 * @param User $user
	 * @param string $resourceId
	 */
	public function loadRole(User $user, $resourceId=null)
	{
		if(self::$useLdap && $user->authFrom == 'ldap'){
			return $this->_loadRoleFromLdap($user, $resourceId);
		}
		elseif($user->memberof && $user->authFrom == 'db'){
			return $this->_loadRoleFromLocalDb($user, $resourceId);

			$userId = $user->getLogin();
			$this->userId = $userId;
			$roles = array(
				(int)$user->memberof,
			);
			$this->userRole = $roles;
			$this->acl->addRole($userId, $roles);
			return $roles;
		}
		else{
			return $this->_loadRoleFromLocalDb($user, $resourceId);
		}
	}

	/**
	 * Load a role from Db and add to Acl
	 * @param User $user
	 * @param string $resourceId
	 */
	protected function _loadRoleFromLdap(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();

		$memberof = $user->memberof;

		if($memberof){
			if(in_array('RbportailAdmin', $memberof) || $userId=='administrateur' ){
				$roles[] = self::ROLE_ADMIN;
			}
			if(in_array('RbportailProjectManager', $memberof)){
				$roles[] = self::ROLE_MANAGER;
			}
			if(in_array('RbportailCollaborator', $memberof)){
				$roles[] = self::ROLE_DOMAINUSER;
			}
			if(in_array('Cloud', $memberof)){
				$roles[] = self::ROLE_DOMAINUSER;
			}
			if(in_array('GG caomeca', $memberof)){
				$roles[] = self::ROLE_DOMAINUSER;
			}
			if(count($roles)==0){
				throw new NoRoleException('User has no role define for this application');
			}
			else{
				$this->acl->addRole($userId, $roles);
			}
		}
		else{
			throw new \Exception( sprintf( 'A group must be set for user %s', $user->getLogin() ) );
		}
		$this->userRole = $roles;
		return $roles;
	}

	/**
	 * Load a role from Db and add to Acl
	 * @param User $user
	 * @param string $resourceId
	 */
	protected function _loadRoleFromLocalDb(User $user, $resourceId=null)
	{
		if(!$resourceId){
			$resourceId = $this->mainResource->getResourceId();
		}

		$userId = $user->getLogin();
		$this->userId = $userId;
		$roles = array();

		$memberof = $user->memberof;

		if($memberof == self::ROLE_ADMIN || $userId=='administrateur' ){
			$roles[] = self::ROLE_ADMIN;
		}
		elseif($memberof == self::ROLE_MANAGER){
			$roles[] = self::ROLE_MANAGER;
		}
		elseif($memberof == self::ROLE_DOMAINUSER){
			$roles[] = self::ROLE_DOMAINUSER;
		}
		elseif($memberof == self::ROLE_USER){
			$roles[] = self::ROLE_USER;
		}
		elseif($memberof == self::ROLE_GUEST){
			$roles[] = self::ROLE_GUEST;
		}
		else{
			throw new NoRoleException('User has no role define for this application');
		}

		$this->acl->addRole($userId, $roles);
		$this->userRole = $roles;
		return $roles;
	}
}
