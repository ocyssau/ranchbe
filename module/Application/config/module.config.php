<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link  http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
	'router' => array(
		'routes' => array(
			'index' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/[:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Index',
						'action' => 'index'
					)
				)
			),
			'extendedproperties' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/extendedproperties[/][:action[/][:id]]',
					'defaults' => array(
						'controller' => 'Application\Controller\Extendedproperties',
						'action' => 'index'
					)
				)
			),
			'auth' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/auth',
					'defaults' => array(
						'controller' => 'Application\Controller\Auth',
						'action' => 'login'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'login' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/login',
							'defaults' => array(
								'action' => 'login'
							)
						)
					),
					'logout' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/logout',
							'defaults' => array(
								'action' => 'logout'
							)
						)
					),
					'authenticate' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/authenticate',
							'defaults' => array(
								'action' => 'authenticate'
							)
						)
					)
				)
			),
			'help' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/help[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Help',
						'action' => 'index'
					)
				)
			),
			'search' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/search[/:action]',
					'defaults' => array(
						'controller' => 'Application\Controller\Search',
						'action' => 'index'
					)
				)
			),
			// The following is a route to simplify getting started creating
			// new controllers and actions without needing to create a new
			// module. Simply drop new controllers in, and you can access them
			// using the path /application/:controller/:action
			'application' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/rbapplication',
					'defaults' => array(
						'__NAMESPACE__' => 'Application\Controller',
						'controller' => 'Index',
						'action' => 'index'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/[:controller[/:action]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
							),
							'defaults' => array()
						)
					)
				)
			)
		)
	),
	'service_manager' => array(
		'abstract_factories' => array(
			'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
			'Zend\Log\LoggerAbstractServiceFactory'
		),
		'aliases' => array(
			'translator' => 'MvcTranslator'
		)
	),
	'translator' => array(
		'locale' => 'en_US',
		'translation_file_patterns' => array(
			array(
				'type' => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.mo'
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Application\Controller\Index' => 'Application\Controller\IndexController',
			'Application\Controller\Auth' => 'Application\Controller\AuthController',
			'Application\Controller\Help' => 'Application\Controller\HelpController',
			'Application\Controller\Search' => 'Application\Controller\SearchController',
			'Application\Controller\Extendedproperties' => 'Application\Controller\ExtendedpropertiesController'
		)
	),
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions' => true,
		'doctype' => 'HTML5',
		'not_found_template' => 'error/404',
		'exception_template' => 'error/index',
		'template_map' => array(
			'module_layouts' => array(
				'Admin' => 'layout/layout',
				'Workflow' => 'layout/ranchbe',
				'Ranchbe' => 'layout/ranchbe',
				'Pdm' => 'layout/ranchbe',
				'Discussion' => 'layout/ranchbe',
				'Ged' => 'layout/ranchbe'
			),
			'layout/ranchbe' => __DIR__ . '/../view/layout/ranchbe.phtml',
			'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
			'layout/consult' => __DIR__ . '/../view/layout/consult.phtml',
			'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
			'error/404' => __DIR__ . '/../view/error/404.phtml',
			'error/403' => __DIR__ . '/../view/error/403.phtml',
			'error/index' => __DIR__ . '/../view/error/index.phtml'
		),
		'template_path_stack' => array(
			__DIR__ . '/../view'
		)
	),
	// Placeholder for console routes
	'console' => array(
		'router' => array(
			'routes' => array()
		)
	)
);
