<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\EventManager\EventInterface as Event;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Rbs\Auth;

class Module implements AutoloaderProviderInterface
{
	/**
	 *
	 * @param Event $e
	 * @throws \Exception
	 */
	public function onBootstrap(Event $e)
	{
		$config = $e->getApplication()->getServiceManager()->get('Configuration');

		session_start();

		if(!isset($config['rbp'])){
			throw new \Exception('Config for Ranchbe is not set', E_ERROR);
		}

		rbinit_daoservice($config['rbp']);

		$eventManager        = $e->getApplication()->getEventManager();
		$eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
			$controller      = $e->getTarget();
			$controllerClass = get_class($controller);
			$moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
			$config          = $e->getApplication()->getServiceManager()->get('config');
			if (isset($config['view_manager']['template_map']['module_layouts'][$moduleNamespace])) {
				$controller->layout( $config['view_manager']['template_map']['module_layouts'][$moduleNamespace] );
			}
		}, 100);
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	}

	/**
	 *
	 */
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	/**
	 *
	 */
	public function getViewHelperConfig()
	{
		return array(
			'invokables' => array(
				'tableSqlSortableHeader' => 'Application\View\Helper\TableSqlSortableHeader',
				'accessCodeFormater' => 'Application\View\Helper\AccessCodeFormater',
				'categoryIdToName' => 'Application\View\Helper\CategoryIdToName',
				'userIdToName' => 'Application\View\Helper\UserIdToName',
				'processIdToName' => 'Application\View\Helper\ProcessIdToName',
				'projectIdToName' => 'Application\View\Helper\ProjectIdToName',
				'versionFormater' => 'Application\View\Helper\VersionFormater',
				'getPostit' => 'Application\View\Helper\GetPostit',
				'getThumbnail' => 'Application\View\Helper\GetThumbnail',
				'dateFormater' => 'Application\View\Helper\DateFormater',
				'doctypeFormater' => 'Application\View\Helper\DoctypeFormater',
				'fileicon' => 'Application\View\Helper\FileIcon',
				'displayPoint' => 'Application\View\Helper\DisplayPoint',
				'filesizeFormater' => 'Application\View\Helper\FilesizeFormater',
				'displayAddress' => 'Application\View\Helper\DisplayAddress',
				'cidToClass' => 'Application\View\Helper\CidToClass',
			),
		);
	}

	/**
	 *
	 */
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'AuthStorage' => function($sm){
					$storage = new Auth\Storage('rbportail');
					return $storage;
				},
				'AuthService' => function($sm) {
					//$sm = ServiceManager
					$config = $sm->get('Configuration');
					$AuthAdapter = Auth\Factory::getAdapter($config['rbp']['auth']); //= Application\Model\Auth\Ldap or DbAdapter
					$authService = new AuthenticationService();
					$authService->setAdapter($AuthAdapter);
					$authService->setStorage($sm->get('AuthStorage'));
					return $authService;
				},
				'Workflow' => function($sm) {
					$config = $sm->get('Configuration');
					\Docflow\Service\Workflow\Code::$processPath = $config['rbp']['path.workflow.process'];
					$workflow = new \Docflow\Service\Workflow();
					$workflow->serviceManager = $sm;
					return $workflow;
				}
			),
			'aliases' => array(),
			'abstract_factories' => array(
			),
			'invokables' => array(),
			'services' => array(),
			'shared' => array(),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\ModuleManager\Feature.AutoloaderProviderInterface::getAutoloaderConfig()
	 */
	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
					'Rbplm'=>realpath('./lib/Rbplm'),
					'View'=>realpath('./View'),
					'Form'=>realpath('./Form'),
					'ProcessDef'=>realpath('./data/Process/ProcessDef'),
				),
			),
		);
	}
}
