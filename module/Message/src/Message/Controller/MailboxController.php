<?php
namespace Message\Controller;

use Application\Controller\AbstractController;
use Rbplm\Sys\Message;
use Rbplm\People;
use Form\Application\PaginatorForm;
use Rbs\Dao\Sier\Filter;
use Rbplm\Dao\Filter\Op;

class MailboxController extends AbstractController
{

	public $pageId = 'message_mailbox';

	public $defaultSuccessForward = 'message/mailbox/index';

	public $defaultFailedForward = 'message/mailbox/index';

	/**
	 */
	public function init()
	{
		parent::init();
		$this->message = new Message(Message::TYPE_MAILBOX);
		$this->indexTemplate = 'message/mailbox/index.phtml';

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{
		$view = $this->view;
		$view->setTemplate($this->indexTemplate);

		$ownerUid = People\CurrentUser::get()->getLogin();
		$request = $this->getRequest();
		$factory = \Rbs\Space\Factory::get();
		$ranchbe = \Ranchbe::get();
		$message = $this->message;

		$list = $factory->getList($message::$classId);
		$dao = $factory->getDao($message::$classId);

		$filter = new \Rbs\Dao\Sier\Filter('', false);
		$filter->andfind($ownerUid, $dao->toSys('owner'), Op::EQUAL);

		$paginator = new \Application\Form\PaginatorForm($this->pageId);
		$paginator->setData($_POST)->setData($_GET);
		$paginator->setMaxLimit($list->countAll($filter));
		$paginator->prepare()->bindToView($view)->bindToFilter($filter)->save();

		$filterForm = new \Message\Form\FilterForm($factory, $this->pageId);
		$filterForm->load();
		$filterForm->bindToFilter($filter)->save();

		$list->load($filter, $filterForm->bind);
		$view->list = $list->toArray();
		$view->filter = $filterForm;

		/* set data for gauge */
		$cellsize = 200;
		$percentage = 1;
		$mailboxMaxSize = $ranchbe->getConfig('message.mailbox.maxsize');
		if ( $mailboxMaxSize > 0 ) {
			$currentSize = $paginator->maxLimit;
			$percentage = ($currentSize / $mailboxMaxSize) * 100;
			$cellsize = round($percentage / 100 * 200);
			if ( $currentSize > $mailboxMaxSize ) {
				$cellsize = 200;
			}
			if ( $cellsize < 1 ) {
				$cellsize = 1;
			}
			$percentage = round($percentage);
		}

		$view->mailboxSize = $currentSize;
		$view->mailboxMaxsize = $mailboxMaxSize;
		$view->cellsize = $cellsize;
		$view->percentage = $percentage;
		$view->pageTitle = 'Mail Box';

		return $view;
	}

	/**
	 *
	 */
	public function readAction()
	{
		(isset($_REQUEST['priority'])) ? $priority = $_REQUEST['priority'] : $priority = null;
		(isset($_REQUEST['sort_mode'])) ? $sortMode = $_REQUEST['sort_mode'] : $sortMode = null;
		(isset($_REQUEST['offset'])) ? $offset = $_REQUEST['offset'] : $offset = null;
		(isset($_REQUEST['find'])) ? $find = $_REQUEST['find'] : $find = null;
		(isset($_REQUEST['id'])) ? $id = $_REQUEST['id'] : $id=null;

		if ( !$id ) {
			return $this->successForward();
		}

		$message = $this->message;
		$factory = \Rbs\Space\Factory::get();
		$dao = $factory->getDao($message);
		$template = 'message/read/index.tpl';
		$dao->loadFromId($message, $id);
		$view = $this->view;
		$view->setTemplate('message/read/index.phtml');

		$view->flag = $flag;
		$view->priority = $priority;
		$view->flagval = $flagval;
		$view->offset = $offset;
		$view->sort = $sortMode;
		$view->find = $find;

		/* Mark the message as read in the receivers mailbox */
		$message->isRead(true);

		/* Get the message and assign its data to template vars */
		$view->message = $message;
		return $view;
	}

	/**
	 * Mark messages if the mark button was pressed
	 */
	public function markAction()
	{
		$factory = \Rbs\Space\Factory::get();
		$dao = $factory->getDao(Message::$classId);
		$markas = (string)$this->params()->fromQuery('markas');
		if ( $this->msgIds ) {
			foreach( array_keys($this->msgIds) as $msgId ) {
				$parts = explode('_', $markas);
				$message = new Message(Message::TYPE_MAILBOX);
				$dao->loadFromId($message, $msgId);
				$message->isFlagged = true;
				$dao->save($message);
			}
		}
		return $this->successForward();
	}

	/**
	 * Delete messages if the delete button was pressed
	 */
	public function deleteAction()
	{
		(isset($_REQUEST['checked'])) ? $msgIds = $_REQUEST['checked'] : $msgIds=array();

		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		if ( $msgIds ) {
			foreach( $msgIds as $msgId ) {
				$dao->deleteFromId($msgId);
			}
		}
		return $this->successForward();
	}

	/**
	 * Archive messages if the archive button was pressed
	 */
	public function archiveAction()
	{
		(isset($_REQUEST['checked'])) ? $checked = $_REQUEST['checked'] : $checked=array();

		$ownerUid = People\CurrentUser::get()->getLogin();

		$factory = \Rbs\Space\Factory::get();
		$ranchbe = \Ranchbe::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);
		$list = $factory->getList(Message\Archived::$classId);

		$filter = new Filter('', false);
		$filter->andfind($ownerUid, $dao->toSys('owner'), Op::EQUAL);

		$count = $list->countAll($filter);
		$maxArchiveSize = $ranchbe->getConfig('message.archive.maxsize');

		if ( $maxArchiveSize > 0 && $count >= $maxArchiveSize ) {
			$msg = tra("Archive is full. Delete some messages from archive first.");
			$this->errorStack()->error($msg);
			$this->successForward();
		}

		if ( $checked ) {
			$filter = "`id`=:id";
			foreach( $checked as $id ) {
				$bind = array(':id' => $id);
				$dao->archive($filter, $bind);
			}
		}
		return $this->successForward();
	}

	/**
	 * Archive messages if the archive button was pressed
	 */
	public function autoarchiveAction()
	{
		$days = (int)$this->params()->fromQuery('days');

		$userUid = People\CurrentUser::get()->getLogin();
		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		if ( $days < 1 ) {
			return $this->successForward();
		}

		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$filter = "`owner`=:user AND `date`<=:age";
		$bind = array(
			':user' => $userUid,
			':age' => $age
		);
		$dao->archive($filter, $bind);

		return $this->successForward();
	}

	/**
	 * Delete old messages
	 */
	public function autodeleteAction()
	{
		$userId = People\CurrentUser::get()->getUid();
		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		$days = (int)$this->params()->fromQuery('days');

		if ( $days < 1 ) {
			return $this->successForward();
		}

		$age = date("U") - ($days * 3600 * 24);

		// @todo: only move as much msgs into archive as there is space left in there
		$filter = "`owner`=:user AND `date`<=:age";
		$bind = array(
			':user' => $userId,
			':age' => $age
		);

		$dao->delete($filter, $bind);

		return $this->successForward();
	}


	/**
	 * Download messages if the download button was pressed
	 */
	public function downloadAction()
	{
		(isset($_REQUEST['checked'])) ? $checked = $_REQUEST['checked'] : $checked=array();

		$factory = \Rbs\Space\Factory::get();
		$message = $this->message;
		$dao = $factory->getDao($message::$classId);

		/* if message ids are handed over, use them: */
		if ( $checked ) {
			foreach( $checked as $id ) {
				$m = clone($message);
				$dao->loadFromId($m, $id);
				$items[] = $m;
			}
		}
		$this->view->items = $items;

		$fileName = 'mailbox-' . time("U") . '.txt';
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: application/txt");
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		die();
	}

} //End of class
