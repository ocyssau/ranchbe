<?php
namespace Message\Controller;

use Application\Controller\AbstractController;
use Rbplm\Sys\Message;
use Rbs\Sys\Mail;
use Rbplm\People;
use Rbs\Space\Factory as DaoFactory;

class ComposeController extends AbstractController
{

	public $pageId = 'message_compose';

	public $defaultSuccessForward = 'message/compose/send';

	public $defaultFailedForward = 'message/compose/send';

	/**
	 * (non-PHPdoc)
	 *
	 * @see Controller.Controller::init()
	 */
	public function init()
	{
		parent::init();

		/* Record url for page and Active the tab */
		$tabs = \View\Helper\MainTabBar::get($this->view);
		$tabs->getTab('home')->activate();
		$this->layout()->tabs = $tabs;
	}

	/**
	 */
	public function indexAction()
	{}

	/**
	 * Strip Re:Re:Re: from subject
	 */
	public function replyAction()
	{
		$msgId = $this->getRequest()->getParam('msgId');
		$reply = $this->getRequest()->getParam('reply');
		$replyall = $this->getRequest()->getParam('replyall');

		$currentUser = People\CurrentUser::get();
		$message = new Message(Message::TYPE_MAILBOX);

		$msg = $message->getMessage($currentUser->getId(), $msgId);
		$subject = $msg['subject'];
		$body = $msg['body'];

		// Strip Re:Re:Re: from subject
		if ( $reply || $replyall ) {
			$subject = tra("Re:") . ereg_replace("^(" . tra("Re:") . ")+", "", $subject);
		}
	}

	/**
	 */
	public function sendAction()
	{
		$view = $this->view;

		isset($_POST['validate']) ? $validate = $_POST['validate'] : $validate = null;
		isset($_POST['bymessage']) ? $byMessage = $_POST['bymessage'] : $byMessage = true;
		isset($_POST['bymail']) ? $byMail = $_POST['bymail'] : $byMail = false;
		isset($_POST['toall']) ? $toAll = $_POST['toall'] : $toAll = null;

		$datas = $_POST;
		isset($_POST['subject']) ? $datas['subject'] = $_POST['subject'] : $subject = null;
		isset($_POST['body']) ? $datas['body'] = $_POST['body'] : $body = null;
		isset($_POST['priority']) ? $datas['priority'] = $_POST['priority'] : $priority = null;

		$factory = DaoFactory::get();
		$currentUser = People\CurrentUser::get();

		$message = new Message(Message::TYPE_SEND);
		$message->setOwner($currentUser);
		$message->setFrom($currentUser->getLogin(), $currentUser->getLogin());

		$message->bymail = $byMail;
		$message->bymessage = $byMessage;
		$message->toall = $toAll;

		$form = new \Message\Form\ComposeForm($view);
		$form->bind($message);

		/* Try to validate the form */
		if ( $validate ) {
			$form->setData($datas);
			if ( $form->isValid() ) {
				if ( $toAll ) {
					$users = $factory->getList(People\User::$classId);
					$users->load();
					foreach( $users as $user ) {
						$to = $user['login'];
						$message->addTo($to, $user['login']);
					}
				}

				if ( $byMail ) {
					try {
						$userDao = $factory->getDao(People\User::$classId);
						$mailMessage = new \Rbs\Sys\Mail();
						$from = $currentUser->getMail();
						if ( $from ) {
							$mailMessage->setFrom($currentUser->getMail(), $currentUser->getLogin());
						}
						else {
							throw new \Exception('Current user mail is not defined');
						}

						foreach( $message->getTo() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addTo($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						foreach( $message->getCc() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addCc($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						foreach( $message->getBcc() as $login ) {
							try {
								$user = new People\User();
								$userDao->loadFromLogin($user, $login);
								$toMail = $user->getMail();
								if ( $toMail ) {
									$mailMessage->addBcc($toMail, $user->getFullName());
								}
							}
							catch( \Exception $e ) {}
						}
						$mailMessage->setSubject($message->getSubject());
						$mailMessage->setBody($message->getBody());
						$mailMessage->send();
					}
					catch( \Exception $e ) {
						$this->errorStack()->error($e->getMessage());
					}
				}
				if ( $byMessage ) {
					try {
						$dao = $factory->getDao(Message::$classId);
						$dao->save($message);

						$dao = $factory->getDao(Message\Sent::$classId);
						$dao->save($message);
					}
					catch( \Exception $e ) {
						$this->errorStack()->error($e->getMessage());
					}
				}
			}
		}

		$view->setTemplate($form->template);
		$view->pageTitle = 'Compose Message';
		$view->form = $form;
		return $view;
	}
} //End of class

