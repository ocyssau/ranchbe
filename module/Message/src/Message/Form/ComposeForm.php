<?php
namespace Message\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ArraySerializable as Hydrator;
use Zend\InputFilter\InputFilterProviderInterface;

class ComposeForm extends Form implements InputFilterProviderInterface
{

	/**
	 *
	 * @param string $name
	 */
	public function __construct($view, $factory = null)
	{
		/* we want to ignore the name passed */
		parent::__construct('composeForm');

		$this->template = 'message/compose/index';
		$this->setAttribute('method', 'post')
			->setHydrator(new Hydrator(false))
			->setInputFilter(new InputFilter())
			->setAttribute('method', 'post');

		$this->daoFactory = $factory;
		$inputFilter = $this->getInputFilter();

		/* TO */
		$this->add(array(
			'name' => 'to',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('To')
			)
		));
		$inputFilter->add(array(
			'name' => 'to',
			'required' => true,
			'filters' => array(
				new \Message\Form\Filter\StringToArray()
			)
		));

		/* CC */
		$this->add(array(
			'name' => 'cc',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Cc')
			)
		));
		$inputFilter->add(array(
			'name' => 'cc',
			'required' => false,
			'filters' => array(
				new \Message\Form\Filter\StringToArray()
			)
		));

		/* BCC */
		$this->add(array(
			'name' => 'bcc',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'type' => 'text',
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Bcc')
			)
		));
		$inputFilter->add(array(
			'name' => 'bcc',
			'required' => false,
			'filters' => array(
				new \Message\Form\Filter\StringToArray()
			)
		));

		/* SUBJECT */
		$this->add(array(
			'name' => 'subject',
			'type' => 'Zend\Form\Element\Text',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'size' => 110
			),
			'options' => array(
				'label' => tra('Subject')
			)
		));
		$inputFilter->add(array(
			'name' => 'subject',
			'required' => true
		));

		/* BODY */
		$this->add(array(
			'name' => 'body',
			'type' => 'Zend\Form\Element\Textarea',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control',
				'rows' => 20,
				'cols' => 108
			),
			'options' => array(
				'label' => tra('Body')
			)
		));
		$inputFilter->add(array(
			'name' => 'body',
			'required' => false
		));

		/* PRIORITY */
		$this->add(array(
			'name' => 'priority',
			'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'placeholder' => '',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('Priority'),
				'value_options' => array(
					1 => '1 - Very Low',
					2 => '2 - Low',
					3 => '3 - Normal',
					4 => '4 - High',
					5 => '5 - Very High'
				)
			)
		));
		$inputFilter->add(array(
			'name' => 'priority',
			'required' => true
		));

		/* TO ALL */
		$this->add(array(
			'name' => 'toall',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('To All Ranchbe Users')
			)
		));
		$inputFilter->add(array(
			'name' => 'toall',
			'required' => false
		));

		/* BY MAIL */
		$this->add(array(
			'name' => 'bymail',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('By Mail')
			)
		));
		$inputFilter->add(array(
			'name' => 'bymail',
			'required' => false
		));

		/* BY MESSAGE */
		$this->add(array(
			'name' => 'bymessage',
			'type' => 'Zend\Form\Element\Checkbox',
			'attributes' => array(
				'class' => 'form-control'
			),
			'options' => array(
				'label' => tra('By Ranchbe Internal Message')
			)
		));
		$inputFilter->add(array(
			'name' => 'bymessage',
			'required' => false
		));

		/* Submit button */
		$this->add(array(
			'name' => 'validate',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Save'),
				'id' => 'submitbutton',
				'class' => 'btn btn-success'
			)
		));

		/* Cancel button */
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'submit',
				'value' => tra('Cancel'),
				'id' => 'cancelbutton',
				'class' => 'btn btn-default'
			)
		));
	}

	/**
	 *
	 * @return \Zend\InputFilter\InputFilter
	 */
	public function getInputFilterSpecification()
	{
		return array();
	}

	/**
	 * Recursively extract values for elements and sub-fieldsets
	 *
	 * @return array
	 */
	protected function extract()
	{
		$datas = parent::extract();

		if ( is_array($datas['to']) ) {
			$datas['to'] = implode(' ', $datas['to']);
		}
		if ( is_array($datas['from']) ) {
			$datas['from'] = implode(' ', $datas['from']);
		}
		if ( is_array($datas['cc']) ) {
			$datas['cc'] = implode(' ', $datas['cc']);
		}
		if ( is_array($datas['bcc']) ) {
			$datas['bcc'] = implode(' ', $datas['bcc']);
		}

		return $datas;
	}
}
