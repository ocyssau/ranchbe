<?php
return array(
	'router' => array(
		'routes' => array(
			'message' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/message',
					'defaults' => array(
						'__NAMESPACE__' => 'Message\Controller',
						'controller' => 'Mailbox',
						'action' => 'index'
					)
				),
				'may_terminate' => true,
				'child_routes' => array(
					'default' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/[:controller[/:action[/:id]]]',
							'constraints' => array(
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
							),
							'defaults' => array()
						)
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Message\Controller\Compose' => 'Message\Controller\ComposeController',
			'Message\Controller\Mailbox' => 'Message\Controller\MailboxController',
			'Message\Controller\Sent' => 'Message\Controller\SentController',
			'Message\Controller\Archive' => 'Message\Controller\ArchiveController',

		)
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Message' => __DIR__ . '/../view'
		)
	),
);
