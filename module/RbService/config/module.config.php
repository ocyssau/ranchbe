<?php
return array(
    'router' => array(
        'routes' => array(
            'rb-service.rest.container' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/container[/:container_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\Container\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.vault-record' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/vault-record[/:vault_record_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\VaultRecord\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.docfile-version' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/docfile-version[/:docfile_version_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\DocfileVersion\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.document-version' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/document-version[/:document_version_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\DocumentVersion\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.fs-data' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/fs-data[/:fs_data_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\FsData\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.link' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/link[/:link_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\Link\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.checkin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkin[/:checkin_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\Checkin\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.checkout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkout[/:checkout_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\Checkout\\Controller',
                    ),
                ),
            ),
            'rb-service.rest.upload' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/upload[/:upload_id]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rest\\Upload\\Controller',
                    ),
                ),
            ),
            'rb-service.rpc.download' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/download[/:file]',
                    'defaults' => array(
                        'controller' => 'RbService\\V1\\Rpc\\Download\\Controller',
                        'action' => 'download',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'rb-service.rest.container',
            3 => 'rb-service.rest.vault-record',
            4 => 'rb-service.rest.docfile-version',
            5 => 'rb-service.rest.document-version',
            6 => 'rb-service.rest.fs-data',
            7 => 'rb-service.rest.link',
            9 => 'rb-service.rest.checkin',
            10 => 'rb-service.rest.checkout',
            11 => 'rb-service.rest.upload',
            12 => 'rb-service.rpc.download',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'RbService\\V1\\Rest\\Container\\ContainerResource' => 'RbService\\V1\\Rest\\Container\\ContainerResourceFactory',
            'RbService\\V1\\Rest\\VaultRecord\\VaultRecordResource' => 'RbService\\V1\\Rest\\VaultRecord\\VaultRecordResourceFactory',
            'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionResource' => 'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionResourceFactory',
            'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionResource' => 'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionResourceFactory',
            'RbService\\V1\\Rest\\FsData\\FsDataResource' => 'RbService\\V1\\Rest\\FsData\\FsDataResourceFactory',
            'RbService\\V1\\Rest\\Link\\LinkResource' => 'RbService\\V1\\Rest\\Link\\LinkResourceFactory',
            'RbService\\V1\\Rest\\Checkin\\CheckinResource' => 'RbService\\V1\\Rest\\Checkin\\CheckinResourceFactory',
            'RbService\\V1\\Rest\\Checkout\\CheckoutResource' => 'RbService\\V1\\Rest\\Checkout\\CheckoutResourceFactory',
            'RbService\\V1\\Rest\\Upload\\UploadResource' => 'RbService\\V1\\Rest\\Upload\\UploadResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'RbService\\V1\\Rest\\Container\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\Container\\ContainerResource',
            'route_name' => 'rb-service.rest.container',
            'route_identifier_name' => 'container_id',
            'collection_name' => 'container',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\Container\\ContainerEntity',
            'collection_class' => 'RbService\\V1\\Rest\\Container\\ContainerCollection',
            'service_name' => 'Container',
        ),
        'RbService\\V1\\Rest\\VaultRecord\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\VaultRecord\\VaultRecordResource',
            'route_name' => 'rb-service.rest.vault-record',
            'route_identifier_name' => 'vault_record_id',
            'collection_name' => 'vault_record',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\VaultRecord\\VaultRecordEntity',
            'collection_class' => 'RbService\\V1\\Rest\\VaultRecord\\VaultRecordCollection',
            'service_name' => 'VaultRecord',
        ),
        'RbService\\V1\\Rest\\DocfileVersion\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionResource',
            'route_name' => 'rb-service.rest.docfile-version',
            'route_identifier_name' => 'docfile_version_id',
            'collection_name' => 'docfile_version',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionEntity',
            'collection_class' => 'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionCollection',
            'service_name' => 'DocfileVersion',
        ),
        'RbService\\V1\\Rest\\DocumentVersion\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionResource',
            'route_name' => 'rb-service.rest.document-version',
            'route_identifier_name' => 'document_version_id',
            'collection_name' => 'document_version',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionEntity',
            'collection_class' => 'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionCollection',
            'service_name' => 'DocumentVersion',
        ),
        'RbService\\V1\\Rest\\FsData\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\FsData\\FsDataResource',
            'route_name' => 'rb-service.rest.fs-data',
            'route_identifier_name' => 'fs_data_id',
            'collection_name' => 'fs_data',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\FsData\\FsDataEntity',
            'collection_class' => 'RbService\\V1\\Rest\\FsData\\FsDataCollection',
            'service_name' => 'FsData',
        ),
        'RbService\\V1\\Rest\\Link\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\Link\\LinkResource',
            'route_name' => 'rb-service.rest.link',
            'route_identifier_name' => 'link_id',
            'collection_name' => 'link',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\Link\\LinkEntity',
            'collection_class' => 'RbService\\V1\\Rest\\Link\\LinkCollection',
            'service_name' => 'Link',
        ),
        'RbService\\V1\\Rest\\Checkin\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\Checkin\\CheckinResource',
            'route_name' => 'rb-service.rest.checkin',
            'route_identifier_name' => 'checkin_id',
            'collection_name' => 'checkin',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\Checkin\\CheckinEntity',
            'collection_class' => 'RbService\\V1\\Rest\\Checkin\\CheckinCollection',
            'service_name' => 'Checkin',
        ),
        'RbService\\V1\\Rest\\Checkout\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\Checkout\\CheckoutResource',
            'route_name' => 'rb-service.rest.checkout',
            'route_identifier_name' => 'checkout_id',
            'collection_name' => 'checkout',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_http_methods' => array(
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\Checkout\\CheckoutEntity',
            'collection_class' => 'RbService\\V1\\Rest\\Checkout\\CheckoutCollection',
            'service_name' => 'Checkout',
        ),
        'RbService\\V1\\Rest\\Upload\\Controller' => array(
            'listener' => 'RbService\\V1\\Rest\\Upload\\UploadResource',
            'route_name' => 'rb-service.rest.upload',
            'route_identifier_name' => 'upload_id',
            'collection_name' => 'upload',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'RbService\\V1\\Rest\\Upload\\UploadEntity',
            'collection_class' => 'RbService\\V1\\Rest\\Upload\\UploadCollection',
            'service_name' => 'upload',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'RbService\\V1\\Rest\\Container\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\VaultRecord\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\DocfileVersion\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\DocumentVersion\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\FsData\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\Link\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\Checkin\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\Checkout\\Controller' => 'HalJson',
            'RbService\\V1\\Rest\\Upload\\Controller' => 'HalJson',
            'RbService\\V1\\Rpc\\Download\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'RbService\\V1\\Rest\\Container\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\VaultRecord\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\DocfileVersion\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\DocumentVersion\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\FsData\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Link\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Checkin\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Checkout\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Upload\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'RbService\\V1\\Rpc\\Download\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'RbService\\V1\\Rest\\Container\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\VaultRecord\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\DocfileVersion\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\DocumentVersion\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\FsData\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Link\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Checkin\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'RbService\\V1\\Rest\\Checkout\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rest\\Upload\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
            'RbService\\V1\\Rpc\\Download\\Controller' => array(
                0 => 'application/vnd.rb-service.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'RbService\\V1\\Rest\\Container\\ContainerEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.container',
                'route_identifier_name' => 'container_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\Container\\ContainerCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.container',
                'route_identifier_name' => 'container_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\VaultRecord\\VaultRecordEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.vault-record',
                'route_identifier_name' => 'vault_record_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\VaultRecord\\VaultRecordCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.vault-record',
                'route_identifier_name' => 'vault_record_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.docfile-version',
                'route_identifier_name' => 'docfile_version_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\DocfileVersion\\DocfileVersionCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.docfile-version',
                'route_identifier_name' => 'docfile_version_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.document-version',
                'route_identifier_name' => 'document_version_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\DocumentVersion\\DocumentVersionCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.document-version',
                'route_identifier_name' => 'document_version_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\FsData\\FsDataEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.fs-data',
                'route_identifier_name' => 'fs_data_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\FsData\\FsDataCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.fs-data',
                'route_identifier_name' => 'fs_data_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\Link\\LinkEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.link',
                'route_identifier_name' => 'link_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\Link\\LinkCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.link',
                'route_identifier_name' => 'link_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\Checkin\\CheckinEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.checkin',
                'route_identifier_name' => 'checkin_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\Checkin\\CheckinCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.checkin',
                'route_identifier_name' => 'checkin_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\Checkout\\CheckoutEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.checkout',
                'route_identifier_name' => 'checkout_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'RbService\\V1\\Rest\\Checkout\\CheckoutCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.checkout',
                'route_identifier_name' => 'checkout_id',
                'is_collection' => true,
            ),
            'RbService\\V1\\Rest\\Upload\\UploadEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.upload',
                'route_identifier_name' => 'upload_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'RbService\\V1\\Rest\\Upload\\UploadCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'rb-service.rest.upload',
                'route_identifier_name' => 'upload_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'RbService\\V1\\Rest\\Link\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\Link\\Validator',
        ),
        'RbService\\V1\\Rest\\Container\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\Container\\Validator',
        ),
        'RbService\\V1\\Rest\\VaultRecord\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\VaultRecord\\Validator',
        ),
        'RbService\\V1\\Rest\\DocfileVersion\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\DocfileVersion\\Validator',
        ),
        'RbService\\V1\\Rest\\DocumentVersion\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\DocumentVersion\\Validator',
        ),
        'RbService\\V1\\Rest\\Checkin\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\Checkin\\Validator',
        ),
        'RbService\\V1\\Rest\\Checkout\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\Checkout\\Validator',
        ),
        'RbService\\V1\\Rest\\Upload\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rest\\Upload\\Validator',
        ),
        'RbService\\V1\\Rpc\\Download\\Controller' => array(
            'input_filter' => 'RbService\\V1\\Rpc\\Download\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'RbService\\V1\\Rest\\Link\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'parentId',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'childId',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'lindex',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            4 => array(
                'name' => 'data',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            5 => array(
                'name' => 'class',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => 'Class of link:
Link/Document/Docfile
Link/Docfile/VaultRecord
...',
            ),
        ),
        'RbService\\V1\\Rest\\Container\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\HtmlEntities',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\Word\\SeparatorToCamelCase',
                        'options' => array(
                            'separator' => '/',
                        ),
                    ),
                ),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
                'description' => 'name of the container',
                'error_message' => 'name must be set',
            ),
            1 => array(
                'name' => 'description',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\HtmlEntities',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'allow_empty' => true,
                'continue_if_empty' => true,
                'description' => 'Description of the container',
                'error_message' => 'Description must be set',
            ),
            2 => array(
                'name' => 'reposit',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\HtmlEntities',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
                'description' => 'Full path to reposit directory to use. This directory must be existing',
                'error_message' => 'Reposit is not set or directory is not existing',
            ),
            3 => array(
                'name' => 'forseenclosedate',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\HtmlEntities',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
                'description' => 'forseen close date of the container',
                'error_message' => 'forseen close date is not set or his date format is invalid',
            ),
            4 => array(
                'name' => 'project',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
                'description' => 'project is a json data with id or name of the parent project',
                'error_message' => 'project is not set',
            ),
            5 => array(
                'name' => 'process',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => true,
                'continue_if_empty' => true,
                'description' => 'process is a json data with id or name of the default process',
                'error_message' => 'process is not a json data',
            ),
            6 => array(
                'name' => 'extendproperties',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => true,
                'continue_if_empty' => true,
                'description' => 'extendproperties is a json data with extends properties to set for new container',
                'error_message' => 'extendproperties is not a json data',
            ),
            7 => array(
                'name' => 'container_id',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => true,
                'continue_if_empty' => true,
                'description' => '',
                'error_message' => '',
            ),
        ),
        'RbService\\V1\\Rest\\Document\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'description',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'RbService\\V1\\Rest\\Docfile\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'description',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'RbService\\V1\\Rest\\VaultRecord\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'description',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'RbService\\V1\\Rest\\DocfileVersion\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            1 => array(
                'name' => 'docfileId',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            2 => array(
                'name' => 'version',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'RbService\\V1\\Rest\\DocumentVersion\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'documentId',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'version',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
        'RbService\\V1\\Rest\\Checkin\\Validator' => array(
            0 => array(
                'name' => 'files',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'randomize' => true,
                            'target' => './data/',
                        ),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\UploadFile',
                        'options' => array(),
                    ),
                ),
                'description' => 'zip file containing all files to updates',
                'allow_empty' => true,
                'continue_if_empty' => true,
                'type' => 'Zend\\InputFilter\\FileInput',
                'error_message' => '<files> parameter must be a zip file',
            ),
            1 => array(
                'name' => 'document1',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'description' => 'contain json document property. Create any other document2, document3, documentn as you want.',
                'allow_empty' => true,
                'continue_if_empty' => true,
                'error_message' => 'document1 is not set',
            ),
            2 => array(
                'name' => 'updatefile',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringToLower',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'si vraie, met à jour les fichiers du vault à partir des fichiers transmis',
                'error_message' => 'you must set updatefile parameter to 0 or 1',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            3 => array(
                'name' => 'releasing',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Boolean',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'si vrai, libère les documents après la mise à jour',
                'error_message' => 'you must set releasing parameter to 0 or 1',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
        ),
        'RbService\\V1\\Rest\\Checkout\\Validator' => array(
            0 => array(
                'name' => 'getfiles',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => 'If true, return files in respons',
                'error_message' => 'you must set getfiles parameter to 0 or 1',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            1 => array(
                'name' => 'putinws',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => 'If true, put files in the wildspace of the current user',
                'error_message' => 'you must set putinws parameter to 0 or 1',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
            2 => array(
                'name' => 'replace',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'description' => 'If true, replace files if existing in wildspace',
                'error_message' => 'you must set replace parameter to 0 or 1',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
        ),
        'RbService\\V1\\Rpc\\Updownfile\\Validator' => array(
            0 => array(
                'name' => 'zipfile',
                'required' => true,
                'filters' => array(),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\UploadFile',
                        'options' => array(),
                    ),
                ),
                'description' => 'zip file',
            ),
        ),
        'RbService\\V1\\Rest\\Upload\\Validator' => array(
            0 => array(
                'name' => 'zipfile',
                'required' => true,
                'filters' => array(),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\UploadFile',
                        'options' => array(),
                    ),
                ),
                'description' => 'zip file',
                'error_message' => 'not a zip file',
            ),
        ),
        'RbService\\V1\\Rpc\\Download\\Validator' => array(
            0 => array(
                'name' => 'file',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\BaseName',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\HtmlEntities',
                        'options' => array(),
                    ),
                    2 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'name of file to download',
                'error_message' => 'file must be set',
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            '_driver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    0 => __DIR__ . '/../src//Model',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    '\\Model' => '_driver',
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'RbService\\V1\\Rpc\\Download\\Controller' => 'RbService\\V1\\Rpc\\Download\\DownloadControllerFactory',
        ),
    ),
    'zf-rpc' => array(
        'RbService\\V1\\Rpc\\Download\\Controller' => array(
            'service_name' => 'download',
            'http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'route_name' => 'rb-service.rpc.download',
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'RbService\\V1\\Rest\\Container\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\VaultRecord\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\DocfileVersion\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\DocumentVersion\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\FsData\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\Link\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\Checkin\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\Checkout\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rest\\Upload\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'RbService\\V1\\Rpc\\Download\\Controller' => array(
                'actions' => array(
                    'download' => array(
                        'GET' => false,
                        'POST' => false,
                        'PATCH' => false,
                        'PUT' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
        ),
    ),
);
