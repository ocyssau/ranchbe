<?php
return array(
    'RbService\\V1\\Rpc\\Download\\Controller' => array(
        'GET' => array(
            'description' => null,
            'request' => null,
            'response' => '{
   "file": "name of file to download"
}',
        ),
        'POST' => array(
            'description' => null,
            'request' => '{
   "file": "name of file to download"
}',
            'response' => null,
        ),
    ),
);
