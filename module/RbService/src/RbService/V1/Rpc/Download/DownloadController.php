<?php
namespace RbService\V1\Rpc\Download;

use Zend\Mvc\Controller\AbstractActionController;

class DownloadController extends AbstractActionController
{
    public function downloadAction()
    {
        $config = $this->getServiceLocator()->get('Config');
        $downloadBasePath = $config['path']['download'];
        
        $inputFilter = $this->getEvent()->getParam('ZF\ContentValidation\InputFilter');
        if($inputFilter){
            $inputs = $inputFilter->getValues();
            $file=$inputs['file'];
        }
        else{
            $file = $this->params()->fromRoute('file', 'none');
        }
        
        $fullpath = realpath($downloadBasePath . '/' . basename($file));
        
        if(!is_file($fullpath)){
            return "error";
        }
        
        $finfo = new \finfo(FILEINFO_MIME, '/usr/share/misc/magic');
        $mimeType = $finfo->file($fullpath);
        $fileName = basename($fullpath);
        $size=filesize($fullpath);
        
        header("Content-disposition: attachment; filename=$fileName");
        header("Content-Type: " . $mimeType);
        header("Content-Transfer-Encoding: $fileName\n"); //  \n is require
        header("Content-Length: ".($size));
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
        header("Expires: 0");
        readfile($fullpath);
        die;
    }
}
