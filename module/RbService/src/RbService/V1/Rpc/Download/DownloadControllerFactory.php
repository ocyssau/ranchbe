<?php
namespace RbService\V1\Rpc\Download;

class DownloadControllerFactory
{
    public function __invoke($controllers)
    {
        return new DownloadController();
    }
}
