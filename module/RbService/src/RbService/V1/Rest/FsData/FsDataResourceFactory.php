<?php
namespace RbService\V1\Rest\FsData;

class FsDataResourceFactory
{
    public function __invoke($services)
    {
        return new FsDataResource();
    }
}
