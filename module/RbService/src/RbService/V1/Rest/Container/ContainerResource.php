<?php
namespace RbService\V1\Rest\Container;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Rbplm\Org\Workitem as Model;
use Rbs\Org\WorkitemDao as ModelDao;

use Rbplm\Org\Project;
use Rbplm\Wf\Process;
use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Connexion;

class ContainerResource extends AbstractResourceListener
{

    /**
     * Create a resource
     * 
     * @param  mixed $data
     * 
     *            .id
     *            .name
     *            .description
     *            .reposit
     *            .forseenclosedate
     *            .project.id
     *            .project.name
     *            .process.id
     *            .process.name
     *            .extendproperties.property1
     *            .extendproperties.property2
     *            .extendproperties.propertyn
     * 
     * @return ApiProblem|mixed
     */
    public function create($input)
    {
        /*
        $name=$input['name'];
        $description=$input['description'];
        $repositPath=$input['reposit'];
        $forseenCloseDate=$input['forseenclosedate'];
        */
        
        $inputFilter = $this->getInputFilter();
        
        $name=$inputFilter->getValue('name');
        $description=$inputFilter->getValue('description');
        $repositPath=$inputFilter->getValue('reposit');
        $forseenCloseDate=new \Rbplm\Sys\Date($inputFilter->getValue('forseenclosedate'));
        
        try{
            $Model = Model::init($name);
            $Model->description = $description;
            $Model->repositPath = $repositPath;
            $Model->forseenCloseDate = $forseenCloseDate->getTimestamp();
            
            //get project
            $project = json_decode($input->project);
            if(isset($project->id)){
                $Project = new Project();
                DaoFactory::get()->getDao($Project)->loadFromId($Project, $project->id);
                $Model->setParent($Project);
            }
            elseif(isset($project->name)){
                $Project = new Project();
                DaoFactory::get()->getDao($Project)->loadFromName($Project, $project->name);
                $Model->setParent($Project);
            }
            
            //get process
            if(isset($input->process)){
                $process = json_decode($input->process);
                if(isset($process->id)){
                    $Process = new Process();
                    DaoFactory::get()->getDao($Process)->loadFromId($Process, $process->id);
                    $Model->setProcess($Process);
                }
                elseif(isset($process->name)){
                    $Process = new Process();
                    DaoFactory::get()->getDao($Process)->loadFromName($Process, $process->name);
                    $Model->setProcess($Process);
                }
            }
            
            //extends properties
            if(isset($input->extendproperties)){
                $extendProperties = json_decode($input->extendproperties);
                foreach($extendProperties as $pName=>$pValue){
                    $Model->$pName = $pValue;
                }
            }
            
            DaoFactory::get()->getDao($Model)->save($Model);
            return $Model;
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try {
            $Dao = new ModelDao();
            $Model = $Dao->loadFromId(new Model(), $id);
            $Dao->suppress($Model);
            return $Model;
        }
        catch (\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
    
    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        $Dao = new ModelDao();
        foreach($data as $id){
            try {
                $Model = $Dao->load(new Model(), 'id='.$id);
                $Dao->suppress($Model);
            }
            catch (\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
        }
    }
    
    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $Dao = new ModelDao();
        try {
            $Model = $Dao->loadFromId(new Model(), $id);
            return $Model;
        }
        catch (\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
    
    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        try{
            $Dao = new ModelDao();
            $List=$Dao->newList()->load();
            $Collection = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($List->toArray()));
            return $Collection;
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }

    /**
     * PATCH
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        $enable=array(
            'name',
            'description',
            'status',
            'parentId',
            'reposit',
            'processId',
            'forseenCloseDate',
            'aliasId',
        );
        $data=array_intersect($data, $enable);
        
        try{
            $Model = new Model();
            DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);
            foreach($data as $name=>$value){
                $Model->$name=$value;
            }
            DaoFactory::get()->getDao($Model)->save($Model);
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }

    /**
     * PATCH
     * Replace a collection or members of a collection
     * 
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($datas)
    {
        $enable=array(
            'name',
            'description',
            'status',
            'parentId',
            'repositPath',
            'processId',
            'forseenCloseDate',
            'aliasId',
        );
        foreach($datas as $data){
            $id=$data['id'];
            $data=array_intersect($data, $enable);
            try{
                $Model = new Model();
                DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);
                foreach($data as $name=>$value){
                    $Model->$name=$value;
                }
                DaoFactory::get()->getDao($Model)->save($Model);
            }
            catch(\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
        }
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * PUT
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return array('otot');
        
        $enable=array(
            'name',
            'description',
            'status',
            'parentId',
            'repositPath',
            'processId',
            'forseenCloseDate',
            'aliasId',
        );
        $data=array_intersect($data, $enable);
        
        try{
            $Model = new Model();
            DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);
            foreach($data as $name=>$value){
                $Model->$name=$value;
            }
            DaoFactory::get()->getDao($Model)->save($Model);
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
        return $data;
    }
}
