<?php
namespace RbService\V1\Rest\Document;

class DocumentResourceFactory
{
    public function __invoke($services)
    {
        return new DocumentResource();
    }
}
