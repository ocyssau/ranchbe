<?php
namespace RbService\V1\Rest\Link;

class LinkResourceFactory
{
    public function __invoke($services)
    {
        return new LinkResource();
    }
}
