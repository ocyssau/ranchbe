<?php
namespace RbService\V1\Rest\Upload;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UploadResource extends AbstractResourceListener
{

    /**
     * POST
     * Upload a zip file
     *
     * @param  mixed $input
     * @return ApiProblem|mixed
     */
    public function create($input)
    {
        //$config = $this->getEvent()->getApplication()->getServiceManager()->get('Configuration');
        //$uploadBasePath = $config['path']['upload'];
        $uploadBasePath = realpath('./data/upload');

        $data = $this->getInputFilter()->getValues();
        $zipfile = $data['zipfile'];
        if($zipfile['error']==0){
            $zipfilename=uniqid().'.zip';
            if(!copy($zipfile['tmp_name'], $uploadBasePath . '/' . $zipfilename)){
                $zipfile['error']=10000;
                $error=error_get_last();
            }
        }
        return array(
            'filename'=>$zipfilename,
            'type'=>$zipfile['type'],
            'size'=>$zipfile['size'],
            'errorcode'=>$zipfile['error'],
            'errormessage'=>$error['message'],
        );
    }

    /**
     * DELETE
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($file)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');

        $config = $this->getServiceLocator()->get('Config');
        $uploadBasePath = $config['path']['upload'];

        $file=basename($file);
        if(is_file( $uploadBasePath . '/' . $file)){

        }
    }

}
