<?php
namespace RbService\V1\Rest\Upload;

class UploadResourceFactory
{
    public function __invoke($services)
    {
        return new UploadResource();
    }
}
