<?php
namespace RbService\V1\Rest\Checkout;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Proxy\Generic;
use Rbplm\Dao\Connexion;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Document\Link as Doclink;

use Rbplm\Sys;
use Rbplm\Sys\Exception as RbplmException;
use Rbplm\Sys\Error;
use RbService\V1\Rest\Checkout\Exception;

use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;
use Rbplm\Signal;

class CheckoutResource extends AbstractResourceListener
{
    const ERROR_UPLOADFILE_CHECKSUM=10000;
    const ERROR_DOCUMENT_LOAD=10001;
    const ERROR_DOCFILE_LOAD=10002;
    const NOT_CHECKOUTED=10004;
    const USE_BY_OTHER_USER=10005;
    const FILE_NOT_EXISTING=10006;

    const ERROR_CHECKOUT=10007;
    const CHECKOUTED=10008;
    const NOT_FREE=10009;

    /**
     * Checkout a resource
     * POST
     *
     * @param  mixed $data
     *
     * document1.id
     * ou
     * document1.name
     * document1.version
     *
     * @return ApiProblem|mixed
     *
     * return array
     *     document1.[properties]
     *             .docfile1.name
     *             .docfile1.id
     *             .docfile1.version
     *             .docfile1.iteration
     *             .docfile1.description
     *             .docfile1.rule
     *             .docfile1.data.name
     *             .docfile1.data.md5
     *             .docfile1.data.size
     *             .docfile1.data.data
     *
     */
    public function create($input)
    {
        $getfiles=(int)$input->getfiles; //if true return files in a zip
        $putfilesinws=(int)$input->putinws; //if true put files in the wildspace of the user
        $replace=(int)$input->replace; //if true put files in the wildspace of the user
        $Zip=null;

        /*---------------- prepare zip archive -------------------*/
        if ($getfiles){
            //create a ZipArchive
            $Zip = new \ZipArchive();
            $zipFullpath = CurrentUser::get()->getWildspace()->getPath().'/'.uniqid().'.zip';
            $Zip->open($zipFullpath, \ZipArchive::CREATE);
        }

        /*------------- parse the document from input -------------*/
        for($i=2,$index='document1'; isset($input->$index); $index='document'.$i++)
        {
            $doc1 = json_decode($input->$index);

            /*--------load document from db --------------*/
            try {
                $Document = new DocumentVersion();
                //load from id or from name
                if(isset($doc1->id)){
                    $id = $doc1->id;
                    DaoFactory::get()->getDao($Document)->loadFromId($Document, $id);
                }
                elseif(isset($doc1->name)){
                    $name = $doc1->name;
                    DaoFactory::get()->getDao($Document)->loadFromName($Document, $name);
                }
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'index'=>$index,
                    'function'=>'create',
                    'docname'=>$name,
                    'docid'=>$id,
                    'code'=>self::ERROR_DOCUMENT_LOAD,
                    'message'=>'Document can not be load',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                );
                continue;
            }

            /*-------------- Checkout -------------------*/
            try{
                $feed = $this->_checkOutDocument($Document,$putfilesinws,$replace);
                $return['feedback'][]=array(
                    'index'=>$index,
                    'documentid'=>$Document->getUid(),
                    'documentname'=>$Document->getName(),
                    'documentiteration'=>$Document->iteration,
                    'checkoutfeed'=>$feed,
                );
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'index'=>$index,
                    'function'=>'checkout',
                    'docname'=>$Document->getName,
                    'docid'=>$Document->getName,
                    'code'=>self::ERROR_CHECKOUT,
                    'message'=>'Error during document checkout',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                );
                continue;
            }

            /*-------------- put files in zip archive -------------------*/
            foreach($Document->getDocfiles() as $df){
                $fileName=$df->path.'/'.$df->name;
                $return['feedback']['files'][]=array(
                    'filename'=>$fileName,
                    'document'=>array(
                        'id'=>$Document->getUid(),
                        'name'=>$Document->getName(),
                        'iteration'=>$Document->iteration,
                    ),
                    'docfile'=>$df->toArray(),
                );
                if ($getfiles){
                    $Zip->addFile($fileName, $df->name);
                }
            }
        } // End of loop


        /*------------- parse docfile from input ----------------*/
        $Registry=new \ArrayObject();
        for($i=2,$dfindex='docfile1'; isset($input->$dfindex); $dfindex='docfile'.$i++)
        {
            $docfile1 = json_decode($input->$dfindex);

            //load docfile from db
            try {
                $id=null;
                $name=null;
                $Docfile = new DocfileVersion();
                if(isset($docfile1->id) && $docfile1->id){
                    $id = $docfile1->id;
                    DaoFactory::get()->getDao($Docfile)->loadFromId($Docfile, $id);
                }
                elseif( isset($docfile1->name) ){
                    $name = $docfile1->name;
                    DaoFactory::get()->getDao($Docfile)->loadFromName($Docfile, $name);
                }

                //load document
                if( !$Registry->offsetExists($Docfile->parentId) ){
                    $Document = new DocumentVersion();
                    DaoFactory::get()->getDao($Document)->loadFromId($Document, $Docfile->parentId);
                    $Registry->offsetSet($Document->getUid(), $Document);
                }
                else{
                    $Document = $Registry->offsetGet($Docfile->parentId);
                }
                $Document->getDocfiles()->add($Docfile);
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'index'=>$dfindex,
                    'function'=>'create',
                    'docfile'=>$name,
                    'docfileid'=>$id,
                    'code'=>self::ERROR_DOCFILE_LOAD,
                    'message'=>'Docfile can not be load',
                );
                continue;
            }
        } //end of loop for

        /*-------- checkout documents -----------------------*/
        foreach($Registry as $Document){
            try{
                $feed = $this->_checkOutDocument($Document,$putfilesinws,$replace);
                $return['feedback'][]=array(
                    'documentid'=>$Document->getUid(),
                    'documentname'=>$Document->getName(),
                    'documentiteration'=>$Document->iteration,
                    'checkoutfeed'=>$feed,
                );
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'function'=>'checkout',
                    'docname'=>$Document->getName,
                    'docid'=>$Document->getName,
                    'code'=>self::ERROR_CHECKOUT,
                    'message'=>'Error during document checkout',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                );
                continue;
            }
        }

        /*----------------- if zip push datas to client in base64 format ------------------*/
        if ($Zip){
            $Zip->close();
            if(is_file($zipFullpath)){
                $finfo = new \finfo(FILEINFO_MIME, '/usr/share/misc/magic');
                $return['zipfile']['filename']=basename($zipFullpath);
                $return['zipfile']['typemime']=$finfo->file($zipFullpath);
                $return['zipfile']['length']=filesize($zipFullpath);
                $return['zipfile']['md5']=md5_file($zipFullpath);
                $return['zipfile']['data']=\base64_encode(\file_get_contents($zipFullpath));
            }
        }

        return $return;
    }

    /**
     *
     * @param \Rplm\Ged\Document\Version $Document
     * @throws \Rbplm\Sys\Exception
     * @return array
     */
    protected function _checkOutDocument(DocumentVersion $Document, $putfilesinws, $replace)
    {
        $return=array();

        if( $Document->checkAccess() != AccessCode::FREE ){
            throw new Exception('DOCUMENT IS NOT FREE', self::NOT_FREE, $Document->checkAccess() );
        }
        if( $Document->lockById != '' ) {
            throw new Exception('DOCUMENT_IS_USE_BY_OTHER_USER', self::USE_BY_OTHER_USER );
        }

        /*--------------- get list of associated non checkouted files ------------------*/
        //$docfileList = DaoFactory::get()->getDao('Rbplm\Ged\Docfile\Version')->newList()->load('document_id='.$Document->getUid());
        $DfDao = DaoFactory::get()->getDao('Rbplm\Ged\Docfile\Version');
        $Filter = $DfDao->newFilter();
        $Filter->andfind($Document->getUid(), 'parentId', Op::OP_EQUAL);
        $Filter->andfind(0, 'accessCode', Op::OP_EQUAL); //only non checkout docfiles
        $docfileList = $DfDao->newList()->load($Filter);

        if($docfileList->count()==0){
            $return[]='NONE DOCFILES FOUND';
        }

        //convert List in docfile object
        foreach($docfileList as $item)
        {
            $Docfile = new DocfileVersion();
            DaoFactory::get()->getDao($Docfile)->loadFromArray($Docfile, $item);
            $Document->getDocfiles()->add($Docfile); //attach docfile instance to document
            $this->_checkOutDocfile($Docfile, $putfilesinws, $replace, true);
        } //End of foreach

        //lock document
        $Document->lock(AccessCode::CHECKOUT, CurrentUser::get());
        DaoFactory::get()->getDao($Document)->save($Document);

        return $return;
    }

    /**
     * Checkout a docfile and copy data to ws
     *
     * @param \Rbplm\Ged\Docfile $Docfile
     * @param Bool $checkAccess		if true, check if access code is right
     * @throws Exception
     * @return \Rbplm\Ged\Docfile\Version
     */
    protected function _checkOutDocfile($Docfile, $putfilesinws, $replace, $checkAccess=true)
    {
        //Check if the Document is checkOut by me
        if($checkAccess){
            if( $Docfile->checkAccess() != AccessCode::FREE ){
                throw new Exception('NOT_FREE', self::NOT_FREE, $Docfile->checkAccess() );
            }
        }
        Signal::emit($Docfile, Signal::SIGNAL_PRE_CHECKOUT);

        //copy data in wildspace
        if($putfilesinws){
            $Docfile->getData()->getFsData()->copy(CurrentUser::get()->getWildspace()->getPath().'/'.$Docfile->getData()->getName(), 0777, true);
        }

        //lock docfile
        $Docfile->lock(AccessCode::CHECKOUT, CurrentUser::get());

        //save
        DaoFactory::get()->getDao($Docfile)->save($Docfile);

        Signal::emit($Docfile, Signal::SIGNAL_POST_CHECKOUT);

        return $Docfile;
    } //End of method

    /**
     * Return the zip file
     * GET
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($fileName)
    {
        $zipFullpath = CurrentUser::get()->getWildspace()->getPath().'/'.$fileName;
        $this->_pushFile($zipFullpath);
    }

    /**
     *
     * @param unknown_type $file
     * @return boolean
     */
    private function _pushFile($file)
    {
        $fileName = basename($file);
        $finfo = new \finfo(FILEINFO_MIME, '/usr/share/misc/magic');
        $mimeType = $finfo->file($file);
        header("Content-disposition: attachment; filename=$fileName");
        header("Content-Type: " . $mimeType);
        header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
        header("Content-Length: ".filesize($file));
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
        header("Expires: 0");
        readfile($file);
    } //End of method


} //End of class
