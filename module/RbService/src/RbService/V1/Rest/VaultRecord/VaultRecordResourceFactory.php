<?php
namespace RbService\V1\Rest\VaultRecord;

class VaultRecordResourceFactory
{
    public function __invoke($services)
    {
        return new VaultRecordResource();
    }
}
