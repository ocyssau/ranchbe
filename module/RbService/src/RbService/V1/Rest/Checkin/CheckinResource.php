<?php
namespace RbService\V1\Rest\Checkin;

use Zend\Log\Filter\SuppressFilter;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Proxy\Generic;
use Rbplm\Dao\Connexion;

use Rbplm\Org\Workitem;
use Rbplm\Org\Project;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Wf\Process;

use Rbplm\Ged\Category;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Document\Link as Doclink;
use Rbplm\Ged\Document\DoctypeRecognizer;

use Rbplm\Vault\Vault;
use Rbplm\Vault\Reposit;
use Rbplm\Vault\Record;
use Rbplm\Sys\Fsdata;
use Rbplm\Ged\Historize;

use Rbplm\Sys;
use Rbplm\Sys\Exception as RbplmException;
use Rbplm\Sys\Error;
use RbService\V1\Rest\Checkin\Exception;

use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;
use Rbplm\Signal;

class CheckinResource extends AbstractResourceListener
{

	const ERROR_UPLOADFILE_CHECKSUM=10000;
	const ERROR_DOCUMENT_LOAD=10001;
	const ERROR_DOCFILE_LOAD=10002;
	const ERROR_CHECKIN=10003;
	const NOT_CHECKOUTED=10004;
	const CHECKOUTED_BY_OTHER_USER=10005;
	const FILE_NOT_EXISTING=10006;


	/**
	 * Checkin a resource
	 * POST
	 *
	 * @param  mixed $input
	 *
	 * input may be a list of document as document1, document2, etc...
	 * or/and
	 * a list of docfile as docfile1, docfile2, etc...
	 *
	 * .document1.id
	 *           .version
	 *           .name
	 *           .docfile1.name
	 *           .docfile1.id
	 *           .docfile1.data.name
	 *           .docfile1.data.md5
	 *           .docfile1.data.size
	 *           .docfile1.data.data
	 * .docfile1.name
	 *          .id
	 *          .version
	 *          .data.name
	 *          .data.md5
	 *          .data.size
	 *          .data.data
	 * .updatefile
	 * .releasing
	 * .zipdata
	 * .zipfile
	 *
	 * @return ApiProblem|mixed
	 */
	public function create($input)
	{
		$user = new User();
		DaoFactory::get()->getDao($user)->loadFromName($user, 'admin');
		CurrentUser::set($user);
		$wildspace=$user->getWildspace();

		$inputFilter = $this->getInputFilter();
		$updatefile=$inputFilter->getValue('updatefile');
		$releasing=(boolean)$inputFilter->getValue('releasing');

		$return=array();

		/*------------- Get data from miscelanous source ------------------*/

		//update files from request and put in ws
		if($updatefile=='fromrequest'){
			try{
				//unzip files in ws
				$zipfile = $inputFilter->getValue('zipfile');
				$zip = new \ZipArchive();
				$zip->open($zipfile['tmp_name']);
				$zip->extractTo($wildspace->getPath());
				$zip->close();

				//delete tmp files
				unlink($zipfile['tmp_name']);
				$update=true;
			}
			catch(\Exception $e){
				return new ApiProblem(405, 'fromrequest is call but zip file is not reachable' . $zipfile['name']);
			}
		}
		//update files from zipdata base64 encoded
		elseif($updatefile=='fromzipdata'){
			$zipdata = $input->zipdata;
			$zipfileName=$wildspace->getPath().'/'.uniqid().'.zip';
			file_put_contents($zipfileName, base64_decode($zipdata));

			$zip = new \ZipArchive();
			$zip->open($zipfileName);
			$zip->extractTo($wildspace->getPath());
			$zip->close();

			unlink($zipfileName);
			$update=true;
		}
		//update files from wildspace
		elseif($updatefile=='fromws'){
			$update=true;
		}
		elseif($updatefile=='none'){
			$update=false;
		}
		else{
			return new ApiProblem(405, 'I dont understand updatefile parameter value '.$updatefile);
		}

		/*---------parse document in input------------*/
		for($i=2,$docindex='document1'; isset($input->$docindex); $docindex='document'.$i++)
		{
			$doc1 = json_decode($input->$docindex);

			/*-------- check md5 of transmited files -------------*/
			for($j=2,$dfindex='docfile'; isset($input->$dfindex); $dfindex='docfile'.$j++)
			{
				$docfile1 = $doc1->$dfindex;

				$remoteMd5 = $docfile1->data->md5;
				$localMd5 = md5_file($wildspace->getPath().'/'.$docfile1->data->name);
				if($remoteMd5 <> $localMd5){
					$return['error'][]=array(
						'index'=>$docindex.'.'.$dfindex,
						'function'=>'create',
						'filename'=>$docfile1->data->name,
						'code'=>self::ERROR_UPLOADFILE_CHECKSUM,
						'message'=>'Md5 checksum error, retry to upload',
					);
					return $return;
				}
			} //end of loop for

			/*--------load document from db --------------*/
			try {
				$document = new DocumentVersion();
				$id=null;
				$name=null;
				if(isset($doc1->id)){
					$id = $doc1->id;
					DaoFactory::get()->getDao($document)->loadFromId($document, $id);
				}
				elseif(isset($doc1->name)){
					$name = $doc1->name;
					DaoFactory::get()->getDao($document)->loadFromName($document, $name);
				}
			}
			catch (\Exception $e){
				$return['error'][]=array(
					'index'=>$docindex.'.'.$dfindex,
					'function'=>'create',
					'docname'=>$name,
					'docid'=>$id,
					'code'=>self::ERROR_DOCUMENT_LOAD,
					'message'=>'Document can not be load',
				);
				continue;
			}

			/*-------------- Checkin -------------------*/
			try{
				$feed = $this->_checkInDocument($document,$update,$releasing);
				$return['feedback'][]=array(
					'index'=>$docindex,
					'documentid'=>$document->getUid(),
					'documentname'=>$document->getName(),
					'documentiteration'=>$document->iteration,
					'checkinfeed'=>$feed,
				);
			}
			catch (\Exception $e){
				$return['error'][]=array(
					'index'=>$docindex.'.'.$dfindex,
					'function'=>'checkin',
					'docname'=>$document->getName,
					'docid'=>$document->getName,
					'code'=>self::ERROR_CHECKIN,
					'message'=>'Error during document checkin',
					'exception_code'=>$e->getCode(),
					'exception_messsage'=>$e->getMessage(),
				);
				continue;
			}
		} //end of loop for

		/*------------- parse docfile in input ----------------*/
		$registry=new \ArrayObject();
		for($i=2,$dfindex='docfile1'; isset($input->$dfindex); $dfindex='docfile'.$i++)
		{
			$docfile1 = json_decode($input->$dfindex);

			//check md5 of transmited files
			$remoteMd5 = $docfile1->data->md5;
			$localMd5 = md5_file($wildspace->getPath().'/'.$docfile1->data->name);
			if( $remoteMd5<>$localMd5 ){
				$return['error'][]=array(
					'index'=>$dfindex,
					'function'=>'create',
					'filename'=>$docfile1->data->name,
					'code'=>self::ERROR_UPLOADFILE_CHECKSUM,
					'message'=>'Md5 checksum error, retry to upload',
				);
				return $return;
			}

			//load docfile from db
			try {
				$docfile = new DocfileVersion();
				if( isset($docfile1->id) ){
					$id = $docfile1->id;
					DaoFactory::get()->getDao($docfile)->loadFromId($docfile, $id);
				}
				elseif( isset($docfile1->name) ){
					$name = $docfile1->name;
					DaoFactory::get()->getDao($docfile)->loadFromName($docfile, $name);
				}

				//load document
				if( !$registry->offsetExists($docfile->parentId) ){
					$document = new DocumentVersion();
					DaoFactory::get()->getDao($document)->loadFromId($document, $docfile->parentId);
					$registry->offsetSet($document->getUid(), $document);
				}
				else{
					$document = $registry->offsetGet($docfile->parentId);
				}
				$document->getDocfiles()->add($docfile);
			}
			catch (\Exception $e){
				$return['error'][]=array(
					'index'=>$dfindex,
					'function'=>'create',
					'docfile'=>$name,
					'docfileid'=>$id,
					'code'=>self::ERROR_DOCFILE_LOAD,
					'message'=>'Docfile can not be load',
				);
				continue;
			}
		} //end of loop for

		/*-------- checkin documents -----------------------*/
		foreach($registry as $document){
			try{
				$feed = $this->_checkInDocument($document,$update,$releasing);
				$return['feedback'][]=array(
					'documentid'=>$document->getUid(),
					'documentname'=>$document->getName(),
					'documentiteration'=>$document->iteration,
					'checkoutfeed'=>$feed,
				);
			}
			catch (\Exception $e){
				$return['error'][]=array(
					'function'=>'checkout',
					'docname'=>$document->getName,
					'docid'=>$document->getName,
					'code'=>self::ERROR_CHECKIN,
					'message'=>'Error during document checkout',
					'exception_code'=>$e->getCode(),
					'exception_messsage'=>$e->getMessage(),
				);
				continue;
			}
		}

		return $return;
	}

	/**
	 * Replace files from the wildspace to vault reposit directory
	 * and unlock the Document.
	 * return Document or false.
	 *
	 * CheckIn is use after modify a Document. The files are copy from the user directory (the wildspace)
	 * to the protected and share reposit directory (the vault).
	 *
	 * @param Rbplm\Ged\Document\Version $document
	 * @param Bool $update, if true, copy files from wildspace to vault, else ignore updated files
	 * @param Bool $releasing, if true, unlock Document
	 * @return array
	 */
	protected function _checkInDocument(\Rbplm\Ged\Document\Version $document, $update, $releasing)
	{
		$return=array();

		if( $document->checkAccess() != AccessCode::CHECKOUT ){
			throw new Exception('NOT_CHECKOUTED', self::NOT_CHECKOUTED, $document->checkAccess() );
		}
		if( $document->lockById != CurrentUser::get()->getUid() ) {
			throw new Exception('CHECKOUTED_BY_OTHER_USER', self::CHECKOUTED_BY_OTHER_USER );
		}

		/*--------------- get list of associated checkouted files ------------------*/
		$dfDao = DaoFactory::get()->getDao('Rbplm\Ged\Docfile\Version');
		$Filter = $dfDao->newFilter();
		$Filter->andfind($document->getUid(), 'parentId', Op::OP_EQUAL);
		$Filter->andfind(1, 'accessCode', Op::OP_EQUAL); //only checkout docfiles
		//$return[]=$Filter->__toString();
		$docfileList = $dfDao->newList()->load($Filter);

		if($docfileList->count()==0){
			$return[]='NONE CHECKOUTED DOCFILES FOUND';
		}

		//checkIn associated files
		$hasChanged = false;

		//convert List in docfile object
		foreach($docfileList as $item)
		{
			$docfile = new DocfileVersion();
			DaoFactory::get()->getDao($docfile)->loadFromArray($docfile, $item);
			$document->getDocfiles()->add($docfile); //attach docfile instance to document

			$prevIteration=$docfile->iteration;

			$this->_checkInDocfile($docfile, $update, $releasing, true);

			if($docfile->iteration>$prevIteration){
				$hasChange=true;
			}
		} //End of foreach

		//if at least one file has change, increment iteration
		if($hasChange){ //update
			$document->iteration++;
			$document->setUpdateBy(CurrentUser::get());
			$document->updated=time();
		}

		//release document if all his docfile are release
		if($releasing){
			$Filter = $dfDao->newFilter();
			$Filter->andfind($document->getUid(), 'parentId', Op::OP_EQUAL);
			$Filter->andfind(0, 'accessCode', Op::OP_SUP); //only checkout docfiles
			$docfileList = $dfDao->newList()->load($Filter);
			//$return[]=$Filter->__toString();

			//$list=DaoFactory::get()->getDao('\Rbplm\Ged\Docfile\Version')->getListFromParentId((int)$document->getUid(), 'accessCode > 0');

			if($docfileList->count()==0){
				$document->unLock();
			}
			else{
				$return[]= sprintf('CAN NOT CHECKIN DOCUMENT: %0 CHECKOUTED DOCFILES ARE FOUNDED',$docfileList->count());
			}
		}

		DaoFactory::get()->getDao($document)->save($document);

		return $return;
	}//End of method

	/**
	 * Replace a file checkout in the vault and unlock it.
	 *    Return the Rb_Docfile of new iteration or false
	 *
	 * The checkIn copy file from the wildspace to vault reposit dir
	 *   If the file has been changed(check by md5 code comparaison), create a new iteration
	 *
	 * @param Rbplm\Vault\Record  	$docfile The new record to replace
	 * @param Bool $releasing		if true, release the docfile after replace
	 * @param Bool $checkAccess		if true, check if access code is right
	 * @throws Exception
	 * @return \Rbplm\Ged\Docfile\Version
	 */
	protected function _checkInDocfile($docfile, $update, $releasing, $checkAccess=true)
	{
		//Check if the Document is checkOut by me
		if($checkAccess){
			if( $docfile->checkAccess() != AccessCode::CHECKOUT ){
				throw new Exception('NOT_CHECKOUTED', self::NOT_CHECKOUTED, $docfile->checkAccess() );
			}
			if( $docfile->lockById != CurrentUser::get()->getUid() ) {
				throw new Exception('CHECKOUTED_BY_OTHER_USER', self::CHECKOUTED_BY_OTHER_USER );
			}
		}

		Signal::emit($docfile, Signal::SIGNAL_PRE_CHECKIN);

		//if update option is false, just unlock file
		if($update){
			$this->_updateVaultFromWs($docfile);
		}

		if($releasing){
			$docfile->unLock();
		}

		DaoFactory::get()->getDao($docfile)->save($docfile);

		Signal::emit($docfile, Signal::SIGNAL_POST_CHECKIN);

		return $docfile;
	} //End of method

	/**
	 * Update vault file od docfile from version in wilspace.
	 * Wilspace file must have same name that docfile name.
	 *
	 * @param \Rbplm\Ged\Docfile\Version $docfile
	 * @return \Rbplm\Ged\Docfile\Version
	 * @throws Exception
	 */
	protected function _updateVaultFromWs($docfile)
	{
		//initialize vault
		$reposit = \Rbplm\Vault\Reposit::init($docfile->getData()->path);
		$vault = new \Rbplm\Vault\Vault($reposit, DaoFactory::get()->getDao('\Rbplm\Vault\Record'));

		//get file from wildspace
		$file1=CurrentUser::get()->getWildspace()->getPath().'/'.$docfile->getName();

		if(!is_file($file1)){
			throw new Exception('FILE_%0%_IS_NOT_EXISTING', self::FILE_NOT_EXISTING, $file1 );
		}

		//check if file is modified and update vault if necessary
		if($docfile->getData()->md5 != md5_file($file1)){ //file has change, update vault
			//create a iteration copy
			Historize::depriveRecordToIteration($reposit, $docfile->getData(), $docfile->iteration);
			//update vault
			$vault->record($docfile->getData(), $file1, 'file', $docfile->getName());
			$docfile->iteration++;
		}

		return $docfile;
	}

	/**
	 * Fetch all or a subset of resources
	 *
	 * @param  array $params
	 * @return ApiProblem|mixed
	 */
	public function fetchAll($params = array())
	{
		return new ApiProblem(405, 'The GET method has not been defined for collections');
	}

}
