<?php
namespace RbService\V1\Rest\Relation;

class RelationResourceFactory
{
    public function __invoke($services)
    {
        return new RelationResource();
    }
}
