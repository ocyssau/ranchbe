<?php
namespace RbService\V1\Rest\DocfileVersion;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Rbplm\Dao\Factory as DaoFactory;

use Rbplm\Ged\Docfile\Version as Model;
use Rbs\Ged\DocfileDao as ModelDao;

use Rbplm\Ged\Document\Version as DocumentVersion;


class DocfileVersionResource extends AbstractResourceListener
{
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try{
            $Model = Model::init($data['name']);
            $Model->number=$data['name'];
            $Model->description = $data['description'];
            $Model->version = $data['version'];
            $Model->iteration = $data['iteration'];
            
            if($data['document']['id']){
                $Document = new Document();
                DaoFactory::get()->getDao($Document)->loadFromId($Document, $data['document']['id']);
                $Docfile->setParent($Document);
            }
            elseif($data['document']['name']){
                $Document = new Document();
                DaoFactory::get()->getDao($Document)->loadFromName($Document, $data['document']['name']);
                $Docfile->setParent($Document);
            }
            
            DaoFactory::get()->getDao($Model)->save($Model);
            return $Model;
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
    
    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try {
            $Dao = new ModelDao();
            $Model = $Dao->load(new Model(), 'id='.$id);
            $Dao->suppress($Model);
        }
        catch (\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
    
    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        $Dao = new ModelDao();
        foreach($data as $id){
            try {
                $Model = $Dao->load(new Model(), 'id='.$id);
                $Dao->suppress($Model);
            }
            catch (\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
        }
    }
    
    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $Dao = new ModelDao();
        try {
            $Model = $Dao->load(new Docfile(), 'id='.$id);
            return $Model;
        }
        catch (\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
    
    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }
    
    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }
    
    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $enable=array(
            'name',
            'number',
            'description',
            'version',
            'iteration',
        );
        $data=array_intersect($data, $enable);
        
        try{
            $Model = new Model();
            DaoFactory::get()->getDao($Model)->loadFromId($Model, $id);
            foreach($data as $name=>$value){
                $Model->$name=$value;
            }
            DaoFactory::get()->getDao($Model)->save($Model);
        }
        catch(\Exception $e){
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
    }
}
