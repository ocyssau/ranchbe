<?php
namespace RbService\V1\Rest\DocfileVersion;

class DocfileVersionResourceFactory
{
    public function __invoke($services)
    {
        return new DocfileVersionResource();
    }
}
