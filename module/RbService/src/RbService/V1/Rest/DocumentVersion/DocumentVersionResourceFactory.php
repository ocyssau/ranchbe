<?php
namespace RbService\V1\Rest\DocumentVersion;

class DocumentVersionResourceFactory
{
    public function __invoke($services)
    {
        return new DocumentVersionResource();
    }
}
