<?php
namespace RbService\V1\Rest\DocumentVersion;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

use Rbplm\Dao\Factory as DaoFactory;
use Rbplm\Dao\Loader;
use Rbplm\Dao\Connexion;
use Rbplm\People\CurrentUser;

use Rbplm\Ged\Document\Version as DocumentVersion;
use Rbplm\Ged\Docfile\Version as DocfileVersion;
use Rbplm\Ged\Document\Link as Doclink;

use Rbplm\Sys;
use Rbplm\Sys\Exception as RbplmException;
use Rbplm\Sys\Error;
use RbService\V1\Rest\Checkout\Exception;

use Rbplm\Dao\Filter\Op;
use Rbplm\Ged\AccessCode;
use Rbplm\Signal;

class DocumentVersionResource extends AbstractResourceListener
{
    const ERROR_UPLOADFILE_CHECKSUM=10000;
    const ERROR_DOCUMENT_LOAD=10001;
    const ERROR_DOCFILE_LOAD=10002;
    const NOT_CHECKOUTED=10004;
    const USE_BY_OTHER_USER=10005;
    const FILE_NOT_EXISTING=10006;
    
    const ERROR_CHECKOUT=10007;
    const CHECKOUTED=10008;
    const NOT_FREE=10009;
    
    
    /**
     * Create a resource
     *
     * document.name
     *         .version
     *         .iteration
     *         .description
     *         .category.name
     *         .category.id
     *         .container.name
     *         .container.id
     *         .process.id
     *         .process.name
     *         .docfile1.name
     *         .docfile1.id
     *         .docfile1.version
     *         .docfile1.iteration
     *         .docfile1.description
     *         .docfile1.rule
     *         .docfile1.data.name
     *         .docfile1.data.md5
     *         .docfile1.data.size
     *         .docfile1.data.data
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $User = new User();
        DaoFactory::get()->getDao($User)->loadFromName($User, 'admin');
        CurrentUser::set($User);

        /*------------- parse the document from input -------------*/
        for($i=2,$index='document1'; isset($input->$index); $index='document'.$i++)
        {
            $doc1 = json_decode($input->$index);
            
            try{
                $feed = $this->_createOneDocument($doc1);
                $return['feedback'][]=array(
                    'index'=>$index,
                    'documentid'=>$Document->getUid(),
                    'documentname'=>$Document->getName(),
                    'documentiteration'=>$Document->iteration,
                    'checkoutfeed'=>$feed,
                );
            }
            catch (\Exception $e){
                $return['error'][]=array(
                    'index'=>$index,
                    'function'=>'checkout',
                    'docname'=>$Document->getName,
                    'docid'=>$Document->getName,
                    'code'=>self::ERROR_CHECKOUT,
                    'message'=>'Error during document checkout',
                    'exception_code'=>$e->getCode(),
                    'exception_messsage'=>$e->getMessage(),
                );
                continue;
            }
        }

        return $return;
    }

    /**
     *
     * @param stdObject $doc1
     */
    private function _createOneDocument($doc1)
    {
        try {
            $name = $doc1->name;
            $description = $doc1->description;
            $version = $doc1->version;
            $iteration = $doc1->iteration;
            $containerId = $doc1->container->id;
            $categoryId = $doc1->category->id;
            $processId = $doc1->process->id;
            $Workitem = new Workitem();
            DaoFactory::get()->getDao($Workitem)->load($Workitem, $containerId);
        }
        catch (\Exception $e){
            return array(
                'returnCode'=>$e->getCode(),
                'ErrorMessage'=>$e->getMessage(),
            );
        }

        try {
            $Document = new DocumentVersion();
            $DocDao = DaoFactory::get()->getDao($Document);
            $DocDao->loadFromName($Document, $name);
            return array(
                'returnCode'=>1000,
                'ErrorMessage'=>'DOCUMENT_NAME_IS_EXISTING',
            );
        }
        catch (\Exception $e){
            $Document = DocumentVersion::init($name);
            $Document->version = $version;
            $Document->iteration = $iteration;
        }

        $Document->description = $description;
        $Document->categoryId = $categoryId;
        $Document->processId = $processId;
        $Document->setParent($Workitem);

        $Reposit = Reposit::init($Workitem->repositPath);
        //$Reposit = Reposit::init('/tmp/reposit/00-000');

        //Docfiles
        $i=1;
        $index='docfile'.$i;
        //return array('test'=>$doc1->docfile1->name);
        while(isset($doc1->$index)){
            $inDocfile=$doc1->$index;
            $Docfile = DocfileVersion::init($inDocfile->name);
            $Docfile->version = $inDocfile->version;
            $Docfile->iteration = $inDocfile->iteration;
            $Docfile->description = $inDocfile->description;
            $Docfile->rule = $inDocfile->rule;
            $Docfile->setParent($Document);
            if($inDocfile->rule == 0){
                $mainDocfile=$inDocfile;
            }
            //return array('test'=>$Docfile->name);
            try{
                //Put data in Vault
                $data = base64_decode($inDocfile->data->data);
                $Record = Record::init($Docfile->name);
                $toPath = $Reposit->getUrl() .'/'. $Docfile->name;
                file_put_contents($toPath, $data);

                $toFsdata = new Fsdata($toPath);
                $Record->setFsdata($toFsdata);
                $Docfile->setData($Record);
                unset($data);
            }
            catch (Rbplm\Sys\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
            catch (\Exception $e){
                return new ApiProblem($e->getCode(), $e->getMessage());
            }
            $Docfiles[]=$Docfile;

            $i++;
            $index='docfile'.$i;
        }

        $Doctype = new Doctype();
        //DaoFactory::get()->getDao($Doctype)->loadFromName($Doctype, 'text__plain');
        DaoFactory::get()->getDao($Doctype)->loadFromDocument($Doctype, $Document, $mainDocfile->data->extension);
        $Document->setDoctype($Doctype);

        //Save Document
        $DocDao->save($Document);

        //Save Docfiles
        $DfDao = DaoFactory::get()->getDao($Docfile);
        foreach($Docfiles as $Docfile){
            $DfDao->save($Docfile);
        }

        return array(
            'returnCode'=>0,
            'documentId'=>$Document->id,
            'docfile_id'=>$Docfile->id,
            'new_file_in'=>$toPath,
        );
    }


    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            $Document = new DocumentVersion();
            $DocDao = DaoFactory::get()->getDao($Document);
            $DocDao->loadFromId($Document, $id);
        }
        catch (\Exception $e){
        }

        return array(
            'test'=>'ok',
            'test2'=>'ok2'
        );
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
