<?php
namespace RbService;

use ZF\Apigility\Provider\ApigilityProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface as Event;

use Rbplm\Dao\Connexion As Connexion;
use Rbplm\Dao\Factory as DaoFactory;
use Rbs\Dao\Sier\Loader;

use Rbplm\Sys\Exception;
use Rbplm\Sys\Error;
use Rbplm\Sys\Filesystem;
use Rbplm\People\User;
use Rbplm\People\CurrentUser;
use Rbplm\Sys\Trash;

use Rbplm\Rbplm;
use Rbplm\Sys\Logger;

class Module implements ApigilityProviderInterface
{
    public function onBootstrap(Event $e)
    {
        $config = $e->getApplication()->getServiceManager()->get('Configuration');

        Rbplm::setConfig($config['rbplm']);
        Connexion::setConfig($config['rbplm']['db']);
        Loader::setConnexion(Connexion::get());
        User\Wildspace::$basePath = $config['rbplm']['path.wildspace'];
        DaoFactory::setConfig(include(APPLICATION_PATH.'/vendor/Rbs/config/objectdaomap.dist.php'));
        //Rbplm::setLogger(Logger::singleton());
        Filesystem::isSecure(false);
        Trash::init(realpath($config['rbplm']['path.reposit.trash']));

        $User = new User();
        DaoFactory::get()->getDao($User)->loadFromName($User, 'admin');
        CurrentUser::set($User);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                    'Rbplm'=>realpath('./vendor/Rbplm'),
                    'Rbs'=>realpath('./vendor/Rbs')
                ),
            ),
        );
    }
}
