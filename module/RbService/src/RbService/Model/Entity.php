<?php

namespace RbService\Model;

class Entity
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $message;

    /**
     * @var int
     */
    public $timestamp;

    /**
     * @var string
     */
    public $user;
}
