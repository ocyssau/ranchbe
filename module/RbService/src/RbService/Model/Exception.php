<?php 
namespace RbService\Model;

class Exception extends \Exception
{
	function __construct($message, $code=null, $args=null)
	{
		if(is_array($args)){
			foreach($args as $key=>$val){
				$message = str_replace('%'.$key.'%', $val, $message);
			}
		}
		else if(is_string($args)){
			$message = str_replace('%0%', $args, $message);
		}
		parent::__construct($message, $code);
	}
}
