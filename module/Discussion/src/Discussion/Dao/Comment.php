<?php
//%LICENCE_HEADER%
//%LICENCE_HEADER%

namespace Discussion\Dao;

use Rbs\Dao\Sier as DaoSier;


/** SQL_SCRIPT>>
 CREATE TABLE discussion_comment(
 `id` int NOT NULL AUTO_INCREMENT,
 `uid` VARCHAR(255) NOT NULL,
 `cid` CHAR(16) NOT NULL DEFAULT '568be4fc7a0a8',
 `name` VARCHAR(255) NULL DEFAULT NULL,
 `discussionUid` VARCHAR(255) NOT NULL,
 `ownerId` VARCHAR(255) NULL,
 `parentId` int NULL,
 `parentUid` varchar(255) NULL,
 `updated` datetime NOT NULL,
 `body` TEXT NOT NULL,
 PRIMARY KEY (`id`)
 );
 <<*/

/** SQL_ALTER>>
 ALTER TABLE discussion_comment ADD UNIQUE (uid);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UID` (`uid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_DISCUSSIONUID` (`discussionUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_OWNERID` (`ownerId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTID` (`parentId` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_PARENTUID` (`parentUid` ASC);
 ALTER TABLE `discussion_comment` ADD INDEX `DISCUSSION_COMMENT_UPDATED` (`updated` ASC);
 <<*/

/** SQL_INSERT>>
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

class Comment extends DaoSier
{

	/**
	 * @var string
	 */
	public static $table='discussion_comment';
	public static $vtable='discussion_comment';

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'cid'=>'cid',
		'uid'=>'uid',
		'name'=>'name',
		'body'=>'body',
		'discussionUid'=>'discussionUid',
		'ownerId'=>'ownerId',
		'parentId'=>'parentId',
		'parentUid'=>'parentUid',
		'updated'=>'updated',
	);

	/**
	 * Recursive function
	 * @return Dao
	 */
	public function deleteFromId($id, $withChilds=true, $withTrans=null)
	{
		($withTrans===null) ? $withTrans = $this->options['withtrans'] : null;
		if($withTrans) $this->connexion->beginTransaction();

		if(!$this->deleteFromIdStmt){
			$table = $this->_table;
			$sql = 'DELETE FROM '.$table.' WHERE `id`=:id';
			$this->deleteFromIdStmt = $this->connexion->prepare($sql);
			$sql = 'SELECT `id` FROM '.$table.' WHERE `parentId`=:id';
			$this->selectToDeleteFromIdStmt = $this->connexion->prepare($sql);
		}

		try{
			$bind = array(':id'=>$id);
			$this->selectToDeleteFromIdStmt->execute($bind);
			$result = $this->selectToDeleteFromIdStmt->fetchAll(\PDO::FETCH_COLUMN);

			if($withChilds){
				foreach($result as $childId){
					$this->deleteFromId($childId, true, false);
				}
			}

			$this->deleteFromIdStmt->execute($bind);

			if($withTrans) $this->connexion->commit();
		}
		catch(\Exception $e){
			if($withTrans) $this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of method

}

