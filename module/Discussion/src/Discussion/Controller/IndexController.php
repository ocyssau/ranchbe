<?php

namespace Discussion\Controller;

//models and Dao
use Discussion\Model\Comment;
use Rbs\Space\Factory as DaoFactory;
use Rbplm\Dao\Filter\Op;

class IndexController extends \Application\Controller\AbstractController
{

	/**
	 * (non-PHPdoc)
	 * @see Zend\Mvc\Controller.AbstractActionController::indexAction()
	 */
	public function indexAction()
	{
		$factory = DaoFactory::get();
		$view = $this->view;

		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$discussionUid = $request->getQuery('discussionUid');

		$list = $factory->getList(Comment::$classId);
		$filter = $factory->getFilter(Comment::$classId);
		$filter->andFind($discussionUid, 'discussionUid', Op::EQUAL);
		$list->load($filter->__toString());

		$view->list = $list;
		return $view;
	}

	/**
	 *
	 */
	public function addAction()
	{
		$factory = DaoFactory::get();
		$view = $this->view;

		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$request = $this->getRequest();
		$discussionUid = $request->getQuery('discussionUid');
		$parentId = $request->getQuery('parentId');
		$body = $request->getQuery('body');

		//$ownerId = People\CurrentUser::get()->getLogin();
		//$updated = new \DateTime();

		$model = Comment::init(uniqid());
		$model->hydrate(array(
			'discussionUid'=>$discussionUid,
			'parentId'=>$parentId,
			'body'=>$body,
			));

		$dao = $factory->getDao($model->cid);
		$dao->save($model);

		$view->object = $model;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$factory = DaoFactory::get();
		$view = $this->view;

		//check authorization
		$acl = $this->getAcl();
		if(!$acl->hasRight($acl::RIGHT_MANAGE)){
			return $this->notauthorized();
		}

		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$view->setTerminal(true);
		}

		$id = $request->getQuery('commentid');

		$dao = $factory->getDao(Comment::$classId);
		/* @var \PDO $connexion */
		$connexion = $dao->getConnexion();
		$dao->deleteFromId($id);
		return $view;
	}
}

