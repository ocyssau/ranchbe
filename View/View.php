<?php
namespace View;

/**
 * Smarty templating engine
 */
require_once 'smarty/Smarty.class.php';

/**
 * Adapter for smarty view
 *
 * @category   Ranchbe
 * @package    RbView
 * @copyright
 * @license
 */
class View extends \Smarty
{
	/**
	 * Constructor.
	 *
	 * @param array $config Configuration key-value pairs.
	 */
	public function __construct($config = array())
	{
		parent::__construct();
		$this->assign('SCRIPT_NAME', isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : @$GLOBALS['HTTP_SERVER_VARS']['SCRIPT_NAME']);

		//Parametres de compilation
		if (array_key_exists('compile', $config)) {
			$this->compile_dir = $config['compile']['path'];
			$this->force_compile = $config['compile']['force'] ? true : false;
			$this->compile_check = $config['compile']['check'] ? true : false;
		}

		//Parametres de template
		if (array_key_exists('template', $config)) {
			$this->template_dir = $config['template']['path'];
			$this->left_delimiter = $config['template']['leftDelimiter'];
			$this->right_delimiter = $config['template']['rightDelimiter'];
			$this->php_handling = $config['template']['php_handling'];
		}

		//Parametres de cache
		if (array_key_exists('cache', $config)) {
			$this->cache_dir = $config['cache']['path'];
			$this->caching = $config['cache']['enabled'] ? 2 : 0;
			$this->cache_lifetime = (int) $config['cache']['lifetime'];
		}

		//Parametres de config
		if (array_key_exists('config', $config)) {
			$this->config_dir = $config['config']['path'];
			$this->config_overwrite = $config['config']['overwrite'] ? true : false;
			$this->config_booleanize = $config['config']['booleanize'] ? true : false;
			$this->config_read_hidden = $config['config']['read_hidden'] ? true : false;
			$this->config_fix_newlines = $config['config']['fix_newlines'] ? true : false;
		}

		//Parametres de debug
		if (array_key_exists('debug', $config)) {
			$this->debugging = $config['debug']['debug'] ? true : false;
			$this->debug_tpl = $config['debug']['template'];
			$this->debugging_ctrl = $config['debug']['ctrl'];
		}

		//Parametres pour plugins
		if (array_key_exists('plugin', $config)) {
			$this->plugins_dir = $config['plugin']['path'];
		}
	}

	/**
	 * Prevent E_NOTICE for nonexistent values
	 *
	 * @param  string $key
	 * @return null
	 */
	public function __get($name)
	{
		if(!isset($name)) {
			return $this->_tpl_vars;
		}
		if(isset($this->_tpl_vars[$name])) {
			return $this->_tpl_vars[$name];
		}
	}

	/**
	 * Allows testing with empty() and isset() to work inside
	 * templates.
	 *
	 * @param  string $key
	 * @return boolean
	 */
	public function __isset($key)
	{
		return isset ( $this->_tpl_vars[$key] );
	}

	/**
	 * Directly assigns a variable to the view script.
	 *
	 * Checks first to ensure that the caller is not attempting to set a
	 * protected or private member (by checking for a prefixed underscore); if
	 * not, the public member is set; otherwise, an exception is raised.
	 *
	 * @param string $key The variable name.
	 * @param mixed $val The variable value.
	 * @return void
	 * @throws Zend_View_Exception if an attempt to set a private or protected
	 * member is detected
	 */
	public function __set($key, $val)
	{
		$this->_tpl_vars[$key] = $val;
		return $this;
	}

	/**
	 * Allows unset() on object properties to work
	 *
	 * @param string $key
	 * @return void
	 */
	public function __unset($key)
	{
		if (isset($this->_tpl_vars[$key])) {
			unset($this->_tpl_vars[$key]);
		}
	}

	/**
	 * Escapes a value for output in a view script.
	 *
	 * If escaping mechanism is one of htmlspecialchars or htmlentities, uses
	 * {@link $_encoding} setting.
	 *
	 * @param mixed $var The output to escape.
	 * @return mixed The escaped value.
	 */
	public function escape($var)
	{
		if (in_array($this->_escape, array('htmlspecialchars', 'htmlentities'))) {
			return call_user_func($this->_escape, $var, ENT_COMPAT, $this->_encoding);
		}

		return call_user_func($this->_escape, $var);
	}

	/**
	 * Set encoding to use with htmlentities() and htmlspecialchars()
	 *
	 * @param string $encoding
	 * @return Zend_View_Abstract
	 */
	public function setEncoding($encoding)
	{
		$this->_encoding = $encoding;
		return $this;
	}

	/**
	 * Return current escape encoding
	 *
	 * @return string
	 */
	public function getEncoding()
	{
		return $this->_encoding;
	}

	/**
	 * Return current escape encoding
	 *
	 * @return string
	 */
	public function baseUrl($path)
	{
		$baseUrl = trim($this->_tpl_vars['baseurl'], '/');
		$path = trim($path, '/');
		return $baseUrl.'/'.$path;
	}
}


