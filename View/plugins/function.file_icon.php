<?php
/**
 * Ranchbe plugin
 */


/**
 * Ranchbe {file_icon} function plugin
 *
 * Type:     function<br>
 * Name:     file_icon<br>
 * Date:     Mar 30, 2007<br>
 * Purpose:  display icon in accordance to file extension<br>
 * Input:<br>
 *         - extension = extension of file
 *
 * Examples: {file_icon extension=".pdf"}
 * Output:   pdf.gif
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_function_file_icon( $params, $smarty )
{

	if (isset ($params['extension']) ){
		$extension = trim($params['extension'] , ".");
	}
	else return 'no icon';

	$imgBaseUrl = $smarty->get_template_vars('baseImgUrl').'/filetypes';
	$imgBasePath = 'img/filetypes';

	if (!isset ($params['icontype']) ){
		$imgExtension = '.gif';
	}
	else{
		$imgExtension = $params['icontype'];
	}
	
	$iconfile = $imgBasePath .'/'. $extension . $imgExtension;
	
	if ( !is_file($iconfile) ){
		$iconUrl = $imgBaseUrl .'/_default.gif';
	}
	else{
		$iconUrl = $imgBaseUrl .'/'. $extension . $imgExtension;
	}

	return '<img border="0" alt="no icon" src="' . $iconUrl . '" />';
}
