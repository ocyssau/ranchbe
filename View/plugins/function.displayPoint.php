<?php
/**
 * RanchBE Smarty plugin
 *
 * Examples: {displayForm formName="form"}
 */
function smarty_function_displayPoint($params, $smarty)
{
	$point = $params['pt'];
	$html = '<ul class="list-group">
				<li class="list-group-item">'.round($point[0], 4).' mm</li>
				<li class="list-group-item">'.round($point[1], 4).' mm</li>
				<li class="list-group-item">'.round($point[2], 4).' mm</li>
	</ul>';
	echo $html;
} //End of function
