<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

use \Rbs\Space\Factory as DaoFactory;
/**
 *
 */
function smarty_modifier_cidToClassName($cid)
{
	if($cid=='') return null;
	$factory = DaoFactory::get(DaoFactory::$last);
	$class = $factory->getModelClass($cid);
	if($class){
		return $class;
	}
	else{
		return $cid;
	}
}
