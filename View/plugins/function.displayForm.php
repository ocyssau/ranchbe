<?php
/**
 * RanchBE Smarty plugin
 *
 * Examples: {displayForm formName="form"}
 */
function smarty_function_displayForm($params, $smarty)
{
	$formName = $params['formName'];
	if($smarty->$formName){
		return $smarty->$formName->toHtml();
	}
} //End of function
