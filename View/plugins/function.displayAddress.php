<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty plugin
 *
 * Examples: {displaytab}
 */
function smarty_function_displayAddress($params, $smarty)
{
	$addresses = $params['addresses'];
	$result = array();

	if($addresses instanceof Zend\Mail\Address){
		$mail = $addresses->getEmail();
		echo $mail;
	}
	elseif(is_array($addresses)){
		foreach($addresses as $address){
			if($address instanceof Zend\Mail\Address){
				$mail = $address->getEmail();
			}
			elseif(is_string($address)){
				$mail = trim($address, '"');
			}
			$result[] = "<li class=\"list-group-item\">$mail</li>";
		}
	}

	if($result){
		$html = '<ul class="list-group">'.implode('', $result).'</ul>';
	}
	else{
		$html = '<ul class="list-group"></ul>';
	}
	echo $html;
}
