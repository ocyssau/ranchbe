<?php

// Do NOT change this plugin under any circunstances!
function smarty_function_sameurl($params, &$smarty)
{
	$query = array();

	//get params from url
	$redirectQueryStr = $_SERVER['REDIRECT_QUERY_STRING'];
	foreach(explode('&', $redirectQueryStr) as $q){
		$t = explode('=', $q);
		$query[$t[0]]=$t[1];
	}

	//replace params in url by function options
	//get the params from the options past to sameurl function. its the request params to change
	/*
	if(is_array($params)){
		foreach($params as $name=>$val){
			$query[$name] = urlencode($name).'='.urlencode($val);
		}
	}
	*/

	$queryStr = implode('&',$query);
	if($redirectQueryStr){
		$queryStr = $redirectQueryStr.'&'.$queryStr;
	}

	return $queryStr;
}

