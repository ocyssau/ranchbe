<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
$smarty = Ranchbe::getView();

/**
 * Include the {@link shared.make_timestamp.php} plugin
 */
require_once $smarty->_get_plugin_filepath('shared','make_timestamp');
/**
 * Smarty date_format modifier plugin
 *
 * Type:     modifier<br>
 * Name:     date_format<br>
 * Purpose:  format filesize for human<br>
 * Input:<br>
 */
function smarty_modifier_filesize_format($int='0')
{
  if($int == 0) $format = "";
	else if($int <= 1024)				$format = $size." oct";
	else if($int <= (10*1024))			$format = sprintf ("%.2f k%s",($int/1024),"o");
	else if($int <= (100*1024))			$format = sprintf ("%.1f k%s",($int/1024),"o");
	else if($int <= (1024*1024))			$format = sprintf ("%d k%s",($int/1024),"o");
	else if($int <= (10*1024*1024))		$format = sprintf ("%.2f M%s",($int/(1024*1024)),"o");
	else if($int <= (100*1024*1024))		$format = sprintf ("%.1f M%s",($int/(1024*1024)),"o");
	else $format = sprintf ("%d M%s",($int/(1024*1024)),"o");
	return $format;
}

/* vim: set expandtab: */

?>
