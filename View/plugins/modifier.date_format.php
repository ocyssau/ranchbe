<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */
/**
 * Smarty date_format modifier plugin
 *
 * Type:     modifier<br>
 * Name:     date_format<br>
 * Purpose:  format datestamps via strftime<br>
 * Input:<br>
 *         - string: input date string
 *         - format: strftime format for output
 * @return string|void
 */
function smarty_modifier_date_format($timestamp, $format=LONG_DATE_FORMAT)
{
	if( $timestamp != null ){
		$dateTime = new \DateTime();
		$dateTime->setTimeStamp($timestamp);
		return $dateTime->format($format);
	}
	else{
		return;
	}
}
