<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {get_doc_postit} function plugin
 *
 * Type:     function<br>
 * Name:     get_visu<br>
 * Purpose:  return a image from filepath
 * @param array
 * @param Smarty
 * @return string
 * Examples: {get_visu document_id=$value}
 */
function smarty_function_get_visu($params, &$smarty)
{
	$odocument =& $smarty->get_registered_object('odocument');
	$space_name = $odocument->space->SPACE_NAME;

	require_once 'class/common/attachment.php';
	$viewer = new viewer();
	$viewer->init( $odocument );
	if( !empty($params['document_id']) ){
		$html = $viewer->displayVisu( $params['document_id'] );//Display visu of document
		if( $html != false){
			$html .= '
			<a href="{$baseurl}/application/index/viewfile?document_id='.$params['document_id'].'&ticket='.$params['ticket'].'&space='.$space_name.'" title="'.tra('View document').'">
			'.tra('Download visualisation file').'</a>';
			return $html;
		}
		else{
			$html = $viewer->displayPicture( $params['document_id'] );//Display image of document
			if( $html != false){
				$html .= '
				<a href="{$baseurl}/application/index/viewfile?document_id='.$params['document_id'].'&ticket='.$params['ticket'].'&space='.$space_name.'" title="'.tra('View document').'">
				'.tra('Download visualisation file').'</a>';
				return $html;
			}
		}
	}
	return tra('No visualisation file');
} //End of function

