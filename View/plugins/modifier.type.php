<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     type<br>
 * Purpose:  replace type id by the type name<br>
 * Input: $type_id<br>
 */
function smarty_modifier_type($typeId='0')
{
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['type'][$typeId]) ) {
		return $cacheUsualName['type'][$typeId];
	}

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$typeId'";
	$stmt = $conn->query($sql);
	$ret = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['type'][$typeId] = $ret;
	return $ret;
}
