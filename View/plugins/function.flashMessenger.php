<?php
/**
 * RanchBE Smarty plugin
 *
 * Examples: {datepickerDateFormat from="MY FORMAT"}
 */
function smarty_function_flashMessenger($params, $smarty)
{
	$assignTo = $params['assignTo'];
	$return = array();

	$flash = \Rbs\Sys\FlashMessenger::get();
	if( $flash->hasMessage() ){
		$return['messages'] = $flash->getMessages();
	}
	if( $flash->hasError() ){
		$return['errors'] = $flash->getErrors();
	}
	if( $flash->hasSuccess() ){
		$return['success'] = $flash->getSuccess();
	}
	if( $flash->hasWarning() ){
		$return['warnings'] = $flash->getWarnings();
	}
	$flash->clear();
	$smarty->assign($assignTo, $return);
} //End of function
