<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     process<br>
 * Purpose:  replace process id by the process name<br>
 * Input: $process_id<br>
 */
function smarty_modifier_process($processId=0)
{
	return $processId;
}
