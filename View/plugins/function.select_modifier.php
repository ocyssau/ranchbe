<?php
/**
 * Ranchbe plugin
 *


 /**
 * Ranchbe {select_modifier} function plugin
 *
 * Type:     function<br>
 * Name:     select_modifier<br>
 * Date:     Mar 30, 2007<br>
 * Purpose:  transform id in human name from the id and the type<br>
 * Input:<br>
 *         - id = integer
 *         - type = texte
 *
 * Examples: {select_modifier id=5 type=document_indice_id}
 * @author   Olivier Cyssau
 * @version  1.0
 */

function smarty_function_select_modifier( $params ){

	if (!isset ($params['id']) )
		$params['id'] = '';

	if (!isset ($params['type']))
		$params['type'] = '';

	switch ( $params['type'] ) {
		case 'category_id':
			break;
		case 'document_indice_id':
			break;
		case 'default_process_id':
			break;
		case 'doctype_id':
			break;
		case 'update_by':
			break;
		case 'open_by':
			break;
		case 'check_out_by':
			break;
		case 'update_by':
			break;
		default:
	} //End of switch
}
