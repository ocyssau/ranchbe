<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

$smarty_function_get_doc_postit_Factory=null;

/**
 * RanchBE Smarty {get_doc_postit} function plugin
 *
 * Type:     function<br>
 * Name:     get_doc_postit<br>
 * Purpose:  make a pop window if document is commented
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_get_doc_postit($params, &$smarty)
{
	global $smarty_function_get_doc_postit_dao;

	$spaceName = $params['spaceName'];
	$documentId = $params['documentId'];

	if(!isset($smarty_function_get_doc_postit_Factory)){
		$smarty_function_get_doc_postit_dao = \Rbs\Space\Factory::get($spaceName)->getDao(\Rbs\Postit\Postit::$classId);
	}
	$dao = $smarty_function_get_doc_postit_dao;

	$postits = $dao->getFromParent($documentId)->fetchAll();

	if(!$postits){
		return;
	}

	$html .= '<div class="rb-postit">';
	$html .= '<div class="rb-postit-header"><img src="'.$baseUrl.'/img/icons/comment/comment.png" title="postit"  alt="postit" /></div>';

	foreach($postits as $postit ){
		$id = $postit['id'];
		$spaceName = $postit['spaceName'];

		$html .= "<div class=\"rb-postit-item ui-corner-all\" data-id=\"$id\" data-spacename=\"$spaceName\" >";
		$html .= '<i>By '.smarty_modifier_username($postit['ownerId']).'</i><br />';
		$text = iconv('ISO-8859-2', "ISO-8859-1//TRANSLIT", $postit['body'] ); //convert $rawdata in ISO-8859-1
		$text = str_replace("\r\n", '<br />',$text);
		$text = str_replace("\n", '<br />',$text);
		$text = str_replace("\r", '<br />',$text);
		$text = str_replace('\'', '\\\'',$text);
		$html .= $text . '<br />';
		$html .= '<a href="#" class="deletepostit-btn">Delete</a>';
		$html .= '</div>';
	}

	$html .= '</div>';

	return $html;
} //End of function
