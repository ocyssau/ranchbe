<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty username modifier plugin
 *
 * Type:     modifier<br>
 * Name:     container<br>
 * Purpose:  replace container id by the container number<br>
 * Input: container_id<br>
 */
function smarty_modifier_container($containerId)
{
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['container'][$containerId]) ){
		return $cacheUsualName['container'][$containerId];
	}

	$spaceName = Ranchbe::getView()->get_template_vars('spacename');
	$table = $spaceName.'s';
	$nameKey = $spaceName.'_number';
	$idKey = $spaceName.'_id';

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT $nameKey FROM $table WHERE $idKey = '$containerId'";
	$stmt = $conn->query($sql);
	$ret  = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['container'][$containerId] = $ret;
	return $ret;
}
