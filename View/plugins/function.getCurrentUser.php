<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

use Rbplm\People\CurrentUser;

/**
 */
function smarty_function_getCurrentUser($params, $smarty)
{
	isset($params['as']) ? $as = $params['as'] : $as='id';
	switch($as){
		case 'id':
			return CurrentUser::get()->getId();
			break;
		case 'uid':
			return CurrentUser::get()->getUid();
			break;
		case 'name':
			return CurrentUser::get()->getName();
			break;
	}
}
