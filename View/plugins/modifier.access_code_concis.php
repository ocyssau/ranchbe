<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     access_code<br>
 * Purpose:  replace access code by a human text<br>
 * Input: $access_code<br>
 */
function smarty_modifier_access_code_concis($access_code='0')
{
	$smarty = Ranchbe::getView();
	$imgBaseUrl = $smarty->get_template_vars('baseImgUrl');
	switch($access_code){
		case 0:
			break;
		case 1:
		case 5:
		case 10:
		case 11:
		case 15:
			return '<img border="0" src="'.$imgBaseUrl.'/icons/lock.png" />';
			break;
		case 12:
			return '<img border="0" src="'.$imgBaseUrl.'/icons/document/markToSuppress.png" />';
			break;
		default:
			return '<img border="0" src="'.$imgBaseUrl.'/icons/lock.png" />'.$access_code;
			break;
	}

}//End of function

