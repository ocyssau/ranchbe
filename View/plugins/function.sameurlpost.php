<?php

// To convert url get data to post data (in hidden field)!
function smarty_function_sameurlpost($params, $smarty)
{
	$sameurl_elements = $smarty->get_template_vars('sameurl_elements');
	if(!is_array($sameurl_elements)) {
		$sameurl_elements = array();
	}

	//$data = $_SERVER['REQUEST_URI'];
	$first=true;
	$sets=Array();

	foreach($params as $name=>$val) {
		if(isset($_REQUEST[$name])) {
			$_REQUEST[$name]=$val;
		}
		else {
			if(in_array($name,$sameurl_elements)&&!is_array($name)&&!is_array($val)) {
				if(!in_array($name,$sets)) {
					if($first) {
						$first = false;
						$sep='?';
					}
					else {
						$sep='&amp;';
					}
					$data.=$sep.urlencode($name).'='.urlencode($val);
					$sets[]=$name;
				}
			}
		}
	}
	if(count($_REQUEST) > 0){
		foreach($_REQUEST as $name=>$val) {
			if(isset($$name)) {
				$val = $$name;
			}
			if(in_array($name,$sameurl_elements)&&!is_array($name)&&!is_array($val)) {
				if(!in_array($name,$sets)) {
					if($first) {
						$first = false;
						$sep='?';
					}
					else {
						$sep='&amp;';
					}
					$data.='<input type="hidden" name="'.$name.'" value="'.$val.'" />';
					$sets[]=$name;
				}
			}
		}
	}
	print($data);
}
