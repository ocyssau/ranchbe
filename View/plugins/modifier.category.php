<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     category<br>
 * Purpose:  replace category id by the category name<br>
 * Input: $category_id<br>
 */
function smarty_modifier_category($categoryId=0)
{
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['category'][$categoryId]) ) {
		return $cacheUsualName['category'][$categoryId];
	}

	$spaceName = Ranchbe::getView()->get_template_vars('spacename');
	$table = $spaceName.'_categories';

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT category_number FROM $table WHERE category_id = '$categoryId'";
	$stmt = $conn->query($sql);
	$ret  = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['category'][$categoryId] = $ret;
	return $ret;
}
