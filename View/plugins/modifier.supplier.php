<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty username modifier plugin
 *
 * Type:     modifier<br>
 * Name:     supplier<br>
 * Purpose:  replace supplier id by the supplier name<br>
 * Input: $supplier_id<br>
 */
function smarty_modifier_supplier($supplierId='0')
{
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['supplier'][$supplierId]) ) {
		return $cacheUsualName['supplier'][$supplierId];
	}

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT partner_number FROM partners WHERE partner_id = '$supplierId'";
	$stmt = $conn->query($sql);
	$ret = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['supplier'][$supplierId] = $ret;
	return $ret;
}
