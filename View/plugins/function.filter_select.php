<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty plugin
 *
 * Type:     function<br>
 * Name:     filter_select<br>
 * Purpose:  select the correct filter to display normalized name from the id<br>
 * Input: $id of the object, and $type of the object<br>
 * type = [bookshop, cadlib, doctype, workitem, mockup, project, process, partner, nofilter]
 * if type = nofilter return the value of id without translation
 * Examples: {filter_select id=$partner_id type=$type}
 * @author   Olivier Cyssau
 * @version  1.0
 */

function partner($id)
{
	$cacheUsualName =& \Ranchbe::$registry;
	$dbranchbe=Ranchbe::getDb();

	if( isset($cacheUsualName['supplier'][$id]) ) {
		return $cacheUsualName['supplier'][$id];
	}

	if(is_numeric($id)){
		$query = "SELECT partner_number FROM partners WHERE partner_id = '$id'";
		if(!$cacheUsualName['supplier'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['supplier'][$id] = $id;
		}
		else{
			return $cacheUsualName['supplier'][$id];
		}
	}else return $id;
} // End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function container($id){
	global $Manager;
	if(is_numeric($id))
		return $Manager->GetName($id);
	return $id;
} //End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function doctype($id){
	$dbranchbe=Ranchbe::getDb();
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['type'][$id]) ) {
		return $cacheUsualName['type'][$id];
	}

	if(is_numeric($id)){
		$query = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$id'";
		if(!$cacheUsualName['type'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['type'][$id] = $id;
		}else{
			return $cacheUsualName['type'][$id];
		}
	}else return $id;
} //End

/**
 *
 * @param unknown_type $id
 */
function container_indice($id){
	return $id;
} // End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function username($id){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['username'][$id]) ) {
		return $cacheUsualName['username'][$id];
	}

	if(is_numeric($id)){
		$userlib=Ranchbe::getUserLib();
		return $cacheUsualName['username'][$id] = $userlib->get_user_name($id);
	}
	return $id;
} // End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function document_indice($id){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['document_indice'][$id]) ) {
		return $cacheUsualName['document_indice'][$id];
	}

	if(is_numeric($id)){
		$dbranchbe=Ranchbe::getDb();
		$query = "SELECT indice_value FROM document_indice WHERE document_indice_id = '$id'";
		if(!$cacheUsualName['document_indice'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['document_indice'][$id] = $id;
		}else{
			return $cacheUsualName['document_indice'][$id];
		}
	}else return $id;
} // End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function category($id){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['category'][$id]) ) {
		return $cacheUsualName['category'][$id];
	}

	if(is_numeric($id)){
		$dbranchbe=Ranchbe::getDb();
		global $Manager;
		$TABLE = $Manager->SPACE_NAME.'_categories';
		$query = "SELECT category_number FROM $TABLE WHERE category_id = '$id'";
		if(!$cacheUsualName['category'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['category'][$id] = $id;
		}else{
			return $cacheUsualName['category'][$id];
		}
	}else return $id;
} // End

/**
 *
 * @param unknown_type $id
 * @return unknown
 */
function process($id){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['process'][$id]) ) {
		return $cacheUsualName['process'][$id];
	}

	if(is_numeric($id)){
		$dbranchbe=Ranchbe::getDb();
		$query = "SELECT normalized_name FROM galaxia_processes WHERE pId = '$id'";
		if(!$cacheUsualName['process'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['process'][$id] = $id;
		}else{
			return $cacheUsualName['process'][$id];
		}
	}else return $id;
} // End

/**
 *
 * @param unknown_type $id
 */
function project($id){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['project'][$id]) ) {
		return $cacheUsualName['project'][$id];
	}

	if(is_numeric($id)){
		$dbranchbe=Ranchbe::getDb();
		$query = "SELECT project_number FROM projects WHERE project_id = '$id'";
		if(!$cacheUsualName['project'][$id] = $dbranchbe->GetOne($query)){
			return $cacheUsualName['project'][$id] = $id;
		}else{
			return $cacheUsualName['project'][$id];
		}
	}else return $id;
} // End

/**
 *
 */
/**
 *
 * @param unknown_type $params
 * @return string|Ambigous <string, void>|Ambigous <unknown, boolean>|unknown
 */
function smarty_function_filter_select($params)
{

	if (!isset ($params['id']) )
		return '';

	if (!isset($params['type']) && !isset($params['field_name']))
		return '';

	if (!isset($params['type']) && isset($params['field_name'])){
		$params['type'] = $params['field_name'];
	}

	switch ( $params['type'] ) {
		case 'doctype':
		case 'doctype_id':
			return doctype($params['id']);
			break;
		case 'document_indice':
		case 'document_indice_id':
			return document_indice($params['id']);
			break;
		case 'category_id':
		case 'category':
			return category($params['id']);
			break;
		case 'date':
		case (in_array($params['type'],array('cont_open_date','open_date','cont_close_date','close_date','cont_forseen_close_date','forseen_close_date','update_date','check_out_date'))):
			$smarty = Ranchbe::getView();
			require_once $smarty->_get_plugin_filepath('modifier','date_format');
			return smarty_modifier_date_format($params['id']);
			break;
		case 'user':
		case (in_array($params['type'],array('cont_open_by','cont_close_by','close_by','close_by','check_out_by','open_by'))):
			return username($params['id']);
			break;
		case 'container_indice':
		case (in_array($params['type'],array('cont_cont_indice_id','bookshop_indice_id','cadlib_indice_id','workitem_indice_id','mockup_indice_id','project_indice_id'))):
			return container_indice($params['id']);
			break;
		case 'process':
		case (in_array($params['type'],array('cont_default_process_id','default_process_id'))):
			return process($params['id']);
			break;
		case 'container':
		case (in_array($params['type'],array('bookshop_id','mockup_id','cadlib_id','workitem_id','project_id'))):
			return container($params['id']);
			break;
		case 'workitem':
			return container($params['id']);
			break;
		case 'project':
			return project($params['id']);
			break;
		case 'partner':
			return partner($params['id']);
			break;
		case 'mockup':
			return container($params['id']);
			break;
		case 'cadlib':
			return container($params['id']);
			break;
		case 'bookshop':
			return container($params['id']);
			break;
		default:
			return $params['id'];
	} //End of switch types

} //End of function

