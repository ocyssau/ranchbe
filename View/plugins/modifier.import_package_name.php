<?php
/**
 * Ranchbe plugin
 * @package Ranchbe
 */

/**
 * Ranchbe process modifier plugin
 *
 * Purpose:  replace import_order by the package name<br>
 * Input: $import_order<br>
 */
function smarty_modifier_import_package_name($import_order='0'){
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['import_package_name'][$import_order]) ) {
		return $cacheUsualName['import_package_name'][$import_order];
	}

	$dbranchbe=Ranchbe::getDb();
	global $Manager;
	$spaceName = $Manager->SPACE_NAME;
	$query = 'SELECT package_file_name FROM '.$spaceName.'_import_history WHERE import_order = \''.$import_order.'\'';
	if(!$cacheUsualName['import_package_name'][$import_order] = $dbranchbe->GetOne($query))
		return $cacheUsualName['import_package_name'][$import_order] = tra('undefined');
	return $cacheUsualName['import_package_name'][$import_order];
}

?>
