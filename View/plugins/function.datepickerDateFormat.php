<?php
/**
 * RanchBE Smarty plugin
 *
 * Examples: {datepickerDateFormat from="MY FORMAT"}
 */
function smarty_function_datepickerDateFormat($params, $smarty)
{
	$fromFormat = $params['from'];
	$assignTo = $params['assignTo'];

	$dateformat = str_replace('-Y','-yy', $fromFormat);
	$dateformat = str_replace('d-','dd-',$dateformat);
	$dateformat = str_replace('-m','-mm',$dateformat);
	$dateformat = str_replace('%','',$dateformat);

	$smarty->assign($assignTo, $dateformat);
} //End of function
