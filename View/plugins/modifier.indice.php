<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     indice<br>
 * Purpose:  replace indice id by the indice name<br>
 * Input: $indice_id<br>
 */
function smarty_modifier_document_indice($indiceId=0)
{
	$cacheUsualName =& \Ranchbe::$registry;

	if( isset($cacheUsualName['indice'][$indiceId]) ){
		return $cacheUsualName['indice'][$indiceId];
	}

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT `indice_value` FROM `document_indice` WHERE `document_indice_id` = '$indiceId'";
	$stmt = $conn->query($sql);
	$ret  = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['indice'][$indiceId] = $ret;
	return $ret;
}
