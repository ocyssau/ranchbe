<?php
/**
 * Smarty plugin
 * @package Ranchbe
 */

/**
 * Smarty username modifier plugin
 *
 * Type:     modifier<br>
 * Name:     project<br>
 * Purpose:  replace project id by the project number<br>
 * Input: project_id<br>
 */
function smarty_modifier_project($projectId='0')
{
	$cacheUsualName =& \Ranchbe::$registry;
	if( isset($cacheUsualName['project'][$projectId]) ){
		return $cacheUsualName['project'][$projectId];
	}

	$conn = \Rbplm\Dao\Connexion::get();
	$sql = "SELECT project_number FROM projects WHERE project_id = '$projectId'";
	$stmt = $conn->query($sql);
	$ret  = $stmt->fetchColumn(0);

	if(!$ret){
		$ret = tra('undefined');
	}

	$cacheUsualName['project'][$projectId] = $ret;
	return $ret;
}
