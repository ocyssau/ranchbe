<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty plugin
 *
 * Examples: {displaytab}
 */
function smarty_function_displayTabs($params, $smarty)
{
	if($smarty->tabs){
		echo $smarty->tabs->toHtml();
	}
} //End of function
