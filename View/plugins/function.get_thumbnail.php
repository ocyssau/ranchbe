<?php
/**
 * Smarty plugin
 * @package RanchBE
 * @subpackage plugins
 */

/**
 * RanchBE Smarty {get_doc_postit} function plugin
 *
 * Type:     function<br>
 * Name:     get_thumbnail<br>
 * Purpose:  return a image from filepath
 * @param array
 * @param Smarty
 * @return string
 * Examples: {get_thumbnail imagefile_path=$value}
 */
function smarty_function_get_thumbnail($params, $smarty)
{
	$documentId = $params['document_id'];
	
	$imgBaseUrl = $smarty->get_template_vars('baseCustomImgUrl').'/thumbnails';
	$imgBasePath = 'data/img/thumbnails';
	$imgExtension = '.gif';
	$thumbfile = $imgBasePath .'/'. $documentId . $imgExtension;
	if ( is_file($thumbfile) ){
		$thumbUrl = $imgBaseUrl .'/'. $documentId . $imgExtension;
		return '<img border="0" alt="no thumbs" src="' . $thumbUrl . '" />';
	}
	else{
		return '<!--no thumbs-->';
	}
} //End of function

/* vim: set expandtab: */
