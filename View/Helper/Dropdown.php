<?php
namespace View\Helper;

class Dropdown extends Tab
{
	public $links;
	public $glyphicon = 'glyphicon-user';

	/**
	 *
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 * @param TabBar $father
	 */
	public function __construct($tabname, $links)
	{
		parent::__construct($tabname);
		$this->links = $links;
	}

	/**
	 *
	 */
	public function activate($name)
	{
		$this->active = $name;
		return parent::activate();
	}

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		if($this->isActive()){
			$class = 'active';
		}

		$html = "<li class=\"dropdown $class\">";
		$html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
		$html .= $this->label;
		$html .= '<span class="glyphicon '.$this->glyphicon.'"></span>';
		$html .= '<span class="caret"></span></a>';
		$html .= '<ul class="dropdown-menu" role="menu">';
		foreach($this->links as $name=>$link){
			($name==$this->active) ? $class = 'active' : $class = null;
			$html .= "<li class=\"$class\">$link</li>";
		}
		$html .= '</ul></li>';
		return $html;
	} //End of method

} //End of class
