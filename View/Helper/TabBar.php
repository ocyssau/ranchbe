<?php
namespace View\Helper;

class TabBar extends Tab
{
	protected $tabs=array(); //array of tab objects

	/**
	 * @param string
	 * @return Tab
	 */
	public function getTab($name)
	{
		return $this->tabs[$name];
	} //End of method

	/**
	 * @return array
	 */
	public function getTabs()
	{
		return $this->tabs;
	} //End of method


	/**
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 */
	public function addTab($tab)
	{
		$this->tabs[$tab->getName()] = $tab;
		$tab->setParent($this);
		return $this;
	} //End of method

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		$html = '<div id="navbar" class="collapse navbar-collapse">';
		$html = '<ul class="rb-mainnavtabpanel rb-navtabpanel tabnav nav navbar-nav">';
		foreach($this->tabs as $tab){
			if( $tab instanceof TabBar ){
				$html .= $tab->renderAsTab();
				if($tab->isActive()){
					$nextTabs = $tab;
				}
			}
			else{
				$html .= $tab->render();
			}
		}
		$html .= '</ul></div>';
		if($nextTabs){
			$html .= $nextTabs->render();
		}
		return $html;
	} //End of method

	/**
	 *
	 * @return string
	 */
	public function renderAsTab()
	{
		return parent::render();
	}



} //End of class
