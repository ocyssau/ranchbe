<?php
namespace View\Helper;

/**
 * OBSOLETE
 * Generate a excel file from a adodb recordset
 *
*/
class excelRender
{
	public $beginLine;
	public $beginCol;
	public $rs; //recordset
	public $workbook; //Spreadsheet_Excel_Writer
	public $worksheet; //
	public $title; //

	/**
	 *
	 * @param unknown_type $rs
	 * @param unknown_type $title
	 */
	function __construct(&$rs, $title)
	{
		$this->rs = $rs;
		$this->beginLine = 0;
		$this->beginCol = 0;
		$this->title = $title;
		require_once "Spreadsheet/Excel/Writer.php";

		// Creating a workbook
		$this->workbook = new Spreadsheet_Excel_Writer();

		// Creating a worksheet
		$this->worksheet =& $this->workbook->addWorksheet($this->title.'_sheet');
	} //End of method

	/*
	 *
	*
	*
	*/
	function render()
	{
		$format =& excel_render($this);

		// sending HTTP headers
		$this->workbook->send($this->title.'_content.xls');

		//Write title line
		$ncols = $this->rs->FieldCount();
		$col = $this->beginCol;

		for ($i=0; $i < $ncols; $i++) {
			$field = $this->rs->FetchField($i); //Returns an object containing the name, type and max_length of the associated field
			if ($field) {
				$fname = htmlspecialchars($field->name);
			}
			else {
				$fname = 'Field '.($i+1);
				//$typearr[$i] = 'C';
			}
			$this->worksheet->write($this->begin_line, $col, $fname, $format['title']);
			$col++;
		}

		//Write body
		$lin = $this->begin_line + 1;
		while ($rowInArray = $this->rs->FetchRow()) { //Returns array containing current row, or false if EOF
			$col = $this->begin_col;
			foreach($rowInArray as $val){
				$this->worksheet->write($lin, $col, $val,$format['body']);
				$col++;
			}
			$lin++;
		}

		// Let's send the file
		$this->workbook->close();
	} //End of method

} //End of class

