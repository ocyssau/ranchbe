<?php
namespace View\Helper;

use Rbs\Sys\Session;
use Rbplm\People\CurrentUser;

/**
 * Factory for build main tab bar of application
 *
 * @author olivier
 *
 */
class MainTabBar
{

	protected static $_instance;

	protected $view;

	protected $tabBar;

	protected $tabs;

	/**
	 *
	 * @param \View\View $view
	 */
	protected function __construct($view)
	{
		$this->view = $view;
		$this->tabBar = new TabBar('mainTabBar');
		$view->tabs = $this;

		$this->tabs = array(
			'myspace' => array(
				'url' => $view->baseUrl('/workplace/wildspace/index'),
				'name' => 'myspace',
				'label' => 'My space'
			),
			'admin' => array(
				'url' => $view->baseUrl('/application/admin/display'),
				'name' => 'admin',
				'label' => 'Admin'
			)
		);
	}

	/**
	 * $config = array(
	 * 'mockup'=>true,
	 * 'cadlib'=>true,
	 * 'bookshop'=>true,
	 * 'partner'=>true,
	 * 'admin'=>true,
	 * )
	 *
	 * @return MainTabBar
	 */
	public function init($config)
	{
		$this->tabBar->addTab(new Tab('home', $this->view->baseUrl('/application/home/home'), tra('Welcome')));
		$this->tabBar->addTab(new Tab('project', $this->view->baseUrl('/project/index/display'), tra('Projects')));
		$this->tabBar->addTab(new Tab('workitem', $this->view->baseUrl('/ged/workitem/index'), tra('WorkItems')));
		$this->tabBar->addTab($this->getMyspaceMenu());

		if ( $config['module.product'] ) {
			$this->tabBar->addTab(new Tab('product', $this->view->baseUrl('/pdm/productversion'), tra('Products')));
		}

		if ( $config['module.mockup'] ) {
			$this->tabBar->addTab(new Tab('mockup', $this->view->baseUrl('/ged/mockup/index'), tra('Mockups')));
		}

		if ( $config['module.cadlib'] ) {
			$this->tabBar->addTab(new Tab('cadlib', $this->view->baseUrl('/ged/cadlib/index'), tra('Cadlibs')));
		}

		if ( $config['module.bookshop'] ) {
			$this->tabBar->addTab(new Tab('bookshop', $this->view->baseUrl('/ged/bookshop/index'), tra('Bookshops')));
		}

		if ( $config['module.partner'] ) {
			$this->tabBar->addTab(new Tab('partner', $this->view->baseUrl('/partner/index/display'), tra('Partners')));
		}

		if ( $config['module.admin'] ) {
			$this->tabBar->addTab($this->getAdminMenu());
		}

		$this->tabBar->addTab($this->getUserMenu());
		$this->tabBar->addTab($this->getApplicationMenu());

		return $this;
	}

	/**
	 */
	public function getUserMenu()
	{
		$baseurl = BASEURL;
		$currentUser = CurrentUser::get()->getLogin();
		$dropdown = new Dropdown('User', array(
			'user'=>'<a href="#">Connected as ' . $currentUser . '</a>',
			'logout'=>'<a href="' . $baseurl . '/auth/logout">Logout</a>',
			'preferences'=>'<a href="' . $baseurl . '/user/preference/editme">Preferences</a>',
			'context'=>'<a href="' . $baseurl . '/session/manager/index">Context</a>'
		));
		$dropdown->glyphicon = 'glyphicon-user';
		$dropdown->setLabel($currentUser);
		return $dropdown;
	}

	/**
	 */
	public function getApplicationMenu()
	{
		$dropdown = new Dropdown('Ranchbe', array(
			'version'=>'<a href="#">' . \Ranchbe::getVersion() . '</a>',
			'website'=>'<a href="'.\Ranchbe::WEBSITE.'" title="RanchBE">Site Web</a>',
			'about'=>'<a href="' . $baseurl . '/rbapplication/index/about">About Ranchbe</a>',
			'help'=>'<a href="' . $baseurl . '/help">Help</a>',
			'apidoc'=>'<a href="' . $baseurl . '/apidoc/index.html">Api Documentation</a>'
		));
		$dropdown->glyphicon = 'glyphicon-info-sign';
		return $dropdown;
	}

	/**
	 */
	public function getMyspaceMenu($name = null)
	{
		$mySpace = Tab::get('myspace');

		$context = Session::get();
		$containerId = $context->containerId;
		$spaceName = $context->spaceName;

		$links = array();

		if ( $containerId ) {
			$containerUrl = $this->view->baseUrl("/rbdocument/manager/index?containerid=$containerId&space=$spaceName");
			$links[container] = '<a href="' . $containerUrl . '">' . $context->containerNumber . '</a>';
		}

		$wsUrl = $this->view->baseUrl('/workplace/wildspace/index');
		$links['wildspace'] = '<a href="' . $wsUrl . '" class=\"%anchor%\">' . tra('Wildspace') . '</a>';

		if ( $_SESSION['DisplayRecordfile'] ) {
			$url = $this->view->baseUrl('/recordfile/manager/display');
			$name = $context->containerNumber . '_' . tra('Files');
			$links['container_files'] = "<a href=\"$url\" class=\"%anchor%\">$name</a>";
		}
		elseif ( $_SESSION['DisplayDocfile'] ) {
			$url = $this->view->baseUrl('/docfile/manager/display');
			$name = $context->containerNumber . '_' . tra('Files');
			$links['container_files'] = "<a href=\"$url\" class=\"%anchor%\">$name</a>";
		}

		$url = $this->view->baseUrl('/custom/index/index');
		$links['custom'] = '<a href="' . $url . '" class=\"%anchor%\">' . tra('Custom') . '</a>';

		$dropdown = new Dropdown('myspace', $links);
		$dropdown->glyphicon = '';
		$dropdown->setLabel('MySpace');
		return $dropdown;
	}

	/**
	 * Singleton
	 *
	 * @param array $config
	 */
	public static function get($view = null)
	{
		if ( !$view ) {
			$view = \Ranchbe::getView();
		}

		if ( !self::$_instance ) {
			$config = \Ranchbe::get()->getConfig();
			self::$_instance = new self($view);
			self::$_instance->init($config);
		}
		return self::$_instance;
	}

	/**
	 *
	 * @return MainTabBar
	 */
	public function getUrl($tabName)
	{
		if ( isset($_SESSION['tabs'][$tabName]['url']) ) {
			return $_SESSION['tabs'][$tabName]['url'];
		}
		else {
			return $this->tabs[$tabName]['url'];
		}
	}

	/**
	 *
	 * @param
	 *        	string
	 * @return Tab
	 */
	public static function getTab($name)
	{
		return Tab::get($name);
	}

	/**
	 *
	 * @return MainTabBar
	 */
	public function getConsult()
	{
		$context = \Ranchbe::getContext();
		$myConsult = & Tab::get('consult');
		$containerId = $context->getProperty('container_id');
		// generate list of container
		if ( $context->getProperty('links') ) foreach( $context->getProperty('links') as $class_id => $objects ) {
			if ( !($class_id == 20 || $class_id == 21 || $class_id == 22 || $class_id == 23) ) continue;
			if ( is_array($objects) ) foreach( $objects as $object ) {
				switch ($class_id) {
					case 20:
						$name = 'bookshop_number';
						$id = 'bookshop_id';
						$space_name = 'bookshop';
						break;
					case 21:
						$name = 'cadlib_number';
						$id = 'cadlib_id';
						$space_name = 'cadlib';
						break;
					case 22:
						$name = 'mockup_number';
						$id = 'mockup_id';
						$space_name = 'mockup';
						break;
					case 23:
						$name = 'workitem_number';
						$id = 'workitem_id';
						$space_name = 'workitem';
						break;
				}
				$myConsult->addTab($object[$name] . 'Tab', $this->view->baseUrl('/context/consult/consult/space/') . $space_name . '/container_id/' . $object[$id], $object[$name]);
			}
		} // End of foreach
		return $this;
	}

	/**
	 *
	 * @return MainTabBar
	 */
	public function getAdminMenu()
	{
		$dropdown = new Dropdown('Admin', array(
			'doctype'=>'<a href="' . $baseurl . '/ged/doctype/index">' . tra('Doctypes') . '</a>',
			'workflow'=>'<a href="' . $baseurl . '/workflow/process/index">' . tra('Workflow') . '</a>',
			'user'=>'<a href="' . $baseurl . '/user/useradmin/display">' . tra('Users') . '</a>',
			'role'=>'<a href="' . $baseurl . '/user/roleadmin/display?resourceid=0&areaid=1">' . tra('Roles For Application') . '</a>',
			'tools'=>'<a href="' . $baseurl . '/tools/index/display">' . tra('Tools') . '</a>'
		));
		$dropdown->setLabel('Admin');
		$dropdown->glyphicon = 'glyphicon glyphicon-cog';
		return $dropdown;
	}

	/**
	 */
	public function toHtml()
	{
		return $this->tabBar->render();
	}
} //End of class
