<?php
namespace View\Helper;

class Tab
{
	protected $name; //name of tab
	protected $url; //url of link
	protected $label; //displayed name
	protected $isActive = false;
	protected $parent;
	protected $level = 0; //level 0 for main bar and +1 for each sub-bar
	protected static $_instances = array (); //registry of tabs

	/**
	 *
	 * @param string $tabname
	 * @param string $url
	 * @param string $label
	 * @param TabBar $father
	 */
	public function __construct($tabname, $url=null, $label=null, TabBar $parent = null)
	{
		$this->name = $tabname;
		$this->url = $url;
		$this->label = $label;
		$this->parent = $parent;
		self::$_instances [$this->name] =& $this;
	}

	/**
	 * @param string $tabname
	 */
	public static function get($name)
	{
		if (self::$_instances[$name])
			return self::$_instances[$name];
		else
			return false;
	}

	/**
	 *
	 */
	public function activate()
	{
		if ($this->parent) {
			$this->parent->setUrl( $this->url );
			$this->parent->activate();
		}
		$this->isActive = true;
		return $this;
	}

	/**
	 *
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 *
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 *
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 *
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @return string
	 */
	public function setLabel($label)
	{
		$this->label = $label;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isActive()
	{
		return $this->isActive;
	}

	/**
	 *
	 * @return string
	 */
	public function render()
	{
		$html = '';
		$class = '';
		if($this->isActive()){
			$class = ' active';
		}
		$html .= '<li class="rb-navtab '.$class.'"><a href="'.$this->getUrl().'">'.$this->getLabel().'</a></li>';
		return $html;
	} //End of method
} //End of class
