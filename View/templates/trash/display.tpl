{*Smarty template*}

{include file='layouts/htmlheader.tpl'}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<h1 class="pagetitle">{tr}Files in trash{/tr}</h1>

{*--------------------list header----------------------------------*}
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto"></th>
	<th class="heading">{tr}original_file{/tr}</th>
	<th class="heading">{tr}file_name{/tr}</th>
	<th class="heading">{tr}file_path{/tr}</th>
	<th class="heading">{tr}file_size{/tr}</th>
	<th class="heading">{tr}file_mtime{/tr}</th>
</tr></thead>

{*--------------------list body---------------------------*}
<tbody>
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
	<td class="thin">{$list[list].original_file}</td>
	<td class="thin">{$list[list].name}</td>
	<td class="thin">{$list[list].path}</td>
	<td class="thin">{$list[list].size|filesize_format}</td>
	<td class="thin">{$list[list].mtime|date_format}</td>
</tr>
{/section}
</tbody>
</table>
</form>
