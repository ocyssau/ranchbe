{*Smarty template*}

{$form.javascript}

<h1>Success of importation: </h1>
{$body}

{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$baseurl}/document/manager/index">

<table class="normal table table-bordered">
	<tr>
		{foreach item=field_name from=$fields}
		<th class="heading auto">{$field_name}</th>
		{/foreach}
	</tr>
	{*--------------------list Body----------------------------------*}
	{cycle print=false values="even,odd"}
	{section name=list loop=$list}
		<tr class="{cycle}">
			{foreach item=field_name from=$fields}
				<td>{$list[list].$field_name}</td>
			{/foreach}
		</tr>
	{/section}
</table>

<input type="hidden" name="xmlfile" value="{$xmlfile}" />
<input type="submit" value="Exit" name="action" />
<input type="hidden" name="page_id" value="documentImportRbXml" />

</form>

