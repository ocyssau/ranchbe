{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

{$form.javascript}

<h1 class="pagetitle">{tr}Batch jobs{/tr}</h1>
<form name="close"><input type="button" onclick="window.close()" value="Close"></form>

<h2>
Free space on mockups directories: {$freeMockupDiskSpace}<br/>
Free space on import directories: {$freeImportDiskSpace}<br/>
</h2>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}
<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto"></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=id">
   {tr}Job Id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=name">
   {tr}Name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=runner">
   {tr}Runner{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=submit">
   {tr}Submit{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=submit_by">
   {tr}Submit by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=planned">
   {tr}Planned{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=state">
   {tr}State{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=start">
   {tr}Start{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=end">
   {tr}End{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=duration">
   {tr}Duration{/tr}</a></th>
  <th class="heading"></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    {*-Specifics fields-*}
    <td class="thin"><a href="{$smarty.server.PHP_SELF}?action=detail&checked={$list[list].id}">{$list[list].id}</a></td>
    <td class="thin">{$list[list].name}</td>
    <td class="thin">{$list[list].runner}</td>
    <td class="thin">{$list[list].submit|date_format}</td>
    <td class="thin">{$list[list].submit_by|username}</td>
    <td class="thin">{$list[list].planned|date_format}</td>
    <td class="thin">{$list[list].state}</td>
    <td class="thin">{$list[list].start|date_format}</td>
    <td class="thin">{$list[list].end|date_format}</td>
    <td class="thin">{$list[list].duration}</td>
    <td class="thin"><a href="{$smarty.server.PHP_SELF}?action=detail&checked={$list[list].id}">See details</a></td>
   </tr>
  {/section}
  </table>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this job{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="reinit" title="{tr}ReInit{/tr}" 
 onclick="pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/flag_green.png" title="{tr}ReInit{/tr}" alt="{tr}ReInit{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="seelog" title="{tr}See log{/tr}" 
 onclick="pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/flag_blue.png" title="{tr}See logt{/tr}" alt="{tr}See log{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
</form>


