{*Smarty template*}

{literal}
<style>
.input{
	padding-top:20px;
}
</style>

<script>
$(function() {
	setDisplayFromType("text");
	
	var selectType = $("[name='type']");
	selectType.change(function(){
		setDisplayFromType($(this).val());
	});
});

	/**
	 * 
	 */
	function setDisplayFromType(type)
	{
		$(".specific").hide();
		switch(type) {
			case "select":
				$(".select").show();
				break;
			case "selectFromDB":
				$(".selectFromDB").show();
				break;
			case "partner":
			case "doctype":
			case "user":
			case "category":
			case "version":
				$(".selectFromDbPredefined").show();
				break;
			case "text":
			case "longtext":
			case "htmlarea":
				$(".text").show();
				break;
			case "decimal":
			case "integer":
				$(".numeric").show();
				break;
			case "date":
				$(".date").show();
				break;
		}
	}
</script>
{/literal}

{$form.javascript}

<h1 class="pagetitle">{$pageTitle}</h1>

{*Create form*}
<form {$form.attributes}>
{$form.hidden}

<fieldset class="common">
	{if $form.extendedcid}
	<div class="input">
	<b>{$form.extendedcid.label}:</b>
	{$form.extendedcid.html}
	</div>
	{/if}

	{if $form.extendedcid}
	<div class="input">
	<b>{$form.type.label}:</b>
	{$form.type.html}
	</div>
	{/if}
	
	<div class="input">
	<b>{$form.appName.label}:</b>
	{$form.appName.html}
	</div>
	
	<div class="input">
	<b>{$form.description.label}:</b>
	{$form.description.html}
	</div>
	
	<div class="input">
	<b>{$form.label.label}:</b>
	{$form.label.html}
	</div>
	
	<div class="input">
	<b>{$form.required.label}:</b>
	{$form.required.html}
	<i>Require the information</i>
	</div>
</fieldset>

<fieldset class="specific text selectFromDB selectFromDbPredefined select">
	<div class="input">
	<b>{$form.size.label}:</b>
	<i>Max size of the field or number of line to display for select field</i>
	{$form.size.html}
	</div>
</fieldset>

 <fieldset class="specific text">
	<div class="input">
	<b>{$form.regex.label}:</b>
	<i>You can input here a regular expression to check input information</i>
	{$form.regex.html}
	<a href="javascript:popup('{$baseurl}/testRegExpr.htm','testRegExpr')">{tr}Need help?{/tr}</a></p>
	</div>
</fieldset>

<fieldset class="specific select">
	<div class="input">
	<b>{$form.multiple.label}:</b>
	{$form.multiple.html}
	<i>Enable the multi selection with select field.</i>
	</div>

	<div class="input">
	<b>{$form.return.label}:</b>
	{$form.return.html}
	<i>Return the normalized name or return id</i>
	</div>

	<div class="input">
	<b>{$form.list.label}:</b>
	<i>You must separate each word/sentence by '#' without space.</i>
	{$form.list.html}
	</div>
</fieldset>

<fieldset class="specific selectFromDB">
	<div class="input">
	<b>{$form.dbtable.label}:</b>
	{$form.dbtable.html}
	<i>Name of the table from where to gets datas.</i>
	</div>

	<div class="input">
	<b>{$form.forValue.label}:</b>
	{$form.forValue.html}
	<i>Name of the field used for set the value return by the select</i>
	</div>

	<div class="input">
	<b>{$form.forDisplay.label}:</b>
	{$form.forDisplay.html}
	<i>Name of the field used for set the text to display</i>
	</div>
</fieldset>

<fieldset class="specific date">
	<div class="input">
	<b>{$form.dateFormat.label}:</b>
	{$form.dateFormat.html}
	<i>Format of date. i.e: %Y/%m/%d display 2005/05/28 i.e</i>
	</div>
</fieldset>

<fieldset class="specific numeric">
	<div class="input">
	<b>{$form.min.label}:</b>
	{$form.min.html}
	</div>

	<div class="input">
	<b>{$form.max.label}:</b>
	{$form.max.html}
	</div>

	<div class="input">
	<b>{$form.step.label}:</b>
	{$form.step.html}
	</div>
</fieldset>

{$form.requirednote}

<div class="validate">
{if not $form.frozen}
	{$form.cancel.html}&nbsp;{$form.validate.html}
{/if}
</div>

</form>

{if $form.errors}
<p>
	<b>Collected Errors:</b>
	{foreach key=name item=error from=$form.errors}
		<font color="red">{$error}</font> in element [{$name}]
	{/foreach}
</p>
{/if}

