{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
	$.each($('.optionSelector:checked'), function( index, item ) {
		displayOption(item,0);
	});	
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form id="filterf" action="{$smarty.server.REQUEST_URI}" method="post" class="form-inline">

{$form.hidden}

<fieldset>
	<label for="find_name"><small>{tr}{$form.find_name.label}{/tr}</small></label>
	{$form.find_name.html}
	
	<label for="find_description"><small>{tr}{$form.find_description.label}{/tr}</small></label>
	{$form.find_description.html}
	
	<label for="find_label"><small>{tr}{$form.find_label.label}{/tr}</small></label>
	{$form.find_label.html}
	
	<label for="find_extendedcid"><small>{tr}{$form.find_extendedcid.label}{/tr}</small></label>
	{$form.find_extendedcid.html}
	
	<input type="submit" name="filter" value="{tr}filter{/tr}" class="btn btn-default btn-sm" />
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" class="btn btn-default btn-sm" />
</fieldset>

</form>
</div>
