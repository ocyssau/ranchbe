{*Smarty template*}


<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";

	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);

	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}All data of this field will be lost. Are you sure?{/tr}{literal}')){
			var url = baseurl+'/metadata/manager/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default">
	<div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/metadata/manager/create?spacename={$spacename}">{tr}Create{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default"><div class="panel-body">
{$filter}
</div></div>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	</th>
	<th class="heading auto"></th>
	
	<th class="heading sortable" data-field="id">#</th>
	<th class="heading sortable" data-field="extendedCid">{tr}Extended Class{/tr}</th>
	<th class="heading sortable" data-field="appName">{tr}Application Name{/tr}</th>
	<th class="heading sortable" data-field="name">{tr}Db Field Name{/tr}</th>
	<th class="heading sortable" data-field="label">{tr}Label{/tr}</th>
	<th class="heading sortable" data-field="field_description">{tr}Description{/tr}</th>
	<th class="heading sortable" data-field="field_type">{tr}Type{/tr}</th>
	<!-- {*
	<th class="heading sortable" data-field="field_size">{tr}Size{/tr}</th>
	<th class="heading sortable" data-field="field_regex">{tr}Regex{/tr}</th>
	<th class="heading sortable" data-field="field_required">{tr}Required{/tr}</th>
	<th class="heading sortable" data-field="field_multiple">{tr}Multiple{/tr}</th>
	<th class="heading sortable" data-field="return_name">{tr}Return{/tr}</th>
	<th class="heading sortable" data-field="field_list">{tr}List{/tr}</th>
	<th class="heading sortable" data-field="table_name">{tr}Dbtable{/tr}</th>
	<th class="heading sortable" data-field="field_for_value">{tr}forValue{/tr}</th>
	<th class="heading sortable" data-field="field_for_display">{tr}forDisplay{/tr}</th>
	<th class="heading sortable" data-field="date_format">{tr}DateFormat{/tr}</th>
	<th class="heading sortable" data-field="getter">{tr}Getter{/tr}</th>
	<th class="heading sortable" data-field="setter">{tr}Setter{/tr}</th>
	<th class="heading sortable" data-field="min">{tr}Min{/tr}</th>
	<th class="heading sortable" data-field="max">{tr}Max{/tr}</th>
	<th class="heading sortable" data-field="step">{tr}Step{/tr}</th>
	<th class="heading sortable" data-field="dbfilter">{tr}DB Filter{/tr}</th>
	<th class="heading sortable" data-field="dbquery">{tr}DB Query{/tr}</th>
	<th class="heading sortable" data-field="attributes">{tr}Attributes{/tr}</th>
	*} -->
</tr></thead>

<tbody>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">
<td class="selectable">
<input type="checkbox" name="checked[]" value="{$list[list].id}"/>
</td>

<td><a href="{$baseurl}/metadata/manager/edit?id={$list[list].id}&spacename={$spacename}" title="{tr}edit{/tr}">
<img border="0" alt="{tr}edit{/tr}" src="{$baseurl}/img/icons/edit.png" /></a>
</td>

<td class="selectable">{$list[list].id}</td>
<td class="selectable">{$list[list].extendedCid|cidToClassName}</td>
<td class="selectable">{$list[list].appName}</td>
<td class="selectable">{$list[list].name}</td>
<td class="selectable">{$list[list].label}</td>
<td class="selectable">{$list[list].description}</td>
<td class="selectable">{$list[list].type}</td>
<!-- {*
<td class="selectable">{$list[list].size}</td>
<td class="selectable">{$list[list].regex}</td>
<td class="selectable">{$list[list].required|yesorno}</td>
<td class="selectable">{$list[list].multiple|yesorno}</td>
<td class="selectable">{$list[list].return}</td>
<td class="selectable">{$list[list].list}</td>
<td class="selectable">{$list[list].dbtable}</td>
<td class="selectable">{$list[list].forValue}</td>
<td class="selectable">{$list[list].forDisplay}</td>
<td class="selectable">{$list[list].dateFormat}</td>
<td class="selectable">{$list[list].getter}</td>
<td class="selectable">{$list[list].setter}</td>
<td class="selectable">{$list[list].min}</td>
<td class="selectable">{$list[list].max}</td>
<td class="selectable">{$list[list].step}</td>
<td class="selectable">{$list[list].dbfilter}</td>
<td class="selectable">{$list[list].dbquery}</td>
<td class="selectable">{$list[list].attributes}</td>
*} -->
</tr>
{/section}
</tbody>
</table>

<button class="btn btn-default btn-sm btn-delete" name="action" value="suppress" title="{tr}Delete{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Delete{/tr}" width="16" height="16" />
</button>

<button class="btn btn-default btn-sm refresh-btn" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<input type="hidden" name="spacename" value="{$spacename}" />
<input type="hidden" name="layout" value="{$layout}" />
</form>

