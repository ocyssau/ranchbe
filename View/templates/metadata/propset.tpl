{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

{popup_init src="$baseurl/js/overlib.js"}

{$form.javascript}

{literal}
<script language='Javascript' type='text/javascript'>
<!--
function PropsetNameInput(monlien,propset_name,title){
  var newName = prompt(title, propset_name );
  if(newName == null){
    document.location.href = '#';
    return false;
  }
  monlien.search = monlien.search+'&propset_name='+newName;
  //alert(monlien.href);
  document.location.href = monlien.href;
  return true;
}
//-->
</script>
{/literal}

<div id="tiki-center">

<div id="page-bar">
  <span class="button2">
  <a class="linkbut" href="DocMetadata.php?request_page=propset&action=createPropset&space={$CONTAINER_TYPE}&ticket={$ticket}" title="{tr}Create a new propset{/tr}"
   onClick="PropsetNameInput(this,'','{tr}Input the property set name{/tr}');return false;">
  {tr}Create a new propset{/tr}
  </a>
  </span>
</div>


<h1 class="pagetitle">{tr}Properties set{/tr} :</h1>

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto" width="20px"></th>
  <th class="heading auto" width="20px"></th>
  <th class="heading" width="30px"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=propset_id">
   {tr}propset_id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=propset_name">
   {tr}propset_name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=">
   {tr}linked properties{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].propset_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">
    <a href="{$smarty.server.PHP_SELF}?request_page=propset&action=editPropset&checked={$list[list].propset_id}&space={$CONTAINER_TYPE}&ticket={$ticket}" title="{tr}edit{/tr}"
     onClick="PropsetNameInput(this,'{$list[list].propset_name}','{tr}Input the property set name{/tr}');return false;">
    <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
    </a>
    </td>

    <td class="thin">{$list[list].propset_id}</td>
    <td class="thin">{$list[list].propset_name}</td>
    <td class="thin">
      {if $list[list].properties}
      <ul>
        {section name=prop loop=$list[list].properties}
          <li>
            <a href="javascript:popupP('DocMetadata.php?action=modify&field_name={$list[list].properties[prop].field_name}&space={$CONTAINER_TYPE}','EditMetadata',600,800)">{$list[list].properties[prop].field_description}</a>
            <a href="DocMetadata.php?request_page=propset&action=unlinkProperty&propset_id={$list[list].propset_id}&link_id={$list[list].properties[prop].link_id}&space={$CONTAINER_TYPE}" 
            onclick="return confirm('{tr}Do you want really unlink{/tr} {$list[list].properties[prop].field_description}')">
           <img border="0" alt="{tr}unlink it{/tr}" src="img/icons/trash.png" /></a>
         </li>
        {/section}
      </ul>
      {/if}
    <a href="DocMetadata.php?request_page=propset&action=linkProperty&propset_id={$list[list].propset_id}&space={$CONTAINER_TYPE}">
    {tr}Add a property{/tr}</a>
    </td>
   </tr>
  {/section}
  </table>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppressPropset" title="{tr}Suppress{/tr}"
 onclick="if(confirm('{tr}Are you sure that you want suppress this propset?{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>
<br />

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="hidden" name="request_page" value="propset" />
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />


</div>
</body>
