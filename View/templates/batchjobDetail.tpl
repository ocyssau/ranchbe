{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h1 class="pagetitle">{$pageTitle}</h1>

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">

<table><tr>

<td>
<table class="normal table table-bordered">
 <tr>
  <td class="heading">{tr}Job Id{/tr}</a></td>
  <td>{$properties.id}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Name{/tr}</a></td>
  <td>{$properties.name}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Runner{/tr}</a></td>
  <td>{$properties.runner}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Submit{/tr}</a></td>
  <td>{$properties.submit|date_format}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Submit by{/tr}</a></td>
  <td>{$properties.submitBy|username}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Planned{/tr}</a></td>
  <td>{$properties.planned|date_format}</td>
 </tr>
 <tr>
  <td class="heading">{tr}State{/tr}</a></td>
  <td>{$properties.state}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Start{/tr}</a></td>
  <td>{$properties.start|date_format}</td>
 </tr>
 <tr>
  <td class="heading">{tr}End{/tr}</a></td>
  <td>{$properties.end|date_format}</td>
 </tr>
 <tr>
  <td class="heading">{tr}Duration{/tr}</a></td>
  <td>{$properties.duration}</td>
 </tr>
</table>
</td>


<td>
<table>
<tr><td class="heading">{tr}Content{/tr}</td></tr> 
<tr><td class="heading">{tr}Package{/tr}</td> <td class="heading">{tr}Target{/tr}</td></tr> 
  {section name=content loop=$data}
   <tr class="{cycle}">
    <td class="thin">[{$data[content].packageId}]{$data[content].packageName}</td>
    <td class="thin">[{$data[content].targetId}]{$data[content].target}</td>
   </tr>
  {/section}
</table>
</td>


</tr></table>




<p></p>
</form>


{$properties.data.0.packageName} 

