<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>RanchBE</title>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />

<link rel="shortcut icon" href="{$baseurl}/{$shortcut_icon}" />

<link href="{$baseurl}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{$baseurl}/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="{$baseurl}/css/application.css" rel="stylesheet" type="text/css" />
<link href="{$baseurl}/styles/default.css" rel="stylesheet" type="text/css" />
<link href="{$baseurl}/js/jqueryui/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<!-- 
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
-->

<script src="{$baseurl}/js/jquery/jquery.min.js"></script>
<script src="{$baseurl}/js/jqueryui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{$baseurl}/bootstrap/js/bootstrap.min.js"></script>
<script src="{$baseurl}/js/lib.js"></script>
<script src="{$baseurl}/js/application.js"></script>

{$additionnal_header}

<noscript>
BE CAREFUL : You must enable javascript and pop-upfor use RanchBE.
</noscript>
</head>
