{literal}
<style>
.transparent{
	background-color: transparent;
}

</style>
{/literal}

<div class="modal fade rbloadwaiting" tabindex="-1" id="rbloadwaiting">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body center-block">
				<img src="{$baseurl}/img/loadwaiting.gif" class="center-block"/>
				<h1 class="center-text">Waiting...</h1>
			</div>
		</div>
	</div>
</div>
