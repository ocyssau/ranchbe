{*Smarty template*}
{include file="layouts/htmlheader.tpl"}

{datepickerDateFormat from=$smarty.const.SHORT_DATE_FORMAT assignTo="dateformat"}

<script>
document.baseurl = "{$baseurl}";
{literal}
var ranchbe = new ranchbe();
ranchbe.init();
$(function() {
});
{/literal}
</script>

{flashMessenger assignTo="rbFlashMessenger"}

<body>

{include file="layouts/tabs/tabs.tpl"}

<div id="rb-main" class="container rb-main">

	<div class="rb-feedbacks">
		{section name=flasherr loop=$rbFlashMessenger.errors}
			<div class="alert alert-danger alert-dismissible fade in" role="alert">{$rbFlashMessenger.errors[flasherr]}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		{/section}
		{section name=flasherr loop=$rbFlashMessenger.messages}
			<div class="alert alert-info alert-dismissible fade in" role="alert">{$rbFlashMessenger.messages[flasherr]}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		{/section}
		{section name=flasherr loop=$rbFlashMessenger.success}
			<div class="alert alert-success alert-dismissible fade in" role="alert">{$rbFlashMessenger.success[flasherr]}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		{/section}
		{section name=flashwarning loop=$rbFlashMessenger.warnings}
			<div class="alert alert-warning alert-dismissible fade in" role="alert">{$rbFlashMessenger.warnings[flashwarning]}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		{/section}
	</div>

	<div id="rb-top">{include file="layouts/header.tpl"}</div>
	<div id="rb-body">
		{if $tool_bar}
			<div id="BoiteActions">{include file="$tool_bar"}</div>
		{/if}
		
		{if $mid}
			{include file=$mid}
		{/if}
		
		{if $midcontent}
			{$midcontent}
		{/if}
	</div>
	<div id="rb-footer">{include file="layouts/footer.tpl"}</div>
</div>

{include file="layouts/loadwaiting.tpl"}

</body>
