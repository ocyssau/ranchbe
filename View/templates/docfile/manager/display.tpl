{*Smarty template*}

{literal}
<script>
$(function() {
	var url = window.location.origin + window.location.pathname;
	{/literal}
	var orderby="{$orderby}";
	var order="{$order}";
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(orderby, order, url.url);
});
</script>
{/literal}

<div class="rbgui-container container-fluid">

<div class="panel panel-default">
<div class="panel-body">
	<div id="page-bar">
		<a class="linkbut btn btn-default btn-xs rb-action hidedfmanager" href="{$baseurl}/docfile/manager/hide">{tr}Hide file manager{/tr}</a>
	</div>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
	<div class="panel-body">
		{$filter}
	</div>
</div>

{*--------------------list header----------------------------------*}
<div class="panel panel-default">

{* -------------------Pagination------------------------ *}
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">
	<form name="checkform" method="post" action="#">
		{include file='docfile/manager/list1.tpl'}
		{include file='docfile/manager/gabysbar.tpl'}
		<input type="hidden" name="containerid" value="{$containerid}" />
		<input type="hidden" name="spacename" value="{$spacename}" />
	</form>
</div>
</div>
</div>

