{*Smarty template*}

<div id="action-menu" class="gabysbar">

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}document.checkform.action='{$baseurl}/docfile/manager/delete?space={$spacename}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$baseurl}/docfile/manager/display?space={$spacename}'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

</div>
