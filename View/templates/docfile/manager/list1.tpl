<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
		<div id="displaySelectedRowCount">0</div>
	</th>

	<th class="heading sortable" data-field="parentNumber">{tr}Document{/tr}</th>
	<th>
	<span class="heading sortable" data-field="name">{tr}File Name{/tr}</span>
	<span class="heading sortable" data-field="extension">{tr}Extension{/tr}</span>
	<span class="heading sortable" data-field="type">{tr}Type{/tr}</span>
	<span class="heading sortable" data-field="accessCode">{tr}Access Code{/tr}</span>
	<span class="heading sortable" data-field="lifeStage">{tr}State{/tr}</span>
	</th>
	
	<th class="heading sortable" data-field="size">{tr}Size{/tr}</th>
	
	<th class="heading">
		{tr}CheckOut{/tr}
			<span class="heading sortable" data-field="lockById">{tr}By{/tr}</span> -
			<span class="heading sortable" data-field="locked">{tr}Date{/tr}</span>
			<br/>
		{tr}Last Update{/tr}
			<span class="heading sortable" data-field="updateById">{tr}By{/tr}</span> -
			<span class="heading sortable" data-field="updated">{tr}Date{/tr}</span>
			<br/>
		{tr}Created{/tr}
			<span class="heading sortable" data-field="createById">{tr}By{/tr}</span> -
			<span class="heading sortable" data-field="created">{tr}Date{/tr}</span>
			<br/>
		{tr}Mtime{/tr}
			<span class="heading sortable" data-field="mtime">{tr}Date{/tr}</span>
	</th>
</tr></thead>

<tbody>
	{cycle print=false values="even,odd"}
	{section name=list loop=$list}
	<tr class="{cycle}">
	<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].file_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

	<td>
	<a class="rb-popup" href="{$baseurl}/rbdocument/detail/index?documentid={$list[list].parentId}&spacename={$spacename}">
	{$list[list].parentNumber} - {$list[list].parentVersion|document_indice}.{$list[list].parentIteration}</a></td>
	
	<td>
	<a href="{$baseurl}/docfile/manager/viewfile?spacename={$spacename}&checked[]={$list[list].id}&ticket={$ticket}" title="{tr}View file{/tr}">
	{file_icon extension=$list[list].extension}
	{$list[list].name}.{$list[list].iteration}</a> ({$list[list].type})
	{$list[list].accessCode|access_code}
	</td>
	
	<td class="selectable">{$list[list].size|filesize_format}</td>
	
	<td class="selectable">
	{if $list[list].lockById}
	<b>{tr}Checkout{/tr} : </b>{$list[list].lockById|username} - {$list[list].locked|date_format}
	<br />
	{/if}
	<b>{tr}Update{/tr} : </b>{$list[list].updateById|username} - {$list[list].updated|date_format}
	<br />
	<b>{tr}Created{/tr} : </b>{$list[list].createById|username} - {$list[list].created|date_format}
	<br />
	<b>{tr}mtime{/tr} : </b>{$list[list].mtime|date_format}
	</td>
	</tr>
	{/section}
</tbody>
</table>
