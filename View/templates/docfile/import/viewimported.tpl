{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h1 class="pagetitle">{tr}Package imported files{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name">
  {tr}File Name{/tr}</a> /
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
  {tr}File Extension{/tr}</a>
  </th>

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
  {tr}File Extension{/tr}</a></th>-->

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_path">
  {tr}File Path{/tr}</a></th>-->

  <th class="heading">{tr}Open{/tr}<hr />
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_by">
  {tr}By{/tr}</a>/<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_date">{tr}Date{/tr}</a>
  </th>

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_date">
  {tr}Open Date{/tr}</a></th>-->

  <!--<th class="heading">{tr}Update{/tr}<hr />
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_by">
  {tr}By{/tr}</a>/<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_date">{tr}Date{/tr}</a></th>-->

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_access_code">
  {tr}Access code{/tr}</a></th>-->

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_order">
  {tr}Import order{/tr}</a></th>

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_state">
  {tr}State{/tr}</a></th>-->

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_version">
  {tr}Version{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type">
  {tr}Type{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size">
  {tr}Size{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime">
  {tr}Mtime{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_md5">
  {tr}MD5{/tr}</a></th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <!--{*<td class="thin">{$list[list].file_id}
    <a href="{$CONTAINER_TYPE}FileManage.php?action=viewFile&checked[]={$list[list].file_path}/{$list[list].file_name}&ticket={$ticket}" title="{tr}View file{/tr}">
    {file_icon extension=$list[list].file_extension }
    {$list[list].file_name}</a></td>*}-->

    <td class="thin">
    <a class="link" href="{$baseurl}/document/manager/viewfile?space={$CONTAINER_TYPE}&checked[]={$list[list].file_id}&ticket={$ticket}" title="{tr}View file{/tr}">
    {file_icon extension=$list[list].file_extension }
    {$list[list].file_name}</a></td>

    <!--{*<td class="thin">{$list[list].file_extension}</td>*}-->
    <!--{*<td class="thin">{$list[list].file_path}</td>*}-->
    <td class="thin">{$list[list].file_open_by|username}<hr />
    {$list[list].file_open_date|date_format}</td>
    <!--{*<td class="thin">{$list[list].file_update_by|username}<hr />
    {$list[list].file_update_date|date_format}</td>*}-->
    <!--{*<td class="thin">{$list[list].file_access_code}</td>*}-->
    <td class="thin">{$list[list].import_order}</td>
    <!--{*<td class="thin">{$list[list].file_state}</td>*}-->
    <td class="thin">{$list[list].file_version}</td>
    <td class="thin">{$list[list].file_type}</td>
    <td class="thin">{$list[list].file_size|filesize_format}</td>
    <td class="thin">{$list[list].file_mtime|date_format}</td>
    <td class="thin">{$list[list].file_md5}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

<br />


<button class="mult_submit btn btn-default btn-sm" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="action" value="{$action|escape}" />
<input type="hidden" name="import_order" value="{$import_order}" />
</form>


<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />
<br />
<br />
