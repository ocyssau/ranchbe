{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h1 class="pagetitle">{tr}Import history{/tr}</h1>

<fieldset>
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">
  <br />
  <label>
  <input size="4" type="text" name="numrows" value="{$numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
  <br />

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field">
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$all_field item=name key=field}
   <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

</fieldset>
</form>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_order">
  {tr}import_order{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_name">
  {tr}package_file_name{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_extension">
  {tr}package_file_extension{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=description">
  {tr}description{/tr}</a>
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$CONTAINER_TYPE}_id">
  {tr}{$CONTAINER_TYPE}{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=state">
  {tr}state{/tr}</a></th>

  <th class="heading">{tr}imported{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_by">
  {tr}by{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_date">
  {tr}date{/tr}</a>
  </th>

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_logfiles_file">
  {tr}import_logfiles_file{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_errors_report">
  {tr}import_errors_report{/tr}</a>
  </th>

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_path">
  {tr}package_file_path{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_mtime">
  {tr}package_file_mtime{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_size">
  {tr}package_file_size{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_extension">
  {tr}package_file_extension{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_md5">
  {tr}package_file_md5{/tr}</a>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].import_order}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    </td>

    <td class="thin">
      <a href="javascript:popupP('ImportManager.php?space={$CONTAINER_TYPE}&action=viewImported&import_order={$list[list].import_order}','viewImported', 800, 900)" title="{tr}See linked imported files{/tr}">
      <img border="0" alt="{tr}viewImported{/tr}" src="img/icons/zoom.gif" />
      </a>
    </td>

      <td class="thin">{$list[list].import_order}</td>
      <td class="thin">
        {$list[list].package_file_name}<br />
        {$list[list].description}
      </td>
      <td class="thin">{filter_select id=$list[list].$CONTAINER_ID type=$CONTAINER_TYPE}</a></td>
      <td class="thin">{$list[list].state}</td>
      <td class="thin">
        <b>{tr}imported{/tr} : </b>{$list[list].import_by|username} - {$list[list].import_date|date_format}<br />
      </td>

      <td class="thin">
      {if $list[list].import_logfiles_file}
        <a href="ImportHistory.php?space={$CONTAINER_TYPE}&action=viewLogFile&import_order={$list[list].import_order}" title="{tr}Log{/tr}">
        ViewLog</a><br />
      {/if}
      {if $list[list].import_errors_report}
        <a href="ImportHistory.php?space={$CONTAINER_TYPE}&action=viewErrorFile&import_order={$list[list].import_order}" title="{tr}Errors{/tr}">
        ViewErrors</a>
      {/if}
      </td>

      <td class="thin">
      <ul>
        <li>{tr}package_file_path{/tr} : {$list[list].package_file_path}</li>
        <li>{tr}package_file_mtime{/tr} : {$list[list].package_file_mtime|date_format}</li>
        <li>{tr}package_file_size{/tr} : {$list[list].package_file_size|filesize_format}</li>
        <li>{tr}package_file_md5{/tr} : {$list[list].package_file_md5}</li>
      </ul>
      </td>
   </tr>
  {/section}
  </table>

<i>{tr}Perform action{/tr} :</i>
<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppressHistory" title="{tr}Suppress{/tr}"
 onclick="if(confirm('{tr}Do you want really suppress this history{/tr}')){ldelim}pop_no(checkform);{rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="cleanHistory" title="{tr}Clean history{/tr}"
 onclick="if(confirm('{tr}Suppress all not imported record?{/tr}')){ldelim}pop_no(checkform);{rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/history_clean.png" title="{tr}Clean history{/tr}" alt="{tr}Clean history{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="getImportHistory" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>
