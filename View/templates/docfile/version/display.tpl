{*Smarty template*}

{literal}
<script>
function deleteVersion()
{
	if(confirm({/literal}'{tr}Do you want really suppress this file{/tr}'{literal})){
		document.checkform.action={/literal}'{$baseurl}/docfile/version/delete';{literal}
		pop_no(checkform);
	}
	else{
		return false;
	}
}

function putinws()
{
	document.checkform.action={/literal}'{$baseurl}/docfile/version/putinws';{literal}
	pop_no(checkform);
}

</script>
{/literal}

<div class="rbgui-container container-fluid">

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------list header----------------------------------*}
<div class="panel panel-default">

<div class="panel-body">
<form name="checkform" method="post" action="#">

<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
		<div id="displaySelectedRowCount">0</div>
	</th>

	<th>
	<span class="heading" data-field="name">{tr}File Name{/tr}</span>
	<span class="heading" data-field="extension">{tr}Extension{/tr}</span>
	<span class="heading" data-field="type">{tr}Type{/tr}</span>
	<span class="heading" data-field="lifeStage">{tr}State{/tr}</span>
	</th>
	
	<th class="heading" data-field="size">{tr}Size{/tr}</th>
	
	
	<th class="heading">
		{tr}CheckOut{/tr}
			<span class="heading" data-field="lockById">{tr}By{/tr}</span> -
			<span class="heading" data-field="locked">{tr}Date{/tr}</span>
			<br/>
		{tr}Last Update{/tr}
			<span class="heading" data-field="updateById">{tr}By{/tr}</span> -
			<span class="heading" data-field="updated">{tr}Date{/tr}</span>
			<br/>
		{tr}Created{/tr}
			<span class="heading" data-field="createById">{tr}By{/tr}</span> -
			<span class="heading" data-field="created">{tr}Date{/tr}</span>
			<br/>
		{tr}Mtime{/tr}
			<span class="heading" data-field="mtime">{tr}Date{/tr}</span>
	</th>
	
</tr></thead>

<tbody>
{cycle print=false values="even,odd"}
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].id}" /></td>

	<td>
	<a href="{$baseurl}/docfile/version/viewfile?spacename={$spacename}&fileid={$list[list].id}" title="{tr}View file{/tr}">
	{file_icon extension=$list[list].extension}
	{$list[list].name}.{$list[list].iteration}</a>
	({$list[list].type})
	{$list[list].lifeStage}
	</td>
	
	<td class="selectable">{$list[list].size|filesize_format}</td>
	
	<td class="selectable">
	{if $list[list].lockById}
	<b>{tr}Checkout{/tr} : </b>{$list[list].lockById|username} - {$list[list].locked|date_format}
	<br />
	{/if}
	<b>{tr}Update{/tr} : </b>{$list[list].updateById|username} - {$list[list].updated|date_format}
	<br />
	<b>{tr}Created{/tr} : </b>{$list[list].createById|username} - {$list[list].created|date_format}
	<br />
	<b>{tr}mtime{/tr} : </b>{$list[list].mtime|date_format}
	</td>
</tr>
{/section}
</tbody>
</table>

<input type="hidden" name="containerid" value="{$containerid}" />
<input type="hidden" name="fileid" value="{$fileid}" />
<input type="hidden" name="spacename" value="{$spacename}" />
</form>
</div>
</div>
</div>

<div id="dfversion-action-menu {$uniqid}" class="rb-action-menu">
	<button class="btn btn-default btn-sm rbaction" type="submit" name="delete" title="{tr}Delete{/tr}">
		<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Delete{/tr}" />
	</button>

	<button class="btn btn-default btn-sm rbaction" type="submit" name="putInWs" title="{tr}Put in wildspace{/tr}">
		<img class="icon" src="{$baseurl}/img/icons/document/document_read.png" alt="{tr}Put in wildspace{/tr}"/>
	</button>
</div>
