{*Smarty template*}

<script>
$(function() {ldelim}
	var url = window.location.origin + window.location.pathname;
	var orderby="{$orderby}";
	var order="{$order}";
	var url=getUrlWithoutSort();
	initSortableHeader(orderby, order, url.url);
	
	{literal}
	/* CONTEXT MENU */
	initContextMenu(".contextmenu-showbtn", "#project-context-menu", function(e){
		e.preventDefault();

		var data = $(this).parents("#project-context-menu").first().data('menuItemData');
		var id = data.id;
		var spacename = data.spacename;
		var basecamp = data.basecamp;
		var currentUserId = data.currentUserId;
		
		var confirmMessage = $(this).data('confirm');
		if(typeof(confirmMessage) != 'undefined'){
			if(confirm(confirmMessage)){
				null;
			}
			else{
				return false;
			}
		}
		
		var url = $(this).attr('href');
		var urlDecomposed = url.split('?');
		var baseUrl = urlDecomposed[0];
		var searchUrl = urlDecomposed[1];

		var idkey = $(this).data('idkey');
		if(typeof(idkey)=='undefined'){
			idkey = 'projectid';
		}
		
		url = baseUrl+"?"+idkey+"="+id;
		if(basecamp != ""){
			url = url+"&basecamp="+basecamp;
		}
		if(typeof(searchUrl) != "undefined"){
			url = url+"&"+searchUrl;
		}

		document.location.assign(url);
		return false;
	});
	
	/*BUTTONS DEFINITIONS*/
	$(".delete-btn").click(function(){
		if(confirm('Do you want really delete this projects?')){
			var url = document.baseurl+'/project/index/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	
	{/literal}
{rdelim});
</script>

<ul id="project-context-menu" class="ul-contextmenu">
	<li>
		<a href="{$baseurl}/project/index/edit" title="{tr}edit project{/tr}">
		<img border="0" alt="" src="{$baseurl}/img/icons/edit.png" />
		{tr}Edit{/tr}</a>
	</li>
	<li>
		<a href="{$baseurl}/project/index/delete" title="{tr}Suppress project{/tr}" 
			data-confirm="The Project must be empty to be deleted, Do you want to continue?">
		<img border="0" alt="" src="{$baseurl}/img/icons/trash.png" />
		{tr}Delete{/tr}</a>
	</li>
	<li>
		<a href="{$baseurl}/user/roleadmin/display?areaid=5" title="{tr}Permissions{/tr}" data-idkey="resourceid">
		<img border="0" alt="" src="{$baseurl}/img/icons/user.png" />
		{tr}Permissions{/tr}</a>
	</li>
</ul>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/project/index/create">{tr}Create{/tr}</a>
</div></div>

<h1 class="pagetitle">{tr}Project manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default"><div class="panel-body">
{$filter}
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">


{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
<tr><thead>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading sortable" data-field="name">{tr}Number{/tr}</th>
  <th class="heading sortable" data-field="project_description">{tr}Description{/tr}</th>
  <th class="heading sortable" data-field="project_state">{tr}State{/tr}</a></th>
  <th class="heading sortable" data-field="open_date">{tr}Creation date{/tr}</th>
  <th class="heading sortable" data-field="open_by">{tr}Created by{/tr}</th>
  <th class="heading sortable" data-field="forseen_close_date">{tr}Forseen close date{/tr}</th>
  <th class="heading sortable" data-field="close_date">{tr}Close date{/tr}</th>
</thead></tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=projects loop=$list}
<tbody>
<tr class="{cycle}" data-id="{$list[projects].project_id}">

	<td class="selectable">
	<input type="checkbox" name="checked[]" value="{$list[projects].project_id}" {if $projects[projects].checked eq 'y'}checked="checked" {/if}/>
	</td>

	<td>
		<div>
		<button id="contextmenu" class="contextmenu-showbtn btn btn-default btn-sm" 
			data-id="{$list[projects].project_id}"
			data-spacename="{$spacename}" 
			data-basecamp="{$basecamp}">Actions Menu<span class="caret"></span></button>
		</div>
	</td>

	<td>
	<a class="link" href="#" title="{tr}Number{/tr}: {$list[projects].name}">
	{$list[projects].name}</a>
	</td>
	
	<td class="selectable" class="link" >{$list[projects].project_description}</td>
	<td class="selectable" class="link" >{$list[projects].project_state}</td>
	<td class="selectable" class="link" >{$list[projects].open_date|date_format}</td>
	<td class="selectable" class="link" >{$list[projects].open_by|username}</td>
	<td class="selectable" class="link" >{$list[projects].forseen_close_date|date_format}</td>
	<td class="selectable" class="link" >{$list[projects].close_date|date_format}</td>
</tr>
{sectionelse}
<tr><td colspan="6">{tr}No projects to display{/tr}</td></tr>
{/section}
</tbody></table>

<button class="btn btn-default btn-sm delete-btn" type="submit" value="delete" title="{tr}Delete{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Delete{/tr}" />
</button>

</form>

</div>
</div>
