{*Smarty template*}

<div id="page-bar" class="panel panel-default"><div class="panel-body">
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkworkitem?projectid={$projectId}">{tr}Link a workitem{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkmockup?projectid={$projectId}">{tr}Link a mock-Up{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkpartner?projectid={$projectId}">{tr}Link a partner{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linklib?projectid={$projectId}">{tr}Link a librairy{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkbib?projectid={$projectId}">{tr}Link a bookshop{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkdoctype?projectid={$projectId}">{tr}Link a doctype{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/project/adminlinks/linkdefaultprocess?projectid={$projectId}">{tr}Link a default process{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">

<table class="normal table table-bordered">
 <tr>
 <thead>
  <th class="heading auto"></th>
  <th class="heading">{tr}{$HeaderCol1}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol2}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol3}{/tr}</th>
  </thead>
 </tr>
 <tbody>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[{$list[list].$col1}][]" value="{$list[list].$id}" /></td>
    <td class="thin">{$list[list].$col1}</td>
    <td class="thin">{$list[list].$col2}</td>
    <td class="thin">{$list[list].$col3}</td>
   </tr>
  {/section}
 </tbody>
</table>

<input type="hidden" name="projectid" value="{$projectId}">
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />


<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppressLink" title="{tr}Suppress link{/tr}" id="01"
 onclick="document.checkform.action='{$baseurl}/project/adminlinks/delete'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/link_break.png" title="{tr}Suppress link{/tr}" alt="{tr}Suppress link{/tr}" width="16" height="16" />
</button>

</form>
