{*Smarty template*}

<h1 class="pagetitle">
	{tr}Assign permissions to group{/tr}: {$group_info.0.group_define_name}</a>
</h1>

{$msg}
<br />

{*----------------Permissions list header--------------------------*}
<form action="{$baseurl}/project/assignpermission/update" method="post">
	<input type="hidden" name="group" value="{$group|escape}" /> 
	<input type="hidden" name="type" value="{$type|escape}" /> 
	<input type="hidden" name="project_id" value="{$project_id|escape}" /> 
	<input type="hidden" name="area_id" value="{$area_id|escape}" />
	<table class="normal table table-bordered">
		<tr>
			<td class="heading">&nbsp;</td>
			<td class="heading"><a class="tableheading">{tr}desc{/tr}</td>
		</tr>

		{*----------------Permissions list body-------------------------*}
		{cycle values="odd,even" print=false}
		{section name=user loop=$perms}
		<input type="hidden" name="permName[{$perms[user].right_id}]" />
		<tr>
			<td class="{cycle advance=false}"><input type="checkbox"
				name="perm[{$perms[user].right_id}]" 
				{section name=group_perms loop=$group_perms}
					{if $group_perms[group_perms].right_id eq $perms[user].right_id}
			          checked="checked" 
			        {/if}
		        {/section}
		        />
				{*$perms[user].right_id*}
			</td>
			<td class="{cycle}">{tr}{$perms[user].right_description}{/tr}</td>
		</tr>
		{/section}
	</table>
	
	<input type="submit" name="update" value="{tr}Update{/tr}" />
	
</form>
