{*Smarty template*}
<div class="container">

<h2>{$pageTitle}</h2>

<form {$form.attributes}>
{$form.hidden}

<table class="normal table table-bordered">
  <tr>
    <td class="formcolor">{tr}{$form.name.label}{/tr}:</td>
    <td class="formcolor">{$form.name.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
 
  <tr>
    <td class="formcolor">{tr}{$form.description.label}{/tr}:</td>
    <td class="formcolor">{$form.description.html}</td>
  </tr>
  
  <tr>
    <td class="formcolor">{tr}{$form.forseenCloseDate.label}{/tr}:</td>
    <td class="formcolor">{$form.forseenCloseDate.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor" colspan=2>
    {$form.validate.html}
    {$form.cancel.html}
    </td>
  </tr>
 {/if}

  <tr>
       <td class="formcolor" colspan=2>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}
</div>
