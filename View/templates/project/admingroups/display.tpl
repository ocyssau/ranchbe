{*Smarty template*}

    <h2>{tr}List of existing groups{/tr}</h2>

{*--------------------Group list header----------------*}
<table class="normal table table-bordered">
<tr>
    <th class="heading">{tr}name{/tr}</th>
    <th class="heading">{tr}desc{/tr}</th>
    <th class="heading">{tr}Permissions{/tr}</th>
</tr>

{*--------------------Group list body----------------*}
{cycle values="even,odd" print=false} {* ---SmartyCode to alternate colors of rows---*}
  {section name=user loop=$users}
   <tr class="{cycle}">
      <td>{$users[user].group_define_name}</td>
      <td>{tr}{$users[user].group_description}{/tr}</td>
      <td>
       <a class="link" 
          href="{$baseurl}/project/assignpermission/display?group={$users[user].group_id}&project_id={$project_id}&area_id={$area_id}"
	          title="{tr}permissions{/tr}">
          <img border="0" alt="{tr}permissions{/tr}" src="{$baseurl}/img/icons/key.gif" />
       </a>
      </td>
   </tr>
  {/section}
</table>
</div>

<p></p>
