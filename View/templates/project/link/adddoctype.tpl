{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<form name="close"><input type="button" onclick="window.close()" value="Close"></form>
<hr />

<h2>{tr}Linked doctypes{/tr}</h2>

{* -------------------Pagination------------------------ *}
{*{include file='pagination.tpl'}*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Container{/tr}</th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_number">
   {tr}Doctype{/tr}</a></th>
  <th class="heading">{tr}Process{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].doctype_id}" /></td>
    <td class="thin">{$container_number}</td>
    <td class="thin">{$list[list].doctype_number}</td>
    <td class="thin">{$list[list].process_id|process}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{*{include file='pagination.tpl'}*}

<tr><td class="thin">
<input type="checkbox" class="switcher" data-toswitch="[name='link_id[]']" />
<td class="form" colspan="18"><label for="clickall">{tr}select all{/tr}</label></td></tr>

<br>

{*Multiselection select action form *}
<img class="icon" src="{$baseurl}/img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="flag" value="linkdoctype" title="{tr}Link doctype{/tr}" id="02"
	 onclick="document.checkform.action='{$baseurl}/project/adminlinks/linkdoctype'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/doctype/doctype_link.png" title="{tr}Link doctype{/tr}" alt="{tr}Link doctype{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="project_id" value="{$project_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>


<form name="close"><input type="button" onclick="window.close()" value="Close"></form>
