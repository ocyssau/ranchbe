{*Smarty template*}

<div id="page-bar">
	<span class="button2">
		<a class="linkbut"
			href="javascript:popupP('{$baseurl}/recordfile/importhistory/display?space={$spaceName}','{$randWindowName}',800 , 1200)">
			{tr}History import{/tr}</a></span>
	<span class="button2">
		<a class="linkbut"
			href="{$baseurl}/recordfile/import/display?space={$spaceName}">{tr}Import{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Recorded files manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{$filter} 

{* -------------------Pagination------------------------ *}
{$paginator} 

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
<thead><tr>
<th class="heading auto">
<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
<div id="displaySelectedRowCount">0</div>
</th>

	<th class="heading"><a class="tableheading"
	href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name">
		{tr}File Name{/tr}</a> . <a class="tableheading"
	href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
		{tr}File Extension{/tr}</a> - <a class="tableheading"
	href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_version">
		{tr}Version{/tr}</a><br /> <a class="tableheading"
	href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type">
		(<i>{tr}Type{/tr}</i>)
	</a></th>
	
	<th class="heading">
	<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_order">
	{tr}Imported from{/tr}</a></th>
	
	<th class="heading">{tr}Open{/tr} <a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_by">
			{tr}By{/tr}</a>/<a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_date">{tr}Date{/tr}</a>
		<br /> {tr}Update{/tr} <a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_by">
			{tr}By{/tr}</a>/<a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_date">{tr}Date{/tr}</a>
		<br /> <a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime">
			{tr}Mtime{/tr}</a>
	</th>
	
	<th class="heading"><a class="tableheading"
		href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size">
			{tr}Size{/tr}</a></th>
</tr>

{*--------------------list body---------------------------*} 
{cycle print=false values="even,odd"} 
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="selectable"><input type="checkbox" name="checked[]"
		value="{$list[list].file_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

	<td class="thin"><a class="link"
		href="{$baseurl}/recordfile/manager/viewfile?space={$spaceName}&checked[]={$list[list].file_id}&ticket={$ticket}"
		title="{tr}View file{/tr}"> {file_icon extension=$list[list].file_extension } {$list[list].file_name} -
			{$list[list].file_version}</a> <br /> <i>({$list[list].file_type})</i>
	</td>

	<td class="thin"><a
		href="javascript:popupP('ImportHistory.php?action=getImportHistory&space={$CONTAINER_TYPE}&find_field=package_file_name&find={$list[list].import_order|import_package_name}','viewImported', 600 , 1024)"
		title="{tr}See package details{/tr}">
			{$list[list].import_order|import_package_name}</a> 
			<br /> <font
		size="-3">(<i>{tr}Order{/tr} : {$list[list].import_order}</i>)
	</font></td>

	<td class="thin"><b>{tr}Created{/tr} : </b>{$list[list].file_open_by|username}
		- {$list[list].file_open_date|date_format}<br /> <b>{tr}Updated{/tr}
			: </b>{$list[list].file_update_by|username} -
		{$list[list].file_update_date|date_format}<br /> <b>{tr}mtime{/tr}
			: </b>{$list[list].file_mtime|date_format}<br /></td>

	<td class="thin">{$list[list].file_size|filesize_format}</td>
	{/section}
</table>

<tr>
	<td class="thin"><input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" /></td>
	<td class="form" colspan="18"><label for="clickall">
			{tr}select all{/tr} : </label></td>
</tr>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}document.checkform.action='{$baseurl}/recordfile/manager/delete?space={$spaceName}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}"
 onclick="document.checkform.action='{$baseurl}/recordfile/manager/putinws?space={$spaceName}'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$baseurl}/recordfile/manager/display?space={$spaceName}'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<input type="hidden" name="containerid" value="{$containerid}" />
<input type="hidden" name="spacename" value="{$spaceName}" />
</form>
