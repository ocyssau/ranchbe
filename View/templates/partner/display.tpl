{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";

	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);

	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}Do you want really suppress this partner?{/tr}{literal}')){
			var url = document.baseurl+'/partner/index/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});

	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default">
	<div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/partner/index/create?spacename={$spacename}">{tr}Create{/tr}</a>
	</div>
</div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">

{*--------------------list header----------------------------------*}
<form id="checkform" name="checkform" method="post" action="#">
<table class="normal table table-bordered">
<thead><tr>
<th class="heading auto">
<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
<div id="displaySelectedRowCount">0</div>
</th>

<th class="heading auto"></th>

<th class="heading sortable" data-field="firstname">{tr}First Name{/tr}</th>
<th class="heading sortable" data-field="lastname">{tr}Last Name{/tr}</th>
<th class="heading sortable" data-field="mail">{tr}Email{/tr}</th>
<th class="heading sortable" data-field="website">{tr}Website{/tr}</th>
<th class="heading sortable" data-field="adress">{tr}Adress{/tr}</th>
<th class="heading sortable" data-field="city">{tr}City{/tr}</th>
<th class="heading sortable" data-field="zipcode">{tr}Zipcode{/tr}</th>
<th class="heading sortable" data-field="phone">{tr}Phone{/tr}</th>
<th class="heading sortable" data-field="cellphone">{tr}Cellphone{/tr}</th>
<th class="heading sortable" data-field="company">{tr}Company{/tr}</th>
<th class="heading sortable" data-field="activity">{tr}Activity{/tr}</th>
<th class="heading sortable" data-field="type">{tr}Type{/tr}</th>
</tr></thead>

<tbody>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"}
{section name=list loop=$list}
<tr class="{cycle}">
<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].id}"/></td>

	<td>
	<a href="{$baseurl}/partner/index/edit?id={$list[list].id}" title="{tr}Edit{/tr}">
	<img border="0" alt="{tr}Edit{/tr}" src="{$baseurl}/img/icons/edit.png" />
	</a>

	<a href="{$baseurl}/partner/index/suppress?checked[]={$list[list].id}" title="{tr}Suppress{/tr}">
	<img border="0" alt="{tr}Suppress{/tr}" src="{$baseurl}/img/icons/trash.png" />
	</a>
	</td>

	<td class="selectable">{$list[list].firstname}</td>
	<td class="selectable">{$list[list].lastname}</td>
	<td class="selectable"><a href="mailto:{$list[list].mail}">{$list[list].mail}</a></td>
	<td class="selectable"><a href="http://{$list[list].website}">{$list[list].website}</a></td>
	<td class="selectable">{$list[list].adress}</td>
	<td class="selectable">{$list[list].city}</td>
	<td class="selectable">{$list[list].zipcode}</td>
	<td class="selectable">{$list[list].phone}</td>
	<td class="selectable">{$list[list].cellphone}</td>
	<td class="selectable">{$list[list].company}</td>
	<td class="selectable">{$list[list].activity}</td>
	<td class="selectable">{$list[list].type}</td>
</tr>
{/section}
</table>

<button class="btn btn-default btn-sm delete-btn" type="submit" name="action" value="suppressCat" title="{tr}Suppress{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Suppress{/tr}"/>
</button>

<button class="mult_submit btn btn-default btn-sm refresh-btn" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" alt="{tr}Refresh{/tr}"/>
</button>

</form>
