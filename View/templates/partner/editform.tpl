{*Smarty template*}

<div class="container">

<h2>{$pageTitle}</h2>

<form {$form.attributes}>
{$form.hidden}

<table class="normal table table-bordered">

<tr>
<td class="formcolor">{tr}{$form.type.label}{/tr}:</td>
<td class="formcolor">{$form.type.html}</td>
</tr>

<tr>
<td class="formcolor">{tr}{$form.firstname.label}{/tr}:</td>
<td class="formcolor">{$form.firstname.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.lastname.label}{/tr}:</td>
<td class="formcolor">{$form.lastname.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.adress.label}{/tr}:</td>
<td class="formcolor">{$form.adress.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.city.label}{/tr}:</td>
<td class="formcolor">{$form.city.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.zipcode.label}{/tr}:</td>
<td class="formcolor">{$form.zipcode.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.phone.label}{/tr}:</td>
<td class="formcolor">{$form.phone.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.cellphone.label}{/tr}:</td>
<td class="formcolor">{$form.cellphone.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.mail.label}{/tr}:</td>
<td class="formcolor">{$form.mail.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.website.label}{/tr}:</td>
<td class="formcolor">{$form.website.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.activity.label}{/tr}:</td>
<td class="formcolor">{$form.activity.html}</td>
</tr>
<tr>
<td class="formcolor">{tr}{$form.company.label}{/tr}:</td>
<td class="formcolor">{$form.company.html}</td>
</tr>

{if not $form.frozen}
<tr><td colspan=2>
	{$form.validate.html}
	{$form.cancel.html}
</td></tr>

{/if}
</table>

{tr}{$form.requirednote}{/tr}
</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
<font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}
