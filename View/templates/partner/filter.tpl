{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form id="filterf" action="#" method="post" class="form-inline">

{$form.hidden}
{sameurlpost}

<fieldset>
	<label for="find_number"><small>{tr}{$form.find_number.label}{/tr}</small></label>
	{$form.find_number.html}
	
	<label for="find"><small>{tr}{$form.find.label}{/tr}</small></label>
	{$form.find.html}
	
	<label for="find_field"><small>{tr}{$form.find_field.label}{/tr}</small></label>
	{$form.find_field.html}
	
	<input type="submit" name="filter" value="{tr}filter{/tr}" />
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</fieldset>
</form>
</div>
