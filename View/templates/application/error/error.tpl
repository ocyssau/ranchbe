{include file="layouts/htmlheader.tpl"}

<div id="tiki-main">
  <div id="tiki-mid"> <br />
    <div class="cbox">
      <div class="cbox-title">
        {tr}Error{/tr}
      </div>
      <div class="cbox-data"> <br />
        <ul>
        {foreach item=item from=$msg}
          <li>{$item}</li>
        {/foreach}
        </ul>
        <br /><br />
        <a href="javascript:history.back()" class="linkmenu">{tr}Go back{/tr}</a> |
        <a href="{$baseurl}/application/home/index" class="linkmenu">{tr}Return to home page{/tr}</a> |
        <br /><br />
      </div>
    </div>
  </div>
</div>
</body>
