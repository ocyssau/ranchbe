{literal}
<script type='text/javascript'>
<!--
function closeMbox(id){
  element = document.getElementById(id);
  element.style.visibility = 'hidden';
  return true;
}
//-->
</script>
{/literal}

<div class="mbox" id="executionReport">
  <div class="mbox-title">{tr}Execution report{/tr}</div>
  <img class="mbox-closebutton" src="{$baseurl}/img/icons/cross.png" onClick="closeMbox('executionReport');" title="{tr}Close{/tr}">

  <div class="mbox-data">
  {section name=errors loop=$errors}
    {if $errors[errors].level == Fatal}
      <p class="fatal-text">
      <img border="0" alt="FATAL" src="{$baseurl}/img/fatal60.png" />
      {$errors[errors].message}</p>
    {elseif $errors[errors].level == Error}
      <p class="error-text">
      <img border="0" alt="ERROR" src="{$baseurl}/img/error60.png" />
      {$errors[errors].message}</p>
    {elseif $errors[errors].level == Warning}
      <p class="warning-text">
      <img border="0" alt="WARNING" src="{$baseurl}/img/warning60.png" />
      {$errors[errors].message}</p>
    {else}
      <p class="info-text">
      <img border="0" alt="INFO" src="{$baseurl}/img/info60.png" />
      {$errors[errors].message}</p>
    {/if}
    {if $debug}
      <p class="debug-text">
      <b>Error level</b> :  {$errors[errors].level}<br />
      <b>Code</b> :         {$errors[errors].code}<br />
      <b>For package</b> :  {$errors[errors].package}<br />
      <b>In class</b> :     {$errors[errors].context.class}<br />
      <b>In function</b> :  {$errors[errors].context.function}<br />
      <b>In file</b> :      {$errors[errors].context.file}<br />
      <b>In line</b> :      {$errors[errors].context.line}<br />
      </p>
    {/if}
  {/section}
  {if $close_button}
    <input type="button" onclick="window.close()" value="{tr}Close Window{/tr}" title="{tr}Close Window{/tr}">
  {/if}
  {if $back_button}
    <input type="button" onclick="javascript:history.back()" value="{tr}Go back{/tr}" title="{tr}Go back{/tr}">
  {/if}
  {if $home_button}
    <form action="{$baseurl}/application/home/index"><input type="submit" value="{tr}Return to home page{/tr}" title="{tr}Return to home page{/tr}"></form>
  {/if}
  </div>
</div>
