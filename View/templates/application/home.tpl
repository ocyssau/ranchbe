{*Smarty template*}

<div id="page-bar" class="panel panel-default"><div class="panel-body">
   <a class="btn btn-default btn-xs" href="{$baseurl}/message/mailbox/index">{tr}Messages{/tr}</a>
   <a class="btn btn-default btn-xs" href="{$baseurl}/search/index">Recherche</a>
</div></div>

{*-----------Favorites--------------------*}
{if $favorites }
  <p></p>
  <div class="rbbox">
	{include file='container/favorites.tpl'}
  </div>
  <p></p>
{/if}

<div class="jumbotron" style="margin:0;padding:0;">
	<h1><img src="{$baseurl}/{$logo}">{tr}Welcome{/tr} {$current_user_name},
	<small>{tr}You are connected on{/tr} <i>RANCHBE</i>&#169; {$ranchbe_version} {$ranchbe_build}</small>
	</h1>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	{include file='application/home/quoideneuf.tpl'}
</div></div>
	
</div>

<div class="rbbox panel panel-default">
	{include file='application/home/messages.tpl'}
</div>
