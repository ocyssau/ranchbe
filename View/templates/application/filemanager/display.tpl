{*Smarty template*}

<h1 class="pagetitle">{$fileManager_pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div id="searchbar">
{sameurlpost}
<table>
  <form id="filterf" action="{$smarty.server.REQUEST_URI}" method="post">
<tr>
    <td><small>{tr}find{/tr}</small></td>
    <td><small>{tr}In{/tr} :</small></td>
    <td></td>
</tr>
<tr>
  <td ><input size="16" type="text" name="find" value="{$find|escape}" /></td>
  <td >
  	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
  	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
  	{foreach from=$all_field item=name key=field}
       <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
  	{/foreach}
  	</select>
  </td>
  <td ><input type="submit" name="filter" value="{tr}filter{/tr}" /></td>
  </form>
<td>
  <form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
    {sameurlpost}
    <input type="hidden" name="offset" value="0" />
    <input type="hidden" name="sort_field" value="" />
    <input type="hidden" name="sort_order" value="" />
    <input type="hidden" name="where" value="" />
    <input type="hidden" name="find" value="" />
    <input type="hidden" name="find_field" value="" />
    <input type="hidden" name="numrows" value="" />
    <input type="hidden" name="request_page" value="SQL_request" />
    <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
  </form>
</td>
</tr>
</table>	
</div>

{* -------------------Pagination------------------------ *}


{literal}
<script language='Javascript' type='text/javascript'>
<!--
function newNameInput(monlien,file_name,title){
  var newName = prompt(title, file_name );
  if(newName == null){
    document.location.href = '#';
    return false;
  }
  monlien.search = monlien.search+'&newName='+newName;
  //alert(monlien.href);
  document.location.href = monlien.href;
  return true;
}
//-->
</script>
{/literal}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$fileManager_scripts}">
<table class="normal table table-bordered">
<tr>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	</th>
	
	<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name&displayMd5={$displayMd5}">
	{tr}File name{/tr}</a></th>
	
	<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime&displayMd5={$displayMd5}">
	{tr}mtime{/tr}</a></th>
	
	<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size&displayMd5={$displayMd5}">
	{tr}File size{/tr}</a></th>
</tr>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>


{literal}
<script language='Javascript' type='text/javascript'>
<!--
function toggleLine(id,img){
  element = document.getElementById(id);
  if(element.style.height == '20px'){
    element.style.height = 'auto';
    img.src = 'img/untoggle.png';
  }else{
    element.style.height = '20px';
    img.src = 'img/toggle.png';
  }
  //alert('le champ a pour valeur : "'+element.style.height+"'");
  return true;
}

function toggleAllLine(){
  alert('all lines');
  return true;
}
//-->
</script>
{/literal}

    <td class="thin">
    <div id="{$list[list].natif_file_name}" style="overflow:hidden;height:20px;margin:0;padding:0;">
      <a href="{$fileManager_scripts}&action=download&checked[]={$list[list].file_name}" title="{tr}Download{/tr}">
        {file_icon extension=$list[list].file_extension}
      </a>
      <a href="{$fileManager_scripts}&action=executeQueryFile&checked={$list[list].file_name}" title="{tr}Execute query{/tr}">
        {$list[list].natif_file_name}
      </a>
        <img src="img/toggle.png" title="{tr}toggle action{/tr}" alt="{tr}actions{/tr}" onclick="toggleLine('{$list[list].natif_file_name}',this)">
      <br />

      <!-- Action hide menu -->
      <ul>
        <li>
          <a href='{$fileManager_scripts}&action=renameQueryFile&checked={$list[list].file_name}&ticket={$ticket}' title='{tr}Rename{/tr}'
           onClick="newNameInput(this,'{$list[list].file_name}','{tr}Rename: input the new file name{/tr}');return false;">
          {tr}Rename{/tr}
          </a>
        </li>
        <li>
          <a href="{$fileManager_scripts}&action=copyQueryFile&checked={$list[list].file_name}&ticket={$ticket}" title="{tr}Copy{/tr}"
           onClick="newNameInput(this,'{$list[list].file_name}','{tr}Copy: input the new file name{/tr}');return false;">
          {tr}Copy{/tr}
          </a>
        </li>
        <li>
          <a href="{$fileManager_scripts}&action=editQueryFile&checked={$list[list].file_name}&ticket={$ticket}" title="{tr}Edit{/tr}">
          {tr}Edit{/tr}
          </a>
        </li>
      </ul>

    </div>
    </td>

    <td class="thin">{$list[list].file_mtime|date_format}</td>
    <td class="thin">{$list[list].file_size|filesize_format}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

<tr>
<td class="thin">
<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
</td>
<td class="form" colspan="18"><label for="clickall">{tr}select all{/tr} : </label></td></tr>

<br>

<div id="action-menu">

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppressQueryFile" title="{tr}Suppress{/tr}" id="01"
 onclick="if(confirm('{tr}Do you want really suppress this files{/tr}'))
 {ldelim}document.checkform.action='{$fileManager_scripts}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="DownloadZip" title="{tr}Download zip{/tr}" id="05"
 onclick="document.checkform.action='{$fileManager_scripts}'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/document/document_zip.png" title="{tr}Download zip{/tr}" alt="{tr}Download zip{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="update" title="{tr}Refresh{/tr}" id="O4"
 onclick="document.checkform.action='{$fileManager_scripts}'; pop_no(checkform)">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

</div>

<p></p>

{sameurlpost}
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="request_page" value="SQL_request" />
<input type="hidden" name="displayMd5" value="{$displayMd5}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>


{* ------------------- Upload section ------------------------ *}
<fieldset>
<legend align="top"><i>{tr}Add a query file{/tr}</i></legend>
<form action="{$fileManager_scripts}" method="post" name="upload" enctype="multipart/form-data">
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" />
</fieldset>
{sameurlpost}
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="displayMd5" value="{$displayMd5}" />
</form>
