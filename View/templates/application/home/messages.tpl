  <h2>{tr}New messages{/tr}</h2>
  
  <table class="normal table table-bordered">
    <tr>
    <thead>
      <th class="heading" >&nbsp;</th>
      <th class="heading sortable" data-field="user_from">{tr}sender{/tr}</a></th>
      <th class="heading sortable" data-field="subject">{tr}subject{/tr}</a></th>
      <th class="heading sortable" data-field="date">{tr}date{/tr}</a></th>
      <th class="heading sortable" data-field="size">{tr}size{/tr}</a></th>
    </tr></thead>
    
    {cycle values="odd,even" print=false}
    {section name=user loop=$items}
    <tr><tbody>
      <td class="prio{$items[user].priority}">{if $items[user].isFlagged eq 'y'}<img src="{$baseurl}/img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />{/if}</td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].user_from}</td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}"><a class="readlink" href="{$baseurl}/messu-read.php?offset={$offset}&amp;flag={$flag}&amp;priority={$items[user].priority}&amp;flagval={$flagval}&amp;sort_mode={$sort_mode}&amp;find={$find}&amp;msgId={$items[user].msgId}">{$items[user].subject}</a></td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>
      <td  style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
    </tbody></tr>
    
    {sectionelse}
    <tr><td colspan="6">{tr}No messages to display{/tr}</td></tr>
    {/section}
  </table>
