{*Smarty template*}

{literal}
<script>
function paginatorNext(element){
	var page=$('#paginatorpage').val();
	page++;
	$('#paginatorpage').val(page);
}

function paginatorPrev(element){
	var page=$('#paginatorpage').val();
	if(page>1){
		page--;
	}
	$('#paginatorpage').val(page);
}

function paginatorSubmit(element, formSelector){
	var limit = $(element).parents('.paginator').first().find("[name='paginatorlimit']").first().val();
	var page = $(element).parents('.paginator').first().find("[name='paginatorpage']").first().val();
	$(formSelector).append('<input type="hidden" name="paginatorlimit" value="'+limit+'">');
	$(formSelector).append('<input type="hidden" name="paginatorpage" value="'+page+'">');
	$(formSelector).submit();
}

</script>
{/literal}

{* -------------------Pagination------------------------ *}
<div class="paginator mini" align="center">
	<label for="paginator-limit"><small>{tr}{$paginatorForm.paginatorlimit.label}{/tr}</small></label>
	{$paginatorForm.paginatorlimit.html}
	
	<label for="paginatorprev"><small>{tr}{$paginatorForm.paginatorprev.label}{/tr}</small></label>
	{$paginatorForm.paginatorprev.html}
	
	<label for="paginatorpage"><small>{tr}{$paginatorForm.paginatorpage.label}{/tr}</small></label>
	{$paginatorForm.paginatorpage.html}
	
	<label for="paginatornext"><small>{tr}{$paginatorForm.paginatornext.label}{/tr}</small></label>
	{$paginatorForm.paginatornext.html}
</div>
