{literal}
<style>
</style>

<script>

/**
 * Object of assocfile tab manager. Define method to trigger actions and encapsulate properties of current page.
 */
function associatedFile(){
	this.options = {
			baseurl:'',
			documentId:'',
			space:'',
			uploadUrl:''
	};
	
	this.setOption = function(name,value){
		eval("this.options."+name+"='"+value+"'");
		return this;
	};
	
	this.setOptions = function(options){
		this.options = options;
		return this;
	};
	
	this.ping = function(){
		console.log("hello!")
		};
		
	this.action = function(button, url,successMsg,erroMsg,confirmMsg,successcallback){
		if(confirmMsg != ""){
			if(!confirm(confirmMsg)){
				return false;
			}
		}
		
		//retrieve the parent form
		var datastring = $(button).parents("form").serialize();
		console.log( datastring );
		
		$.ajax({
	           type: "POST",
	           url: url,
	           data: datastring,
	           dataType: "json",
	           success: function(data){
	        	    //alert("success");
					document_detail_reload("assocfiles");
	           },
	           error: function(data){
	        	   //alert("error");
	        	   this.ajaxErrorToMsg(data);
	           }
	       });
		return false;
	}
	
	this.suppressfile = function(button){
		var url="/"+this.options.baseurl+"/document/detail/suppresslink";
		var successMsg="";
		var erroMsg="Can not delete this file. Check errors and retry";
		var confirmMsg="Do you want really suppress this file links";
		return this.action(button, url,successMsg,erroMsg,confirmMsg);
	}
};

var assocfile = new associatedFile();
assocfile.ping();
assocfile.setOption('space', 'workitem').setOption('baseurl','rbsier');
console.log(assocfile.options.space);
console.log(assocfile.options.baseurl);

var options = {
		baseurl:'rbsier',
		documentId:150,
		space:'workitem',
		uploadUrl:'/document/detail/index'
};
assocfile.setOptions(options);
console.log(assocfile.options.space);
console.log(assocfile.options.baseurl);

//pas correct :
/*
function myObject(){
	function maMethode(){
		alert("ok");
	}	
}
var my=new myObject();
my.maMethode();
*/

//pas correct
/*
function myObject2(){
	maMethode:function(){
		alert("ok");
	}	
}
var my=new myObject2();
my.maMethode();
*/

//correct
function myObject3(){
	this.maMethode=function(){
		alert("ok");
	}	
}
var my = new myObject3();
my.maMethode();


</script>
{/literal}
