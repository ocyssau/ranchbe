<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery-ui/1.9.2/jquery-ui.min.js"></script>

<script src="{$baseurl}/js/jquery.jsPlumb-1.7.5-min.js"></script>
<script src="{$baseurl}/js/jquery.editinplace.js"></script>

<link rel="stylesheet" href="{$baseurl}/bootstrap/css/bootstrap.min.css">
<script src="{$baseurl}/bootstrap/js/bootstrap.min.js"></script>

{literal}

<script>
jsPlumb.ready(function() {
	$(".activity").css('position', 'absolute');
	jsPlumb.draggable($(".activity"), {
		grid : [ 20, 20 ]
	});
	
	jsPlumb.importDefaults({
		DragOptions :{
			cursor : 'pointer',
			zIndex : 2000
		},
		ConnectionOverlays : [
		["Arrow", {location : 1}],
		["Label", {
			location : 0.5,
			id : "label",
			cssClass : "connection-label"
		}]
		],
		Container : "workflow-graph"
	});
	
	var basicType = {
			connector : "StateMachine",
			paintStyle : {
				strokeStyle : "red",
				lineWidth : 4
			},
			hoverPaintStyle : {
				strokeStyle : "blue"
			},
			overlays : [ "Arrow" ]
		};
	jsPlumb.registerConnectionType("basic", basicType);
	
	// this is the paint style for the connecting lines..
	var connectorPaintStyle = {
		lineWidth : 4,
		strokeStyle : "#61B7CF",
		joinstyle : "round",
		outlineWidth : 2
	};
	// .. and this is the hover style.
	var connectorHoverStyle = {
		lineWidth : 4,
		strokeStyle : "#216477",
		outlineWidth : 2,
	};
	var endpointHoverStyle = {
		fillStyle : "#216477",
		strokeStyle : "#216477"
	};

	var endpointSourceOption = {
		isSource : true,
		isTarget : false,
		maxConnections : 10,
		endpoint : "Dot",
		connector :"Flowchart",
		connectorHoverStyle : connectorHoverStyle,
	};

	var endpointTargetOption = {
		isSource : false,
		isTarget : true,
		maxConnections : 10,
		endpoint : "Rectangle",
		paintStyle : {
			fillStyle : "#7AB02C",
			radius : 11
		},
		maxConnections : -1,
	};
	
	var params = {id:"activity1",positionx:50,positiony:50,label:"activity1",type:"activity"};
	addActivity(jsPlumb, params);

	//bind event on creation of connection
	jsPlumb.bind("connection", function(info){
		var connection = info.connection;
		connection.bind("click", function(conn, originalEvent) {
			var status = prompt("Please enter the status fot this connection", "Status");
			connection.getOverlay("label").setLabel(status);
		});
	});
	
	$(".save-btn").click(function(e){
		return saveToJson(jsPlumb);
	});
	
	$(".add-activity-btn").click(function(e){
		$('#add-activity-modal').modal({});
	});
	
	$(".add-activity-save").click(function(e){
		var id = $("#add-activity-name").val();
		var type = $("#add-activity-type").val();
		if(id != null){
			var params = {id:id,positionx:50,positiony:50,label:id, type:type};
			addActivity(jsPlumb, params);
			$('#add-activity-modal').modal('hide');
		}
		return false;
	});
	
	function initActivityButton(parent){
		parent.find(".activity-script-btn").click(function(e){
			var activity = $(this).parents(".activity").first();
			alert(activity.attr('id'));
		});
		
		parent.find(".activity-template-btn").click(function(e){
			var activity = $(this).parents(".activity").first();
			alert(activity.attr('id'));
		});
		
		parent.find(".activity-delete-btn").click(function(e){
			jsPlumb.remove($(this).parents(".activity").first());
		});
	};
	
	function addActivity(jsPlumb, params){
		var id = params.id;
		var label = params.label;
		var positionx = params.positionx;
		var positiony = params.positiony;
		var type = params.type;
		
		if(id==""){
			alert("Id must be set!");
			return;
		}
		var activity = $("#activity-template").clone().appendTo("#workflow-graph");
		
		activity.show();
		activity.css('position', 'absolute');
		activity.css('top', positiony);
		activity.css('left', positionx);
		activity.removeClass("activity-template");
		activity.addClass("activity jsplumb");
		activity.attr("id", id);
		activity.find("span.activity-label").first().html(label);
		activity.find("span.activity-label").first().editInPlace({
			callback : function(unused, enteredText) {
				enteredText = $.trim(enteredText);
				return enteredText;
			},
			bg_over : "#cff",
			field_type : "text",
			textarea_rows : "3",
			textarea_cols : "15"
		});
		
		switch(type){
			case "activity":
				activity.addClass("activity-activity");
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'Top'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "split":
				activity.addClass("activity-split");
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'Top'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomLeft'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomRight'});
				break;
			case "join":
				activity.addClass("activity-join");
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'TopLeft'});
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'TopRight'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "switch":
				activity.addClass("activity-switch");
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'Top'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomCenter'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'Left'});
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'Right'});
				break;
			case "start":
				activity.addClass("activity-start");
				jsPlumb.addEndpoint(id, endpointSourceOption, {anchor:'BottomCenter'});
				break;
			case "end":
				activity.addClass("activity-end");
				jsPlumb.addEndpoint(id, endpointTargetOption, {anchor:'Top'});
				break;
		}
		
		jsPlumb.draggable(activity, {
			grid : [ 20, 20 ]
		});
		initActivityButton(activity);
	}
	
	function saveToJson(jsPlumb){
		var activities = [];
		$(".activity").each(function (idx, elem) {
		    var $elem = $(elem);
		    activities.push({
		    	id: $elem.attr('id'),
		        positionx: parseInt($elem.css("left"), 10),
		        positiony: parseInt($elem.css("top"), 10),
		        label: $elem.find("span.activity-label").first().html()
		    });
		});
	    
	    var connections = [];
	    $.each(jsPlumb.getConnections(), function (idx, connection) {
	        connections.push({
	            connectionid: connection.id,
	            pageSourceid: connection.sourceId,
	            pageTargetid: connection.targetId
	        });
	    });
	    console.log(activities);
	    console.log(connections);
	    console.log(JSON.stringify({activities:activities, connections:connections}));
	}
});
</script>

<style>
div.activity-label {
    background-color: white;
    padding: 0.4em;
	text-align: center;
	text-transform: uppercase;
    font: 20px sans-serif;
}

div.activity-activity {
	border: 5px solid red;
	width: 200px;
	height: 100px;
}

div.activity-join{
	width: 0;
	border-top: 30px solid #6C6;
	border-left: 52px solid transparent;
	border-right: 52px solid transparent;
	width: 200px;
	height: 100px;
}

div.activity-split{
	width: 0;
	border-bottom: 30px solid #6C6;
	border-left: 52px solid transparent;
	border-right: 52px solid transparent;
	width: 200px;
	height: 100px;
}

div.activity-switch{
	width: 200px;
	height: 100px;
}

div.activity-start{
	width: 200px;
	height: 100px;
}

div.activity-end{
	width: 200px;
	height: 100px;
}

.jsplumb-drag {
    border: 4px solid pink;
}

path, ._jsPlumb_endpoint {
    cursor: pointer;
}

.connection-label {
    background-color: white;
    padding: 0.4em;
    font: 12px sans-serif;
    color: #444;
    z-index: 21;
    border: 1px dotted gray;
    opacity: 0.8;
    filter: alpha(opacity=80);
    cursor: pointer;
}

.connection-label._jsPlumb_hover {
    background-color: #5C96BC;
    color: white;
    border: 1px solid white;
}

.workflow-graph{
	border: 1px solid pink;
	width: 800px;
	height: 600px;
	overflow: hide;
}

</style>

<div id="activity-template" class="activity-template" style="top: 0; left: 0; display:none;">
	<div style="">
		<div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
			<button class="btn btn-success activity-script-btn" title="Edit Script"><span class="glyphicon glyphicon-file"></span></button>
			<button class="btn btn-success activity-template-btn" title="Edit Template"><span class="glyphicon glyphicon-blackboard"></span></button>
			<button class="btn btn-danger activity-delete-btn" title="Delete"><span class="glyphicon glyphicon-remove"></span></button>
		</div>
	</div>
	<div class="activity-label">
		<span class="activity-label">1</span>
	</div>
</div>

<body>
	<div id="workflow-graph" class="workflow-graph">
	</div>
	
	<button class="save-btn">save</button>
	<button class="add-activity-btn">add</button>
</body>


</html>
{/literal}

<!-- Modal -->
<div class="modal fade" id="add-activity-modal" tabindex="-1" role="dialog" aria-labelledby="Add Activity" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Activity</h4>
      </div>
      <div class="modal-body">
		<div id="dialog-form" title="Add a Activity">
			<form>
				<fieldset>
				<label for="name">Id</label>
				<input type="text" name="name" id="add-activity-name" value="" class="text ui-widget-content ui-corner-all">
				
				<label for="type">Type</label>
				<select name="type" id="add-activity-type" class="text ui-widget-content ui-corner-all">
				<option value="start">Start</option>
				<option value="activity" selected>Activity</option>
				<option value="switch">Switch</option>
				<option value="split">Split</option>
				<option value="join">Join</option>
				<option value="end">End</option>
				</select>
				</fieldset>
			</form>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary add-activity-save">Save</button>
      </div>
    </div>
  </div>
</div>
