{*Smarty template*}

<div class="container">
<h2>{tr}{$pageTitle}{/tr}</h2>

<form {$form.attributes}>
{$form.hidden}

<ul class="list-group">
{section name=i start=0 loop=$count step=1}
	<li class="list-group-item">{$form.checked[$smarty.section.i.index].html}{$form.checked[$smarty.section.i.index].label}</li>
{/section}
</ul>

<table class="form normal table">
	<tr>
		<td class="formcolor">{$form.tocontainerid.label}:</td>
		<td class="formcolor">{$form.tocontainerid.html}
		<p class="help">{tr}Let blank to put new version in the same container of previous version{/tr}</p>
		</td>
	</tr>
	
	<tr>
		<td class="formcolor">{$form.version.label}:</td>
		<td class="formcolor">{$form.version.html}
		<p class="help">{tr}Let blank to set the next version from current version+1{/tr}</p>
		</td>
	</tr>
	
	 {if not $form.frozen}
	<tr>
		<td class="formcolor" colspan=2>
		<div class="btn-group">
		{$form.cancel.html}{$form.validate.html}
		</div></td>
	</tr>
	 {/if}
	
	<tr>
	 <td class="formcolor" colspan=2>{$form.requirednote}</td>
	</tr>

</table>
</form>
</div>
