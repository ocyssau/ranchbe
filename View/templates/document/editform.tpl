{*Smarty template*}
<div class="container">

<h1 class="pagetitle">{$pageTitle}</h1>

<ul class="rb-doclist list-group">
{foreach from=$documents key="key" item="doc"}
	<li class="list-group-item">{$doc.name}.v{$doc.version}.{$doc.iteration} {$doc.description}</li>
{/foreach}
</ul>


<form {$form.attributes}>
{$form.hidden}

<table class="normal table table-bordered">

<tr>
	<td colspan=2>{$form.help.html}</td>
</tr>

{if $form.number}
<tr>
	<td>{tr}{$form.number.label}{/tr}:</td>
	<td>{$form.number.html}</td>
</tr>
{/if}

{if $form.name}
<tr>
	<td>{tr}{$form.name.label}{/tr}:</td>
	<td>{$form.name.html}</td>
</tr>
{/if}

<tr>
	<td>{tr}{$form.description.label}{/tr}:</td>
	<td>{$form.description.html}</td>
</tr>

<tr>
	<td>{tr}{$form.categoryId.label}{/tr}:</td>
	<td>{$form.categoryId.html}</td>
</tr>

<!-- Display optionnals fields -->
{section name=of loop=$extended}
	{assign var="fn" value=$extended[of].appName}
	<tr class="formcolor">
		<td>{tr}{$form.$fn.label}{/tr}:</td>
		<td>{$form.$fn.html}</td>
	</tr>
{/section}

{if $form.lock}
<tr>
	<td>{tr}{$form.lock.label}{/tr}:</td>
	<td>{$form.lock.html}</td>
</tr>
{/if}

{if $form.file}
<tr>
	<td>{tr}{$form.file.label}{/tr}:</td>
	<td>{$form.file.html}</td>
</tr>
{/if}

{if not $form.frozen}
<tr>
	<td colspan=2>{$form.cancel.html}&nbsp;{$form.validate.html}</td>
</tr>
{/if}

<tr>
	<td colspan=2>{tr}{$form.requirednote}{/tr}</td>
</tr>

</table>
<input type="hidden" name="basecamp" value="{$basecamp}">
</form>


{foreach key=name item=error from=$form.errors}
	<b>Collected Errors:</b> <br />
	<font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}

</div>
