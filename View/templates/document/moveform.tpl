{*Smarty template*}

<h2>{tr}{$pageTitle}{/tr}</h2>

<form {$form.attributes}>
{$form.hidden}

<fieldset>
<ul class="rb-doclist list-group" title="documents to move">
{section name=list loop=$documents}
	<li class="list-group-item">{$documents[list].name}</li>
{/section}
</ul>
</fieldset>

<fieldset>
{$form.containerid.label}
{$form.containerid.html}
</fieldset>

{if not $form.frozen}
{$form.cancel.html}
{$form.validate.html}
{/if}

</form>
