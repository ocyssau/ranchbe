{*Smarty template*}

{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <!-- Display fields -->
    {foreach item=field_name from=$fields}
      <input type="hidden" name="fields[{$smarty.section.list.index}]" value="{$field_name}" />
      <th class="heading">
      {tr}{$field_name}{/tr}</a></th>
    {/foreach}

    <th class="heading">
    {tr}Result and Actions{/tr}</a></th>
 </tr>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    {foreach item=field_name from=$fields}
      {if $field_name eq category_id}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].category_id}" readonly="1"> ({filter_select id=$list[list].$field_name type=category})
      {elseif $field_name eq document_indice_id}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].document_indice_id}" readonly="1"> ({filter_select id=$list[list].$field_name type=document_indice})
      {elseif $field_name eq document_number}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].document_number}" readonly="1">
      {elseif $field_name eq file}
      <td class="thin"><input type="text" name={$field_name}[{$smarty.section.list.index}]" value="{$list[list].file}" readonly="1">
      {else}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].$field_name}">
      {/if}
      <br><i>Currently:{$currentdata[$smarty.section.list.index].$field_name}</i>
      </td>
    {/foreach}

    <td>
    <br />
    <font color="brown"><i> <!--Errors-->
      {foreach item=error from=$list[list].errors}
        - {$error}<br />
      {/foreach}</i>
    </font>
    {if $list[list].doctype.doctype_number}<!--Doctype-->
    <font color="green"><i>Recognize doctype : <b>{$list[list].doctype.doctype_number}</b></i><br /></font>{/if}
    <!--Actions-->
    <br />
  	<select name="docaction[{$smarty.section.list.index}]">
        {foreach item=action key=return from=$list[list].actions}
          <option value="{$return}">{$action}</option>
        {/foreach}
    </select>
    <br /><br />
   </td>
   </tr>

   <input type="hidden" name="loop" value="{$smarty.section.list.loop}" />
   <input type="hidden" name="documentid[{$smarty.section.list.index}]" value="{$list[list].document_id}" />
   <input type="hidden" name="fileid[{$smarty.section.list.index}]" value="{$list[list].file_id}" />
  {sectionelse}
    <tr><td colspan="6">{tr}Nothing to display{/tr}</td></tr>
  {/section}
  </table>
  <br />
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="hidden" value="{$cvsfile}" name="cvsfile" />
  <input type="submit" value="validatecsv" name="action" />
  <input type="submit" value="cancel" name="action" />
</form>
