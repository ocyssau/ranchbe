<div class="container">

<h2>{tr}{$pageTitle}{/tr}</h2>

<div class="help"><p>{$help}</p></div>

<div class="panel panel-default">
	<div class="alert alert-warning">{$warning}</div>

	<div class="panel-body">
		<form {$form.attributes}>
		{$form.hidden}
		
		<ul class="list-group">
		{foreach from=$docfileIds item=docfileId}
			<li class="list-group-item">
			{$form.ifExistmode[$docfileId].label}
			{$form.ifExistmode[$docfileId].html}
			</li>
		{/foreach}		
		</ul>
		
		{$form.validate.html}
		{$form.cancel.html}
		</form>
	</div>
</div>

</div>