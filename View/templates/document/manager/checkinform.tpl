<script src="{$baseurl}/js/rb/rb.rbconverter.js"></script>

<script>
var converterServer = "{$rbgateServerUrl}";
{literal}
$(function() {
	$('input[name="validate"]').click(function(e){
		e.preventDefault();
		
		var files=[];
		$('input[name="files[]"]').each(function(index){
			var filename = $(this).val();
			files[index]=filename;
		});
		
		console.log(files);
		var form = $("#checkinform");
		form.append('<input type="hidden" value="validate" name="validate">');
		var converter = new rbconverter(form);

		converter.setOption('rbconverterUrl', converterServer+'/fromfiles');
		converter.convertAndSubmit(files);
		return false;
	});
	
	$('input[name="preparedata"]').click(function(e){
		e.preventDefault();
		
		var files=[];
		$('input[name="files[]"]').each(function(index){
			var filename = $(this).val();
			files[index]=filename;
		});
		
		console.log(files);
		
		var form = $("#checkinform");
		var converter = new rbconverter(form);
		converter.setOption('rbconverterUrl', converterServer+'/fromfiles');
		converter.convertAndSee(files);
		return false;
	});
	
});
</script>
{/literal}

<div class="container">

<h2>{tr}{$pageTitle}{/tr}</h2>

<div class="help"><p>{$help}</p></div>

<div class="panel panel-default">
	<div class="alert alert-warning">{$warning}</div>

	<div class="panel-body">
		<form {$form.attributes}>
		{$form.hidden}
		
		<ul class="list-group">
		{foreach from=$documentIds item=documentId}
			<li class="list-group-item">
			{$form.checked[$documentId].html}
			{$form.checked[$documentId].label}
			</li>
		{/foreach}
		</ul>
		
		<div class="form-group">
			{$form.comment.label}
			{$form.comment.html}
		</div>

		<p>
		<div class="form-group">
		{$form.cancel.html}
		{$form.preparedata.html}
		{$form.validate.html}
		</div>
		</p>
		</form>
	</div>
</div>
</div>

