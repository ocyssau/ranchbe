{*Smarty template*}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{tr}{$form.header.infos}{/tr}</h2>

<table class="normal table table-bordered">

  <tr class="formcolor">
    <td>{tr}{$form.indice.label}{/tr}:</td>
    <td>{$form.indice.html}</td>
  </tr>

 {if not $form.frozen}
  <tr class="formcolor">
    <td> </td>
    <td>{$form.reset.html}&nbsp;{$form.action.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>

<input type="hidden" name="page_id" value="DocManage_changeindice" />
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{foreach key=name item=error from=$form.errors}
 <b>Collected Errors:</b> <br />
 <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
