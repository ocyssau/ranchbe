{*Smarty template*}

<h2>{tr}Select the file to associate to {/tr}{$documentUid}</h2>

<form {$form.attributes}>
{$form.hidden}

<div class="help">
<p>
You may choose a file to associate to document.
The file must be in wildspace and must have the name of the document in his name (at end, at begin, in middle, anycases is ok).
<p>

<p>
You must set too a Role.
Role determine some functionalities for docfile :
<ul>
	<li>MAIN = the file is the main docfile of document. It use to determine doctype and will be put in wildspace when checkout is mandated.
		Only one MAIN docfile by document is required.
	</li>
	<li>FRAGMENT = Is part of document and will be put in wildspace when checkout is mandated.</li>
	<li>ANNEX = Is part of document and will be put in wildspace when checkout is mandated.</li>
	<li>PICTURE = Is used to create a 2d representation of document and generates thumbnails. NOT GET WHEN DOCUMENT IS CHECKOUT but may be updated when checkin.</li>
	<li>VISU = Is used to create a 2d/3d representation of document. NOT GET WHEN DOCUMENT IS CHECKOUT but may be updated when checkin.</li>
	<li>CONVERT = is the document MAIN or FRAGMENT or ANNEX in other format. NOT GET WHEN DOCUMENT IS CHECKOUT but may be updated when checkin.</li>
</ul>

<p>
<em>If you dont know what to do, choose ANNEX.</em>
</p>

<p>
</div>

<div class="form-group">
	{$form.file.label}
	{$form.file.html}
</div>

<div class="form-group">
	{$form.mainrole.label}
	{$form.mainrole.html}
</div>


{if not $form.frozen}
<div class="form-group">
	{$form.cancel.html}
	{$form.validate.html}
</div>
{/if}

<div class="form-group">{$form.requirednote}</div>

</form>
