<div class="container">

<h2>{tr}{$pageTitle}{/tr}</h2>

<div class="panel panel-default">

	<div class="panel-body">
		<form {$form.attributes}>
		{$form.hidden}
		
		<div class="form-group">
		{$form.phpscript.label}
		{$form.phpscript.html}
		</div>
		
		<div class="form-group">
		{$form.format.csv.label}
		{$form.format.csv.html}
		
		{$form.format.xls.label}
		{$form.format.xls.html}
		</div>
		
		<div class="form-group">
		{$form.validate.html}
		{$form.cancel.html}
		</div>
		</form>
	</div>
</div>

</div>