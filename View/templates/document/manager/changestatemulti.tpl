{*Smarty template*}
{if !$completed}

{if $displayHeader eq 1}
  {$form.javascript}
  
<form {$form.attributes}>

<h2>{tr}{$form.header.infos}{/tr}</h2>
{$form.hidden}

{*--------------------list header----------------------------------*}
<table class="normal table table-bordered">
  <tr>
    <th class="heading"></th>
    <th class="heading">{tr}Document{/tr}</th>
    <th class="heading">{tr}Instance{/tr}</th>
    <th class="heading">{tr}Process{/tr}</th>
    <th class="heading">{tr}Current Activity{/tr}</th>
  </tr>
{/if}

{if $displayBody eq 1}
{*--------------------list Body----------------------------------*}
  <input name="checked[]" type="hidden" value="{$documentid}" />
  <input name="iid[{$documentid}]" type="hidden" value="{$ins_info.$documentid.instanceId}" />

  {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  <tr class="{cycle}">
    <td class="thin">
    {if $isChanged eq 1}<b>Success</b>{/if}
    {if $errors.0.message}
      {if $errors.0.level == Fatal}
        <img border="0" alt="FATAL" src="img/fatal30.png" />
        <font color="red" size="1"><b>FATAL ERROR :</b></font><br />
        {assign var=color value=red}
      {elseif $errors.0.level == Error}
        <img border="0" alt="ERROR" src="img/error30.png" />
        <font color="red" size="1"><b>ERROR :</b></font><br />
        {assign var=color value=red}
      {elseif $errors.0.level == Warning}
        <img border="0" alt="WARNING" src="img/warning30.png" />
        <font color="orange" size="1"><b>WARNING :</b></font><br />
        {assign var=color value=orange}
      {else}
        <img border="0" alt="INFO" src="img/info30.png" />
        <font color="green" size="1"><b>INFO :</b></font><br />
        {assign var=color value=green}
      {/if}

      {section name=errors loop=$errors}
        <font color="{$color}" size="1"><b>{$errors[errors].message}</b></font><br />
      {/section}
    {else}
      <img border="0" alt="OK" src="img/accept.png" />
      <font color="green" size="1"><b>SELECTED</b></font>
      {assign var=color value=green}
    {/if}
    
    
    </td>
    <td class="thin">
      <font color="{$color}" size="2"><b>
      {$doc_info.document_number} - {$doc_info.document_indice_id|document_indice}.{$doc_info.document_version}
      </b></font><br />
      <i>{$doc_info.designation}</i><br />
      {$doc_info.document_access_code|access_code}
      <b>{$doc_info.document_state}</b>
    </td>
    {*INFOS ABOUT INSTANCE*}
    <td class="thin">
    {if $ins_info.instanceId}
      <i>{tr}Instance{/tr}</i>: {$ins_info.name}({$ins_info.instanceId})<br />
      <i>{tr}Created{/tr}</i>: {$ins_info.started|date_format}<br />
      <i>{tr}Status{/tr}</i>: {$ins_info.status}<br />
      <i>{tr}Owner{/tr}</i>: {$ins_info.owner}<br />
      <i>{tr}Next user{/tr}</i>: {$ins_info.nextUser}<br />
    {else}
      <i>none</i>
    {/if}
    </td>
  	<td class="thin">{$proc_info.name} - {$proc_info.version}</td>
    {*INFOS ABOUT ACTIVITY*}
    <td class="thin">
    {if $act_info.name}
      <i>{tr}Activity{/tr}</i> :{$act_info.name}<br />
      <i>{tr}Type{/tr}</i>: {$act_info.type}<br />
      <i>{tr}Started{/tr}</i>: {$act_info.started|date_format}<br />
      <i>{tr}Status{/tr}</i>: {$act_info.actstatus}<br />
      <i>{tr}User{/tr}</i>: {$act_info.user}<br />
    {else}
      <i>none</i>
    {/if}
    </td>
  </tr>
{/if}

{if $displayFooter eq 1}
</table>

  {tr}Next activity{/tr}: {$form.activityId.html}
  {$form.validate.html}&nbsp;{$form.cancel.html}

</form>

<table class="normal table table-bordered">
<tr>
	<td colspan="6" align="center">
  <img src="{$graph}" title="{tr}Graph{/tr}" alt="{tr}Graph is not accessible{/tr}"/>
</td>
</tr>	
</table>

{/if}


{******************************************************************************}
{*ACTIVITY COMPLETED*}
{/if}
{if $completed}

<h1>{tr}Activity completed{/tr}</h1>
{$msg}<br />

<form name="close">
  <input type="button" value="No comment" OnClick="javascript:window.location.href='{$start_page|urldecode}'">
</form>

<form method="post">

<table class="normal table table-bordered">
<tr>
	<td class="odd">{tr}Process{/tr}
	<td class="odd">{$procname} {$procversion}</td>
</tr>
<tr>
	<td class="odd">{tr}Activity{/tr}
	<td class="odd">{$actname}</td>
</tr>
<tr>
	<td></td>
</tr>

<tr class="normal">
	<tr>
		<td class="odd" colspan="2">{tr}Comment{/tr}</td>
	</tr>

		<td class="odd" colspan="2">{tr}Subject{/tr}:<input type="text" name="__title" value="{if $post eq 'y'}{$title}{/if}" {if $post eq 'y'}readonly{/if}/></td>
	<tr>
		<td class="odd" colspan="2"><textarea rows="5" cols="60" name="__comment" {if $post eq 'y'}readonly{/if}>{if $post eq 'y'}{$comment}{/if}</textarea></td>
	</tr>
	{if $post eq 'n'}
	<tr><td class="odd" colspan="2">
    <input type="submit" name="save" value="{tr}Save{/tr}" />
    <input type="hidden" name="iid" value="{$iid}" />
    <input type="hidden" name="__post" value="y" />
    <input type="hidden" name="activityId" value="{$actid}" />
    <input type="hidden" name="CnextUsers" value="{$CnextUsers}" />
    <input type="hidden" name="start_page" value="{$start_page}" />
    </form>

  </td></tr>
  {else}
	<tr><td class="odd" colspan="2">
    <form name="close">
      <input type="button" value="End" OnClick="javascript:window.location.href='{$start_page|urldecode}'">
    </form>
  </td></tr>
  {/if}

</tr>
</table>
{/if}


