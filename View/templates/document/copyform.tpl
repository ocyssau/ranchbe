{*Smarty template*}

<h2>{tr}{$pageTitle}{/tr}</h2>

<form {$form.attributes}>
{$form.hidden}

<div class="form-group">
{$form.containerid.label}
{$form.containerid.html}
<br />
<i>{$number_help}</i>
</div>

<div class="form-group">
{$form.newname.label}
{$form.newname.html}
</div>

<div class="form-group">
{$form.version.label}
{$form.version.html}
</div>


{if not $form.frozen}
	<div class="form-group">
	{$form.cancel.html}
	{$form.validate.html}
	</div>
{/if}

<div class="form-group">
{$form.requirednote}
</div>

</form>
