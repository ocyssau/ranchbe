<div class="container">

<h2>{tr}{$pageTitle}{/tr}</h2>

<div class="help"><p>{$help}</p></div>

<div class="panel panel-default">
	<div class="alert alert-warning">{$warning}</div>

	<div class="panel-body">
		<form {$form.attributes}>
		{$form.hidden}
		
		<ul class="list-group">
		{section name=i start=0 loop=$count step=1}
			<li class="list-group-item">{$form.checked[$smarty.section.i.index].html}{$form.checked[$smarty.section.i.index].label}</li>
		{/section}
		</ul>
		
		{$form.cancel.html}
		{$form.validate.html}
		</form>
	</div>
</div>

</div>