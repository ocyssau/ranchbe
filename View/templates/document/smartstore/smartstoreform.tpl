<form {$form.attributes}>
{$form.hidden}

<table>
{$fieldset}
</table>

<p style="padding-top:20px;">
{$form.cancel.html}
{$form.save.html}
</p>

</form>
