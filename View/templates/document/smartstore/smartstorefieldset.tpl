<tr>
<td style="max-width:200px;display:block;">
<label class="control-label">{$form.actions.$loop.label}</label>
<div class="input-group">{$form.actions.$loop.html}</div>
<div class="control-feedback" style="max-width:200px;max-height:50px;overflow-y:scroll;display:block;">{$feedback}</div>
</td>

<td>
<label class="control-label">{$form.file.$loop.label}</label>
<div class="input-group">{$form.file.$loop.html}</div>
</td>

<td>
<label class="control-label">{$form.number.$loop.label}</label>
<div class="input-group">{$form.number.$loop.html}</div>
</td>

<td>
<label class="control-label">{$form.version.$loop.label}</label>
<div class="input-group">{$form.version.$loop.html}</div>
</td>

<td>
<label class="control-label">{$form.description.$loop.label}</label>
<div class="input-group">{$form.description.$loop.html}</div>
</td>

<td>
<label class="control-label">{$form.categoryId.$loop.label}</label>
<div class="input-group">{$form.categoryId.$loop.html}</div>
</td>

{section name=of loop=$extended}
	{assign var="fn" value=$extended[of].appName}
	<td>
	<label class="control-label">{$form.$fn.$loop.label}</label>
	<div class="input-group">{$form.$fn.$loop.html}</div>
	</td>
{/section}

</tr>
