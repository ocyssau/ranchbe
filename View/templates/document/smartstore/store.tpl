{*Smarty template*}

<script>
var converterServer = "{$rbgateServerUrl}";
{literal}
$(function() {
	$('.form-control').not("[readonly]").parent().prev()
		.append('<span class="glyphicon glyphicon-star-empty"></span>')
		.attr('title', 'Click to set all')
		.css('cursor','pointer')
		.click(function(){
			var inputSpan = $(this).next();
			var input = inputSpan.find(':input');
			if(input.attr('readonly')=='readonly'){
				//alert(input.val());
			}
			else{
				var name = input.attr('name');
				var value = input.val();
				var rootname = name.match("([A-Za-z_]+)(\[[0-9]+\])+$")[1];
				$("[name^="+rootname+"]").val(value);
			}
		});
	
	$('input[name="save"]').click(function(e){
		e.preventDefault();
		
		var files=[];
		$("form .file").each(function(index){
			var filename = $(this).val();
			files[index]=filename;
		})
		
		var data = {};
		data.files = files;
		console.log( data );
		
		$.ajax({
			type : 'get',
			url : converterServer+'/fromfiles',
			data : data,
			dataType: 'json',
			timeout: 3000,
		})
		.done(function(data, textStatus, jqXHR){
			var form = $("form[name='form']");
			form.append('<input type="hidden" value="save" name="save">');
			
			var pdmDataElemt = $('<input type="hidden" name="pdmdata">');
			pdmDataElemt.val(JSON.stringify(data));
			form.append(pdmDataElemt);
			
			console.log(data);
			console.log(pdmDataElemt);
			$("form[name='form']").submit();
		})
		.fail(function(jqXHR, textStatus){
			if(jqXHR.responseJSON){
				alert("Conversion Failed");
				console.log(jqXHR.responseJSON);
			}
			else{
				console.log(jqXHR);
				var form = $("form[name='form']");
				form.append('<input type="hidden" value="save" name="save">')
				$("form[name='form']").submit();
			}
		});
		return false;
	});
	
	$('button[name="preparedata"]').click(function(e){
		e.preventDefault();
		
		var files=[];
		$("form .file").each(function(index){
			var filename = $(this).val();
			files[index]=filename;
		})
		
		var data = {};
		data.files = files;
		console.log( data );
		
		$.ajax({
			type : 'get',
			url : converterServer+'/fromfiles',
			data : data,
			dataType: 'json',
			timeout: 3000,
		})
		.done(function(data, textStatus, jqXHR){
			console.log(data);
		})
		.fail(function(jqXHR, textStatus){
			if(jqXHR.responseJSON){
				alert("Conversion Failed");
				console.log(jqXHR.responseJSON);
			}
			else{
				console.log(jqXHR);
			}
		});
		return false;
	});
});
</script>
{/literal}

<div id="{$fragamentid}">
	<h1>{$pageTitle}</h1>
	{$createForm}
	
	<button name="preparedata" class="btn btn-info">Prepare Data</button>
</div>

