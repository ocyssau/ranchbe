{*Smarty template*}

{literal}
<style>
table{
	table-layout: fixed;
}
th, td {
    overflow: hidden;
}
</style>

<script>
$(function() {
	$(".delete-btn").click(function(){
		if(confirm('Are you sure?')){
			var url = document.baseurl+'/document/history/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});
});
</script>
{/literal}

<div id="page-title">
	<h1 class="pagetitle">{tr}Document history{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
<div id="page-filter" class="panel panel-default"><div class="panel-body">
{$filter}
</div></div>

<div id="page-list" class="panel panel-default">
<div class="panel-heading">
{* -------------------Pagination------------------------ *}
{$paginator}
</div>

<div class="panel-body">

{*--------------------listheader----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto" style="width:40px;">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	<div id="displaySelectedRowCount">0</div>
	</th>
	
	<th class="heading sortable" data-field="action_id" style="width:40px;">#</th>
	
	<th class="heading" style="width:150px;">
	<span class="heading sortable" data-field="action_name">{tr}Action{/tr}</span>
	<span class="heading sortable" data-field="action_by">{tr}By{/tr}</span>
	<span class="heading sortable" data-field="action_date">{tr}At{/tr}</span>
	</th>
	
	<th class="heading" style="width:auto;">
	<span class="heading sortable" data-field="data_id">{tr}Id{/tr}</span>
	<span class="heading sortable" data-field="data_name">{tr}Name{/tr}</span>
	<span class="heading sortable" data-field="data_description">{tr}Description{/tr}</span>
	<span class="heading sortable" data-field="data_lifeStage">{tr}Life Stage{/tr}</span>
	<span class="heading sortable" data-field="data_iteration">{tr}Iteration{/tr}</span>
	<span class="heading sortable" data-field="data_version">{tr}Version{/tr}</span>
	</th>
	
	<th class="heading sortable" data-field="action_comment" style="width:200px;">{tr}Comment{/tr}</th>
	
	<th class="heading" style="width:800px;">
	<span class="heading sortable" data-field="data">{tr}Data{/tr}</span>
	</th>
</tr></thead>

<tbody>
{*--------------------list body---------------------------*} 
{cycle	print=false values="even,odd"}
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="selectable">
	<input type="checkbox" name="checked[]" value="{$list[list].action_id}" />
	</td>

	<td class="selectable">{$list[list].action_id}</td>
			
	<td class="selectable">
	<b>{$list[list].action_name}</b>
	<i>by</i> <b>{$list[list].action_ownerId}</b><br />
	{$list[list].action_created|date_format}
	</td>
	
	<td>
	<a href="{$baseurl}/document/detail/display?documentid={$list[list].data_id}&spacename={$spacename}">
	{$list[list].data_name} - {$list[list].data_version|document_indice}.{$list[list].data_iteration}</a>
	<br />
	<i>{$list[list].data_description}</i>
	{$list[list].data_lifeStage}
	</td>
	
	<td class="selectable">
	{$list[list].action_comment}
	</td>
	
	<td class="selectable">
	{$list[list].data}
	</td>
</tr>
{/section}
</tbody>
</table>

<button class="btn btn-default btn-sm delete-btn" name="delete" title="{tr}Suppress{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/trash.png"/>
</button>

<button class="btn btn-default btn-sm" type="submit" name="refresh"
	value="refresh" title="{tr}Refresh{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/refresh.png"/>
</button>

<input type="hidden" name="documentid" value="{$documentid}" />
<input type="hidden" name="spacename" value="{$spacename}" />
</form>
</div></div>
