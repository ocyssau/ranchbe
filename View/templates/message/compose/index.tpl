{*Smarty template*}
{$form.javascript}

<script>
$(function(){ldelim}
var html_areaEditor = new HTMLArea('message_body_id');
var html_areaConfig = html_areaEditor.config;
html_areaConfig.width = '1000px';
html_areaConfig.height = '400px';
html_areaConfig.imgURL = 'img/htmlarea/';
html_areaConfig.popupURL = 'js/htmlarea/popups/';
html_areaConfig.statusBar = false;
html_areaConfig.hideSomeButtons(' popupeditor ');
html_areaEditor.generate();
{rdelim});
</script>

<div class="container">

<h2>{$pageTitle}</h2>

{include file="message/navbar.tpl"}

<form {$form.attributes}>
{$form.hidden}

<table class="normal table table-bordered" >
<tr>
<td style="width:60px;">{$form.to.label}</td>
<td>{$form.to.html}</td>
</tr>

<tr>
<td>{$form.cc.label}</td>
<td>{$form.cc.html}</td>
</tr>

<tr>
<td>{$form.bcc.label}</td>
<td>{$form.bcc.html}</td>
</tr>

<tr>
<td>{$form.priority.label}</td>
<td>{$form.priority.html}</td>
</tr>

<tr>
<td>{$form.subject.label}</td>
<td>{$form.subject.html}</td>
</tr>
</table>

{$form.toall.html}
{$form.bymessage.html}
{$form.bymail.html}

<p>{$form.validate.html}</p>

<table class="normal table table-bordered" >
<tr>
<td>
{$form.body.html}
</td>
</tr>
</table>
</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
{/if}

</div>
