{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form name="mailboxfilter" action="{$baseurl}/message/mailbox/index" method="get">

{$form.hidden}
{sameurlpost}

<fieldset>
	<label for="find_flags"><small>{tr}{$form.find_flags.label}{/tr}</small></label>
	{$form.find_flags.html}
	
	<label for="find_mailprio"><small>{tr}{$form.find_mailprio.label}{/tr}</small></label>
	{$form.find_mailprio.html}
	
	<label for="find"><small>{tr}{$form.find.label}{/tr}</small></label>
	{$form.find.html}
	
	<input type="submit" name="filter" value="{tr}filter{/tr}" />
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</fieldset>

</form>
</div>


