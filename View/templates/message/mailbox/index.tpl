{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	{/literal}
{rdelim});
</script>

{include file="message/navbar.tpl"}

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<table class="normal table table-bordered">
	<thead><tr>
		<td>
			<table border='0' height='20' cellpadding='0' cellspacing='0'
			 width='200' style='background-color:white;'>
				<tr>
					<td style='background-color:red;' width='{$cellsize}'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td><small>{$percentage}%</small></td>
	</tr></thead>
</table>
{if $percentage >= 100}
	{tr}Mailbox is full! Delete or archive some messages if you want to receive more messages.{/tr}
{/if}

<div class="panel panel-default">
<div class="panel-heading">

{*-------------------- list header ---------------------------*}
<form name="mailboxlist" action="#" method="post">
	<input type="hidden" name="offset" value="{$offset|escape}" />
	<input type="hidden" name="find" value="{$find|escape}" />
	<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
	<input type="hidden" name="flag" value="{$flag|escape}" />
	<input type="hidden" name="flagval" value="{$flagval|escape}" />
	<input type="hidden" name="priority" value="{$priority|escape}" />

	<button class="btn btn-default btn-sm" type="submit" name="suppress" value="suppress" title="{tr}Suppress{/tr}"
	onclick="document.mailboxlist.action='{$baseurl}/message/mailbox/delete';pop_no(mailboxlist);return false;">
	{tr}Suppress{/tr}</button>

	<button class="btn btn-default btn-sm" type="submit" name="archive" value="archive" title="{tr}move to archive{/tr}"
	onclick="document.mailboxlist.action='{$baseurl}/message/mailbox/archive';pop_no(mailboxlist);return false;">
	{tr}move to archive{/tr}</button>

	<select name="markas">
		<option value="isRead_y">{tr}Mark as read{/tr}</option>
		<option value="isRead_n">{tr}Mark as unread{/tr}</option>
		<option value="isFlagged_y">{tr}Mark as flagged{/tr}</option>
		<option value="isFlagged_n">{tr}Mark as unflagged{/tr}</option>
	</select>

	<button class="btn btn-default btn-sm" type="submit" name="mark" value="mark" title="{tr}mark{/tr}"
	onclick="document.mailboxlist.action='{$baseurl}/message/mailbox/mark';pop_no(mailboxlist);return false;">
	{tr}mark{/tr}</button>

	{tr}archive messages older than{/tr} 
	<select name="days">
	<option value="5">5 {tr}days{/tr}</option>
	<option value="10">10 {tr}days{/tr}</option>
	<option value="20">20 {tr}days{/tr}</option>
	<option value="40">40 {tr}days{/tr}</option>
	<option value="60">60 {tr}days{/tr}</option>
	<option value="80">80 {tr}days{/tr}</option>
	<option value="100">100 {tr}days{/tr}</option>
	</select>
	<button class="btn btn-default btn-sm" type="submit" name="autoarchive" value="autoarchive" title="{tr}autoarchive{/tr}"
	onclick="document.mailboxlist.action='{$baseurl}/message/mailbox/autoarchive';pop_no(mailboxlist);return false;">
	{tr}autoarchive{/tr}</button>
</div>

<div class="panel-body">

<table class="normal table table-bordered" id="scroll">
<thead><tr>
	<th class="heading"><input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" /></th>
	<th class="heading sortable" data-field="from">{tr}Sender{/tr}</th>
	<th class="heading sortable" data-field="subject">{tr}Subject{/tr}</th>
	<th class="heading sortable" data-field="date">{tr}Date{/tr}</th>
	<th class="heading">{tr}Reply To{/tr}</th>
</tr></thead>

<tbody class="scrollContent" id="contentTbody">
{*-------------------- list body ----------------------------------*}
{cycle values="odd,even" print=false}
{section name=user loop=$items}
<tr>
<td class="prio{$items[user].priority} selectable">
<input type="checkbox" name="checked[]" id="checked" value="{$items[user].id}" />
{if $items[user].isFlagged}<img src="img/icons/flagged.png" alt="" />{/if}
</td>

<td class="selectable">{$items[user].from}</td>

<td class="selectable">
<a href="{$baseurl}/message/mailbox/read?id={$items[user].id}">{$items[user].subject}</a>
</td>

<td class="selectable">{$items[user].date|date_format}</td>

<td class="prio{$items[user].priority} selectable">
{if $items[user].replyto_hash eq ""}&nbsp;
{else}
	<a class="readlink" href="{$baseurl}/message/mailbox/index?origto={$items[user].replyto_hash}">
	<img src="img/icons/arrow_up.png" alt="{tr}find replied message{/tr}" />
	</a>
{/if}
</td>

</tr>
{sectionelse}
<tr><td colspan="7">{tr}No messages to display{/tr}<td></tr>
{/section}
</tbody>
</table>
{$paginator}
</form>

</div></div>
