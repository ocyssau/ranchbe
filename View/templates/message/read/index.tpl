{literal}
<style>
td.heading{
	width:150px;
}
div.messureadbody{
	box-shadow: 0 0 5px black;
	margin: 0 15px 15px 15px;
}
</style>
{/literal}

<div class="container">

<h2>{$pageTitle}</h2>

{include file="message/navbar.tpl"}

<div class="messureadhead panel panel-default">

<div class="panel-heading messureadflag">
{if $message.isFlagged}
	<img alt="" src="{$baseurl}/img/icons/flagged.png" />
	<a class="btn btn-xs btn-default" href="{$baseurl}">{tr}Unflag{/tr}</a>
{else}
	<a class="btn btn-xs btn-default" href="{$baseurl}">{tr}Flag this message{/tr}</a>
{/if}
</div>

<div class="panel-body">
<table class="normal table table-bordered">
	<tr><td class="heading">{tr}From{/tr}:</td><td>{displayAddress addresses=$message.from}</td></tr>
	<tr><td class="heading">{tr}To{/tr}:</td><td>{displayAddress addresses=$message.to}</td></tr>
	<tr><td class="heading">{tr}Cc{/tr}:</td><td>{displayAddress addresses=$message.cc}</td></tr>
	<tr><td class="heading">{tr}Bcc{/tr}:</td><td>{displayAddress addresses=$message.bcc}</td></tr>
	<tr><td class="heading">{tr}Date{/tr}:</td><td>{$message.created|date_format}</td></tr>
</table>

<table class="normal table table-bordered">
	<tr><td class="heading">{tr}Subject{/tr}:</td><td>{$message.subject}</td></tr>
</table>
</div>

<div class="messureadbody panel-body">
	{$message.body}
</div>
</div>

</div>
