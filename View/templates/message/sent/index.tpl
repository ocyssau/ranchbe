{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	{/literal}
{rdelim});
</script>

{include file="message/navbar.tpl"}

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<table class="normal table table-bordered">
	<thead><tr>
		<td>
			<table border='0' height='20' cellpadding='0' cellspacing='0'
			 width='200' style='background-color:white;'>
				<tr>
					<td style='background-color:red;' width='{$cellsize}'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td><small>{$percentage}%</small></td>
	</tr></thead>
</table>
{if $percentage >= 100}
	{tr}Mailbox is full! Delete or archive some messages if you want to receive more messages.{/tr}
{/if}

<div class="panel panel-default">
<div class="panel-heading">
{*-------------------- list header ----------------------------------*}
<form name="messagesentlist" action="{$baseurl}/message/sent/index" method="post">
	<input type="hidden" name="offset" value="{$offset|escape}" />
	<input type="hidden" name="find" value="{$find|escape}" />
	<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
	<input type="hidden" name="flag" value="{$flag|escape}" />
	<input type="hidden" name="flagval" value="{$flagval|escape}" />
	<input type="hidden" name="priority" value="{$priority|escape}" />
	
	<button class="btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
	onclick="document.messagesentlist.action='{$baseurl}/message/sent/delete';pop_no(messagesentlist);return false;">
	{tr}Suppress{/tr}</button>
</div>

<div class="panel-body">
<table class="normal table table-bordered" >
<thead><tr>
	<th class="heading"><input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" /></th>
	<th class="heading"></th>
	<th class="heading sortable" data-field="user_to">{tr}Receiver{/tr}</th>
	<th class="heading sortable" data-field="subject">{tr}Subject{/tr}</th>
	<th class="heading sortable" data-field="date">{tr}Date{/tr}</th>
	<th class="heading sortable" data-field="isReplied">{tr}Replies{/tr}</th>
	<th class="heading">{tr}Size{/tr}</th>
</tr></thead>

{cycle values="odd,even" print=false}
{section name=user loop=$items}
<tr>
<td class="prio{$items[user].priority} selectable">
<input type="checkbox" name="checked[]" id="checked" value="{$items[user].id}" /></td>


<td class="prio{$items[user].priority selectable}">{if $items[user].isFlagged eq 'y'}<img src="img/icons/flagged.png" alt='{tr}flagged{/tr}' />{/if}</td>

<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority} selectable">{$items[user].user_to}</td>

<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority} selectable">
<a href="{$baseurl}/message/read/index?id={$items[user].id}&type=sent">{$items[user].subject}</a>
</td>

<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority} selectable">{$items[user].date|date_format}</td>

<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority} selectable">
{if $items[user].isReplied eq 'n'}
{tr}no{/tr}
{else}
<a class="readlink" href="{$baseurl}/message/mailbox/index/replyto={$items[user].hash}">
<img src="img/icons/email/email_go.png" alt='{tr}replied{/tr}' border='1'/></a>&nbsp;
<a href="tiki-user_information.php?view_user={$items[user].user_from}">
{$items[user].user_from}</a>
{/if}
</td>

<td style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority} selectable">{$items[user].len}</td>
</tr>
{sectionelse}
<tr><td colspan="6">{tr}No messages to display{/tr}<td></tr>
{/section}
</tbody>
</table>
{$paginator}
</form>

</div></div>
