<script>
$(function(){ldelim}
	initTbody();
{rdelim});
</script>

{include file="message/navbar.tpl"}

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

{if $messu_archive_size gt '0'}
<br />
<table border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td>
			<table border='0' height='20' cellpadding='0' cellspacing='0'
			 width='200' style='background-color:white;'>
				<tr>
					<td style='background-color:red;' width='{$cellsize}'>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td><small>{$percentage}%</small></td>
	</tr>
</table>
[{$messu_archive_number} / {$messu_archive_size}] {tr}messages{/tr}. {if $messu_archive_number eq $messu_archive_size}{tr}Archive is full!{/tr}{/if}
{/if}

<div class="panel panel-default">
<div class="panel-heading">

{*-------------------- list header ----------------------------------*}
<form name="messuarchivelist" action="{$baseurl}/message/archive/index" method="post">

<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="flag" value="{$flag|escape}" />
<input type="hidden" name="flagval" value="{$flagval|escape}" />
<input type="hidden" name="priority" value="{$priority|escape}" />

<button class="mult_submit btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="document.messuarchivelist.action='{$baseurl}/message/archive/delete';pop_no(messuarchivelist);return false;">
{tr}Suppress{/tr}</button>

{tr}delete messages older than{/tr} 
<select name="days">
<option value="5">5 {tr}days{/tr}</option>
<option value="10">10 {tr}days{/tr}</option>
<option value="20">20 {tr}days{/tr}</option>
<option value="40">40 {tr}days{/tr}</option>
<option value="60">60 {tr}days{/tr}</option>
<option value="80">80 {tr}days{/tr}</option>
<option value="100">100 {tr}days{/tr}</option>
</select>
<button class="mult_submit btn btn-default btn-sm" type="submit" name="autodelete" value="autodelete" title="{tr}autodelete{/tr}"
onclick="document.messuarchivelist.action='{$baseurl}/message/archive/autodelete';pop_no(messuarchivelist);return false;">
{tr}autodelete{/tr}</button>
</div>

<div class="panel-body">
<table class="normal table table-bordered" id="scroll">
<thead><tr>
	<th class="heading"><input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" /></th>
	<th class="heading"></th>
	<th class="heading"></th>
	<th class="heading sortable" data-field="user_from">{tr}Sender{/tr}</th>
	<th class="heading sortable" data-field="subject">{tr}Subject{/tr}</th>
	<th class="heading sortable" data-field="date">{tr}Date{/tr}</th>
	<th class="heading">{tr}Reply To{/tr}</th>
	<th class="heading">{tr}Size{/tr}</th>
</tr></thead>

<tbody class="scrollContent" id="contentTbody">
{*-------------------- list body ----------------------------------*}
{cycle values="odd,even" print=false}
{section name=user loop=$items}
<tr>

<td class="prio{$items[user].priority}"><input type="checkbox" name="msg[{$items[user].msgId}]" /></td>
<td class="prio{$items[user].priority}">{if $items[user].isFlagged eq 'y'}<img src="img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />{/if}</td>
<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}"><a href="tiki-user_information.php?view_user={$items[user].user_from}">{$items[user].user_from}</a></td>
<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">
<a href="{$baseurl}/message/read/index?id={$items[user].id}&type=archive">{$items[user].subject}</a>
</td>
<td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>
<tdstyle="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
</tr>
{sectionelse}
<tr><td colspan="6">{tr}No messages to display{/tr}<td></tr>
{/section}
</table>
{$paginator}
</form>

</div></div>
