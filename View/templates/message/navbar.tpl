{*Smarty template*}

<div id="page-bar" class="panel panel-default">
<div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/message/mailbox/index">{tr}Messages{/tr}</a> 
	<a class="btn btn-default btn-xs" href="{$baseurl}/message/compose/send">{tr}Compose{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/message/sent/index">{tr}Sent{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/message/archive/index">{tr}Archive{/tr}</a>
</div></div>

{if $mess_archiveAfter>0}( {tr}Auto-archive age for read messages:{/tr} {$mess_archiveAfter} {tr}days{/tr} ) {/if}

