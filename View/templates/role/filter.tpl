{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form id="filterf" action="{$smarty.server.REQUEST_URI}" method="get" class="form-inline">

{$form.hidden}
{sameurlpost}

<fieldset>
	<label for="find_number"><small>{tr}{$form.find_name.label}{/tr}</small></label>
	{$form.find_name.html}
	
	<input type="submit" value="{tr}filter{/tr}" class="btn btn-default"/>
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" class="btn btn-default"/>
</fieldset>
</form>
</div>
