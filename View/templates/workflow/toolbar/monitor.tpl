<a class="link" href="{$baseurl}/workflowmonitor/processes/index">
 <img border='0' 
  src='{$baseurl}/lib/Galaxia/img/icons/Process.gif' 
  alt='{tr}monitor{/tr}' 
  title='{tr}monitor processes{/tr}' />
</a>

<a class="link" href="{$baseurl}/workflowmonitor/activities/index">
 <img border='0' 
  src='{$baseurl}/lib/Galaxia/img/icons/Activity.gif' 
  alt='{tr}monitor{/tr}' 
  title='{tr}monitor activities{/tr}' />
</a>

<a class="link" href="{$baseurl}/workflowmonitor/instances/index">
 <img border='0' 
  src='{$baseurl}/lib/Galaxia/img/icons/Instance.gif' 
  alt='{tr}monitor{/tr}' 
  title='{tr}monitor instances{/tr}' />
</a>

<a class="link" href="{$baseurl}/workflowmonitor/workitem/index">
 <img border='0' 
  src='{$baseurl}/lib/Galaxia/img/icons/memo.gif' 
  alt='{tr}monitor{/tr}' 
  title='{tr}monitor workitems{/tr}' />
</a>

{*separator*} |

<a class="link" href="{$baseurl}/workflowadmin/processes/index">
 <img border='0' 
  src='{$baseurl}/lib/Galaxia/img/icons/Process.gif'
  alt='{tr}admin{/tr}' 
  title='{tr}edit{/tr}' />{tr}edit{/tr}
</a>
