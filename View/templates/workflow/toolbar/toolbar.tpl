{*Smarty template*}

<div id="page-bar">
  <h3>Actions</h3>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowadmin/processes/index">{tr}Admin process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/processes/index">{tr}Monitor process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/activities/index">{tr}Monitor activities{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/instance/index">{tr}Monitor instances{/tr}</a></span>
<!-- 
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/userprocess/index">{tr}User process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/useractivities/index">{tr}User activities{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="{$baseurl}/workflowmonitor/userinstance/index">{tr}User instance{/tr}</a></span>
-->
</div>
