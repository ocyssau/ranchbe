<table cellpadding='0' cellspacing='0'>
<tr>
 <td>
  <div style="height:20px; padding-top:2px; padding-bottom:2px;border:1px solid black; background-color:white; color:black; font-size:10px;">
    <table  cellpadding='0' cellspacing='0'>
      <tr>
  	   <td><b>{$proc_info.name}:{$proc_info.version}</b>
      	{if $proc_info.isValid eq 'y'}
      		<img border='0' src='{$baseurl}/lib/Galaxia/img/icons/green_dot.gif' alt='{tr}valid{/tr}' title='{tr}valid{/tr}' />
      	{else}
      		<img border='0' src='{$baseurl}/lib/Galaxia/img/icons/red_dot.gif' alt='{tr}invalid{/tr}' title='{tr}invalid{/tr}' />
      	{/if}
      	</td>
      	<td align='right'>
      		<table cellpadding='0' cellspacing='2'>
        		<tr>
        			{if $proc_info.isActive eq 'y'}
        			<td><a class="link" href="{$baseurl}/workflowadmin/process/deactivate?pid={$pid}&deactivate_proc={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/stop.gif' alt='{tr}stop{/tr}' title='{tr}stop{/tr}' /></a></td>
        			{else}
        			{if $proc_info.isValid eq 'y'}
        			<td><a class="link" href="{$baseurl}/workflowadmin/process/activate?pid={$pid}&amp;activate_proc={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/refresh2.gif' alt='{tr}activate{/tr}' title='{tr}activate{/tr}' /></a></td>
        			{/if}{/if}
        			<td><a class="link" href="{$baseurl}/workflowadmin/processes/index?pid={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Process.gif' alt='{tr}processes{/tr}' title='{tr}processes{/tr}' /></a></td>
        			<td><a class="link" href="{$baseurl}/workflowadmin/activities/index?pid={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Activity.gif' alt='{tr}activities{/tr}' title='{tr}activities{/tr}' /></a></td>						
        			<td><a class="link" href="{$baseurl}/workflowadmin/sharedsource/index?pid={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/book.gif' alt='{tr}code{/tr}' title='{tr}code{/tr}' /></a></td>		
        			<td><a class="link" href="{$baseurl}/workflowadmin/graph/index?pid={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/mode_tree.gif' alt='{tr}graph{/tr} 'title='{tr}graph{/tr}' /></a></td>		
        			<td><a class="link" href="{$baseurl}/workflowadmin/roles/index?pid={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/myinfo.gif' alt='{tr}roles{/tr}' title='{tr}roles{/tr}' /></a></td>		
            </tr>
  		    </table>
       </td>	
      </tr>
    </table>
    </div>
 </td>

 <td >
    <div style="height:20px; padding-top:2px; padding-bottom:2px; border:1px solid black; background-color:white; color:black; font-size:10px;">
      <table  cellpadding='0' cellspacing='0'>
       <tr>
      	<td><a class="link" href="{$baseurl}/workflowmonitor/processes/index?filter_process={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Process.gif' alt='{tr}monitor{/tr}' title='{tr}monitor processes{/tr}' />proc{$pid}</a></td>	
      	<td><a class="link" href="{$baseurl}/workflowmonitor/activities/index?filter_process={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Activity.gif' alt='{tr}monitor{/tr}' title='{tr}monitor activities{/tr}' />proc{$pid}</a></td>	
      	<td><a class="link" href="{$baseurl}/workflowmonitor/instances/index?filter_process={$pid}"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Instance.gif' alt='{tr}monitor{/tr}' title='{tr}monitor instances{/tr}' />proc{$pid}</a></td>	
       </tr>
       </table>
      </div>
 </td>

 <td >
    <div style="height:20px; padding-top:2px; padding-bottom:2px; border:1px solid black; background-color:white; color:black; font-size:10px;">
      <table  cellpadding='0' cellspacing='0'>
       <tr>
      	<td><a class="link" href="{$baseurl}/workflowmonitor/processes/index"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Process.gif' alt='{tr}monitor{/tr}' title='{tr}monitor processes{/tr}' />all</a></td>	
      	<td><a class="link" href="{$baseurl}/workflowmonitor/activities/index"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Activity.gif' alt='{tr}monitor{/tr}' title='{tr}monitor activities{/tr}' />all</a></td>	
      	<td><a class="link" href="{$baseurl}/workflowmonitor/instances/index"><img border='0' src='{$baseurl}/lib/Galaxia/img/icons/Instance.gif' alt='{tr}monitor{/tr}' title='{tr}monitor instances{/tr}' />all</a></td>	
       </tr>
       </table>
      </div>
 </td>

</tr>
</table>
