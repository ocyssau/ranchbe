{*Smarty template*}

{popup_init src="$baseurl/js/overlib.js"}

<h1>
<a class="pagetitle" href="{$baseurl}/workflowadmin/graph/index">
{tr}Process Graph{/tr} ({$proc_info.name}:{$proc_info.version})</a>
</h1>

	{if $pid > 0}
	 {include file=workflow/toolbar/process.tpl}
	  {if count($errors)}
	   <div class="wikitext">
	    {tr}This process is invalid{/tr}:<br />
	    {section name=ix loop=$errors}
	     <small>{$errors[ix]}</small><br />
	    {/section}
	   </div>
	  {/if}
	{/if}
	
  <h2>{tr}Process Graph for{/tr} {$info.name}</h2>
  {if $info.graph neq ''}
    <table class="normal table table-bordered">
      <tr>
        <td>
          <center>
            {if $info.map neq ''}
              <img src="{$info.graph}" alt="{$info.name}" border="0" usemap="#procmap" />
              <map name="procmap">
                {$info.map}
              </map>
            {else}
              <img src="{$info.graph}" alt="{$info.name}" border="0" />
            {/if}
          </center>
        </td>
      </tr>
    </table>
  {else}
    {tr}No process graph is available. Either the process still contains errors, the graph is not generated yet, or <a href="http://www.research.att.com/sw/tools/graphviz/">GraphViz</a> is not properly installed.{/tr}
  {/if}
  <br /><br />
  
  
