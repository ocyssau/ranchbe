{*Smarty template*}
{$form.javascript}

<div class="container">

<h2>{$pageTitle}</h2>


<form {$form.attributes}>
	{$form.hidden}

<fieldset>
<legend>Credential</legend>
<ul>
	<label>{tr}{$form.password1.label}{/tr}:</label>
	{$form.password1.html}
	
	<label>{tr}{$form.password2.label}{/tr}:</label>
	{$form.password2.html}
</ul>
</fieldset>

<ul>
	{if not $form.frozen}
		{$form.validate.html}
		{$form.cancel.html}
	{/if}
</ul>

<ul>
	{tr}{$form.requirednote}{/tr}<br />
</ul>

</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
{/if}
</div>
