{*Smarty template*}


<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}Do you want really suppress this user?{/tr}{literal}')){
			var url = document.baseurl+'/user/useradmin/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/useradmin/create">{tr}Create{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/roleadmin/display">{tr}Admin Roles{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">
{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading" style="width:40px;">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	<div id="displaySelectedRowCount">0</div>
	</th>
	
	<th class="heading" style="width:100px;"></th>
	
	<th class="heading sortable" data-field="login">{tr}Login{/tr}</th>
	<th class="heading sortable" data-field="firstname">{tr}First Name{/tr}</th>
	<th class="heading sortable" data-field="lastname">{tr}Last Name{/tr}</th>
	<th class="heading sortable" data-field="mail">{tr}Email{/tr}</th>
	<th class="heading sortable" data-field="lastLogin">{tr}Last Login{/tr}</th>
	<th class="heading">{tr}Roles{/tr}</th>
</tr></thead>

<tbody>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"}
{section name=list loop=$list}
	<tr class="{cycle}">
	<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
	
	<td>
		<a href="{$baseurl}/user/useradmin/edituser?userid={$list[list].id}" title="{tr}edit{/tr}">
		<img border="0" alt="{tr}edit{/tr}" src="{$baseurl}/img/icons/edit.png" />
		</a>
		
		<a href="{$baseurl}/user/useradmin/setpassword?userid={$list[list].id}" title="{tr}Password{/tr}">
		<img src="{$baseurl}/img/icons/user.png" border="0" height="16" width="16" alt='{tr}Password{/tr}' />
		</a>
		
		<a href="{$baseurl}/user/useradmin/delete?userid={$list[list].id}" title="{tr}Delete{/tr}">
		<img src="{$baseurl}/img/icons/trash.png" border="0" height="16" width="16" alt='{tr}Delete{/tr}' />
		</a>
	</td>

	<td class="selectable">{$list[list].login}</td>
	<td class="selectable">{$list[list].firstname}</td>
	<td class="selectable">{$list[list].lastname}</td>
	<td class="selectable">{$list[list].mail}</td>
	<td class="selectable">{$list[list].lastLogin}</td>

	<td>
		{assign var="id" value=$list[list].id}
		{foreach from=$list[list].roles item=role}
			<a class="btn btn-default btn-xs" 
			href="{$baseurl}/acl/permission/assign?roleid={$role.id}&areaid=1&resourceid=1" title="Permissions">
			<img border="0" alt="{tr}Assign Group{/tr}" src="{$baseurl}/img/icons/group_link.png" />
			{$role.name}
			</a>
		{/foreach}
		<a class="btn btn-default btn-xs" 
		href="{$baseurl}/acl/role/assign?userid={$list[list].id}" title="{tr}Assign Role{/tr}">
		<img border="0" alt="{tr}Assign Group{/tr}" src="{$baseurl}/img/icons/group_add.png" />
		</a>
	</td>
</tr>
{/section}
</tbody>
</table>

<button class="delete-btn btn btn-default btn-sm" type="submit" name="action" value="suppress" title="{tr}Delete{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Delete{/tr}" width="16" height="16" />
</button>

</form>
</div></div>
