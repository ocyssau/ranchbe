{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}Do you want really suppress this user?{/tr}{literal}')){
			var url = document.baseurl+'/user/roleadmin/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/roleadmin/create">{tr}Create{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/useradmin/display">{tr}Admin Users{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">
{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="#">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	<div id="displaySelectedRowCount">0</div>
	</th>
	
	<th class="heading auto"></th>
	
	<th class="heading sortable" data-field="name">{tr}Name{/tr}</th>
	<th class="heading sortable" data-field="description">{tr}Description{/tr}</th>
	<th class="heading">{tr}Includes{/tr}</th>
</tr></thead>

<tbody>
{*--------------------list body---------------------------*}
{cycle values="even,odd" print=false}
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="selectable" style="width: 20px;"><input type="checkbox" name="checked[]" value="{$list[list].id}"/></td>

	<td style="width: 80px;">
		<a class="link" href="{$baseurl}/user/roleadmin/edit?id={$list[list].id}" title="{tr}Edit{/tr}">
		<img src="{$baseurl}/img/icons/edit.png" alt='{tr}Edit{/tr}' width="16" height="16"/>
		</a>
		
		<a class="link" href="{$baseurl}/acl/permission/assign?roleid={$list[list].id}&resourceid={$resourceid}&areaid={$areaid}" title="{tr}Permissions{/tr}">
		<img border="0" alt="{tr}Permissions{/tr}" src="{$baseurl}/img/icons/key.png" />
		{$list[list].permcant}
		</a>
		
		<a class="link" href="{$baseurl}/user/roleadmin/delete?id={$list[list].id}" title="{tr}Delete{/tr}">
		<img border="0" alt="{tr}Delete{/tr}" src="{$baseurl}/img/icons/trash.png" />
		</a>
	</td>

	<td class="selectable">{$list[list].name}</td>
	<td class="selectable">{$list[list].description}</td>
	<td class="selectable">{$list[list].permcant}</td>
</tr>
{/section}
</tbody>
</table>

</form>
</div></div>
