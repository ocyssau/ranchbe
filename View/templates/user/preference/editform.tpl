{*Smarty template*}
{$form.javascript}

<div class="container">

<h2>{$pageTitle}</h2>


<form {$form.attributes}>
{$form.hidden}

<fieldset>
<legend>Date</legend>
<ul>
	<label>{tr}{$form.longDateFormat.label}{/tr}:</label>
	{$form.longDateFormat.html}
	
	<label>{tr}{$form.shortDateFormat.label}{/tr}:</label>
	{$form.shortDateFormat.html}
</ul>
</fieldset>

<fieldset>
<legend>Apparence</legend>
<ul>
	<label>{tr}{$form.cssSheet.label}{/tr}:</label>
	{$form.cssSheet.html}
</ul>
</fieldset>

<fieldset>
<legend>Other</legend>
<ul>
	<label>{tr}{$form.lang.label}{/tr}:</label>
	{$form.lang.html}
</ul>
<ul>
	<label>{tr}{$form.maxRecord.label}{/tr}:</label>
	{$form.maxRecord.html}
</ul>
</fieldset>

<ul>
	{if not $form.frozen}
		{$form.validate.html}
		{$form.cancel.html}
	{/if}
</ul>

<ul>
	{tr}{$form.requirednote}{/tr}<br />
</ul>

</form>

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
{/if}
</div>
