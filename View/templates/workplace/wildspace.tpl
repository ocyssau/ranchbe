{*Smarty template*}

<script>
var url = window.location.origin + window.location.pathname;
var orderby="{$orderby}";
var order="{$order}";

{literal}
$(function(){
	var url = getUrlWithoutSort();
	initSortableHeader(orderby, order, url.url);
	
	initEmbededContextMenu(".contextmenu-showbtn");
	
	dialog = $("#rename-dialog-form").dialog({
		autoOpen:false,
		height: 300,
		width: 350,
		modal: true,
		close: function() {
		}
	});
	
	
	$(".rename-btn").click(function(e){
		e.preventDefault();

		var div = $(this).parents('#wildspace-context-menu').prev();
		var filename = div.data("id");
		var url = $(this).attr("href");
		var nameInput = dialog.find("input#name");
		nameInput.val(filename);

		dialog.dialog({
			buttons: {
				"Ok": function() {
					var newName = nameInput.val();
					dialog.dialog( "close" );
					if(newName == null){
						return false;
					}
					$.ajax({
						'type' : 'GET',
						'url' : url,
						'data' : {'newname':newName},
						'dataType': 'html',
						'success' : function(data, textStatus, jqXHR){
							document.location.reload();
						},
						'error' : function(data, textStatus, jqXHR){
							alert("Rename Failed with Error "+jqXHR);
						}
					});
				},
				"Cancel": function() {
					dialog.dialog("close");
				}
			},
		});
		dialog.dialog("open");
		return false;
	});
	
	$(".copy-btn").click(function(e){
		e.preventDefault();

		var div = $(this).parents('#wildspace-context-menu').prev();
		var filename = div.data("id");
		var url = $(this).attr("href");
		var nameInput = dialog.find("input#name");
		nameInput.val(filename);

		dialog.dialog({
			buttons: {
				"Ok": function() {
					var newName = nameInput.val();
					dialog.dialog( "close" );
					if(newName == null){
						return false;
					}
					$.ajax({
						'type' : 'GET',
						'url' : url,
						'data' : {'newname':newName},
						'dataType': 'html',
						'success' : function(data, textStatus, jqXHR){
							document.location.reload();
						},
						'error' : function(data, textStatus, jqXHR){
							alert("Rename Failed with Error "+jqXHR);
						}
					});
				},
				"Cancel": function() {
					dialog.dialog("close");
				}
			},
		});
		dialog.dialog("open");
		return false;
	});
	
	/*BUTTONS DEFINITIONS*/
	$(".btn-delete").click(function(){
		if(confirm('Do you want really suppress this documents?')){
			var url = document.baseurl+'/workplace/wildspace/deletefile';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});
	
	$(".btn-store").click(function(){
		var url = document.baseurl+'/document/smartstore/store';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});
	
	$(".btn-uncompress").click(function(){
		var url = document.baseurl+'/workplace/wildspace/uncompress';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});
	
	$(".btn-zipdownload").click(function(){
		var url = document.baseurl+'/workplace/wildspace/downloadzip';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});

	$(".btn-refresh").click(function(){
		document.location.assign(document.baseurl+'/workplace/wildspace/index');
		return false;
	});
});
</script>

<style>
.dropbox{
	top:55px;
	right:62px;
}
</style>
{/literal}

<h1 class="pagetitle" title="{$wildspacePath}">{tr}Wildspace of{/tr} {$currentUserName}</h1>

{*--------------------DROP ZONE--------------------------*}
{include file='workplace/wildspace/dropzone.tpl'}

{*--------------------Search Bar defintion--------------------------*}
<div id="page-filter" class="panel panel-default"><div class="panel-body">
{$filter}
</div></div>

{* -------------------Pagination------------------------ *}
<div id="page-filter" class="panel panel-default">
<div class="panel-body">

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="#">
<table class="normal table table-bordered">
<tr><thead>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	<div id="displaySelectedRowCount">0</div>
	</th>
	
	<th class="heading auto"></th>
	
	<th class="heading" nowrap>
		<span class="heading sortable" data-field="name" nowrap>{tr}file_name{/tr}</span>
		<span class="heading sortable" data-field="extension" nowrap>{tr}file_extension{/tr}</span>
		<span class="heading sortable" data-field="type" nowrap>{tr}file_type{/tr}</span>
	</th>

	<th class="heading">{tr}doc_infos{/tr}</th>
	<th class="heading sortable" data-field="mtime" nowrap>{tr}mtime{/tr}</th>
	<th class="heading sortable" data-field="size" nowrap>{tr}file_size{/tr}</th>

	{if $displayMd5}
	<th class="heading sortable" data-field="md5" nowrap>{tr}file_md5{/tr}</th>
	{/if}
	
</thead></tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">
	<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

	<td>
		<div data-id="{$list[list].name}" data-basecamp="{$basecamp}">
		<button id="contextmenu" class="contextmenu-showbtn btn btn-default btn-sm">
		Actions Menu<span class="caret"></span></button>
		</div>
		{include file='workplace/wildspace/contextmenu.tpl'}
	</td>
	
	{*-Specifics fields-*}
	<td nowrap>
		<a href="{$baseurl}/workplace/wildspace/downloadfile?file={$list[list].name}&wildspace={$wildspace|escape}" title="{tr}Download{/tr}">
		{file_icon extension=$list[list].extension}
		{$list[list].name}
		</a>
		({$list[list].type})
	</td>

	<td class="selectable">
	{if $list[list].document_id}
		<i>{$list[list].designation}</i><br />
		<b>Checkout by : </b>{$list[list].check_out_by|username}<br />
		<b>In : </b> {tr}{$list[list].container_type}{/tr} {$list[list].container_number}
		{if $list[list].check_out_by == $currentUserId}
			<a href="{$baseurl}/document/manager/checkindoc?checked[]={$list[list].document_id}&container_id={$list[list].container_id}&space={$list[list].container_type}&ticket={$ticket}">
			<img src={$baseurl}/img/icons/document/document_in.png />CheckIn</a>
		{/if}
	{/if}
	</td>

	<td class="selectable">{$list[list].mtime|date_format}</td>
	<td class="selectable">{$list[list].size|filesize_format}</td>
	{if $displayMd5}
	<td class="selectable">{$list[list].md5}</td>
	{/if}
</tr>
{/section}
</table>

{* -------------------GabysBar------------------------ *}
<div id="action-menu" class="gabysbar">
	<button class="btn btn-default btn-sm btn-store" title="{tr}Store{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/document/document_add.png" alt="{tr}Store{/tr}"/>
	</button>
	
	<button class="btn btn-default btn-sm btn-zipdownload" title="{tr}Download zip{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/document/document_zip.png" alt="{tr}Download zip{/tr}"/>
	</button>
	
	<button class="btn btn-default btn-sm btn-uncompress" title="{tr}Uncompress{/tr}" />
	<img class="icon" src="{$baseurl}/img/icons/package/package_uncompress.png" alt="{tr}Uncompress{/tr}"/>
	</button>
	
	<button class="btn btn-default btn-sm btn-delete" title="{tr}Delete{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Suppress{/tr}"/>
	</button>
	
	<button class="btn btn-default btn-sm btn-refresh" title="{tr}Refresh{/tr}">
	<img class="icon" src="{$baseurl}/img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}"/>
	</button>
</div>

<input type="hidden" name="wildspace" value="{$wildspace|escape}" />
<input type="hidden" name="basecamp" value="{$basecamp}" />
</form>

</div>
</div>

<div id="rename-dialog-form" title="Rename File">
<p class="validateTips">All form fields are required.</p>
<form>
	<fieldset>
	<label for="name">New Name</label>
	<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all">
	<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
	</fieldset>
</form>
</div>

