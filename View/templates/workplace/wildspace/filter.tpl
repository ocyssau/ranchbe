{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}


<div id="searchbar">
<form id="filterf" action="{$smarty.server.REQUEST_URI}" method="post" class="form-inline">

{$form.hidden}
{sameurlpost}

<fieldset>
	<label for="find"><small>{tr}{$form.find.label}{/tr}</small></label>
	{$form.find.html}
	
	<label for="displayMd5"><small>{tr}{$form.displayMd5.label}{/tr}</small>{$form.displayMd5.html}</label>
	<label for="displayNew"><small>{tr}{$form.displayNew.label}{/tr}</small>{$form.displayNew.html}</label>
	
	<input type="submit" name="filter" value="{tr}filter{/tr}" class="btn btn-default btn-sm"/>
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" class="btn btn-default btn-sm"/>
</fieldset>
</form>
</div>
