{*Smarty template*}

<h2>{$pageTitle}</h2>

<form {$form.attributes}>
{$form.hidden}

<div class="help">
<p>{$help}<p>
</div>

<div class="formcolor">{$form.file.label}:</div>
<div class="formcolor">{$form.file.html}</div>

 {if not $form.frozen}
    <div class="formcolor">{$form.validate.html}{$form.cancel.html}</div>
 {/if}

<div class="formcolor">{$form.requirednote}</div>

</form>
