<ul id="wildspace-context-menu" class="ul-contextmenu">
	<li>
		<a href="{$baseurl}/workplace/wildspace/downloadfile?file={$list[list].name}" title="{tr}Download{/tr}" class="download-btn">
		{tr}Download{/tr}</a>
	</li>
	
	<li>
		<a href="{$baseurl}/workplace/wildspace/renamefile?file={$list[list].name}" title="{tr}Rename{/tr}" class="rename-btn">
		{tr}Rename{/tr}</a>
	</li>
	
	<li>
		<a href="{$baseurl}/workplace/wildspace/copyfile?file={$list[list].name}" title="{tr}Copy{/tr}" class="copy-btn">
		{tr}Copy{/tr}</a>
	</li>

	<li>
		<a href="{$baseurl}/document/smartstore/store?&checked[]={$list[list].name}&wildspace={$wildspace|escape}" title="{tr}Store{/tr}" class="store-btn">
		<img class="icon" src="{$baseurl}/img/icons/document/document_add.png" />
		{tr}Store{/tr}</a>
	</li>
	
	<li>
		<a href="{$baseurl}/workplace/wildspace/deletefile?file={$list[list].name}" title="{tr}Delete{/tr}" class="delete-btn">
		{tr}Delete{/tr}</a>
	</li>
</ul>
