<script src="{$baseurl}/js/jQuery-MD5/jquery.md5.min.js"></script>

{literal}

<script>
$(function() {
	var documentDialog;
	var file;
	var dropfileDiv = $("#dropfile").last();
	
	dropfileDiv.on('dragenter', function() {
		$(this).css('border', '3px dashed red');
		return false;
	});
	dropfileDiv.on('dragover', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).css('border', '3px dashed red');
		return false;
	});
	dropfileDiv.on('dragleave', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).css('border', '3px dashed #BBBBBB');
		return false;
	});
	
	dropfileDiv.on('drop', function(e){
		if (e.originalEvent.dataTransfer) {
			if (e.originalEvent.dataTransfer.files.length) {
				// Stop the propagation of the event
				e.preventDefault();
				e.stopPropagation();
				$(this).css('border', '3px dashed green');
				
				var files = e.originalEvent.dataTransfer.files;
			  	for (var i = 0, file; file = files[i]; i++) {
					var reader = new FileReader();
					reader.file = file;
					reader.url = document.baseurl+"/workplace/wildspace/upload";
					reader.onload = uploadFile;
					reader.readAsDataURL(file);
				}
			}
		}
		else {
			$(this).css('border', '3px dashed #BBBBBB');
		}
		return false;
	});
	
	//call by fileReader, in the context of the fileReader
	function uploadFile(event) {
		var splitresult = event.target.result.split(',');
		var mimestype = splitresult[0].split(';')[0];
		var encode = splitresult[0].split(';')[1];
		var data = splitresult[1];
		var filename = this.file.name;
		
		var params={};
		params.encode = 'json';
		params.files = {
			file1:{
				name:filename,
				data:{
					name:filename,
					md5:$.md5(data),
					size:data.length,
					data:data,
					mimestype:mimestype,
					encode:encode
				}
			}
		};
		
		$.ajax({
			type : 'post',
			url : this.url,
			data : params,
			dataType: 'html',
			success : function(data, textStatus, jqXHR){
				location.reload();
			},
			error : function(data, textStatus, jqXHR){
				alert("Error during upload of file, check file name and retry " + data);
				return ajaxErrorToMsg(data);
			}
		});
	}
});
</script>
{/literal}

<div id="dropfile" class="dropbox panel">
Drop a file here to add to Wildspace
</div>
	