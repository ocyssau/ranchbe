{*Smarty template*}
<div class="container">

<h1 class="pagetitle">{$pageTitle}</h1>

<form {$form.attributes}>
{$form.hidden}

<table class="normal table table-bordered">

{if $form.file_only}
  <tr>
    <td class="formcolor">{tr}{$form.file_only.label}{/tr}:</td>
    <td class="formcolor">{$form.file_only.html}</td>
  </tr>
{/if}

  <tr>
    <td class="formcolor">{tr}{$form.number.label}{/tr}:</td>
    <td class="formcolor">
      {$form.number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.description.label}{/tr}:</td>
    <td class="formcolor">{$form.description.html}</td>
  </tr>

  <tr>
    <td class="formcolor">{tr}{$form.processId.label}{/tr}:</td>
    <td class="formcolor">{$form.processId.html}</td>
  </tr>


{if $form.parentId}
  <tr>
    <td class="formcolor">{tr}{$form.parentId.label}{/tr}:</td>
    <td class="formcolor">{$form.parentId.html}</td>
  </tr>
{/if}

{if $form.doctypeId}
  <tr>
    <td class="formcolor">{tr}{$form.doctypeId.label}{/tr}:</td>
    <td class="formcolor">{$form.doctypeId.html}</td>
  </tr>
{/if}

{if $form.supplierId}
  <tr>
    <td class="formcolor">{tr}{$form.supplierId.label}{/tr}:</td>
    <td class="formcolor">{$form.supplierId.html}</td>
  </tr>
{/if}

  <tr>
    <td class="formcolor">{tr}{$form.forseenCloseDate.label}{/tr}:</td>
    <td class="formcolor">{$form.forseenCloseDate.html}</td>
  </tr>

  <!-- Display optionnals fields -->
  {section name=of loop=$extended}
  {assign var="fn" value=$extended[of].appName}
  <tr class="formcolor">
    <td>{tr}{$form.$fn.label}{/tr}:</td>
    <td>{$form.$fn.html}</td>
  </tr>
  {/section}

 {if not $form.frozen}
  <tr>
    <td class="formcolor" colspan=2>
    {$form.validate.html}
    {$form.cancel.html}
    </td>
  </tr>
 {/if}

  <tr>
    <td class="formcolor" colspan=2>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>

<input type="hidden" name="layout" value="popup">

</form>

{if $form.errors}
{foreach key=name item=error from=$form.errors}
	<b>Collected Errors:</b> <br />
	<font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
{/if}

</div>