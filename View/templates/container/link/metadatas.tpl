{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".unlink-btn").click(function(){
		if(confirm('{/literal}{tr}Do you want really suppress this property link?{/tr}{literal}')){
			var url = baseurl+'/container/linkmetadata/unlink';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});
	
	$(".addlink-btn").click(function(){
		var url = baseurl+'/container/linkmetadata/addlink';
		$("form[name='checkform']").attr('action', url);
		$("form[name='checkform']").submit();
		return false;
	});
	
	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<h2>{$pageTitle}</h2>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">
<table class="normal table table-bordered">
<tr>
 <thead>
	<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	</th>
	
	<th class="heading sortable" data-field="link_id">#</th>
	<th class="heading sortable" data-field="extendedCid">{tr}Extended Class{/tr}</th>
	<th class="heading sortable" data-field="appName">{tr}Name{/tr}</th>
	<th class="heading sortable" data-field="name">{tr}Db Field Name{/tr}</th>
	<th class="heading sortable" data-field="label">{tr}Label{/tr}</th>
	<th class="heading sortable" data-field="field_description">{tr}Description{/tr}</th>
</thead>
</tr>

{*--------------------list body---------------------------*}
<tbody>
{cycle print=false values="even,odd"}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].link_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    <td class="thin">{$list[list].link_id}</td>
    <td class="thin">{$list[list].extendedCid|cidToClassName}</td>
    <td class="thin">{$list[list].appName}</td>
    <td class="thin">{$list[list].name}</td>
    <td class="thin">{$list[list].label}</td>
    <td class="thin">{$list[list].description}</td>
   </tr>
  {/section}
</tbody>
</table>

<button class="mult_submit btn btn-default btn-sm addlink-btn" name="action" value="addlink" title="{tr}Add Link{/tr}">
<img class="icon" src="{$baseurl}/img/icons/metadata/link.png" alt="{tr}Add Link{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm unlink-btn" name="action" value="unlink" title="{tr}Unlink{/tr}">
<img class="icon" src="{$baseurl}/img/icons/metadata/unlink.png" alt="{tr}Unlink{/tr}" width="16" height="16" />
</button>

<button class="btn btn-default btn-sm refresh-btn" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<input type="hidden" name="containerid" value="{$containerid}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="spacename" value="{$spacename}" />
</form>

