{*Smarty template*} 

<script>
$(function(){ldelim}
var baseurl = "{$baseurl}";

{literal}
{/literal}
{rdelim});
</script>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.REQUEST_URI}">
<table class="normal table table-bordered">
<tr><thead>
	<th class="heading auto">
		<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
	</th>
	<th class="heading sortable" data-field="id">#</th>
	<th class="heading sortable" data-field="doctype_number">{tr}Number{/tr}</th>
	<th class="heading sortable" data-field="doctype_description">{tr}Description{/tr}</th>
	<th class="heading sortable" data-field="file_extension">{tr}File Extension{/tr}</th>
</thead></tr>

{*--------------------list body---------------------------*}
<tbody>
{cycle print=false values="even,odd"}
{section name=list loop=$list}
	<tr class="{cycle}">
	<td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
	<td class="thin">{$list[list].id}</td>
	<td class="thin">{$list[list].name}</td>
	<td class="thin">{$list[list].description}</td>
	<td class="thin">{$list[list].fileExtension}</td>
	</tr>
{/section}
</tbody>
</table>

<input type="submit" name="cancel" value="Cancel" class="btn btn-default"/>
<input type="submit" name="validate" value="Add Link" class="btn btn-success"/>

<input type="hidden" name="containerid" value="{$containerid}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="spacename" value="{$spacename}" />
</form>
