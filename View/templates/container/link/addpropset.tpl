{*Smarty template*}

<h1 class="pagetitle">{tr}Properties set list{/tr}</h1>

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal table table-bordered">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}link_id{/tr}</th>
  <th class="heading">{tr}propset_id{/tr}</th>
  <th class="heading">{tr}property_id{/tr}</th>
  <th class="heading">{tr}propset_name{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].link_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    <td class="thin">{$list[list].link_id}</td>
    <td class="thin">{$list[list].propset_id}</td>
    <td class="thin">{$list[list].property_id}</td>
    <td class="thin">{$list[list].propset_name}</td>
   </tr>
  {/section}
</table>

{*Multiselection select action form *}
<br />
<img class="icon" src="{$baseurl}/img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>

<input type="submit" name="action" value="linkPropset" />

{*Submit checkform form*}
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="action" value="{$action}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>

