{*Smarty template*} 
 
<h1 class="pagetitle">{tr}List of processes{/tr} ({$cant})</h1>

{*--------------------list header----------------------------------*}

<form id="checkform" action="{$smarty.server.REQUEST_URI}" method="post">

    <table class="normal table table-bordered">
     <tr>
      <th class="heading auto"></th>
      <th class="heading">{tr}Name{/tr}</th>
      <th class="heading">{tr}Activities{/tr}</th>
      <th class="heading">{tr}act{/tr}</th>

    {*--------------------list body---------------------------*}
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    
      {foreach from=$items item=proc}
       <tr class="{cycle}">
        <td class="thin">
        <input type="checkbox" name="checked[]" value="{$proc.pId}" 
          onChange='javascript:getElementById("checkform").submit();'/>
          </td>
          
        <td class="thin">{$proc.name} {$proc.version}</td>
        <td class="thin">{$proc.activities}</td>
        
        <td class="thin">{if $proc.isActive eq 'y'}
        <img src='lib/Galaxia/img/icons/refresh2.gif' alt=' ({tr}active{/tr}) ' title='{tr}active process{/tr}' /> {else} &nbsp; {/if}</td>
       </tr>
       {foreachelse}
       
       <tr>
        <td class="{cycle advance=false}" colspan="6"> {tr}No processes defined yet{/tr}</td>
       </tr>{/foreach}
      </table>

{* END OF LISTING *}


<i>{tr}Perform action{/tr} :</i>

<input type="hidden" name="popup" value="1" />
<input type="hidden" name="action" value="{$action}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="hidden" name="link_id" value="{$link_id}" />
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="submit" name="action" value="linkProcess" />

<p></p>

</form>

