{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
	
	$.each($('.optionSelector:checked'), function( index, item ) {
		displayOption(item,0);
	});	
});

function displayOption(element, speed=400){
	if( element.checked ){
		$(element).parent().nextAll('span').children('fieldset').first().show(speed);
	}
	else{
		$(element).parent().nextAll('span').children('fieldset').first().hide(speed);
	}
}

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form id="filterf" action="{$smarty.server.REQUEST_URI}" method="post" class="form-inline">
{$form.hidden}

<fieldset>
	<label for="f_action_name"><small>{tr}{$form.f_action_name.label}{/tr}</small></label>
	{$form.f_action_name.html}

	<label for="f_action_user_name"><small>{tr}{$form.f_action_user_name.label}{/tr}</small></label>
	{$form.f_action_user_name.html}
	
	<label for="f_container_number"><small>{tr}{$form.f_container_number.label}{/tr}</small></label>
	{$form.f_container_number.html}

	<input type="submit" name="filter" value="{tr}filter{/tr}" />
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

	<br />
	
	<label>{$form.f_adv_search_cb.html}
	<small>{tr}{$form.f_adv_search_cb.label}{/tr}</small></label>
	
	<span style="display:block"><fieldset style="display:none">
	
	<label for="find"><small>{tr}{$form.find.label}{/tr}</small></label>
	{$form.find.html}
	
	<label for="find_field"><small>{tr}{$form.find_field.label}{/tr}</small></label>
	{$form.find_field.html}
	
	<label>{$form.f_dateAndTime_cb.html}
	<small>{tr}{$form.f_dateAndTime_cb.label}{/tr}</small></label>
	
	<span style="display:block"><fieldset style="display:none">
	
	{if isset($form.f_action_date_cb)}
		<label>{$form.f_action_date_cb.html}
		<small>{tr}{$form.f_action_date_cb.label}{/tr}</small></label>
			<span style="display:block"><fieldset style="display:none">
				<label for="f_action_date_min"><small>{tr}{$form.f_action_date_min.label}{/tr}</small></label>
				{$form.f_action_date_min.html}
				<label for="f_action_date_max"><small>{tr}{$form.f_action_date_max.label}{/tr}</small></label>
				{$form.f_action_date_max.html}
			</fieldset></span>
	{/if}
	
	</span>
	</fieldset>
	
	</span>
	</fieldset>

</form>
</div>
