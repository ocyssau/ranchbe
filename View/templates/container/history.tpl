{*Smarty template*}
<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";

	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".delete-btn").click(function(){
		if(confirm('Do you want really suppress this history?')){
			var url = document.baseurl+'/container/history/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});
	
	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div id="page-filter" class="panel panel-default"><div class="panel-body">
{$filter}
</div></div>

<div id="page-list" class="panel panel-default">
<div class="panel-heading">
{* -------------------Pagination------------------------ *}
{$paginator}
</div>

<div class="panel-body">
{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="#">
<table class="normal table table-bordered">
<thead><tr>

<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='action_id[]']" />
	<div id="displaySelectedRowCount">0</div>
</th>

<th class="heading auto"></th>

<th class="heading sortable" data-field="action_id">{tr}action_id{/tr}</th>
<th class="heading sortable" data-field="action_name">{tr}action_name{/tr}</a></th>
<th class="heading sortable" data-field="action_ownerId">{tr}action_ownerId{/tr}</a></th>
<th class="heading sortable" data-field="action_created">{tr}action_created{/tr}</a></th>
<th class="heading sortable" data-field="data_id">{tr}Id{/tr}</a></th>
<th class="heading sortable" data-field="data_name">{tr}Number{/tr}</a></th>
<th class="heading sortable" data-field="data_description">{tr}Description{/tr}</a></th>
<th class="heading sortable" data-field="data_status">{tr}Status{/tr}</a></th>
<th class="heading sortable" data-field="data_versionId">{tr}Version{/tr}</a></th>
</tr></thead>

<tbody>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">
<td class="selectable"><input type="checkbox" name="action_id[]" value="{$list[list].action_id}" /></td>

<td>
<a href="{$baseurl}/container/history/delete?action_id[]={$list[list].action_id}&checked[]={$checked}&space={$spacename}" 
title="{tr}Suppress{/tr}: {$list[list].action_id}"
onclick="return confirm('{tr}Do you want really suppress{/tr} history {$list[list].action_id}')">
<img border="0" alt="{tr}Suppress{/tr}: {$list[list].action_id}" src="{$baseurl}/img/icons/trash.png" /></a>
</td>

<td class="selectable" >{$list[list].action_id}</td>
<td class="selectable" >{$list[list].action_name}</td>
<td class="selectable" >{$list[list].action_ownerId}</td>
<td class="selectable" >{$list[list].action_created|date_format}</td>
<td class="selectable" >{$list[list].data_id}</td>
<td class="selectable" >{$list[list].data_name}</td>
<td class="selectable" >{$list[list].data_description}</td>
<td class="selectable" >{$list[list].data_status}</td>
<td class="selectable" >{$list[list].data_versionId}</td>
</tr>
{/section}
</tbody>
</table>

<button class="mult_submit btn btn-default btn-sm delete-btn" type="submit" name="action" value="suppress" title="{tr}Delete{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Delete{/tr}"/>
</button>

<button class="mult_submit btn btn-default btn-sm refresh-btn" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" alt="{tr}Refresh{/tr}"/>
</button>

<input type="hidden" name="orderby" value="{$orderby|escape}" />
<input type="hidden" name="order" value="{$order|escape}" />
<input type="hidden" name="containerid" value="{$containerid}" />
<input type="hidden" name="spacename" value="{$spacename}" />

{section name=checked loop=$checked}
<input type="hidden" name="checked[]" value="{$checked[checked]}" />
{/section}

</form>
</div>

