{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}Are you sure?{/tr}{literal}')){
			var url = document.baseurl+'/acl/permission/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/roleadmin/display">{tr}Admin Roles{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/useradmin/display">{tr}Admin Users{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

<div class="panel panel-default">
<div class="panel-heading">
</div>

<div class="panel-body">
{*--------------------list header----------------------------------*}
<form name="rights" method="post" action="#">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading auto">
	<input type="radio" class="switcher" data-toswitch=".deny" name="selectAll"/>
	Deny
	</th>
	<th class="heading auto">
	<input type="radio" class="switcher" data-toswitch=".allow" name="selectAll"/>
	Permit
	</th>
	<th class="heading auto">
	<input type="radio" class="switcher" data-toswitch=".inherit" name="selectAll"/>
	Inherit
	</th>
	
	<th class="heading" data-field="description">{tr}Description{/tr}</th>
	<th class="heading">{tr}Area{/tr}</th>
</tr></thead>

{*--------------------list body---------------------------*}
<tbody>
{cycle values="even,odd" print=false}
{section name=list loop=$list}
{assign var=name value=$list[list].name}
{assign var=id value=$list[list].id}
<tr class="{cycle}">
	<td class="thin" style="width: 20px;">
	<input class="right deny" type="radio" name="rights[{$id}]" value="deny" {if $list[list].rule eq 'deny'}checked="checked" {/if}/>
	</td>
	
	<td class="thin" style="width: 20px;">
	<input class="right allow" type="radio" name="rights[{$id}]" value="allow" {if $list[list].rule eq 'allow'}checked="checked" {/if}/>
	</td>
	
	<td class="thin" style="width: 20px;">
	<input class="right inherit" type="radio" name="rights[{$id}]" value="inherit" {if $list[list].rule eq 'inherit'}checked="checked" {/if}/>
	</td>

	<td>{$list[list].description}</td>
	<td>{$list[list].areaId}</td>
</tr>
{/section}
</tbody>
</table>

<input type="submit" name="cancel" value="Cancel" class="btn btn-default">
<input type="submit" name="validate" value="Save" class="btn btn-success">

</form>
</div></div>
