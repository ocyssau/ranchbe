{*Smarty template*}
{literal}
<style>
h3{
	margin:0px;
}
</style>
{/literal}

<div id="page-bar" class="panel panel-default"><div class="panel-body">
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/roleadmin/display">{tr}Admin Roles{/tr}</a>
	<a class="btn btn-default btn-xs" href="{$baseurl}/user/useradmin/display">{tr}Admin Users{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

<div class="panel panel-default">
<div class="panel-heading">
	<h3>{tr}User{/tr}</h3>
</div>
<div class="panel-body">
<table class="normal table table-bordered">
	<tr>
	<td class="heading" style="width:150px;">{tr}Login{/tr}:</td>
	<td>{$user.login}</td>
	</tr>

	<tr>
	<td class="heading">{tr}Groups{/tr}:</td>
	<td>
	{section name=a loop=$usergrps}
	<a class="btn btn-warning btn-xs glyphicon glyphicon-remove" 
	href="{$baseurl}/acl/role/delete?userid={$user.id}&roleid={$usergrps[a].id}" title="Delete">
	{$usergrps[a].name}
	</a>
	{/section}
	</td>
	</tr>
</table>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
	<h3>{tr}Available groups{/tr}</h3>
</div>

<div class="panel-body">
<table class="normal table table-bordered">
<thead><tr>
	<th class="heading" style="width:60px;"></th>
	<th class="heading">{tr}Description{/tr}</th>
</tr></thead>

<tbody>
{cycle values="even,odd" print=false}
{section name=groups loop=$availableGroups}
<tr class="{cycle}">
	<td>
	<a class="btn btn-success glyphicon glyphicon-plus" href="{$baseurl}/acl/role/assign?roleid={$availableGroups[groups].id}&userid={$user.id}&validate=1">
	{$availableGroups[groups].name}
	</a>
	</td>
	
	<td>{$availableGroups[groups].description}</td>
</tr>
{/section}
</tbody>
</table>

</div>
</div>

