{*Smarty template*}

<script>
$(function(){ldelim}
	var url = window.location.origin + window.location.pathname;
	var currentOrderby="{$orderby}";
	var currentOrder="{$order}";
	var baseurl = "{$baseurl}";
	
	{literal}
	var url=getUrlWithoutSort();
	initSortableHeader(currentOrderby, currentOrder, url.url);
	
	$(".delete-btn").click(function(){
		if(confirm('{/literal}{tr}Do you want really suppress this category?{/tr}{literal}')){
			var url = document.baseurl+'/category/manager/delete';
			$("form[name='checkform']").attr('action', url);
			$("form[name='checkform']").submit();
		}
		return false;
	});

	$(".refresh-btn").click(function(){
		document.location.reload(true);
		return false;
	});
	
	{/literal}
{rdelim});
</script>

<div id="page-bar" class="panel panel-default"><div class="panel-body">
 <a class="btn btn-default btn-xs" href="{$baseurl}/category/manager/create?spacename={$spacename}">{tr}Create{/tr}</a>
</div></div>

<h1 class="pagetitle">{$pageTitle}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div class="panel panel-default">
<div class="panel-body">
{$filter}
</div></div>

<div class="panel panel-default">
<div class="panel-heading">
{$paginator}
</div>

<div class="panel-body">

{*--------------------list header----------------------------------*}
<form id="checkform" name="checkform" method="post" action="{$baseurl}/category/manager/display">
<table class="normal table table-bordered">
<tr><thead>
<th class="heading auto">
	<input type="checkbox" class="switcher" data-toswitch="[name='checked[]']" />
</th>

<th class="heading auto"></th>

<th class="heading sortable" data-field="number">{tr}Number{/tr}</th>
<th class="heading sortable" data-field="description">{tr}Description{/tr}</th>
<th class="heading sortable" data-field="icon">{tr}Icon{/tr}</th>
</thead></tr>
<tbody>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"}
{section name=list loop=$list}
<tr class="{cycle}">
<td class="selectable"><input type="checkbox" name="checked[]" value="{$list[list].category_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

<td><a href="{$baseurl}/category/manager/edit?categoryid={$list[list].category_id}&spacename={$spacename}" title="{tr}edit{/tr}">
<img border="0" alt="{tr}edit{/tr}" src="{$baseurl}/img/icons/edit.png" /></a>
</td>

{*-Specifics fields-*}
<td class="selectable">{$list[list].category_number}</td>
<td class="selectable">{$list[list].category_description}</td>
<td class="selectable">{$list[list].category_icon}</td>
</tr>
{/section}
<tbody>
</table>

<button class="btn btn-default btn-sm delete-btn" type="submit" name="action" value="suppressCat" title="{tr}Suppress{/tr}">
<img class="icon" src="{$baseurl}/img/icons/trash.png" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit btn btn-default btn-sm refresh-btn" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="{$baseurl}/img/icons/refresh.png" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<input type="hidden" name="spacename" value="{$spacename}" />
</form>
</div></div>

