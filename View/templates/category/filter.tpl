{*Smarty template*}

{literal}
<script>
$(function() {
	$('.submitOnClick').click(function(){
		return filterSubmit(this);
	});
});

function filterSubmit(element){
	$('#filterf').submit();
}
</script>
{/literal}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<form id="filterf" action="{$smarty.server.REQUEST_URI}" method="post" class="form-inline">

{$form.hidden}
{sameurlpost}

<fieldset>
	<label for="find_number"><small>{tr}{$form.find_number.label}{/tr}</small></label>
	{$form.find_number.html}
	
	<label for="find_description"><small>{tr}{$form.find_description.label}{/tr}</small></label>
	{$form.find_description.html}
	
	
	<input type="submit" name="filter" value="{tr}filter{/tr}" class="btn btn-default btn-sm" />
	<input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" class="btn btn-default btn-sm" />
</fieldset>
</form>
</div>
