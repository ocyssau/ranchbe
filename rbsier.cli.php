#!/usr/bin/php
<?php

namespace rbsier\cli;

use Zend\Json\Server\Client as JsonClient;
use Zend\Http\Client as httpClient;
use Ranchbe;
use Zend\Stdlib\ArrayUtils;

/**
 *
 */
function optionh()
{
	$display = 'Utilisation :

	php tests.php
	[-t]
	[-i]
	[-h]

	Avec:

	-i
	: initialise les données de test.

	-h
	: Affiche cette aide.

	-t : Execute la function test <fonction> parmis les fonctions suivantes :';
	echo $display;
	displaylisttest();
}

function displaylisttest()
{
	echo $display='
	date,
	exception,
	instanceof
	my
	';
}

/**
 *
 */
function run()
{
	chdir(__DIR__);
	//var_dump(getcwd());die;

	error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT );
	ini_set ( 'display_errors', 1 );
	ini_set ( 'display_startup_errors', 1 );

	ini_set('xdebug.collect_assignments', 1);
	ini_set('xdebug.collect_params', 4);
	ini_set('xdebug.collect_return', 1);
	ini_set('xdebug.collect_vars', 1);
	ini_set('xdebug.var_display_max_children', 1000);
	ini_set('xdebug.var_display_max_data', 5000);
	ini_set('xdebug.var_display_max_depth', 10);

	/*
	 ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
	ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
	ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
	ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
	ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
	*/

	assert_options( ASSERT_ACTIVE, 1);
	assert_options( ASSERT_WARNING, 1 );
	assert_options( ASSERT_BAIL, 0 );
	//assert_options( ASSERT_CALLBACK , 'myAssertCallback');

	/*
	 ob_start();
	phpinfo();
	file_put_contents('.phpinfos.out.txt', ob_get_contents());
	*/

	echo 'include path: ' . get_include_path ()  . PHP_EOL;

	//$zver=new \Zend\Version\Version();
	//echo 'ZEND VERSION : ' . $zver::VERSION . "\n";

	$shortopts = '';
	$shortopts .= "h";  // affiche l'aide
	$shortopts .= "t:"; // call test function <function>
	$shortopts .= "i"; //initialise les données de test
	$shortopts .= "c:"; // La classe <class> de test a lancer
	$shortopts .= "m:"; // La methode <method> a executer

	$longopts  = array(
	);

	$norun=array('nodao','m', 'p');
	$options = getopt($shortopts, $longopts);

	foreach(array_keys($options) as $o){
		if(!in_array($o, $norun)){
			call_user_func(__NAMESPACE__.'\option'.$o, $options);
		}
	}
	return;
}

/**
 * Execute the specified function
 */
function optiont($options)
{
	//initialize
	boot();
	$suffix = $options['t'];
	if($suffix==''){
		$suffix='my';
	}
	call_user_func(__NAMESPACE__.'\test_'.$suffix, array());
}

/**
 * Installation
 */
function optioni($options)
{
	if ( !is_file('config/autoload/local.php') ){
		echo "'config/autoload/local.php' is not found. Create it: copy config/dist/local.php.dist to config/autoload/local.php";
		die;
	}
	if ( !is_file('public/.htaccess') ){
		copy('config/dist/htaccess.dist', 'public/.htaccess');
	}

	$ranchbe = new Ranchbe();
	$local = include 'config/autoload/local.php';
	$global = include 'config/autoload/global.php';
	$config = ArrayUtils::merge($global,$local);
	$ranchbe->setConfig($config['rbp']);
	$config = $config['rbp'];

	if (!is_dir ($config['path.reposit.wildspace'])){
		mkdir($config['path.reposit.wildspace'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.workitem'])){
		mkdir($config['path.reposit.workitem'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.mockup']) ) {
		mkdir($config['path.reposit.mockup'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.cadlib']) ) {
		mkdir($config['path.reposit.cadlib'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.bookshop']) ) {
		mkdir($config['path.reposit.bookshop'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.trash']) )  {
		mkdir($config['path.reposit.trash'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.import']) ){
		mkdir($config['path.reposit.import'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.unpack']) ) {
		mkdir($config['path.reposit.unpack'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.workitem.archive']) ) {
		mkdir($config['path.reposit.workitem.archive'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.mockup.archive']) ) {
		mkdir($config['path.reposit.mockup.archive'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.cadlib.archive']) ) {
		mkdir($config['path.reposit.cadlib.archive'], 0777, true);
	}
	if (!is_dir ($config['path.reposit.bookshop.archive']) ) {
		mkdir($config['path.reposit.bookshop.archive'], 0777, true);
	}
	if (!is_dir ($config['view.smarty']['compile']['path']) ) {
		mkdir($config['view.smarty']['compile']['path'], 0777, true);
	}
}


/**
 * Backup scripts for ranchbe DATABASES
 * to restore a dump file :
 * mysql -e "source /patch-to-backup/backup-file.sql" databaseName
 *
 * Backup Database
 */
function optionb($options)
{
	//initialize
	boot();
	$config = \Ranchbe::get()->getConfig();
	$dbConfig = $config['rbp']['db']['params'];
	$host   = $dbConfig['host'];
	$user   = $dbConfig['username'];
	$password   = $dbConfig['password'];
	$db    = $dbConfig['dbname'];
	$toDir = $config['rbp']['path.database.backup'];

	if(empty($toDir)||!is_dir($toDir)||is_writable($toDir)){
		new \Exception("$toDir is not writable");
	}

	echo date(DATE_RFC822) . "\n";
	echo "Backup of $db @ $host \n";

	$mysqldumpcmd = 'mysqldump';
	$toSqlFile = $toDir .'/'. $db. '_backup.sql';

	copy($toSqlFile, $toSqlFile . '.1');

	echo 'Backup in file ' . $toSqlFile . "\n";

	// Utilise les fonctions syst�me : MySQLdump
	//Backup in SQL format
	$command = "$mysqldumpcmd --opt -h$host -u$user --password=$password $db --result-file=$toSqlFile";
	$ok = system($command);

	//Backup in XML format
	$xmlFile = $toDir .'/'. $db. '_backup.xml';
	echo 'Backup in file ' . $xmlFile . "\n";
	$command = "$mysqldumpcmd --xml -h$host -u$user --password=$password $db --result-file=$xmlFile";
	$ok = system($command);

	echo "End of backup at " . date(DATE_RFC822) . "\n";
}

/**
 * Execute les test de la classe ou des classes spécifiées
 */
function optionc($options)
{
	boot();

	$method = $options['m'];
	(isset($options['m'])) ? $method = 'Test_' . $options['m'] : $method = null;

	if ( isset($options['nodao']) ) {
		$withDao = false;
	}
	else{
		$withDao = true;
	}

	if ( isset($options['p']) ) {
		$withProfiling = true;
	}
	else{
		$withProfiling = false;
	}

	if(is_array($options['c'])){
		foreach($options['c'] as $class){
			\Rbplm\Test\Test::runOne($class, $method, $withDao, $continue=false, $withProfiling);
		}
	}
	else{
		\Rbplm\Test\Test::runOne($options['c'], $method, $withDao, $continue=false, $withProfiling);
	}
}

/**
 * Initialize
 */
function boot()
{
	require_once 'module/Ranchbe/Ranchbe.php';
	require_once 'config/boot.php';

	$appConfig = include 'config/application.config.php';
	$loader = rbinit_autoloader($appConfig);
	$loader->set('RbsTest\\', 'module/Rbs/src');
	$loader->set('ControllerTest\\', 'module/Test');

	$ranchbe = new Ranchbe();
	$local = include 'config/autoload/local.php';
	$global = include 'config/autoload/global.php';
	$config = ArrayUtils::merge($global,$local);
	$ranchbe->setConfig($config['rbp']);

	rbinit_rbservice($config['rbp']);
	rbinit_enableDebug(true);
}


/**
 *
 */
function test_date()
{
	$tz = new \DateTimeZone('Europe/Paris');
	$date = new \DateTime('now',$tz);
	echo $date->format('Y-m-d H:i:s') . "\n";
	echo $date->getTimestamp() . "\n";

	$date = new \DateTime('05 oct 2050',$tz);
	echo $date->format('Y-m-d') . "\n";
	echo $date->getTimestamp() . "\n";

	//$date = (new \DateTime())->setTimeStamp(time());
	echo $date->format('Y-m-d') . "\n";
}

function test_exception()
{
	try{
		throw new \Rbplm\Sys\Exception('Sys exception message avec un %1% et un %0%', 0, array('MOTIF1', 'MOTIF2'));
	}
	catch(\Rbplm\Sys\Exception $e){
		var_dump($e->getMessage());
		var_dump($e->getCode());
	}
	catch(\Exception $e){
	}
}

/**
 *
 * @throws \Rbplm\Sys\Exception
 */
function test_temp()
{
	include('lang/fr/language.php');
	foreach($lang as $en=>$fr){
		$en = strtolower($en);
		$out[$en]=$fr;
	}
	$strdata = var_export($out, true);
	file_put_contents('language.php', $strdata);
}


function getJsonClient()
{
	$httpClient=new httpClient();
	$client=new JsonClient('http://localhost/rbsier/rest', $httpClient);
	$client->call($method='test',$params=array());
	var_dump($httpClient);
}

run();
