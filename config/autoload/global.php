<?php
if(!defined('DEBUG')) define ('DEBUG' , false); /* Activate debug mode */
if(!defined('LOGFILE')) define ('LOGFILE' , 'logs/ranchbe.log'); /*Path to log file. 0 to desactivate log*/
if(!defined('LOGLEVEL')) define ('LOGLEVEL', 'Error'); /*ERROR log only errors, INFO log infos and errors*/
if(!defined('ALLOW_USER_PREFS')) define ('ALLOW_USER_PREFS' , true); /*Allow to control some RanchBE parameters by user*/
if(!defined('DEFAULT_MAX_RECORD')) define ('DEFAULT_MAX_RECORD' , 200); /*Max record to display for request.*/
if(!defined('DEFAULT_LIFE_TIME')) define ('DEFAULT_LIFE_TIME' , 365); /*default life duration in days for all objects. This value permit calcul of the default forseen close date.*/
if(!defined('IMPORT_USE_WILDSPACE')) define ('IMPORT_USE_WILDSPACE', false);
if(!defined('DESACTIVATE')) define ('DESACTIVATE' , false); /* if true display an advertissement when user try to connect */
if(!defined('DESACTIVATE_MESSAGE')) define ('DESACTIVATE_MESSAGE' , 'RanchBE est en cours de maintenance. Réessayer plus tard'); /*message to display when ranchbe is desactivated*/

return array(
	'rbp'=>array(
		'db' => array(
			'adapter' => 'mysql',
			'params'=>array(
				'host' => 'localhost',
				'dbname' => 'ranchbe',
				'username' => 'ranchbe',
				'password' => 'ranchbe',
				'readonlyuser'=> 'rbconsult',
				'readonlypass'=> 'rbconsult',
				'autocommit'=>0
			)
		),
		'auth'=>array(
			/* db, ldap or passall for no authentications */
			'adapter'=>'ldap',
			'options'=>array(
				/* Typical options for Active Directory */
				'server1'=>array(
					'host'=>'ad.mondomain.com',
					'useStartTls'=>false,
					'accountDomainName'=>'mondomain.com',
					'accountDomainNameShort'=>'MONDOMAIN',
					'accountCanonicalForm'=>3,
					'baseDn'=>'DC=mondomain,DC=com',
					'username' => 'CN=rbportail,OU=Utilisateurs système,DC=mondomain,DC=com',
					'password' => 'rbportail',
					//'accountFilterFormat'=>'(&(|(objectclass=user))(|(memberof=CN=Cloud,OU=Utilisateurs système,DC=sierbla,DC=int)))'
				)
			)
		),

		'filesystem.secure'=>false, /* If true limit access to directory defined by path. configuration directives */

		/*Filesystem paths*/
		'path.reposit.workitem' => 'data/reposit/workitem', /* Default dir to stock workitem files */
		'path.reposit.mockup' => 'data/reposit/mockup', /* Default dir to stock mockup files */
		'path.reposit.cadlib' => 'data/reposit/cadlib', /* Default dir to stock cadlib files */
		'path.reposit.bookshop' => 'data/reposit/bookshop', /* Default dir to stock cadlib files */
		'path.reposit.workitem.archive' => 'data/archive/workitem',
		'path.reposit.mockup.archive' => 'data/archive/mockup',
		'path.reposit.cadlib.archive' => 'data/archive/cadlib',
		'path.reposit.bookshop.archive' => 'data/archive/bookshop',

		'path.reposit.wildspace' => 'data/wildspace', //Default dir to put users files

		'path.reposit.import' => 'data/import',
		'path.reposit.unpack' => 'data/import/unpack', //Default dir to put tmp unzip files from a mockup update package before update
		'path.reposit.trash'=>'data/trash', //Default dir to put suppress files. Its better to put this dir on same partition than reposits
		'path.reposit.log' => 'data/log',

		/* Scripts and triggers */
		'path.scripts.doctype' => 'data/scripts/doctypeTriggers', //Default directory where are store doctypes scripts
		'path.scripts.datatype' => 'data/scripts/datatypeTriggers', //Default directory where are store datatype scripts
		'path.scripts.queries' => 'data/scripts/sqlQueries', //Default directory where are stored the sql user queries files
		'path.scripts.document.xlsrenderer' => 'data/scripts/XlsRenderer/Document',
		'path.scripts.stats' => 'data/scripts/statsQueries', //Default directory where are store statistic query scripts

		/* Backup */
		'path.database.backup' =>'',

		/* Filesystem security */
		'path.authorized'=>array(),

		/*Workflow*/
		'path.workflow.process'=>'data/Process/ProcessDef',

		/*Valid extension for visualisation file.*/
		'visu.fileExtension' => array(
			0 => '.pdf',
			1 => '.3dxml',
			2 => '.wrl',
			3 => '.igs',
			4 => '.vda',
			5 => '.stp',
		),
		'visu.roles.searchorder'=>array(),
		'thumbnails.extension'=>'png',
		'thumbnails.path'=>'public/img/thumbnails',
		'thumbnails.url'=>'img/thumbnails',

		/*View And SMARTY*/
		'icons.doctype.compiled.path' => 'public/img/filetypes32/C', /* Default directory where are store doctypes icon */
		'icons.doctype.compiled.url' => 'img/filetypes32/C',
		'icons.doctype.source.path' => 'public/img/filetypes32', /* Default url where are store doctypes icon */
		'icons.doctype.source.url' => 'img/filetypes32',
		'icons.doctype.type' => 'gif',

		'icons.file.path' => 'public/img/filetypes', /* Default directory where are store files icon */
		'icons.file.url' => 'img/filetypes', /* Default directory where are store files icon */

		'view.lang'=>'en', /*Language to use*/
		'view.charset'=>'UTF-8',
		'view.timezone'=>'UMT',
		'view.style'=>'public/styles/jalist.css',
		'view.longdate.format'=>'d-m-Y H:i:s', /* Format to display date with hour, min, sec. */
		'view.shortdate.format'=>'d-m-Y', /* Format to display short date. */
		'view.hour.format'=>'H:i:s', /* Format to display hour. */
		'view.url.shortcuticon'=>'img/favicon.png',
		'view.url.logo'=>'img/logo.png',

		'view.smarty'=>array(
			'template'=>array(
				'path'=>'View/templates/',
				'leftDelimiter'=>'{',
				'rightDelimiter'=>'}',
				'php_handling'=>true
			),
			'compile'=>array(
				'path'=>'data/templates_c',
				'force'=>false,
				'check'=>true
			),
			'cache'=>array(
				'path'=>'data/cache/',
				'enabled'=>0,
				'lifetime'=>3600,
			),
			'config'=>array(
				'path'=>'configs/',
				'overwrite'=>true,
				'booleanize'=>true,
				'read_hidden'=>true,
				'fix_newlines'=>true,
			),
			'debug'=>array(
				'debug'=>false,
				'template'=>'',
				'ctrl'=>'NONE',
			),
			'plugin'=>array(
				'path'=>array(
					'lib/smarty/plugins',
					'View/plugins',
				)
			)
		),

		//Conveter Rbgate
		'rbgate.server.url'=>'http://localhost:8888/rbcs/rbgate',
		'rbgate.application'=>'catia',

		/*Document and objects*/
		'document.assocfile.checkname'=>true, /* If true, the name of file to associate to a document must be begin by the number of the document, else all files name are permit */
		'document.assocfile.backversiontokeep'=>5, /* Number of version to keep for rollback */
		'document.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by document number if no doctype set */
		'document.version.init'=>1, /* Default indice to indicate when create a new doc */
		'document.smartstore.maxsize'=>150, /* define the max number of files to store in one batch */

		'project.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by project number */
		'project.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help display to user when he create a project */

		'workitem.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by number */
		'workitem.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'mockup.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by number */
		'mockup.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'bookshop.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by number */
		'bookshop.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'cadlib.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by number */
		'cadlib.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		'doctype.name.mask'=>'^[a-zA-Z0-9-_.\/]+$', /* Regex to must be verified by number */
		'doctype.name.mask.help'=>'Le numéro ne peut pas être composé de caractères accentués ou d\'espace', /* Help to display to user */

		/*Mail*/
		'mail.smtp' => array(),
		'message.mailbox.maxsize' => 50,
		'message.archive.maxsize' => 500,
		'message.mail.enable'=>true,
		'admin.mail'=>'',

		/*Modules*/
		'module.product'=>true,/* if true, product tab is displayed*/
		'module.project'=>true,/* if true, project tab is displayed*/
		'module.workitem'=>true,/* if true, workitem tab is displayed*/
		'module.cadlib'=>true,/* if true, cadlib tab is displayed*/
		'module.bookshop'=>true,/* if true, bookshop tab is displayed*/
		'module.partner'=>true,/* if true, partner tab is displayed*/
		'module.admin'=>true,/* if true, admin tab is displayed*/

		/*Security*/
		'login.password.minlength'=>10,
		'login.password.mask'=>'', /* Regex to must be verified by the user password*/
		'login.password.mask.help'=>'', /* Help display to user when he modify password*/
		'user.allowprefs'=>false,

		/*Admin*/
		//'unzip.cmd'=>'.\lib\gzip\bin\gzip -d -f', /* Path to exec to uncompress .Z files */
		'unzip.cmd'=>'/usr/bin/gunzip -f', /* Path to exec to uncompress .Z files */
		'backup.dump.cmd'=>'mysqldump', /* Command to dump database */

		/*Session*/
		'session.used' => true,

		/*Anti Flood*/
		'flood.maxsize'=>100, /* Number of tickets to keep in session */
		'flood.timeinterval'=>10, /* in sec, time interval to  */
		'flood.maxrequest'=>50, /* max request in the timeinterval */
		'flood.wait'=>5, /* If flood detected, wait n sec before execute new request */
	));
