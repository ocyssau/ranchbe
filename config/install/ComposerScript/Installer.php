<?php
namespace ComposerScript;

use Composer\Script\Event;

class Installer
{
	public static function postUpdate(Event $event)
	{
	}

	public static function postInstall(Event $event)
	{
		//$installedPackage = $event->getOperation()->getPackage();
		//$composer = $event->getComposer();
		//copy('vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist','config/autoload/zdt.local.php');
		
		if(!is_dir('./public/bootstrap')){
		    mkdir('./public/bootstrap');
		}
		if(!is_dir('./public/bootstrap/js')){
		    mkdir('./public/bootstrap/js');
		}
		if(!is_dir('./public/bootstrap/css')){
		    mkdir('./public/bootstrap/css');
		}
		if(!is_dir('./public/bootstrap/fonts')){
		    mkdir('./public/bootstrap/fonts');
		}
		
		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/css');
		foreach($directory as $file){
		    if($directory->isFile()){
		        copy($directory->getPathname(), './public/bootstrap/css/'.$directory->getFileName());
		    }
		}
		
		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/js');
		foreach($directory as $file){
		    if($directory->isFile()){
		        copy($directory->getPathname(), './public/bootstrap/js/'.$directory->getFileName());
		    }
		}
		
		$directory = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/fonts');
		foreach($directory as $file){
		    if($directory->isFile()){
		        copy($directory->getPathname(), './public/bootstrap/fonts/'.$directory->getFileName());
		    }
		}
		
		//copy tinymce in js
		if(!is_dir('./public/js/tinymce')){
		    mkdir('./public/js/tinymce');
		}
		if(!is_dir('./public/js/jquery')){
		    mkdir('./public/js/jquery');
		}
		if(!is_dir('./public/js/jqueryui')){
		    mkdir('./public/js/jqueryui');
		    mkdir('./public/js/jqueryui/themes');
		}
		
		self::dircopy('vendor/tinymce/tinymce', 'public/js/tinymce', false);
		self::dircopy('vendor/components/jquery/', 'public/js/jquery', false);
		self::dircopy('vendor/components/jqueryui', 'public/js/jqueryui', false);
		//self::dircopy('vendor/components/jqueryui/themes/', 'public/js/jquery-ui/themes', false);
		
		if(!is_dir('./public/js/jstree')){
		    mkdir('./public/js/jstree');
		}
		
		self::dircopy('vendor/vakata/jstree/dist', 'public/js/jstree', false);
		
		if ( !is_file('public/.htaccess') ){
		    copy('config/dist/htaccess.dist', 'public/.htaccess');
		}
		if ( !is_file('config/autoload/local.php') ){
		    copy('config/dist/local.php.dist', 'config/autoload/local.php');
		}
	}

	public static function warmCache(Event $event)
	{
		// make cache toasty
	}

	/**
	 * @param string $src
	 * @param string $target
	 */
	public static function dircopy($src, $target, $verbose=true)
	{
		$directory = new \DirectoryIterator($src);
		foreach($directory as $file){
			if($directory->isFile()){
				if($verbose) echo 'copy '.$directory->getPathname() . ' to ' . $target.'/'.$directory->getFileName() . "\n";
				copy($directory->getPathname(), $target.'/'.$directory->getFileName());
			}
			elseif($directory->getFileName() == '.git'){
				continue;
			}
			elseif( $directory->isDir() && !$directory->isDot() ){
				$dir = $directory->getPathname();
				$targetDir = $target.'/'.$directory->getFileName();
				if($verbose) echo "directory ".$dir." copy to $targetDir \n";
				if( !is_dir($targetDir) ){
					mkdir( $targetDir );
				}
				self::dircopy( $dir, $targetDir );
			}
		}
	}

}
