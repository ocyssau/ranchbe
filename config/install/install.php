<?php
use Zend\Stdlib\ArrayUtils;

/*
Scripts d'installation � executer en ligne de commande avec les droits administrateur:
sudo php install.php
*/

$confPath = dirname(__FILE__);
$appPath = dirname(dirname($confPath));
var_dump($confPath, $appPath);

chdir($appPath);

require_once 'module/Ranchbe/Ranchbe.php';
require_once 'config/boot.php';

$appConfig = include 'config/application.config.php';
rbinit_autoloader($appConfig);

if ( !is_file('config/autoload/local.php') ){
	echo "'config/autoload/local.php' n'existe pas. Créer le en copiant config/dist/local.php.dist vers config/autoload/local.php";
	die;
}

if ( !is_file('public/.htaccess') ){
	copy('config/dist/htaccess.dist', 'public/.htaccess');
}

$ranchbe = new Ranchbe();
$local = include 'config/autoload/local.php';
$global = include 'config/autoload/global.php';
$config = ArrayUtils::merge($global,$local);
$ranchbe->setConfig($config['rbp']);

$config = $config['rbp'];

if (!is_dir ($config['path.reposit.wildspace'])){
	mkdir($config['path.reposit.wildspace'], 0777, true);
}
if (!is_dir ($config['path.reposit.workitem'])){
	mkdir($config['path.reposit.workitem'], 0777, true);
}
if (!is_dir ($config['path.reposit.mockup']) ) {
	mkdir($config['path.reposit.mockup'], 0777, true);
	}
if (!is_dir ($config['path.reposit.cadlib']) ) {
	mkdir($config['path.reposit.cadlib'], 0777, true);
}
if (!is_dir ($config['path.reposit.bookshop']) ) {
	mkdir($config['path.reposit.bookshop'], 0777, true);
}
if (!is_dir ($config['path.reposit.trash']) )  {
	mkdir($config['path.reposit.trash'], 0777, true);
}
if (!is_dir ($config['path.reposit.import']) ){
	mkdir($config['path.reposit.import'], 0777, true);
}
if (!is_dir ($config['path.reposit.unpack']) ) {
	mkdir($config['path.reposit.unpack'], 0777, true);
}
if (!is_dir ($config['path.reposit.workitem.archive']) ) {
	mkdir($config['path.reposit.workitem.archive'], 0777, true);
}
if (!is_dir ($config['path.reposit.mockup.archive']) ) {
	mkdir($config['path.reposit.mockup.archive'], 0777, true);
}
if (!is_dir ($config['path.reposit.cadlib.archive']) ) {
	mkdir($config['path.reposit.cadlib.archive'], 0777, true);
}
if (!is_dir ($config['path.reposit.bookshop.archive']) ) {
	mkdir($config['path.reposit.bookshop.archive'], 0777, true);
}
if (!is_dir ($config['view.smarty']['compile']['path']) ) {
    mkdir($config['view.smarty']['compile']['path'], 0777, true);
}
