#!/bin/bash

git config --global credential.helper 'cache --timeout=28800'

//installation of bower
sudo apt-get install nodejs
sudo apt-get install npm
sudo npm install -g bower
sudo ln -s /usr/bin/nodejs /usr/bin/node

sudo apt-get install curl apache2 php5 postgresql php5-pgsql postgresql-contrib pgadmin3
sudo apt-get install php5-xdebug
sudo apt-get install php5-uuid

curl -s https://getcomposer.org/installer | php --
php composer.phar self-update
php composer.phar install
cp vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist config/autoload/zdt.local.php

if [ -d /var/www/html ]
then 
	if [ ! -d /var/www/html/rbportail ]
	then 
		sudo ln -s `pwd`/public /var/www/html/rbportail
	fi
else 
	if [ ! -d /var/www/rbportail ]
	then 
		sudo ln -s `pwd`/public /var/www/rbportail
	fi
fi

if [ ! -e /etc/apache2/sites-available/rbportail.conf ]
then 
	sudo cp ./config/dist/apache.conf /etc/apache2/sites-available/rbportail.conf
fi
sudo gedit /etc/apache2/sites-available/rbportail.conf

sudo a2ensite rbportail
sudo a2enmod rewrite
sudo service apache2 restart

sudo -u postgres dropdb --interactive rbportail
sudo -u postgres createuser -P -e --createrole rbportail
sudo -u postgres createdb rbportail --owner rbportail
sudo -u postgres psql -d rbportail <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF

php ./vendor/Rb/rbp.cli.php --initdb

echo "Copier et editer le fichier de config ./config/autoload/local.php.dist -> local.php"
if [ ! -e ./config/autoload/local.php ]
then 
	cp ./config/autoload/local.php.dist ./config/autoload/local.php
fi
gedit ./config/autoload/local.php

echo "Installation de zf tool"
php composer.phar require zendframework/zftool:dev-master
ln -s vendor/bin/zf.php zf.php

if [ -d public/js/jquery ]
then
	wget http://code.jquery.com/jquery-2.1.3.min.js
	mkdir public/js/jquery
	mv jquery-2.1.3.min.js public/js/jquery/jquery.min.js
fi

if[ -d public/js/jquery-ui ]
then
	wget http://jqueryui.com/resources/download/jquery-ui-1.11.2.zip
	unzip jquery-ui-1.11.2.zip
	rm jquery-ui-1.11.2.zip
	mkdir public/js/jquery-ui
	cp jquery-ui-1.11.2/jquery-ui.min.js public/js/jquery-ui/.
	cp jquery-ui-1.11.2/jquery-ui.min.css public/css/.
	mv jquery-ui-1.11.2 vendor/.
fi

if[ -d vendor/jquery-editInPlace-v2.2.1 ]
then
	wget https://jquery-in-place-editor.googlecode.com/files/jquery-editInPlace-v2.2.1.zip
	unzip jquery-editInPlace-v2.2.1.zip
	rm jquery-editInPlace-v2.2.1.zip
	chmod -R 777 jquery-editInPlace-v2.2.1
	cp jquery-editInPlace-v2.2.1/lib/jquery.editinplace.js public/js/.
	mv jquery-editInPlace-v2.2.1 vendor/.
fi

if[ -d vendor/eternicode-bootstrap-datepicker-809a5c2 ]
then
	bower install bootstrap-datepicker
	
	wget https://github.com/eternicode/bootstrap-datepicker/zipball/1.3.1
	mv 1.3.1 datepicker.zip
	unzip datepicker.zip
	rm datepicker.zip
	cp -r eternicode-bootstrap-datepicker-809a5c2/js public/js/datepicker
	cp -r eternicode-bootstrap-datepicker-809a5c2/css public/css/datepicker
	mv eternicode-bootstrap-datepicker-809a5c2 vendor/.
fi

wget https://github.com/jssor/slider/archive/master.zip
unzip master.zip
if [ -d public/js/jssor ] 
then
	cp -r slider-master/js/*.js public/js/jssor/.
else
	mkdir public/js/jssor
	cp -r slider-master/js/*.js public/js/jssor/.
fi
rm master.zip
mv slider-master vendor/.

if[ -d vendor/codemirror ]
then
	bower install codemirror
	cp -r bower_components/codemirror public/js/codemirror
fi

