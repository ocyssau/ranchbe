#!/bin/bash

cd ../..

echo "Input the proxy host or let blank if no proxy";
read proxyhost

if [ ! -z "$proxyhost" ]
then
	echo "Input the proxy port"
	read proxyport

	echo "Input the proxy user or let blank if none"
	read proxyuser

	if [ ! -z $proxyuser ]
	then
		export proxyauth=$proxyuser

		echo "Input the proxy passwd or let blank if none"
		read proxypasswd
		if [ ! -z $proxypasswd ]
		then
			export proxyauth=$proxyauth:$proxypasswd
		fi
		export proxyauth=$proxyauth@
	fi

	export http_proxy=http://$proxyauth$proxyhost:$proxyport
	export https_proxy=http://$proxyauth$proxyhost:$proxyport
	export HTTP_PROXY=http://$proxyauth$proxyhost:$proxyport
	export HTTPS_PROXY=http://$proxyauth$proxyhost:$proxyport
fi

sudo php5dismod xdebug
php installer
php composer.phar self-update
php composer.phar install
sudo php5enmod xdebug

sudo apt-get install curl apache2 php5
sudo apt-get install php5-xdebug
sudo apt-get install libssh2-1-dev libssh2-php
sudo php5enmod xdebug
sudo a2enmod rewrite
sudo service apache2 restart

if [ ! -f /etc/apache2/sites-available/rbsier.conf ]
then
	sudo cp config/dist/apache.conf /etc/apache2/sites-available/rbsier.conf
	sudo nano /etc/apache2/sites-available/rbsier.conf
fi

echo "Installation de zf tool"
#php composer.phar require zendframework/zftool:dev-master
#ln -s vendor/bin/zf.php zf.php
#cp vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist config/autoload/zdt.local.php

git config --global credential.helper 'cache --timeout=28800'

//installation of bower
sudo apt-get install nodejs
sudo apt-get install npm
sudo npm install -g bower
sudo ln -s /usr/bin/nodejs /usr/bin/node

chmod -R 755 *
chmod -R 777 data/
sudo php install.php
chmod 777 View/templates/

if [ ! -d lib/adodb]
then
	mkdir lib/adodb
fi
if [ ! -f lib/adodb/adodb.inc.php]
then
	#wget http://sourceforge.net/projects/adodb/files/adodb-php5-only/adodb-519-for-php5/adodb519.tar.gz
	#tar zxvf adodb519.tar.gz
	#mv adodb5 adodb

	wget http://downloads.sourceforge.net/project/adodb/adodb-php-4-and-5/adodb-493-for-php/adodb493.zip
	unzip adodb493.zip
	chmod -R 755 adodb
fi


if [ ! -f public/js/jquery.editinplace.js ]
then
	wget https://jquery-in-place-editor.googlecode.com/files/jquery-editInPlace-v2.2.1.zip
	unzip jquery-editInPlace-v2.2.1.zip
	rm jquery-editInPlace-v2.2.1.zip
	chmod -R 777 jquery-editInPlace-v2.2.1
	cp jquery-editInPlace-v2.2.1/lib/jquery.editinplace.js public/js/.
	rm -r jquery-editInPlace-v2.2.1
fi

#For dev
#ln -s ~/git/rbservice/rbService/vendor/Rbs/ lib/Rbs
#ln -s ~/git/rbplm3/rbplm3/vendor/Rbplm/ lib/Rbplm
ln -s `pwd`/vendor/ocyssau/rbplm3/rbplm3/module/Rbplm/src/Rbplm lib/Rbplm

#jquery-md5
cd public/js
git clone https://github.com/gabrieleromanato/jQuery-MD5
cd ../..

#threejs
cd public/js
git clone https://github.com/mrdoob/three.js.git
cd ../..

#Database init
mysqladmin create rbsier -u root -p
mysql -u root -p rbsier < Docs/db/exportStructureRbsier.sql
mysql -u root -p rbsier < Docs/db/mysql/datas.sql
mysql -u root -p rbsier < Docs/db/inituser.sql
mysql -u root -p rbsier < Docs/db/mysql/sequences.sql
mysql -u root -p rbsier < Docs/db/from0.6.1TO0.6.2.sql
