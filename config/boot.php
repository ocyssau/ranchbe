<?php
//%LICENCE_HEADER%
rbinit_includepath();

use Rbplm\People;
use Rbs\Dao\Factory as DaoFactory;
use Rbs\Auth;
use Rbplm\Rbplm;
use Rbplm\Dao\Connexion;
use Rbplm\Sys\Filesystem;
use Rbs\Sys\ErrorStack;

use Zend\Authentication\AuthenticationService;

function rbinit_filesystem()
{
	$ranchbe = Ranchbe::get();

	$user = Ranchbe::getCurrentUser();
	if($user){
		$wildspace = new \Rbplm\People\User\Wildspace($user);
		Filesystem::addAuthorized($wildspace->getPath());
		$wildspace->init();
	}

	if($ranchbe->getConfig('filesystem.secure')){
		Filesystem::isSecure(true);
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.workitem')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.mockup')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.cadlib')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.bookshop')));

		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.workitem.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.mockup.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.cadlib.archive')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.bookshop.archive')));

		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.trash')));

		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.import')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.reposit.unpack')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.doctype')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.datatype')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.queries')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.xlsrender')));
		Filesystem::addAuthorized(realpath($ranchbe->getConfig('path.scripts.stats')));

		foreach($ranchbe->getConfig('path.authorized') as $path){
			Filesystem::addAuthorized(realpath($path));
		}
	}
	else{
		Filesystem::isSecure(false);
	}
}

/**
 * Init constants for web interface
 *
 * @return void
 */
function rbinit_web()
{
	//Disable auto register global of sessions variables.
	//Note that register_globals must be set to false too.
	ini_set('session.bug_compat_42', false);

	define('CRLF',"<br />");

	if( getenv('BASEURL') ){
		define('BASEURL', getenv('BASEURL') );
	}
	else{
		define('BASEURL', '' );
	}
}

/**
 *
 */
function rbinit_daoservice($config)
{
	if(is_file('config/autoload/objectdaomap.local.php')){
		$map = include 'config/autoload/objectdaomap.local.php';
	}
	else{
		$map = include 'config/dist/objectdaomap.config.php';
	}

	Connexion::setConfig($config['db']);
	$connexion = Connexion::get();

	$factory = DaoFactory::get();
	$factory->setMap($map['All']);
	$factory->setConnexion($connexion);
	$factory->setOption('listclass', '\Rbs\Dao\Sier\DaoList');
	$factory->setOption('filterclass', '\Rbs\Dao\Sier\Filter');

	\Rbs\Space\Factory::setMap($map);
	\Rbs\Space\Factory::setConnexion($connexion);
	\Rbs\Space\Factory::setOption('listclass', '\Rbs\Dao\Sier\DaoList');
	\Rbs\Space\Factory::setOption('filterclass', '\Rbs\Dao\Sier\Filter');

	\Rbs\Sys\Session::setConnexion($connexion);
}

/**
 *
 */
function rbinit_rbservice($config)
{
	rbinit_daoservice($config);

	Rbplm::setConfig($config);
	\Rbplm\Sys\Filesystem::isSecure(false);

	\Rbplm\Sys\Trash::init($config['path.reposit.trash']);
	\Rbplm\Uuid::init('UniqId');
	People\User\Wildspace::$basePath = $config['path.reposit.wildspace'];

	/* pour changer tous les messages d'erreurs en ErrorException */
	set_error_handler("rbinit_exception_error_handler", E_ERROR);
}

/**
 *
 * @param unknown_type $errno
 * @param unknown_type $errstr
 * @param unknown_type $errfile
 * @param unknown_type $errline
 * @throws ErrorException
 */
function rbinit_exception_error_handler($errno, $errstr, $errfile, $errline ) {
	throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

/**
 *
 */
function rbinit_rbplmuser()
{
	//DaoFactory::get()->setMap(include('config/autoload/objectdaomap.local.php'));
	//$connectedUserId = $_SESSION['ludata']['auth']['propertyValues']['auth_user_id'];
	//$user = new People\User();
	//DaoFactory::get()->getDao($user)->loadFromId($user, $connectedUserId);
	//People\CurrentUser::set($user);
}

/**
 *
 */
function rbinit_module()
{
}

/**
 * Init constants for command line interface
 */
function rbinit_cli()
{
	if (stristr($_SERVER['HTTP_USER_AGENT'] , 'windows')){
		define('CLIENT_OS', 'WINDOWS');
		define('PHP_EOL', '\r\n');
	}
	else if (stristr($_SERVER['HTTP_USER_AGENT'] , 'macintosh') || stristr($_SERVER['HTTP_USER_AGENT'] , 'mac_powerpc')){
		define('CLIENT_OS', 'MACINTOSH');
		define('PHP_EOL', '\r');
	}
	else{
		define('CLIENT_OS', 'UNIX');
		define('PHP_EOL', '\n');
	}
	define('CRLF',PHP_EOL);
}

/**
 * Init constants for sessions
 */
function rbinit_session($config)
{
	$useSession = false;

	if( getenv('USESESSION') == 'On' ){
		$useSession = true;
	}
	else{
		$useSession = $config['session.used'];
	}

	if( $useSession ){
		ini_set('session.use_cookies', 1);
		ini_set('session.use_only_cookies', 1);
	}
}

/**
 * Init a autoloader.
 * Not use in application, prefer Zend_Autoload.
 * If $zend = true, init a Zend_Loader_Autoloader
 * else add _rbinit_loader in spl_autoload_register()
 *
 * @param boolean $zend	[OPTIONAL]
 */
function rbinit_autoloader($config)
{
	$loader = include 'vendor/autoload.php';
	$loader->set('Application\\', 'module/Application/src');

	foreach($config['modules'] as $module){
		$loader->set($module.'\\', 'module/'.$module.'/src');
	}

	$loader->set('Controller\\', '.');
	$loader->set('View\\', '.');
	$loader->set('Form\\', '.');
	$loader->set('Rbplm\\', 'lib');
	$loader->set('XlsRenderer\\', 'data/scripts/');

	return $loader;
}

/**
 * Init include path
 */
function rbinit_includepath()
{
	// Ensure library/ is on include_path
	set_include_path ( implode ( PATH_SEPARATOR,
		array (
			realpath('./lib/adodb'),
			realpath('./lib/PEAR'),
			realpath('./lib'),
			get_include_path()
		)
	));
}

/**
 * Init logger
 */
function rbinit_logger()
{
	$logger = \Rbplm\Sys\Logger::singleton();
	$logger->setApplicationName('Ranchbe');

	$usr = Ranchbe::getCurrentUser();
	if($usr){
		$handle = $usr->getLogin();
	}
	$logger->setPrefix("[$handle]");
	Ranchbe::setLogger($logger);
}

/**
 * Init logger
 */
function rbinit_errorStack()
{
	rbinit_logger();
	$errorStack = new ErrorStack('ranchbe');
	Ranchbe::setErrorStack($errorStack);
}

/**
 *
 */
function rbinit_checkInstall($config)
{
	if(ini_get('register_globals') == true){
		print 'Before use RanchBE you must set Register_global php directive to off in php.ini file.
		You can too set this directive in .htaccess file. See php documentation.';
	}

	//------------------------------------------------------------------------
	//Check that necessary directories exists
	//------------------------------------------------------------------------
	/*
	if (!is_dir ($config['path.reposit.wildspace'])){
		print 'Error : '.$config['path.reposit.wildspace'].'  do not exits'.CRLF;
		$wrong=true;
	}
	*/
	if (!is_dir ($config['path.reposit.workitem'])){
		print 'Error : '.$config['path.reposit.workitem'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.mockup']) ) {
		print 'Error : '.$config['path.reposit.mockup'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.cadlib']) ) {
		print 'Error : '.$config['path.reposit.cadlib'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.bookshop']) ) {
		print 'Error : '.$config['path.reposit.bookshop'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.trash']) )  {
		print 'Error : '.$config['path.reposit.trash'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.import']) ){
		print 'Error : '.$config['path.reposit.import'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.unpack']) ) {
		print 'Error : '.$config['path.reposit.unpack'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.workitem.archive']) ) {
		print 'Error : '.$config['path.reposit.workitem.archive'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.mockup.archive']) ) {
		print 'Error : '.$config['path.reposit.mockup.archive'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.cadlib.archive']) ) {
		print 'Error : '.$config['path.reposit.cadlib.archive'].'  do not exits'.CRLF;
		$wrong=true;
	}
	if (!is_dir ($config['path.reposit.bookshop.archive']) ) {
		print 'Error : '.$config['path.reposit.bookshop.archive'].'  do not exits'.CRLF;
		$wrong=true;
	}

	if($wrong){
		die;
	}
}

/**
 *
 */
function rbinit_enableDebug($enable=true)
{
	if($enable){
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);

		ini_set('xdebug.show_local_vars', 0);
		ini_set('xdebug.dump_undefined', 1); //Display undefined var too
		//ini_set('xdebug.dump.POST', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		//ini_set('xdebug.dump.GET', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();

		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 100);
		ini_set('xdebug.var_display_max_data', 1500);
		ini_set('xdebug.var_display_max_depth', 3);
		//ini_set('xdebug.max_nesting_level', 10); //tree nested levels of array elements and object relations are displayed

		ini_set('xdebug.show_exception_trace' , 0);
		ini_set('xdebug.trace_format' , 0); //1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
	}
	else{
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT);
		ini_set('display_errors', 0 );
		ini_set('display_startup_errors', 1 );
		ini_set('xdebug.show_local_vars', 0);
		ini_set('xdebug.dump_undefined', 0); //Display undefined var too
		ini_set('xdebug.dump.POST', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.dump.GET', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
		ini_set('xdebug.collect_assignments', 1);
		ini_set('xdebug.collect_params', 4);
		ini_set('xdebug.collect_return', 1);
		ini_set('xdebug.collect_vars', 1);
		ini_set('xdebug.var_display_max_children', 10);
		ini_set('xdebug.var_display_max_data', 50);
		ini_set('xdebug.var_display_max_depth', 3);
		ini_set('xdebug.max_nesting_level', 10); //three nested levels of array elements and object relations are displayed
		ini_set('xdebug.show_exception_trace' , 0);
		ini_set('xdebug.trace_format' , 0); //1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
	}
}

/**
 *
 */
function rbinit_authservice($config)
{
	$authAdapter = Auth\Factory::getAdapter($config['auth']);
	$authService = new AuthenticationService();
	$authService->setAdapter($authAdapter);

	$storage = new Auth\Storage('rbportail');
	$authService->setStorage($storage);
	Ranchbe::setAuthservice($authService);
}

/**
 *
 */
function rbinit_currentUser()
{
	$authservice = Ranchbe::getAuthservice();
	$storage = $authservice->getStorage();

	if ( $authservice->hasIdentity() ){
		$userProperties = $storage->read();
		$user = new People\User();
		$user->hydrate($userProperties);
		$user->setLastLogin(new \DateTime());
		People\CurrentUser::set($user);
		Ranchbe::setCurrentUser($user);

		if($user->isLoaded()){
			DaoFactory::get()->getDao($user)->updateLastLogin($user);
		}

		rbinit_acl($user);
	}
}

/**
 *
 */
function rbinit_acl($user)
{
	$acl = new \Application\Model\Acl\Application();
	$user->appacl = $acl;

	/*
	 if(!isset($user->appacl)){
	$appAcl = new Acl\Application();
	$appAcl->loadRole(People\CurrentUser::get());

	$session =& $_SESSION[$this->storage->getNamespace()][$this->storage->getMember()];
	$session['appacl'] = $appAcl;
	$user->appacl = $appAcl;
	}
	*/
}

/**
 *
 */
function rbinit_userprefs($config)
{
	$authservice = Ranchbe::getAuthservice();
	$allowPrefs = $config['user.allowprefs'];

	if( $allowPrefs && $authservice->hasIdentity() ){
		$usr = People\CurrentUser::get();
		try{
			$preference = new People\User\Preference();
			DaoFactory::get()->getDao($preference->cid)->loadFromOwnerId($preference, $usr->getId());
			$prefs = $preference->getPreferences();
		}
		catch(\Exception $e){
			$prefs = $preference->getPreferences();
		}

		if( !empty($prefs['css_sheet']) && $prefs['css_sheet'] != 'default' ){
			$config['view.style'] = $prefs['css_sheet'];
		}
		if( !empty($prefs['lang']) && $prefs['lang'] != 'default' ){
			$config['view.lang'] = $prefs['lang'];
		}
		if( !empty($prefs['long_date_format']) && $prefs['long_date_format'] != 'default' ){
			$config['view.longdate.format'] = $prefs['long_date_format'];
		}
		if( !empty($prefs['short_date_format']) && $prefs['short_date_format'] != 'default' ){
			$config['view.shortdate.format'] = $prefs['short_date_format'];
		}
		if( !empty($prefs['time_zone']) && $prefs['time_zone'] != 'default' ){
			$config['view.timezone'] = $prefs['time_zone'];
		}
		if( !empty($prefs['time_zone']) && $prefs['time_zone'] != 'default' ){
			$config['view.timezone'] = $prefs['time_zone'];
		}
		if( !empty($prefs['max_record']) && $prefs['max_record'] != 'default' ){
			$config['view.maxrecord'] = $prefs['max_record'];
			define(MAX_RECORD , $prefs['max_record']);
		}
		unset($prefs);
	}
	else{
		define(MAX_RECORD , $config['view.maxrecord']);
	}
}

/**
 *
 * @param array $config
 */
function rbinit_view($config)
{
	Ranchbe::setView(rbinit_viewFactory($config));
}

/**
 * Return a new Smarty instance
 */
function rbinit_viewFactory($config)
{
	$ranchbe = Ranchbe::get();
	$view = new View\View($config['view.smarty']);

	/* Set the default charset */
	ini_set('default_charset' , $config['view.charset']);

	/* Assign value of the version to smarty */
	$view->assign('ranchbe_version', $ranchbe::VERSION);
	$view->assign('ranchbe_build', $ranchbe::BUILD);
	$view->assign('ranchbe_copyright', $ranchbe::COPYRIGHT);

	$view->assign('css_sheet', $config['view.style']);
	$view->assign('charset', $config['view.charset']);
	$view->assign('shortcut_icon', $config['view.url.shortcuticon'] );
	$view->assign('logo', $config['view.url.logo'] );
	$view->assign('allow_user_prefs', $config['user.allowprefs'] );
	$view->assign('lang', $config['view.lang'] );
	Ranchbe::$lang = $config['view.lang'];

	$currentUser = Ranchbe::getCurrentUser();

	$view->assign('current_user_name', People\CurrentUser::get()->getLogin());
	$view->assign('baseurl', BASEURL);
	$view->assign('baseJsUrl', BASEURL.'/public/js');
	$view->assign('baseCssUrl', BASEURL.'/public/css');
	$view->assign('baseImgUrl', BASEURL.'/img');
	$view->assign('baseCustomImgUrl', BASEURL.'/data/img');
	$view->assign('doctypeIconBaseUrl', BASEURL.'/img/filetypes32/C');
	$view->assign('thumbnailBaseurl', $config['thumbnails.url']);
	$view->assign('iconsFileBaseurl', $config['icons.file.url']);
	$view->assign('rbgateServerUrl', $config['rbgate.server.url']);

	define('SHORT_DATE_FORMAT', $config['view.shortdate.format']);
	define('LONG_DATE_FORMAT', $config['view.longdate.format']);

	return $view;
}

/**
 *
 * @param boolean $bool
 */
function rbinit_unactive($bool){
	if($bool === true){
		// Display the template
		$smarty = Ranchbe::getView();
		$smarty->assign('isDesctivated_message', DESACTIVATE_MESSAGE); //Assign variable to smarty
		$smarty->assign('accueilTab', 'active');
		$smarty->assign('mid', 'isDesactivated.tpl');
		$smarty->display('layouts/ranchbe.tpl');
		die;
	}
}

/**
 * Init the context
 */
function rbinit_context()
{
	$session = \Rbs\Sys\Session::get();
	if(!isset($session->containerId)){
		$session->load( People\CurrentUser::get()->getId() );
	}
}

/*
 * Serialize the $_request and create hidden fom
*/
function serialize_request_post(){
	$html ='';
	foreach( $_REQUEST as $key=>$val ){
		if($key == 'logout' || $key == 'handle' || $key == 'passwd' || $key == 'handle' || $key == 'tab' ||
			$key == 'kt_language' || $key == 'tz_offset' || $key == 'username' || $key == 'ml_id' || $key == 'PHPSESSID' || $key == 'LuSession'
		){
			continue;
		}
		if( is_array($val) ){
			foreach( $val as $sub_key=>$sub_sval ){
				$html .= '<input type="hidden" name="'.$key.'['.$sub_key.']'.'" value="'.$sub_sval.'" />'."\n";
			}
		}else{
			$html .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />'."\n";
		}
	}
	return $html;
}


function check_flood($page_id='')
{
	//Anti-flood V3. Condition existance du ticket en session. Tous les tickets sont conserves en session jusqu'a leur utilisation.
	//Inspired by principle described in http://www.developpez.net/forums/archive/index.php/t-104783.html
	//If session jeton is egal to jeton record in form, generate a new jeton.
	//session_register('jeton');

	$smarty = Ranchbe::getView();

	//Check time between 2 requests
	if (time() - $_SESSION['ticket_time'] < 1){ // If the page is recall after time < to one second
	}
	else{
		$_SESSION['ticket_time'] = time();
	}

	if(empty($_REQUEST['ticket'])){ //init the ticket
		$ticket = md5(uniqid(mt_rand(), true));
		$_SESSION['ticket'][$ticket] = true;
		$smarty->assign('ticket', $ticket); //Regenerate a ticket and assign it to page
		return false;
	}

	if (isset($_SESSION['ticket'][$_REQUEST['ticket']])){ //Check if ticket is in SESSION
		unset($_SESSION['ticket'][$_REQUEST['ticket']]); // Suppress ticket in session
		$ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
		$_SESSION['ticket'][$ticket] = true; // Record new ticket in session
		$smarty->assign('ticket', $ticket); // Assign it to page
		return true;
	}
	else{
		$ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
		$_SESSION['ticket'][$ticket] = true; // Record new ticket in session
		$smarty->assign('ticket', $ticket); // Assign it to page
		return false;
	}

	return true;

}

/** translate a English string
 * @param $content - English string
 * @param $lg - language - if not specify = global current language
 */
function tra($txt) {
	return Ranchbe::tra($txt);
}
