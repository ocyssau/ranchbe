<?php
namespace Form\Partner;

use Rbplm\Dao\Pg\Filter;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param project $project
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$action = ($action == '') ? $_SERVER['REDIRECT_URL'] : $action;
		\HTML_QuickForm::__construct('FilterForm', 'POST', $action);
		$this->setAttribute('class','form-inline');
		$this->view = $view;
		$this->factory = $factory;
		$this->nameSpace = $nameSpace;
		$this->template = 'partner/filter.tpl';
		$this->elemtFactory = new \Form\ElementFactory($this, $factory);


		//Find in field ----------------------------------------------
		$list = array (
			'first_name' => 'first_name',
			'last_name' => 'last_name',
			'partner_type' => 'partner_type',
			'adress' => 'adress',
			'city' => 'city' ,
			'zip_code' => 'zip_code',
			'phone' => 'phone',
			'cell_phone' => 'cell_phone',
			'mail' => 'mail',
			'web_site' => 'web_site',
			'activity' => 'activity',
			'company' => 'company',
		);
		$this->elemtFactory->select($list, array(
			'name'=>'find_field',
			'description'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Find in field ----------------------------------------------
		$this->addElement( 'text', 'find', 'find', array('size'=>16) );
		$this->addElement( 'text', 'find_number', 'Number', array('size'=>16) );

		//Select action by user ----------------------------------------------
		$actions = array(
			'open_by'=>'Created By',
		);
		$this->elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'description'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_number']){
			$filter->andFind($defaults['find_number'], 'partner_number', Op::OP_CONTAINS);
		}

		//FIND
		if($defaults['find'] && $defaults['find_field']){
			$filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
		}

		return $this;
	}

}
