<?php
namespace Form\Partner;

class EditForm extends \Form\AbstractForm
{

	/**
	 *
	 */
	public function __construct($view, $factory)
	{
		$name = 'PartnerEdit';
		parent::__construct($name, $view, $factory);
		$this->template = 'partner/editform.tpl';

		//Add hidden fields
		$this->addElement('hidden', 'id', '');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//FIRST NAME
		$this->addElement('text', 'firstname', tra('First Name'), array('class'=>'form-control','autocomplete'=>'off'));

		//LAST NAME
		$this->addElement('text', 'lastname', tra('Last Name'), array('class'=>'form-control','autocomplete'=>'off'));

		//MAIL
		$this->addElement('text', 'mail', tra('Mail'), array('class'=>'form-control','autocomplete'=>'off'));

		//ADRESS
		$this->addElement('text', 'adress', tra('Adress'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'city', tra('City'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'zipcode', tra('Zipcode'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'phone', tra('Phone'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'cellphone', tra('Cell Phone'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'website', tra('Web Site'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'activity', tra('Activity'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('text', 'company', tra('Company'), array('class'=>'form-control','autocomplete'=>'off'));

		//Construct array for selection set
		$selectSet = array(
			null=>'',
			'customer'=>tra('Customer'),
			'supplier'=>tra('Supplier'),
			'staff'=>tra('Staff'),
		);
		$this->addElement('select', 'type' , tra('Type'), $selectSet );

		//Add validation rules to check input data
		$this->addRule('type', tra('Partner type is required'), 'required');
		$this->applyFilter('__ALL__', 'trim');
		$this->applyFilter('lastname', 'mb_strtoupper');
		$this->applyFilter('firstname', 'mb_strtolower');
		$this->applyFilter('firstname', 'ucfirst');
		$this->applyFilter('uid', 'mb_strtoupper');
	}

}
