<?php
namespace Form\Role;

class EditForm extends \Form\AbstractForm
{

	/**
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('RoleEdit', 'POST');
		$this->template = 'user/roleadmin/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		//Add hidden fields
		$this->addElement('hidden', 'id');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//NAME
		$this->addElement('text', 'name', tra('Role Name'), array('class'=>'form-control','autocomplete'=>'off'));

		//DESCRIPTION
		$this->addElement('text', 'description', tra('Description'), array('class'=>'form-control','autocomplete'=>'off'));
	}
}
