<?php
namespace Form\Role;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;
use Rbplm\People;

class FilterForm extends \Form\Filter\AbstractFilterForm
{

	/**
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		\HTML_QuickForm::__construct('FilterForm', 'POST', '#');
		$this->setAttribute('class','form-inline');
		$this->view = $view;
		$this->factory = $factory;
		$this->nameSpace = $nameSpace;
		$this->template = 'role/filter.tpl';

		//NAME
		$this->addElement( 'text', 'find_name', 'Name', array('size'=>16, 'class'=>'form-control') );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		$dao = $this->factory->getDao(People\Group::$classId);

		//NAME
		if($defaults['find_name']){
			$filter->andFind($defaults['find_name'], $dao->toSys('name'), Op::CONTAINS);
		}

		return $this;
	}
}
