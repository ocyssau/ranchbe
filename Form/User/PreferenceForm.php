<?php
namespace Form\User;

class PreferenceForm extends \Form\AbstractForm
{

	/**
	 * @param unknown_type $thisName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('PreferenceEdit', 'POST');
		$this->template = 'user/preference/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		//Add hidden fields
		$this->addElement('hidden', 'id');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//LONG DATE
		$longDateFormatSet = array(
				'%d-%m-%Y %H:%M:%S'=>'28-12-1969 12:45:20',
				'%d/%m/%Y %Hh%Mmn%Ss'=>'28/12/1969 12h45mn20s',
				'%d/%m/%Y %Hh%M'=>'28/12/1969 12h45',
				'%d/%m/%y %Hh%M'=>'28/12/69 12h45',
				'%Y-%m-%d %H:%M:%S'=>'1969-12-28 12:45:20',
				'%d-%m-%Y'=>'29-12-1969',
				'%Y-%m-%d'=>'1969-12-28',
				'default' => tra('default'),
		);
		$select = $this->addElement('select', 'longDateFormat', tra('Long Date Format'), $longDateFormatSet, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		//SHORT DATE
		$shortDateFormatSet = array(
				'%d-%m-%Y'=>'28-12-1969',
				'%Y-%m-%d'=>'1969-12-28',
				'%d-%m-%y'=>'28-12-69',
				'%y-%m-%d'=>'69-12-28',
				'default' =>tra('default'),
		);
		$select = $this->addElement('select', 'shortDateFormat', tra('Short Date Format'), $shortDateFormatSet, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		//CSS
		$cssSheetSet = glob('styles/*.css');
		$cssSheetSet = array_combine($cssSheetSet,$cssSheetSet);
		$cssSheetSet['default'] = tra('default');
		$select = $this->addElement('select', 'cssSheet', tra('Style'), $cssSheetSet, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		//LANG
		$langSet = array('fr'=>tra('French'),'en'=>tra('English'),'default'=>tra('default'));
		$select = $this->addElement('select', 'lang', tra('Language'), $langSet, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		//MAX RECORDS
		$maxRecordSet = array(50,100,200,300,400,500);
		$maxRecordSet = array_combine($maxRecordSet,$maxRecordSet);
		$maxRecordSet['default'] = tra('default');
		$select = $this->addElement('select', 'maxRecord', tra('Max Record For Lists'), $maxRecordSet, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);
	}
}
