<?php
namespace Form\User;

class EditForm extends \Form\AbstractForm
{

	/**
	 * @param unknown_type $thisName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('CategoryEdit', 'POST');
		$this->template = 'user/useradmin/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		//Add hidden fields
		$this->addElement('hidden', 'id');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//LOGIN
		$this->addElement('text', 'login', tra('Login'), array('class'=>'form-control','autocomplete'=>'off'));

		//FIRST NAME
		$this->addElement('text', 'firstname', tra('First Name'), array('class'=>'form-control','autocomplete'=>'off'));

		//LAST NAME
		$this->addElement('text', 'lastname', tra('Last Name'), array('class'=>'form-control','autocomplete'=>'off'));

		//MAIL
		$this->addElement('text', 'mail', tra('Mail'), array('class'=>'form-control','autocomplete'=>'off'));

		$this->addRule('mail', 'Mail is not valid', 'email');
		$this->addRule('mail', 'Mail is required', 'required');
		$this->addRule('firstname', 'Firstname is required', 'required');
		$this->addRule('login', 'Login is required', 'required');
	}
}
