<?php
namespace Form\User;

class PasswordForm extends \Form\AbstractForm
{

	/**
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('CategoryEdit', 'POST');
		$this->template = 'user/useradmin/passwordform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		//Add hidden fields
		$this->addElement('hidden', 'id');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default','autocomplete'=>'off'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success','autocomplete'=>'off'));

		//PASSWORD
		$this->addElement('password', 'password1', tra('Password'), array('class'=>'form-control','autocomplete'=>'off'));
		$this->addElement('password', 'password2', tra('Re-Password'), array('class'=>'form-control','autocomplete'=>'off'));

		$this->addRule(array('password1', 'password2'), 'The passwords do not match', 'compare', null, 'server');
		$this->addRule('password1', 'Password is required', 'required');
		//$this->addRule('password1', 'Password is too short', 'minlength', MIN_PASS_LENGTH);
		//$this->addRule('password1', DEFAULT_PASS_MASK_HELP, 'regex', DEFAULT_PASS_MASK);
	}
}
