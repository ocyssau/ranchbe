<?php
namespace Form\Filter;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class StandardSimple extends \Form\Filter\AbstractFilterForm
{
	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $nameSpace)
	{
		$action = ($action == '') ? $_SERVER['REDIRECT_URL'] : $action;
		\HTML_QuickForm::__construct('FilterForm', 'POST', $action);
		$this->setAttribute('class','form-inline');
		$this->view = $view;
		$this->nameSpace = $nameSpace;
		$this->template = 'filter/standardsimple.tpl';

		//Find in field ----------------------------------------------
		$this->addElement('text', 'find', 'find', array('size'=>16) );
		$selectSet = array();
		$this->addElement('select', 'find_field', 'Find In', $selectSet, array('id'=>'find_field') );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		return $this;
	}

	/**
	 *
	 */
	public function reset($filter)
	{
	}
}
