<?php 
namespace Form\Filter;

use Rbplm\Dao\Pg\Filter;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class Standard extends \Form\Filter\AbstractFilterForm
{
	/**
	 * 
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $nameSpace)
	{
		parent::__construct($view, $nameSpace);
		$this->template = 'filter/standard.tpl';
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		return $this;
	}
	
	/**
	 *
	 */
	public function reset($filter)
	{
		return $this;
	}
}
