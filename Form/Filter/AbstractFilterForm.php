<?php
namespace Form\Filter;

use Rbplm\Dao\Pg\Filter;
use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

abstract class AbstractFilterForm extends \Form\AbstractForm
{
	/**
	 * Name of session var to store filters
	 * @var string
	 */
	public $nameSpace;

	/**
	 * @var string
	 */
	public $template;

	/**
	 * @var Smarty
	 */
	public $view;

	/**
	 * @var array
	 */
	public $values;

	/**
	 *
	 * @var \Form\ElementFactory
	 */
	public $elemtFactory;

	/**
	 * @param Smarty $view
	 * @param \Rbs\Space\Factory $factory
	 * @param string $nameSpace Name of the session key where store the filter parameters
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$action = ($action == '') ? '#' : $action;
		\HTML_QuickForm::__construct('FilterForm', 'POST', $action);
		$this->setAttribute('class','form-inline');

		$this->view = $view;
		$this->factory = $factory;
		$this->nameSpace = $nameSpace;
		$this->elemtFactory = new \Form\ElementFactory($this, $factory);

		//Find in field ----------------------------------------------
		$this->addElement( 'text', 'find', 'find', array('size'=>16) );

		$this->elemtFactory->selectUser(array(
			'name'=>'f_action_user_name',
			'label'=>'By',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		//Add elements ----------------------------------------------
		$this->addElement( 'text', 'find_number', 'Number', array('size'=>16) );
		$this->addElement( 'text', 'find_designation', 'Designation', array('size'=>16) );

		//checkbox to hide/show sections
		$this->addElement( 'advcheckbox', 'f_adv_search_cb', 'Advanced filter', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'advcheckbox', 'f_dateAndTime_cb', 'Date and time', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );

		$this->addElement( 'advcheckbox', 'f_open_date_cb', 'Open date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_open_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_open_date_max', 'Inferior to', array('class'=>'datepicker') );

		$this->addElement( 'advcheckbox', 'f_close_date_cb', 'Close date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_close_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_close_date_max', 'Inferior to', array('class'=>'datepicker') );

		$this->addElement( 'advcheckbox', 'f_fsclose_date_cb', 'Forseen close date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_fsclose_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_fsclose_date_max', 'Inferior to', array('class'=>'datepicker') );
	}

	/**
	 *
	 * @param unknown $values
	 * @return unknown[]
	 */
	protected function _bind($values)
	{
		$defaults=array();
		foreach($this->_elements as $element){
			$name = $element->getName();
			if( isset($values[$name]) ){
				$defaults[$name] = $values[$name];
			}
		}
		return $defaults;
	}

	/**
	 * Bind standards date and actions filters
	 *
	 * @param Filter $filter
	 * @param array $defaults
	 */
	protected function _bindAdvancedOptions($filter, $defaults)
	{
		//ACTION USER
		if($defaults['f_action_field'] && $defaults['f_action_user_name']){
			$filter->andFind($defaults['f_action_user_name'], $defaults['f_action_field'], Op::CONTAINS);
		}

		//DATE AND TIME
		if($defaults['f_dateAndTime_cb']){
			//OPEN
			if($defaults['f_open_date_cb']){
				if($defaults['f_open_date_min']){
					$filter->andFind($this->dateToSys($defaults['f_open_date_min']), 'open_date', Op::SUP);
				}
				if($defaults['f_open_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_open_date_max']), 'open_date', Op::INF);
				}
			}
			//CLOSE
			if($defaults['f_close_date_cb']){
				if($defaults['f_close_date_min']){
					$dateAsSys = \DateTime::createFromFormat(SHORT_DATE_FORMAT, $defaults['f_close_date_min'])->getTimestamp();
					$filter->andFind($this->dateToSys($defaults['f_close_date_min']), 'close_date', Op::SUP);
				}
				if($defaults['f_close_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_close_date_max']), 'close_date', Op::INF);
				}
			}
			//FORSEEN CLOSE
			if($defaults['f_fsclose_date_cb']){
				if($defaults['f_fsclose_date_min']){
					$filter->andFind($this->dateToSys($defaults['f_fsclose_date_min']), 'forseen_close_date', Op::SUP);
				}
				if($defaults['f_fsclose_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_fsclose_date_max']), 'forseen_close_date', Op::INF);
				}
			}
		}
	}

	/**
	 * @param string $date
	 * @return timeStamp
	 */
	public function dateToSys($date)
	{
		return \DateTime::createFromFormat(SHORT_DATE_FORMAT, $date)->getTimestamp();
	}

	/**
	 *
	 */
	public function save()
	{
		$_SESSION['filter'][$this->nameSpace] = $this->values;
		return $this;
	}

	/**
	 *
	 */
	public function reset($filter)
	{
		$_SESSION['filter'][$this->nameSpace]=null;
		return $this;
	}

	/**
	 *
	 */
	public function render()
	{
		$this->prepareRenderer();
		$content = $this->view->fetch($this->template);
		return $content;
	}

	/**
	 * Set the session namespace for save filters
	 */
	public function setNameSpace($string)
	{
		$this->nameSpace = $string;
		return $this;
	}

	/**
	 *
	 * @param DaoFilter $filter
	 */
	public function getValue($property)
	{
		if(isset($this->values[$property])){
			return $this->values[$property];
		}
	}

}
