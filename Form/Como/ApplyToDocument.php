<?php 
namespace Form\Como;

require_once('GUI/GUI.php');
require_once('Form/AbstractForm.php');

use Ranchbe;
use HTML_QuickForm;

class ApplyToDocument extends HTML_QuickForm
{
	
	public function __construct($thisName='', $method='post', $action='', $target='', $attributes=null, $trackSubmit = false)
	{
		parent::__construct($thisName, $method, $action, $target, $attributes, $trackSubmit);
		$this->template = 'como/form/applytodocument.tpl';
		
		$this->addElement('header', null, 'Visualiser les comos d\'un document');
		
		$lsSelectOptions = array('elementId' => 'documentls', //element id, name must be same that method in .class.php
				'printStyle' => 0, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
				'autoComplete' => 0, //if 0 the autocomplete attribute will not be set. Default not set;
				'autoserverPath' => 'DocumentLiveSearchServer.php?q=%string%&inputid=documentls', //path to server scripts. Special word %string% indicate the input string parameter to search.
		);
		$this->addElement('livesearch_select', 'document', 'Document', $lsSelectOptions, array('size' => 32) );
		
		$this->addElement('hidden', 'space', 'workitem');
		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('submit', 'next', 'Next >>');
	}
	
	
	/**
	 */
	function process()
	{
		if($_REQUEST['byPeriod']){
			header('Location: /RtGet.php?action=GetRtForPeriod');
		}
		else if($_REQUEST['byDocument']){
			header('Location: /RtGet.php?action=GetRtApplyToDocument');
		}
	} // end func process
}
