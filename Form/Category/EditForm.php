<?php
namespace Form\Category;

class EditForm extends \Form\AbstractForm
{

	/**
	 * @param unknown_type $thisName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('CategoryEdit', 'POST');
		$this->template = 'category/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		$this->addElement('hidden', 'categoryid');
		$this->addElement('hidden', 'spacename', strtolower($factory->getName()));

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//Add form elements
		$this->addElement('text', 'name', tra('Number') , array('class'=>'form-control'));
		$this->addElement('text', 'description', tra('Description') , array('class'=>'form-control'));

		// applies new filters to the element values
		$this->applyFilter('__ALL__', 'trim');

		//Add validation rules to check input data
		$this->addRule('category_number', tra('Name is required'), 'required');
		$this->addRule('category_description', tra('Description is required'), 'required');
	}

	/**
	 *
	 * @param array $values
	 */
	public function bind($category)
	{
		$submitValues = $this->exportValues();
		$category->description = $submitValues['description'];
		$category->setName($submitValues['name']);
	}

	/**
	 *
	 * @param array $values
	 */
	public function setData($values)
	{
		$this->setDefaults( array (
			'categoryid' => $values['id'],
			'name' => $values['name'],
			'description' => $values['description'],
		));
	}
}
