<?php
namespace Form\Category;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param project $project
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$action = ($action == '') ? $_SERVER['REDIRECT_URL'] : $action;
		\HTML_QuickForm::__construct('FilterForm', 'POST', $action);
		$this->setAttribute('class','form-inline');
		$this->view = $view;
		$this->factory = $factory;
		$this->nameSpace = $nameSpace;
		$this->template = 'category/filter.tpl';

		//Find in field ----------------------------------------------
		$this->addElement( 'text', 'find_description', 'Description', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'find_number', 'Number', array('size'=>16, 'class'=>'form-control') );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_number']){
			$filter->andFind($defaults['find_number'], 'category_number', Op::OP_CONTAINS);
		}

		//DESCRIPTION
		if($defaults['find_description']){
			$filter->andFind($defaults['find_description'], 'category_description', Op::OP_CONTAINS);
		}

		return $this;
	}

}
