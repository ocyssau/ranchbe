<?php
namespace Form\Application;

use Rbplm\Dao\FilterInterface;

class PaginatorForm extends \Form\AbstractForm // implements InputFilterAwareInterface
{

	public $maxLimit = 1000;

	public $limit = 50;

	public $page = 1;

	public $maxPage = 2;

	public $offset = 0;

	public $order = 'asc';

	public $orderby = '';

	/**
	 * Name of session var to store filters
	 *
	 * @var string
	 */
	public $nameSpace;

	/**
	 *
	 * @var string
	 */
	public $template;

	/**
	 *
	 * @var Smarty
	 */
	public $view;

	/**
	 *
	 * @var array
	 */
	public $values;

	/**
	 *
	 * @param Smarty $view
	 * @param string $nameSpace
	 * @param string $formSelector
	 *        	Css selector of form to submit
	 */
	public function __construct($view, $nameSpace, $formSelector = '#paginator')
	{
		$this->view = $view;
		$this->template = 'application/paginator.tpl';
		$this->nameSpace = $nameSpace;

		$this->addElement('hidden', 'orderby');
		$this->addElement('hidden', 'order');

		$this->addElement('select', 'paginatorlimit', 'Result per page', array(
			10 => 10,
			20 => 20,
			50 => 50,
			100 => 100,
			1000 => 1000
		), array(
			'id' => 'paginatorlimit',
			'onChange' => "paginatorSubmit(this, '" . $formSelector . "');"
		));

		$this->addElement('select', 'paginatorpage', 'Page', array(), array(
			'id' => 'paginatorpage',
			'onChange' => "paginatorSubmit(this, '" . $formSelector . "');"
		));

		$this->addElement('button', 'paginatornext', '>', array(
			'id' => 'paginatornext',
			'onClick' => "paginatorNext(this);paginatorSubmit(this, '" . $formSelector . "');"
		));

		$this->addElement('button', 'paginatorprev', '<', array(
			'id' => 'paginatorprev',
			'onClick' => "paginatorPrev(this);paginatorSubmit(this, '" . $formSelector . "');"
		));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(FilterInterface $filter)
	{
		$defaults1 = $this->_bind($_SESSION['paginator'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);
		$defaults = array_merge($defaults1, $defaults2);

		$this->orderby = $filter->getSort();
		$this->order = $filter->getOrder();

		($defaults['paginatorpage'] < 1) ? $defaults['paginatorpage'] = 1 : null;
		(isset($defaults['paginatorpage'])) ? $this->page = (int)$defaults['paginatorpage'] : $defaults['paginatorpage'] = $this->page;
		(isset($defaults['paginatorlimit'])) ? $this->limit = (int)$defaults['paginatorlimit'] : $defaults['paginatorlimit'] = $this->limit;
		($defaults['orderby']) ? $this->orderby = $defaults['orderby'] : $defaults['orderby'] = $this->orderby;
		($defaults['order']) ? $this->order = $defaults['order'] : $defaults['order'] = $this->order;

		$this->values = $defaults;
		$this->setDefaults($defaults);

		/* PAGINATION */
		$this->filter = $filter;
		$this->offset = ($this->page - 1) * $this->limit;
		$this->maxPage = ceil($this->maxLimit / $this->limit);

		$filter->page($this->page, $this->limit);

		/* Set select of page */
		$options = array(
			1 => 1
		);
		for ($i = 2; $i <= $this->maxPage; $i++) {
			$options[$i] = $i;
		}
		$this->getElement('paginatorpage')->loadArray($options);

		$filter->sort($this->orderby, $this->order);
		return $this;
	}

	/**
	 */
	protected function _bind($values)
	{
		$defaults = array();
		(isset($values['paginatorlimit'])) ? $defaults['paginatorlimit'] = $values['paginatorlimit'] : null;
		(isset($values['paginatorpage'])) ? $defaults['paginatorpage'] = $values['paginatorpage'] : null;
		(isset($values['orderby'])) ? $defaults['orderby'] = $values['orderby'] : null;
		(isset($values['order'])) ? $defaults['order'] = $values['order'] : null;
		return $defaults;
	}

	/**
	 *
	 * @param integer $maxLimit
	 */
	public function setMaxLimit($maxLimit)
	{
		$this->maxLimit = (int)$maxLimit;
	}

	/**
	 */
	public function render($view = null)
	{
		if ( $view == null ) {
			$view = $this->view;
		}
		$this->prepareRenderer('paginatorForm');
		$content = $view->fetch($this->template);
		return $content;
	}

	/**
	 * Set view variables from paginator datas
	 *
	 * @param
	 *        	$view
	 */
	public function bindToView($view = null)
	{
		if ( $view == null ) {
			$view = $this->view;
		}

		$view->paginator = $this->render($view);
		$view->orderby = $this->orderby;
		$view->order = $this->order;
		return $this;
	}

	/**
	 */
	public function save()
	{
		$_SESSION['paginator'][$this->nameSpace] = $this->values;
		return $this;
	}

	/**
	 */
	public function reset()
	{
		$_SESSION['paginator'][$this->nameSpace] = null;
		return $this;
	}
}
