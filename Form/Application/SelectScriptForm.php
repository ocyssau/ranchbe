<?php
namespace Form\Application;

class SelectScriptForm extends \Form\AbstractForm
{

	/**
	 * @param Smarty $view
	 */
	public function __construct($view, $path, $factory=null)
	{
		$name = 'xlsrendererSelector';
		parent::__construct($name, $view, $factory);
		$this->setAttribute('class', 'form-inline');
		$this->template = 'document/manager/selectscriptform.tpl';
		$this->view = $view;

		//SELECT RESULT FORMAT(xls or csv)
		$this->addElement('radio', 'format', '', 'csv', 'csv', array('class'=>'form-control'));
		$this->addElement('radio', 'format', '', 'excel', 'xls', array('class'=>'form-control'));

		//SELECT XLS RENDER
		$files = glob($path.'/*.php');
		foreach($files as $file){
			$className = str_replace('.php', '', basename($file));
			$selectSet[$className] = $className;
		}
		$this->setScripts($selectSet, 'XLS Renderer');

		//SUBMIT
		$this->addElement('submit', 'validate', 'Ok', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
	}

	/**
	 *
	 * @param array $list
	 */
	public function setScripts($list, $selectName)
	{
		$select = $this->addElement('select', 'phpscript', $selectName, $list, array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);
		return $this;
	}
}

