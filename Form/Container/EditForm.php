<?php
namespace Form\Container;

use Ranchbe;

abstract class EditForm extends \Form\AbstractForm
{
	public $mode; //edit | create

	/**
	 * @param \Smarty
	 * @param \space
	 */
	public function __construct($view, $factory)
	{
		parent::__construct($name, $view, $factory);
		$this->ranchbe = Ranchbe::get();

		$this->setAttribute('class', 'inline-form');
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory($factory);
		$this->template = 'container/editform.tpl';

		$this->addElement('hidden', 'id');
		$this->addElement('hidden', 'action');
		$this->addElement('hidden', 'layout');

		$this->addElement('text', 'number', tra('Number'), array('class'=>'form-control'));
		$this->addElement('textarea', 'description', tra('Description'), array('size'=>40, 'class'=>'form-control'));
		$this->view->assign('number_help', $this->getHelp());

		$params = array(
			'name' => 'processId',
			'size' => 1,
			'multiple' => false,
			'required' => false,
		);
		$elemtFactory->selectProcess($params);

		//Contruct date select field
		$params = array(
			'name' => 'forseenCloseDate',
			'required' => true,
		);
		$elemtFactory->selectDate($params , $this);

		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));

		//Add validation rules to check input data
		//$this->addRule('forseenCloseDate', 'forseenCloseDate is required', 'required', null, 'server');
		//$this->addRule('forseenCloseDate', 'forseenCloseDate must be a date', 'date', null, 'server');
		//$this->addRule('uid', 'Number is required', 'required', null, 'server');
		$mask = $this->getMask();
		//$this->addRule('uid', 'This number is not valid', 'regex', "/$mask/" , 'server');
		//$this->addRule('parentId', 'Project is required', 'required', null, 'server');
		//$this->addRule('parentId', 'Project is required', 'numeric', null, 'server');
		//$this->addRule('parentId', 'Project is required', 'nonzero', null, 'server');
	}

	/**
	 *
	 */
	public function getMask()
	{
		return $this->ranchbe->getConfig($this->factory->getName().'.name.mask');
	}

	/**
	 *
	 */
	public function getHelp()
	{
		return $this->ranchbe->getConfig($this->factory->getName().'.name.mask.help');
	}
}
