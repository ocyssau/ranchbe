<?php
namespace Form\Container;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 *
	 * @var string
	 */
	public $spaceName;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		parent::__construct($view, $factory, $nameSpace);
		$this->template = 'container/filter.tpl';

		//Find in field ----------------------------------------------
		$this->addElement( 'text', 'find', 'find', array('size'=>16) );
		$list = array(
			'id'=>'id',
			'status'=>'status',
			'indice'=>'versionId',
		);
		$this->elemtFactory->select($list, array(
			'name'=>'find_field',
			'label'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		), $this);

		//Select action by user ----------------------------------------------
		$actions = array(
			'closeById'=>'Close By',
			'createById'=>'Created By',
		);
		$this->elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'label'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		), $this);
	}

	/**
	 *
	 * @param unknown_type $optionalFields
	 */
	public function setExtended($optionalFields)
	{
		if(is_array($optionalFields)){
			foreach($optionalFields as $val){
				$extended[0][$val['appname']] = $val['field_description'];
				$this->addElement( 'text', $val['appname'], $val['field_description'], array('size'=>$val['field_size']) );
			}
		}
		$this->view->assign('extended',$optionalFields);
		$this->extended = $optionalFields;
	}


	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_number']){
			$filter->andFind($defaults['find_number'], 'uid', Op::CONTAINS);
		}

		//DESIGNATION
		if($defaults['find_designation']){
			$filter->andFind($defaults['find_designation'], 'description', Op::CONTAINS);
		}

		//ADVANCED SEARCH
		if($defaults['f_adv_search_cb']){
			//FIND IN
			if($defaults['find'] && $defaults['find_field']){
				$filter->andFind($defaults['find'], $defaults['find_field'], Op::CONTAINS);
			}

			//bind defaults date and action filters
			$this->_bindAdvancedOptions($filter, $defaults);
		}

		//EXTENDED
		if(is_array($this->extended)){
			foreach($this->extended as $val){
				$fieldName = $val['appname'];
				if($defaults[$fieldName]){
					$filter->andFind($defaults[$fieldName], $fieldName, Op::CONTAINS);
				}
			}
		}

		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form\Filter.AbstractFilterForm::_bindAdvancedOptions()
	 */
	protected function _bindAdvancedOptions($filter, $defaults)
	{
		//ACTION USER
		if($defaults['f_action_field'] && $defaults['f_action_user_name']){
			$filter->andFind($defaults['f_action_user_name'], $defaults['f_action_field'], Op::CONTAINS);
		}

		//DATE AND TIME
		if($defaults['f_dateAndTime_cb']){
			//OPEN
			if($defaults['f_open_date_cb']){
				if($defaults['f_open_date_min']){
					$filter->andFind($this->dateToSys($defaults['f_open_date_min']), 'created', Op::SUP);
				}
				if($defaults['f_open_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_open_date_max']), 'created', Op::INF);
				}
			}
			//CLOSE
			if($defaults['f_close_date_cb']){
				if($defaults['f_close_date_min']){
					$dateAsSys = \DateTime::createFromFormat(DATE_FORMAT, $defaults['f_close_date_min'])->getTimestamp();
					$filter->andFind($this->dateToSys($defaults['f_close_date_min']), 'closed', Op::SUP);
				}
				if($defaults['f_close_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_close_date_max']), 'closed', Op::INF);
				}
			}
			//FORSEEN CLOSE
			if($defaults['f_fsclose_date_cb']){
				if($defaults['f_fsclose_date_min']){
					$filter->andFind($this->dateToSys($defaults['f_fsclose_date_min']), 'forseenCloseDate', Op::SUP);
				}
				if($defaults['f_fsclose_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_fsclose_date_max']), 'forseenCloseDate', Op::INF);
				}
			}
		}
	}

	/**
	 *
	 */
	protected function _bindExtended()
	{
	}

}
