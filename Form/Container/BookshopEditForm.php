<?php
namespace Form\Container;

class BookshopEditForm extends EditForm
{

	/**
	 *
	 * @param unknown $view
	 * @param unknown $factory
	 */
	public function __construct($view, $factory)
	{
		parent::__construct($view, $factory);

		$elemtFactory = $this->getElemtFactory();

		$selectSet = array(0=>'Document Manager',1=>'File Manager');
		$this->addElement('select', 'file_only' , tra('Type'), $selectSet, array('id'=>'file_only') );
	}
}

