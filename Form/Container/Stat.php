<?php 
namespace Form\Container;

require_once 'GUI/GUI.php';
require_once 'Form/AbstractForm.php';
require_once 'class/project.php';

class Stat extends \Form\AbstractForm
{
	/**
	 * @param \Smarty
	 * @param \container
	 */
	public function __construct($view, $container)
	{
		parent::__construct($thisName='statistics', $method='post', $action='getstats', $target='', $attributes=null, $trackSubmit=false);
		$this->view = $view;

		$path_queries_scripts = PATH_STAT_QUERY;
		
		$files = glob($path_queries_scripts . '/stats_query_*.php');
		foreach ($files as $filename) {
			$list[basename($filename)] = substr(basename(basename($filename), '.php') , 12);
		}
		$select = $this->addElement('select', 'statistics_queries', tra('Queries'), $list);
		$select->setSize(10);
		$select->setMultiple(true);

		//Add hidden fields
		$this->addElement('hidden', 'container_id', $container->GetId() );
		$this->addElement('hidden', 'space', $container->space->SPACE_NAME );
		$this->addElement('hidden', 'action', 'getStats');
		$this->addElement('submit', 'submit', 'Display');
	}
	
	/**
	 * 
	 * @param unknown_type $values
	 */
	public function bind($values)
	{
	}

}
