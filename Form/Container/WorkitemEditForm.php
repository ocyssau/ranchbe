<?php
namespace Form\Container;

class WorkitemEditForm extends EditForm
{
	/**
	 *
	 * @param unknown_type $view
	 * @param unknown_type $factory
	 */
	public function __construct($view, $factory)
	{
		parent::__construct($view, $factory);
		$elemtFactory = $this->getElemtFactory();

		$params = array(
			'name'=>'parentId',
			'size'=>1,
			'multiple'=>false,
			'required'=>true,
		);
		$elemtFactory->selectProject($params);
	}
}
