<?php
namespace Form\Container\History;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$this->view = $view;
		$this->factory = $factory;
		$this->nameSpace = $nameSpace;
		$this->template = 'container/history/filter.tpl';
		$elemtFactory = $this->getElemtFactory($factory);
		$this->setAttribute('class','form-inline');

		$dao = $factory->getDao(\Rbs\Org\Container\History::$classId);

		$this->addElement( 'text', 'f_action_name', 'Action', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'f_container_number', 'Container Number', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'find', 'find', array('size'=>16) );

		$this->addElement('hidden', 'spacename', strtolower($factory->getName()));
		$this->addElement('hidden', 'containerid');

		$elemtFactory->selectUser(array(
			'name'=>'f_action_user_name',
			'label'=>'By',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		//Find in field ----------------------------------------------
		$elemtFactory->select(
			array(
				'action_name' => 'Action Name',
				$dao->toSys('data_name') => 'Number',
				$dao->toSys('data_status') => 'State',
				$dao->toSys('data_indice') => 'Indice',
			),
			array(
				'name'=>'find_field',
				'description'=>'In',
				'multiple'=>false,
				'returnName'=>false,
				'required'=>false,
				'size'=>1,
		));

		$this->addElement( 'advcheckbox', 'f_adv_search_cb', 'Advanced filter', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'advcheckbox', 'f_dateAndTime_cb', 'Date and time', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'advcheckbox', 'f_action_date_cb', 'Action date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_action_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_action_date_max', 'Inferior to', array('class'=>'datepicker') );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		$historyDao = $this->dao;

		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['f_container_number']){
			$filter->andFind($defaults['f_container_number'], $historyDao->toSys('name'), Op::OP_CONTAINS);
		}

		//ACTION NAME
		if($defaults['f_action_name']){
			$filter->andFind($defaults['f_action_name'], 'action_name', Op::OP_CONTAINS);
		}

		//ACTION USER
		if($defaults['f_action_user_name']){
			$filter->andFind($defaults['f_action_user_name'], 'action_by', Op::OP_CONTAINS);
		}

		//DATE AND TIME
		if($defaults['f_action_date_cb']){
			//FIND IN
			if($defaults['find'] && $defaults['find_field']){
				$filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
			}

			//ACTION DATE
			if($defaults['f_action_date_min']){
				if($defaults['f_action_date_min']){
					$filter->andFind($historyDao->dateToSys($defaults['f_action_date_min']), 'action_date', Op::OP_SUP);
				}
				if($defaults['f_action_date_max']){
					$filter->andFind($historyDao->dateToSys($defaults['f_action_date_max']), 'action_date', Op::OP_INF);
				}
			}
		}

		return $this;
	}
}

