<?php
namespace Form\Container\Alias;

class EditForm extends \Form\AbstractForm
{
	public $mode; //edit | create

	/**
	 * @param \Smarty
	 * @param \space
	 */
	public function __construct($view, $factory)
	{
		parent::__construct($name, $view, $factory);
		$this->template = 'container/alias/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$elemtFactory = $this->getElemtFactory($factory);

		$this->addElement('hidden', 'id');
		$this->addElement('hidden', 'page_id');
		$this->addElement('hidden', 'layout');
		$this->addElement('hidden', 'spacename', strtolower($factory->getName()));

		$this->addElement('text', 'uid', tra('Number'), array('class'=>'form-control'));
		$this->addRule('uid', tra('Required'), 'required', null, 'client');

		$this->addElement('textarea', 'description', tra('Description'), array('class'=>'form-control'));
		$this->addRule('description', tra('Required'), 'required', null, 'client');

		$params = array(
			'name'=>'aliasOfId',
			'description'=>tra('Alias of'),
			'size'=>1,
			'multiple'=>false,
			'returnName'=>false,
			'required'=>true);
		$elemtFactory->selectContainer($params);

		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));

		//Add validation rules to check input data
		//$this->addRule('number', 'Number is required', 'required', null, 'server');
		//$this->addRule('number', 'This number is not valid', 'regex', "/$mask/" , 'server');
		//$this->addRule('project_id', 'Project is required', 'required', null, 'server');
		//$this->addRule('project_id', 'Project is required', 'numeric', null, 'server');
		//$this->addRule('project_id', 'Project is required', 'nonzero', null, 'server');
	}

}
