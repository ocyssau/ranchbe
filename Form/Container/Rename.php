<?php 
namespace Form\Container;

require_once('GUI/GUI.php');
require_once('Form/AbstractForm.php');

class Rename extends \Form\AbstractForm
{
	
	/**
	 * 
	 * @param unknown_type $formName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct()
	{
		parent::__construct($formName='', $method='post', $action='', $target='', $attributes=null, $trackSubmit = false);
		
		$this->addElement('hidden', 'checked[]', $containerId);
		$this->addElement('hidden', 'action', 'rename');
		$this->addElement('header', null, tra('Rename container ').' '.$this->container->getProperty('container_number'));
		$this->addElement('text', 'newName', tra('New name'));
		$this->addRule('newName', tra('Required'), 'required', null, 'client');
		$mask = constant('DEFAULT_'. strtoupper($this->container->SPACE_NAME) . '_MASK');
		if($mask){
		    $this->addRule('newName', 'This number is not valid', 'regex', "/$mask/" , 'server');
		}
	}
	
	/**
	 * 
	 * @param unknown_type $values
	 */
	public function bind($values)
	{
		//Set defaults values
		$this->setDefaults( array (
			'project_id'  => $values['project_id'],
			'project_description'  => $values['project_description'],
			'project_number'  => $values['project_number'],
			'submit'  => 'save',
			'forseen_close_date'=>$fsCloseDate
		));
		return $this;
	}
}
