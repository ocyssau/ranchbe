<?php
namespace Form\Container;

class MockupEditForm extends EditForm
{

	public function __construct($view, $factory)
	{
		parent::__construct($view, $factory);

		$selectSet = array(0=>'Document Manager',1=>'File Manager');
		$this->addElement('select', 'file_only' , tra('Type'), $selectSet, array('id'=>'file_only') );
	}


	/**
	 *
	 * @param unknown_type $values
	 */
	public function bind($values)
	{
		$dateTime = new \DateTime();
		$dateTime->setTimeStamp($values['forseen_close_date']);

		$defaults = array(
			'number'      => $values['mockup_number'],
			'description' => $values['mockup_description'],
			'project_id'  => $values['project_id'],
			'forseen_close_date' => $dateTime->format(SHORT_DATE_FORMAT),
			'default_process_id' => $values['default_process_id'],
			'container_id' => $values['mockup_id'],
			'file_only' => $values['file_only'],
		);

		foreach($this->optionalFields as $field){
			$name = $field['field_name'];
			$val = $values[$name];

			if($field['field_type'] == 'date'){
				$dateTime->setTimeStamp( $val );
				$val = $dateTime->format(SHORT_DATE_FORMAT);
			}

			$defaults[$name] = $val;
		}

		$this->setDefaults($defaults);
		return $this;
	}

	/**
	 * Return an array with key as model property name and value from user inputs
	 *
	 * @param array $values
	 * @return array
	 */
	public function toModel($values)
	{
		$values = parent::toModel($values);

		$bind['forseen_close_date'] = $values['forseen_close_date'];
		$bind['mockup_number'] = $values['number'];
		$bind['mockup_description'] = $values['description'];
		$bind['mockup_id'] = $values['container_id'];
		$bind['project_id'] = $values['project_id'];
		$bind['default_process_id'] = $values['default_process_id'];
		$bind['file_only'] = $values['file_only'];

		return $bind;
	} //End of function
}
