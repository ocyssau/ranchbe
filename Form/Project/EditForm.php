<?php
namespace Form\Project;

use DateTime;

class EditForm extends \Form\AbstractForm
{
	public $daoFactory;

	/**
	 *
	 * @param unknown_type $formName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('ProjectEdit', 'POST');
		$this->template = 'project/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory();

		//Add hidden fields
		$this->addElement('hidden', 'id', '');
		$this->addElement('hidden', 'layout');

		$this->addElement('text', 'name', tra('Number'), array('class'=>'form-control'));
		$this->addElement('textarea', 'description', tra('Description'), array('col'=>30, 'class'=>'form-control'));

		$params = array(
			'name' => 'forseenCloseDate',
			'required' => true,
		);
		$elemtFactory->selectDate($params);

		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//Add validation rules to check input data
		$this->addRule('forseenCloseDate', tra('fsCloseDate is required'), 'required');
		$this->addRule('forseenCloseDate', tra('fsCloseDate must be a date'), 'date');
		$this->addRule('name', tra('Project number is required'), 'required');
		
		$mask = \Ranchbe::get()->getConfig('project.name.mask');
		if($mask){
		    $help = \Ranchbe::get()->getConfig('project.name.mask.help');
		    $this->addRule('name', 'This number is not valid', 'regex', "/$mask/" , 'server');
		    $this->view->assign('number_help', $help);
		}
		$this->addRule('description', 'Project description is required', 'required');
	}
}
