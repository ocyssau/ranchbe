<?php
namespace Form\Project;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param project $project
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		parent::__construct($view,$factory, $nameSpace);
		$this->template = 'project/filter.tpl';

		//Find in field ----------------------------------------------
		$list = array(
			'project_description' => 'Designation',
			'project_state' => 'State',
		);
		$this->elemtFactory->select($list, array(
			'name'=>'find_field',
			'label'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Select action by user ----------------------------------------------
		$actions = array(
			'close_by'=>'Close By',
			'open_by'=>'Created By',
		);
		$this->elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'label'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_number']){
		    $filter->andFind($defaults['find_number'], 'project_number', Op::OP_CONTAINS);
		}

		//DESIGNATION
		if($defaults['find_designation']){
		    $filter->andFind($defaults['find_designation'], 'designation', Op::OP_CONTAINS);
		}

		//ADVANCED SEARCH
		if($defaults['f_adv_search_cb']){
		    //FIND IN
		    if($defaults['find'] && $defaults['find_field']){
		    	$filter->with(array(
		    		'table'=>$this->container->SPACE_NAME.'_categories',
		    		'on'=>'category_id',
		    		'alias'=>'category',
		    		'select'=>array('category_number'),
		    		'direction'=>'outer'
		    	));
		        $filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
		    }
		    $this->_bindAdvancedOptions($filter, $defaults);
		}

		return $this;
	}

}
