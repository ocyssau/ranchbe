<?php
namespace Form;

class DefaultForm extends AbstractForm
{

	/**
	 * Returns a reference to default renderer object
	 * Dont use GLOBAL var as in HTML QUICK FORM
	 */
	function defaultRenderer()
	{
		if(!$this->defaultRenderer){
			$this->defaultRenderer = new \Form\Renderer\DefaultRenderer();
		}
		return $this->defaultRenderer;
	} // end func defaultRenderer
}
