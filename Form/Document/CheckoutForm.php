<?php
namespace Form\Document;

class CheckoutForm extends \Form\AbstractForm
{

	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory)
	{
		parent::__construct('checkoutform', 'POST', $action='#');

		$this->template = 'document/manager/checkoutform.tpl';
		$this->view = $view;
		$this->view->documentIds = array();
		$this->setAttribute('class', 'form-inline');

		$this->addElement('hidden', 'spacename', $factory->getName());
		$this->addElement('submit', 'validate', 'Ok', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));

		$this->selectSet = array ('keep' => tra ( 'Keep file in wildspace' ), 'replace' => tra ( 'Replace file in wildspace' ), 'ignore' => tra ( 'Dont Checkout' ) );
	}

	/**
	 * $docfile parent must be set beform.
	 *
	 * @param Docfile\Version $docfile
	 */
	public function addDocfile($docfile)
	{
		$docfileIds = $this->view->docfileIds;
		$documentIds = $this->view->documentIds;

		//$label = $document->getUid().' v'.$document->version.'.'.$document->iteration;
		$document = $docfile->getParent();
		$fileName = $docfile->getName();
		$documentId = $document->getId();
		$docfileId = $docfile->getId();
		$i = $docfileId;

		$messageElements = array($fileName, $document->getUid());
		$message = vsprintf ( tra('What do want to do with file <b>%s</b> of document <b>%s</b>?'), $messageElements);

		$this->addElement('select', 'ifExistmode['.$i.']', $message, $this->selectSet, array ('id' => 'ifExistmode['.$i.']', 'class'=>'form-control'));
		$this->addElement('hidden', 'documentid['.$i.']', $documentId);
		$this->addElement('hidden', 'docfileid['.$i.']', $docfileId);

		$documentIds[] = $documentId;
		$docfileIds[] = $docfileId;

		$this->view->documentIds = $documentIds;
		$this->view->docfileIds = $docfileIds;
	}
}
