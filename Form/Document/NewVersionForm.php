<?php
namespace Form\Document;

class NewVersionForm extends \Form\AbstractForm
{
	/**
	 * @var DaoFactory
	 */
	public $factory;
	public $count=0;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($factory, $view)
	{
		parent::__construct('newVersion', 'POST', $action='');
		$this->template = 'document/newversionform.tpl';
		$this->factory = $factory;
		$this->view = $view;
		$formFactory = new \Form\ElementFactory($this, $factory);
		$spaceName = $factory->getName();

		//HIDDEN
		$this->addElement('hidden', 'containerid', null);

		//TARGET CONTAINER
		$formFactory->selectContainer(array(
			'name'=>'tocontainerid',
			'label'=>'Target Container',
			'multiple'=>false,
			'return_name'=>false,
			'required'=>false,
			'size'=>1,
			'class'=>'form-control',
		));

		//VERSION
		$formFactory->selectDocumentVersion(array(
			'name'=>'version',
			'label'=>tra('Version'),
			'multiple'=>false,
			'return_name'=>false,
			'required'=>false,
			'size'=>1,
		));

		//SUBMIT
		$this->addElement('submit', 'validate', 'Validate', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind($object)
	{
		return $this;
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function setData($datas)
	{
		$this->setDefaults($datas);
		return $this;
	}

	/**
	 * @param Document\Version $document
	 */
	public function addDocument($document)
	{
		$label = $document->getUid().' v'.$document->version.'.'.$document->iteration;
		$id = $document->getId();
		$cb = $this->addElement('advcheckbox','checked['.$this->count.']', $beforeLabel, $label, $attributes, $id);
		$cb->setChecked(true);
		$this->count++;
		$this->view->count = $this->count;
	}
}

