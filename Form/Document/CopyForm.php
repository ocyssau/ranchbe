<?php
namespace Form\Document;

class CopyForm extends \Form\AbstractForm
{
	/**
	 * @var DaoFactory
	 */
	public $factory;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($factory, $view)
	{
		parent::__construct('copyDocument', 'POST', $action='');
		$this->template = 'document/copyform.tpl';
		$this->factory = $factory;
		$this->view = $view;
		$formFactory = new \Form\ElementFactory($this, $factory);

		$spaceName = $factory->getName();

		//HIDDEN
		$this->addElement('hidden', 'documentid', null);
		$this->addElement('hidden', 'page_id', 'DocManage_copydoc');

		//TARGET CONTAINER
		$formFactory->selectContainer(array(
			'name'=>'containerid',
			'label'=>'Target Container',
			'multiple'=>false,
			'return_name'=>false,
			'required'=>true,
			'size'=>1,
		));

		//NEW NAME
		$this->addElement('text', 'newname', tra('New name'), array('size'=>50, 'class'=>'form-control'));
		$this->addRule('newname', tra('Required'), 'required', null, 'client');

		//VERSION
		$formFactory->selectDocumentVersion(array(
			'name'=>'version',
			'label'=>tra('Indice'),
			'multiple'=>false,
			'return_name'=>false,
			'required'=>true,
			'size'=>1,
		));

		//SUBMIT
		$this->addElement('submit', 'validate', tra('Ok'), array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', tra('Cancel'), array('class'=>'btn btn-default'));
	}
}

