<?php
namespace Form\Document;

/**
 *
 *
 */
class CheckinForm extends \Form\AbstractForm
{

	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $checkinAndKeep = false)
	{
		parent::__construct('checkinform', 'POST', $action='#');

		$this->template = 'document/manager/checkinform.tpl';
		$this->view = $view;
		$this->view->documentIds = array();
		$this->setAttribute('class', 'form-inline');

		$this->addElement('textarea', 'comment', 'Comment', array('rows'=>'5','cols'=>'50','class'=>'form-control'));
		$this->addElement('hidden', 'spacename', $factory->getName());
		$this->addElement('hidden', 'checkinandkeep', $checkinAndKeep);

		$this->addElement('submit', 'validate', 'Ok', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('button', 'preparedata', 'Prepare Data', array('class'=>'btn btn-info'));
	}

	/**
	 * $docfile parent must be set beform.
	 *
	 * @param Docfile\Version $docfile
	 */
	public function addDocument($document)
	{
		$documentIds = $this->view->documentIds;
		$documentId = $document->getId();
		$i = $documentId;

		$label = $document->getNumber().' v'.$document->version.'.'.$document->iteration;
		$cb = $this->addElement('advcheckbox', 'checked['.$i.']', $beforeLabel, $label, array('class'=>'form-control'), $documentId);
		$cb->setChecked(true);

		$documentIds[] = $documentId;
		$this->view->documentIds = $documentIds;

		/** @var \Rbplm\Ged\Docfile\Version $df */
		foreach($document->getDocfiles() as $df){
			$this->addElement('hidden', 'files[]', $df->getName());
		}
	}
}
