<?php

namespace Form\Document;

class SmartStoreForm extends \Form\Sub
{
	public $feedbacks = array();
	public $errors = array();
	public $actions = array();

	/**
	 * @var \subform
	 */
	public $form;

	protected $document; //object document
	protected $container; //object container
	protected $space; //object space
	protected $wildspacePath; //string path to wildspace
	protected $areaId; //int
	protected $actionDefinition = array(
		'create_doc' => 'Create a new document',
		'update_doc_file' => 'Update the document and files',
		'update_doc' => 'Update the document',
		'update_file' => 'Update the file only',
		'add_file' => 'Add the file to current document',
		'ignore' => 'Ignore',
		'upgrade_doc' => 'Create a new indice',
	);

	/**
	 * Genere une ligne de formulaire dans le cas du store multiple
	 * Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
	 * et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
	 * Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.
	 *
	 * La liste des champs est accessible dans la reponse depuis le <input hidden> fields.
	 *
	 * @param integer $loopId
	 *
	 * $input is the result of checkOne method
	 *
	 * @param array $input
	 *
		$input = array(
		'errors'=>array(),
		'actions'=>array(),
		'document'=>null, //Document object
		'docfile'=>null, //Docfile object
		'fsdata'=>null, //Fsdata object
		'container'=>null, //Container object
		'file'=>'', //input file full path,
		'spaceName'=>''
		);
	 *
	 */
	function __construct($loopId, $view, $factory, $renderer=null)
	{
		$name = 'fieldset_'.$loopId;
		parent::__construct($name, $view, $factory, $renderer);

		$this->loopId = $loopId;
		$this->_requiredNote = '';

		if( count($this->actions) == 1 ){
			if ( isset($this->actions['ignore']) ){
				$disabled = 'DISABLED';
				$readonly = 'READONLY';
				$this->addElement('text', "file[$loopId]", tra('File') ,array('readonly', 'size'=>32));
				$this->addElement('text', "number[$loopId]", tra('Number') , array('readonly', 'size'=>32));
				$this->errorsToElement();
				return $this;
			}
		}

		//ACTIONS
		$params = array(
				'multiple' => false,
				'name' => "actions[$loopId]",
				'label' => tra('Action'),
				'size' => '1',
				'returnName' => false,
				'disabled' => $disabled,
				'class'=>'form-control'
		);
		$this->elemtFactory->select($actions, $params);

		//FILE
		$this->addElement('text', "file[$loopId]", tra('File') ,array('readonly', 'size'=>32, 'class'=>'form-control file'));
		$this->addRule("file[$loopId]", 'This file name is not valid', 'regex', "/$mask/" , 'server');
		$this->addElement('hidden', "fields[$loopId][]", 'file');

		//NUMBER
		$this->addElement('text', "number[$loopId]", tra('Number') , array('readonly', 'size'=>32, 'class'=>'form-control number'));
		$this->addRule("number[$loopId]", 'This document name is not valid', 'regex', "/$mask/" , 'server');

		//VERSION
		$params = array(
			'multiple' => false,
			'name' => 'version['.$loopId.']',
			'label' => tra('Version'),
			'size' => '1',
			'required' => true,
			'disabled' => $disabled,
			'class'=>'form-control version'
		);
		$this->elemtFactory->selectDocumentVersion($params);
		$this->addElement('hidden', "fields[$loopId][]", 'version');

		//DESCRIPTION=DESIGNATION
		$label = tra('Designation');
		$this->addElement('textarea', 'description['.$loopId.']' , $label , array('rows'=>2,'cols'=>32, $disabled, $readonly,'class'=>'form-control description'));
		$this->addRule('description['.$loopId.']', tra('is required'), 'required');
		$this->addElement('hidden', "fields[$loopId][]", 'description');

		//CATEGORY
		$params = array(
			'multiple' => false,
			'name' => 'categoryId['.$loopId.']',
			'label' => tra('Category'),
			'size' => '1',
			'returnName' => false,
			'disabled' => $disabled,
			'class'=>'form-control category'
		);
		$this->elemtFactory->selectCategory($params);
		$this->addElement('hidden', "fields[$loopId][]", 'categoryId');

		$this->addElement('hidden', "loopId[$loopId]", $loopId);
		$this->addElement('hidden', "documentId[$loopId]");
		$this->addElement('hidden', "fileId[$loopId]");

		//Apply new filters to the element values
		$this->applyFilter('__ALL__', 'trim');
	}//End of method

	/**
	 * Create hidden field to retrieve initial action list
	 */
	public function setActions($actions)
	{
		foreach($actions as $action=>$description){
			$this->addElement('hidden', 'actions['.$loopId.']['.$action.']', $description);
		}
	}

	/**
	 *
	 */
	public function freeze()
	{
		$this->addElement('hidden', 'isFrozen['.$this->loopId.']', '1');
		parent::freeze();
	}

	/**
	 */
	public function setData($datas)
	{
		$loopId = $this->loopId;
		$defaults = array();
		foreach($datas as $dName=>$dValue){
			$dName = $dName."[$loopId]";
			if(is_array($dValue)){
				$defaults[$dName] = $dValue[$loopId];
			}
			else{
				$defaults[$dName] = $dValue;
			}
		}

		if($data['isFrozen'][$loopId]==1){
			$this->freeze();
		}

		$this->setDefaults($defaults);
	}

	/**
	 */
	public function setProcessor($smartProcessor)
	{
		$loopId = $this->loopId;
		$defaults = array();

		$file = $smartProcessor->fsdata->getName();
		$dName = "file[$loopId]";
		$defaults[$dName] = $file;

		$actions = $smartProcessor->actions;
		$dName = "actions[$loopId]";
		$select = $this->getElement($dName);
		foreach($actions as $value=>$label){
			$select->addOption($label, $value);
		}
		$defaults[$dName] = $smartProcessor->action;

		$errors = $smartProcessor->errors;
		$str = '';
		foreach($errors as $error){
			$str .= '<span class="feedback">'.$error.'</span>';
		}
		//$this->addElement('html', $str);
		$this->view->feedback = $str;

		$documentData = $smartProcessor->document->getArrayCopy();
		foreach($documentData as $dName=>$dValue){
			$dName = $dName."[$loopId]";
			$defaults[$dName] = $dValue;
		}

		if ( $smartProcessor->isFrozen ){
			$this->addElement('hidden', 'isFrozen['.$this->loopId.']', '1');
			$this->freeze();
		}

		$this->setDefaults($defaults);
	}


	/**
	 * @param DocumentVersion $document
	 */
	public function bind($smartProcessor)
	{
		$values = $this->exportValues();
		$loopId = $this->loopId;
	}

	/**
	 * generate html for errors
	 */
	public function errorsToElement($errors)
	{
		foreach($errors as $error){
			if($error){
				$errorMsg .= str_replace('{error}', $error, $this->errorTemplate);
			}
		}
		if($errorMsg) $this->addElement('static', '', $errorMsg);
	} //End of function

	/**
	 * generate html for feedback
	 */
	public function feedbackToElement($feedbacks)
	{
		foreach($feedbacks as $feedback){
			if($feedback){
				$feedbackMsg .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
			}
		}
		if($feedbackMsg) $this->addElement('static', '', $feedbackMsg);
	} //End of function


} //End of class
