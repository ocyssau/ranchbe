<?php
namespace Form\Document;

class EditForm extends \Form\AbstractForm
{
	/**
	 * @var DaoFactory
	 */
	public $factory;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($factory, $view, $containerId=null)
	{
		parent::__construct('editDocument', 'POST', $action='');
		$this->template = 'document/editform.tpl';
		$this->factory = $factory;
		$this->view = $view;
		$this->elemtFactory = new \Form\ElementFactory($this, $factory);
		$spaceName = $factory->getName();

		//HIDDEN
		$this->addElement('hidden', 'page_id', 'DocManage_editdoc');
		$this->addElement('hidden', 'ticket');
		$this->addElement('hidden', 'id', null);

		//NUMBER
		$this->addElement('text', 'number' , tra('Number') , array('rows'=>2,'cols'=>32, $disabled, $readonly));

		//NAME
		$this->addElement('text', 'name' , tra('Name') , array('rows'=>2,'cols'=>32, $disabled, $readonly));

		//DESCRIPTION=DESIGNATION
		$this->addElement('textarea', 'description' , tra('Designation') , array('rows'=>2,'cols'=>32, $disabled, $readonly));

		//CATEGORY
		$params = array(
			'multiple' => false,
			'name' => 'categoryId',
			'description' => tra('Category'),
			'size' => '1',
			'returnName' => false,
			'disabled' => $disabled,
			'containerId'=>$containerId
		);
		$this->elemtFactory->selectCategory($params);

		//Add submit button
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));

		/* Rules */
		$this->addRule('description', tra('is required'), 'required');
		$this->addRule('number', 'This number is required', 'required');
		$mask = \Ranchbe::get()->getConfig('document.name.mask');
		$this->addRule('number', 'This number is not valid', 'regex', "/$mask/" , 'server');
		$this->addRule('name', 'Name is required', 'required');

		//Apply new \filters to the element values
		$this->applyFilter('__ALL__', 'trim');
	}

	public function setMode($mode='edit')
	{
		if($mode=='edit'){
			$this->removeElement('number');
			$this->removeElement('name');
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function OLD_____bind($document)
	{
		$this->binded = $object;
		$this->setDefaults($object->getArrayCopy());
		return $this;

		$values = $this->exportValues();
		foreach($values as $k=>$value){
			if($value==''){
				unset($values[$k]);
			}
		}

		foreach($this->extended as $asApp){
			$document->$asApp = $values[$asApp];
		}

		$document->hydrate($values);
		return $this;
	}

	/**
	 * In case of multi edit, add a document to edit
	 */
	public function add($document)
	{
		$this->documents[] = $document;
		$this->addElement('hidden', 'checked[]', $document->getId());
	}

}

