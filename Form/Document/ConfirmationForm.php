<?php
namespace Form\Document;

class ConfirmationForm extends \Form\AbstractForm
{
	public $count=0;

	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($factory, $view)
	{
		parent::__construct('confirmation', 'POST', $action=null);

		$this->template = 'document/confirmationform.tpl';
		$this->setAttribute('class', 'form-inline');
		$this->view = $view;
		$this->view->count = 0;

		//$this->elemtFactory = new \Form\ElementFactory($this);

		$this->addElement('hidden', 'spacename', $factory->getName());
		$this->addElement('submit', 'validate', 'Ok', array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
	}

	/**
	 * @param Document\Version $document
	 */
	public function addDocument($document)
	{
		$label = $document->getUid().' v'.$document->version.'.'.$document->iteration;
		$id = $document->getId();
		$attributes = array('class'=>'form-control');
		$cb = $this->addElement('advcheckbox','checked['.$this->count.']', $beforeLabel, $label, $attributes, $id);
		$cb->setChecked(true);
		$this->count++;
		$this->view->count = $this->count;
	}
}
