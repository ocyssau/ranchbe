<?php
namespace Form\Document\History;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$this->view = $view;
		$this->nameSpace = $nameSpace;
		$this->template = 'document/history/filterform.tpl';

		$this->elemtFactory = new \Form\ElementFactory($this, $factory);

		$this->addElement( 'text', 'f_action_name', 'Action', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'f_document_number', 'Document Number', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'f_document_version', 'Document Version', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'find', 'find', array('size'=>16, 'class'=>'form-control') );

		$this->elemtFactory->selectUser(array(
			'name'=>'f_action_user_name',
			'label'=>'By',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'handle',
			'sortOrder'=>'ASC',
		));

		//Find in field ----------------------------------------------
		$this->elemtFactory->select(
			array (
				'action_name' => 'Action Name',
				'document_number' => 'Number',
				'designation' => 'Designation',
				'document_access_code' => 'Access',
				'document_state' => 'State',
				'indice_value' => 'Indice',
				'document_version' => 'Version',
				'doctype_number' => 'Type',
			),
			array(
				'name'=>'find_field',
				'description'=>'In',
				'multiple'=>false,
				'returnName'=>false,
				'required'=>false,
				'size'=>1,
		));

		$this->addElement( 'advcheckbox', 'f_adv_search_cb', 'Advanced filter', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'advcheckbox', 'f_dateAndTime_cb', 'Date and time', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'advcheckbox', 'f_action_date_cb', 'Action date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_action_date_min', 'Superior to', array('class'=>'datepicker form-control') );
		$this->addElement( 'text', 'f_action_date_max', 'Inferior to', array('class'=>'datepicker form-control') );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//DOCUMENT NUMBER
		if($defaults['f_document_number']){
			$filter->andFind($defaults['f_document_number'], 'document_number', Op::OP_CONTAINS);
		}

		//DOCUMENT VERSION
		if($defaults['f_document_version']){
			$filter->andFind($defaults['f_document_version'], 'document_version', Op::OP_CONTAINS);
		}

		//ACTION NAME
		if($defaults['f_action_name']){
			$filter->andFind($defaults['f_action_name'], 'action_name', Op::OP_CONTAINS);
		}

		//ACTION USER
		if($defaults['f_action_user_name']){
			$filter->andFind($defaults['f_action_user_name'], 'action_by', Op::OP_CONTAINS);
		}

		//DATE AND TIME
		if($defaults['f_action_date_cb']){
			//FIND IN
			if($defaults['find'] && $defaults['find_field']){
				$filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
			}

			//ACTION DATE
			if($defaults['f_action_date_min']){
				if($defaults['f_action_date_min']){
					$filter->andFind($this->dateToSys($defaults['f_action_date_min']), 'action_date', Op::OP_SUP);
				}
				if($defaults['f_action_date_max']){
					$filter->andFind($this->dateToSys($defaults['f_action_date_max']), 'action_date', Op::OP_INF);
				}
			}
		}

		return $this;
	}
}

