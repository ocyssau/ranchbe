<?php
namespace Form\Document;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 *
	 * @var string
	 */
	public $spaceName;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($view, $factory, $nameSpace, $containerId=null)
	{
		parent::__construct($view, $factory, $nameSpace);
		$this->template = 'document/filter.tpl';
		$this->spaceName = $spaceName;
		$elemtFactory = $this->elemtFactory;

		$allField[0] = array (
			'designation' => 'Designation',
			//'document_number' => 'Number',
			//'category_number' => 'Category',
			//'document_access_code' => 'Access',
			//'document_state' => 'State',
			'indice_value' => 'Indice',
			'document_version' => 'Version',
			//'doctype_number' => 'Type',
		);

		//Find in field ----------------------------------------------
		$list = array_merge(array(
			'category_number'=>'Category',
			'indice_value'=>'Indice',
			'doctype_number'=>'Doctype',
		), $allField[0]);

		$elemtFactory->select($list, array(
			'name'=>'find_field',
			'label'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Select action by user ----------------------------------------------
		$actions = array(
			'check_out_by'=>'Checkout By',
			'update_by'=>'Update By',
			'open_by'=>'Created By',
		);
		$elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'label'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Add elements ----------------------------------------------
		$this->addElement( 'advcheckbox', 'displayHistory', 'Display history', ' ', array('class'=>'submitOnClick') );
		$this->addElement( 'advcheckbox', 'onlyMy', 'Only me', ' ', array('class'=>'submitOnClick') );
		$this->addElement( 'advcheckbox', 'checkByMe', 'Only check by me', ' ', array('class'=>'submitOnClick') );
		$this->addElement( 'advcheckbox', 'displayThumbs', 'Display thumbnails', ' ', array('class'=>'submitOnClick') );
		$this->addElement( 'text', 'find_doctype', 'Doctype', array('size'=>8) );
		$this->addElement( 'text', 'find_state', 'State', array('size'=>8) );


		$this->addElement( 'advcheckbox', 'f_check_out_date_cb', 'CheckOut date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_check_out_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_check_out_date_max', 'Inferior to', array('class'=>'datepicker') );

		$this->addElement( 'advcheckbox', 'f_update_date_cb', 'Update date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_update_date_min', 'Superior to', array('class'=>'datepicker form-control') );
		$this->addElement( 'text', 'f_update_date_max', 'Inferior to', array('class'=>'datepicker form-control') );

		//select category ----------------------------------------------
		$elemtFactory->selectCategory(array(
			'name'=>'find_category',
			'label'=>'Category',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
			'sortBy'=>'category_number',
			'sortOrder'=>'ASC',
			'containerId'=>$containerId
		));

		//select access ----------------------------------------------
		$list=array(
			'free'=>'Free',
			'1'=>'CheckedOut',
			'5'=>'InWorkflow',
			'10'=>'Validated',
			'11'=>'Locked',
			'12'=>'Marked to suppress',
			'15'=>'Historical',
		);
		$elemtFactory->select($list, array(
			'name'=>'find_access_code',
			'label'=>'Access',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		if($defaults['onlyMy']){
			$me = \Ranchbe::getCurrentUser()->getid();
			$subFilter = new DaoFilter('', false);
			$subFilter->orFind($me, 'check_out_by', Op::OP_EQUAL);
			$subFilter->orFind($me, 'update_by', Op::OP_EQUAL);
			$subFilter->orFind($me, 'open_by', Op::OP_EQUAL);
			$filter->subor($subFilter);
		}

		if($defaults['checkByMe']){
			$me = \Ranchbe::getCurrentUser()->getid();
			$filter->andFind($me, 'check_out_by', Op::OP_EQUAL);
		}

		if(!$defaults['displayHistory']){
			$filter->andfind( array(15, 20), 'document_access_code', Op::OP_NOTBETWEEN );
		}

		//NUMBER
		if($defaults['find_number']){
			$filter->andFind($defaults['find_number'], 'document_number', Op::OP_CONTAINS);
		}

		//DESIGNATION
		if($defaults['find_designation']){
			$filter->andFind($defaults['find_designation'], 'designation', Op::OP_CONTAINS);
		}

		//DOCTYPE
		if($defaults['find_doctype']){
			$filter->with(array(
				'table'=>'doctypes',
				'on'=>'doctype_id',
				'alias'=>'doctype',
				'select'=>array('doctype_number'),
				'direction'=>'outer'
			));
			$filter->andFind($defaults['find_doctype'], 'doctype_number', Op::OP_CONTAINS);
		}

		//ACCESS CODE
		if($defaults['find_document_access_code']){
			if ($defaults['find_document_access_code'] == 'free'){
				$acode = 0;
			}
			else{
				$acode = $defaults['find_document_access_code'];
			}
			$filter->andFind($acode, 'document_access_code', Op::OP_EQUAL);
		}

		//STATE
		if($defaults['find_document_state']){
			$filter->andFind($defaults['find_document_state'], 'document_state', Op::OP_EQUAL);
		}

		//CATEGORY
		if($defaults['find_category']){
			$filter->andFind($defaults['find_category'], 'category_id', Op::OP_EQUAL);
		}

		//ADVANCED SEARCH
		if($defaults['f_adv_search_cb']){
			//FIND IN
			if($defaults['find'] && $defaults['find_field']){
				$filter->with(array(
					'table'=>$this->container->SPACE_NAME.'_categories',
					'on'=>'category_id',
					'alias'=>'category',
					'select'=>array('category_number'),
					'direction'=>'outer'
				));
				$filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
			}

			//bind defaults date and action filters
			$this->_bindAdvancedOptions($filter, $defaults);

			//DATE AND TIME
			if($defaults['f_dateAndTime_cb']){
				//CHECKOUT
				if($defaults['f_check_out_date_cb']){
					if($defaults['f_check_out_date_min']){
						$filter->andFind($this->dateToSys($defaults['f_check_out_date_min']), 'check_out_date', Op::OP_SUP);
					}
					if($defaults['f_check_out_date_max']){
						$filter->andFind($this->dateToSys($defaults['f_check_out_date_min']), 'check_out_date', Op::OP_INF);
					}
				}
				//UPDATE
				if($defaults['f_update_date_cb']){
					if($defaults['f_update_date_min']){
						$filter->andFind($this->dateToSys($defaults['f_update_date_min']), 'update_date', Op::OP_SUP);
					}
					if($defaults['f_update_date_max']){
						$filter->andFind($this->dateToSys($defaults['f_update_date_max']), 'update_date', Op::OP_INF);
					}
				}
			}
		}

		//EXTENDED
		if(is_array($this->extended)){
			foreach($this->extended as $val){
				$fieldName = $val['appname'];
				if($defaults[$fieldName]){
					$filter->andFind($defaults[$fieldName], $fieldName, Op::OP_CONTAINS);
				}
			}
		}

		return $this;
	}

	/**
	 *
	 * @param unknown_type $optionalFields
	 */
	public function setExtended($optionalFields)
	{
		if(is_array($optionalFields)){
			foreach($optionalFields as $val){
				$extended[0][$val['appname']] = $val['field_description'];
				$this->addElement( 'text', $val['appname'], $val['field_description'], array('size'=>$val['field_size']) );
			}
		}
		$this->view->assign('extended', $optionalFields);
		$this->extended = $optionalFields;
	}

	/**
	 *
	 */
	public function reset($filter)
	{
		$_SESSION['filter'][$this->nameSpace]=null;
		$filter->andfind( array(15, 20), 'document_access_code', Op::OP_NOTBETWEEN );
		return $this;
	}
}

