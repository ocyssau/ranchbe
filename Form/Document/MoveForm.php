<?php
namespace Form\Document;

class MoveForm extends \Form\AbstractForm
{
	/**
	 * @var DaoFactory
	 */
	public $factory;

	/**
	 * @param string $spaceName		space name as Workitem, Cadlib...
	 * @param Smarty $view
	 * @param string $spaceName		nameSpace for session recording
	 */
	public function __construct($factory, $view)
	{
		parent::__construct('copyDocument', 'POST', $action='');
		$this->setAttribute('class', 'form-inline');
		$this->template = 'document/moveform.tpl';
		$this->factory = $factory;
		$this->view = $view;
		$formFactory = new \Form\ElementFactory($this, $factory);
		$spaceName = $factory->getName();

		//HIDDEN
		$this->addElement('hidden', 'page_id', 'DocManage_move');
		$this->addElement('hidden', 'spaceName', $spaceName);

		//TARGET CONTAINER
		$formFactory->selectContainer(array(
			'name'=>'containerid',
			'label'=>'Target Container',
			'multiple'=>false,
			'return_name'=>false,
			'required'=>true,
			'size'=>1,
		));

		//SUBMIT
		$this->addElement('submit', 'validate', tra('Ok'), array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', tra('Cancel'), array('class'=>'btn btn-default'));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function add($document)
	{
		$this->documents[] = $document;
		$this->addElement('hidden', 'checked[]', $document->getId());
		return $this;
	}

}

