<?php
namespace Form\Document;

use Rbplm\Ged\Docfile\Role;


class AssociateFileForm extends \Form\Wildspace\SelectFileForm
{
	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($wildspace, $filter=null, $view)
	{
		parent::__construct($wildspace, $filter, $view);
		$this->template = 'document/manager/associatefileform.tpl';

		$selectSet = Role::toArray();
		$this->elemtFactory->select($selectSet, array(
				'name'=>'mainrole',
				'returnName'=>false,
				'label'=>'Select Role',
				'required'=>true,
				'size'=>1
		));

		$this->addElement('submit', 'validate', tra('Ok'), array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', tra('Cancel'), array('class'=>'btn btn-default'));
	}
}
