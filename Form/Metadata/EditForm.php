<?php
namespace Form\Metadata;

/**
 *
 *
 */
class EditForm extends \Form\AbstractForm
{
	public $optionalFields = array();
	public $mode; //edit | create

	public $fieldTypes = array(
		'text',
		'longtext',
		'htmlarea',
		'partner' ,
		'doctype' ,
		'user' ,
		'version' ,
		'process' ,
		'category' ,
		'date' ,
		'integer' ,
		'decimal',
		'select' ,
		'selectFromDB',
		'liveSearch'
	);

	/**
	 * @param \Smarty
	 * @param \space
	 */
	public function __construct($view, $factory)
	{
		\HTML_QuickForm::__construct('MetadataEdit', 'POST');
		$this->template = 'metadata/editform.tpl';
		$this->setAttribute('class', 'inline-form');
		$this->view = $view;
		$this->daoFactory = $factory;
		$elemtFactory = $this->getElemtFactory($factory);

		//Hidden
		$this->addElement('hidden', 'spacename', strtolower($factory->getName()));
		$this->addElement('hidden', 'id');
		$this->addElement('hidden', 'layout');

		//Add submit button
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Save', array('class'=>'btn btn-success'));

		//Add form elements
		$this->addElement('text', 'appName', tra('Name'), array('size'=>40, 'class'=>'form-control'));
		$this->addElement('text', 'description', tra('Description'), array('size'=>40,'class'=>'form-control'));
		$this->addElement('text', 'label', tra('Label'), array('size'=>40,'class'=>'form-control'));

		$selectSet = array_combine($this->fieldTypes,$this->fieldTypes);
		$select = $this->addElement('select', 'type' , tra('Type'), $selectSet , array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		$selectSet = array(
			'569e92709feb6'=>'Document',
			'569e94192201a'=>'Workitem'
		);
		$select = $this->addElement('select', 'extendedcid' , tra('Extend Class'), $selectSet , array('class'=>'form-control'));
		$select->setSize(1);
		$select->setMultiple(false);

		$this->addElement('textarea', 'regex', tra('Regex'), array('cols'=>40, 'class'=>'form-control'));
		$this->addElement('textarea', 'list', tra('List'), array('cols'=>40, 'class'=>'form-control'));
		$this->addElement('checkbox', 'required', tra('Required'));
		$this->addElement('checkbox', 'multiple', tra('Multiple'));
		$this->addElement('checkbox', 'return', tra('Return Name'));
		$this->addElement('text', 'size', tra('Size'), array('size'=>10,'class'=>'form-control'));
		$this->addElement('text', 'dbtable', tra('Db Table Name'), array('cols'=>40,'class'=>'form-control'));
		$this->addElement('text', 'forValue', tra('For Value'), array('cols'=>40,'class'=>'form-control'));
		$this->addElement('text', 'forDisplay', tra('For Display'), array('cols'=>40,'class'=>'form-control'));
		$this->addElement('text', 'dateFormat', tra('Date Format'), array('cols'=>20,'class'=>'form-control'));

		$this->addElement('text', 'min', tra('Min Value'), array('cols'=>20,'class'=>'form-control'));
		$this->addElement('text', 'max', tra('Max Value'), array('cols'=>20,'class'=>'form-control'));
		$this->addElement('text', 'step', tra('Step between value'), array('cols'=>20,'class'=>'form-control'));

		//Add validation rules to check input data
		$this->addRule('size', 'Should be numeric', 'numeric', null, 'server');
		$this->addRule('appName', tra('Name is required'), 'required');
		$this->addRule('appName', tra('name should contains lower letters and/or _ only'), 'regex', '/^[a-z_]+$/', 'server');
		$this->addRule('description', tra('Description is required'), 'required');
		$this->addRule('type', tra('Type is required'), 'required');

		// applies new filters to the element values
		$this->applyFilter('__ALL__', 'trim');
	}

	/**
	 *
	 * @param array $values
	 * @return AbstractForm
	 */
	public function OLD___setData($values)
	{
		unset($values['dao']);
		$this->setDefaults($values);
		return $this;
	}

	/**
	 *
	 */
	public function OLD____bind($property)
	{
		$values = $this->exportValues();
		$property->hydrate($values);
		return $this;
	} //End of function

}
