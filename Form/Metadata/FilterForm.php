<?php
namespace Form\Metadata;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{

	/**
	 * @param project $project
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		$action = ($action == '') ? $_SERVER['REDIRECT_URL'] : $action;
		\HTML_QuickForm::__construct('FilterForm', 'POST', $action);
		$this->setAttribute('class','form-inline');

		$this->nameSpace = $nameSpace;
		$this->view = $view;
		$this->template = 'metadata/filter.tpl';

		$factory = $factory;
		$elemtFactory = new \Form\ElementFactory($this, $factory);

		//Hidden elements
		$this->addElement( 'hidden', 'spacename', $factory->getName() );

		//Add elements ----------------------------------------------
		$this->addElement( 'text', 'find_name', 'Number', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'find_description', 'Description', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'text', 'find_label', 'Label', array('size'=>16, 'class'=>'form-control') );

		$selectSet = array(
			'569e92709feb6'=>'Document',
			'569e94192201a'=>'Workitem'
		);
		$elemtFactory->select($selectSet, array(
			'name'=>'find_extendedcid',
			'multiple'=>false,
			'size'=>1,
			'label'=>'Extended Class'
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_name']){
		    $filter->andFind($defaults['find_name'], 'field_name', Op::OP_CONTAINS);
		}

		//DESIGNATION
		if($defaults['find_description']){
		    $filter->andFind($defaults['find_description'], 'field_description', Op::OP_CONTAINS);
		}

		//LABEL
		if($defaults['find_label']){
			$filter->andFind($defaults['find_label'], 'label', Op::OP_CONTAINS);
		}

		//Cid
		if($defaults['find_extendedcid']){
			$filter->andFind($defaults['find_extendedcid'], 'extendedCid', Op::OP_EQUAL);
		}

		return $this;
	}

}
