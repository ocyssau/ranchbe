<?php
namespace Form\Docfile;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $factory, $nameSpace)
	{
		parent::__construct($view, $factory, $nameSpace);
		$this->template = 'docfile/filter.tpl';
		$this->spaceName = $spaceName;
		$elemtFactory = $this->elemtFactory;

		$allField = array (
			'file_extension' => 'Extensions',
			'import_order' => 'Import order',
			'file_path' => 'Path',
			'file_open_by' => 'Created by' ,
			'file_open_date' => 'Created date',
			'file_update_by' => 'Last update by',
			'file_update_date' => 'Last update date',
			'file_access_code' => 'Access',
			'file_state' => 'State',
			'file_version' => 'Version',
			'file_type' => 'Type',
			'file_size' => 'Size',
		);

		//Find in field ----------------------------------------------
		$elemtFactory->select($allField, array(
			'name'=>'find_field',
			'description'=>'In',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Select action by user ----------------------------------------------
		$actions = array(
			'file_checkout_by'=>'Checkout By',
			'file_update_by'=>'Update By',
			'file_open_by'=>'Created By',
		);
		$elemtFactory->select($actions, array(
			'name'=>'f_action_field',
			'description'=>'Action',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));

		//Add elements ----------------------------------------------
		$this->addElement( 'text', 'find_state', 'State', array('size'=>8) );

		$this->addElement( 'advcheckbox', 'f_check_out_date_cb', 'CheckOut date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_check_out_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_check_out_date_max', 'Inferior to', array('class'=>'datepicker') );

		$this->addElement( 'advcheckbox', 'f_update_date_cb', 'Update date', ' ', array('onclick'=>'displayOption(this);', 'class'=>'optionSelector') );
		$this->addElement( 'text', 'f_update_date_min', 'Superior to', array('class'=>'datepicker') );
		$this->addElement( 'text', 'f_update_date_max', 'Inferior to', array('class'=>'datepicker') );

		//select access ----------------------------------------------
		$list = array(
			'free'=>'Free',
			'1'=>'CheckedOut',
			'5'=>'InWorkflow',
			'10'=>'Validated',
			'11'=>'Locked',
			'12'=>'Marked to suppress',
			'15'=>'Historical',
		);
		$elemtFactory->select($list, array(
			'name'=>'find_access_code',
			'description'=>'Access',
			'multiple'=>false,
			'returnName'=>false,
			'required'=>false,
			'size'=>1,
		));
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//NUMBER
		if($defaults['find_number']){
		    $filter->andFind($defaults['find_number'], 'file_name', Op::OP_CONTAINS);
		}

		//ACCESS CODE
		if($defaults['find_access_code']){
		    if ($defaults['find_access_code'] == 'free'){
		        $acode = 0;
		    }
		    else{
		        $acode = $defaults['find_access_code'];
		    }
		    $filter->andFind($acode, 'file_access_code', Op::OP_EQUAL);
		}

		//STATE
		if($defaults['find_state']){
		    $filter->andFind($defaults['find_state'], 'file_state', Op::OP_EQUAL);
		}

		//ADVANCED SEARCH
		if($defaults['f_adv_search_cb']){
		    //FIND IN
		    if($defaults['find'] && $defaults['find_field']){
		        $filter->andFind($defaults['find'], $defaults['find_field'], Op::OP_CONTAINS);
		    }

		    //bind defaults date and action filters
		    $this->_bindAdvancedOptions($filter, $defaults);

		    //DATE AND TIME
		    if($defaults['f_dateAndTime_cb']){
		        //CHECKOUT
		        if($defaults['f_check_out_date_cb']){
		            if($defaults['f_check_out_date_min']){
		                $filter->andFind($this->dateToSys($defaults['f_check_out_date_min']), 'check_out_date', Op::OP_SUP);
		            }
		            if($defaults['f_check_out_date_max']){
		                $filter->andFind($this->dateToSys($defaults['f_check_out_date_min']), 'check_out_date', Op::OP_INF);
		            }
		        }
		        //UPDATE
		        if($defaults['f_update_date_cb']){
		            if($defaults['f_update_date_min']){
		                $filter->andFind($this->dateToSys($defaults['f_update_date_min']), 'update_date', Op::OP_SUP);
		            }
		            if($defaults['f_update_date_max']){
		                $filter->andFind($this->dateToSys($defaults['f_update_date_max']), 'update_date', Op::OP_INF);
		            }
		        }
		    }
		}
		return $this;
	}

	/**
	 *
	 */
	public function reset($filter)
	{
		$_SESSION['filter'][$this->nameSpace]=null;
		$filter->andfind( array(15, 20), 'file_access_code', Op::OP_NOTBETWEEN );
		return $this;
	}
}

