<?php
namespace Form\Renderer;

class CollectionRenderer extends DefaultRenderer
{

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$headerTemplate = "<h1>{header}</h1>";

		$elementTemplate ='<tr><td colspan=100>{element}</td></tr>';

		$formTemplate ='<form{attributes}>{hidden}<table class="normal table table-bordered">{content}</table></form>';
		$requiredNoteTemplate ='{requiredNote}';

		$this->setFormTemplate($formTemplate);
		$this->setHeaderTemplate($headerTemplate);
		$this->setElementTemplate($elementTemplate);
		$this->setRequiredNoteTemplate($requiredNoteTemplate);
	}
}
