<?php
namespace Form\Renderer;

require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

class ArraySmarty extends \HTML_QuickForm_Renderer_ArraySmarty
{

	/**
	 * Constructor
	 *
	 * @param  object  reference to the Smarty template engine instance
	 * @access public
	 */
	public function __construct($view,$staticLabels=false)
	{
		parent::HTML_QuickForm_Renderer_ArraySmarty($view, $staticLabels);
		$this->setRequiredTemplate(
			'{if $error}
			<font color="red">{tr}{$label|upper}{/tr}</font>
			{else}
			{$label}
			{if $required}
			<font color="red" size="1">*</font>
			{/if}
			{/if}'
		);
	}
}
