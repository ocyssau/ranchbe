<?php
namespace Form\Renderer;

class SubformRenderer extends DefaultRenderer
{

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$formTemplate = '<tr class=""><div class="form-inline">{hidden}{content}</div></tr>';

		$elementTemplate = '<td>
			<!-- BEGIN required --><span class="rbform-required-note">*</span><!-- END required -->
			<label class="control-label">{label}</label>
			<div class="input-group">
			{element}
			<!-- BEGIN error --><span class="rbform-error-note">{error}</span><br /><!-- END error -->
			</div>
		</td>';

		$this->setFormTemplate($formTemplate);
		$this->setHeaderTemplate($headerTemplate);
		$this->setElementTemplate($elementTemplate);
	}
}
