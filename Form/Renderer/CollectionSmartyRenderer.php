<?php
namespace Form\Renderer;

class CollectionSmartyRenderer extends ArraySmarty
{
	/**
	 * Constructor
	 *
	 * @param  object  reference to the Smarty template engine instance
	 * @access public
	 */
	public function __construct($view, $template)
	{
		parent::__construct($view);
		$this->template = $template;
	}

	/**
	 *
	 */
	public function toHtml()
	{
		$formAsArray = $this->toArray(true);
		$this->_tpl->assign('form', $formAsArray);
		$this->_tpl->assign('fieldset', $this->_html);
		$this->_tpl->assign('loop', $this->loop);
		$html = $this->_tpl->fetch($this->template);
		return $html;
	}
}
