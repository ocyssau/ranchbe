<?php
namespace Form\Renderer;

include_once('HTML/QuickForm/Renderer/Default.php');

class DefaultRenderer extends \HTML_QuickForm_Renderer_Default
{

	/**
	 *
	 */
	public function __construct()
	{
		parent::HTML_QuickForm_Renderer_Default();

		$requiredNoteTemplate ="{requiredNote}";
		$this->setRequiredNoteTemplate($requiredNoteTemplate);

		$this->setElementTemplate(
			'<tr><td>
			<!-- BEGIN required --><span class="rbform-required-note">*</span><!-- END required -->
			<div class="control-label">{label}</div>
			<!-- BEGIN error --><span class="rbform-error-note">{error}</span><br /><!-- END error -->
			{element}
			</td>
			</tr>'
		);

		$this->setHeaderTemplate(
			'<tr><td colspan=2 style="align=center">
			<h1>{header}</h1></td>
			</tr>'
		);

		$this->setFormTemplate(
			'<form{attributes}>
			<table class="form">
			{content}
			</table>
			</form>');
	}
}
