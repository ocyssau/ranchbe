<?php
namespace Form;

require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

class Rendererset extends \HTML_QuickForm_Renderer_ArraySmarty
{
	/**
	 * 
	 */
	public function __construct($form, $view, $assignTo='form')
	{
		parent::__construct($view, true);
		
		$this->setRequiredTemplate(
				'{if $error}
				<font color="red">{tr}{$label|upper}{/tr}</font>
				{else}
				{$label}
				{if $required}
				<font color="red" size="1">*</font>
				{/if}
				{/if}'
		);
		
		$this->setErrorTemplate(
				'{if $error}
				<font color="orange" size="1">{$error}</font><br />
				{/if}{$html}'
		);
		$form->accept($this);
		$view->assign($assignTo, $this->toArray());
	}
}
