<?php
namespace Form\Wildspace;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;
use Rbplm\Sys\Filesystem;


class SelectFileForm extends \Form\AbstractForm
{
	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($wildspace, $filter=null, $view)
	{
		$this->template = 'workplace/wildspace/selectfileform.tpl';
		$this->wildspace = $wildspace;
		$this->view = $view;
		$this->elemtFactory = new \Form\ElementFactory($this);

		/* HIDDEN */
		$this->addElement('hidden', 'documentid', null);

		/* FILES */
		$path = $wildspace->getPath();
		$list = $wildspace::getDatas($path, $filter);
		foreach($list as $file){
			$selectSet[$file['name']] = $file['name'];
		}

		$this->elemtFactory->select($selectSet, array(
			'name'=>'file',
			'returnName'=>false,
			'label'=>'Select File',
			'required'=>true,
			'size'=>1
			));


		/* SUBMIT */
		$this->addElement('submit', 'validate', tra('Ok'), array('class'=>'btn btn-success'));
		$this->addElement('submit', 'cancel', tra('Cancel'), array('class'=>'btn btn-default'));
	}
}
