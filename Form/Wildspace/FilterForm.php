<?php
namespace Form\Wildspace;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;
use Rbplm\Sys\Filesystem;


class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 *
	 * @param container $container
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($wildspace, $view, $nameSpace)
	{
		$this->template = 'workplace/wildspace/filter.tpl';
		$this->wildspace = $wildspace;
		$this->view = $view;

		$this->addElement('text', 'find', 'find', array('size'=>16, 'class'=>'form-control') );
		$this->addElement( 'advcheckbox', 'displayMd5', 'Display Md5', '', array('class'=>'submitOnClick', 'id'=>'displayMd5'));
		$this->addElement( 'advcheckbox', 'displayNew', 'Display Only New', '', array('class'=>'submitOnClick', 'id'=>'displayNew'));

		$allField = array (
			'file_name' => 'file_name',
			'designation' => 'designation',
			'file_size' => 'file_size',
			'file_path' => 'file_path',
			'file_mtime' => 'file_mtime' ,
			'file_md5' => 'file_md5',
			'file_extension' => 'file_extension',
			'file_racine' => 'file_racine',
			'file_type' => 'file_type',
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(Filesystem\Filter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		if($defaults['find']){
			$filter->andFind($defaults['find'], 'name', Op::CONTAINS);
		}

		if($defaults['displayMd5']){
			$this->displayMd5 = true;
		}
		if($defaults['displayNew']){
			$this->displayNew = true;
		}

		return $this;
	}
}

