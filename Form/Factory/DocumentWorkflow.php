<?php
namespace Form\Factory;

/**
 * Factory to create form definition of workflow to run on document.
 * @author olivier
 *
 */
class DocumentWorkflow
{

	protected $_feedbacks = array();
	protected $_errors = array();
	protected $loopId = 1;

	protected $doc_properties = array();
	protected $document;
	protected $process;
	protected $workflow;
	protected $instance;
	protected $iid;
	protected static $_previous=array(); //array()

	//public $subformTemplate = "<table class=\"normal\"><tr>\n{hidden}\n{content}\n</tr></table>";
	public $subformTemplate = "<tr class=odd>\n{hidden}\n{content}\n</tr>";

	public $subheaderTemplate = "<h1>{header}</h1>";

	public $subelementTemplate =
	"<td NOWRAP>{label}
	<!-- BEGIN required -->
	<font color=red>*</font>
	<!-- END required -->
	<!-- BEGIN error -->
	<font color=red><i><b><br />{error}</b></i></font>
	<!-- END error -->
	<br />
	{element}
	</td>";

	public $feedbackTemplate = "<font color=green>{feedback}</font><br />";

	public $errorTemplate = "<font color=red><b>{error}</b></font><br />";

	public $error_stack;

	/**
	 *
	 */
	function __construct(&$workflow)
	{
		$this->workflow = $workflow;
		$this->_feedbacks = array();
		$this->_errors = array();
		$this->_isFrozen = false;
	}//End of method

	/**
	 *
	 * @param Document $document
	 */
	public function setDocument($document)
	{
		$this->document = $document;
		return $this;
	}

	/**
	 *
	 * @param Document $document
	 */
	public function setProcess($process)
	{
		$this->process = $process;
		return $this;
	}

	/**
	 *
	 * @param Document $document
	 */
	public function setInstance($instance)
	{
		$this->instance = $instance;
		return $this;
	}

	/**
	 * Genere une ligne de formulaire dans le cas du store multiple
	 *
	 * @param unknown_type $loop_id
	 * @param unknown_type $docaction
	 * @param unknown_type $validate
	 */
	public function buildForm($loopId=1, $docaction=null, $validate=false)
	{
		$this->form = new \Form\Sub('form_'.$loopId, 'post');
		$document = $this->document;

		$defaultRender =& $this->form->defaultRenderer();
		$defaultRender->setFormTemplate($this->subformTemplate);
		$defaultRender->setHeaderTemplate($this->subheaderTemplate);
		$defaultRender->setElementTemplate($this->subelementTemplate);

		$this->form->_requiredNote = '';
		$this->loopId = $loopId;

		if( $this->instance ){ //If a workflow instance is running
			//Check if activity is identical to previous selected document process : to ensure coherence between selected documents
			if( self::$_previous['activityId'] ){
				if( self::$_previous['activityId'] !== $runningActivityId ){
					$msg = 'document %s have not same state that %s. it is not selected';
					$e1 = $document->getName();
					$e2 = $this->session->current['document_number'];
					$this->_errors[] = sprintf( tra($msg), $e1, $e2);
				}
			}
		}
		else{  // If no instance link get the process and display activities
			//Check if this item has access free
			$accessCode = $document->checkAccess();
			if(!($accessCode == 0 || ( $accessCode > 1 && $accessCode <= 5 ) ) ){
				$this->_errors[] = tra('This item is not free');
			}

			//Check if there is a process linked to doctype, container, project
			if( !$this->process ){
				$this->_errors[] = tra('no default process is find');
			}

			//check if process is valid
			if( !$this->process->isValid() ){
				$this->_errors[] = tra('This process is not valide');
			}

			//Check if process is identical to previous document process : to ensure coherence between documents
			if( self::$_previous['pId'] ){
				if( self::$_previous['pId'] != $this->process->getId()){
					$msg = 'document %element% is rejected : have not same process that other selected document';
					$e = $document->getName();
					$this->_errors[] = sprintf( tra($msg), $e );
				}
			}
		} //End of else

		$this->_feedbackToElement();

		if($this->_errors){
			$font_color = 'red';
		}
		else{
			$font_color = 'green';
			$this->form->addElement('hidden', 'document_id['.$this->loopId.']', $document->getId() );
			self::$_previous['pId'] = $this->process->getId();
			self::$_previous['activityId'] = $runningActivityId;
		}

		//Display document infos
		$html = '<b><font color='.$font_color.'>'.$document->getName().'</font></b><br />'.
				'<i>'.$document->description.'</i><br />'.
				$document->getLockName().
				' <b>'.$document->lifeStage.'</b>';
		$this->form->addElement('static', '', $html );

		//Display infos about process
		$html = $this->process->getNormalizedName() .
		' ( '. $this->process->getTitle() .' )';
		$this->form->addElement('static', '', $html );

		$dateFormat = \Ranchbe::get()->getConfig('view.longdate.format');

		//Display infos about running instance
		if($this->instance){
			$instance = $this->instance;
			$html =
			'<i>'.tra('Instance').'</i>: '.$instance->getName().'('.$instance->getTitle().')<br />'.
			'<i>'.tra('Created').'</i>: '.$instance->getStarted($dateFormat).'<br />'.
			'<i>'.tra('Status').'</i>: '.$instance->getStatus().'<br />'.
			'<i>'.tra('Owner').'</i>: '.$instance->getOwner(true).'<br />'.
			'<i>'.tra('Next user').'</i>: '.$instance->getNextUsers().'<br />'
			;
		}
		else{
			$html = 'none';
		}
		$this->form->addElement('static', '', $html );

		return $this->form;
	} //End of function

	/**
	 * generate html for feedback
	 */
	protected function _feedbackToElement()
	{
		if($this->_errors){
			foreach($this->_errors as $error){
				$this->form->addElement('hidden', 'errors['.$this->loopId.'][]', $error);
				$error_text .= str_replace('{error}', $error, $this->errorTemplate);
			}
			$semaphore = '<font color="red" size="1"><b>ERROR :</b></font>
			<img border="0" alt="FATAL" src="img/error30.png" />';
			$this->form->addElement('static', '', $semaphore.' '.$error_text);
		}
		else{
			foreach($this->_feedbacks as $feedback){
				$this->form->addElement('hidden', 'feedbacks['.$this->loopId.'][]', $feedback);
				$feedback_text .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
			}
			$semaphore = '<font color="green" size="1"><b>SELECTED</b></font>
			<img border="0" alt="OK" src="img/accept.png" />';
			$this->form->addElement('static', '', $semaphore.' '.$feedback_text);
		}
	} //End of function

	/**
	 *
	 * @return multitype:
	 */
	public function getErrors()
	{
		return $this->_errors;
	} //End of function

	/**
	 *
	 */
	public function getFeedbacks()
	{
		return $this->_feedbacks;
	} //End of function

	/**
	 *
	 */
	public function getDocflow()
	{
		return $this->workflow;
	} //End of function

} //End of class

