<?php

namespace Form;

use Rbs\Space\Factory as DaoFactory;
use Workflow\Model\Wf;
use Rbplm\Org\Project;
use Rbplm\Org\Workitem;
use Rbplm\Ged\Category;
use Rbplm\People\User;
use Rbs\Ged\Document\IndiceDao as DocIndices;

class ElementFactory
{
	/**
	 *
	 * @var DaoFactory
	 */
	protected $factory;

	/**
	 *
	 * @var DaoFactory
	 */
	protected $form;

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function __construct($form, $factory=null)
	{
		$this->form = $form;
		$this->factory = $factory;
	}

	/**
	 *
	 * @param DaoFactory $factory
	 */
	public function setDaoFactory($factory)
	{
		$this->factory = $factory;
	}

	/**
	 *
	 $params = array(
	 'where',
	 'name',
	 'label',
	 'value',
	 'multiple',
	 'returnName',
	 'required',
	 'advSelect',
	 'display_both',
	 'disabled',
	 'fieldId',
	 );
	 *
	 * @param array $list
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function select($list, $params, $validation = 'client')
	{
		$form = $this->form;

		(empty($params['fieldId'])) ? $params['fieldId'] = $params['name'] : null;
		(!isset($params['label'])) ? $params['label'] = $params['name'] : null;
		(isset($params['class'])) ? $class = $params['class'] : $class = 'form-control rb-select';

		if(!empty($params['value'])){
			$form->setDefaults(array(
				$params['name'] => $params['value'],
			));
		}

		//Construct array for selection set
		if($params['advSelect'] == 0){
			$selectSet[NULL] = ''; //Leave a blank option for default none selected. Not useful for advanced select
		}

		if(is_array($list)){
			foreach ( $list as $key=>$values ){
				if($params['display_both']){
					$selectSet[$key] = $key.'-'.$values;
				}
				else{
					$selectSet[$key] = $values;
				}
			}
		}
		else {
			$selectSet = array();
		}

		if($params['advSelect'] == 1){
			$form->removeAttribute('name');        // XHTML compliance
			//Construct object for advanced select
			require_once 'HTML/QuickForm/advmultiselect.php';
			$select =& $form->addElement('advmultiselect', $params['name'], tra($params['label']), $selectSet,
				array('size' => 5,
					'class' => 'pool', 'style' => 'width:300px;'
				)
			);
			$select->setLabel(array(tra($params['label']), 'Available', 'Selected'));

			$select->setButtonAttributes('add',    array('value' => 'Add >>',
				'class' => 'inputCommand'
			));
			$select->setButtonAttributes('remove', array('value' => '<< Remove',
				'class' => 'inputCommand'
			));
		}
		else{
			//Construct object for normal select
			$select = $form->addElement( 'select', $params['name'] , tra($params['label']), $selectSet, array($params['disabled'],'id'=>$params['fieldId'], 'class'=>$class) );

			//Set the size
			if(isset($params['size'])){
				$select->setSize($params['size']);
			}
			else {
				$select->setSize(5);
			}

			//Enable multi selection
			if($params['multiple']){
				$select->setMultiple(true);
			}
			else{
				$select->setMultiple(false);
			}
		}

		//Add required rule
		if($params['required']){
			$form->addRule($params['name'], tra('is required'), 'required', $validation);
		}
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectPartner($params, $validation = 'client')
	{
		$form = $this->form;
		$factory = $this->factory;

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;

		$select = array(
			'child.partner_id AS id',
			'child.partner_number AS name'
		);

		/* @var \Rbs\Dao\DaoList $list */
		$list = $factory->getList(\Rbs\People\Somebody::$classId);
		$list->load();

		$selectSet = array();
		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['name']] = $item['name'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['id']] = $item['name'];
			}
		}
		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectDoctype($params, $validation = 'client')
	{
		$form = $this->form;

		if(!empty($params['where'])){
			$p['where'][] = $params['where'];
		}

		$p['select'] = array('doctype_id' , 'doctype_number');
		$p['sortOrder']='ASC';
		$p['sortBy']='doctype_number';


		if(!isset($params['returnName'])) $params['returnName'] = true;

		/* Construct array for set partner select options */
		if(!$params['returnName']){
			foreach ( $doctypeList as $key=>$values ){
				$doctypeSelectSet[$values['doctype_id']] = $values['doctype_number'];
			}
		}
		else{
			foreach ( $doctypeList as $key=>$values ){
				$doctypeSelectSet[$values['doctype_number']] = $values['doctype_number'];
			}
		}

		$this->select($doctypeSelectSet , $params , $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param unknown_type $spaceName
	 * @param string $validation
	 */
	function selectDocumentVersion($params)
	{
		$form = $this->form;

		//Get list of indice
		$factory = $this->factory;
		$dao = new DocIndices($factory->getConnexion());
		if(empty($params['value'])) $params['value'] = 1;
		$stmt = $dao->getIndices();
		while($current=$stmt->fetch()){
			$list[$current['id']]=$current['name'];
		}
		$this->select($list, $params, $form);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectUser($params, $validation = 'client')
	{
		$form = $this->form;

		$factory = $this->factory;
		$list = $factory->getList(User::$classId);
		$list->load("1=1 LIMIT 100");

		$selectSet = array();
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;
		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['id']] = $item['login'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['login']] = $item['login'];
			}
		}
		$this->select($selectSet , $params , $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectProcess($params, $validation = 'client')
	{
		$form = $this->form;

		$factory = $this->factory;
		$list = $factory->getList(Wf\Process::$classId);
		$processList = $list->load("1=1 LIMIT 100");

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;

		if(!$params['returnName']){
			foreach($processList as $process){
				$name = $process['name'].'_'.$process['version'];
				$processSelectSet[$process['pId']] = $name;
			}
		}
		else{
			foreach($processList as $process){
				$name = $process['name'].'_'.$process['version'];
				$processSelectSet[$name] = $name;
			}
		}

		$this->select($processSelectSet , $params , $form , $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectProject($params, $validation = 'client')
	{
		$form = $this->form;

		//Construct array for set select project options
		$factory = $this->factory;
		$list = $factory->getList(Project::$classId);
		$list->load("1=1 LIMIT 100");

		$selectSet = array();
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;

		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['project_number']] = $item['project_number'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['project_id']] = $item['project_number'];
			}
		}
		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param container $container
	 * @param string $validation
	 */
	function selectCategory($params, $validation = 'client')
	{
		$form = $this->form;
		$factory = $this->factory;

		(isset($params['containerId'])) ? $containerId=$params['containerId'] : $containerId=null;
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;

		if(!$containerId){
			$select = array(
				'category_id AS id',
				'category_number AS name'
			);
			$list = $factory->getList(Category::$classId);
			$list->select($select);
			$list->load("1=1 LIMIT 100");
		}
		else{
			$select = array(
				'child.category_id AS id',
				'child.category_number AS name'
			);
			$lnkDao = $factory->getDao(\Rbs\Org\Container\Link\Category::$classId);
			$list = $lnkDao->getChildren($containerId, $select)->fetchAll();
		}

		$selectSet = array();
		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['name']] = $item['name'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['id']] = $item['name'];
			}
		}
		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param unknown_type $spaceName
	 * @param string $validation
	 */
	function selectContainer($params, $validation = 'client')
	{
		$form = $this->form;

		//Construct array for set select project options
		$factory = $this->factory;
		$spaceName = strtolower($factory->getName());
		$list = $factory->getList(Workitem::$classId);
		$list->select(array(
			$spaceName.'_number AS uid',
			$spaceName.'_id AS id'
		));
		$list->load("1=1 LIMIT 500");

		$selectSet = array();
		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;
		if($returnName){
			foreach( $list as $item ){
				$selectSet[$item['uid']] = $item['uid'];
			}
		}
		else{
			foreach( $list as $item ){
				$selectSet[$item['id']] = $item['uid'];
			}
		}

		$this->select($selectSet, $params, $form, $validation);
	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param propset $propset
	 * @param string $validation
	 */
	function selectPropset($params, propset $propset, $validation = 'client')
	{
		$form = $this->form;

		//$list array
		//$params array
		//$manager is the object from to get the method GetIndices
		/*
		 $params = array(
		 	'where',
		 	'name',
		 	'label'
		 	'value',
		 	'multiple',
		 	'returnName',
		 	'required',
		 	'size',
		 	'sortBy' => '',
		 	'sortOrder' => '',
		 	*/

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;
		//Get list of categories
		$list = $propset->GetPropset();

		if(!$params['returnName']){
			foreach ($list as $value ){ //Rewrite result array for quickform convenance
				$select[$value['propset_id']] = $value['propset_name'];
			}
		}
		else{
			foreach ($list as $value ){ //Rewrite result array for quickform convenance
				$select[$value['propset_name']] = $value['propset_name'];
			}
		}
		$this->select($select , $params , $form , $validation);

	}

	/**
	 *
	 * @param array $params
	 * @param Form $form
	 * @param metadata $property
	 * @param string $validation
	 */
	function selectProperty($params, metadata $property, $validation = 'client')
	{
		$form = $this->form;

		//$list array
		//$params array
		//$manager is the object from to get the method GetIndices
		/*
		 $params = array(
		 	'where',
		 	'name',
		 	'label'
		 	'value',
		 	'multiple',
		 	'returnName',
		 	'required',
		 	'size',
		 	'sortBy' => '',
		 	'sortOrder' => '',
		 	*/

		(isset($params['returnName'])) ? $returnName = $params['returnName'] : $returnName=false;
		//Get list of categories
		$p = array('select'=>array('property_id','label') );
		$list = $property->GetMetadata($p);

		if(!$params['returnName']){
			foreach ($list as $value ){ //Rewrite result array for quickform convenance
				$select[$value['property_id']] = $value['label'];
			}
		}
		else{
			foreach ($list as $value ){ //Rewrite result array for quickform convenance
				$select[$value['label']] = $value['label'];
			}
		}

		$this->select($select , $params , $form , $validation);

	}

	/**
	 $params = array(
	 ["name"]=>string
	 ["label"]=>string
	 ["required"]=>string
	 ["value"]=>string
	 ["date_format"]=>string / default 'dMY'
	 ["date_language"]=>string / default 'fr'
	 *
	 *
	 * @param array $params
	 * @param Form $form
	 * @param string $validation
	 */
	function selectDate($params, $validation = 'client')
	{
		$form = $this->form;

		if(!isset($params['label'])){
			$params['label'] = $params['name'];
		}

		$attributes = array('size'=>$params['size'],'class'=>'datepicker form-control');
		$calendar = $form->addElement('text', $params['name'], tra($params['label']), $attributes);

		if($params['required']){
			$form->addRule($params['name'], tra('is required'), 'required', null, $validation);
		}
	}

	/**
	 $field = array(
	 ["name"]=>string
	 ["appname"]=>string
	 ["label"]=>string
	 ["type"]=>string
	 ["regex"]=>string
	 ["required"]=>string
	 ["multiple"]=>string
	 ["size"]=>string
	 ["returnName"]=>string
	 ["list"]=>string
	 ["where"]=>string
	 ["regexMessage"]=>string
	 ["value"]=>string
	 ["sortOrder"]=>string
	 ["sortBy"]=>string
	 ["tableName"]=>string
	 ["fieldforValue"]=>string
	 ["fieldforDisplay"]=>string
	 ["dateFormat"]=>string
	 ["dateLanguage"]=>string / default 'fr'
	 ["displayBoth"]=>bool / default false
	 ["disabled"]=>string
	 ["position"]=>int
	 *
	 * @param array $field
	 * @param Form $form
	 * @param string $validation
	 */
	function element($property)
	{
		$form = $this->form;
		extract($property);

		(isset($name)) ? $sysName = $name : null;
		(isset($appName)) ? $name = $appName : null;

		switch ( $type ) {
			case 'text':
				if(!isset($size) || is_null($size)){
					$size = 20;
				}

				$form->addElement('text', $name , $label ,array('value'=>$value, 'size'=>$size , 'maxlength'=>$size, $disabled, 'id'=>$id, 'class'=>$class));
				$form->addRule($name , tra('should be less than or equal to') .' '. $size . ' characters', 'maxlength', $size , $validation);

				if(!empty($regex)){ //Add rule with regex
					if(empty($regexMessage)) {
						$regexMessage = tra('invalid format');
					}
					$form->addRule($name, $regexMessage, 'regex', '/'.$regex.'/', $validation);
				}

				if($required){ //Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				break;

			case 'long_text':
			case 'html_area':
				if(!isset($size)){
					$size = 20;
				}

				$form->addElement('textarea', $name , $label ,array('value'=>$value, 'cols'=>$size, $disabled, 'id'=>$id, 'class'=>$class) );
				if(!empty($regex)){
					if(empty($regexMessage)) $regexMessage = tra('invalid format');
				}

				$form->addRule($name, $regexMessage, 'regex', '/'.$regex.'/m', $validation);
				if($required){ //Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				break;

			case 'partner':
				$this->selectPartner($property);
				break;

			case 'doctype':
				$this->selectDoctype($property);
				break;

			case 'document_indice':
				$this->selectDocumentVersion($property);
				break;

			case 'container_indice':
				break;

			case 'user':
				$this->selectUser($property);
				break;

			case 'process':
				$this->selectProcess($property);
				break;

			case 'category':
				$this->selectCategory($property);
				break;

			case 'date':
				$this->selectDate($property);
				break;

			case 'integer':
			case 'decimal':
				if(!isset($size)){
					$size = 20;
				}
				$form->addElement('text', $name , $label ,array('value'=>$value, 'size'=>$size, $disabled, 'id'=>$id, 'class'=>$class));
				if($required){ //Add rule with require option
					$form->addRule($name, tra('is required'), 'required', null, $validation);
				}
				$form->addRule($name,tra('should be numeric'), 'numeric', null, $validation);
				break;

			case 'select':
				$list = explode('#' , $list);
				$list = array_combine($list , $list);
				$this->select($list , $property);
				break;

			case 'selectFromDB':
				$sortBy = "`$fieldforDisplay`";
				$select = implode(', ', array("`$fieldforValue` AS `value`" , "`$fieldforDisplay` AS `display`"));
				$connexion = \Rbplm\Dao\Connexion::get();
				$stmt = $connexion->query("SELECT $select FROM $dbtable ORDER BY $sortBy ASC LIMIT 1000");
				$list = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				//Construct array for set select options
				if(!$returnName){
					foreach ( $list as $key=>$values )
					{
						$selectSet[$values['display']] = $values['display'];
					}
				}
				else{
					foreach ( $list as $key=>$values )
					{
						$selectSet[$values['display']] = $values['display'];
					}
				}
				$this->select($selectSet , $field , $form);
				break;
			default:
		} //End of switch

		//Set the default value of the field from field['value
		// -- if its a multiple field, explode the string value into array
		if($multiple && isset($value)){
			$value = explode('#' , $value);
		}
	}
}

