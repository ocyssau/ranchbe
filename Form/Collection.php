<?php
namespace Form;

class Collection extends \Form\AbstractForm
{

	protected $_subforms = array();

	/**
	 *
	 * @param unknown_type $formName
	 * @param unknown_type $method
	 * @param unknown_type $action
	 * @param unknown_type $target
	 * @param unknown_type $attributes
	 * @param unknown_type $trackSubmit
	 */
	public function __construct($formName='', $method='post', $renderer=null)
	{
		$this->defaultRenderer = $renderer;
		parent::HTML_QuickForm($formName, $method, $action, $target, $attributes, $trackSubmit);
	}

	/**
	 * Returns a reference to default renderer object
	 * Dont use GLOBAL var as in HTML QUICK FORM
	 */
	function defaultRenderer()
	{
		if(!$this->defaultRenderer){
			$this->defaultRenderer = new \Form\Renderer\CollectionRenderer();
		}
		return $this->defaultRenderer;
	} // end func defaultRenderer


	/**
	 * Add a subform to this collection
	 *
	 * @param $subform
	 */
	public function add($subform)
	{
		if (is_object($subform) && is_subclass_of($subform, 'HTML_QuickForm')) {
			$this->_subforms[] = $subform;
		}
		return $this;
	}

	/**
	 * Accepts a renderer
	 *
	 * @param HTML_QuickForm_Renderer     An HTML_QuickForm_Renderer object
	 * @return void
	 */
	function accept(\HTML_QuickForm_Renderer $renderer)
	{
		$renderer->startForm($this);
		foreach($this->_subforms as $subform){
			$renderer->_html .= $subform->toHtml();
		}

		foreach (array_keys($this->_elements) as $key) {
			$element = $this->_elements[$key];
			$elementName = $element->getName();
			$required = ($this->isElementRequired($elementName) && !$element->isFrozen());
			$error = $this->getElementError($elementName);
			$element->accept($renderer, $required, $error);
		}

		$renderer->finishForm($this);
	}

	/**
	 * freeze all subform
	 *
	 */
	function freeze($elementList=null)
	{
		foreach($this->_subforms as $subform){
			$subform->freeze($elementList);
		}
		parent::freeze($elementList);
		return $this;
	}

	/**
	 *
	 * @return multitype:
	 */
	function getSubform()
	{
		return $this->_subforms;
	}

}
