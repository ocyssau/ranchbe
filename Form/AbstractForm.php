<?php
namespace Form;

require_once 'HTML/QuickForm.php'; //Librairy to easily create forms

class AbstractForm extends \HTML_QuickForm
{

	/**
	 * @var Smarty
	 */
	public $view;

	/**
	 *
	 * @var array
	 */
	public $extended = array();

	/**
	 *
	 * @var HTML_QuickForm_Renderer_Default
	 */
	protected $defaultRenderer;

	/**
	 *
	 */
	function __construct($name, $view, $factory, $renderer=null)
	{
		$this->defaultRenderer = $renderer;
		\HTML_QuickForm::__construct($name, $method='post', $action='#', $target=null, $attributes=null, $trackSubmit=null);

		$this->view = $view;
		$this->_tpl = $view;
		$this->factory = $factory;
		$this->elemtFactory = new \Form\ElementFactory($this, $factory);
	}

	/**
	 * Returns a reference to default renderer object
	 * Dont use GLOBAL var as in HTML QUICK FORM
	 */
	function defaultRenderer()
	{
		if(!$this->defaultRenderer){
			$this->defaultRenderer = new \Form\Renderer\ArraySmarty($this->view);
		}
		return $this->defaultRenderer;
	}

	/**
	 * Attach a model object to this form
	 *
	 * @param Any
	 * @return AbstractForm
	 */
	public function bind($object)
	{
		$this->binded = $object;
		$this->setDefaults($object->getArrayCopy());
		return $this;
	}

	/**
	 * Set the values of the form
	 *
	 * @param array
	 * @return AbstractForm
	 */
	public function setData($array)
	{
		$this->setDefaults($array);
		return $this;
	}

	/**
	 * Set the values of the form
	 *
	 * @param array
	 * @return AbstractForm
	 */
	public function validate()
	{
		$this->applyFilter('__ALL__', 'trim');
		$values = $this->exportValues();
		$this->binded->hydrate($values);

		if($this->extended){
			foreach($this->extended as $asApp){
				$this->binded->$asApp = $values[$asApp];
			}
		}

		return parent::validate();
	}

	/**
	 * Set a view to Form. If $view is null, construct a new view object.
	 * @param array $values
	 * @return AbstractForm
	 */
	public function setView($view=null)
	{
		if($view){
			$this->view = $view;
		}
		else{
			$this->view = rbinit_viewFactory();
		}
		return $this;
	}

	/**
	 * @param string template var name to set in template
	 * @return AbstractForm
	 */
	public function prepareRenderer($templateVarName='form')
	{
		$view = $this->view;

		/* Set the renderer for display QuickForm form in a smarty template */
		$this->renderer = $this->defaultRenderer();
		$this->accept($this->renderer);
		$view->assign($templateVarName, $this->renderer->toArray());

		return $this;
	}

	/**
	 *
	 */
	public function getElemtFactory($factory=null)
	{
		if(!isset($this->elemtFactory)){
			$this->elemtFactory = new ElementFactory($this, $factory);
		}
		return $this->elemtFactory;
	}

	/**
	 * @param array $extended In App sementic
	 */
	public function setExtended(array $extended)
	{
		foreach($extended as $attributes){
			$this->elemtFactory->element($attributes);
			$this->extended[$attributes['name']] = $attributes['appName'];
		}
		$this->view->extended = $extended;
	}

}
