<?php
namespace Form\Message;

class ComposeForm extends \Form\AbstractForm
{

	/**
	 *
	 */
	public function __construct($view, $factory=null)
	{
		parent::__construct('composeForm', $view, $factory);
		$this->template = 'message/compose/index.tpl';
		$this->setAttribute('class', 'form-inline');
		$this->daoFactory = $factory;

		/* Add submit button */
		$this->addElement('submit', 'cancel', 'Cancel', array('class'=>'btn btn-default'));
		$this->addElement('submit', 'validate', 'Send', array('class'=>'btn btn-success'));

		//TO
		$this->addElement('text', 'to', tra('To'), array('class'=>'form-control'));
		//CC
		$this->addElement('text', 'cc', tra('Cc'), array('class'=>'form-control'));
		//BCC
		$this->addElement('text', 'bcc', tra('Bcc'), array('class'=>'form-control'));
		//SUBJECT
		$this->addElement('text', 'subject', tra('Subject'), array('class'=>'form-control'));
		//BODY
		$this->addElement('textarea', 'body', '', array('class'=>'form-control', 'rows'=>20, 'cols'=>118));
		//PRIORITY
		$selectSet = array(
			1=>'1 - Very Low',
			2=>'2 - Low',
			3=>'3 - Normal',
			4=>'4 - High',
			5=>'5 - Very High'
		);
		$select = $this->addElement('select', 'priority', tra('Priority'), $selectSet, array('class'=>'form-control'));
		$select->setMultiple(false);
		$select->setSize(1);

		$this->addElement('advcheckbox', 'toall', '', tra('To All Ranchbe Users'), array('class'=>'form-control'));
		$this->addElement('advcheckbox', 'bymail', '', tra('By Mail'), array('class'=>'form-control'));
		$this->addElement('advcheckbox', 'bymessage', '', tra('By Ranchbe Internal Message'), array('class'=>'form-control'));

		/* Add validation rules to check input data */
		$this->addRule('subject', tra('Subject is required'), 'required');
		$this->addRule('to', tra('To is required'), 'required');
	}

	/**
	 * Attach a model object to this form
	 *
	 * @param Any
	 * @return AbstractForm
	 */
	public function setDefaults($datas)
	{
		if(is_array($datas['to'])){
			$datas['to'] = implode(' ', $datas['to']);
		}
		if(is_array($datas['from'])){
			$datas['from'] = implode(' ', $datas['from']);
		}
		if(is_array($datas['cc'])){
			$datas['cc'] = implode(' ', $datas['cc']);
		}
		if(is_array($datas['bcc'])){
			$datas['bcc'] = implode(' ', $datas['bcc']);
		}

		return parent::setDefaults($datas);
	}

	/**
	 * Set the values of the form
	 *
	 * @param array
	 * @return AbstractForm
	 */
	public function validate()
	{
		$this->applyFilter('__ALL__', 'trim');
		$values = $this->exportValues();

		/* Parse the to, cc and bcc fields into an array */
		if($values['to']){
			$to = preg_split( '/\s*(,|\s)\s*/', $values['to'] );
			$values['to'] = $to;
		}
		if($values['cc']){
			$cc = preg_split( '/\s*(,|\s)\s*/', $values['cc'] );
			$values['cc'] = $cc;
		}
		if($values['bcc']){
			$bcc = preg_split( '/\s*(,|\s)\s*/', $values['bcc'] );
			$values['bcc'] = $bcc;
		}

		$this->binded->hydrate($values);
		return \HTML_QuickForm::validate();
	}
}
