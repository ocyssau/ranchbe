<?php
namespace Form\Message;

use Rbplm\Dao\Pg\Filter;

use Rbs\Dao\Sier\Filter as DaoFilter;
use Rbplm\Dao\Filter\Op;

class FilterForm extends \Form\Filter\AbstractFilterForm
{
	/**
	 * @param project $project
	 * @param Smarty $view
	 * @param string $nameSpace
	 */
	public function __construct($view, $nameSpace)
	{
		$this->view = $view;
		$this->nameSpace = $nameSpace;
		$this->template = 'message/mailbox/filter.tpl';

		$selectSet = array (
			0=>'all',
			1,2,3,4,5
		);
		$select = $this->addElement('select', 'find_mailprio', tra('Priority'), $selectSet, array('id'=>'find_mailprio'));

		$selectSet = array (
			0=>'all',
			'isRead'=>'Is Read',
			'isFlagged'=>'Is Flagged',
			'isNotRead'=>'Is Not Read',
			'isNotFlagged'=>'Is Not Flagged',
		);
		$select = $this->addElement('select', 'find_flags', tra('Flag'), $selectSet, array('id'=>'find_flags'));

		//Find in field ----------------------------------------------
		$this->addElement( 'text', 'find', 'find', array('size'=>16) );
	}

	/**
	 * (non-PHPdoc)
	 * @see Form.AbstractForm::bind()
	 */
	public function bind(DaoFilter $filter)
	{
		if($_REQUEST['resetf']){
			return $this->reset($filter);
		}

		$defaults1 = $this->_bind($_SESSION['filter'][$this->nameSpace]);
		$defaults2 = $this->_bind($_REQUEST);

		$defaults = array_merge($defaults1, $defaults2);
		$this->values = $defaults;
		$this->setDefaults($defaults);

		//FIND
		if($defaults['find']){
		    $filter->andFind($defaults['find'], 'body', Op::CONTAINS);
		}

		if($defaults['find_mailprio']){
			$filter->andFind($defaults['find_mailprio'], 'priority', Op::CONTAINS);
		}

		if($defaults['find_flags']){
			$filter->andFind($defaults['find_flags'], 'flag', Op::CONTAINS);
		}

		return $this;
	}

}
