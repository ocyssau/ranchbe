<?php
namespace Form;

class Sub extends \Form\AbstractForm
{
	/**
	 * Returns a reference to default renderer object
	 * Dont use GLOBAL var as in HTML QUICK FORM
	 */
	function defaultRenderer()
	{
		if(!$this->defaultRenderer){
			$this->defaultRenderer = new \Form\Renderer\SubformRenderer();
		}
		return $this->defaultRenderer;
	} // end func defaultRenderer

	/**
	 * @param array $extended In App sementic
	 */
	public function setExtended(array $extended)
	{
		$loopId = $this->loopId;
		foreach($extended as $attributes){
			$attributes['appName'] = $attributes['appName']."[$loopId]";
			$attributes['class'] = 'form-control extended '.$attributes['appName'];
			$this->elemtFactory->element($attributes);
			$this->extended[$attributes['name']] = $attributes['appName'];
		}
		$this->view->extended = $extended;
	}

} //End of class
