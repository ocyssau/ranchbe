<?php

class Container_Archiver{
	
	/**
	 * 
	 * @var container
	 */
	protected $_container;
	
	/**
	 * 
	 * Constructor
	 * @param document $document
	 */
	public function __construct( container $container ){
		$this->_container = $container;
	}
	
	/**
	 * 
	 * Run archiving on document.
	 * All document of container are archived.
	 * 
	 */
	public function archive(){
		$filter = array( 
					'select'=>array('document_id'),
					'where'=>array('document_access_code < 100'),
		);
		$documentList = $this->_container->GetAllDocuments($filter, true);
		$ok = true;
		foreach($documentList as $infos){
			$Document = $this->_container->initDoc( $infos['document_id'] );
			$DocArchiver = new Document_Archiver($Document);
			if( !$DocArchiver->archive() ){
				$ok = false;
			}
		}
		
		if($ok){
			$type = $this->_container->SPACE_NAME;
			$this->_container->Update( array(
				$type . '_state'=>'Archived',
				'access_code'=>100,
				'close_date'=>time(),
			));
			return true;
		}else{
			return false;
		}
		
	}
	
}
