<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+


require_once('class/common/rdirectory.php');

class Rb_Import_Log
{
	protected $_file = null;
	protected $_path = null;
	protected $_fileHandle = null;
	
	function __construct($file)
	{
		$this->_file = $file;
		$path = dirname($this->_file);
		if( !is_dir($path) ){
			rdirectory::createdir($path);
		}
		$this->_fileHandle = fopen($this->_file, 'w');
	} //End of construct
	
	/**
	 * @param string
	 */
	function log($str)
	{
		fwrite ($this->_fileHandle, $str . PHP_EOL);
	}//End of method
	
	/**
	 * @param string
	 */
	function getFile()
	{
		return $this->_file;
	}//End of method
	
	function __destruct(){
		fclose($this->_fileHandle);
	}

}//End of class

