<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class serialize{

	//--------------------------------------------------------------------
	function __construct(){
		$this->errorStack = Ranchbe::getErrorStack();
	}//End of method

	function serializeObject(&$object, $return_object = false){
		$class = get_class($object);
		switch($class){
			case('document'):
				return $this->serializeDocument($object, $return_object);
				break;
			case('recordfile'):
				return $this->serializeRecordfile($object, $return_object);
				break;
			case('docfile'):
				return $this->serializeDocfile($object, $return_object);
				break;
		}
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Serialized data in xml file
	 * if $return_object = true return a xmlWriter object else return string
	 */
	function serializeDocument(document &$document, $return_object = false){

		$docInfos = $document->GetDocumentInfos();
		require_once('./class/common/metadata.php');
		$metadata = new docmetadata($document->space);
		$optionalFields = $metadata->GetMetadataLinked( NULL, $document->GetProperty('container_id') );

		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0', 'ISO-8859-1');
		$xml->setIndent(true);
		$xml->startElement ('document');

		$xml->startElement('properties');
		$xml->writeElement('document_id' , $docInfos['document_id']);
		$xml->writeElement('document_number' , $docInfos['document_number']);
		$xml->writeElement('designation' , $docInfos['designation']);
		$xml->writeElement('document_state' , $docInfos['document_state']);
		$xml->writeElement('document_access_code' , $docInfos['document_access_code']);
		$xml->writeElement('document_version' , $docInfos['document_version']);
		$xml->writeElement($document->GetFieldName('id') , $docInfos[$document->GetFieldName('id')]);
		$xml->writeElement('document_indice_id' , $docInfos['document_indice_id']);
		$xml->writeElement('doctype_id' , $docInfos['doctype_id']);
		$xml->writeElement('category_id' , $docInfos['category_id']);
		$xml->writeElement('check_out_date' , $docInfos['check_out_date']);
		$xml->writeElement('check_out_by' , $docInfos['check_out_by']);
		$xml->writeElement('update_date' , $docInfos['update_date']);
		$xml->writeElement('update_by' , $docInfos['update_by']);
		$xml->writeElement('open_date' , $docInfos['open_date']);
		$xml->writeElement('open_by' , $docInfos['open_by']);
		$xml->writeElement('from_document_id' , $docInfos['from_document_id']);
		$xml->startElement('extends_properties');
		foreach( $optionalFields as $optionalField ){
			$xml->writeElement($optionalField['field_name'] , $docInfos[ $optionalField['field_name'] ]);
		}
		$xml->endElement();
		$xml->endElement();

		$xml->startElement('extends_properties_def');
		if( is_array($optionalFields) )
		foreach( $optionalFields as $optionalField ){
			$xml->startElement('property_def');
			foreach( $optionalField as $key=>$value ){
				$xml->writeElement($key , $value);
			}
			$xml->endElement();
		}
		$xml->endElement();


		$xml->startElement('doctype');
		$doctype = $document->initDoctype($docInfos['doctype_id']);
		$doctype_infos = $doctype->GetInfos( $docInfos['doctype_id'] );
		foreach( $doctype_infos as $key=>$value ){
			$xml->writeElement($key , $value);
		}
		$xml->endElement();


		$xml->startElement('category');
		if( !empty($docInfos['category_id']) ){
			require_once('class/category.php');
			$category = new category($document->space, $docInfos['category_id'] );
			$category_infos = $category->GetCategoryInfos();
			if( is_array($category_infos) )
			foreach( $category_infos as $key=>$value ){
				$xml->writeElement($key , $value);
			}
		}
		$xml->endElement();

		$xml->writeElement('default_process_id' , $docInfos['default_process_id']);

		$userlib = Ranchbe::getUserLib();

		$xml->startElement('check_out_by');
		$xml->writeElement('user_id' , $docInfos['check_out_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($docInfos['check_out_by']) );
		$xml->endElement();

		$xml->startElement('update_by');
		$xml->writeElement('user_id' , $docInfos['update_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($docInfos['update_by']) );
		$xml->endElement();

		$xml->startElement('open_by');
		$xml->writeElement('user_id' , $docInfos['open_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($docInfos['open_by']) );
		$xml->endElement();

		$xml->startElement('from_document');
		$xml->writeElement('from_document_id' , $docInfos['from_document_id']);
		$from_document = new document($document->space, $docInfos['from_document_id']);
		$xml->writeElement('document_number' , $from_document->GetProperty('document_number') );
		$xml->writeElement('designation' , $from_document->GetProperty('designation') );
		$xml->endElement();

		//Doclinks
		$xml->startElement('doclinks');
		require_once('class/common/doclink.php');
		$doclink = new doclink( $document );
		$sons = $doclink->GetSons();
		foreach( $sons as $son_info ){
			$xml->startElement('son');
			foreach( $son_info as $key=>$value ){
				$xml->writeElement($key , $value);
			}
			$xml->endElement();
		}
		$xml->endElement();


		//Docfiles
		$xml->startElement('docfiles');
		$docfiles = $document->GetDocfiles();
		if( is_array($docfiles) )
		foreach( $docfiles as $docfile ){
			$xml->startElement('docfile');
			$docfile_infos = $docfile->GetInfos();
			foreach( $docfile_infos as $key=>$value ){
				$xml->writeElement($key , $value);
			}
			$xml->endElement();
		}
		$xml->endElement();


		//container
		$xml->startElement('container');
		$container = $document->GetContainer();
		$container_infos = $container->GetInfos();
		if( is_array($container_infos) )
		foreach( $container_infos as $key=>$value ){
			$xml->writeElement($key , $value);
		}
		$xml->endElement();

		//visufiles
		$xml->startElement('attachments');
		$attachments = $document->GetAttachments();
		if( is_array($attachments) )
		foreach( $attachments as $key=>$value ){
			$xml->writeElement($key , $value);
		}
		$xml->endElement();

		if($return_object)
		return $xml;

		$xml->endElement();
		$xml->endDocument();

		return $xml->flush();

	} //End of method


	//----------------------------------------------------------
	/*!\brief
	 * Serialized data in xml file
	 * if $return_object = true return a xmlWriter object else return string
	 */
	function serializeRecordfile(recordfile &$recordfile, $return_object = false){

		$fileInfos = $recordfile->GetInfos();

		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0', 'ISO-8859-1');
		$xml->setIndent(true);
		$xml->startElement ('recordfile');

		$xml->startElement('properties');
		foreach( $fileInfos as $key=>$value ){
			$xml->writeElement($key , $value);
		}
		$xml->endElement();

		$userlib = Ranchbe::getUserLib();

		$xml->startElement('update_by');
		$xml->writeElement('user_id' , $fileInfos['update_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($fileInfos['update_by']) );
		$xml->endElement();

		$xml->startElement('open_by');
		$xml->writeElement('user_id' , $fileInfos['open_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($fileInfos['open_by']) );
		$xml->endElement();

		if($return_object)
		return $xml;

		$xml->endElement();
		$xml->endDocument();

		return $xml->flush();

	}//End of method


	//----------------------------------------------------------
	/*!\brief
	 * Serialized data in xml file
	 * if $return_object = true return a xmlWriter object else return string
	 */
	function serializeDocfile(docfile &$docfile , $return_object = false){

		$xml = $this->serializeRecordfile($docfile, true);

		$userlib = Ranchbe::getUserLib();

		$xml->startElement('check_out_by');
		$xml->writeElement('user_id' , $fileInfos['check_out_by']);
		$xml->writeElement('user_name' , $userlib->get_user_name($fileInfos['check_out_by']) );
		$xml->endElement();

		if($return_object)
		return $xml;

		$xml->endElement();
		$xml->endDocument();

		return $xml->flush();

	}//End of method

}//End of class

?>
