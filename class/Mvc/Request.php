<?php
namespace Mvc;

class Request
{
	protected $_request;
	protected $_post;
	protected $_query;
	
	public function __construct()
	{
		$this->_request = $_REQUEST;
		$this->_post = $_POST;
		$this->_query = $_GET;
	}
	
	/**
	 * 
	 */
	public function isPost()
	{
		return ($_SERVER['REQUEST_METHOD'] == 'POST') ? true : false;
	}
	
	/**
	 * 
	 */
	public function isGet()
	{
		return ($_SERVER['REQUEST_METHOD'] == 'GET') ? true : false;
	}
	
	/**
	 * 
	 */
	public function getParam($name=null)
	{
		if($name==null){
			return $this->_request;
		}
		isset($this->_request[$name]) ? $return = $this->_request[$name] : $return = null;
		return $return;
	}
	
	/**
	 *
	 */
	public function getPost($name=null)
	{
		if($name==null){
			return $this->_post;
		}
		isset($this->_post[$name]) ? $return = $this->_post[$name] : $return = null;
		return $return;
	}
	
	/**
	 *
	 */
	public function getQuery($name=null)
	{
		if($name==null){
			return $this->_query;
		}
		isset($this->_query[$name]) ? $return = $this->_query[$name] : $return = null;
		return $return;
	}
	
	/**
	 *
	 */
	public function fromQuery($name=null)
	{
		if($name==null){
			return $this->_request;
		}
		isset($this->_request[$name]) ? $return = $this->_request[$name] : $return = null;
		return $return;
	}
	
	
}

