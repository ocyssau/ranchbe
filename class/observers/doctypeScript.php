<?php
require_once('class/observers/script_caller.php');

//Execute doctype scripts
//To associate a scripts on doctype save script in directory DEFAULT_DOCTYPE_SCRIPTS_DIR (see conf) and edit doctype to associate it this script.
//Adjust access right to scripts file : chown [ranchbe_user] [script file]; chmod 500 [script file];
//The script must defined function doctypeScript_[envent name]. This function must return a value TRUE or FALSE.
class doctypeScript extends script_caller{

	protected $document; //(object document)
	public $error_stack; //(object error_stack)

	function __construct(document &$document){
		$this->errorStack = Ranchbe::getErrorStack();
		$this->document = $document; //pass by reference
		$this->args = array(&$document); //pass by reference
		$this->scriptsDir = DEFAULT_DOCTYPE_SCRIPTS_DIR;
		$this->eventName = NULL;
		$this->scriptFile = NULL;
		$this->functionName = NULL;
		$this->className = NULL;
	}//End of method

	function notify($event, &$message){
		$this->eventName = $event;
		$doctype = $this->document->GetDoctype();
		if(!is_a($doctype, 'doctype') ) {
			$this->errorStack->push(LOG, 'Warning', array(), 'doctype is not set' );
			return false;
		}
		switch($event){
			case('doc_pre_update'):
				$script = $doctype->GetProperty('script_pre_update');
				break;

			case('doc_post_update'):
				$script = $doctype->GetProperty('script_post_update');
				break;

			case('doc_pre_store'):
				$script = $doctype->GetProperty('script_pre_store');
				break;

			case('doc_post_store'):
				$script = $doctype->GetProperty('script_post_store');
				break;

			default:
				$script = '';
				return true;
				break;
		}
		$this->eventName = $event;
		$this->scriptFile = $this->scriptsDir.'/'.$script;
		$this->functionName = $this->eventName;
		$this->className = 'doctypeScript_'.substr($script, 0, strrpos($script, '.'));
		if (!empty($script)){
			$ok = $this->execute();
			if( !is_bool($ok) ){
				$this->errorStack->push(ERROR, 'Warning', array('element1'=>$this->document->GetProperty('document_number'), 'element2'=>$this->scriptFile, 'element3'=>$this->functionName),
        'Document %element1% : Script %element2% failed or have none return. Add "return true" at function end. Check that function %element3% is correctly defined in script file %element2%.');
				return false;
			}
			else if($ok === false){
				return false;
			}
		}
		return true;
	} //End of method

} //End of class

?>
