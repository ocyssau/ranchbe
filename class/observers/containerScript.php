<?php
require_once('class/observers/script_caller.php');

//Execute evenement scripts
//To associate a scripts on evenement just create a scripts in the events_scripts dir of the deposit_dir of container
//with name containerScript_[event name].php and adjust access right to this file : chown [ranchbe_user] [script file]; chmod 500 [script file];
class containerScript extends script_caller{

	protected $document; //(object document)

	function __construct(document &$document){
		$this->errorStack = Ranchbe::getErrorStack();
		$this->document = $document; //pass by reference
		$this->args = array(&$document); //pass by reference
		$container = $this->document->GetContainer();
		$this->scriptsDir = $container->GetProperty('default_file_path').'/__events_scripts';
		$this->eventName = NULL;
		$this->scriptFile = NULL;
		$this->functionName = NULL;
		$this->className = NULL;
	}//End of method

	function notify($event, &$message){
		$container = $this->document->GetContainer();
		$this->eventName = $event;
		$this->className = 'containerScript_'.$container->GetProperty('container_id');
		$this->scriptFile = $this->scriptsDir.'/'.$this->className.'.php';
		$this->functionName = $this->eventName;

		if( !$this->execute() ){
			$this->errorStack->push(ERROR, 'Warning', array('element1'=>$this->document->GetProperty('document_number'), 'element2'=>$this->scriptFile),
      'Document %element1% : Script %element2% failed or have none return. Add "return true" at function end');
			return false;
		}
		return true;
	} //End of method

} //End of class
