<?php
require_once('./class/common/observer.php');
abstract class progressObserver extends rb_observer{

  function __construct(HTML_Progress2 &$progressBar){
    $this->_progressBar = $progressBar;
  }

  function notify($event, &$obj){
    $this->_eventName = $event;
    $this->_object = $obj;

    switch($this->_eventName){
      case('doc_pre_update'):
        break;

      default:
        return true;
        break;
    }
    

    return $this->_eventName;
  } //End of method

} //End of class

?>
