<?php

class Rb_Workflow_Activity extends BaseActivity 
{

	/**
	 * 
	 * @param workflow $workflow
	 * @param array $document_ids		parameters to send to view script
	 */
	function display(workflow $workflow, $params = array() )
	{
		if(!$this->activityId){
			trigger_error('Unknown activity id', E_USER_WARNING);
		}
		
		//set references to object to use in activities scripts
		$activity = $this;

		//set var to use in activities scripts
		$process_id = $this->getProcessId();
		if( $workflow->getProcess() ){
			$process = $workflow->getProcess();
		}
		else{
			$workflow->initProcess($process_id);
			$process = $workflow->getProcess();
		}
		$template = $process->getNormalizedName(). '/' .$this->getNormalizedName(). '.tpl';
		
		// load then the compiled version of the activity
		//compileActivity is defined in galaxia_setup.php
		$codes = compileActivity($process, $activity); //recompile activity if necessary and return path to scripts
		if( is_file($codes['view']) ){
			if( !include($codes['view']) ){
				trigger_error('Unknown view script :'.$codes['view'], E_USER_WARNING);
				//return $this;
			}
		}
		Ranchbe::getView()->display( $template );

		return $this;
	}


	/** Factory method returning an activity of the desired type
	 *  loading the information from the database.
	 * 
	 * @param integer $activityId
	 */
	function getActivity($activityId)
	{
		$query = "SELECT * FROM `".GALAXIA_TABLE_PREFIX."activities` WHERE `activityId`=?";
		$result = $this->query($query, array($activityId));
		if(!$result->numRows()) return false;
		$res = $result->fetchRow();
		switch($res['type']) {
			case 'start':
				$act = new Rb_Workflow_Activity_Start($this->db);
				break;
			case 'end':
				$act = new Rb_Workflow_Activity_End($this->db);
				break;
			case 'join':
				$act = new Rb_Workflow_Activity_Join($this->db);
				break;
			case 'split':
				$act = new Rb_Workflow_Activity_Split($this->db);
				break;
			case 'standalone':
				$act = new Rb_Workflow_Activity_Standalone($this->db);
				break;
			case 'switch':
				$act = new Rb_Workflow_Activity_SwitchActivity($this->db);
				break;
			case 'activity':
				$act = new Rb_Workflow_Activity_Activity($this->db);
				break;
			default:
				trigger_error('Unknown activity type:'.$res['type'], E_USER_WARNING);
		}

		$act->setName($res['name']);
		$act->setProcessId($res['pId']);
		$act->setNormalizedName($res['normalized_name']);
		$act->setDescription($res['description']);
		$act->setIsInteractive($res['isInteractive']);
		$act->setIsAutoRouted($res['isAutoRouted']);
		$act->setIsAutomatic($res['isAutomatic']);
		$act->setIsComment($res['isComment']);
		$act->setActivityId($res['activityId']);
		$act->setType($res['type']);

		//Now get forward transitions

		//Now get backward transitions

		//Now get roles
		$query = "SELECT `roleId` FROM `".GALAXIA_TABLE_PREFIX."activity_roles` WHERE `activityId`=?";
		$result=$this->query($query,array($res['activityId']));
		while($res = $result->fetchRow()) {
			$this->roles[] = $res['roleId'];
		}
		$act->setRoles($this->roles);
		return $act;
	}

}
