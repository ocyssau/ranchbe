<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

use Workflow\Process;

/**  This class manage workflow associate to objects.
 *
* workflow is an agrega of basics objects of the workflow : process, activity, instance
* is create a coherente system and defined many factory methods to use easly the galaxia engine
* How to use :
* $workflow = new workflow(); //First construct object
* now you can init a process or an instance. example :
* $docflow->initInstance($iid); //init an instance
* $docflow->initProcess(); //init the process. the arg is not require, the process id is get from the instance
*
* if you prefere init an process before :
* $docflow->initProcess();
* $docflow->initInstance($iid);
* $docflow->initProcess(); //recall initProcess to get $pid from instance
*
*/
class workflow
{

	protected $process; //(Galaxia/process Object) default process or current process of document
	protected $instance; //(Galaxia/instance Object) current instance running on document
	protected $usr; //(Object) current user
	protected $dbGalaxia; //(Object) adodb to access galaxia database
	protected $dbranchbe; //(Object) adodb to access ranchbe database
	protected $activities; //(array)

	public $instanceManager; //(Object)
	public $activityManager; //(Object)
	public $roleManager; //(Object)
	public $processManager; //(Object)

	public $error_stack; //(Object)
	public $logger; //(Object)
	public $template; //(Object)
	public $viewscript; //(String)

	//--------------------------------------------------------------------
	function __construct(){
		$this->usr = Ranchbe::getCurrentUser();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();
		$this->dbGalaxia = Ranchbe::getDb();
		$this->dbranchbe = Ranchbe::getDb();
	}

	//-------------------------------------------------------------------------
	/**  attach process
	 *  Return boolean
	*/
	function setProcess(process &$process){
		$this->process = $process;
		return true;
	}

	/**  attach instance
	 *  Return boolean
	*
	*/
	function setInstance(instance &$instance){
		$this->instance = $instance;
		return true;
	}

	/**
	 * attach process
	 *  Return object
	 *
	 */
	public function initProcess($processId = 0)
	{
		if( $processId == 0 ){
			if( is_object($this->instance) ){ //try to get the pid from instance
				$processId = $this->instance->getProcessId(); //if a process is linked to instance
			}
			else{
				if( is_object($this->activity) ){ //try to get the pid from activity
					$processId = $this->activity->getProcessId(); //if a process is linked to instance
						
				}
			}
		}
		
		if(!isset($this->process)){
			$this->process = new Process($this->dbGalaxia);
		}
		
		if($processId != 0){
			$this->process->getProcess($processId);
		}
		return $this->process;
	}

	//-------------------------------------------------------------------------
	/**  attach instance
	 *  Return object
	*
	*/
	function initInstance($instance_id=0)
	{
		if( !isset($this->instance) ){
			$this->instance = new \Workflow\Instance($this->dbGalaxia);
		}
		
		if($instance_id != 0){
			$this->instance->getInstance($instance_id);
		}
		
		return $this->instance;
	}

	/**
	 *  attach activity
	 *  Return object Activity or BaseActivity
	 *
	 */
	public function initActivity($activityId = 0)
	{
		if( !isset($this->activity) ){
			$this->activity = new \Workflow\Activity($this->dbGalaxia);
		}

		if($activityId != 0){
			$this->activity = $this->activity->getActivity($activityId);
		}

		return $this->activity;
	}

	/**
	 *
	 */
	function getProcess()
	{
		return $this->process;
	}

	/**
	 *  Return object
	 *
	 */
	function getInstance()
	{
		return $this->instance;
	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return object
	*
	*/
	function getActivity($activityId=0)
	{
		return $this->initActivity($activityId);
	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return object
	*
	*/
	function getUser()
	{
		return $this->usr;
	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return array
	*
	*/
	function getRunningActivity()
	{
		return $this->instance->RunningActivities;
	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return array
	*
	*/
	function getInstanceInfos()
	{
		if(!isset($this->instance)){
			return false;
		}
		return array(
				'properties' => &$this->instance->properties,
				'status' => &$this->instance->status,
				'pId' => &$this->instance->pId,
				'instanceId' => &$this->instance->instanceId,
				'owner' => &$this->instance->owner,
				'started' => &$this->instance->started,
				'name' => &$this->instance->name,
				'ended' => &$this->instance->ended,
				'nextActivity' => &$this->instance->nextActivity,
				'nextUser' => &$this->instance->nextUser
		);

	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return array
	*
	*/
	function getProcessInfos()
	{
		if(!isset($this->process)){
			return false;
		}
		return array(
				'name' => &$this->process->name,
				'description' => &$this->process->description,
				'normalized_name' => &$this->process->normalizedName,
				'version' => &$this->process->version,
				'pId' => &$this->process->pId
		);
	}

	//-------------------------------------------------------------------------
	/**  get
	 *  Return array with keys : standalone, activities, running
	*
	* bool @$force , true for reget activities from database
	*
	*/
	function getInstanceActivityInfo($force = false)
	{
		if($force = false)
			if(isset($this->activities) )
			return $this->activities;

		if(!isset($this->instance)) return false;

		$this->initManager('instanceManager');
		$this->initManager('activityManager');

		$standalone = $this->activityManager->listActivities($this->process->pId , 0, -1, 'flowNum_asc', '', "type = 'standalone'");
		$this->activities['standalone'] = $standalone['data'];
		$instance_id = $this->instance->getInstanceId();
		$process_id = $this->process->pId;
		if( $instance_id == 0 && $process_id != 0 ){
			$this->activities['activities'] = $this->activityManager->getActivities( $process_id );
		}else{
			$this->activities['activities'] = $this->instanceManager->get_instance_activities( $instance_id );
			$this->activities['running'] = $this->instanceManager->get_running_activity( $this->instance->getInstanceId() );
		}

		return $this->activities;
	}


	//-------------------------------------------------------------------------
	/*
	 function getActivityViewScript($activityId)
	 {
	if( $activityId == 0 ){
	$this->errorStack->push(ERROR, 'Error', array(), 'activity_id is NULL');
	return false;
	}

	$this->getActivityTemplate($activityId);
	if($this->template){
	$this->viewscript = $this->process->getNormalizedName(). '/' .$this->activity->getNormalizedName(). '.view.php';
	return $this->viewscript;
	}else{
	return $this->viewscript = false;
	}
	} //End of function
	*/

	//-------------------------------------------------------------------------
	/** Get the activity template
	 *
	 */
	public function getActivityTemplate($activityId)
	{
		if( $activityId == 0 ){
			$this->errorStack->push(ERROR, 'Error', array(), 'activity_id is NULL');
			return false;
		}

		$this->initActivity($activityId);
		$this->initProcess();
		
		if ( $this->activity->isInteractive() ) {
			$this->template = $this->process->getNormalizedName(). '/' .$this->activity->getNormalizedName(). '.tpl';
			return $this->template;
		}
		else{
			$this->logger->log( 'activity is not interactive or auto is set to true' );
			return $this->template = false;
		}
	} //End of function

	//-------------------------------------------------------------------------
	/**  execute the activity
	 *
	*/
	function execute($activityId, $iid, $name)
	{
		if( $activityId == 0 ){
			$this->errorStack->push(ERROR, 'Error', array(), 'activity_id is NULL');
			return false;
		}

		$this->logger->log('workflow::execute activityId: '.$activityId.' instanceId: '.$iid);

		$activity = $this->initActivity($activityId);

		// Only run a start activity if none instance is linked to document
		// and if access is = 0 to prevent reassign process on document wich is terminate
		if (!empty($iid) && $activity->getType() == 'start'){
			$msg = 'There is already an activity in progress on this item or is terminated';
			$this->errorStack->push(ERROR, 'Info', array(), $msg);
			return false;
		}

		// A start activity requires a name for the instance
		if ($activity->getType() == 'start') {
			$_REQUEST['name'] = $name; //$_REQUEST['name'] is necessary for galaxia scripts
		}

		// Get activity roles and check it
		$act_roles = $activity->getRoles();
		$user_roles = $activity->getUserRoles($this->usr->getLogin() );
		
		// Only check roles if this is an interactive activity
		if ($activity->isInteractive()) {
			if (!count(array_intersect($act_roles, $user_roles)) ) {
				$msg='You cant execute this activity';
				$this->errorStack->push(ERROR, 'Fatal', array(), $msg);
				return false;
			}
		}

		$this->instance->getInstance($iid);
		if( !$this->instance->executeActivity($activityId, $auto) ){
			return false;
		}

		//if is not a interactive activity complete it
		if ( !$activity->isInteractive() ) {
			$this->instance->complete($activityId, false, true);
		}

		$this->instance->sendMessageNextUsers();

		return $this->instance;

	}

	/**
	 * 
	 * @param unknown_type $process
	 * @param unknown_type $activity
	 * @return multitype:string
	 */
	public function compileActivity($process, $activity)
	{
		// Get paths for original and compiled activity code
		$origcode = GALAXIA_PROCESSES . '/'  . $process->getNormalizedName(). '/code/activities/' . $activity->getNormalizedName() . '.php';
		$compcode = GALAXIA_PROCESSES . '/'  . $process->getNormalizedName() . '/compiled/' . $activity->getNormalizedName(). '.php';
	
		// Now get paths for original and compiled template
		$origtmpl = GALAXIA_PROCESSES . '/'  . $process->getNormalizedName(). '/code/templates/'  . $activity->getNormalizedName() . '.tpl';
		$comptmpl = 'templates/' . $process->getNormalizedName() . '/' . $activity->getNormalizedName(). '.tpl';
	
		//Get path to view script
		$compviewscript = GALAXIA_PROCESSES . '/'  . $process->getNormalizedName() . '/code/views/' . $activity->getNormalizedName(). '.php';
	
		$recomplile = false; //var init to choose recompile or not
		//If activity is not interactive, they are not template file
		if($activity->isInteractive()){
			if (filemtime($origtmpl) > filemtime($comptmpl))
				$recomplile = true;
		}
		
		if ((filemtime($origcode) > filemtime($compcode))){
			$recomplile = true;
		}
	
		// Check whether the activity code or template are newer than their compiled counterparts,
		// i.e. check if the source code or template were changed; if so, we need to recompile
		if ($recomplile) {
			// Recompile the activity
			$activityManager = new \Workflow\Manager\Activity($this->dbGalaxia);
			$activityManager->compile($activity->getProcessId(), $activity->getActivityId());
		}
	
		// Get paths for shared code and activity
		$shared = GALAXIA_PROCESSES . '/'  . $process->getNormalizedName(). '/code/shared.php';
		$source = $compcode;
		
		return array('shared'=>$shared, 'source'=>$source, 'view'=>$compviewscript, 'template'=>$comptmpl);
	
	} //End of function
	

	//---------------------------
	/* Update instance comment after validation of post operations
	 */
	function validateActivityComment($activity_id, $title, $comment)
	{
		if( !isset($this->instance ) ){ //need instance object...
			$this->errorStack->push(ERROR, 'Error', array(), 'instance is not init');
			return false;
		}

		$logger->log( 'workflow::validateActivityComment : comment instance: '.
				$this->instance->getInstanceId().' activity id: '.$activity_id );

		//$__comments = $instance->get_instance_comments($activity->getActivityId()); //actual comment

		$activity = $this->initActivity($activityId);

		$instance->replace_instance_comment($cid, $activity->getActivityId(), $activity->getName(),
				$this->usr->getLogin(), $title, $comment);

		//Send comments to next user
		$CnextUsers = explode('%3B',$_REQUEST['CnextUsers']);
		$from = $user;
		$cc   = '';
		$subject = $title;
		$body =   'Process :'  . $process->getVersion() .'<br>'.
				'Activity :' . $activity->getName()   .'<br>'.
				'Subject :'  . $title   .'<br>'.
				'Comment :'  . $comment .'<br>'
				;
				$priority = 3;

				$messulib = new Messu($this->dbGalaxia);
				/*foreach( $CnextUsers as $to ){
				 $messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
				}*/

				return true;

	} //End of function

	//---------------------------
	/* reset the process
	 *
	* \param $instanceId (int) optionnel. if omit try to get instance_id from current instance
	*/
	function resetProcess( $instanceId = 0)
	{
		if( isset($this->instance ) && $instanceId == 0 ){ //need instance object...
			$instanceId = $this->instance->getInstanceId();
		}
		if( $instanceId == 0 ){
			$this->errorStack->push(ERROR, 'Error', array(), 'instance is not init');
			return false;
		}

		$this->logger->log( 'workflow::resetProces : instance id: '.$this->instance->getInstanceId() );

		$this->initManager('instanceManager');
		$this->instanceManager->set_instance_status($instanceId, 'aborted');
		return true;
	} //End of function

	/** 
	 * factory method to get manager objects :
	 *
	* bool @$force , true for reget activities from database
	*
	*/
	function initManager($manager)
	{
		switch($manager){
			case 'instanceManager':
				if(!isset($this->instanceManager)){
					$this->instanceManager = new \Workflow\Manager\Instance($this->dbGalaxia);
				}
				break;

			case 'activityManager':
				if(!isset($this->activityManager)){
					$this->activityManager = new \Workflow\Manager\Activity($this->dbGalaxia);
				}
				break;

			case 'roleManager':
				if(!isset($this->roleManager)){
					$this->roleManager = new \Workflow\Manager\Role($this->dbGalaxia);
				}
				break;

			case 'processManager':
				if(!isset($this->processManager)){
					$this->processManager = new \Workflow\Manager\Process($this->dbGalaxia);
				}
				break;

			default:
				return false;
				break;

		}
	}

	function errorNotify($message)
	{
		$this->errorStack->push(ERROR, 'Error', array(), $message);
	}

}//End of class
