<?php

// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief 
*
*/

require_once('./class/viewerDrivers/defaultViewer.inc.php');

class xmlPlayer extends defaultViewer {

  function __construct($Viewer){
    $this->Viewer = $Viewer;
    $this->Manager &= $Viewer->getProperty('Manager');
  }//End of method
       
  static function embededViewer($file){
    return ('
        <object type="application/x-3dxmlplugin" id="xmlplayer" width="500" height="300" style="MARGIN: 2px" border="0">
        <param name="DocumentFile" value="'.$file.'"">
        </object>
    ');
  }//End of method
} //End of class

?>
