<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief 
*
*/
class defaultViewer {

  function __construct($Viewer)
  {
  $this->Viewer = $Viewer;
  $this->Manager &= $Viewer->getProperty('Manager');
  }//End of method
       
  static function embededViewer($file)
  {
  }//End of method

  function ViewFile($file)
  {
    return $this->ViewRawFile($file);
  }//End of method

//-------------------------------------------------------

  /*! \brief View the document file.
  *
  * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
  * If the mimetype = no_read, display a error.
  * Return true or false.
    \param $file(string) path of the file to display on the client computer
  */
  function ViewRawFile($file) {
    $fileName = basename($file);
    if ($this->Viewer->noread == 'no_read') {
      $this->Manager->errorStack->push(ERROR, 'Fatal', array('element'=>$file), 'you cant view this file %element% by this way' );
      return false;}

    header("Content-disposition: attachment; filename=$fileName");
    header("Content-Type: " . $this->Viewer->mimetype);
    header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
    header("Content-Length: ".filesize($file));
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    readfile($file);
    die;
  }//End of method

} //End of class
?>
