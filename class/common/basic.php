<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

use Rbs\Dao\Sier\Filter as Filter;

require_once 'PEAR/ErrorStack.php';
require_once 'Log.php';
require_once 'class/common/observable.php';

/*! \brief Class for manage errors occuring in ranchbe objects.
 *
* This class extends the PEAR_ErrorStack class. PEAR_ErrorStack is the next generation
* of error manager for PHP and replace the PEAR_error package.
* See http://www.go-pear.org/manual/fr/core.pear.pear-errorstack.intro.php.<br>
*
* Errors are push in the stack by method error_stack->push(CODE(string), LEVEL(string), PARAMS(array), MSG(string) );<br>
* The CODE = ERROR for an common error, ERROR_DB for an database error.<br>
* LEVEL = Fatal, Warning, Info, Debug, Log<br>
* PARAM = is a array with optionnals keys<ul>
*    <li>element : for use in MSG</li>
*    <li>query : is the content of the query. To use for debug</li>
*    <li>debug : to records var content for debug</li></ul>
* MSG is the message to display to user. If a key element is record in PARAM, the text %element% will be replaced by his value record in param array.<br>
*
* For control display to user the constant DEBUG of the ranchbe_setup.php file is set to true for
* display all content of the error.
* If set to false, display only the MSG.
*/
class errorManager extends PEAR_ErrorStack {

	/*! \brief Check if errors occured and display user message.
	 *
	* This method display message to user if an error is in the stack.
	* @param $params(array)
	* @param $params['close_button'](bool), if true display a button to close the window
	* @param $params['back_button'](bool), if true display a button to back to previous page
	* @param $params['home_button'](bool), if true display a button to come back to home page
	*/
	function checkErrors($params=''){
		if ($this->hasErrors()){
			$smarty = Ranchbe::getView();
			$smarty->assign('debug', DEBUG);
			$errors = $this->getErrors();
			$smarty->assign('errors', $errors);
			if(isset($params['close_button'])){
				$smarty->assign('close_button', $params['close_button']);
			}
			if(isset($params['back_button'])){
				$smarty->assign('back_button', $params['back_button']);
			}
			if(isset($params['home_button'])){
				$smarty->assign('home_button', $params['home_button']);
			}
			$smarty->display('application/error/stack.tpl');
		}
	}//End of method

	//Overload of ErrorStack::_log method
	function _log($err)
	{
		if ($this->_logger) {
			$logger = &$this->_logger;
		} else {
			$logger = &$GLOBALS['_PEAR_ERRORSTACK_DEFAULT_LOGGER'];
		}
		if (is_a($logger, 'Log')) {
			$levels = array(
					'Exception' => PEAR_LOG_CRIT,
					'Critical' => PEAR_LOG_CRIT,
					'Fatal' => PEAR_LOG_ERR,
					'Error' => PEAR_LOG_ERR,
					'Alert' => PEAR_LOG_ALERT,
					'Warning' => PEAR_LOG_WARNING,
					'Info' => PEAR_LOG_INFO,
					'Notice' => PEAR_LOG_NOTICE,
					'Debug' => PEAR_LOG_DEBUG
			);
			if ( isset($err['level']) ) {
				$level = $levels[$err['level']];
			} else {
				$level = PEAR_LOG_INFO;
			}
			if(LOGLEVEL == 'Error' && strtolower($err['level']) == 'info'){
				return;
			}else{
				$logger->log('[level: '.$err['level']
						.'] [file: '.$err['context']['file']
						.'] [line: '.$err['context']['line']
						.'] [function: '.$err['context']['function']
						.'] [class: '.$err['context']['class'].'] '
						.$err['message'] , $level, $err);
			}
		} else { // support non-standard logs
			call_user_func($logger, $err);
		}
	}//End of method


}//End of class


class union{

	function GetAll($params){
		$params['getQuery'] = true;
		$leftQuery = $leftObject->GetAll();
		$rightQuery = $rightObject->GetAll();

		$query = 'SELECT DISTINCT '.$leftQuery['selectClose'].' FROM '.$leftQuery['from']
		.' '.$leftQuery['joinClose']
		.' '.$leftQuery['whereClose']
		.' UNION '
		.' SELECT '.$rightQuery['selectClose'].' FROM '.$rightQuery['from']
		.' '.$rightQuery['joinClose']
		.' '.$rightQuery['whereClose']
		.' '.$rightQuery['orderClose']
		;
	}

}//End of class



//----------------------------------------------------------------------

/*! \brief Basic class for manage database access.
 *
*  This class manage access to database. It define standard method to create, modify or suppress a single
*  or a group of records in database.
*  She define too standards methods to get records.
*/
abstract class basic{

	public $selectClose = ''; /*!< define the select close for query the database*/
	public $whereClose  = ''; /*!< define the where close for query the database*/
	public $joinClose  = '';
	public $orderClose  = ''; /*!< define the order for sort the result*/
	public $limit       = ''; /*!< limit the number of records in the result*/
	public $offset      = ''; /*!< defined the pagination page to display*/
	public $recordCount; //(Integer)
	public $history; //(Array) Data to record in the history
	public $errorStack; //PEAR_ErrorStack error manager
	public $space; //(Object)

	/*! \brief Standard method to create a record in database
	 *
	*   Return the object id if success, false else.
	*   This method use the transactions.
	*   @param $data(array), its an array where keys/values are fields/values of the record to create.
	*/
	function BasicCreate($data){
		//Set empty values to NULL
		foreach($data as $key=>$val){
			if(empty($val) && $val !== 0){
				$data[$key]=NULL;
			}
			else {
				$data[$key]=$val;
			}
		}

		$this->dbranchbe->StartTrans();
		$id = $this->dbranchbe->GenID($this->OBJECT_TABLE.'_seq', 1); //Increment the sequence number
		$data[$this->FIELDS_MAP_ID] = $id;

		if (!$this->dbranchbe->AutoExecute( $this->OBJECT_TABLE , $data , 'INSERT')){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE)), $this->dbranchbe->ErrorMsg());
			$this->dbranchbe->FailTrans();
		}

		if(!$this->dbranchbe->CompleteTrans()){
			return false;
		}

		if(is_object($this->logger)){
			$this->logger->log('new inserted entry id : '.$id.' in table :'.$this->OBJECT_TABLE);
		}

		return $id;
	}//End of method

	//----------------------------------------------------------------------
	/*! \brief Standard method to update a record in database.
	 *   Return true if success, false else.
	*
	*   This method dont use the transactions.
	*   @param $data(array), its an array where keys/values are fields/values of the record to update.
	*   @param $object_id, is the id of the record to update.
	*/
	function BasicUpdate($data , $object_id){
		//Set empty values to NULL
		foreach($data as $key=>$val){
			if(empty($val)){
				$data[$key]=NULL;
			}
			else{
				$data[$key]=$val;
			}
		}

		//Update the object data
		if (!$this->dbranchbe->AutoExecute( $this->OBJECT_TABLE , $data , 'UPDATE' , "$this->FIELDS_MAP_ID = '$object_id'")){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, "$this->FIELDS_MAP_ID LIKE $object_id", "$this->OBJECT_TABLE")), $this->dbranchbe->ErrorMsg());
			return false;
		}

		foreach($data as $key=>$val){
			$this->history[$key] = $val; //Keep data for history
		}

		if(is_object($this->logger)){
			$this->logger->log('updated record id : '.$object_id.' in table :'.$this->OBJECT_TABLE);
		}

		return true;
	}//End of method

	//----------------------------------------------------------------------
	/*! \brief Standard method to suppress a record in database
	 *
	*   Return true if success, false else.
	*   This method use the transactions.
	*   @param $object_id, is the id of the record to suppress.
	*/
	function BasicSuppress($id) {
		$query = 'DELETE FROM '.$this->OBJECT_TABLE.' WHERE '.$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID.' = \''.$id.'\'';
		//Execute the query
		if(!$this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}
		if(is_object($this->logger))
			$this->logger->log('suppressed record id : '.$id.' in table :'.$this->OBJECT_TABLE);
		return true;
	}//End of method

	/**
	 * @param array $params
	 */
	function load($id)
	{
		$query = 'SELECT * FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID." = '$id'";
		$properties = $this->dbranchbe->GetRow($query);
		if($properties === false){
			$this->errorStack->push( ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
			return false;
		}
		$this->loadFromArray($properties);
		return $infos;
	}//End of method

	/**
	 *
	 * @param array $properties
	 */
	public function loadFromArray($properties)
	{
		$this->core_prop = $properties;
		return $this;
	}
	
	//----------------------------------------------------------------------
	/*! \brief Standard method to get all or a filter list of records of a table.
	 *
	*   Return a array if success, false else.
	*   This method dont use the transactions.
	@param $params[offset](integer) offset sql option
	@param $params[maxRecords](integer) limit sql option
	@param $params[sort_field](string) field use for sort sql option
	@param $params[sort_order]('ASC' or 'DESC') sort direction sql option
	@param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
	This array is translate to a query option like 'where key=value'
	@param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
	This array is translate to a query option like 'WHERE key LIKE %value%'
	@param $params[select](array) its a nonassoc array with all fields to put in the sql select option
	*/
	protected function GetAllBasic($params)
	{
		if($params instanceof Filter){
			$this->whereClose = $params->whereToString();
			$this->selectClose = $params->selectToString();
			$this->joinClose = $params->withToString();
			$this->orderClose = $params->orderToString();
			$this->limit = $params->getLimit();
			$this->offset = $params->getOffset();
		}
		else{
			if ( isset($params['getQuery']) && $params['getQuery'] == true ){
				return $this->GetQueryOptionsArray( $params );
			}

			if( is_array( $this->unionObjects ) ) { //Be carefull to loops!
				return $this->GetAllUnion( $params );
			}

			$this->GetQueryOptions($params);
		}

		$query = 'SELECT '.$this->selectClose.' FROM '.$this->OBJECT_TABLE
		.' '.$this->joinClose
		.' '.$this->whereClose
		.' '.$this->orderClose
		.' '.$this->extra
		;

		$ADODB_COUNTRECS = false;
		if(!$rs = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}
		else{
			$All = $rs->GetArray(); //To transform result in array;
			foreach ($All as $key => $value){ //Probably no more in used...
				$All[$key]['object_class'] = $this->SPACE_NAME; //Add the type of object to each key
			}
			return $All;
		}
	}//End of method


	/**
	 *
	 * @param Filter $params
	 */
	public function getList(Filter $filter)
	{
		$this->whereClose = $filter->whereToString();
		$this->selectClose = $filter->selectToString();
		$this->joinClose = $filter->withToString($this->OBJECT_TABLE);
		$this->orderClose = $filter->orderToString();
		$this->limit = $filter->getLimit();
		$this->offset = $filter->getOffset();

		$query = 'SELECT '.$this->selectClose.' FROM '.$this->OBJECT_TABLE
		.' '.$this->joinClose
		.' WHERE'.$this->whereClose
		. $this->orderClose
		.' '.$this->extra
		;

		$ADODB_COUNTRECS = false;
		if(!$rs = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			var_dump($this->dbranchbe->ErrorMsg());die;
			return false;
		}
		else{
			$All = $rs->GetArray(); //To transform result in array;
			foreach ($All as $key => $value){ //Probably no more in used...
				$All[$key]['object_class'] = $this->SPACE_NAME; //Add the type of object to each key
			}
			return $All;
		}
	}//End of method


	/**
	 *
	 * @param Filter $params
	 */
	public function count(Filter $filter)
	{
		$query = 'SELECT COUNT('.$this->GetFieldName('id').') AS count FROM '.$this->OBJECT_TABLE
		.' WHERE'.$filter->whereToString();

		$ADODB_COUNTRECS = false;
		if(!$rs = $this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}
		else{
			$result = $rs->FetchRow();
		}
		return $result['count'];
	}//End of method


	//--------------------------------------------------------------------
	/*!\brief
	 */
	protected function GetAllUnion( $params = array() )
	{
		if( !is_array( $this->unionObjects ) ){
			return $this->GetAllBasic( $params );
		}
		array_unshift( $this->unionObjects  , $this ); //add current object to union list

		foreach( $this->unionObjects as $unionObject ){
			$P = $params;
			$P['getQuery'] = true; //set only the components of query without execute it
			$P['select'] = array();
			foreach( $params['select'] as $selectElement ){
				$P['select'][] = $unionObject->FIELDS[$selectElement];
			}
			$query_element = $unionObject->GetAll($P);
			$queries[] = 'SELECT DISTINCT '.$query_element['selectClose'].' FROM '.$query_element['from']
			.' '.$query_element['joinClose']
			.' '.$query_element['whereClose'];
		}

		if(!empty( $queries )){
			$query = implode(' UNION ', $queries);
		}

		$query = $query.' '.$this->orderClose.' '.$this->extra;

		if(!$rs = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}
		else{
			$All = $rs->GetArray(); //To transform result in array;
			return $All;
		}
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 */
	function SetUnion($object , $select)
	{
		if( !is_object($object) ){
			return false;
		}
		if( !method_exists($object, 'GetAll' ) ){
			return false;
		}

		if($select){
			foreach($select as $field){
				if( empty($object->FIELDS[$field] ) )
					$object->FIELDS[$field] = $this->OBJECT_TABLE.'.'.$field;
			}
		}

		return $this->unionObjects[] = $object;
	}//End of method

	//----------------------------------------------------------------------
	/*! \brief Get the id from the number of an object. return id if success, false else.
	 *
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	*  @param  $number(string) is the number to translate in id. Number is the content of the field [object]_number
	*/
	function GetBasicId($number){
		$query = 'SELECT '.$this->FIELDS_MAP_ID
		.' FROM '.$this->OBJECT_TABLE
		.' WHERE '.$this->FIELDS_MAP_NUM.' = \''.$number.'\'';
		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//-------------------------------------------------------------
	/*! \brief Get the number from the id of an object
	 *   return id if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	*  @param  $object_id(integer) is the id of the object to translate
	*/
	function GetBasicNumber($object_id){

		if (is_null($object_id) && !is_numeric($object_id)){
			$this->errorStack->push(ERROR, 'Fatal', array('element'=>$object_id, 'debug'=>array()), 'Invalid object_id : %element%' );
		}

		$query = 'SELECT '.$this->FIELDS_MAP_NUM
		.' FROM '.$this->OBJECT_TABLE
		.' WHERE ('.$this->FIELDS_MAP_ID.' = \''.$object_id.'\')';
		$one = $this->dbranchbe->GetOne($query);
		return $one;

	}//End of method

	//-------------------------------------------------------------------
	/*! \brief Get informations about one object
	 *   return a array if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	*  @param  $object_id(integer) is the id of the object
	*  @param  $selectClose(array) to define the sql select option
	*  @param $get(string) = 'row': return just one row of the recordset
	*  @param $get(string) = 'one': return just the first field of the first row of the recordset
	*/
	function GetBasicInfos($object_id , $selectClose = '' , $get='row'){
		if (is_null ( $object_id ) || !is_numeric( $object_id )){
			$this->errorStack->push(ERROR, 'Fatal', array('element'=>$object_id, 'debug'=>array()), 'Invalid object_id : %element%');
		}

		if (!empty( $selectClose )){
			$selectClose = implode(' , ' , $selectClose);
		}
		else{
			$selectClose = '*';
		}

		$query = 'SELECT '.$selectClose.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID." = '$object_id'";

		if($get == 'one'){
			$infos = $this->dbranchbe->GetOne($query);
		}
		else{
			$infos = $this->dbranchbe->GetRow($query);
		}

		if($infos === false){
			$this->errorStack->push( ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
			return false;
		}

		return $infos;
	}//End of method

	//-------------------------------------------------------------------
	/*! \brief Get records from database
	 *   return a array if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	*  @param $get(string) = 'all': return all records
	*  @param $get(string) = 'row': return just one row of the recordset
	*  @param $get(string) = 'one': return just the first field of the first row of the recordset
	*  @param $rset_return(bool) = 'true': return the recordset(object) in place array
	@param $params[offset](integer) offset sql option
	@param $params[maxRecords](integer) limit sql option
	@param $params[sort_field](string) field use for sort sql option
	@param $params[sort_order]('ASC' or 'DESC') sort direction sql option
	@param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
	This array is translate to a query option like 'where key=value'
	@param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
	This array is translate to a query option like 'WHERE key LIKE %value%'
	@param $params[select](array(field1,field2,field3,...)) to define the sql select option
	\$params[where](array field => value) Input special where close
	\$params['with'](array table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
	*/
	function Get($table , $params , $get='all' , $rset_return=false){

		$this->GetQueryOptions($params);

		$query = "SELECT $this->selectClose FROM $table
		$this->joinClose
		$this->whereClose
		$this->orderClose
		";

		if($get ==='one')$rset = $this->dbranchbe->GetOne($query);
		if($get ==='row')$rset = $this->dbranchbe->GetRow($query);
		if($get ==='all')$rset = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset);

		if($rset === false){
			if($this->errorStack){
				$this->errorStack->push(ERROR_DB, 'Error', array('query'=>$query, 'debug'=>array($this->limit,$this->offset)), $this->dbranchbe->ErrorMsg());
			}
			return false;
		}

		if($get ==='all' && $rset_return === false){
			$rset = $rset->GetArray(); //To transform object result in array
		}

		return $rset;
	}//End of method

	//-------------------------------------------------------------------
	/*! \brief Get informations about one object like GetBasicInfos function
	 *   but accept the number of object in input
	*   return a array if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup, product, partner...etc.
	*  @param  $object_number(string) is the number of the object
	*  @param  $selectClose(array) to define the sql select option
	*/
	function GetBasicInfosNum($object_number , $selectClose = ''){

		if(is_null($object_number)){
			$this->errorStack->push(ERROR, 'Fatal', array('element'=>$object_number, 'debug'=>array()), 'Invalid object_number : %element%' );
		}

		if (!empty( $selectClose ) ) {
			$selectClose = implode(' , ' , $selectClose);
		} else { $selectClose = '*';
		}

		$query = "SELECT $selectClose FROM $this->OBJECT_TABLE WHERE $this->FIELDS_MAP_NUM = '$object_number'
		";

		if(!$infos = $this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return FALSE;
		}else{
			$infos = $infos->GetArray(); //To transform object result in array;
			return $infos[0];
		}

	}//End of method

	//-------------------------------------------------------------------
	/*! \brief Get number of indice list link to space
	 *  return an array indice_id=>indice_number if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup...etc.
	*  Each space has his own indice list definition.
	*  @param  $object_id(integer) is the id of the object
	*  @param  $currentIndice_id(integer) If is set return only the indice greater than $currentIndice_id.
	*/
	//TODO: Cette methode n'a pas de raison d etre herite par les objets partner, doctypes, metadata....
	function GetIndiceList($currentIndice_id = 0){

		$query = 'SELECT '.$this->FIELDS_MAP_INDICE.' , indice_value FROM '.$this->INDICE_TABLE
		.' WHERE '.$this->FIELDS_MAP_INDICE.' > '.$currentIndice_id;

		if(!$all = $this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}else{
			$all = $all->GetAssoc(); //To transform object result in array;
			return $all;
		}

	}//End of method

	//----------------------------------------------------------
	/*! \brief Create a link between 2 objects
	 *  return true if success, false else.
	*
	*  object is somthing like : project, workitem, bookshop, cadlib, mockup...etc.
	*  This is a generic function to create a record in the table $linktable
	*
	*  @param  $object_id(integer) is the id of the object
	*  @param  $linkTable(string) Name of the table to use for links
	*  @param  $data(array) field=>value for create record in link table
	*/
	protected function CreateLink($object_id , $linkTable , $data){
		$data[$this->FIELDS_MAP_ID] = $object_id;
		if (!$this->dbranchbe->AutoExecute( "$linkTable" , $data , 'INSERT')){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data,$linkTable)), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief This method decompose the var $params and return the correct synthase for the "where" and "select" for SQL query.
	 *
	*  Exemple of query :
	*  $query = "SELECT $this->selectClose FROM `TABLE`
	*     $this->whereClose
	*     $this->orderClose
	*     ";
	*  $dbranchbe->SelectLimit( $query , $this->limit , $this->offset);
	*  This method is associated to script "filterManager.php" wich get values from forms
	*
	*  return :
	*  $this->selectClose
	*  $this->whereClose
	*  $this->orderClose
	*  $this->limit
	*  $this->offset
	*  $this->from

	@param $params[offset](integer) offset sql option
	@param $params[maxRecords](integer) limit sql option
	@param $params[sort_field](string) field use for sort sql option
	@param $params[sort_order]('ASC' or 'DESC') sort direction sql option
	@param $params[exact_find](array) its a assoc array where key=a field and value=a word to find in the table.
	This array is translate to a query option like 'where key=value'
	@param $params[find](array) its a assoc array where key=a field and value=a string to find in the table.
	This array is translate to a query option like 'WHERE key LIKE %value%'
	to find multi value on the same field just separate the word by close OR or AND like this :
	nom1 OR nom2 OR nom3 with space on each side of OR
	@param $params[select]=array(field1,field2,field3,...) its a nonassoc array with all fields to put in the sql select option
	\$params[where](array) Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	\$params['with'] = array(type=>"INNER or OUTER" , table=>"table to join" , col=>"field use for ON condition") Use this param for create a join close on query
	\$params['extra'] = any query to add to end
	*/
	protected function GetQueryOptions($params){

		$fromClose  = $this->OBJECT_TABLE;

		if ( isset($params['with']) && is_array($params['with']) ) {
			if( isset($params['with'][0]) ){
				foreach( $params['with'] as $with ){
					if( empty($with['type']) ) $with['type'] = 'INNER';
					$array_joinClose[] = $with['type'].' JOIN '. $with['table'] .' ON '. $with['table'].'.'.$with['col'] .'='.$this->OBJECT_TABLE.'.'.$with['col'];
				}
			}
			else{
				if( empty($params['with']['type']) ) $params['with']['type'] = 'INNER';
				$joinClose = $params['with']['type'].' JOIN '. $params['with']['table'] .' ON '. $params['with']['table'].'.'.$params['with']['col'] .'='.$this->OBJECT_TABLE.'.'.$params['with']['col'];
			}
		}
		if( is_array($array_joinClose) ){
			$joinClose = implode(' ', $array_joinClose );
		}

		if(!empty ($params['find'])){
			foreach ( $params['find'] as $field => $term ){
				if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field];
				$orClose = array();
				$andClose = array();
				$termOrExplode = explode(' OR ', $term ); //Explode
				$termAndExplode = explode(' AND ', $term ); //Explode
				if( count($termOrExplode) > 1 ){ //A OR is require
					foreach($termOrExplode as $term){
						$orClose[] = "$field LIKE ('%$term%')";
					}
					$whereClose[] = '('.implode(' OR ', $orClose).')';
				}
				if( count($termAndExplode) > 1 ){ //A AND is require
					foreach($termAndExplode as $term){
						$andClose[] = "$field LIKE ('%$term%')";
					}
					$whereClose[] = '('.implode(' AND ', $andClose).')';
				}
				if(count($termOrExplode) <= 1 && count($termAndExplode) <= 1){ //just a simple string
					$whereClose[] = $field." LIKE ('%$term%')";
				}
			}
		}

		if(!empty ($params['exact_find'])){
			foreach ( $params['exact_find'] as $field => $term){
				if( !empty( $this->FIELDS[$field] ) ) $field = $this->FIELDS[$field];
				$whereClose[] = "($field = '$term')";
			}
		}

		if(isset($params['where']) && is_array ($params['where'])){
			foreach($params['where'] as $wc)
				$whereClose[] = "($wc)";
		}

		if(!empty($params['offset']) && is_numeric($params['offset'])){
			$offset = $params['offset'];
		}
		else{
			$offset = 0;
		}

		if(!empty($params['numrows']) && is_numeric($params['numrows'])){
			$limit = $params['numrows'];
		}
		else{
			$limit = '9999';
		}

		if(!empty($params['sort_field'])){
			if (!isset($params['sort_order'])) $params['sort_order']='ASC';
			$orderClose = " ORDER BY " . $params['sort_field'] . " " . $params['sort_order'];
		}

		if(!empty( $whereClose )){
			$whereClose = 'WHERE ' . implode(' AND ', $whereClose);
		}

		if (isset($params['select']) && is_array($params['select'])){
			$selectClose = implode(' , ' , $params['select']);
		}
		else {
			$selectClose = '*';
		}

		if (isset($selectClose))  {
			$this->selectClose = $selectClose;
		}
		else{
			$this->selectClose = NULL;
		}

		if (isset($whereClose))  {
			$this->whereClose  = $whereClose;
		}
		else{
			$this->whereClose = NULL;
		}

		if (isset($orderClose))  {
			$this->orderClose  = $orderClose;
		}
		else{
			$this->orderClose = NULL;
		}
		if (isset($limit))        $this->limit       = $limit;
		if (isset($offset))       $this->offset      = $offset;
		if (isset($fromClose))    $this->from        = $fromClose;
		if (isset($joinClose))    $this->joinClose   = $joinClose;
		if (isset($params['extra'])) $this->extra    = $params['extra'];
	}//End of method

	//---------------------------------------------------------------------------
	function GetQueryOptionsArray( $params=array() )
	{
		$this->GetQueryOptions($params);
		return array(
				'selectClose'=>$this->selectClose,
				'whereClose'=>$this->whereClose,
				'orderClose'=>$this->orderClose,
				'limit'=>$this->limit,
				'offset'=>$this->offset,
				'from'=>$this->from,
				'joinClose'=>$this->joinClose,
				'extra'=>$this->extra,
		);
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief This method set class var FILE_TABLE
	 *  Return core_prop(array)
	*
	* This method set the var FILE_TABLE with the correct value of the table
	* wich contents the file associate to document.
	* In data model there is 2 distincts tables for manages files of a file_manager container(name = []_file_table)
	* and file of a document_manager container(name = []_doc_file_table).
	*/
	//TODO: Cette methode n'a pas de raison d etre herite par les objets partner, doctypes, metadata....
	function SetFileTable()
	{
		$this->FILE_TABLE = $this->DOC_FILE_TABLE;
	}//End of method

	//---------------------------------------------------------------------------
	function GetTableName($table)
	{
		switch($table){
			case 'history':
				if(isset($this->HISTORY_TABLE))
					return $this->HISTORY_TABLE;
				return false;
				break;
			case 'object':
				if(isset($this->OBJECT_TABLE))
					return $this->OBJECT_TABLE;
				return false;
				break;
			case 'indice':
				if(isset($this->INDICE_TABLE))
					return $this->INDICE_TABLE;
				return false;
				break;
				/*
				 case 'metadata':
				if(isset($this->METADATA_TABLE))
				 return $this->METADATA_TABLE;
				return false;
				break;
				*/
		} //End of switch
		return false;
	}//End of method

	//---------------------------------------------------------------------------
	function GetFieldName($field)
	{
		switch($field){
			case 'id':
				if(isset($this->FIELDS_MAP_ID))
					return $this->FIELDS_MAP_ID;
				return false;
				break;
			case 'number':
				if(isset($this->FIELDS_MAP_NUM))
					return $this->FIELDS_MAP_NUM;
				return false;
				break;
			case 'description':
				if(isset($this->FIELDS_MAP_DESC))
					return $this->FIELDS_MAP_DESC;
				return false;
				break;
			case 'state':
				if(isset($this->FIELDS_MAP_STATE))
					return $this->FIELDS_MAP_STATE;
				return false;
				break;
			case 'indice':
				if(isset($this->FIELDS_MAP_INDICE))
					return $this->FIELDS_MAP_INDICE;
				return false;
				break;
			case 'father':
				if(isset($this->FIELDS_MAP_FATHER))
					return $this->FIELDS_MAP_FATHER;
				return false;
				break;
		} //End of switch
		return false;
	}//End of method
}// End of class

//---------------------------------------------------------------------------
/*! \brief Class for manage objects of ranchBE.
 *
*   An object is one element of this list :
*   projects, workitems, bookshops, cadlibs, mockups, products,
*   and other container wich can be derived from the class containersManager.
*   doctypes and partners are specials objects wich have separate manager.
*   This class can be use without instance
*   Ranchbe-objects are not programs objects but an abstractions concept of RanchBE
*   RanchBE-objects are the bases of all components of ranchbe.
*   This class is derived by all the API classes
*/
abstract class objects extends rb_observable {

	/*! \brief Get a list of all object recorded in ranchBE.
	 *
	*   return associative array if success, false else.
	*   Objects are :
	*   project, workitem, bookshop, cadlib, mockup, product, and other container.
	*   This method return too the partners and the doctype if $other=true
	@param $params is use for manage the display. See parameters function of GetQueryOptions()
	*/
	function ObjectGet($params = array()){

		$projectsManager   = new project();
		$workitemsManager  = new workitem();
		$productsManager   = new product();
		$containersManager = new container();

		$projects    = $projectsManager->GetAllBasic($params);
		$workitems   = $workitemsManager->GetAllBasic($params);
		$products    = $productsManager->GetAllBasic($params);
		$containers  = $containerManager->GetAllBasic($params);

		$objectsList['projects']   = $projects;
		$objectsList['workitems']  = $workitems;
		$objectsList['products']   = $products;
		$objectsList['containers'] = $containers;

		return $objectsList;

	}//End of method

	//-----------------------------------------------------------------------
	/*! \brief create the basic data for objects project, workitem, bookshop, cadlib, mockup, product, and other container
	 *   return the object id if success, false else.
	*
	*   This create LiveUser rights definition for manage the access to the new object. The right name must be definied
	*   by the array $perm['rights']. For manage the area, we use LiveUser_admin wich use MDB2 abstraction layer for access to database
	*   So, the code here is special for LiveUser because RanchBE use the abstraction layer Adodb.
	*   In future, we hope use only one abstraction layer for entire RanchBE application.
	*   The value of array data must be check befor. Here, we suppose that $data is secure.
	*   Set :
	*      $this->id
	$this->number
	$this->description
	$this->indice
	$this->state
	$this->open_date
	$this->open_by
	$this->forseen_close_date
	$this->history

	@param $data(array) , its an array where keys/values are fields/values of the record to create.
	@param $perms(array) see exemple :<br />
	array(2) {<br />
	["area_suffix"]=> string(8) "project_"<br />
	["rights"]=>array(12) {<br />
	["container_document_get"]=>string(14) "list documents"<br />
	["container_document_manage"]=>string(23) "create/modify documents"<br />
	["container_document_suppress"]=>string(18) "suppress documents"<br />
	["container_document_assoc"]=>string(39) "associate manually a file to a document"<br />
	["container_document_unlock"]=>string(16) "unlock documents"<br />
	["container_document_archive"]=>string(24) "manage documents archive"<br />
	["container_document_change_indice"]=>string(28) "change indice of a documents"<br />
	["container_document_change_state"]=>string(27) "change state of a documents"<br />
	["container_document_change_number"]=>string(28) "change number of a documents"<br />
	["container_document_link_file"]=>string(32) "manage files linked to documents"<br />
	["container_document_history"]=>string(24) "manage documents history"<br />
	["container_document_version"]=>string(25) "manage documents versions"<br />
	}
	}
	*/
	function BasicCreate($data , $perms = array()){
		$usr = Ranchbe::getCurrentUser();
		$db = Ranchbe::getDb();

		if(isset($perms['rights'])){ //Create a new area for liveuser
			$userlib=Ranchbe::getUserLib();

			global $trans; //true if MDB2 driver support transaction. See LUconf.php
			if ($trans){
				$res = $db->beginTransaction(); //Initialise transaction for MDB2 use by LiveUser only.
			}

			$area_define_name = $perms['area_suffix'] . md5(uniqid(rand(), true));
			$area_id = $userlib->add_area($area_define_name);

			if ( $area_id === false ) {
				$this->errorStack->push(ERROR, 'Fatal', array('element'=>$area_id, 'debug'=>array($area_define_name,$area_id)), 'cant create area and perm definition' );
				//print 'cant create area and perm definition' ;
				if ($trans) $res = $db->rollback();
				die(1);
			}

			//Create right definition
			foreach( $perms['rights'] as $define_name => $description){
				$userlib->add_right ($area_id  , $define_name , $description );
			}

			$data['area_id']   = $area_id; //Define var for data creation
			//$this->area_id     = $data['area_id']; //Update object property

		} //End of define area

		$data['open_date']            = time(); //Must be a timestamp
		$data['open_by']              = $usr->getid();
		$data['update_by'] = $usr->getid();
		$data['update_date'] = time();

		$id = basic::BasicCreate($data);

		if (!$id){
			if ( $trans ) $res = $db->rollback(); //RollBack the transaction for LiveUser
			return false;
		}else{
			if ($trans) $res = $db->commit(); //Valid the transaction for LiveUser
		}

		//Update the object :
		/*$this->id          = $data["$this->FIELDS_MAP_ID"];
		 $this->number      = $data["$this->FIELDS_MAP_NUM"];
		$this->description = $data["$this->FIELDS_MAP_DESC"];
		$this->indice      = $data["$this->FIELDS_MAP_INDICE"];
		$this->state       = $data["$this->FIELDS_MAP_STATE"];
		$this->open_date   = $data['open_date'];
		$this->open_by     = $data['open_by'];
		$this->forseen_close_date  = $data['forseen_close_date'];*/

		$this->history = $data;

		return $id;

	}//End of method

	//----------------------------------------------------------------------
	/*! \brief suppress the basic data for objects like :
	 */
	//function BasicSuppress($object_id) {
	//TODO : Update the object history
	//return parent::BasicSuppress($object_id);
	//}//End of method

	//----------------------------------------------------------------------
	/*! \brief modify the basic data for objects like :
	 *   project, workitem, bookshop, cadlib, mockup, product, and other container
	*   return true if success, false else.
	*
	*   This method call the BasicUpdate method of BasicManager
	*   And update the object properties if modified
	@param $data(array), its an array where keys/values are fields/values of the record to update.
	@param $object_id, is the id of the record to update.
	*/
	function BasicUpdate( $data , $object_id ) {
		if(!parent::BasicUpdate( $data , $object_id )) return false; //Update the database

		//Update the object :
		if (!isset($data['number']))
			$this->number      = $data["$this->FIELDS_MAP_NUM"];

		if (!isset($data['description']))
			$this->description    = $data["$this->FIELDS_MAP_DESC"];

		if (!isset($data['indice']))
			$this->indice         = $data["$this->FIELDS_MAP_INDICE"];

		if (!isset($data['state']))
			$this->state          = $data["$this->FIELDS_MAP_STATE"];

		if (!isset($data['forseen_close_date']))
			$this->forseen_close_date  = $data['forseen_close_date'];

		if (!isset($data['close_date']))
			$this->close_date  = $data['close_date'];

		if (!isset($data['close_by']))
			$this->close_by  = $data['close_by'];

		if (!isset($data['default_process_id']))
			$this->default_process_id  = $data['default_process_id'];

		if (!isset($data['project_id']))
			$this->project_id  = $data['project_id'];

		return true;

	}//End of method

} //End of class

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*! \brief Class for manage history to follow the life of all ranchBE components.
 */
class history extends basic
{

	private $Manager;
	public  $space;
	protected $HISTORY_TABLE;
	protected $FIELDS_MAP_ID;
	protected $dbranchbe;
	protected $usr;

	/*
	 $Manager is a container or a document?
	*/
	function __construct($Manager)
	{
		if ( !( is_a($Manager, 'document') || is_a($Manager, 'container') || is_a($Manager, 'project') ) ) { //$Manager is a container or a document
			return false;
		}

		$this->Manager = $Manager;
		$this->space = $Manager->space;
		$this->HISTORY_TABLE = $this->Manager->GetTableName('history');
		$this->OBJECT_TABLE = $this->HISTORY_TABLE;
		$this->FIELDS_MAP_ID = $this->Manager->GetFieldName('id');

		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->errorStack = Ranchbe::getErrorStack();
	}//End of method

	/*! \brief Method for write history data in history table. Return the histo_order if success, else false
	 *
	*   The history table name is set by the HISTORY_TABLE property of the object(see [container]Manager.php)
	@param $data(array), its an array where keys/values are fields/values of the history record to write.
	*/
	function write ($data){
		$this->dbranchbe->StartTrans();
		$seq = "$this->HISTORY_TABLE" . '_seq';
		$data['histo_order'] = $this->dbranchbe->GenID($seq , 1 ); //Increment the sequence number
		if( !isset($data['action_date']) ){
			$data['action_date']    = time();
		}
		if( !isset($data['action_by']) ){
			$data['action_by']      = $this->usr->getLogin();
		}
		if (!$this->dbranchbe->AutoExecute( "$this->HISTORY_TABLE" , $data , 'INSERT')){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data,$linkTable)), $this->dbranchbe->ErrorMsg());
			return $this->dbranchbe->CompleteTrans(false);
		}
		if (!$this->dbranchbe->CompleteTrans()){
			return false;
		}
		return $data['histo_order'];
	}//End of method

	//------------------------------------------------------------------------
	/*! \brief Method for get all history record filter by the option set in $params
	 *
	*   The history table name is set by the HISTORY_TABLE property of the object(see [container]Manager.php)
	@param $params is use for manage the display. See parameters function of GetQueryOptions()
	*/
	function GetAllHistory ($params){
		$params['table'] = $this->HISTORY_TABLE;
		$this->GetQueryOptions($params);

		$query = 'SELECT '.$this->selectClose.' FROM '.$this->HISTORY_TABLE.' '.
				$this->joinClose.' '.
				$this->whereClose.' '.
				$this->orderClose;

		if(!$All = $this->dbranchbe->SelectLimit( $query , $this->limit , $this->offset)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array($this->limit , $this->offset)), $this->dbranchbe->ErrorMsg());
			return false;
		}
		else{
			$All = $All->GetArray(); //To transform result in array;
			return $All;
		}
	}//End of method

	//------------------------------------------------------------------------
	/*! \brief Method for get history of a particular object record filter by the option set in $params
	 *
	@param integer $id is the id of the object to list history
	@param array $params is use for manage the display. See parameters function of GetQueryOptions()
	*/
	function GetHistory($id , $params =''){
		$params['exact_find'][$this->FIELDS_MAP_ID] = $id;
		return $this->GetAllHistory($params);
	}//End of method

	//------------------------------------------------------------------------
	/*! \brief Method for remove history records
	 *
	*   The $params is use for create a query for select all records to suppress
	@param array (exact_find=>"word to find in field", find_field=>"field where to search word")
	*/
	private function RemoveHistory ( $params ){

		if (!empty ($params['exact_find']) && !empty ($params['find_field'])) {
			$find = $params['exact_find'];
			$whereClose[] = "($params[find_field] = '$find' )";
		}

		if (! empty( $whereClose )){
			$whereClose = 'WHERE ' . implode(' AND ', $whereClose);
		}else return('none whereclose');

		$query = "DELETE FROM $this->HISTORY_TABLE $whereClose";

		if ( $this->dbranchbe->Execute( "$query") === false ) { //Write database
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}

		return true;

	}//End of method

	//------------------------------------------------------------------------
	/*! \brief Method for remove history records from the histo_order id
	 *
	*/
	function RemoveHistorySingle ( $histo_order ){

		$params['exact_find']  = $histo_order;
		$params['find_field'] =  'histo_order';

		return $this->RemoveHistory($params);

	}//End of method

} //End of class
