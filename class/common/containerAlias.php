<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('class/common/container.php');

/*! \brief
 */
class containerAlias extends container
{

	function __construct(space &$space , $alias_id=NULL)
	{
		parent::__construct( $space );
		/*
		 $this->FIELDS = array(
		 		'container_id'=>$this->space->SPACE_NAME.'_alias.workitem_id',
		 		'container_number'=>$this->space->SPACE_NAME.'_alias.workitem_number',
		 		'container_description'=>$this->space->SPACE_NAME.'_alias.workitem_description',
		 		'object_class'=>$this->space->SPACE_NAME.'_alias.object_class',
		 		'container_state'=>$this->OBJECT_TABLE.'.workitem_state',
		 		'file_only'=>$this->OBJECT_TABLE.'.file_only',
		 		'container_indice_id'=>$this->OBJECT_TABLE.'.workitem_indice_id',
		 		'project_id'=>$this->OBJECT_TABLE.'.project_id',
		 		'default_process_id'=>$this->OBJECT_TABLE.'.default_process_id',
		 		'default_file_path'=>$this->OBJECT_TABLE.'.default_file_path',
		 		'open_date'=>$this->OBJECT_TABLE.'.open_date',
		 		'open_by'=>$this->OBJECT_TABLE.'.open_by',
		 		'forseen_close_date'=>$this->OBJECT_TABLE.'.forseen_close_date',
		 		'close_date'=>$this->OBJECT_TABLE.'.close_date',
		 		'close_by'=>$this->OBJECT_TABLE.'.close_by',
		 		'alias_id'=>$this->space->SPACE_NAME.'_alias.alias_id',
		 );
		*/

		$this->FIELDS[$this->FIELDS_MAP_ID]     = $this->space->SPACE_NAME.'_alias.'.$this->FIELDS_MAP_ID;
		$this->FIELDS[$this->FIELDS_MAP_NUM]    = $this->space->SPACE_NAME.'_alias.'.$this->FIELDS_MAP_NUM;
		$this->FIELDS[$this->FIELDS_MAP_DESC]   = $this->space->SPACE_NAME.'_alias.'.$this->FIELDS_MAP_DESC;
		$this->FIELDS['object_class']           = $this->space->SPACE_NAME.'_alias.object_class';
		$this->FIELDS['alias_id']           	= $this->space->SPACE_NAME.'_alias.alias_id';
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all container.
	* return an array if success, else return false.
	\param $params is use for manage the display. See parameters function of GetQueryOptions()
	*/
	function GetAll( $params = array() )
	{
		$params['with']['table'] = $this->space->SPACE_NAME.'_alias';
		$params['with']['col'] = $this->FIELDS_MAP_ID;
		return $this->GetAllBasic($params);
	}//End of method

} //End of class

