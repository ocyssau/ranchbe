<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*
 DROP TABLE `workitem_alias`;
CREATE TABLE `workitem_alias` (
		`alias_id` int(11) NOT NULL ,
		`workitem_id` int(11) NOT NULL,
		`workitem_number` varchar(16)  NOT NULL,
		`workitem_description` varchar(128)  default NULL,
		`object_class` varchar(10)  NOT NULL default 'workitemAlias',
		PRIMARY KEY  (`alias_id`),
		UNIQUE KEY `UC_workitem_alias` (`alias_id`,`workitem_id`),
		KEY `K_workitem_alias_1` (`workitem_id`)
		KEY `K_workitem_alias_2` (`workitem_number`),
) ENGINE=InnoDB ;
*/

/*! \brief
 */
class containerAliasDAO extends basic
{

	function __construct(container &$container , $alias_id=NULL){
		$this->dbranchbe = Ranchbe::getDb();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->space = $container->space;

		$this->OBJECT_TABLE  = $this->space->SPACE_NAME.'_alias';
		$this->FIELDS_MAP_ID = 'alias_id';
		$this->FIELDS_MAP_NUM = $this->space->CONT_FIELDS_MAP_NUM;
		$this->FIELDS_MAP_DESC = $this->space->CONT_FIELDS_MAP_DESC;

		/*
		 $this->FIELDS = array(
		 		'container_id'         =>$this->OBJECT_TABLE.'.'.$container->GetFieldName('id'),
		 		'container_number'     =>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_NUM,
		 		'container_description'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_DESC,
		 		'class_object'         =>$this->OBJECT_TABLE.'.class_object',
		 		'alias_id'             =>$this->OBJECT_TABLE.'.alias_id',
		 );
		*/

		$this->FIELDS = array(
				$container->GetFieldName('id')=>$this->OBJECT_TABLE.'.'.$container->GetFieldName('id'),
				$this->FIELDS_MAP_NUM         =>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_NUM,
				$this->FIELDS_MAP_DESC        =>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_DESC,
				'class_object'         =>$this->OBJECT_TABLE.'.class_object',
				'alias_id'             =>$this->OBJECT_TABLE.'.alias_id',
		);

		if(!is_null($alias_id)){
			$this->init($alias_id);
		}

		$this->AREA_ID = $container->AREA_ID;

	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the properties
	 *
	* \param $alias_id(integer)
	*/
	function init($alias_id)
	{
		$this->alias_id = $alias_id;
		if(isset($this->core_prop)) unset($this->core_prop);
		return true;
	}//End of method

	//--------------------------------------------------------------------
	/*! \brief Add an alias
	 *
	* \param $params(array)
	*/
	function Create( $params )
	{
		//Filter input
		foreach($params as $key=>$val){
			$this->SetProperty($key, $val);
		}
		return $this->BasicCreate( $this->core_prop );
	}//End of method

	//----------------------------------------------------------
	/*! \brief Update a alias
	 *
	\param $alias_id(integer)
	\param $data(array)
	*/
	function Update( $params )
	{
		if(!isset($this->alias_id)){
			$this->errorStack->push(LOG, 'Error', array(), '$this->alias_id is not set');
			die;
			return false;
		}
		unset($params['alias_id']); //to prevent the change of id
		foreach($params as $key=>$val){
			$this->SetProperty($key, $val);
		}
		return $this->BasicUpdate($this->core_prop , $this->alias_id);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Suppress an alias
	 *
	\param $alias_id(integer)
	*/
	function Suppress( $alias_id )
	{
		return $this->BasicSuppress($alias_id);
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all container.
	* return a array if success, else return false.
	\param $params is use for manage the display. See parameters function of GetQueryOptions()
	*/
	function GetAll($params = array() )
	{
		$params['with']['table'] = $this->space->SPACE_NAME.'_alias';
		$params['with']['col'] = $this->FIELDS_MAP_ID;

		return $this->GetAllBasic($params);
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the container by the property name. init() must be call before
	 *
	* \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
	*/
	function GetProperty($property_name)
	{
		if(!isset($this->alias_id)){
			$this->errorStack->push(LOG, 'Error', array(), '$this->alias_id is not set');
			return false;
		}

		switch($property_name){
			case 'container_id':
				if(isset($this->core_prop[ $this->space->CONT_FIELDS_MAP_ID ]))
					return $this->core_prop[ $this->space->CONT_FIELDS_MAP_ID ];
				else $realName = $this->space->CONT_FIELDS_MAP_ID;
				break;

			case 'container_number':
				if(isset($this->core_prop[$this->FIELDS_MAP_NUM])){
					return $this->core_prop[$this->FIELDS_MAP_NUM];
				}else $realName = $this->FIELDS_MAP_NUM;
				break;

			case 'container_description':
				if(isset($this->core_prop[$this->FIELDS_MAP_DESC]))
					return $this->core_prop[$this->FIELDS_MAP_DESC];
				else $realName = $this->FIELDS_MAP_DESC;
				break;

			default:
				if(isset($this->core_prop[$property_name]))
					return $this->core_prop[$property_name];
				else $realName = $property_name;
				break;

		} //End of switch

		$query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->alias_id.'\'';
		$res = $this->dbranchbe->GetOne($query);
		if($res === false){
			$this->errorStack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($this->alias_id)), $this->dbranchbe->ErrorMsg());
			return false;
		}
		else
			return $this->core_prop[$realName] = $res;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the container by the property name. init() must be call before
	 *
	* \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
	*/
	function SetProperty($property_name, $property_value)
	{
		switch($property_name){
			case 'container_id':
				return $this->core_prop[$this->space->CONT_FIELDS_MAP_ID] = $property_value;
				break;

			case 'container_number':
				return $this->core_prop[$this->FIELDS_MAP_NUM] = $property_value.'_alias';
				break;

			case 'container_description':
				return $this->core_prop[$this->FIELDS_MAP_DESC] = $property_value;
				break;

			default:
				return $this->core_prop[$property_name] = $property_value;
				break;
		} //End of switch
	}//End of method

} //End of class

