<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief This class manage metadata associate to container and documents.
 *
 * This class create,suppress,modify metadatas linked to the ranchBE components.
 * For use this class the method set_type must be call first.
 */
abstract class metadata extends basic
{

	protected $OBJECT_TABLE;//Table contenant la definition des metadonnees
	protected $EXTEND_TABLE;//table dont le "datadictonary" doit etre etendu
	public $error_stack;
	public $space;
	public $dbranchbe;
	public $usr;

	//List of type of fields that can be set in metadatas
	public $accept_field_type = array('text', 'long_text','html_area', 'partner' , 'doctype' , 'user' , 'document_indice' , 'process' , 'category' , 'date' , 'integer' , 'decimal', 'select' , 'selectFromDB', 'liveSearch');

	//--------------------------------------------------------------------
	/*! \brief Set the type of field.
	 *
	 * This method return the type of field to create in the table of the component(container or document table).
	 <br />return :<br />
	 $this->field_type(string)   For set the type of field in database. See type list in the comments.
	 <br />
	 List of valid type :<br />
	 C:  Varchar, capped to 255 characters.<br />
	 X:  Larger varchar, capped to 4000 characters (to be compatible with Oracle). <br />
	 XL: For Oracle, returns CLOB, otherwise the largest varchar size.<br />
	 <br />
	 C2: Multibyte varchar<br />
	 X2: Multibyte varchar (largest size)<br />
	 <br />
	 B:  BLOB (binary large object)<br />
	 <br />
	 D:  Date (some databases do not support this, and we return a datetime type)<br />
	 T:  Datetime or Timestamp<br />
	 L:  Integer field suitable for storing booleans (0 or 1)<br />
	 I:  Integer (mapped to I4)<br />
	 I1: 1-byte integer<br />
	 I2: 2-byte integer<br />
	 I4: 4-byte integer<br />
	 I8: 8-byte integer<br />
	 F:  Floating point number<br />
	 N:  Numeric or decimal number<br />
	 <br />
	 $this->field_size(integer)   Size of the field varchar or string.<br />
	 $this->valid_infos(array)<br /> list of infos to set when you create the metadata.


	 * \param $Manager(object) Object of the component(document or container) to manage metadatas
	 * \param $type(string) Type of container(bookshop, cadlib, workitem, mockup...)
	 */
	function set_type($type){

		switch ( $type ) {
			case 'text':
			case 'long_text':
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_regex','field_required');
				break;
			case 'html_area':
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_required');
				break;
			case 'partner':
				$this->field_type = 'C';
				$this->field_size = 32;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'doctype':
				$this->field_type = 'C';
				$this->field_size = 32;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'document_indice':
				$this->field_type = 'C';
				$this->field_size = 16;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'container_indice':
				$this->field_type = 'C';
				$this->field_size = 16;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'user':
				$this->field_type = 'C';
				$this->field_size = 32;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'process':
				$this->field_type = 'C';
				$this->field_size = 80;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'category':
				$this->field_type = 'C';
				$this->field_size = 32;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_where');
				break;
			case 'date':
				$this->field_type = 'I';
				$this->valid_infos = array('field_required');
				break;
			case 'integer':
				$this->field_type = 'I';
				$this->valid_infos = array('field_size','field_required');
				break;
			case 'decimal':
				$this->field_type = 'F';
				$this->valid_infos = array('field_size','field_required');
				break;
			case 'select':
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','field_list');
				break;
			case 'selectFromDB':
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','table_name','field_for_value','field_for_display');
				break;
			case 'liveSearch':
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_required','field_multiple','return_name','table_name','field_for_value','field_for_display');
				break;
			default:
				$this->field_type = 'C';
				$this->field_size = 255;
				$this->valid_infos = array('field_size','field_regex','field_required');
				break;
		} //End of switch

	}//End of method

	//--------------------------------------------------------------------
	/*! \brief Create a metadata.
	 *
	 * \param $params(array) field=>value of metadata to record.
	 */
	function CreateMetadata($params){

		if(!isset($this->field_type)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'type is not set');
			return false;}

			//Filter input data
			foreach ( $this->valid_infos as $input) {
				if (isset($params["$input"]) && !empty($params["$input"]))
				$data["$input"] = $params["$input"];
			}

			//Add common input data
			$data['field_name'] = $params['field_name']; //only minuscules characters and _ are permit
			$data['field_description'] = $params['field_description'];
			$data['field_type'] = $params['field_type'];

			if( !$this->BasicCreate($data) ) {
				$this->errorStack->push(ERROR, 'Fatal', array('prop_name'=>$data['field_name']), 'can not create property %prop_name%' );
				return false;
			}

			/*
			 $this->dbranchbe->StartTrans();

			 if (!$this->dbranchbe->AutoExecute( "$this->OBJECT_TABLE" , $data , 'INSERT')){
			 $this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			 return $this->dbranchbe->CompleteTrans(false);
			 }
			 */

			// -- Create a adodb datadictionnay object
			$dbdict = NewDataDictionary($this->dbranchbe);

			// -- Check if the field is not yet existing
			$field_in_table = $this->dbranchbe->MetaColumnNames($this->EXTEND_TABLE);
			if(in_array($data['field_name'], $field_in_table)){
				$this->errorStack->push(ERROR, 'Fatal', array('element'=>$data['field_name'], 'debug'=>array($data)), 'Field %element% exist');
				return $this->dbranchbe->CompleteTrans(false);}

				//Create the field in the document table
				$flds = array(
				array($data['field_name'], $this->field_type , $this->field_size),
				);
				$sqlarray = $dbdict->ChangeTableSQL($this->EXTEND_TABLE, $flds);
				if(!$dbdict->ExecuteSQLArray($sqlarray) ) {
					$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
					return $this->dbranchbe->CompleteTrans(false);
				}

				if(!$this->dbranchbe->CompleteTrans()){
					return false;
				}

				return true;

	}//End of method

	//----------------------------------------------------------
	/*! \brief Get list of metadatas.
	 *
	 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetMetadata($params=array()){
		$this->GetQueryOptions($params);
		$query = "SELECT $this->selectClose FROM $this->OBJECT_TABLE as m
		$this->whereClose
		$this->orderClose
              ";
		if(!$All = $this->dbranchbe->SelectLimit($query , $this->limit , $this->offset)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array($this->limit , $this->offset)), $this->dbranchbe->ErrorMsg());
			return false;
		}else{
			$All = $All->GetArray(); //To transform result in array;
			return $All;
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get infos about one metadata
	 *
	 \param $field_name(string) name of the metadata to get infos.
	 */
	function GetMetadataInfos($field_name){

		$params['exact_find']['field_name'] = $field_name;
		if(!$infos = $this->GetMetadata($params)){
			return false;
		}else
		return $infos[0];

	}//End of method

	//----------------------------------------------------------
	/*! \brief Update a metadata
	 *
	 * You can not change the field name of the metadata.
	 \param $field_name(string) name of the metadata to get infos.
	 \param $data(array) Field=>value of metadata property to update.
	 */
	function ModifyMetadata( $field_name , $data ){

		//to prevent the change of the field name
		unset($data['field_name']);

		if(!$this->dbranchbe->AutoExecute( "$this->OBJECT_TABLE" , $data , 'UPDATE' , "field_name = '$field_name'")){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}

		return true;

	}//End of method

	//----------------------------------------------------------
	/*! \brief Suppress a metadata.
	 *
	 * This method suppress the infos record in []_metadata table and suppress the col in the container or document table.
	 * All data records in this column will be lost.
	 \param $field_name(string) name of the metadata to get infos.
	 */
	function SuppressMetadata($field_name){

		$query = "DELETE FROM $this->OBJECT_TABLE WHERE field_name = '$field_name' ";

		if(!$this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return $this->dbranchbe->CompleteTrans(false);
		}

		//Create a adodb datadictionnay object
		$dbdict = NewDataDictionary($this->dbranchbe);
		$sqlarray = $dbdict->DropColumnSQL($this->EXTEND_TABLE, $field_name);
		if (!$dbdict->ExecuteSQLArray($sqlarray)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return $this->dbranchbe->CompleteTrans(false);
		}

		if (!$this->dbranchbe->CompleteTrans()){
			return false;
		}

		return true;

	}//End of method

} //End of class


class docmetadata extends metadata
{

	private $REL_TABLE;//Table de liaison
	private $FIELDS_MAP_REL;//champ de liaison

	//--------------------------------------------------------------------
	/*! \brief init the property for the metadataManager object.
	 *
	 * This method set the table of the metadataManager.
	 * \param $Manager(object) Object of the component(document or container) to manage metadatas.
	 * \param $type(string) Type of field to manage.
	 */
	function __construct(space $space , $type=''){
		$this->errorStack = Ranchbe::getErrorStack();
		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->logger = Ranchbe::getLogger();
		$this->space = $space;
		
		//$this->OBJECT_TABLE=$space->DOC_METADATA_TABLE;
		$this->OBJECT_TABLE = $this->space->SPACE_NAME.'_metadata';
		$this->EXTEND_TABLE = $this->space->DOC_TABLE;
		$this->REL_TABLE=$this->OBJECT_TABLE.'_rel';
		$this->FIELDS_MAP_ID = 'property_id';

		$this->FIELDS_MAP_REL = $space->CONT_FIELDS_MAP_ID;
		$this->FIELDS_MAP_DESC = 'field_description';

		if(!empty($type)){
			$this->set_type($type);
		}
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get all metadatas of the document of the container
	 * Return a array with properties of the metadatas
	 */
	function GetMetadataLinked($params=array(), $container_id){
		$params['exact_find'][$this->FIELDS_MAP_REL] = $container_id;
		$metadatasTable = $this->OBJECT_TABLE;
		$linkTable  = $this->REL_TABLE;

		$this->GetQueryOptions($params);
		//Junction of tables doctypes and link_table
		$query = "SELECT $this->selectClose FROM $linkTable as l JOIN $metadatasTable as m
              ON l.field_name = m.field_name
              $this->whereClose
              $this->orderClose
              ";
              if(!$links = $this->dbranchbe->Execute($query)){
              	$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
              	return false;
              }else{
              	$links = $links->GetArray(); //TODO: transform object result in array
              	foreach ($links as $key => $value)
              	$links[$key]['object_class'] = 'metadata'; //Add the type of object to each key
              	return $links;
              }
              return false;
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Link a metadata to the document of the container
	 *
	 \param $field_name(string) Name of the metadata.
	 */
	function LinkMetadata($field_name, $container_id){
		$this->dbranchbe->StartTrans();
		$id = $this->dbranchbe->GenID($this->REL_TABLE.'_seq', 1); //Increment the sequence number
		$data['link_id'] = $id;
		$data[$this->FIELDS_MAP_REL] = $container_id;
		$data['field_name'] = $field_name;
		if(!$this->dbranchbe->AutoExecute( $this->REL_TABLE , $data , 'INSERT'))
		$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, $this->REL_TABLE)), $this->dbranchbe->ErrorMsg());
		if ($this->dbranchbe->CompleteTrans() === false){
			return false;
		}
		return $id;
	}//End of method

	//----------------------------------------------------------

	/*!\brief
	 * Suppress metadata linked to documents of the container
	 *
	 \param $link_id(integer) Primary key of the link.
	 */
	function SuppressMetadaLink($link_id){
		if (empty($link_id)){
			$this->errorStack->push(ERROR, 'Fatal', array(), 'none selected link' );
			return false;}
			$query = "DELETE FROM $this->REL_TABLE WHERE link_id = '$link_id'";
			//Update the object data
			if ( $this->dbranchbe->Execute($query) === false){
				$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
				return false;
			}
			return true;
	}//End of method

} //End of class

//==============================================================
class contmetadata extends metadata{

	/*! \brief init the property for the metadataManager object.
	 *
	 * This method set the table of the metadataManager.
	 * \param $Manager(object) Object of the component(document or container) to manage metadatas.
	 * \param $type(string) Type of field to manage.
	 */
	function __construct(space $space , $type=''){
		$this->errorStack = Ranchbe::getErrorStack();
		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->logger = Ranchbe::getLogger();
		$this->space = $space;
		//$this->OBJECT_TABLE=$space->CONT_METADATA_TABLE;
		$this->OBJECT_TABLE=$space->SPACE_NAME.'_cont_metadata';
		$this->FIELDS_MAP_ID = 'property_id';
		$this->EXTEND_TABLE=$space->CONT_TABLE;

		$this->FIELDS_MAP_DESC = 'field_description';

		if(!empty($type)){
			$this->set_type($type);
		}
	}//End of method

} //End of class

?>
