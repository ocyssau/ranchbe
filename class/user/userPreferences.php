<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/* Version 1
 CREATE TABLE `user_prefs` (
 `pref_id` int(11) NOT NULL,
 `user_id` int(11) NOT NULL,
 `pref_name` varchar(32)  NOT NULL default '',
 `pref_value` varchar(128)  default NULL,
 PRIMARY KEY  (`pref_id`),
 UNIQUE KEY `UC_user_prefs` (`user_id`,`pref_name`)
 ) ENGINE=InnoDB;
 */

/* Version 2 = actual
 CREATE TABLE `user_prefs` (
 `user_id` int(11) NOT NULL,
 `css_sheet` varchar(32)  NOT NULL default 'default',
 `lang` varchar(32)  NOT NULL default 'default',
 `long_date_format` varchar(32)  NOT NULL default 'default',
 `short_date_format` varchar(32)  NOT NULL default 'default',
 `hour_format` varchar(32)  NOT NULL default 'default',
 `time_zone` varchar(32)  NOT NULL default 'default',
 `wildspace_path` varchar(128)  NOT NULL default 'default',
 `max_record` varchar(128)  NOT NULL default 'default',
 PRIMARY KEY  (`user_id`)
 ) ENGINE=InnoDB;

 ALTER TABLE `user_prefs`
 ADD CONSTRAINT `FK_user_prefs_1` FOREIGN KEY (`user_id`) REFERENCES `liveuser_perm_users` (`perm_user_id`);
 */

require_once('./class/common/basic.php');

/*! \brief manage user preferences
 */
class userPreferences extends basic
{

	protected $SPACE_NAME   = 'user_prefs';
	protected $OBJECT_TABLE  = 'user_prefs';
	protected $FIELDS_MAP_ID  = 'user_id';
	protected $TABLE_SEQ      = 'user_prefs_seq';

	protected $dbranchbe; //object
	protected $usr; //object

	private $preferences; //Array

	private $parameters; //Array

	function __construct(liveuser $user){
		$this->dbranchbe = Ranchbe::getDb();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();

		$this->usr = $user;
		$this->user_id = $this->usr->getProperty('perm_user_id');

		$this->parameters = array('css_sheet', 'lang', 'long_date_format', 'short_date_format', 'hour_format', 'time_zone', 'wildspace_path', 'max_record');
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the preference value from his name
	*
	* \param $preference_name(string) = name of pref
	*/
	function GetPreference($preference_name){
		if( isset($this->preferences[$preference_name]) )
		return $this->preferences[$preference_name];

		//$query = 'SELECT pref_value FROM '.$this->OBJECT_TABLE.' WHERE user_id = \''.$this->user_id.'\' AND pref_name = \''.$preference_name.'\'';
		$query = 'SELECT '.$preference_name.' FROM '.$this->OBJECT_TABLE.' WHERE user_id = \''.$this->user_id.'\'';
		$res = $this->dbranchbe->GetOne($query);
		if($res === false){
			$this->errorStack->push(ERROR, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			return false;
		}
		return $this->preferences[$preference_name] = $res;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Set the preference value from his name
	*
	* \param $preference_name(string)
	* \param $preference_value(mixed)
	*/
	function SetPreference($preference_name, $preference_value){
		return $this->preferences[$preference_name] = $preference_value;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get all preferences of current user
	*
	*/
	function GetPreferences(){
		if( isset($this->preferences) )
		return $this->preferences;

		$params = array(); //init var
		$params['select'] = $this->parameters;
		$params['exact_find'] = array($this->FIELDS_MAP_ID => $this->user_id);

		$res = $this->Get($this->OBJECT_TABLE , $params , 'row' , false);

		if($res === false){
			return false;
		}else if(empty($res['lang']) ){ //none records
			$vals = array_fill(0, count($this->parameters), 'default' );
			return $this->preferences = array_combine($this->parameters, $vals);
		}else{
			return $this->preferences = $res;
		}

	}//End of method

	//----------------------------------------------------------
	/*! \brief init or re-init object
	*
	*/
	function init($user=NULL){
		if(isset($this->preferences)) unset($this->preferences);
		if( !is_null($user) ){
			$this->usr = $user;
			$this->user_id = $this->usr->getid();
		}
	}//End of method

	//----------------------------------------------------------
	/*! \brief Save object in database
	*   Create object if do not exist
	*/
	function Freeze(){
		//Test if object exist
		$query = 'SELECT '.$this->FIELDS_MAP_ID.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->user_id.'\'';
		$one = $this->dbranchbe->GetOne($query);

		if( empty($one) ){ //its a new pref definition
			$this->preferences[$this->FIELDS_MAP_ID] = $this->user_id;
			if (!$this->dbranchbe->AutoExecute( $this->OBJECT_TABLE , $this->preferences , 'INSERT')){
				$this->errorStack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
				return false;
			}

		}else{ //just update pref
			if( isset($this->preferences[$this->FIELDS_MAP_ID]) ) unset($this->preferences[$this->FIELDS_MAP_ID]);
			$this->BasicUpdate( $this->preferences , $this->user_id );
		}

	}//End of method

} //End of class

?>
