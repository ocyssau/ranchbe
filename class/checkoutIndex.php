<?php

/*! \brief This class is used to retrieve informations about checkout document from the file in the wildspace.
 * The table checkout_index keep container name, space name, document name and user information about file to display to the final user.
 *
 */
class checkoutIndex{
	protected $docfile;
	protected $space;
	protected $error_stack;
	protected $dbranchbe;
	protected $usr;
	protected $OBJECT_TABLE;
	protected $SPACE_NAME;

	function __construct(docfile $docfile){
		$this->dbranchbe = Ranchbe::getDb();
		$this->usr = Ranchbe::getCurrentUser();
		$this->errorStack = Ranchbe::getErrorStack();
		$this->logger = Ranchbe::getLogger();

		$this->docfile = $docfile;
		$this->space = $docfile->space;

		$this->CurrentUserId = $this->usr->getid();

		$this->OBJECT_TABLE  = 'checkout_index';
		$this->SPACE_NAME = $this->space->SPACE_NAME;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Create entry in index table for retrieve container of a checkout file.
	 *
	 * When a file is checkOut, a record is create in the "checkout_index" table
	 * for record the file name attach to document that are checkOut.
	 * This infos are used for retrieve the file that are checkout from the list of files in the Wildspace.
	 \param $file(integer) file name of file to get
	 */
	function IndexCheckOutAdd(){
		$data['file_name']        = $this->docfile->GetProperty('file_name');
		$data['container_type']   = $this->SPACE_NAME;
		$odocument = $this->docfile->initDoc();
		$data['container_id']     = $odocument->GetProperty('container_id');
		$data['designation']      = $odocument->GetProperty('designation');
		$ocontainer = $odocument->initContainer();
		$data['container_number'] = $ocontainer->GetName();
		$data['document_id']      = $this->docfile->GetProperty('document_id');
		$data['check_out_by']     = $this->CurrentUserId;

		if (!$this->dbranchbe->AutoExecute( "checkout_index" , $data , 'INSERT')){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, "checkout_index")), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Suppress entry in checkout index table.
	 *
	 * When a file is checkOut, a record is create in the "checkout_index" table
	 * for record the file name attach to document that are checkOut.
	 * This infos are used for retrieve the file that are checkout from the list of files in the Wildspace.
	 * This record is suppressed when checkin is submit.

	 \param $file(integer) file name of file to get
	 */
	function IndexCheckOutDel(){
		$file_name = $this->docfile->GetProperty('file_name');
		$query = 'DELETE FROM checkout_index WHERE file_name = \''.$file_name.'\' AND container_type = \''.$this->SPACE_NAME.'\'';
		//Execute the query
		if (!$this->dbranchbe->Execute($query)){
			$this->errorStack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
		}
		return true;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Get infos on stored document linked to "$file".
	 *
	 \param $file(integer) file name of file to get
	 */
	static function GetCheckoutIndex($file){
		$dbranchbe = Ranchbe::getDb();
		$query = 'SELECT * FROM checkout_index WHERE file_name = \''.$file.'\'';
		if(!$infos = $dbranchbe->GetRow($query)){
			return false;
		}
		else {
			return $infos;
		}
	}//End of method

}//End of class

?>
