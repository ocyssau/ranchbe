<?php

/**
 * TODO: implementer un iterateur.
 *
 * TODO: revoir la definition de this::_current
 *
 */


require_once('./class/common/document.php');
require_once('./class/common/doclink.php');

/**
 *
 * Import a list of document and create relations from a xml file
 * @author o_cyssau
 *
 */
class Document_Import_RbXmlTree
{

	/**
	 * @var string
	 */
	protected $_xmlfile;

	/**
	 *
	 * @var DOMDocument
	 */
	protected $_domdocument;

	/**
	 *
	 * @var DOMElement
	 */
	protected $_rootElement;

	/**
	 *
	 * @var document
	 */
	protected $_rootDocument;

	/**
	 *
	 * @var doclink
	 */
	protected $_rootDocrel;

	/**
	 * Current element serialized in array
	 *
	 * @var array
	 */
	protected $_current;

	/**
	 *
	 */
	public $errors = array();


	/**
	 *
	 * Stack of all partNumber in treatment order
	 * @var array
	 */
	protected $_partNumberStack = array();


	/**
	 *
	 * @param string $xmlfile
	 */
	public function __construct( $xmlfile, $space_name ){
		$this->_space = new space($space_name);
		$this->_xmlfile = $xmlfile;
		$this->getDomxml();
	}

	/**
	 *
	 * Get the Domxml base object from the xml file
	 * @param string $xmlfile
	 * @return DOMDocument
	 * @throws Exception
	 */
	public function getDomxml()
	{
		if( !$this->_domdocument ){
			if( !is_file($this->_xmlfile) ){
				throw new Exception($this->_xmlfile . ' is not a file');
			}
			$this->_domdocument = new DOMDocument();
			$this->_domdocument->load( $this->_xmlfile );
		}
		$this->_rootElement = $this->_domdocument->documentElement->getElementsByTagName('Product')->item(0);
		return $this->_domdocument;
	}

	/**
	 *
	 * Enter description here ...
	 * @return DOMElement
	 */
	public function getRootNode()
	{
		return $this->_rootElement;
	}

	/**
	 *
	 * Run the importation
	 * @return array
	 */
	public function run()
	{
		$childsCollection = $this->_rootElement->getElementsByTagName('Product');
		//$childsCollection = $this->_rootElement->childNodes;

		$success = array();
		for($i=0; $i < $childsCollection->length; $i++  ){
			try{
				$item = $childsCollection->item($i);

				if( $item->nodeName != 'Product'){
					continue;
				}

				$partNumber = $item->getAttribute('PartNumber');

				if( in_array($partNumber, $this->_partNumberStack) ){
					$this->_partNumberStack[] = $partNumber;
					continue;
				}

				$this->_importElement( $item );
				$this->_partNumberStack[] = $partNumber;
				$success[] = $this->_current;
			}catch(Exception $e){
				$this->errors[] = $e->getMessage();
			}
		}
		return $success;
	}

	/**
	 *
	 * Enter description here ...
	 * @throws Exception
	 * @return document
	 */
	protected function _getRootDocument(){
		if(!$this->_rootDocument){
			$document = new document($this->_space);
			$father_ds = $this->_rootElement->getAttribute('PartNumber');
			$rootDocumentId = $document->DocumentExist($father_ds);
			if($rootDocumentId < 1){
				throw new Exception('Bad DS number or DS is not recorded in Ranchbe');
			}
			$this->_rootDocument = new document($this->_space, $rootDocumentId);
			$this->_rootDocrel = new doclink( $this->_rootDocument );
		}
		return $this->_rootDocument;
	}


	/**
	 * Translate RbXml file to array
	 *
	 * @return array
	 */
	public function toArray(){
		$childsCollection = $this->_rootElement->getElementsByTagName('Product');
		//$childsCollection = $this->_rootElement->childNodes;
		$out = array();
		for($i=0; $i < $childsCollection->length; $i++  ){
			$item = $childsCollection->item($i);
			if( $item->nodeName == 'Product'){
				$out[] = $this->elementToArray( $item );
			}
		}
		return $out;
	}


	/**
	 * Translate RbXml element to array
	 *
	 * @param DOMElement $element
	 * @return array
	 */
	public static function elementToArray( DOMElement &$element ){

		$aMaterials = array();
		$cMaterials 	= $element->getElementsByTagName('Materials')->childNodes;
		if($cMaterials->length > 0){
			for($i=0; $item = $cMaterials->item($i); $cMaterials->length  ){
				$aMaterials[] = $item->getAttribute('Name');
			}
		}

		$RelativePosition = $element->getElementsByTagName('RelativePosition')->item(0);
		if( $RelativePosition ){
			$aPosition = array(
				'Ux'=>$RelativePosition->getAttribute('Ux'),
				'Uy'=>$RelativePosition->getAttribute('Uy'),
				'Uz'=>$RelativePosition->getAttribute('Uz'),
				'Vx'=>$RelativePosition->getAttribute('Vx'),
				'Vy'=>$RelativePosition->getAttribute('Vy'),
				'Vz'=>$RelativePosition->getAttribute('Vz'),
				'Wx'=>$RelativePosition->getAttribute('Wx'),
				'Wy'=>$RelativePosition->getAttribute('Wy'),
				'Wz'=>$RelativePosition->getAttribute('Wz'),
				'Tx'=>$RelativePosition->getAttribute('Tx'),
				'Ty'=>$RelativePosition->getAttribute('Ty'),
				'Tz'=>$RelativePosition->getAttribute('Tz'),
			);
		}else{
			$aPosition = array();
		}

		$weight 	= '';
		$volume 	= '';
		$wetArea 	= '';
		$Analyse = $element->getElementsByTagName('Analyse')->item(0);
		if( $Analyse ){
			$weight 	= $Analyse->getAttribute('Mass');
			$volume 	= $Analyse->getAttribute('Volume');
			$wetArea 	= $Analyse->getAttribute('WetArea');
		}

		$out = array(
			'document_name' => $element->getAttribute('Name'),
			'partNumber'=> $element->getAttribute('PartNumber'),
			'weight' 	=> $weight,
			'volume' 	=> $volume,
			'wetArea' 	=> $wetArea,
			'materials' => implode('#', $aMaterials),
			'position' 	=> implode('#', $aPosition),
		);

		return $out;
	}


	/**
	 *
	 * Enter description here ...
	 * @param DOMElement $element
	 * @return void
	 * @throws Exception
	 */
	protected function _importElement( DOMElement &$element ){
		$partNumber = $element->getAttribute('PartNumber');
		$father_ds	= $this->_getRootDocument()->GetProperty('document_number');

		$id = $this->_getRootDocument()->DocumentExist($partNumber);

		if($id > 0){
			$document = new document($this->_space, $id);
			$this->_current = self::elementToArray($element);
			$this->_current['father_ds'] = $father_ds;
			if( !$document->UpdateDocument( $this->_current ) ){
				throw new Exception( sprintf('Update failed for document %s', $partNumber) );
			}
			//Set the parent
			$this->_rootDocrel->AddSon( $document->GetId() );
		}else{
			throw new Exception( sprintf('The document %s is not existing', $partNumber) );
		}
	}

}

