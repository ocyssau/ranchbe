<?php

/**
 * 
 * @author o_cyssau
 *
 */
class Sys_Folder extends SplFileInfo {
	
	/**
	 * 
	 * @var DirectoryIterator
	 */
	protected $_iterator;
	
	public function getIterator(){
		if ( !$this->_iterator ){
			$this->_iterator = new DirectoryIterator( $this->getPathname() );
		}
		
		return $this->_iterator;
	}
	
}

