<?php

namespace ProcessDef\PROCWF001v10;

use Ranchbe\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;

/**
 * A class for activity act01
 *
 * @package WF001v10
 * @generated 2015-12-30T01:41:21+0100
 * @author
 */
class ActAlivrer extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
        $document = $this->workflow->document;
        $instance = $this->workflow->instance;
        
        
        $document->lifeStage = 'A_Livrer'; //Update state of the document
        
        $instanceId = $instance->getInstanceId();
        $instance->set('a_livrer_user', $user); //Set instance property to record the user wich submit request
        $instance->set('previousActivity','a_livrer'); //Set la propriete previousActivity
        $instance->set('previousUser', $user); //Set la propriete previousUser
        
        $subject = $document->GetProperty('document_number') . ' a livrer';
        $body = 'le plan ' .$document->GetProperty('document_number').'-'.$document->GetProperty('document_indice_id').' est bon a livrer<br>';
        $instance->setMessageToNextUsers($subject, $body);
        
        return true;
    }
}

