<?php

namespace ProcessDef\PartsLivrablev30;

use Ranchbe\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;

/**
 * A class for activity act01
 *
 * @package WF001v10
 * @generated 2015-12-30T01:41:21+0100
 * @author
 */
class ActEnd extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
        
        $document = $this->workflow->document;
        $instance = $this->workflow->instance;
        
        //For be able to create new activities from a document, we must suppress link between the cow
        //and the instance. This activity must be interactive and only "end" type.
        $document->UnLinkInstanceToDocument(); //Suppress link between document and instance
        return true;
    }
}


