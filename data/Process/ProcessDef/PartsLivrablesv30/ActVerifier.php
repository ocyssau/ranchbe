<?php

namespace ProcessDef\PartsLivrablev30;

use Ranchbe\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;

/**
 *
 * @package WF001v10
 * @generated 2015-12-30T01:41:21+0100
 * @author
 */
class ActVerifier extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
        
        $document = $this->workflow->document;
        $instance = $this->workflow->instance;
        
        if (isset($_REQUEST['approve'])) {
        	$document->LockDocument(5); //Lock document access with special code
        	$document->ChangeState('approuve'); //Update state of the document
        	$instance->setNextActivity('A_Livrer');
        	$instance->complete();
        	//record property to statistic
        	$instance->set('verifier_user',$user);
        	$instance->set('check','approve');
        }
        
        if (isset($_REQUEST['reject'])) {
        	$document->LockDocument(0); //Lock document access with special code
        	$document->ChangeState('rejete'); //Update state of the document
        	$instance->setNextActivity('end');
        	$instance->complete();
        	$instance->set('verifier_user',$user);
        	$instance->set('check','reject');
        
        	//Send the error_note and error_code to user wich request the verification
        	//include_once './class/messu/messulib.php';
        	$subject = $Documentinfos['document_number'] . ' rejet�';
        	$body = 'le plan ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' est rejet� pour les raisons suivantes :<br>';
        	if( is_array($_REQUEST['error_code']) )
        		foreach($_REQUEST['error_code'] as $error){
        		$body .= '<b> '. $error .' </b><br>';
        	}
        	//record errors in property to statistic
        	$instance->set('error_code',$_REQUEST['error_code']);
        	if(!empty($_REQUEST['error_note']))
        		$body .= '<b>Notes</b> : <br>' . trim($_REQUEST['error_note']);
        	$priority = 1;
        	//record property to statistic
        	$instance->set('error_note',trim($_REQUEST['error_note']));
        
        	//Get the previous user from the properties??
        	$to = $instance->getOwner();
        	$instance->sendMessage($to, $subject, $body);
        }
        
        $instance->set('previousActivity','verifier');//Set la propriete previousActivity
        $instance->set('previousUser',$user);//Set la propriete previousUser
        
        return true;
    }
}


