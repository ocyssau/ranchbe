<?php

namespace ProcessDef\PartsLivrablev30;

use Ranchbe\Service\Workflow\Prototype\ActivityTrigger;
use Application\Model\Exception;

/**
 *
 * @package WF001v10
 * @generated 2015-12-30T01:41:21+0100
 * @author
 */
class ActLivrer extends ActivityTrigger
{

    public function trigger()
    {
        parent::trigger();
        
        $document = $this->workflow->document;
        $instance = $this->workflow->instance;
        
        $document->LockDocument(10); //Lock document access with special code
        $document->ChangeState('Livrer'); //Update state of the document
        
        $livraison_deposit_dir = '/DATA6/livraison/a_livrer/ranchbe';
        $document->CopyAssociatedFiles($livraison_deposit_dir , true);
        
        //Corrige bug primes qui bloque les import de fichier dont le mtime du xml est dans la meme seconde
        //que le mtime de fichier CATIA
        $afiles = $document->GetAssociatedFiles();
        $mainfiles = $livraison_deposit_dir .'/'. $afiles[0]['file_name'];
        if (is_file("$mainfiles" . ".qcseal") ){
        	$date_mainf = filemtime("$mainfiles");
        	touch("$mainfiles" . ".qcseal", $date_mainf + 2);
        }
        
        $instance->set('previousActivity','livrer');//Set la propriete previousActivity
        $instance->set('previousUser',$user);//Set la propriete previousUser
        
        return true;
    }
}
