<?php
namespace ProcessDef\PartsLivrablev30;

use Ranchbe\Service\Workflow\Prototype\ActivityTrigger;

/**
 * A class for activity act01
 *
 * @package WF001v10
 *          @generated 2015-12-30T01:41:21+0100
 * @author
 *
 */
class ActAbort extends ActivityTrigger
{
	public function trigger()
	{
		parent::trigger();

		$document = $this->workflow->document;
		$instance = $this->workflow->instance;

		echo "Cette action n'est plus possbile<br>";
		die();

		if ( $docflow->resetProcess() ) {
			$document->errorStack->push(ERROR, 'Info', array(
				'element' => $document->getNumber()
			), 'le process du document %element% a ete annule');
			return true;
		}
		else {
			return false;
		}

		return true;
	}
}


