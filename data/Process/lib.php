<?php

/**
 *
 * Verifie les refus
 * @param document 			$document
 * @return string|false		Message to display
 */
function Activities_Lib_CheckRt($document){
	$msg = true;
	require_once './class/Rt.php';
	$Rt = new Rt();

	$document_version = $document->GetProperty('document_version');
	$doctype_id = $document->GetProperty('doctype_id');
	if($doctype_id == Rt::$_doctype_id){
		$msg .= '<b>';
		$msg .= 'Vous ne pouvez pas appliquer cette activit� a un refus technique.<br />';
		$msg .= 'Veuillez lancer l\'activit� rt_validate pour valider un RT <br />';
		$msg .= '</b>';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	$appliedStatus 	= Rt::$_appliedStatus;
	$confirmedStatus 	= Rt::$_confirmedStatus;

	$filter = '';
	$filter .= ' rt_status != \''.$appliedStatus.'\'';
	$filter .= ' AND rt_status != \''.$confirmedStatus.'\'';
	$filter .= ' AND rt_access_code < 15';
	$filter .= ' AND from_version <= ' . $document_version;
	$select = array();
	$RtRs = $Rt->GetRtApplyToDocument($document, false, true, $select, $filter);
	
	//var_dump($appliedStatus, $confirmedStatus, $RtRs->RowCount());die;
	//var_dump($appliedStatus, $confirmedStatus, $RtRs->RowCount());die;

	$li = '';
	if($RtRs){
		while($row = $RtRs->FetchRow() ){
			$aid = null;
			$rtId = $row['rt_id'];
			$href = '/rbsier/document/manager/multichangestate?step=step1&checked[]='.$rtId.'&activityId='.$aid;

			if($row['rt_status'] != 'rt_applied' && $row['rt_status'] != 'rt_confirmed' ){
				$li .= '<li>' . $row['rt_number'];
				$li .= ' <a href="'.$href.'"> Vous devriez appliquer la tache rt_validate sur ce refus et relancer </a>';
				$li .= '</li>';
			}
		}
		if($li){
			$msg = $document->GetNumber() . ' : <br/>';
			$count = $RtRs->RowCount();
			$msg .= "il y a $count refus applicable a ce document:<br />";
			$msg .= $li;
			$msg .= '<ul>';
			$msg .= '</ul>';
			$msg .= 'Vous devez les valider avant des lancer ce process<br />';
			$document->errorStack->push(ERROR, 'Error', array(), $msg);
			return false;
		}
	}
	return $msg;
}

/**
 *
 * Verifie la presence et les dates du qcseal
 * @param document 			$document
 * @return string|false		Message to display
 */
function Activities_Lib_CheckQcseal($document){
	$msg = true;
	//Recupere les fichiers associes au document
	$afiles = $document->GetAssociatedFiles();
	$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];


	if (!($afiles[0]['file_extension'] == '.CATPart' ||
	$afiles[0]['file_extension'] == '.CATProduct' ||
	$afiles[0]['file_extension'] == '.CATDrawing'))
	{
		return true;
	}


	//Verifie la presence du qcseal
	if (!is_file("$mainfiles" . ".qcseal") ){
		/*print "<b>INFOS: </b><br/>";
		 print "<b>le fichier .qseal est abscent<b/><br/>";
		 print "Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache<br/>";
		 print '<form><input type="button" onclick="window.close()" value="Close"></form><br />';
		 */
		$msg = 'Le fichier .qseal est abscent';
		$msg .= 'Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	//Verifie la concordance des dates du qcseal
	$date_xmlf = filemtime("$mainfiles" .'.qcseal');
	$date_mainf = filemtime("$mainfiles");
	$time_shift = $date_xmlf - $date_mainf;
	if ( $time_shift < -2 ) {
		$msg = 'Le fichier qchecker nest pas a jour';
		$msg .= 'Vous devez regenerer le fichier qchecker pour pouvoir executer cette tache';
		$msg .= '<i>mtime du fichier qchecker</i>: '. $date_xmlf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
		$msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	//TODO : Verifier les erreurs dans le fichiers qchecker

	return $msg;
}

/**
 *
 * Verifie la presence et les dates du qcseal
 * @param document 			$document
 * @return string|false		Message to display
 */
function Activities_Lib_CheckTubex($document){
	$msg = true;
	//Recupere les fichiers associes au document
	$afiles = $document->GetAssociatedFiles();
	$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

	if ($afiles[0]['file_extension'] == '.CATProduct')
	{
		$tubex_files[] = $afiles[0]['file_path'] .'/'. $afiles[0]['file_root_name']. '.tubexc.xml';
		$tubex_files[] = $afiles[0]['file_path'] .'/'. $afiles[0]['file_root_name']. '.tubex.xml';

		foreach($tubex_files as $tubex_file){
			//Verifie la presence du tubex
			if ( is_file($tubex_file) ){
				//Verifie la concordance des dates
				$date_tubex = filemtime($tubex_file);
				$date_mainf = filemtime($mainfiles);
				$time_shift = $date_tubex - $date_mainf;
				if ( $time_shift < -30 ) {
					$msg = 'Le fichier '.$tubex_file.' nest pas a jour';
					$msg .= 'Vous devez regenerer le fichier tubex pour pouvoir executer cette tache';
					$msg .= '<i>mtime du fichier tubex</i>: '. $date_tubex . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
					$msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
					$document->errorStack->push(ERROR, 'Error', array(), $msg);
					return false;
				}
			}
		}
	}

	return $msg;
}



/**
 *
 * Verifie la presence et les dates du pdf
 * @param document 			$document
 * @param boolean 			$force	If true, force check of pdf, else check onlty if present.
 * @return string|false		Message to display
 */
function Activities_Lib_CheckPdf($document, $force=true){
	$msg = true;

	//Recupere les fichiers associes au document
	$afiles = $document->GetAssociatedFiles();
	$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

	if ($afiles[0]['file_extension'] != '.CATDrawing')
	{
		return true;
	}

	$pdf_file = $afiles[0]['file_path'] .'/'. $afiles[0]['file_root_name']. '.pdf';
	//Verifie la presence du pdf
	if ( !is_file($pdf_file) ){
		if($force){
			$msg = 'Le fichier '.$pdf_file.' est abscent';
			$msg .= 'Vous devez generer le fichier .pdf et le lier a ce document pour pouvoir executer cette tache';
			$document->errorStack->push(ERROR, 'Error', array(), $msg);
			return false;
		}else{
			return true;
		}
	}

	//Verifie la concordance des dates du pdf
	$date_pdf = filemtime($pdf_file);
	$date_mainf = filemtime($mainfiles);
	$time_shift = $date_pdf - $date_mainf;
	if ( $time_shift < -30 ) {
		$msg = 'Le fichier '.$pdf_file.' nest pas a jour';
		$msg .= 'Vous devez regenerer le fichier pdf pour pouvoir executer cette tache';
		$msg .= '<i>mtime du fichier pdf</i>: '. $date_pdf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
		$msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	return $msg;
}

/**
 *
 * Valide les refus techniques
 * @param document 			$document
 * @return string|false		Message to display
 */
function Activities_Lib_RtValidate($document){
	$msg = true;
	require_once './class/Rt.php';

	//Check that document is a doctype
	$doctype_id = $document->GetProperty('doctype_id');
	if($doctype_id != Rt::$_doctype_id){
		$msg = 'This document is not a rt';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	$document->ChangeState('rt_applied');
	$document->LockDocument(10); //validate

	return $msg;
}


/**
 *
 * Confirme le rt
 * @param document 			$document
 * @return string|false		Message to display
 */
function Activities_Lib_ConfirmRt(document &$document, &$instance, $successNextActivity, $faultNextActivity){

	//$container =& $document->GetContainer();
	$space =& $document->space;

	require_once('./class/Rt.php');
	$Rt = new Rt();

	$filter = ' rt_status != \''. Rt::$_confirmedStatus .'\'';
	$filter .= ' AND rt_access_code < 15';
	$select = array();
	$rs = $Rt->GetRtApplyToDocument($document, false, true, $select, $filter);

	if(!$rs) return true;

	$i=0;
	$validTag = true;
	$reports = array();

	while( $row = $rs->FetchRow() ){
		if($row['rt_status'] == 'rt_applied'){  //only on applied rt
			$valid = $_REQUEST['rt_validation'][$row['rt_id']];
			$RtDoc = new document($space, $row['rt_id']);
			if($valid){
				$RtDoc->ChangeState('rt_confirmed');
			}else{
				$reports[] = 'Le refus ' . $row['rt_number'] . ' n\'est pas correctement appliqu� sur ce document';
				$RtDoc->LockDocument(0); //Lock document access with special code
				$RtDoc->ChangeState('rt_rejected');
				$validTag = false;
			}
		}
		$i++;
	}

	if( !$validTag ){
		$document->LockDocument(0); //Lock document access with special code
		$document->ChangeState('rejete'); //Update state of the document

		$instance->setNextActivity($faultNextActivity);
		$instance->complete();
		$instance->set('check','reject');
	}else{
		$instance->setNextActivity($successNextActivity);
		$instance->complete();
		$instance->set('check','approve');
	}

	return $reports;

}

