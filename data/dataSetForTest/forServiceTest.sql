DELETE FROM workitem_doc_files WHERE file_id=10;
DELETE FROM workitem_doc_files WHERE file_id=11;
DELETE FROM workitem_documents WHERE document_id=50;
DELETE FROM workitem_documents WHERE document_id=51;
DELETE FROM workitems WHERE workitem_id=10;
DELETE FROM projects WHERE project_id=10;

INSERT INTO `projects` (`project_id`,`project_number`,`project_description`,`project_state`,`default_process_id`,`project_indice_id`,`area_id`,`open_date`,`open_by`,`forseen_close_date`,`close_by`,`close_date`,`link_id`) VALUES (10,'TESTPROJECT','Projet de test','init',NULL,NULL,100,1407149737,1,NULL,NULL,NULL,NULL);

INSERT INTO `liveuser_area` (`area_id`, `application_id`, `area_define_name`) VALUES (100, 1, 'TESTPROJECT153da8284bdf8e9ee317b');

INSERT INTO `workitems` (`workitem_id`,`workitem_number`,`workitem_state`,`workitem_description`,`file_only`,`access_code`,`workitem_indice_id`,`project_id`,`default_process_id`,`default_file_path`,`open_date`,`open_by`,`forseen_close_date`,`close_date`,`close_by`,`container_type`,`alias_id`,`object_class`) VALUES (10,'TESTWI','init','Wi de test',0,NULL,NULL,10,NULL,'/tmp/reposit1',1407149738,1,1407150738,NULL,NULL,'workitem',NULL,'workitem');

INSERT INTO `workitem_documents` (`document_id`,`document_number`,`document_state`,`document_access_code`,`document_version`,`workitem_id`,`document_indice_id`,`instance_id`,`doctype_id`,`default_process_id`,`category_id`,`check_out_by`,`check_out_date`,`designation`,`issued_from_document`,`update_date`,`update_by`,`open_date`,`open_by`,`ci`,`type_gc`,`work_unit`,`father_ds`,`need_calcul`) VALUES (50,'DOCUMENT_TEST_1','init',0,2,10,1,NULL,35,NULL,NULL,NULL,NULL,NULL,NULL,1419118262,1,1419118262,1,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `workitem_documents` (`document_id`,`document_number`,`document_state`,`document_access_code`,`document_version`,`workitem_id`,`document_indice_id`,`instance_id`,`doctype_id`,`default_process_id`,`category_id`,`check_out_by`,`check_out_date`,`designation`,`issued_from_document`,`update_date`,`update_by`,`open_date`,`open_by`,`ci`,`type_gc`,`work_unit`,`father_ds`,`need_calcul`) VALUES (51,'DOCUMENT_TEST_2','init',0,2,10,1,NULL,35,NULL,NULL,NULL,NULL,NULL,NULL,1419155805,1,1419155805,1,NULL,NULL,NULL,NULL,NULL);

INSERT INTO `workitem_doc_files` (`file_id`,`document_id`,`file_name`,`file_path`,`file_version`,`file_access_code`,`file_root_name`,`file_extension`,`file_state`,`file_type`,`file_size`,`file_mtime`,`file_md5`,`file_checkout_by`,`file_checkout_date`,`file_open_date`,`file_open_by`,`file_update_date`,`file_update_by`) VALUES (10,50,'DOCUMENT_TEST_1_df1.txt','/home/olivier/git/rbService/rbService/data/repositTest/5496995cc0aa4',2,0,'DOCUMENT_TEST_1_df1','.txt','init','file',11,1419155805,'5ab358c7515ef15d3e31f50e651c3cf5',NULL,NULL,1419155805,1,1419155805,1);
INSERT INTO `workitem_doc_files` (`file_id`,`document_id`,`file_name`,`file_path`,`file_version`,`file_access_code`,`file_root_name`,`file_extension`,`file_state`,`file_type`,`file_size`,`file_mtime`,`file_md5`,`file_checkout_by`,`file_checkout_date`,`file_open_date`,`file_open_by`,`file_update_date`,`file_update_by`) VALUES (11,50,'DOCUMENT_TEST_1_df2.txt','/home/olivier/git/rbService/rbService/data/repositTest/5496995cc0aa4',2,0,'DOCUMENT_TEST_1_df2','.txt','init','file',11,1419155805,'5ab358c7515ef15d3e31f50e651c3cf5',NULL,NULL,1419155805,1,1419155805,1);
