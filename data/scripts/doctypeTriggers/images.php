<?php
class doctypeScript_images{

  /* Renome le document pour eviter un conflit avec le draw */
  function doc_pre_store(&$document){
    echo '<b>Image pre store scripts : </b><br />';
    $docfile =& $document->GetDocfile(0);
    if( is_object($docfile) ){
      $fsdata =& $docfile->GetFsdata();
      $document_number = $fsdata->getProperty('file_root_name').'_image';
    }else {
      $document_number = $document->GetProperty('document_number').'_image';
    }
    $document->SetDocProperty('document_number', $document_number);
    return true;
  }

}
?>
