<?php

class doctypeScript_drawing{
	
	function doc_pre_store(&$document){
		//echo "Run doctypeScript_drawing::doc_pre_store <br>";
		require_once('doctypes_scripts/doctype_script_lib.php');
		$ret1 = doctypeScript_doc_check_pdf($document);
		if( $ret1 == false ){
			$document->stopIt = true;
			return false;
		}
		return true;
	}
	
	function doc_post_store(&$document){
		//echo "Run doctypeScript_drawing::doc_post_store <br>";
		require_once('doctypes_scripts/doctype_script_lib.php');
		$ret1 = doctypeScript_doc_associate_pdf($document);
		$ret2 = doctypeScript_doc_associate_qcseal($document);
		$ret3 = doctypeScript_doc_associate(&$document, 'tif');
		$ret4 = doctypeScript_doc_associate(&$document, 'csv');
		if( $ret2 == false ) return false;
		return true;
	}
	
	function doc_pre_update(&$document){
		//echo "Run doctypeScript_drawing::doc_pre_update <br>";
		require_once('doctypes_scripts/doctype_script_lib.php');
		$ret1 = doctypeScript_doc_check_pdf($document);
		if( $ret1 == false ){
			$document->stopIt = true;
			return false;
		}
		return true;
	} //End of method
	
	function doc_post_update(&$document){
		//echo "Run doctypeScript_drawing::doc_post_update <br>";
		require_once('doctypes_scripts/doctype_script_lib.php');
		$ret1 = doctypeScript_doc_associate_pdf($document);
		$ret2 = doctypeScript_doc_associate_qcseal($document);
		$ret3 = doctypeScript_doc_associate(&$document, 'tif');
		$ret4 = doctypeScript_doc_associate(&$document, 'csv');
		if( $ret2 == false ) return false;
		return true;
	} //End of method
	
}
