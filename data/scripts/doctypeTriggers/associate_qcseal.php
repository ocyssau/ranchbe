<?php

class doctypeScript_associate_qcseal{

  //Associe le qcseal s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
  function doc_post_update(&$document){
    $docfile =& $document->GetDocfile(0); //Get the main file
    if(!is_object($docfile) ) return true; //s'il n'y a pas de fichier associ�s on sort
    $file_name = $docfile->GetProperty('file_name');
    $wildspace = $docfile->GetWildspace();
    $wildspace_path = $wildspace->GetPath();
  
    //Chemin du qcseal dans le wildspace
    $qcseal = $wildspace_path.'/'.$file_name.'.qcseal';
  
    //Verifie que le qcseal n'est pas deja attache
    $afiles = $document->GetAssociatedFiles();
    if( is_array($afiles) ){
      if( in_array( $qcseal, $afiles ) ){
        return true; //si le qcseal est deja associe on sort
      }
    }
  
    if( is_file($qcseal) ){
      $document->AssociateFile($qcseal , true); //Associe le .qcseal au document
    }
  
    return true;
  }

  //Associe le qcseal s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
  function doc_post_store(&$document){
    $docfile =& $document->GetDocfile(0); //Get the main file
    if(!is_object($docfile) ) return true; //s'il n'y a pas de fichier associ�s on sort
    $file_name = $docfile->GetProperty('file_name');
    $wildspace =& $docfile->GetWildspace();
    $wildspace_path = $wildspace->GetPath();
  
    //Chemin du qcseal dans le wildspace
    $qcseal = $wildspace_path.'/'.$file_name.'.qcseal';
  
    //Verifie que le qcseal n'est pas deja attache
    $afiles = $document->GetAssociatedFiles();
    if( is_array($afiles) ){
      if( in_array( $qcseal, $afiles ) ){
        return true; //si le qcseal est deja associe on sort
      }
    }
  
    if( is_file($qcseal) ){
      $document->AssociateFile($qcseal , true); //Associe le .qcseal au document
    }
  
    return true;
  }

}

?>
