<?php

/**
 * Associe le pdf s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
 *
 * @param $document
 * @return boolean
 *
 */
function doctypeScript_doc_associate_pdf(&$document){
	return doctypeScript_doc_associate($document, 'pdf', 'ADDROOT');
	
	/*
	$docfile =& $document->GetDocfile(0); //Get the main file

	if(!is_object($docfile) ) {
		return true; //s'il n'y a pas de fichier associ�s on sort
	}

	$fileRootName = $docfile->GetProperty('file_root_name');
	$fileName = $docfile->GetProperty('file_name');
	$wildspace = $docfile->GetWildspace();
	$wildspacePath = $wildspace->GetPath();

	//Chemin du pdf dans le wildspace
	$pdfRootName = $fileRootName . '.pdf';
	$pdfFile = $wildspacePath . '/' . $pdfRootName;

	//Verifie que le pdf n'est pas deja attache
	$afiles = $document->GetAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $pdfRootName){
				return true; //si le pdf est deja associe on sort
			}
		}
	}

	if( is_file($pdfFile) ){
		$document->AssociateFile($pdfFile , true); //Associe le .pdf au document
	}

	return true;
	*/
}

//Verifie la date du pdf en pre-store ou pre-update
function doctypeScript_doc_check_pdf(&$document){
	$docfile =& $document->GetDocfile(0); //Get the main file
	
	if( !is_object($docfile) ) {
		return true; //s'il n'y a pas de fichier associ�s on sort
	}
	
	$fileRootName = $docfile->GetProperty('file_root_name');
	$fileName = $docfile->GetProperty('file_name');
	$wildspace = $docfile->GetWildspace();
	$wildspacePath = $wildspace->GetPath();
	
	//Chemin du pdf dans le wildspace
	$pdfRootName = $fileRootName . '.pdf';
	$pdfFile = $wildspacePath  . '/' . $pdfRootName;
	$mainFile = $wildspacePath . '/' . $fileName;
	
	//var_dump($fileRootName, $fileName, $pdfRootName, $pdfFile, $mainFile);die;
	
	if( is_file($pdfFile) ){
		$date_pdf = filemtime($pdfFile);
		if( is_file($mainFile) ){
			$date_mainf = filemtime($mainFile);
		}
		else{
			$date_mainf = $docfile->GetProperty('file_mtime');
		}
		$time_shift = $date_pdf - $date_mainf;
		if ( $time_shift < -30 ) {
			$msg = 'Le fichier ' . $pdfFile . ' nest pas a jour';
			$document->errorStack->push(ERROR, 'Error', array(), $msg);
			return false;
		}
	}
	return true;
}

/**
 * Associe le fichier avec extension $extension s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
 *
 * @param $document
 * @param string $extension 	extension du fichier a associer sans le point (ex: tiff)
 * @param string $mode 			ADDROOT: build assoc file name from file root name. ex: file.ext->assoc to file.extension
 * 								ADDNAME: build assoc file name from file name. ex: file.ext->assoc to file.ext.extension
 * @return boolean
 */
function doctypeScript_doc_associate(&$document, $extension, $mode='ADDROOT'){
	$docfile =& $document->GetDocfile(0); //Get the main file
	
	if(!is_object($docfile) ) {
		return true; //s'il n'y a pas de fichier associ�s on sort
	}
	
	$fileRootName = $docfile->GetProperty('file_root_name');
	$fileName = $docfile->GetProperty('file_name');
	$wildspace = $docfile->GetWildspace();
	$wildspacePath = $wildspace->GetPath();
	
	//Chemin du fichier a associer dans le wildspace
	if($mode == 'ADDROOT'){
		$assocFileName = $fileRootName . '.' . $extension;
	}
	else{
		$assocFileName = $fileName . '.' . $extension;
	}
	$assocFullpath = $wildspacePath . '/' . $assocFileName;

	//Verifie que le fichier n'est pas deja attache
	$afiles = $document->GetAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $assocFileName){
				return true; //si le fichier est deja associe on sort
			}
		}
	}

	if( is_file($assocFullpath) ){
		$document->AssociateFile($assocFullpath , true); //Associe le fichier au document
	}

	return true;
}



/**
 * Associe le qcseal s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
 * @param $document
 * @return boolean
 *
 */
function doctypeScript_doc_associate_qcseal(&$document){
	return doctypeScript_doc_associate($document, 'qcseal', 'ADDNAME');
	
	/*
	$docfile =& $document->GetDocfile(0); //Get the main file
	
	if(!is_object($docfile) ) {
		return true; //s'il n'y a pas de fichier associ�s on sort
	}
	
	$fileRootName = $docfile->GetProperty('file_root_name');
	$fileName = $docfile->GetProperty('file_name');
	$wildspace = $docfile->GetWildspace();
	$wildspacePath = $wildspace->GetPath();
	
	$extension = '.qcseal';
	
	//Chemin du qcseal dans le wildspace
	$assocFileName = $fileName . '.' . $extension;
	$assocFullpath = $wildspacePath . '/' . $assocFileName;
	
	//Verifie que le qcseal n'est pas deja attache
	$afiles = $document->GetAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $assocFileName){
				return true; //si le qcseal est deja associe on sort
			}
		}
	}

	if( is_file($assocFullpath) ){
		$document->AssociateFile($assocFullpath , true); //Associe le .qcseal au document
	}

	return true;
	*/
}

/**
 *
 * Associe le dxf s'il existe dans le wildspace et s'il n'est pas d�ja associ�.
 * @param $document
 * @return boolean
 */
function doctypeScript_doc_associate_dxf(&$document){
	return doctypeScript_doc_associate($document, 'dxf', 'ADDROOT');
	
	/*
	$docfile =& $document->GetDocfile(0); //Get the main file

	if(!is_object($docfile) ) {
		return true; //s'il n'y a pas de fichier associ�s on sort
	}
	
	$fileRootName = $docfile->GetProperty('file_root_name');
	$fileName = $docfile->GetProperty('file_name');
	$wildspace = $docfile->GetWildspace();
	$wildspacePath = $wildspace->GetPath();
	
	//Chemin du dxf dans le wildspace
	$dxfRootName = $fileRootName . '.dxf';
	$dxfFile = $wildspacePath . '/' . $dxfRootName;
	
	//Verifie que le dxf n'est pas deja attache
	$afiles = $document->GetAssociatedFiles();
	if( is_array($afiles) ){
		foreach($afiles as $f ){
			if ($f['file_name'] == $dxfRootName){
				return true; //si le pdf est deja associe on sort
			}
		}
	}
	
	if( is_file($dxfFile) ){
		$document->AssociateFile($dxfFile , true); //Associe le .dxf au document
	}

	return true;
	*/
}



/**
 * Create an attachment
 *
 * @param $document
 * @param $check_date
 * @param $check_file
 * @return boolean
 */
function checkAttachment(&$document, $check_date=true, $check_file=false){
	//to doing this scripts valid for pre-update and pre-store
	if(!isset($fsdata)){
		$mainfile =& $document->GetDocfile(0);
		require_once('./class/common/fsdata.php');
		$fsdata = new fsdata($document->WILDSPACE.'/'.$mainfile->GetProperty('file_name'));
	}
	$pfile = $document->WILDSPACE.'/'.$document->GetProperty('document_number').'.jpg'; //picture file path
	//If a visualisation file type is set for doctype get visu file
	$doctype =& $document->GetDoctype();
	$vfile_ext = $doctype->GetProperty('visu_file_extension'); //Get visu file extension from doctype property
	if(!empty($vfile_ext)){
		$vfile = $document->WILDSPACE.'/'.$document->GetProperty('document_number').$vfile_ext; //visufile path
	}

	$mainfile_mtime = $fsdata->GetProperty('file_mtime');
	foreach(array($vfile, $pfile) as $afile){
		if(empty($afile)) continue; //astuce to choose test or not
		if( is_file($afile) && $check_date ){
			//Compute the shift time :
			$time_shift = filemtime($afile) - $mainfile->GetProperty('file_mtime');
			//Check shift time. Attachment can not be more old of 5min that main file
			if ( $time_shift < -300 ) {
				$document->errorStack->push(ERROR, 'Warning', array('element1'=>$afile,'element2'=>$time_shift),
        'Attachment file %element1% is not todate'.'<br />'.
        'You must run catia scripts and retry'.'<br />'.
        '<i>shift time:</i> %element2% sec <br />'
        );
			}
		}else if ( $check_file ){
			$document->errorStack->push(ERROR, 'Warning', array('element1'=>$afile),
      'Attachment file %element1% is not reachable'.'<br />');
		}
		/*
		 else{
		 $document->errorStack->push(ERROR, 'Info', array('element1'=>$afile),
		 'Attachment file %element1% is not reachable'.'<br />'.
		 'You must run catia scripts and retry'
		 );
		 //return false;
		 }
		 */
	} //end foreach

	require_once('class/common/attachment.php');
	if(is_file($vfile)){
		$ovisu = new visu($document); //Construit un objet visu
		$ovisu->setExtension($vfile_ext); //D�finie l'extension du fichier de visualisation
		$ovisu->init_filepath(); //initialise le chemin vers le fichier de visualisation (ie: ./deposit_dir/AFF001/_attachments/visu/1.wrl)
		if($ovisu->attach($vfile)){ //Copie le fichier $vfile vers le path d�finis par la m�thode init_filepath
			//unlink($vfile); //Supprime le fichier d'origine pour nettoyer le wildspace
		}
	}
	if(is_file($pfile)){
		$opicture = new picture($document);
		$opicture->setExtension('.jpg');
		$opicture->init_filepath();
		if($opicture->attach($pfile)){
			$opicture->generateThumb();
			//unlink($pfile);
		}
	}
	return true;
}
