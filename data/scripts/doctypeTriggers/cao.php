<?php
require_once('doctypes_scripts/doctype_script_lib.php');

class doctypeScript_cao
{

	function doc_post_store(&$document)
	{
		$ret0 = checkAttachment($document, true, false);
		$ret1 = doctypeScript_doc_associate_pdf($document);
		$ret2 = doctypeScript_doc_associate_qcseal($document);
		$ret3 = doctypeScript_doc_associate(&$document, 'tif');
		$ret3 = doctypeScript_doc_associate(&$document, 'csv');
		if( $ret2 == false ){
			return false;
		}
		return true;
	} //End of method


	function doc_post_update(&$document)
	{
		$ret0 = checkAttachment($document, true, false);
		$ret1 = doctypeScript_doc_associate_pdf($document);
		$ret2 = doctypeScript_doc_associate_qcseal($document);
		$ret3 = doctypeScript_doc_associate(&$document, 'tif');
		$ret3 = doctypeScript_doc_associate(&$document, 'csv');
		if( $ret2 == false ){
			return false;
		}
		return true;
	} //End of method

}

