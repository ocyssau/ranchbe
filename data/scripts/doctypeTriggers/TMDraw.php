<?php

class doctypeScript_TMDraw{

  /* Renome le drawing pour eviter un conflit avec le product */
  function doc_pre_store(&$document){
    echo '<b>TMDraw pre store scripts : </b><br />';
    
    $docfile =& $document->GetDocfile(0);
    if( is_object($docfile) ){
      $fsdata =& $docfile->GetFsdata();
      $document_number = $fsdata->getProperty('file_root_name').'_drawing';
    }else {
      $document_number = $document->GetProperty('document_number').'_drawing';
    }
  
    $document->SetDocProperty('document_number', $document_number);
  
    return true;
  
  }

}
?>
