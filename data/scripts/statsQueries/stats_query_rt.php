<?php

require_once 'class/Rt.php';
require_once 'GUI/GUI.php';
require_once 'HTML/QuickForm/livesearch_select.php';

function stats_query_rt($container)
{
	$space = $container->space;
	$spaceName = $space->getName();
	$containerId = $_REQUEST['container_id'];
	$baseurl = BASEURL;
	$smarty = Ranchbe::getView();

	if( !$_REQUEST['stat_query_rt_step'] ){
		$step = 'inputparams';
	}
	else{
		$step = $_REQUEST['stat_query_rt_step'];
	}

	$form = new Stats_Query_Rt_Form_GetRtForPeriod('GetRtForPeriod', 'post', $baseurl.'/container/'.$spaceName.'/getstats');
	$form->buildForm();

	switch($step){
		case 'inputparams':
			stats_query_rt_inputparams($form, $smarty);
			die;
		case 'displayresult':
			$fromDate = $form->getSubmitValue('from_date');
			$toDate = $form->getSubmitValue('to_date');
			$pitch = (int) $form->getSubmitValue('pitch');
			$on_container_ids = $form->getSubmitValue('on_container_id');

			$period = $toDate - $fromDate;
			if($period < 0){
				$period = $fromDate - $toDate;
				$fromDate = $toDate;
				$toDate = $fromDate + $period;
			}

			if(!$pitch){
				$pitch = 3600 * 24 * 362;
			}

			if(is_array($on_container_ids)){
				foreach($on_container_ids as $container_id){
					$cfilter[] = 'container_id = '.$container_id;
				}
				$cfilter = implode(' OR ', $cfilter);
				$cfilter = '('.$cfilter.')';
			}
			$header =  '<h1>For period from '.formatdate($fromDate, 'short').' to '.formatdate($toDate, 'short').'<br />';
			$header .=  '</h1><br />';

			$graphs[] = array('text' => $header);
			$graphs[] = _stats_query_rt_getType($fromDate, $toDate, $pitch, $cfilter);
			foreach(_stats_query_rt_getQualityRatio($fromDate, $toDate, $pitch, $cfilter) as $graph){
				$graphs[] = $graph;
			}
			return $graphs;
	}
}

/**
 * Build form for input parameters
 *
 */
function stats_query_rt_inputparams(Stats_Query_Rt_Form_GetRtForPeriod $form, $smarty)
{
	// Try to validate the form
	if ( !$form->validate() ) {
		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('layouts/ranchbe.tpl');
		return;
	}
}

/**
 *
 * @param unknown_type $fromDate
 * @param unknown_type $toDate
 * @param unknown_type $pitch
 * @param unknown_type $cfilter
 * @return multitype:string multitype:string
 */
function _stats_query_rt_getQualityRatio($fromDate, $toDate, $pitch, $cfilter)
{
	$baseurl = BASEURL;
	$dbranchbe=Ranchbe::getDb();

	$state = 'Livrer';

	//Count rt on period
	$view_name = 'view_rt';
	$sqlrt = 'SELECT COUNT(DISTINCT rt_id) FROM ' . $view_name;
	$sqlrt .= ' WHERE rt_open_date > ?';
	$sqlrt .= ' AND rt_open_date < ?';
	if($cfilter){
		$sqlrt .= ' AND ' . $cfilter;
	}
	$stmtrt = $dbranchbe->Prepare($sqlrt);

	//Select documents delivred on period
	$sqldelivred = 'SELECT COUNT(DISTINCT inst_name) FROM view_instances_activities';
	$sqldelivred .= ' WHERE act_name LIKE ?';
	$sqldelivred .= ' AND inst_start > ?';
	$sqldelivred .= ' AND inst_start < ?';
	if($cfilter){
		$sqldelivred .= ' AND ' . $cfilter;
	}
	$stmtdelivred = $dbranchbe->Prepare($sqldelivred);

	$ratiotext = '<ul>';
	$delivredtext = '<ul>';

	$sampleStart = $fromDate; //date de debut de lechantillon
	$sampleEnd = 0; //date de fin de lechantillon
	$i=0;
	while($sampleEnd < $toDate){
		$sampleEnd = $sampleStart + $pitch;

		$countRt[$i] = $dbranchbe->GetOne($stmtrt, array($sampleStart, $sampleEnd));
		$countDelivred[$i] = $dbranchbe->GetOne($stmtdelivred, array($state, $sampleStart, $sampleEnd));
		if($countDelivred[$i] > 0){
			$ratios[$i] = 100 - ( ($countRt[$i] / $countDelivred[$i]) * 100 );
		}
		else{
			$ratios[$i] = 0;
		}
		$legends[$i] = formatdate($sampleStart, 'short');

		$ratiotext .= '<li>'. $legends[$i] .' : '. round($ratios[$i], 2) .'%</li>';
		$delivredtext .= '<li>'. $legends[$i] .' : '. $countDelivred[$i] .'</li>';
		$rttext .= '<li>'. $legends[$i] .' : '. $countRt[$i] .'</li>';
		$typtext .= '<li>'. $legends[$i] .' : '. $countRt[$i] .'</li>';

		$sampleStart = $sampleEnd;
		$i++;
	}
	$ratiotext .= '</ul>';
	$delivredtext .= '</ul>';

	$totalRt = $dbranchbe->GetOne($stmtrt, array($fromDate, $toDate));
	$totalDelivred = $dbranchbe->GetOne($stmtdelivred, array($state, $fromDate, $toDate));

	//Ratio graph
	$header =  '<h1>Quality ratio<br /></h2><br />';
	$graphs[] = array('text' => $header);
	$title = urlencode ('Ratio delivred/rt'); //titre du graph
	$values = urlencode(serialize($ratios));
	$legends = urlencode(serialize($legends));
	$graphs[] = array(
			'image' => "<img src='$baseurl/inc/graphs/graphBar.php?title=$title&values=$values&legend=$legends' alt='no graph'/>",
			'text' => $ratiotext
	);


	//Delivred graph
	$header =  '<h1>Delivred count(Total=' . $totalDelivred . ')<br /></h2><br />';
	$graphs[] = array('text' => $header);
	$title = urlencode ('Delivred count'); //titre du graph
	$values = urlencode(serialize($countDelivred));
	$graphs[] = array(
			'image' => "<img src='$baseurl/inc/graphs/graphBar.php?title=$title&values=$values&legend=$legends' alt='no graph'/>",
			'text' => $delivredtext
	);

	return $graphs;
}

/**
 *
 * @param unknown_type $fromDate
 * @param unknown_type $toDate
 * @param unknown_type $pitch
 * @param unknown_type $cfilter
 * @return multitype:string
 */
function _stats_query_rt_getType($fromDate, $toDate, $pitch, $cfilter)
{
	$dbranchbe=Ranchbe::getDb();
	$baseurl=BASEURL;

	/*Select type on period*/
	$view_name = 'view_rt';
	$sqltyp = 'SELECT DISTINCT rt_id, rt_reason FROM ' . $view_name;
	$sqltyp .= ' WHERE rt_open_date > ?';
	$sqltyp .= ' AND rt_open_date < ?';
	if($cfilter){
		$sqltyp .= ' AND ' . $cfilter;
	}
	$stmttyp = $dbranchbe->Prepare($sqltyp);

	/*Get list of rt descrptions*/
	$Rt = new Rt();
	$codeDesignations = array();
	$all = $Rt->GetAll();
	if($all){
		foreach($Rt->GetAll() as $c){
			$codeDesignations[$c['code']] = $c['description'];
		}
	}

	$sampleStart = $fromDate; //date de debut de lechantillon
	$sampleEnd = 0; //date de fin de lechantillon
	$dispatch = array();
	$total = 0;
	$totalEmpty = 0;
	$i=0;
	while($sampleEnd < $toDate){
		$sampleEnd = $sampleStart + $pitch;
		$rs = $dbranchbe->Execute($stmttyp, array($sampleStart, $sampleEnd));
		if($rs==false){
			continue;
		}
		while( $row = $rs->FetchRow() ){
			$reasons = json_decode($row['rt_reason']);
			foreach($reasons as $reason){
				if($reason){
					$dispatch[$reason] = $dispatch[$reason] + 1;
					$total++;
				}
				else{
					$totalEmpty++;
				}
			}
		}
		$sampleStart = $sampleEnd;
		$i++;
	}

	$inText = '<ul>';
	foreach($dispatch as $key=>$value){
		$inText .= '<li>'. $key .' : '. $value . ' (' . $codeDesignations[$key] . ') </li>';
	}
	$inText .= '<li>Total des refus: ' . $total . '</li>';
	$inText .= '<li>Total des refus vides : ' . $totalEmpty . '</li>';
	$inText .= '</ul><br />';

	//Ratio graph
	$header =  '<h1><a href=./RtCodes.php>Types of RTs</a><br /></h2><br />';
	$inText = $header . $inText;
	$title = urlencode ('Types of RTs'); //titre du graph
	$values = urlencode( serialize( array_values($dispatch) ) );
	$legends = urlencode( serialize( array_keys($dispatch) ) );
	return array(
			'image' => "<img src='$baseurl/inc/graphs/graphBar.php?title=$title&values=$values&legend=$legends' alt='no graph'/>",
			'text' => $inText
	);
}

/**
 *
 * @author olivier
 *
 */
class Stats_Query_Rt_Form_GetRtForPeriod extends HTML_QuickForm
{
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Visualiser les refus sur la période');

		$date_params = array(
				'field_name' => 'from_date',
				'default_value' => time() - ( 3600 * 24 * 365),
				'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$date_params = array(
				'field_name' => 'to_date',
				'default_value' => time(),
				'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$byDay = 3600 * 24;
		$byMonth = $byDay * 31;
		$byTrimestre = $byMonth * 3;
		$bySemestre = $byMonth * 6;
		$byYear = $byDay * 365;
		$list = array($byDay=>'par jour', $byMonth=>'par mois', $byTrimestre=>'par trimestre', $bySemestre=>'par semestre', $byYear=>'par an');
		$params = array(
				'field_name'=>'pitch',
				'field_description'=>'Par periode de',
				'default_value'=>$byMonth,
				'field_multiple'=>false,
				'return_name'=>false,
				'field_required'=>true,
				'adv_select'=>false,
				'display_both'=>false,
				'disabled'=>false,
				'field_id'=>'pitch',
		);
		construct_select($list, $params, $this);

		$params = array(
				'field_name'=>'on_container_id',
				'field_description'=>'For container, or let empty for all',
				'default_value'=>$_REQUEST['container_id'],
				'field_multiple'=>true,
				'return_name'=>false,
				'field_required'=>false,
				'adv_select'=>true,
				'display_both'=>false,
				'disabled'=>false,
				'field_id'=>'container',
		);

		construct_select_container($params, $this, container::_factory($_REQUEST['space']) );

		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('hidden', 'stat_query_rt_step', 'displayresult');
		$this->addElement('hidden', 'container_id', $_REQUEST['container_id']);
		$this->addElement('hidden', 'space', $_REQUEST['space']);
		foreach($_REQUEST['statistics_queries'] as $sq){
			$this->addElement('hidden', 'statistics_queries[]', $sq);
		}

		$this->addElement('submit', 'next', 'Next >>');
	}
}
