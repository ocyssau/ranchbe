<?php

/**
 * @param string $query
 */
function count_query($query){
	$dbranchbe = Ranchbe::getDb();

	if(!$Rset = $dbranchbe->Execute($query)){
		print 'error on query: '.$dbranchbe->ErrorMsg().'<br />'.$query;
		return false;
	}
	else{
		return $Rset->RecordCount();
	}
} //End of function

/**
 * @param string $type_id
 * @return string|unknown
 */
function typeName($type_id='0')
{
	$dbranchbe = Ranchbe::getDb();

	$query = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$type_id'
            ";

	if(!$number = $dbranchbe->GetOne($query)){
		return 'undefined';
	}
	else{
		return $number;
	}
} //End of function

/**
 * @param string $indice_id
 * @return string|unknown
 */
function indiceName($indice_id='0')
{
	$dbranchbe = Ranchbe::getDb();
	
	$query = "SELECT indice_value FROM document_indice WHERE document_indice_id = '$indice_id'";

	if(!$number = $dbranchbe->GetOne($query)){
		return 'undefined';
	}
	else{
		return $number;
	}
} //End of function

/**
 * @param string $category_id
 * @return string|unknown
 */
function categoryName($category_id='0')
{
	$dbranchbe = Ranchbe::getDb();
	global $Manager;

	$TABLE = $Manager->SPACE_NAME.'_categories';

	$query = "SELECT category_number FROM $TABLE WHERE category_id = '$category_id'";

	if(!$number = $dbranchbe->GetOne($query)){
		return 'undefined';
	}
	else{
		return $number;
	}
} //End of function

