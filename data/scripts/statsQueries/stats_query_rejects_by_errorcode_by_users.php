<?php

require_once 'GUI/GUI.php';
require_once 'HTML/QuickForm/livesearch_select.php';

class Stats_Query_rejects_by_errorcode_Form extends HTML_QuickForm
{

	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Rejets sur la periode');

		$date_params = array(
				'field_name' => 'from_date',
				'default_value' => time() - ( 3600 * 24 * 365),
				'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$date_params = array(
				'field_name' => 'to_date',
				'default_value' => time(),
				'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$byDay = 3600 * 24;
		$byMonth = $byDay * 31;
		$byTrimestre = $byMonth * 3;
		$bySemestre = $byMonth * 6;
		$byYear = $byDay * 365;
		$list = array($byDay=>'par jour', $byMonth=>'par mois', $byTrimestre=>'par trimestre', $bySemestre=>'par semestre', $byYear=>'par an');
		$params = array(
				'field_name'=>'pitch',
				'field_description'=>'Par periode de',
				'default_value'=>$byMonth,
				'field_multiple'=>false,
				'return_name'=>false,
				'field_required'=>true,
				'adv_select'=>false,
				'display_both'=>false,
				'disabled'=>false,
				'field_id'=>'pitch',
		);
		construct_select($list, $params, $this);

		$params = array(
				'field_name'=>'on_container_id',
				'field_description'=>'For container, or let empty for all',
				'default_value'=>$_REQUEST['container_id'],
				'field_multiple'=>true,
				'return_name'=>false,
				'field_required'=>false,
				'adv_select'=>true,
				'display_both'=>false,
				'disabled'=>false,
				'field_id'=>'container',
		);

		construct_select_container($params, $this, container::_factory($_REQUEST['space']) );

		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('hidden', 'stat_query_step', 'displayresult');
		$this->addElement('hidden', 'container_id', $_REQUEST['container_id']);
		$this->addElement('hidden', 'space', $_REQUEST['space']);
		foreach($_REQUEST['statistics_queries'] as $sq){
			$this->addElement('hidden', 'statistics_queries[]', $sq);
		}

		$this->addElement('submit', 'next', 'Next >>');
	}
}

/**
 * Build form for input parameters
 *
 */
function stats_query_inputparams(Stats_Query_rejects_by_errorcode_Form $form, $smarty){
	// Try to validate the form
	if ( !$form->validate() ) {
		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('layouts/ranchbe.tpl');
		return;
	}
}

function _stats_query_getQualityRatio($fromDate, $toDate, $pitch, $cfilter){
	
	$dbranchbe=Ranchbe::getDb();
	$baseurl = BASEURL;

	$state = 'Livrer';

	//Count rt on period
	$view_name = 'view_rejected_docc';
	$sqlrt = 'SELECT COUNT(docid) FROM ' . $view_name;
	$sqlrt .= ' WHERE started > ?';
	$sqlrt .= ' AND started < ?';
	if($cfilter){
		$sqlrt .= ' AND ' . $cfilter;
	}
	$stmtrt = $dbranchbe->Prepare($sqlrt);

	//Select documents delivred on period
	$sqldelivred = 'SELECT COUNT(DISTINCT inst_name) FROM view_instances_activities';
	$sqldelivred .= ' WHERE act_name LIKE ?';
	$sqldelivred .= ' AND inst_start > ?';
	$sqldelivred .= ' AND inst_start < ?';
	if($cfilter){
		$sqldelivred .= ' AND ' . $cfilter;
	}
	$stmtdelivred = $dbranchbe->Prepare($sqldelivred);

	$ratiotext = '<ul>';
	$delivredtext = '<ul>';

	$sampleStart = $fromDate; //date de debut de lechantillon
	$sampleEnd = 0; //date de fin de lechantillon
	$i=0;
	while($sampleEnd < $toDate){
		$sampleEnd = $sampleStart + $pitch;

		$countRt[$i] = $dbranchbe->GetOne($stmtrt, array($sampleStart, $sampleEnd));
		$countDelivred[$i] = $dbranchbe->GetOne($stmtdelivred, array($state, $sampleStart, $sampleEnd));
		if($countDelivred[$i] > 0){
			$ratios[$i] = 100 - ( ($countRt[$i] / $countDelivred[$i]) * 100 );
		}else{
			$ratios[$i] = 0;
		}
		$legends[$i] = formatdate($sampleStart, 'short');

		$ratiotext .= '<li>'. $legends[$i] .' : '. round($ratios[$i], 2) .'%</li>';
		$delivredtext .= '<li>'. $legends[$i] .' : '. $countDelivred[$i] .'</li>';
		$rttext .= '<li>'. $legends[$i] .' : '. $countRt[$i] .'</li>';
		$typtext .= '<li>'. $legends[$i] .' : '. $countRt[$i] .'</li>';

		$sampleStart = $sampleEnd;
		$i++;
	}
	$ratiotext .= '</ul>';
	$delivredtext .= '</ul>';

	$totalRt = $dbranchbe->GetOne($stmtrt, array($fromDate, $toDate));
	$totalDelivred = $dbranchbe->GetOne($stmtdelivred, array($state, $fromDate, $toDate));

	//Ratio graph
	$header =  '<h1>Quality ratio<br /></h2><br />';
	$graphs[] = array('text' => $header);
	$title = urlencode ('Ratio delivred/rt'); //titre du graph
	$values = urlencode(serialize($ratios));
	$legends = urlencode(serialize($legends));
	$graphs[] = array(
			'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legends' alt='no graph'/>",
			'text' => $ratiotext);


	//Delivred graph
	$header =  '<h1>Delivred count(Total=' . $totalDelivred . ')<br /></h2><br />';
	$graphs[] = array('text' => $header);
	$title = urlencode ('Delivred count'); //titre du graph
	$values = urlencode(serialize($countDelivred));
	$graphs[] = array(
			'image' => "<img src='$baseurl/inc/graphs/graphBar.php?title=$title&values=$values&legend=$legends' alt='no graph'/>" ,
			'text' => $delivredtext);

	return $graphs;
}


function stats_query_rejects_by_errorcode_by_users($container)
{
	$space = $container->space;
	$spaceName = $space->getName();
	$smarty = Ranchbe::getView();
	$dbranchbe = Ranchbe::getDb();
	$baseurl = BASEURL;

	$form = new Stats_Query_rejects_by_errorcode_Form('GetRejectForPeriod', 'post', $baseurl.'/container/'.$spaceName.'/getstats');
	$form->buildForm();

	if( !$_REQUEST['stat_query_step'] ){
		$step = 'inputparams';
	}else{
		$step = $_REQUEST['stat_query_step'];
	}

	switch($step){
		case 'inputparams':
			stats_query_inputparams($form, $smarty);
			die;
		case 'displayresult':
			$fromDate = $form->getSubmitValue('from_date');
			$toDate = $form->getSubmitValue('to_date');
			$pitch = (int) $form->getSubmitValue('pitch');
			$on_container_ids = $form->getSubmitValue('on_container_id');

			$period = $toDate - $fromDate;
			if($period < 0){
				$period = $fromDate - $toDate;
				$fromDate = $toDate;
				$toDate = $fromDate + $period;
			}

			if(!$pitch){
				$pitch = 3600 * 24 * 362;
			}

			if(is_array($on_container_ids)){
				foreach($on_container_ids as $container_id){
					$cfilter[] = 'container_id = ' . $container_id;
				}
				$cfilter = implode(' OR ', $cfilter);
				$cfilter = '('.$cfilter.')';
			}

			$header =  '<h1>For period from '.formatdate($fromDate, 'short').' to '.formatdate($toDate, 'short').'<br />';
			$header .=  '</h1><br />';

			// ---------------------- DISPLAY ERRORS BY TYPE -------------------------------
			//Query all reject from history table and galaxia_instance table

			/*
			 $query = "SELECT document_number,action_by,started,owner,status,properties FROM $space->DOC_HISTORY_TABLE as histo
			JOIN galaxia_instances as instance
			ON histo.instance_id = instance.instanceId
			WHERE action_name = 'ChangeState'
			AND document_state LIKE 'rejete%'
			' AND started > ?';
			' AND started < ?';
			";
			*/

			$view_name = 'view_rejected_docc';
			$sql = 'SELECT * FROM ' . $view_name;
			$sql .= ' WHERE started > ?';
			$sql .= ' AND started < ?';
			$sql .= ' AND status = \'completed\'';

			if($cfilter){
				$sqlrt .= ' AND ' . $cfilter;
			}
			$stmtrt = $dbranchbe->Prepare($sqlrt);

			$graphs[] = array('text' => "<h1>QUERY:  </h1>" . $query . '<br>' . $header);

			//Execute query and get the recordset in Rset var
			if($Rset = $dbranchbe->Execute($query)){
				if($Rset->RecordCount() === 0){
					echo 'None to display<br>';
					echo $query;
					return false;
				}
				$cumul_errors_by_type = array();
				while ($row = $Rset->FetchRow()) {
					$props = unserialize($row['properties']); //unserialize the properties wich are store in BLOB type field indatabase
					if(is_null($props['error_code'])){
						$props['error_code'] = array('unknow');
					}
					$users[] = $row['owner']; // Users wich have submit a workflow
					if(is_array($props['error_code'])){
						foreach($props['error_code'] as $error_code){ //pour chaque code erreur on cree une entree de tableau avec en valeur l'utilisateur qui a soumis le document a la verif.
							$cumul_errors_by_type[$error_code][] = $row['owner'];
						}
					}
				}
			}
			else{
				print 'error in query';
				return false;
			}

			$text =  "<h1>Erreurs par type et par utilisateur</h2><br />";
			$graphs[] = array('text' => $text);

			foreach($cumul_errors_by_type as $error_type=>$count_by_user){
				$Count_error[$error_type] = array_count_values($cumul_errors_by_type[$error_type]);
				$text = 'Type : "'. $error_type . '" :' . '<br />';
				$graphs[] = array('text' => $text);
				$values = array();$legend = array(); //init graph var
				foreach($Count_error[$error_type] as $user=>$val){
					//Set the text to display
					$text = $val . ' erreur type "' . $error_type . '" pour l\'utilisateur '.$user. '<br />';
					$graphs[] = array('text' => $text);
					//Display graph
					$values[] = $val;
					$legend[] = $user;
				}
				$title  = urlencode ($error_type);
				$values = urlencode(serialize($values));
				$legend = urlencode(serialize($legend));
				$graphs[] = array(
						'image' => "<img src='$baseurl/inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>",
						'text' => ''
				);
				$text = '';
			}
			return $graphs;
	}
}
