<?php

namespace xlsRenderer\Document;

require_once "Spreadsheet/Excel/Writer.php";

class Renderer1
{
	/**
	 *
	 * @param unknown_type $container
	 */
	public function render($list, $name)
	{
		// Creating a workbook
		$workbook = new \Spreadsheet_Excel_Writer();

		// sending HTTP headers
		$workbook->send($name.'.xls');

		// Creating a worksheet
		$worksheet =& $workbook->addWorksheet($name);

		//The header
		$format =& $workbook->addFormat(array(
			'Size' => 10,
			'bold'    => 1,
			'Align' => 'center'
			));

		foreach(array_keys($list[0]) as $key){
			$worksheet->write(0, $col, $key, $format);
			$col++;
		}

		// The actual data
		$i = 0;
		while($lin < count($list)){
			$col = 0;
			$lin = $i+1;
			foreach($list[$i] as $key=>$val){
				$worksheet->write($lin, $col, $val);
				$col++;
			}
			$i++;
		}

		// Let's send the file
		$workbook->close();
	}
}
