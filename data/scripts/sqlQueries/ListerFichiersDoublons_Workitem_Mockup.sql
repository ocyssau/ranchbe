SELECT `mockup_files`.`file_md5` AS MD5, `mockup_files`.`file_name` AS NAME, `mockup_files`.`file_path` AS PATH FROM mockup_files
WHERE `file_md5` IN(
SELECT Count(*) as dbl_count
FROM `mockup_files` GROUP BY `file_md5` HAVING Count(*) > 1
)

UNION

SELECT `workitem_doc_files`.`file_md5` AS MD5, `workitem_doc_files`.`file_name` AS NAME, `file_path` AS PATH FROM workitem_doc_files
WHERE `file_md5` IN(
SELECT Count(*) as dbl_count
FROM `workitem_doc_files` GROUP BY `file_md5` HAVING Count(*) > 1
)

