SELECT sum(`cumul_size`) AS total_size FROM(
	SELECT sum(`file_size`) as cumul_size,
	`file_size`, Count(*) as dbl_count,
	`file_md5`, `file_name`
	FROM workitem_doc_files 
	GROUP BY `file_md5`
	HAVING Count(*) > 1
) AS T1;

