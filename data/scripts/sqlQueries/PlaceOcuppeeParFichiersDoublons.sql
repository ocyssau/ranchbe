SELECT T1, T2 FROM(

SELECT sum(`cumul_size`) AS total_size FROM(
	SELECT sum(`file_size`) as cumul_size,
	`file_size`, Count(*) as dbl_count,
	`file_md5`, `file_name`
	FROM workitem_doc_files 
	GROUP BY `file_md5`
	HAVING Count(*) > 1
) AS T1,

SELECT sum(`cumul_size`) as total_size FROM(
	SELECT sum(`file_size`) as cumul_size,
	`file_size`, Count(*) as dbl_count,
	`file_md5`, `file_name`, `file_path`
	FROM mockup_files 
	GROUP BY `file_md5`
	HAVING Count(*) > 1
) AS T2

);
