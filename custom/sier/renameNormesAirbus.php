<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>RanchBE-Renomme Normes AIRBUS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <link href="styles/jalist.css" rel="stylesheet" type="text/css">
    <link href="styles/PhpLayerMenu.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="img/ranchbe_logo_20.png" />
    <script type="text/javascript" src="lib/tiki-js.js"></script>
    <script type="text/javascript" src="lib/lib.js"></script>

    <noscript>
    BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up.
    </noscript>

</head>
<body>

<?php

require_once ('conf/ranchbe_setup.php');
require_once './class/wildspace.php';
$wildspace = new wildspace();

/* Standards :
* ABS , ASNA , ASNB , ASND , ASNE , AN , ASN , AS , BSSP , BS , DAN , DHS , DIN , EN , LN , MIL , MS , NAS , NSA , PQ , SA , SL , TR
*
//
*/

$prefix =   array(
            'ASNA',
            'ASNB',
            'ASNE',
            'ASND',
            'ASN',
            'AS',
            'NASM',
            'NAS',
            'NSA',
            'NAS',
            'PQ',
            'ABS',
            'BAS',
            'prEN',
            'ABD',
            'ADET',
            'A007',
            'AIMS',
            'DAN',
            'DIN',
            'MS',
            'AN',
            'MIL',
            'AIPS',
            'AM',
            'AP',
            'IPS',
            'MM',
            'MTG',
            'MTS',
            'MTU',
            'MTU',
            'MTC',
            'IQDA',
            'ASDT',
            'BS',
            'C0',
            'MIM',
            'MIX',
            'CAB',
            'CAP',
            'CAP',
            'CBE',
            'E-00',
            'ABP',
            'AIPS',
            'AIQI',
            'D00',
            'DHS',
            'IPDA',
            'M20',
            'MBBN',
            'ME',
            'P0',
            'MU',
            'S007',
            'TR',
            'TDD',
          );

sort($prefix);

$file_path = $wildspace->GetPath().'/nouveau_telechargement_docmaster';
$limit = 150; //Nombre maximum de fichier a traiter
$selected_prefix = $_REQUEST['type'];
$extension = '.pdf';

//Fichier excel extrait de docmaster
$docMaster = $file_path.'/'.$selected_prefix.'.xls';
$renamed_file_path = $wildspace->GetPath();

$doctype_number = 'airbus__norme__'.$selected_prefix;
require_once './class/doctype.php';
$doctype = new doctype();
$res = $doctype->GetInfosNum( $doctype_number , array('recognition_regexp') );
$recognition_regexp = $res[0];
//var_dump($recognition_regexp);


//------------------------------------------------------------------------------
//Affiche une aide
//------------------------------------------------------------------------------
?>

<h1>Outil de renommage et de génération des fichier cvs d'import a partir de docmaster</h1>

<p>Ce script génère un fichier CSV pret à etre utilisé par les outils d'importation de document de RanchBE
et tente de renommer les fichiers de normes issus de Docmaster selon le formalisme interne SIER.
</p>

<p>
- Les fichiers à renommer/importer doivent êtres présents dans le dossier <b><?php echo $file_path; ?></b><br />
- L'extrait de docmaster au format xls doit être nommé <b><?php echo $docMaster; ?></b><br />
- Les fichiers renommés seront déplacés dans le dossier <b><?php echo $renamed_file_path; ?></b><br />
- Document type : <b><?php echo $doctype_number; ?></b><br />
- Expression a respecter : <b><?php echo $recognition_regexp; ?></b>
</p>

<p><a href="wildspace.php">Retourner au wildspace</a></p>

<?php
//------------------------------------------------------------------------------
//Renomme les fichiers
//------------------------------------------------------------------------------
if( $_REQUEST['action'] == 'renommer' ){
  if( !isset($_REQUEST['type']) ){
    die('aucun type selectionné. Il faut selectionner un type<br />');
  }

  if( !is_dir( $renamed_file_path ) ){
    if( !mkdir( $renamed_file_path ) ){
      echo 'cration impossible de '.$renamed_file_path;
      return false;
    }
  }

  $i = 1;
  $csvFile = $renamed_file_path.'/'.$i.'_'.$_REQUEST['type'].'_import.csv';
  while( is_file($csvFile) ){
    $csvFile = $renamed_file_path.'/'.$i.'_'.$_REQUEST['type'].'_import.csv';
    $i++;
  }

  $handle = fopen($csvFile  , a );
  fwrite($handle , 'document_number;file;designation;maj_int_ind;min_int_ind;doc_source;commentaires'.chr(10) );

  foreach($_REQUEST['original_name'] as $key=>$val){

//var_dump($_REQUEST['importer'][$key]);
    if($_REQUEST['importer'][$key] != 1) continue; //Si la case continuer n'est pas cocher on ignore ce fichier

    $from_file = "$file_path" .'/'. $val;
    $to_file = $renamed_file_path.'/'. $_REQUEST['trans_name'][$key];
    if(is_file($to_file)) {
      echo '<b><font color="red">Le fichier '.$to_file.' existe déja dans le Wildspace. Il est ignoré</font></b><br />';
      continue;
    }
    if(copy($from_file , $to_file)){
      echo 'copie ' . $from_file .' to '. $to_file .'<br />';
      //Generate the csv file for import in database
      $csv = substr($_REQUEST['trans_name'][$key], 0, strrpos($_REQUEST['trans_name'][$key], '.')).';'; //Number of doc
      $csv .= $_REQUEST['trans_name'][$key].';'; //Number of file
      $csv .= $_REQUEST['designation'][$key].';'; //Designation
      $csv .= $_REQUEST['rev_maj'][$key].';'; //Maj rev
      $csv .= $_REQUEST['rev_min'][$key].';'; //Min rev
      $csv .= 'docmaster;'; //doc_source
      $csv .= $_REQUEST['commentaires'][$key].';'; //Min rev
      //$csv .= $_REQUEST['langue'][$key].';'; //Langue
      //$csv .= $_REQUEST['historique'][$key].';'; //Historique
      $csv .= chr(10); //fin de ligne

      fwrite($handle , $csv);
      if( unlink($from_file) ){
        echo 'suppression de ' . $from_file .'<br />';
      }else{
        echo '<b><font color="red">Impossible de supprimer '.$from_file.'</font></b><br />';
      }
    }
  }

  fclose($handle);

}

//------------------------------------------------------------------------------
//Selection du type :  creation du formulaire :
//------------------------------------------------------------------------------
echo '<FORM METHOD="post">';
//Selection du type
echo '<SELECT NAME="type">';
foreach($prefix as $type){
  if($_REQUEST['type'] == $type)
    echo '<option value="'.$type.'" selected>'.$type.'</option>';
  else
    echo '<option value="'.$type.'">'.$type.'</option>';
}
echo '</select>';

echo '<INPUT TYPE="submit" NAME="action" VALUE="recapitule">';
echo '<INPUT TYPE="hidden" NAME="request_page" VALUE="renameNormesAirbus">';
echo '</FORM>';

//Generer un recapitulatif avec ancien nom et nouveau nom propos�.

//Set length
switch( $_REQUEST['type'] ){
  case 'A007':
  case 'ABD':
  case 'ABS':
    $length = 7;
    break;
  case 'ADET':
    $length = 8;
    break;
  case 'AIPS':
  case 'AIMS':
  case 'AM':
  case 'AN':
  case 'AS':
  case 'ASDT':
    $length = 13;
    break;
  case 'MM':
    $length = 13;
    break;
  default:
    $length = 13;
    break;
}

echo '<FORM METHOD="post">';
echo '<INPUT TYPE="hidden" NAME="request_page" VALUE="renameNormesAirbus">';


echo '<table border="1">';
echo '<tr>';
echo '<th><b>Importer</b></th>';
echo '<th><b>Nom original</b></th>';
echo '<th><b>Nom modifie : </b></th>';
echo '<th><b>REV MAJ : </b></th>';
echo '<th><b>REV MIN : </b></th>';
echo '<th><b>DESIGNATION : </b></th>';
echo '<th><b>COMMENTAIRES : </b></th>';
echo '</tr>';

//Liste les fichiers dans le $file_path et genere un nouveau nom
$files = glob($file_path .'/'. $selected_prefix .'*'.$extension);

//Parse le fichier extrait de Docmaster
//set_include_path('.'. PATH_SEPARATOR . './lib/adodb'. PATH_SEPARATOR .'../../lib/PEAR');
require_once 'Spreadsheet/Excel/Reader.php';
$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('CP1251');
$data->read($docMaster);

$i=0;
foreach ($files as $file){
  if($i > $limit) break;
  //$file = call_user_func($functionName, $file , $length , $data->sheets[0]['cells'] , $with_default_only); //Affiche un formulaire ancien nom - nouveau nom
  $file = convert_name( $file , $length , $data->sheets[0]['cells'] , $with_default_only ); //Affiche un formulaire ancien nom - nouveau nom
  $file = $file['DOC'];
  if (strlen($file) != $length) $error[] = $file;
  $i++;
}

echo '</table>';

echo '<INPUT TYPE="submit" NAME="action" VALUE="renommer">';
echo '<INPUT TYPE="hidden" NAME="type" VALUE='.$_REQUEST['type'].'>';
echo '</FORM>';
//echo '<pre>';var_dump($error);echo '</pre>';


//Format du cvs
//Number;REV MAJ;REV MIN;Commentaires;Fichier;

//------------------------------------------------------------------------------
//Fonction de conversion du nom. Genere une ligne de forumlaire.
//------------------------------------------------------------------------------
function convert_name($file , $length , $extrait_docmaster, $with_default_only=true){
  if(empty( $file ) ) return false;
  $originale_file_name = basename($file);

  //Extrait le numero de partie. exemple : _PART_1
  if(preg_match_all( '/(_PART_[0-9]+)|(_PART_MAIN)/' , $originale_file_name , $match)){
    $file = preg_replace( '/(_PART_[0-9]+)|(_PART_MAIN)/' , '' ,  $originale_file_name );
  }
  //var_dump($match);

  $file = clean_name($file);
  $extension = $file['extension'];
  $file = $file['root_name'];
  $explode = explode('_', $file ); //Decoupe la chaine par '_'
  $explode = array_reverse($explode); //inverse le tableau.
  if( count($explode) >= 2 ){
    $result['MIN'] = $explode[0];
    $result['MAJ'] = $explode[1];
    $result['DOC'] = array_pop($explode).array_pop($explode);
  }else{
    $result['DOC'] = array_pop($explode).array_pop($explode);
  }

  //Recolle le numero de partie. Exemple : _PART_1
  if( !empty($match[0][0]) ){
    $result['DOC'] =$result['DOC'].$match[0][0];
  }

  //Cherche dans l'extrait docmaster le document courant
  //La boucle s'execute sur plusieur document, le dernier etant l'indice le plus recent,
  //on suppose que l'on importe le plus recent seulement
  foreach( $extrait_docmaster as $row ){
    $docmaster_result['DOC'] = str_replace(' ', '', $row[2]); //supprime les espaces des numeros docmaster
    if( $docmaster_result['DOC'] == str_replace('_', '', $result['DOC']) ){ //supprime les _ des numeros ranchbe
      $result['DESIGNATION'] = $row[5];
      $result['LANG'] = trim($row[7]);
      $result['HISTO'] = trim($row[3]);
      $MAJ = trim(substr( $row[6], 0, strrpos($row[6], '/') ) );
      $MIN = trim(str_replace('/','',substr( $row[6], strrpos($row[6], '/') ) ) );
    }
  }
  if( empty($result['MAJ']) ){
    $result['MAJ'] = $MAJ;
  }
  if( empty($result['MIN']) ){
    $result['MIN'] = $MIN;
  }
  //On presume que si l'indice min est sup a 4 caracteres, alors l'indice min est abscent et il s'agit de l'indice maj.
  if( (strlen($result['MIN']) >= 4) && empty($result['MAJ']) ){
    $result['MAJ'] = $result['MIN'];
    unset( $result['MIN'] );
  }

  global $file_path;
  if ((strlen($file) != $length && $with_default_only) || (!$with_default_only) ){
    echo '<tr>';
    echo '<td><INPUT TYPE=checkbox NAME="importer[]" VALUE="1" checked></td>';
    echo '<td><A HREF="javascript:popup(\'viewFile.php?file='.$file_path.'/'.$originale_file_name.'\',\'normes\')">'.$originale_file_name.'</A>';
    echo '<INPUT TYPE=HIDDEN NAME="original_name[]" VALUE="' . $originale_file_name . '"></td>';
    echo '<td><INPUT TYPE=TEXTE NAME="trans_name[]" VALUE="' . $result['DOC'].$extension . '" SIZE="70"></td>';
    echo '<td><INPUT TYPE=TEXTE NAME="rev_maj[]" VALUE="' . $result['MAJ'] . '" SIZE="15"></td>';
    echo '<td><INPUT TYPE=TEXTE NAME="rev_min[]" VALUE="' . $result['MIN'] . '" SIZE="15"></td>';
    echo '<td><TEXTAREA NAME="designation[]"  rows="2" cols="70">'.$result['DESIGNATION'].'</TEXTAREA></td>';
    /*echo '<b>LANGUE : </b>';
    echo '<INPUT TYPE=TEXTE NAME="langue[]" VALUE="' . $result['LANG'] . '" SIZE="6">  ';
    echo '<b>HISTORIQUE : </b>';
    echo '<INPUT TYPE=TEXTE NAME="historique[]" VALUE="' . $result['HISTO'] . '" SIZE="6">  ';
    echo '<br>';*/
    echo '<td><TEXTAREA NAME="commentaires[]"  rows="2" cols="70">'.$result['COMMENTAIRES'].'</TEXTAREA></td>';
    echo '</tr>';
  }
  return $result;
}

function clean_name($file_name){
  //supprime les espaces en debut et fin de chaine
  $file = trim(basename($file_name));

  //Extrait l'extension
  $extension = substr($file, strrpos($file, '.'));

  //Supprime l'extension
  $file = preg_replace('/'.$extension.'$/' , '' , $file);

  //remplace les doubles espaces par des espaces simple
  $file = preg_replace('/[ ]{2,}/' , ' ' , $file);

  //remplace les espaces par rien
  $file = preg_replace('/[ ]/' , '' , $file);

  //Convertis en majuscule
  $file = strtoupper($file);

  //Convertis l'extension en minuscule
  $extension = strtolower($extension);

  $ret['root_name'] = $file;
  $ret['extension'] = $extension;

  return $ret;
}
