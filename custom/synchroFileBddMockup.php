<?php
/**
 * How to execute:
 * http://ranchbe.sierbla.int/custom.php?module=synchroFileBddMockup.php
 */

echo "<i>This scripts is desactivated.</i>";
die;

require_once './class/common/document.php';
require_once './class/common/container.php';
require_once './class/common/docfile.php';
require_once './class/common/space.php';
require_once './class/doctype.php';

$smarty = Ranchbe::getView();

ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

// Load the controller
require_once('HTML/QuickForm/Controller.php');
// Load the base Action class (we will subclass it later)
require_once('HTML/QuickForm/Action.php');
require_once 'HTML/QuickForm/Action/Next.php';
require_once 'HTML/QuickForm/Action/Back.php';

$space = new space('workitem');
$Manager = new container($space);
require_once('./GUI/GUI.php');


// Class representing a form
class FirstPage extends HTML_QuickForm_Page
{
	function buildForm()
	{
		$this->_formBuilt = true;

		$this->addElement('header', null, 'Synchronize files on filesystem and records in bdd.');
		//$help .= '<a href="http://ranchbe.sierbla.int/viewFile.php?document_id=11395&space=bookshop">HELP</a>';
		//$this->addElement('static', 'help', $help);

		//selection des maquettes � mettre a jour
		$mockups = array(
		            'field_type'=>'selectFromDB',
		            'field_name'=>'mockups',
		            'field_description'=>'Maquettes sources',
					'table_name'=>'mockups',
					'field_for_display'=>'mockup_number',
		            'field_for_value'=>'default_file_path',
		            'default_value'=>'',
		            'field_multiple'=>true,
		            'return_name'=>false,
		            'field_required'=>false,
		            'adv_select'=>true,
		            'display_both'=>false,
		            'disabled'=>false,
		            'field_id'=>'mockups');
		construct_element($mockups, $this);

		$this->addElement('hidden', 'module', 'mockupToWildspace.php');
		$this->addElement('submit', $this->getButtonName('next'), 'Next >>');

		// Define filters and validation rules
		$this->setDefaultAction('submit');
	}
}

class FirstPageActionNext extends HTML_QuickForm_Action_Next
{
	function perform(&$page, $actionName)
	{
		$space = new space('workitem');
		$logFile = $wildspacePath .'/mockupToWildspace.log';
		$logger = Log::factory('file', $logFile);

		// save the form values and validation status to the session
		$page->isFormBuilt() or $page->buildForm();
		$pageName =  $page->getAttribute('id');
		$data = $page->controller->container();
		$nextName 		= 'secondPage';
		$previousName 	= 'firstPage';

		$prev = $page->controller->getPage($previousName);
		$next = $page->controller->getPage($nextName);

		$mockups = $page->getElement('mockups')->getValue();

		//suppress empty line from mockups
		$t = array();
		foreach($mockups as $e){
			if($e){
				$t[] = $e;
			}
		}
		$mockups = $t;

		if(!$mockups){
			throw new Exception('You must select at least one mockup');
		}

		$errors = false;
		while( !$CsvSplFile->eof() ){
			$line = $CsvSplFile->fgetcsv();
			$fileName = $line[$fileKey];
			if(!$fileName){
				continue;
			}

			$to = $wildspacePath . '/'. $fileName;
			if( is_file($to) ){
				$errors = true;
				$logger->log('FAILED: file '. $to . ' is existing ' );
			}

			foreach($mockups as $path){
				$isFound = false;
				$mockupFile = $path . '/' . $fileName;

				if(is_file($mockupFile)){
					$isFound = true;
					if( copy($mockupFile, $to ) ){
						$isCopy = true;
						$logger->log('copy '. $mockupFile . ' to ' . $to);
						$continue;
					}
					else{
						$errors = true;
						$logger->log('FAILED: error during copy of '. $mockupFile . ' to ' . $to);
					}
				}
			}
			if(!$isFound){
				$errors = true;
				$logger->log('FAILED: '. $fileName . ' is not found in mockups');
			}
		}

		if($errors){
			echo "La copie est terminee mais il y a des erreurs. Consulter le log $logFile <br />";
		}
		else{
			echo "La copie est terminee. <br />";
		}

	} //End of method
} //End of class


// Instantiate the Controller
$controller = new HTML_QuickForm_Controller('rtimport');
// Set defaults for the form elements
$controller->setDefaults(array(
    'document' => ''
));

$firstPage = new FirstPage('firstPage');
$controller->addPage($firstPage);
$firstPage->addAction('next', new FirstPageActionNext());

// Process the request
ob_start();
$controller->run();
// Display the template
$smarty->assign('midcontent', ob_get_clean());
$smarty->display('ImportRt.tpl');

