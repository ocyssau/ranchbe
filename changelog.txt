﻿0.6:
BUGS :
- Bug : la generation du SearchOrder catia ne fonctionne plus.
- Bug : impossible de créer des doclinks.
- Bug : lorsque l'on stock un document dans cadlib/bookshop/mockup affiche un message d'erreur vide.
- Bug: les doclinks ne sont pas copiés lors de la copie du document
- Bug : Le tri de document sur les propriétés étendues ne fonctionne pas.
- Bug : On ne peux pas séléctionner d'autres documents que dans l'espace workitem pour définir un doclink..
- Bug : Les date de mise à jours n'evolue pas lors de la mise à jour d'un "file manager container"
- Bug : [client 192.9.200.100] PHP Fatal error:  Allowed memory size of 102760448 bytes exhausted (tried to allocate 83 bytes) in /SHARES/SIER/SCRIPTS/ranchbe/lib/adodb/drivers/adodb-mysql.inc.php on line 632, referer: http://trinity/ranchbe/ImportManager.php?space=mockup
	Increase the value of memory_limit.

GUI :
- Supprime les messages d'erreurs dû à l'abscence des fichiers attachments lors du CopyDocument, MoveDocument, SuppressDocument.
- Ajoute le docfiles manager qui permet de lister tous les fichiers d'un conteneur de documents.
- Exportation de la liste des documents vers excel. Création d'un concept de "render" qui permet de pré-définir des mise en page des exports vers excel.
- ajout des fonctions pour decompresser un package zip ou créer un package zip depuis une selection de fichier du wildspace.
- modification GUI de la page "document detail windows".
- ajout d'une page d'information sur RanchBE.
- ajout d'une demande de commentaire avant checkin.
- le bouton changement d'indice permet de faire évoluer l'indice de plusieurs documents en même temps sans qu'il soit nécessaire de les vérouiller préalablement.
- Après reconnexion l'utilisateur est dirigé vers la page requise plutôt que la page d'accueil par défaut.
Workflow GALAXIA :
- Ajoute les options "comment" et "automatic". "comment" contrôle l'affichage de la page de saisie d'un commentaire à la fin de l'execution d'une tâche. "automatic" permet de définir une activité
automatiquement executée sans intervention de l'utilisateur.
- L'utilisation du mot OR et maintenant permise dans les champs de recherche pour définir des conditions de recherches.
- Refonte de la fonction utilistateur "Store" en une fonction intelligente qui s'adapte a l'état du document. Par exemple, si le fichier existe dans la base donnée, il vous proposera de le mettre à jour ou de créer un nouveau indice.
- Ajoute un onglet a la prairie qui permet de rendre accessible en consultation les autres conteneurs lié au projet courant.
- Ajoute une page "préférences utilisateurs" pour permettre à l'utilisateur de contrôler certains aspect de ranchbe
- Les actions sur les documents sont maintenant affichés selon la possibilité ou non de les executer.
- Ajouts des fonctions markToSuppress et unMarkToSuppress dans le docment manager.
- Les messages peuvent maintenant être mise en forme avec du HTML à l'aide de HTML_area
- Modification de la fonction Store qui est maintenant capable de determiner automatiquement les actions possibles : update, création, upgradeIndice...
- Modification de la présentation de la page document_detail_window.
- Création des alias de conteneur : Un conteneur peut-être nommé de plusieurs nom differents.
- Les categories sont liés maintenant au conteneur. Ainsi chaque conteneur a son propre jeu de categories.
- Ajout de postits liés au commentaires.

MODEL DE DONNEES :
- Création de la table documents qui est une vue des tables workitem_documents, cadlib_documents, mockup_documents, bookshop_documents
- Création de la table document_relation utilisée par la classe productStructure/relationship.
- Ajoute un champ "doc_space" aux tables workitem_documents, cadlib_documents, mockup_documents, bookshop_documents avec par default la valeur workitem, cadlib, bookshop ou mockup.
	ce champ permet de retrouver l'espace du document depuis une requête dans la vue documents
- Ajoute un champ "comment" aux tables workitem_documents_history, cadlib_documents_history, mockup_documents_history, bookshop_documents_history.
	ce champ permet d'enregistrer les commentaires utilisateurs lors du checkin.
- Ajoute un champ "isAutomatic" à la table galaxia_activities : Ce champ définis le mode d'execution de l'activité. Si elle est automatique, elle sera executé sans requêtes de l'utilisateur.
- Ajoute un champ "isComment" à la table galaxia_activities : 
	Ce champ permet de définir si l'écran de saisie de commentaire s'affiche après l'execution. Cette possibilité n'est pas implémentée pour l'instant.
- création des tables [space]_doc_files_versions
- Renomme le champ issued_from_document en from_document_id dans les tables [space]_documents et [space]_document_history

CORE :
- Ecriture de la classe doclink et déplacement des fonctions relatives au doclink dans cette nouvelle classe. Cette classe ne sert que pour assurer la compatibilté avec les versions précedentes du model.
	- Ajoute une fonction pour visualiser les doclinks pères dans le documentdetailwindow.
	- Renomme les champs des doclinks dr_l_document_id devient dr_son_id / dr_l_document_id devient dr_father_id
- Ecriture de la classe productStructure/relationship qui permet de définir des relations entre documents appartenants à des espaces differents. Elle remplace la classe doclink.
- ajout de fonctions de programation evenementiel : ajoute class observable et observer. modifie les héritages. Les classes documents et container sont maintenant obervables.
	- ajout de notification d'évènement dans les classes documents et container.
- les règles d'écriture des scripts sont entierement revues. les  scripts sont maintenant déclenchés par les évenements. ajout des classes doctypeScript, containerScript, script_caller
- Création des datatypes pour les données Cadds(c). Ceci permet à RanchBE de manipuler les répertoires de données Cadds(c) comme tout autre données.
- Création de la classe fsdataVersion. Cette classe manipule les versions d'une donnée (classe fsdata). Les versions sont attachées aux données par simple convention de nommage (comme c'était déja).
	pour créer un objet fsdataVersion il faut d'abord créer un objet fsdata : voir la méthode recordfile::initFsdataVersion en exemple.
- Suppression des methodes recordfile::PutVersionInWildspace, recordfile::RemoveVersion, fsdata::RemoveVersion, fsdata::GetVersions, 
- Creation de la methode recordfile::initFsdataVersion.
- modification de docfile : suppression de toute les references à des chemin du système de fichier. Remplacé par des appels aux classe fsdata et fsdataVersion
- on peut maintenant visualiser les fichiers des versions.
- Ajoute la multi selection de documents pour les changement d'état.
	- crée les objets workflow, docflow, instanceDocflow.
	- réorganisation des scripts de pilotage du workflow Galaxia
	- modification des objets Galaxia : Instance : modification des procédures d'execution des activités.
- reorganisation du code
	- les fichiers de configuration sont tous dans le dossier conf
	- déplacement de galaxia_setup.php
	- remplacement du dossier "container" par "inc"
- ajoute un logger: création de la classe log.
- modifie le nom des types adraw-c4, adraw-c5 qui deviennent adrawc4 et adrawc5.
- modifie le nom de depot des fichiers versions. Auparavant les fichiers versions étaient stockés dans /[deposit_dir]/[container name]/versions, ils sont maintenant dans /[deposit_dir]/[container name]/__versions.
	pour mettre à jour le dépot des versions ranchbe antèrieur, renommer simplement le dossier versions en __versions des ndossier de dépot de chaque container.
- ajoute les methodes wildspace::GetUserId et wildspace::GetUserName
- recordFile::MoveFile() : le paramètre est un objet conteneur à la place du dossier de destination. C'est plus logique car un objet recordFile est obligatoirement lié à un conteneur, les fichiers ne peuvent être déplacés en dehors des dossiers du conteneur.
- création de la methode recordFile::GetRepositdirPath. Cette methode devient nécessaire depuis que les données peuvent êtres stockés dans des sous-repertoires du deposit directory.
- création des datatypes : adraw, adrawnatif et zipadraw. "adrawnatif" manipule les répertoires de donnée des adraws de CADDS(c).
	"zipadraw" est un adraw zippé. "adraw" accepte l'un et l'autre et il est capable de générer le zip depuis le natif.
- il est maintenant possible d'associer des scripts particulier en fonction du datatype. Ces scripts sont executés par les évènements générés par les objets recordfile et docfile.
- Ajoute une action par défaut au login.tpl :<form method="post" action="{$smarty.server.REQUEST_URI}&logout=0">, ceci permet de rediriger l'utilisateur vers les page requise.
- Modifie la definition des area de liveuser dans la classe workitem. La fonction check_ticket accepte maintenant un tableau en parametre "$areas". Ceci permet de ne pas limiter la définition des permissions à une seule area.
- GALAXIA : Modifie ActivityManager.php, galaxia_admin_activity.php, galaxia_admin_activity.tpl, galaxia_admin_processes.php pour ajouter les options isComment et isAutomatic.
- GALAXIA : Modifie galaxia_save_process.php et galaxia_admin_process.php(line 50~60) : ajoute des filtres de conversions de charset.
- GALAXIA : ProcessManager::unserialize_process() Ajoute un parametre pour setter le charset a utiliser et ajoute un xml_parser_set_option.
- GALAXIA : ProcessManager::serialize_process() Ajoute la serialisation des valeurs de isComment et isAutomatic
- GALAXIA : ProcessManager::import_process() Ajoute l'import des valeurs de isComment et isAutomatic
- Redéfinition du script ranchbe_setup et déplacement de la définitions des constantes de configuration modifiable par l'utilisateurs dans les scripts conf/local_setup.php et conf/default_setup.php.
- Création des scripts conf/local_setup.php et conf/default_setup.php qui contiennent la défintion des constantes de configuration locales
- Modifie les contrôles du acces_code dans la méthode document::UpgradeIndice pour exclure le code 12 (=markToSuppress).
- Ajoute un type HTML_Area aux métadonnées étendus qui permet de créer une zone de texte riche.
- Les fichiers versions sont maintenant associés au docfile par une table de la bdd plutôt que par des conventions de nommage des répertoires.
	- Création de la classe recordfileVersion et nombreuse modifications dans les classes document, docfile, recordfile

0.5.1:
- Refonte de l'interface utilisateur du document manager : La ligne selectionnée est marqué par un fond different. Présentation revue.
- Dans le filtre de document: Si la case 'advenced filter' n'est pas coché les filtres avancés ne sont pas pris en compte. Idem pour les dates.
- Homogenéisation de l'écriture des filtres dans filterManager.php
- modifie filtre pour permettre retour page 1 lorsque les critères de recherche changent.
- Modification de la navigation lors des changement d'état. Maintenant tous ce passe dans une seule fenêtre.
- Ajout d'une fonction pour changer l'indice sur plusieurs documents en une fois.
- Modification des appels aux scripts : dorénavant les macros doivent êtres définis dans une fonction.

0.5:
- Réecriture complete de l'API. Les relations d'héritages ont été fortement cassées afin de faire des objets plus petits et plus indépendants
- Ajout de la fonctions d'import de document depuis un fichier zip.
- Correction bugs sur les filtres.
- Correction bugs dans les rapports statistiques.

0.4.6 :
- Révision de la gestion des fichiers "attachments". Il sont dorénavant identifiés avec l'id du document est sotckés dans des répertoires à la racine du dossier de dépot de l'espace concerné.
- Modification de la méthode checkout.
- Modification de la méthode checkin. Lors de la venue d'une erreur lors du checkin d'un fichier,
  le rollback se limite au fichier en cours checkin et le document reste à l'état checkout. Il est possible de faire
  un "checkin" sur un document dont l'ensemble des fichiers ne sont pas à l'état "checkout".
- modification de l'affichage des erreurs. Dorénavant, les erreurs sont toutes présentées dans un seul pavé.
- Ajout de la méthode "PutDocInWildspace" dans la class documentsManager appelé depuis la page documentManage.php.
- L'objet history est isolé de la chaîne d'héritage principale.
- Divers bugs corrigés.

0.4.5 :
- Il est maintenant possible de créer des liens entres documents.
- Les fonctions Move et Copy sont opérationnels.
- Modification filter de document. IL est maintenant possible de faire des filtres multi-critères.
- Correction Bug : action requise rien ne se passe. Action effectuée sur second clic.
- Ajout option de format pour le type de metadonnee 'date'.
- Ajout du type de metadonnee 'selectFromDB' pour creer des champs de selection issue d'une table de la base de donnée.
- Ajoute une bouton "loupe" dans l'historique des importations pour consulter les fichiers liées au package.
- Revision des imports de fichiers.
- Import/export de partners.
- Generation de searchOrder Catia a partir du contexte.
- Creation d'une class viewerManager pour gérer la visulation des fichiers et en particulier la visualisation des fichiers CAO.
- Les fichiers associé ont maintenant un attribut "file_class" variable pour gérer les visualisations et les apperçus.
- Les fichiers associé ont un attribut "auto" pour controler les fichiers a copier dans le wildspace lors du checkout.
- La taille de l'attribut "file_type" est reduit a 16 caracteres.
- La fenetre de detail offre la possibilite de  visualiser les fichier wrl. Installation du plugin CortonaVRMLCLient est necessaire.
- Le fichier /conf/mimes.csv accepte un attribut supplementaire "driver" pour spécifier de driver a utiliser pour de la visulation. Les drivers sont dans le dossier class/viewerDrivers
- Il est maintenant possible de visualiser les fichiers associes directement depuis la fenetre de detail.
- Le parametre ASSOC_FILE_CHECK_NAME dans le fichier ranchbe_setup permet de controler (un peu) le nom des fichiers qui sont manuellement associés aux document.
- La fonction check_flood a ete revue.

0.4.4 :
- La fonction rechercher permet de visualiser directement les documents.
- Ajoute la possibilité de filter les date dans la barre de recherche du conteneur et dans les modules de recherches général et du conteneur.
- Ajoute la possibilité de filter les utilisateurs dans la barre de recherche du conteneur et dans les modules de recherches général et du conteneur.
- Le bouton "selectionner tout" dans le module message fonctionne.
- Ajoute la possibilité d'utiliser jscalendar pour selectionner les dates.
- Ajoute un bouton pour que l'utilisateur puisse changer son mot de passe.
- Ajoute bouton 'envoyer à tous' dans le gestionnaire d'envoie de message.
- Ajoute un champ upload dans le package manager du module d'importation.

- Ajout d'un champ container_type dans les tables bookshops, cadlibs, mockups et workitems.
ce champ est utilisé par la fonction de recherche generale pour retrouver le container lors de l'affichage du document.
- Ecrit une class QuickForm pour jscalendar.
- ajoute un parametre pour regler la durée de vie des sessions.
- refonte de la gestion des erreurs avec PEAR/ErrorStack.
- Ajout de constantes pour régler les rÌgles de validation des mots de passe.
- Creation de la documentation de class avec doxydoc.

- Divers bugs corrigés.
