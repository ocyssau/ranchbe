<?php
// +----------------------------------------------------------------------+
// | PHP Version 4                                                        |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the PHP license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available at through the world-wide-web at                           |
// | http://www.php.net/license/3_0.txt                                   |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
// | Author:  Giuseppe Dessì <thesee@fastwebnet.it>                       |
// +----------------------------------------------------------------------+

/**
 * Class to dynamically create an HTML input text element that
 * at every keypressed javascript event, call by XMLHttpRequest for sending
 * the request to the server. A PHP script or XML or a static file returns
 * a response in a arbitrary format, depending on the our needed.
 * We can see the data on a live dropdown select
 *
 * @author       Giuseppe Dessì <thesee@fastwebnet.it>
 * @author       Olivier CYSSAU <olivier.cyssau@acensea.com>
 */
require_once 'HTML/QuickForm/text.php';

class HTML_QuickForm_LiveSearch_Select extends HTML_QuickForm_text
{

	/**
	 * Options for the LiveSearch_Select input text element
	 *
	 * @var       array
	 * @access    private
	 */
	var $_options = array();

	/**
	 * Original name of element
	 *
	 * @var       string
	 * @access    private
	 */
	var $realName = '';

	/**
	 * The values passed by the hidden element
	 *
	 * @var array
	 * @access private
	 */
	var $_value = null;
	var $_hidden_value = null;

	/**
	 * Class constructor
	 *
	 * @param     string    $elementName    (required)Input field name attribute
	 * @param     string    $elementLabel   (required)Input field label in form
	 * @param     array     $options        (optional)LiveSearch_Select options
	 * @param     mixed     $attributes     (optional)Either a typical HTML attribute string
	 *                                      or an associative array. Date format is passed along the attributes.
	 * @access    public
	 * @return    void
	 */
	function HTML_QuickForm_LiveSearch_Select($elementName = null, $elementLabel = null, $options = null, $attributes = null)
	{
		$this->HTML_QuickForm_text($elementName, $elementLabel, $attributes);
		$this->_persistantFreeze = true;
		$this->_type = 'livesearch_select';
		if (isset($options)) {
			$this->setOptions($options);
		}
	} //end constructor

	/**
	 * Gets the private name for the element
	 *
	 * @param   string  $elementName The element name to make private
	 *
	 * @access public
	 * @return string
	 */
	function getPrivateName($elementName)
	{
		return 'q'.$elementName;
	}

	/**
	 * Sets the element's value
	 *
	 * @param    mixed   Element's value
	 * @access   public
	 */
	function setValue($value)
	{
		$this->_hidden_value = $value;
		if (isset($this->_options['callback']) AND is_callable($this->_options['callback']) ) {
			if (isset($this->_options['dbh']) ) {
				$dbh = $this->_options['dbh'];
			} else {
				$dbh = NULL;
			}
			$this->_value = call_user_func($this->_options['callback'], $dbh, $value);
			$this->updateAttributes(array(
                                        'value' => $this->_value
			)
			);
		} else {
			$this->updateAttributes(array(
                                        'value' => $value
			)
			);
		}
	}

	/**
	 * Gets the element's value
	 *
	 * @param    mixed   Element's value
	 * @access   public
	 */
	function getValue($internal = NULL)
	{
		if ($this->_flagFrozen) {
			if ($internal) {
				return $this->_value;
			} else {
				return $this->_hidden_value;
			}
		} else {
			return $this->_hidden_value;
		}
	}

	/**
	 * Sets the options for the LiveSearch_Select input text element
	 *
	 * @param     array    $options    Array of options for the LiveSearch_Select input text element
	 * @access    public
	 * @return    void
	 */
	function setOptions($options)
	{
		$this->_options = $options;
	} // end func setOptions

	/**
	 * Returns the value of field without HTML tags
	 *
	 * @since     1.0
	 * @access    public
	 * @return    string
	 */
	function getFrozenHtml()
	{
		$value = $this->getValue(true);
		$id = $this->getAttribute('id');
		return ('' != $value ? htmlspecialchars($value): '&nbsp;').'<input' . $this->_getAttrString(array(
                       'type'  => 'hidden',
                       'name'  => $this->realName,
                       'value' => $this->getValue()
		) + (isset($id)? array('id' => $id): array())) . ' />';
	} //end func getFrozenHtml

	/**
	 * Returns Html for the LiveSearch_Select input text element
	 *
	 * @access      public
	 * @return      string
	 */
	function toHtml()
	{
		$this->realName = $this->getName();
		$liveform = '';
		$scriptLoad = '';
		$style = 'display: block;';
		$divstyle = ' class="divstyle" ';
		$ulstyle = ' class="ulstyle" ';
		$listyle = ' class="listyle" ';
		$zeroLength = 0;
		$printStyle = 1;
		$this->updateAttributes(array(
								'name' => $this->getPrivateName($this->realName)
								));
		if (isset($this->_options['style']) AND $this->_options['style'] != '') {
			$style = $this->_options['style'];
		}
		if (isset($this->_options['divstyle']) AND $this->_options['divstyle'] != '') {
			$divstyle =  ' class="'.$this->_options['divstyle'].'" ';
		}
		if (isset($this->_options['ulstyle']) AND $this->_options['ulstyle'] != '') {
			$ulstyle =  ' class="'.$this->_options['ulstyle'].'" ';
		}
		if (isset($this->_options['listyle']) AND $this->_options['listyle'] != '') {
			$listyle =  ' class="'.$this->_options['listyle'].'" ';
		}
		if (isset($this->_options['searchZeroLength']) AND $this->_options['searchZeroLength'] == 1) {
			$zeroLength = 1;
		} else {
			$zeroLength = 0;
		}
		if (isset($this->_options['printStyle']) AND $this->_options['printStyle'] != 1) {
			$printStyle = 0;
		}
		if (isset($this->_options['autoserverPath']) AND $this->_options['autoserverPath'] != '') {
			$autoserverPath = $this->_options['autoserverPath'];
		} else {
			$autoserverPath = '';
		}
		if ( $this->_options['autoComplete'] ) {
			$this->updateAttributes(array('autocomplete' => 'on'));
		}else{
			$this->updateAttributes(array('autocomplete' => 'off'));
		}
		$inputId = $this->_options['elementId'];
		$divId = $this->_options['elementId'] . '_list';
		$outputId = $inputId . '_output';
		
        //'onkeyup' => 'javascript:ls_showResult(this.value, \''.$this->getName().'Result\', \'target_'.$this->_options['elementId'].'\', \''.$this->_options['elementId'].'\', \''.$this->realName.'\', '.$zeroLength.');',
		$onkeyup = "javascript:ls_showResult(this.value, '$inputId', '$divId', '$autoserverPath');";
		//'onblur' => 'javascript:liveSearchHide(\''.$this->getName().'Result\');'
		$onclick = "javascript:toggleBlock('$divId');";
		
		$this->updateAttributes(array(
                                      'onkeyup' => $onkeyup,
                                      'onclick' => $onclick,
                                      'id' => $this->_options['elementId'],
                                      'style' => $style));
		
		if ($this->_flagFrozen) {
			return $this->getFrozenHtml();
		} else {
			$liveform .= "<div $divstyle id=\"$divId\"></div>";
			$html = parent::toHtml() . $liveform;
			
			//Hidden input to return
			$html .= '<input type="hidden" name="'.$this->realName.'" id="'.$outputId.'" value="'.$this->_hidden_value.'" />';
			
			if (!defined('HTML_QUICKFORM_LIVESEARCH_EXISTS')) {
				define('HTML_QUICKFORM_LIVESEARCH_EXISTS', true);
			}
			if (!defined('HTML_QUICKFORM_JS_INIT_EXISTS')) {
				define('HTML_QUICKFORM_JS_INIT_EXISTS', true);
			}
		}
		
		return $html;
		
	}// end func toHtml

} // end class HTML_QuickForm_LiveSearch_Select




if (class_exists('HTML_QuickForm')) {
	HTML_QuickForm::registerElementType('livesearch_select', 'HTML/QuickForm/livesearch_select.php', 'HTML_QuickForm_LiveSearch_Select');
}


