<?php
/**
 * RanchBE plugin
 * @package Ranchbe
 */

/**
 * Smarty process modifier plugin
 *
 * Type:     modifier<br>
 * Name:     tr<br>
 * Purpose:  translate input<br>
 * Input: $content<br>
 */
function smarty_modifier_process($content=''){
  if ( !empty($content) ) {
    global $language;
    global $lang;
    include_once("lang/$language/language.php");
      if(isset($lang[$content])) {
        return $lang[$content];  
      } else {
        return $content;        
      }
  }
}

/* vim: set expandtab: */
?>
