<?php
/**
 * RanchBe truncate modifier plugin
 *
 * Type:     modifier<br>
 * Name:     truncate<br>
 * Purpose:  Suppress blank space from a string
 */

function smarty_modifier_trim($string , $char='')
{
    //return rtrim($string , $char);

  if (!empty($char) )  return trim($string , $char);

  return trim($string);
}

/* vim: set expandtab: */

?>
