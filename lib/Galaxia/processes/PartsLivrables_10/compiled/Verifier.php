<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/PartsLivrables_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php

	if (isset($_REQUEST['approve'])) {
    $document->LockDocument(5); //Lock document access with special code
    $document->ChangeState('approuve'); //Update state of the document
    $instance->setNextActivity('A_Livrer');
  	$instance->complete();
	}

	if (isset($_REQUEST['reject'])) {
    $document->LockDocument(0); //Lock document access with special code
    $document->ChangeState('rejete'); //Update state of the document
		$instance->setNextActivity('end');
		$instance->complete();
	}

return true;

?>
<?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
