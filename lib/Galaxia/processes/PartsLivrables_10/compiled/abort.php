<?php include_once('C:\wamp\www\ranchBE0.4\lib\Galaxia/processes/PartsLivrables_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a standalone activity
?>
<?php

    if(!isset($document_id)) {print 'none document selected';die;}
    if(!isset($iid)) {print 'none instance selected';die;}

    //Abort the process
    require_once ('galaxia_setup.php');
    include_once 'lib/Galaxia/ProcessManager.php';
    include_once 'lib/Galaxia/API.php';
  	$instanceManager->set_instance_status($iid, 'aborted');

    //Unlink the process from document
  	if ($Manager->UnLinkInstanceToDocument($document_id)){
  		$smarty->assign('msg', tra("The process has been unlinked from the document"));
  		$smarty->display("comment.tpl");
    }else{
  		$smarty->assign('msg', tra("The reset of process has failed"));
  		$smarty->display('application/error/simple.tpl');
    }

    //Change state and access code of document

?>
<?php
//Code to be executed after a standalone activity
?>
<?php
//Code shared by all activities (pos)
?>
