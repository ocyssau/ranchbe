<?php

$document->ChangeState('a_livrer'); //Update state of the document

$instance_id = $instance->getInstanceId();

//Set instance property to record the user wich submit request
$instance->set('a_livrer_user',$user);

$subject = $document->GetProperty('document_number') . ' a livrer';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.$document->GetProperty('document_indice_id').' est bon a livrer<br>';
$instance->setMessageToNextUsers($subject, $body);

return true;
?>
