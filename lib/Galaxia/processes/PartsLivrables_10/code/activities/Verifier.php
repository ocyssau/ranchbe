<?php

	if (isset($_REQUEST['approve'])) {
    $document->LockDocument(5); //Lock document access with special code
    $document->ChangeState('approuve'); //Update state of the document
    $instance->setNextActivity('A_Livrer');
  	$instance->complete();
	}

	if (isset($_REQUEST['reject'])) {
    $document->LockDocument(0); //Lock document access with special code
    $document->ChangeState('rejete'); //Update state of the document
		$instance->setNextActivity('end');
		$instance->complete();
	}

return true;

?>
