<?php

if (isset($_REQUEST['approve'])) {
  $document->LockDocument(5); //Lock document access with special code
  $document->ChangeState('approuveCalcul'); //Update state of the document
  $instance->setNextActivity('a_livrer');
	$instance->complete();
  //record property to statistic
  $instance->set('check','approuveCalcul');
  $subject = $Documentinfos['document_number'] . ' est valid� par le calcul';
  $body = 'le plan ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' a �t� valid� par '.$user.' :<br>';
  $to = array( $instance->getOwner(), $instance->get('previousUser') ); //Get the previous user from the properties
  $instance->sendMessage($to, $subject, $body);
}

if (isset($_REQUEST['reject'])) {
  $document->LockDocument(0); //Lock document access with special code
  $document->ChangeState('rejeteCalcul'); //Update state of the document
	$instance->setNextActivity('end');
	$instance->complete();
  //record property to statistic
  $instance->set('check','rejeteCalcul');

  //Send the error_note and error_code to user wich request the verification
  $subject = $Documentinfos['document_number'] . ' est rejet� par le calcul';
  $body = 'le plan ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' est rejet� pour les raisons suivantes :<br>';
  if( is_array($_REQUEST['error_code']) )
  foreach($_REQUEST['error_code'] as $error){
    if( $error == 'autre' ){
      $body .= '<b>Autre</b><br />';
    }else{
      $body .= '<b>'.$error.'</b><br />';
    }
  }
  //Send the error_note and error_code to user wich request the verification
  if(!empty($_REQUEST['error_note']))
    $body .= '<b>Commentaires additionnels</b> : '.trim($_REQUEST['error_note']).'<br />';

  $instance->set('error_code',$_REQUEST['error_code']);//record errors in property to statistic
  $instance->set('error_note',trim($_REQUEST['error_note'])); //record notes in property to statistic
  $priority = 1;
  $to = array( $instance->getOwner(), $instance->get('previousUser') ); //Get the previous user from the properties
  $instance->sendMessage($to, $subject, $body);
}

$instance->set('verifier_calcul_user',$user);
$instance->set('previousActivity','verification_calcul');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser
return true;
?>