<?php
$smarty = Ranchbe::getView();
$smarty->assign('title', 'Check rt for documents');

//var_dump($params, $_SESSION);

$document_ids =& $params[0];
$container =& $params[1];

require_once('./class/Rt.php');

$list = array();

foreach($document_ids as $document_id){
	$document =& $container->initDoc($document_id);
	$Rt = new Rt();
	
	$filter = ' rt_status = \''.Rt::$_appliedStatus.'\'';
	//$filter .= ' AND rt_access_code < 15';
	$filter .= ' AND applyto_version = ' . $document->GetProperty('document_version');
	$select = array();
	$rs = $Rt->GetRtApplyToDocument($document, false, true, $select, $filter);
	
	if(!$rs) return;
	$i=0;
	while( $row = $rs->FetchRow() ){
		//var_dump($row);
		$list[$document_id]['rt'][$i] = $row;
		$list[$document_id]['id'] = $document_id;
		$list[$document_id]['number'] = $document->GetNumber();
		$list[$document_id]['version'] = $document->GetProperty('document_indice_id');
		$list[$document_id]['iteration'] = $document->GetProperty('document_version');
		$i++;
	}
}
//var_dump($list);

$smarty->assign('list', $list);
$smarty->assign('space', $container->SPACE_NAME);
