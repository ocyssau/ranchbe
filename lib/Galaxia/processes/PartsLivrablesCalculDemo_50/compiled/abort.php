<?php include_once('/var/www/ranchbe/lib/Galaxia/processes/PartsLivrablesCalculDemo_50/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a standalone activity
?>
<?php

$doc_normalized_name = $document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') );

$subject='le plan '.$doc_normalized_name.' a �t� annul�';
$body='le plan '.$doc_normalized_name.' a �t� <b>annul�</b> par '.$user.'<br>';

$to = array( $instance->get('previousUser') );
$instance->sendMessage($to, $subject, $body);
$instance->setMessageToNextUsers($subject, $body);

if( $docflow->resetProcess() ){
  $document->errorStack->push(ERROR, 'Info', array('element'=>$document->GetNumber() ), 'le process du document %element% a ete annule');
  return true;
}else{
  return false;
}
?><?php
//Code to be executed after a standalone activity
?>
<?php
//Code shared by all activities (pos)
?>
