<?php

//Recupere les fichiers associ�s au document
$afiles = $document->GetAssociatedFiles();
$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

//$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('a_verifier'); //Update state of the document


$instance->set('a_verifier_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_verifier'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);

return true;

?>
