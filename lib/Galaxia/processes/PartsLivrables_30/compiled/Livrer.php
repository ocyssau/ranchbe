<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/PartsLivrables_30/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before an activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
if ($instance->getActivityStatus($_REQUEST['activityId']) == "completed")
{
	$smarty->assign("msg",tra("This instance of activity is already complete"));
	$smarty->display('application/error/error.tpl');
	die;
}

// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}
?>
<?php
$document->LockDocument(10); //Lock document access with special code
$document->ChangeState('Livrer'); //Update state of the document

$livraison_deposit_dir = '/DATA6/livraison/a_livrer/ranchbe';
$document->CopyAssociatedFiles($livraison_deposit_dir , true);

//Corrige bug primes qui bloque les import de fichier dont le mtime du xml est dans la meme seconde
//que le mtime de fichier CATIA
$afiles = $document->GetAssociatedFiles();
$mainfiles = $livraison_deposit_dir .'/'. $afiles[0]['file_name'];
if (is_file("$mainfiles" . ".qcseal") ){
  $date_mainf = filemtime("$mainfiles");
  touch("$mainfiles" . ".qcseal", $date_mainf + 2);
}

$instance->set('previousActivity','livrer');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;

?>
<?php
//Code to be executed after an activity
?>
<?php
//Code shared by all activities (pos)
?>
