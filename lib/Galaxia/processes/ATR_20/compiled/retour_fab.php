<?php include_once('/var/www/ranchbe/lib/Galaxia/processes/ATR_20/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php

if (isset($_REQUEST['approuve'])) {
    $document->ChangeState('approuve'); //Update state of the document
    $instance->setNextActivity('livrer');
    $instance->complete();
    $instance->set('retour_fab_user',$user);
}

if (isset($_REQUEST['rejeter'])) {
    $document->ChangeState('rejete'); //Update state of the document
    $instance->setNextActivity('rejeter');
    $instance->complete();
    $instance->set('retour_fab_user',$user);

    //Send the error_note and error_code to user wich request the verification
    //include_once './class/messu/messulib.php';
    $subject = $Documentinfos['document_number'] . ' rejet�';
    $body = 'le plan ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' est rejet� pour les raisons suivantes :<br>';
    if( is_array($_REQUEST['error_code']) )
      foreach($_REQUEST['error_code'] as $error){
        $body .= '<b> '. $error .' </b><br>';
      }
    //record errors in property to statistic
    $instance->set('error_code',$_REQUEST['error_code']);
    if(!empty($_REQUEST['error_note']))
      $body .= '<b>Notes</b> : <br>' . trim($_REQUEST['error_note']);
    $priority = 1;
    //record property to statistic
    $instance->set('error_note',trim($_REQUEST['error_note']));

    $to = $instance->getOwner();
    $instance->sendMessage($to, $subject, $body);
	}

$instance->set('previousActivity','retour_fab');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;

?><?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
