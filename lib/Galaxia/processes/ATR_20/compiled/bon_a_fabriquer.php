<?php include_once('/var/www/ranchbe/lib/Galaxia/processes/ATR_20/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a start activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId) && isset($_REQUEST['iid'])) {
  // in case we're looping back to a start activity, we need to retrieve the instance
  $instance->getInstance($_REQUEST['iid']);
} else {
  // otherwise we'll create an instance when this activity is completed
}

?>
<?php
require_once('./inc/activities/lib.php');

//===================================================
//Verifie la presence et les dates du pdf
//===================================================
$msg3 = Activities_Lib_CheckPdf($document);
if($msg3 === false){
	return false;
}

$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('bon_a_fabriquer'); //Update state of the document

$instance->set('bon_a_fabriquer',$user);
$instance->set('previousActivity','bon_a_fabriquer');
$instance->set('previousUser',$user);

return true;

?><?php
//Code to be executed after a start activity
if(isset($_REQUEST['name']))
	$instance->setName($_REQUEST['name']);
?>
<?php
//Code shared by all activities (pos)
?>
