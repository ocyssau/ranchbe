<?php
require_once('./inc/activities/lib.php');

//===================================================
//Verifie la presence et les dates du pdf
//===================================================
$msg3 = Activities_Lib_CheckPdf($document);
if($msg3 === false){
	return false;
}

$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('bon_a_fabriquer'); //Update state of the document

$instance->set('bon_a_fabriquer',$user);
$instance->set('previousActivity','bon_a_fabriquer');
$instance->set('previousUser',$user);

return true;

?>