<?php
$document->LockDocument(10); //Lock document access with special code
$document->ChangeState('livre'); //Update state of the document

$livraison_deposit_dir = '/DATA6/livraison/a_livrer/ranchbe';
$document->CopyAssociatedFiles($livraison_deposit_dir , true);

$instance->set('previousActivity','livrer');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;
?>