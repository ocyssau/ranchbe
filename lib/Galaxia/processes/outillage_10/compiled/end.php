<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/outillage_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before the end activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php
$document->UnLinkInstanceToDocument();
return true;
?><?php
//Code to be executed after the end activity
?>
<?php
//Code shared by all activities (pos)
?>
