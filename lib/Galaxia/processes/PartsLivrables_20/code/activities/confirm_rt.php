<?php
//Get list of documents to process
//var_dump($_REQUEST['rt_validation']);
//var_dump($_REQUEST['rt_associated_document']);
//var_dump($container->SPACE_NAME);

$space = new space($container->SPACE_NAME);

require_once('./class/Rt.php');
$Rt = new Rt();
$rs = $Rt->GetRtApplyToDocument($document, false, true);

if(!$rs) return;

$i=0;
$validTag = true;
$reports = array();
while( $row = $rs->FetchRow() ){
	if($row['rt_status'] == 'rt_applied'){  //only on applied rt
		$valid = $_REQUEST['rt_validation'][$row['rt_id']];
		$RtDoc = new document($space, $row['rt_id']);
		if($valid){
			$RtDoc->ChangeState('rt_confirmed');
		}else{
			$reports[] = 'Le refus ' . $row['rt_number'] . ' n\'est pas correctement appliqu� sur ce document';
			$RtDoc->LockDocument(0); //Lock document access with special code
			$RtDoc->ChangeState('rt_rejected');
			$validTag = false;
		}
	}
	$i++;
}

if( !$validTag ){
	$document->LockDocument(0); //Lock document access with special code
	$document->ChangeState('rejete'); //Update state of the document

	$instance->setNextActivity('end');
	$instance->complete();
	$instance->set('check','reject');
}else{
	$instance->setNextActivity('Verifier');
	$instance->complete();
	$instance->set('check','approve');
}
$instance->set('previousActivity','confirm_rt');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser
$instance->set('confirm_rt_user',$user);


//Send the error_note and error_code to user wich request the verification
if( !$validTag ){
	$documentName = $document->GetNumber() . '.v' . $document->GetProperty('document_indice_id');
	$subject = $documentName . ' rejet�';
	$body = 'le plan ' .$documentName.' est rejet� pour les raisons suivantes :<br>';
	foreach($reports as $report){
		$body .= $report . '<br />';
	}
	$instance->set('error_code','rtnotapply');//record errors in property to statistic
	$priority = 1;
	$to = $instance->getOwner(); //Get the previous user from the properties
	$instance->sendMessage($to, $subject, $body);
}
return true;

?>
