<?php
require_once './class/Rt.php';

//Check that document is a doctype
$doctype_id = $document->GetProperty('doctype_id');
if($doctype_id != Rt::$_doctype_id){
	$msg = 'This document is not a rt';
	$document->errorStack->push(ERROR, 'Error', array(), $msg);
	return false;
}

$document->ChangeState('rt_applied');
$document->LockDocument(10); //validate

?>