<?php

//Recupere les fichiers associes au document
$afiles = $document->GetAssociatedFiles();
$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

//===================================================
//Verifie la presence et les dates du qcseal
//===================================================
//Verifie le xml uniquement pour les documents catia
if ($afiles[0]['file_extension'] == '.CATPart' || 
$afiles[0]['file_extension'] == '.CATProduct' || 
$afiles[0]['file_extension'] == '.CATDrawing')
{
  //Verifie la presence du qcseal
  if (! is_file("$mainfiles" . ".qcseal") ){
    /*print "<b>INFOS: </b><br/>";
    print "<b>le fichier .qseal est abscent<b/><br/>";
    print "Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache<br/>";
    print '<form><input type="button" onclick="window.close()" value="Close"></form><br />';
    */
    $msg = 'Le fichier .qseal est abscent';
    $msg .= 'Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache';
    $document->errorStack->push(ERROR, 'Error', array(), $msg);
    return false;
  }

  //Verifie la concordance des dates du qcseal
  $date_xmlf = filemtime("$mainfiles" .'.qcseal');
  $date_mainf = filemtime("$mainfiles");
  $time_shift = $date_xmlf - $date_mainf;
  if ( $time_shift < -2 ) {
  //if ( $time_shift > 50 || $time_shift < -2 ) {
  //if ( $time_shift < 0 ) {
    /*print  "<b>INFOS: </b><br/>";
    print  "<b>Le fichier qchecker nest pas a jour </b><br/>";
    print  "Vous devez regenerer le fichier qchecker pour pouvoir executer cette tache<br/>";
    print '<i>mtime du fichier qchecker:</i> ' . $date_xmlf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
    print '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
    print '<form><input type="button" onclick="window.close()" value="Close"></form><br />';
    */
    $msg = 'Le fichier qchecker nest pas a jour';
    $msg .= 'Vous devez regenerer le fichier qchecker pour pouvoir executer cette tache';
    $msg .= '<i>mtime du fichier qchecker</i>: '. $date_xmlf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
    $msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
    $document->errorStack->push(ERROR, 'Error', array(), $msg);
    return false;
  }

  //TODO : Verifier les erreurs dans le fichiers qchecker
}

//===================================================
//Verifie la presence et les dates du pdf
//===================================================
if ($afiles[0]['file_extension'] == '.CATDrawing')
{
  $pdf_file = $afiles[0]['file_path'] .'/'. $afiles[0]['file_root_name']. '.pdf';
  //Verifie la presence du pdf
  if (! is_file($pdf_file) ){
    $msg = 'Le fichier '.$pdf_file.' est abscent';
    $msg .= 'Vous devez generer le fichier .pdf et le lier a ce document pour pouvoir executer cette tache';
    $document->errorStack->push(ERROR, 'Error', array(), $msg);
    return false;
  }

  //Verifie la concordance des dates du pdf
  $date_pdf = filemtime($pdf_file);
  $date_mainf = filemtime($mainfiles);
  $time_shift = $date_pdf - $date_mainf;
  if ( $time_shift < -30 ) {
    $msg = 'Le fichier '.$pdf_file.' nest pas a jour';
    $msg .= 'Vous devez regenerer le fichier pdf pour pouvoir executer cette tache';
    $msg .= '<i>mtime du fichier pdf</i>: '. $date_pdf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
    $msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
    $document->errorStack->push(ERROR, 'Error', array(), $msg);
    return false;
  }
}

$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('a_verifier'); //Update state of the document

$instance->set('a_verifier_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_verifier'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);
?>
