<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/PartsLivrables_20/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before an activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
if ($instance->getActivityStatus($_REQUEST['activityId']) == "completed")
{
	$smarty->assign("msg",tra("This instance of activity is already complete"));
	$smarty->display('application/error/error.tpl');
	die;
}

// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}
?>
<?php
$document->ChangeState('A_Livrer'); //Update state of the document

$instance_id = $instance->getInstanceId();

$instance->set('a_livrer_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_livrer'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

$subject = $document->GetProperty('document_number') . ' a livrer';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.$document->GetProperty('document_indice_id').' est bon a livrer<br>';
$instance->setMessageToNextUsers($subject, $body);
return true;
?>
<?php
//Code to be executed after an activity
?>
<?php
//Code shared by all activities (pos)
?>
