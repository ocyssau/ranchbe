<?php
$format='%Y%m%d';

$livraison_deposit_dir='/DATA6/livraison/a_livrer/ranchbe/liv-'.$Manager->GetCurrentContainerNum().'-'.strftime($format,time());
if(!is_dir($livraison_deposit_dir)){
	if(!mkdir($livraison_deposit_dir)){
		$msg = 'la creation du repertoire'.$livraison_deposit_dir.'est impossible';
		$document->errorStack->push(ERROR, 'Error', array(), $msg);
		return false;
	}

	$document->CopyAssociatedFiles($livraison_deposit_dir , true);

	$document->LockDocument(10); //Lock document access with special code
	$document->ChangeState('Livrer'); //Update state of the document
}
