<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/LivraisonSimple_20/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a start activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId) && isset($_REQUEST['iid'])) {
  // in case we're looping back to a start activity, we need to retrieve the instance
  $instance->getInstance($_REQUEST['iid']);
} else {
  // otherwise we'll create an instance when this activity is completed
}

?>
<?php

$format='%Y%m%d';

$livraison_deposit_dir='/DATA6/livraison/a_livrer/ranchbe/liv-'.$Manager->GetCurrentContainerNum().'-'.strftime($format,time());
if(!is_dir($livraison_deposit_dir)){
 if(!mkdir($livraison_deposit_dir)){
   print 'la creation du repertoire'.$livraison_deposit_dir.'est impossible';
   die;
 }
}

$document->CopyAssociatedFiles($livraison_deposit_dir , true);

$document->LockDocument(10); //Lock document access with special code
$document->ChangeState('Livrer'); //Update state of the document

?>
<?php
//Code to be executed after a start activity
if(isset($_REQUEST['name']))
	$instance->setName($_REQUEST['name']);
?>
<?php
//Code shared by all activities (pos)
?>
