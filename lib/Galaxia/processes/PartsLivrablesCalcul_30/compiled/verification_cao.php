<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/PartsLivrablesCalcul_30/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php

if (isset($_REQUEST['approve'])) {
  $document->LockDocument(5); //Lock document access with special code
  $document->ChangeState('approuve'); //Update state of the document
  //Si calcul est requis le plan est envoy� a la verif calcul
  if($document->GetProperty('need_calcul') == 'oui'){
    $instance->setNextActivity('verification_calcul');
    //On envoie un message au verificateur calcul
    $subject = $document->GetProperty('document_number') . ' a verifier';
    $body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' � �t� valid� CAO par '.$user.' et est � verifier par le calcul<br>';
    $body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
    $instance->setMessageToNextUsers($subject, $body);
  }else{ //Sinon le plan est a livrer
    $instance->setNextActivity('a_livrer');
  }
	$instance->complete();
  //record property to statistic
  $instance->set('verifier_user',$user);
  $instance->set('check','approve');
}

if (isset($_REQUEST['reject'])) {
  $document->LockDocument(0); //Lock document access with special code
  $document->ChangeState('rejeteCao'); //Update state of the document
	$instance->setNextActivity('end');
	$instance->complete();
  //record property to statistic
  $instance->set('verifier_user',$user);
  $instance->set('check','rejeteCao');

  //Send the error_note and error_code to user wich request the verification
  $subject = $Documentinfos['document_number'] . ' rejet�';
  $body = 'le plan ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' est rejet� pour les raisons suivantes :<br>';
  if( is_array($_REQUEST['error_code']) )
  foreach($_REQUEST['error_code'] as $error){
    if( $error == 'autre' ){
      $body .= '<b>Autre</b><br />';
    }else{
      $body .= '<b>'.$error.'</b><br />';
    }
  }
  //Send the error_note and error_code to user wich request the verification
  if(!empty($_REQUEST['error_note']))
    $body .= '<b>Commentaires additionnels</b> : '.trim($_REQUEST['error_note']).'<br />';

  $instance->set('error_code',$_REQUEST['error_code']);//record errors in property to statistic
  $instance->set('error_note',trim($_REQUEST['error_note'])); //record notes in property to statistic
  $priority = 1;
  $to = $instance->getOwner(); //Get the previous user from the properties
  $instance->sendMessage($to, $subject, $body);
}

$instance->set('previousActivity','verifier');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser
return true;
?>
<?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
