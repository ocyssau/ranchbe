{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h3>Verification CALCUL</h3>
<br />
<form method="post">

<fieldset>
  <legend>Approuver</legend>
  <ul>
	<input type="submit" name="approve" value="{tr}Approve{/tr}" />
  </ul>
</fieldset>

<fieldset>
  <legend>Rejeter</legend>
  <ul>
    <label><input type="checkbox" name="error_code[]" value="indice" id="indice"/><b>Indice non conforme</b></label><br />
    <label><input type="checkbox" name="error_code[]" value="geometrie" id="geometrie"/><b>Geometrie non conforme</b></label><br />
    <label><input type="checkbox" name="error_code[]" value="fem" id="fem"/><b>FEM non conforme</b></label><br />
  	<input type="submit" name="reject" value="{tr}Reject{/tr}" />
  </ul>
</fieldset>

<input type="submit" name="cancel" value="{tr}Cancel{/tr}" />

</form>

