<?php
$document->ChangeState('A_Livrer'); //Update state of the document

$instance_id = $instance->getInstanceId();

$instance->set('a_livrer_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_livrer'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

$subject = $document->GetProperty('document_number') . ' a livrer';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.$document->GetProperty('document_indice_id').' est bon a livrer<br>';
//$instance->setMessageToNextUsers($subject, $body);
return true;
?>