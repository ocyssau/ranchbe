<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/PartStandard_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a start activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId) && isset($_REQUEST['iid'])) {
  // in case we're looping back to a start activity, we need to retrieve the instance
  $instance->getInstance($_REQUEST['iid']);
} else {
  // otherwise we'll create an instance when this activity is completed
}

?>
<?php
$instance->complete();
return true;
?>
<?php
//Code to be executed after a start activity
if(isset($_REQUEST['name']))
	$instance->setName($_REQUEST['name']);
?>
<?php
//Code shared by all activities (pos)
?>
