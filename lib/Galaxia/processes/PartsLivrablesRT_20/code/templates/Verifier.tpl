{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h3>Approuver ou rejeter</h3>
<br />
<form method="post">

<fieldset>
  <legend>Approuver</legend>
  <ul>
	<input type="submit" name="approve" value="{tr}Approve{/tr}" />
  </ul>
</fieldset>

<fieldset>
  <legend>Rejeter</legend>
  <ul>
    <label><input type="checkbox" name="error_code[]" value="dessin" id="dessin"/><b>dessin</b></label><br />
    <label><input type="checkbox" name="error_code[]" value="conception" id="conception"/><b>conception</b></label><br />
    <label><input type="checkbox" name="error_code[]" value="format" id="format"/><b>format</b></label><br />
    <label><input type="checkbox" name="error_code[]" value="autre" id="autre"/><b>autre</b></label><br />
    <label><b>Note</b><br /><textarea name="error_note" id="error_note" cols="40" rows="5" ></textarea></label><br />
  	<input type="submit" name="reject" value="{tr}Reject{/tr}" />
  </ul>
</fieldset>

<input type="submit" name="cancel" value="{tr}Cancel{/tr}" />

</form>