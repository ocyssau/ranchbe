<?php

//Recupere les fichiers associes au document
$afiles = $document->GetAssociatedFiles();
$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

require_once('./inc/activities/lib.php');

//===================================================
//Verifie les refus
//===================================================
$msg1 = Activities_Lib_CheckRt($document);
if($msg1 === false){
	return false;
}


//===================================================
//Verifie la presence et les dates du qcseal
//===================================================
$msg2 = Activities_Lib_CheckQcseal($document);
if($msg2 === false){
	return false;
}

//===================================================
//Verifie la presence et les dates du pdf
//===================================================
$msg3 = Activities_Lib_CheckPdf($document);
if($msg3 === false){
	return false;
}




$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('a_verifier'); //Update state of the document

$instance->set('a_verifier_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_verifier'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);

?>