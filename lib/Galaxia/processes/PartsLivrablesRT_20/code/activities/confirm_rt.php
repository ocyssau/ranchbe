<?php
//Get list of documents to process
//var_dump($_REQUEST['rt_validation']);
//var_dump($_REQUEST['rt_associated_document']);
//var_dump($container->SPACE_NAME);
/* Pour tromper le verificateur automatique:
$instance->complete();
$instance->setNextActivity($actname);
*/


require_once('./inc/activities/lib.php');

$reports = Activities_Lib_ConfirmRt($document, $instance, 'Verifier', 'end');

$instance->set('previousActivity','confirm_rt');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser
$instance->set('confirm_rt_user',$user);


//Send the error_note and error_code to user wich request the verification
if( $reports ){
	$documentName = $document->GetNumber() . '.v' . $document->GetProperty('document_indice_id');
	$subject = $documentName . ' rejet�';
	$body = 'le plan ' .$documentName.' est rejet� pour les raisons suivantes :<br>';
	foreach($reports as $report){
		$body .= $report . '<br />';
	}
	$instance->set('error_code','rtnotapply');//record errors in property to statistic
	$priority = 1;
	$to = $instance->getOwner(); //Get the previous user from the properties
	$instance->sendMessage($to, $subject, $body);
}

return true;

?>