<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/suiviValidationCalculMeubles_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php


if (isset($_REQUEST['approve'])) {
	$instance->setNextActivity('valider');
	$instance->complete();
	//record property to statistic
	$subject = $Documentinfos['document_number'] . ' est valid� par le client';
	$body = 'le document ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' a �t� valid� par '.$user.' :<br>';
	$to = array( $instance->getOwner(), $instance->get('previousUser') ); //Get the previous user from the properties
	$instance->sendMessage($to, $subject, $body);
}

if (isset($_REQUEST['reject'])) {
	$instance->setNextActivity('rejeter');
	$instance->complete();
	//record property to statistic
	$comments = array();

	//Send the error_note and error_code to user wich request the verification
	$subject = $Documentinfos['document_number'] . ' est rejet� par le client';
	$body = 'le document ' .$Documentinfos['document_number'].'-'.$Documentinfos['document_indice_id'].' est rejet� pour les raisons suivantes :<br>';
	//Send the error_note and error_code to user wich request the verification
	if(!empty($_REQUEST['error_note'])){
		$body .= trim($_REQUEST['error_note']).'<br />';
	}

	$instance->set('error_note',trim($_REQUEST['error_note'])); //record notes in property to statistic
	$priority = 1;
	$to = array( $instance->getOwner(), $instance->get('previousUser') ); //Get the previous user from the properties
	$instance->sendMessage($to, $subject, $body);

	$comments[] = 'rejet� par le client';
	$comments[] = trim($_REQUEST['error_note']);
	$document->history['comment'] = implode('<br />', $comments);
	
}

$instance->set('previousUser',$user);//Set la propriete previousUser
return true;


//on ne peut pas se passer de la balise fermante :
?><?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
