<?php
$document->ChangeState('a_verifier'); //Update state of the document

$instance->set('a_verifier',$user);
$instance->set('previousActivity','a_verifier');
$instance->set('previousUser',$user);

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);

return true;
//on ne peut pas se passer de la balise fermante :
?>