{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h3>Retour client</h3>
<br />
<form method="post">

<fieldset>
  <legend>Approuver</legend>
  <ul>
	<input type="submit" name="approve" value="{tr}Approve{/tr}" />
  </ul>
</fieldset>

<fieldset>
  <legend>Rejeter</legend>
  <ul>
    <label><b>Note</b><br />
    <textarea name="error_note" id="error_note" cols="40" rows="5" ></textarea>
    </label><br />
  	<input type="submit" name="reject" value="{tr}Reject{/tr}" />
  </ul>
</fieldset>

<input type="submit" name="cancel" value="{tr}Cancel{/tr}" />

</form>

