{*Smarty template*}

{include file="layouts/htmlheader.tpl"}

<h3>Changement de donn�es d'entr�e</h3>

<form method="post">

<fieldset>
  <legend>Analyse</legend>
  
<ul>
	<li><input type="checkbox" name="newDoc" id="newDoc">Cr�er un nouvel indice et annuler le workflow en cours</li>
	<li><input type="checkbox" name="newMef" id="newMef">MEF � refaire</li>
	<li><input type="checkbox" name="newDossier" id="newDossier">Dossier � refaire</li>
	<li><input type="checkbox" name="autre" id="autre">Autre</li>
    <li><label><b>Note</b><br />
    <textarea name="note" id="note" cols="40" rows="5" ></textarea>
    </label></li>
  </ul>
</fieldset>

<input type="hidden" name="instanceId" value="{$instanceId}" />

<input type="submit" name="validate" value="Ok" />
<input type="submit" name="cancel" value="Cancel" />

</form>
