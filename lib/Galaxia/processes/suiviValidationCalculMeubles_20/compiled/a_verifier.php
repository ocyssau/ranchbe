<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/suiviValidationCalculMeubles_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before an activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
if ($instance->getActivityStatus($_REQUEST['activityId']) == "completed")
{
	$smarty->assign("msg",tra("This instance of activity is already complete"));
	$smarty->display('application/error/error.tpl');
	die;
}

// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}
?>
<?php
$document->ChangeState('a_verifier'); //Update state of the document

$instance->set('a_verifier',$user);
$instance->set('previousActivity','a_verifier');
$instance->set('previousUser',$user);

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);

return true;
//on ne peut pas se passer de la balise fermante :
?><?php
//Code to be executed after an activity
?>
<?php
//Code shared by all activities (pos)
?>
