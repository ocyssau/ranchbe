<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/suiviValidationCalculMeubles_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a standalone activity
?>
<?php

if ( $_REQUEST['validate'] ) {
	$newDoc = $_REQUEST['newDoc'];
	$newMef = $_REQUEST['newMef'];
	$newDossier = $_REQUEST['newDossier'];
	$autre = $_REQUEST['autre'];
	$note = trim($_REQUEST['note']);
	
	$comments = array();
	//$comments[] = 'ChgtDocsEntrees';
	
	if($newMef){
		chgtde_newmef($docflow, $document);
		$comments[] = 'Refaire MEF';
	}
	
	if($newDossier){
		chgtde_newdossier($docflow, $document);
		$comments[] = 'Refaire Dossier';
	}
	
	if($autre){
		chgtde_autre($docflow, $document);
	}
	
	if($note){
		chgtde_note($docflow, $document);
		$comments[] = $note;
	}
	
	if($newDoc){
		chgtde_newdoc($docflow, $document, $comments);
	}
	
	
	chgtde_sendMessageToOwners($docflow, $document);

	$document->history['action_name'] = 'ChgtDocsEntrees';
	$document->history['comment'] = implode('<br />', $comments);
	//$document->WriteHistory();
	//$document->history['comment'] = ''; //reinit history
}
return true;



function chgtde_newdoc($docflow, $document, $comments){
	$docflow->resetProcess();
	$document->ChangeState('evolutionDE'); //Update state of the document
	$document->LockDocument(10); //Lock document access with special code
	$document->UnLinkInstanceToDocument();
	$document_id = $document->UpgradeIndice(); //nouveau document id
	//$newDocument = new document($document->space, $document_id);
	//$newDocument->history['action_name'] = 'ChgtDocsEntrees';
	//$newDocument->history['comment'] = implode('<br />', $comments);
	//$newDocument->WriteHistory();
	//$newDocument->history = array(); //reinit history
	return $document_id;
}


function chgtde_newmef($docflow, $document){
	return;
}


function chgtde_newdossier($docflow, $document){
	return;
}


function chgtde_autre($docflow, $document){
	return;
}


function chgtde_note($docflow, $document){
	return;
}

function chgtde_sendMessageToOwners($docflow, $document){
	return;
}


?><?php
//Code to be executed after a standalone activity
?>
<?php
//Code shared by all activities (pos)
?>
