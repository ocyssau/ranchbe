<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/A400M_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a start activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId) && isset($_REQUEST['iid'])) {
  // in case we're looping back to a start activity, we need to retrieve the instance
  $instance->getInstance($_REQUEST['iid']);
} else {
  // otherwise we'll create an instance when this activity is completed
}

?>
<?php

//Recupere les fichiers associes au document
$afiles = $document->GetAssociatedFiles();
$mainfiles = $afiles[0]['file_path'] .'/'. $afiles[0]['file_name'];

require_once('./inc/activities/lib.php');

$document->LockDocument(5); //Lock document access with special code
$document->ChangeState('a_verifier'); //Update state of the document

$instance->set('a_verifier_user',$user); //Set instance property to record the user wich submit request
$instance->set('previousActivity','a_verifier'); //Set la propriete previousActivity
$instance->set('previousUser',$user); //Set la propriete previousUser

//Send a message to next users
$subject = $document->GetProperty('document_number') . ' a verifier';
$body = 'le plan ' .$document->GetProperty('document_number').'-'.sprintf( "(indice SI%02s)",$document->GetProperty('document_indice_id') ).' est a verifier<br>';
$body .= '<b>Conteneur :<b> ' . $container->GetName() .'<br>';
$instance->setMessageToNextUsers($subject, $body);
?><?php
//Code to be executed after a start activity
if(isset($_REQUEST['name']))
	$instance->setName($_REQUEST['name']);
?>
<?php
//Code shared by all activities (pos)
?>
