<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/suivi08415_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php

if ($_REQUEST['accepter']) {
    $instance->setNextActivity('accepter_annulation_odf');
    $instance->complete();
    $instance->set('suivi_annulation_user',$user);

}

if ($_REQUEST['rejeter']) {
    $instance->setNextActivity('rejeter_annulation_odf');
    $instance->complete();
    $instance->set('suivi_annulation_user',$user);
}

$instance->set('previousActivity','suivi_annulation');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;

?><?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
