<?php include_once('/SHARES/SIER/SCRIPTS/ranchbe/lib/Galaxia/processes/suivi08415_10/code/shared.php'); ?>
<?php
//Code shared by all the activities (pre)
?>
<?php
//Code to be executed before a switch activity
// If we didn't retrieve the instance before
if(empty($instance->instanceId)) {
  // This activity needs an instance to be passed to 
  // be started, so get the instance into $instance.
  if(isset($_REQUEST['iid'])) {
    $instance->getInstance($_REQUEST['iid']);
  } else {
    // defined in lib/Galaxia/config.php
    galaxia_show_error("No instance indicated");
    die;  
  }
}
// Set the current user for this activity
if(isset($user) && !empty($instance->instanceId) && !empty($activity->activityId)) {
  $instance->setActivityUser($activity->activityId,$user);
}

?>
<?php

if ($_REQUEST['receptionner']) {
    $instance->setNextActivity('receptionner');
    $instance->complete();
    $instance->set('suivi_fab_user',$user);
    
    $masse = trim($_REQUEST['masse_mak']);
    $mat = trim($_REQUEST['materiau_mak']);
    $odf = trim($_REQUEST['odf']);

    $props = array();
    if($odf) $props['father_ds']=$odf;
    if($masse) $props['masse_mak']=$masse;
    if($mat) $props['materiau_mak']=$mat;
    
	//echo $masse; echo $mat;
    $document->UpdateDocument($props);
}

if ($_REQUEST['annuler_odf']) {
    $instance->setNextActivity('annuler_odf');
    $instance->complete();
    $instance->set('suivi_fab_user',$user);
}

$instance->set('previousActivity','suivi_fab');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;


?><?php
//Code to be executed after a switch activity
?>
<?php
//Code shared by all activities (pos)
?>
