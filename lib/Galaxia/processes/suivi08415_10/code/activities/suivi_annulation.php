<?php

if ($_REQUEST['accepter']) {
    $instance->setNextActivity('accepter_annulation_odf');
    $instance->complete();
    $instance->set('suivi_annulation_user',$user);

}

if ($_REQUEST['rejeter']) {
    $instance->setNextActivity('rejeter_annulation_odf');
    $instance->complete();
    $instance->set('suivi_annulation_user',$user);
}

$instance->set('previousActivity','suivi_annulation');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;

?>