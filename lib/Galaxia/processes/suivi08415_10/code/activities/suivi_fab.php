<?php

if ($_REQUEST['receptionner']) {
    $instance->setNextActivity('receptionner');
    $instance->complete();
    $instance->set('suivi_fab_user',$user);
    
    $masse = trim($_REQUEST['masse_mak']);
    $mat = trim($_REQUEST['materiau_mak']);
    $odf = trim($_REQUEST['odf']);

    $props = array();
    if($odf) $props['father_ds']=$odf;
    if($masse) $props['masse_mak']=$masse;
    if($mat) $props['materiau_mak']=$mat;
    
	//echo $masse; echo $mat;
    $document->UpdateDocument($props);
}

if ($_REQUEST['annuler_odf']) {
    $instance->setNextActivity('annuler_odf');
    $instance->complete();
    $instance->set('suivi_fab_user',$user);
}

$instance->set('previousActivity','suivi_fab');//Set la propriete previousActivity
$instance->set('previousUser',$user);//Set la propriete previousUser

return true;


?>