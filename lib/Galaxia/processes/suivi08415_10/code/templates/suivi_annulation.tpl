{*Smarty template*}
{include file="layouts/htmlheader.tpl"}

<h3>Suivi annulation des odf</h3>
<br />
<form method="post">

<fieldset>
  <legend>Accepter l'annulation</legend>
  <ul>
	<input type="submit" name="accepter" value="Accepter" />
  </ul>
</fieldset>

<fieldset>
  <legend>Rejeter l'annulation</legend>
  <ul>
  	<input type="submit" name="rejeter" value="Rejeter" />
  </ul>
</fieldset>

<input type="submit" name="cancel" value="{tr}Cancel{/tr}" />

</form>
